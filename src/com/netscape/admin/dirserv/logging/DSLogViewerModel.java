/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.logging;

import java.io.*;
import java.util.*;
import java.net.*;
import javax.swing.table.DefaultTableModel;
import com.netscape.management.client.comm.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;
import com.netscape.admin.dirserv.task.CGIReportTask;
import com.netscape.admin.dirserv.GlobalConstants;

public class DSLogViewerModel extends DefaultTableModel {

	public DSLogViewerModel(ConsoleInfo consoleInfo, String url) {
		_serverID = (String)consoleInfo.get( "ServerInstance" );
		_url = consoleInfo.getAdminURL() + _serverID + "/" + url;
		_password = consoleInfo.getAuthenticationPassword();
		_username = (String)consoleInfo.get( GlobalConstants.TASKS_AUTH_DN );
		_password = (String)consoleInfo.get( GlobalConstants.TASKS_AUTH_PWD );
	}

    protected void init( String[] columnIDs ) {
		for( int i = 0; i < columnIDs.length; i++ ) {
			String name = 
				_resource.getString( _section, "header-" + columnIDs[i] );
			addColumn( name );
		}
	}

    public void updateNow() {
		updateLogData();
	}

    public int getRowCount() {
		updateLogLength();
		return super.getRowCount();
	}

	/**
	 * Don't allow editing in the table of this model.
	 *
	 * @param row Row to report on.
	 * @param column Column to report on.
	 */
    public boolean isCellEditable(int row, int column) {
		return false;
    }

    /**
     * Fetch data from Directory/Admin Server.
     *
     */
    public void updateLogData() {
        if (!_updating) {
            _updating = true;       // this gets reset in derived classes finish()
            Hashtable args = new Hashtable();
            /* Set which server's log you want */
            args.put("id", _serverID);
            /* Set which logfile you want to view */
            args.put("file", getLogFileName());
            /* Tack on the number of lines to return */
            args.put("num", Integer.toString(getLineCount()));
            /* We only want the CGI to return raw log content */
            args.put("nohtml", "1");
            /* If a filter string was specified, tack that on */
            if ( getFilter() != null )
                args.put("str", getFilter());
            try {
                URL url = new URL( _url );
                Debug.println( "DSLogViewerModel.updateLogData: " + url );
                HttpManager _httpManager = new HttpManager();
                // tell the http manager to use UTF8 encoding
                _httpManager.setSendUTF8(true);
                InputStream data = CGIReportTask.encode(args);
                _httpManager.post(url, getCommClient(), null,
                                  data, data.available(),
                                  CommManager.FORCE_BASIC_AUTH|
                                  CommManager.ASYNC_RESPONSE);
                waitForFinish();
            }
            catch (Exception e) {
                Debug.println( "DSLogViewerModel.updateLogData for " +
                               _url + ": " + e );
            }
            fireTableDataChanged();
            Debug.println("DSLogViewerModel done requesting");
        }
    }

    public void updateLogLength() {
	}

	CommClient getCommClient() {
		return null;
	}

	public String getLogFileName() {
		return _logName;
	}

	public void setLogFileName( String name ) {
		_logName = name;
	}

    public int getLineCount() {
		return _lineCount;
	}

    public void setLineCount( int count ) {
		_lineCount = count;
	}

    public String getFilter() {
		return _filter;
	}

    public void setFilter( String filter ) {
		_filter = filter;
	}

    public static String unEscape( String line ) {
		int ind = line.indexOf( '\\' );
		if ( ind < 0 ) {
			return line;
		}
		byte[] input;
		byte[] b;
		try {
			input = line.getBytes( "UTF8" );
			b = line.getBytes( "UTF8" );
		} catch ( UnsupportedEncodingException e ) {
			Debug.println( "DSLogViewModel.unEscape: " + e + " , getBytes " +
						   line );
			return line;
		} catch ( Exception ex ) {
			Debug.println( "DSLogViewModel.unEscape: " + ex + " , getBytes " +
						   line );
			return line;
		}
		int bIndex = 0;
		int len = input.length;
		bIndex = ind;
		for( int i = ind; i < len; ) {
			char ch = (char)input[i];
			if ( ch == '\\' ) {
				if ( i > (len-3) ) {
					break;
				}
				i++;
				String s = "";
				try {
					s = new String( input, i, 2 );
					int num = Integer.parseInt( s, 16 );
					b[bIndex++] = (byte)num;
					i += 2;
				} catch ( NumberFormatException ex ) {
					/* The server does not currently use the v2 style
					   escaping, but this should work, if it does in the
					   future */
					b[bIndex++] = (byte)'\\';
					b[bIndex++] = input[i++];
				} catch ( Exception e ) {
					Debug.println( "DSLogViewModel.unEscape: " + e +
								   " , parsing " + s );
					return line;
				}
			} else {
				b[bIndex++] = input[i++];
			}
		}
		try {	
			return new String( b, 0, bIndex, "UTF8" );
		} catch ( UnsupportedEncodingException e ) {
			Debug.println( "DSLogViewModel.unEscape: " + e +
						   " , forming new String from " + line );
			return line;
		}
	}

    public synchronized void waitForFinish() {
		_thread = Thread.currentThread();
		try {
			_finished = false;
			while (!_finished) {
				Debug.println("DSLogViewerModel.waitForFinish: going to wait");			
				wait();
				Debug.println("DSLogViewerModel.waitForFinish: wait done");
			}			
		} catch(Exception e) {			
			Debug.println("DSLogViewerModel.waitForFinish: Exception: "+e);			
		}
	}

	synchronized void finish() {		
		try {
			Debug.println("DSLogViewerModel.finish: going to notify");
			_finished = true;
			notify();
			Debug.println("DSLogViewerModel.finish: notify done");			
		} catch(Exception e) {
			Debug.println("DSLogViewerModel.finish: Exception: "+e);
		}		
	}
		

	private boolean _finished = false;

	private int _lineCount = 25;
	private String _filter = null;
	protected String _url;
	String _username;
	String _password;
        private String _serverID = null;
	private String _logName = null;
	protected static ResourceSet _resource;
	protected static String _section;
    boolean _updating = false;
	protected Thread _thread = null;
}
