/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.logging;

import java.io.*;
import java.util.*;
import java.net.*;
import com.netscape.management.client.comm.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;
import com.netscape.admin.dirserv.DSUtil;

public class ErrorLogViewerModel extends DSLogViewerModel {

	public ErrorLogViewerModel(ConsoleInfo consoleInfo, String url) {
		super( consoleInfo, url );
		_resource = DSUtil._resource;
		_section = "errorlog-content";
		setLogFileName( "Errors" );
		init( _columnIDs );
	}

	CommClient getCommClient() {
		return new ErrorLogDataClient( this );
	}

	class ErrorLogDataClient extends LogDataClient {
		ErrorLogDataClient( DSLogViewerModel model ) {
			super( model );
		}

        protected ErrorLogData parse(String logEntry) {
			logEntry = unEscape( logEntry );
			StringBuffer buffer = new StringBuffer(logEntry);
			int bufferLength = buffer.length();

			/* Skip various HTML output */
			if ( (bufferLength < 10) || (buffer.charAt(0) != '[') ) {
//			    Debug.println( "Skipping row: " + logEntry );
				return null;
			}

			ErrorLogData data = new ErrorLogData();
			int state = STATE_START;

			for(int index = 0; index < bufferLength; index++) {
				char character = buffer.charAt(index);
				switch(state) {
				case STATE_START:
					if ( character == '[' )
						state = STATE_DATE;
					break;
	
				case STATE_DATE:
					if ( character == ':' )
						state = STATE_TIME;
					else
						data.date.append(character);
					break;
	
				case STATE_TIME:
					if ( character == ']' )
						state = STATE_DETAIL;
					else if ( character == '-' ) {
						/* Skip past the GMT offset */
						index++;
						while( buffer.charAt(index) != ']' )
							index++;
						state = STATE_DETAIL;
					} else
						data.time.append(character);
					break;
	
				case STATE_DETAIL:
					data.detail.append(character);
					break;
				}
			}
			return data;
		}

		/**
		 * Will be called on receipt of a response from the communication
		 * channel.
		 *
		 * @param replyStream an InputStream object for the response.
		 *  Note that the InputStream is a byte-oriented input API; if
		 *  you intend to read characters from the response stream, you
		 *  should open an InputStreamReader object on the stream to
		 *  read 16-bit unicode characters, to support internationalization.
		 * @param cr the CommRecord object of the current communication
		 *  transaction.
		 * @see CommRecord
		 */
	    public void replyHandler(InputStream replyStream, CommRecord cr) {
			InputStreamReader isr;
			try {
				isr = new InputStreamReader(replyStream, "UTF-8");
			} catch(UnsupportedEncodingException ee) {
				Debug.println(ee.toString());
				isr = new InputStreamReader(replyStream);
			}
			BufferedReader replyBuffer = new BufferedReader(isr);
			try {
				String logEntry;
                /* Empty the table */
    		    setNumRows( 0 );
				while ( (logEntry=replyBuffer.readLine()) != null ) {
					ErrorLogData d = parse(logEntry);
					if ( d != null ) {
						Object row[] = {d.date, d.time, d.detail};
						_model.addRow(row);
					}
				}
			}
			catch (Exception e) {
				Debug.println(e.toString());
			}
			finish();
		}
	}

	static final int STATE_START      = 0;
	static final int STATE_DATE       = 1;
	static final int STATE_TIME       = 2;
	static final int STATE_DETAIL     = 3;

	private static String[] _columnIDs = {"date", "time", "details"};
}

		class ErrorLogData {
			StringBuffer date = new StringBuffer();
			StringBuffer time = new StringBuffer();
			StringBuffer detail = new StringBuffer();
		}
