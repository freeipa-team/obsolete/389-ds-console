/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.logging;

import java.io.*;
import java.util.*;
import java.net.*;
import javax.swing.table.*;
import com.netscape.management.client.comm.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;
import com.netscape.admin.dirserv.DSUtil;

/**
 * LogDataClient
 * Base class for log renderers
 *
 * @version 1.0
 * @author rweltman
 **/
abstract public class LogDataClient implements CommClient {
    public LogDataClient( DSLogViewerModel model ) {
		_model = model;
    }

    abstract public void replyHandler(InputStream replyStream, CommRecord cr);

	/**
	 * Will be called on recognition of an error from the communication
	 * channel.
	 *
	 * @param exception the error exception
	 * @param cr the CommRecord object of the current communication
	 *  transaction.
	 * @see CommRecord
	 */
     public void errorHandler(Exception exception, CommRecord cr) {
		 Debug.println("errorHandler: " + exception);
		 finish();
	 }

	/**
	 * Will be called on recognition of an authentication request from
	 * the communication channel.
	 *
	 * @param authObject a protocol-specific authentication argument.
	 * @param cr the CommRecord object of the current communication
	 *  transaction.
	 * @return the username for authentication.
	 * @see CommRecord
	 */
     public String username(Object authObject, CommRecord cr) {
		 return _model._username;
	 }

	/**
	 * Will be called on recognition of an authentication request from
	 * the communication channel.
	 *
	 * @param authObject a protocol-specific authentication argument.
	 * @param cr the CommRecord object of the current communication
	 *  transaction.
	 * @return the password for authentication.
	 * @see CommRecord
	 */
    public String password(Object authObject, CommRecord cr) {
		return _model._password;
	}

    public synchronized void waitForFinish() {
		while(!_finished) {
			try {
				wait();
			} catch(Exception e) {
				// TODO: do something
			}
		}
	}

    public synchronized void finish() {
		_finished = true;
		_model._updating = false;
		notifyAll();
		_model.finish();
	}
		
    private boolean _finished = false;
    protected DSLogViewerModel _model;
}
