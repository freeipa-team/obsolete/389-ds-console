/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.logging;

import java.io.*;
import java.util.*;
import java.net.*;
import com.netscape.management.client.comm.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;
import com.netscape.admin.dirserv.DSUtil;

public class AuditLogViewerModel extends DSLogViewerModel {

	public AuditLogViewerModel(ConsoleInfo consoleInfo, String url) {
		super( consoleInfo, url );
		_resource = DSUtil._resource;
		_section = "auditlog-content";
		setLogFileName( "Audit" );
		init( _columnIDs );
	}

	CommClient getCommClient() {
		return new AuditLogDataClient( this );
	}

	class AuditLogDataClient extends LogDataClient {
		AuditLogDataClient( DSLogViewerModel model ) {
			super( model );
		}

        protected AuditLogData parse(String logEntry) {			
			logEntry = unEscape( logEntry );
			AuditLogData data = null;
			/* Just to skip the final lines sent by the CGI when looking for 
			   Logs' content */
			if (!logEntry.startsWith("NMC_STATUS:") &&				
				!logEntry.startsWith("Finished at ds_viewlog.pl")) {
				data = new AuditLogData();
				/* No fields to parse, just grab the whole thing */
				data.detail.append( logEntry );
			}
			return data;
		}

		/**
		 * Will be called on receipt of a response from the communication
		 * channel.
		 *
		 * @param replyStream an InputStream object for the response.
		 *  Note that the InputStream is a byte-oriented input API; if
		 *  you intend to read characters from the response stream, you
		 *  should open an InputStreamReader object on the stream to
		 *  read 16-bit unicode characters, to support internationalization.
		 * @param cr the CommRecord object of the current communication
		 *  transaction.
		 * @see CommRecord
		 */
	    public void replyHandler(InputStream replyStream, CommRecord cr) {
			InputStreamReader isr;
			try {
				isr = new InputStreamReader(replyStream, "UTF-8");
			} catch(UnsupportedEncodingException ee) {
				Debug.println(ee.toString());
				isr = new InputStreamReader(replyStream);
			}
			BufferedReader replyBuffer = new BufferedReader(isr);
			try {
				String logEntry;
				Vector data = _model.getDataVector();
                /* Empty the table */
    		    setNumRows( 0 );
				while ((logEntry=replyBuffer.readLine()) != null) {
					AuditLogData d = parse(logEntry);
					if ( d != null ) {
						Object row[] = {d.detail};
						_model.addRow(row);
					}
				}
			}
			catch (Exception e) {
				Debug.println(e.toString());
			}
			finish();
		}
	}

	private static String[] _columnIDs = {"details"};
}

		class AuditLogData {
			StringBuffer detail = new StringBuffer();
		}

