/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv;

import java.awt.*;
import javax.swing.*;
import com.netscape.management.client.util.Debug;

/**
 */
public class SuiScrollPane
	extends com.netscape.management.nmclf.SuiScrollPane
{
    public SuiScrollPane(Component view, int vsbPolicy, int hsbPolicy) {
		super(view, vsbPolicy, hsbPolicy);
    }

    public SuiScrollPane(Component view) {
		this(view, VERTICAL_SCROLLBAR_AS_NEEDED, HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }

    public SuiScrollPane(int vsbPolicy, int hsbPolicy) {
		this(null, vsbPolicy, hsbPolicy);
    }

    public SuiScrollPane() {
		this(null, VERTICAL_SCROLLBAR_AS_NEEDED, HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }

    public JScrollBar createVerticalScrollBar() 
	{
		return new SuiScrollBar(JScrollBar.VERTICAL);
    }

	public void setUpdateWhileAdjusting( boolean update ) {
		updateWhileAdjusting = update;
	}
	private boolean updateWhileAdjusting = false;

	/**
	 * THIS is an extension of JScrollBar.ScrollBar()
	 * 
	 */
	class SuiScrollBar extends JScrollBar
	{
		public SuiScrollBar(int orientation, int value, int extent,
							int min, int max) {
			checkOrientation(orientation);
			blockIncrement = (extent == 0) ? 1 : extent;
			orientation = orientation;
			setModel(new SuiBoundedRangeModel(value, extent, min, max));
			updateUI();
		}

		private void checkOrientation(int orientation) {
			switch (orientation) {
			case VERTICAL:
			case HORIZONTAL:
				break;
			default:
				throw new IllegalArgumentException("orientation must be one of: VERTICAL, HORIZONTAL");
			}
		}

		public SuiScrollBar(int orientation) {
			this(orientation, 0, 10, 0, 100);
		}

		public SuiScrollBar() {
			this(VERTICAL);
		}

		/**
		 * THIS COMES FROM JScrollBar.ScrollBar()
		 * 
		 * If the viewports view is a Scrollable then ask the view
		 * to compute the unit increment.  Otherwise return
		 * super.getUnitIncrement().
		 * 
		 * @see Scrollable#getScrollableUnitIncrement
		 */
		public int getUnitIncrement(int direction) {
			JViewport vp = getViewport();
			if ((vp != null) && (vp.getView() instanceof Scrollable)) {
				Scrollable view = (Scrollable)(vp.getView());
				Rectangle vr = vp.getViewRect();
				int incr = view.getScrollableUnitIncrement(vr, getOrientation(), direction);
				Debug.println(8, "SuiScrollBar.getUnitIncrement: incr=" + incr);
				return incr;
			}
			else {
				int incr = super.getUnitIncrement(direction);
				Debug.println(8, "SuiScrollBar.getUnitIncrement: incr=" + incr);
				return incr;
			}
		}

		/**
		 * THIS COMES FROM JScrollBar.ScrollBar()
		 * 
		 * If the viewports view is a Scrollable then ask the
		 * view to compute the block increment.  Otherwise
		 * the blockIncrement equals the viewports width
		 * or height.  If there's no viewport reuurn 
		 * super.getBlockIncrement().
		 * 
		 * @see Scrollable#getScrollableBlockIncrement
		 */
		public int getBlockIncrement(int direction) {
			JViewport vp = getViewport();
			if (vp == null) {
				return super.getBlockIncrement(direction);
			}
			else if (vp.getView() instanceof Scrollable) {
				Scrollable view = (Scrollable)(vp.getView());
				Rectangle vr = vp.getViewRect();
				return view.getScrollableBlockIncrement(vr, getOrientation(), direction);
			}
			else if (getOrientation() == VERTICAL) {
				return vp.getExtentSize().width;
			}
			else {
				return vp.getExtentSize().height;
			}
		}
	}

	class SuiBoundedRangeModel extends DefaultBoundedRangeModel
	{
		public SuiBoundedRangeModel() {
			super();
		}

		public SuiBoundedRangeModel(int value, int extent, int min, int max) {
			super(value, extent, min, max);
		}

		/** 
		 * Run each ChangeListeners stateChanged() method.
		 * 
		 * @see #setRangeProperties
		 * @see EventListenerList
		 */
		protected void fireStateChanged() {
			if(updateWhileAdjusting || !getValueIsAdjusting())
			{
				super.fireStateChanged();
			}
		}   
	}
}
