/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.event.*;
import java.util.*;
import java.awt.*;

import javax.swing.*;

import com.netscape.management.client.ug.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.panel.UIFactory;

public class DSSearchPanel extends JPanel
                           implements ActionListener,
	                       IStandardResPickerPlugin {

	ConsoleInfo          _ConsoleInfo;
	AttributeSearchFilter[] _AttributeSearchFilter;

	public void initialize( ConsoleInfo info ) {
		_ConsoleInfo = info;
	}

	public String getID() {
		Debug.println( "DSSearchPanel.getID" );
		return "DSSearchPanel";
	}

	public String getDisplayName() {
		return _displayName;
	}

	public Component getSearchUI() {
		Debug.println( "DSSearchPanel.getSearchUI" );
		return this;
	}

	public void setDisplayAttribute(AttributeSearchFilter arFilter[]) {
		_AttributeSearchFilter = arFilter;
	}

	public AttributeSearchFilter[] getDisplayAttribute() {
		return _AttributeSearchFilter;
	}

	public void actionPerformed(ActionEvent e) {
	}

	public DSSearchPanel() {
		super();
		setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor     = gbc.WEST;
		gbc.gridheight = 1;
		gbc.fill       = gbc.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;

        JLabel label = UIFactory.makeJLabel(_section, "filter");

		gbc.gridwidth = 1;
		gbc.insets = new Insets(0,PAD,3,8);
		add(label, gbc);

		_tfFilter = new JTextField(30);
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = new Insets(0,0,3,PAD);
		gbc.gridx = 1;
		gbc.weightx = 1;
		add(_tfFilter, gbc);
	}

	public String getFilterString() {
		return _tfFilter.getText();
	}

	public Dimension getPreferredSize() {
		return getSize();
	}

    public void help() {
		DSUtil.help( _helpToken );
	}

    private JTextField _tfFilter;
	private static final int PAD = 10;
	private static final String _section = "UserGroup";
	private static final String _helpToken = "search-dbox-veryadvanced-help";
	static ResourceSet _resource = DSUtil._resource;
    private static final String _displayName =
	    _resource.getString( _section, "Very-Advanced-label" );
}
