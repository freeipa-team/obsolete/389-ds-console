/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.event.*;
import java.util.*;
import javax.swing.*;

/**
 * DSRadioMenu
 * 
 *
 * @version 1.0
 * @author rweltman
 **/

public class DSRadioMenu extends JMenu implements ActionListener {

	/**
	 * Constructor for menu item containing only radio checkboxes
	 *
	 * @param s Menu label.
	 */
    public DSRadioMenu( String s ) {
		super( s, false );
	}

	/**
	 * Add a checkbox menu item.
	 *
	 * @param s Menu item label.
	 */
    public void add( String s, String category ) {
		JCheckBoxMenuItem item = new JCheckBoxMenuItem( s );
		add( item );
		if ( category != null ) {
			_items.put( item, category );
		}
		item.addActionListener( this );
	}

	/**
	 * On menu item selection, unselect all other menu items.
	 *
	 * @param e Event from the item selected.
	 */
    public void actionPerformed( ActionEvent e ) {
		JCheckBoxMenuItem item = (JCheckBoxMenuItem)e.getSource();
		String category = (String)_items.get( item );
		Enumeration en = _items.keys();
		while( en.hasMoreElements() ) {
			JCheckBoxMenuItem nextItem = (JCheckBoxMenuItem)en.nextElement();
			String nextCategory = (String)_items.get( nextItem );
			if ( nextCategory.equals( category ) ) {
				nextItem.setState( item == nextItem );
			}
		}
	}
	private Hashtable _items = new Hashtable();
}
