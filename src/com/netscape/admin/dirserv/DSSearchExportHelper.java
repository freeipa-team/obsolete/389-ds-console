/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.*;
import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import netscape.ldap.LDAPAttribute;


public class DSSearchExportHelper extends Object implements IEntryChangeListener,
													Runnable,
													ActionListener {
	public DSSearchExportHelper(String exportPath,
								LDAPConnection ldc,
								GenericProgressDialog dlg,
								String base) {
		_exportPath = exportPath;
		_dlg = dlg;
		_ldc = ldc;
		_base = base;
		
		setupDialog();
	}
	protected void setupDialog() {
		String[] args = {DSUtil.inverseAbreviateString(_exportPath,40)};
		_dlg.addStep(DSUtil._resource.getString(_section, "LDAPSearchMode-firstStep-title", args ));
		_dlg.addActionListener((ActionListener)this);
	}
														
														/**
														 *	Handle incoming event from button.
														 *
														 * @param e event
														 */
	public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( GenericProgressDialog.CANCEL ) ) {			
			_continue = false;
			_dlg.disableCancelButton();			
			_dlg.setTextInLabel(DSUtil._resource.getString(_section, "LDAPMode-cancelled-title"));				
			if ( _done ) {
				_dlg.closeCallBack();			
			}
		} else if ( e.getActionCommand().equals(GenericProgressDialog.CLOSE ) ) {				
			_continue = false;
			if ( _done ) {		
				_dlg.closeCallBack();
			}
		}
	}
														
														/**
														 * Called when an entry changes, e.g. on export/import. Return
														 * true to continue, false to stop.
														 */
														
	public boolean entryChanged( String dn, String msg ) {	
		if ( _continue ) {
			if (dn != null) {
				_entryNumber++;
				if ((_entryNumber % 5) == 0) {
					String[] args = { String.valueOf(_entryNumber),
									  DSUtil.abreviateString(dn, 30)};
					_dlg.setTextInLabel(DSUtil._resource.getString(_section, "LDAPSearchMode-progressLabel-title", args));
				}
			}
			return true;
		} else {
			return false;
		}
	}
														
	public boolean entryChanged( String dn ) {
		return entryChanged( dn, null );
	}
														
														
	public void run() {		
		/* To let the main thread display the progress dialog */
		try {
			Thread.sleep(400);
		} catch (Exception e) {
			Debug.println("DSSearchExportHelper.run: " +
						  e );
			e.printStackTrace();	
		}
		executeExport(_exportPath, _base);
	}								
														
	private void executeExport(String exportPath, String base) {
		DSExportImport ds =
			new DSExportImport( _ldc, exportPath, base, true );
		_entryNumber = 0;
		try {
			Thread.sleep(100);
		} catch (Exception e) {				
		}
		ds.addEntryChangeListener(this);
		ds.run();
		boolean status = ds.getStatus();
		if ( status ) {
			if ( ds.getError() == ds.STATUS_CANCEL ) {
				_dlg.closeCallBack();			

			} else {
				_dlg.stepCompleted(0);
				String[] args = { Integer.toString(ds.getEntryCount()) };
				_dlg.setTextInLabel(DSUtil._resource.getString(_section,
															   "succeeded-msg", 
															   args ));				
			}
		} else {
			Exception ex = ds.getException();
			Debug.println("DSSearchExportHelper.executeExport "+ex);
			if ( (ex != null) && (ex instanceof LDAPException) ) {
				if (ds.getChainingSuffix() != null) {
					String extendedMessage = ((LDAPException)ex).getLDAPErrorMessage();
					String[] args = { ds.getChainingName(),
									  ds.getChainingSuffix(),
									  ((LDAPException)ex).errorCodeToString(), 
									  extendedMessage};
					_dlg.setTextInLabel(DSUtil._resource.getString(_section,
																   "failedinchaining-msg",
																   args));
				} else {
					String extendedMessage = ((LDAPException)ex).getLDAPErrorMessage();
					String matchedDN = ((LDAPException)ex).getMatchedDN();
					String msg;
					if ( (extendedMessage != null) && (extendedMessage.length() > 0) ) {
						if ( (matchedDN != null) && (matchedDN.length() > 0) ) {
							String[] args = { ((LDAPException)ex).errorCodeToString(), extendedMessage,
											  matchedDN };
							msg = DSUtil._resource.getString( "general",
															  "ldap-error-dn-msg", args );
						} else {
							String[] args = { ((LDAPException)ex).errorCodeToString(), extendedMessage };
							msg = DSUtil._resource.getString( "general",
															  "ldap-error-msg", args );
						}
					} else {
						msg = ((LDAPException)ex).errorCodeToString();
					}
					_dlg.setTextInLabel(msg);
				}
			} else {
				if (ds.getChainingSuffix() != null) {		
					String[] args = { ds.getChainingName(),
									  ds.getChainingSuffix()
					};
					_dlg.setTextInLabel(DSUtil._resource.getString(_section,
																   "failedinchainingnonldaperror-msg",
																   args));
				} else {
					_dlg.setTextInLabel(DSUtil._resource.getString(_section,"failed-msg"));
				}
			}				
		}
		_done = true;
		_dlg.waitForClose();
	}
														
    private int _entryNumber = 0;													
	private GenericProgressDialog _dlg;
	private String _base;
	private String _exportPath;
	private boolean _continue = true;
	private boolean _done = false;
	LDAPConnection _ldc;
	private String _section = "export";
}
