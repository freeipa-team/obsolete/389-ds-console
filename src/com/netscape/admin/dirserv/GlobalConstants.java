/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

/**
 * GlobalConstants
 * Constants used throughout the Directory console
 *
 * @version 1.0
 * @author rweltman
 **/
public class GlobalConstants {
	static public String PREFERENCES_CONFIRM = "Confirmation";
	static public String PREFERENCES_CONFIRM_DELETE_OBJECTCLASS =
	    "ConfirmationDeleteObjectclass";
	static public String PREFERENCES_CONFIRM_DELETE_ATTRIBUTE =
	    "ConfirmationDeleteAttribute";
	static public String PREFERENCES_CONFIRM_DELETE_ENTRY =
	    "ConfirmationDeleteEntry";
	static public String PREFERENCES_CONFIRM_DELETE_SUBTREE =
	    "ConfirmationDeleteSubtree";
	static public String PREFERENCES_CONFIRM_DELETE_INDEX =
	    "ConfirmationDeleteIndex";	
	static public String PREFERENCES_CONFIRM_DELETE_AGREEMENT =
	    "ConfirmationDeleteAgreement";
	static public String PREFERENCES_CONFIRM_MODIFY_CHANGELOG =
	    "ConfirmationModifyChangeLog";
	static public String PREFERENCES_CONFIRM_OVERWRITE_DATABASE =
	    "ConfirmationOverwriteDatabase";
	static public String PREFERENCES_CONFIRM_STOP_SERVER =
	    "ConfirmationStopServer";
									
	// this is the bind DN to use when looking up tasks
	// in the Config directory
	static public final String TASKS_AUTH_DN = "AdminUsername";
	static public final String TASKS_AUTH_PWD = "AdminUserPassword";

	// This is the key for storing a ConsoleInfo inside another ConsoleInfo.
	// It's used by DSTaskModel.findTasks() when allocating
	// ConsoleInfo for each tasks.
	static public final String CONSOLE_INFO = "consoleinfo";
}
