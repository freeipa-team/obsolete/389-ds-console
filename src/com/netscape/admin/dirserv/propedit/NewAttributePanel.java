/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.propedit;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.Vector;
import java.util.Hashtable;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.UITools;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.BlankPanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;

/**
 * NewAttributePanel
 * 
 *
 * @version 1.0
 * @author rweltman
 **/
public class NewAttributePanel extends BlankPanel {
	public NewAttributePanel( IDSModel model, Vector attrs,
							  Hashtable used ) {
		super( model, _section, false );
		setTitle( _resource.getString(_section, "title") );
		_attrs = attrs;
		_used = used;
		_helpToken = "property-new-attribute-dbox-help";
	}

	public void init() {
        _myPanel.setLayout( new GridBagLayout() );
		GridBagConstraints gbc = getGBC();
		gbc.insets.top = 0;
		gbc.insets.left = 0;
		gbc.ipadx = 0;

		JLabel langLabel = makeJLabel( _section, "languages", _resource );
		gbc.gridwidth = 1;
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1;
        _myPanel.add( langLabel, gbc );

		JLabel subtypesLabel = makeJLabel( _section, "subtypes", _resource );
        gbc.gridwidth = gbc.REMAINDER;
        _myPanel.add( subtypesLabel, gbc );

	    _comboLanguages = makeJComboBox( _section, "languages",
										 " ", _resource );
		langLabel.setLabelFor(_comboLanguages);

		/* Blank first item */
		_comboLanguages.addItem( " " );
		_languages = getAliases( "languages", _comboLanguages );
		gbc.gridwidth = 1;
        _myPanel.add( _comboLanguages, gbc );

	    _comboSubtypes = makeJComboBox( _section, "subtypes",
										 " ", _resource );
		subtypesLabel.setLabelFor(_comboSubtypes);

		/* Blank first item */
		_comboSubtypes.addItem( " " );
		_subtypes = getAliases( "subtypes", _comboSubtypes );
        gbc.gridwidth = gbc.REMAINDER;
        _myPanel.add( _comboSubtypes, gbc );

		_list = makeJList( _attrs );
		_list.setVisibleRowCount(20);
		_list.setSelectedIndex( 0 );
        JScrollPane scroll = new JScrollPane(_list);
		scroll.setBorder( UITools.createLoweredBorder() );
		gbc.weighty = 1;
		gbc.fill = gbc.BOTH;
        _myPanel.add( scroll, gbc );

		AbstractDialog dlg = getAbstractDialog();
		if ( dlg != null ) {
			dlg.setOKButtonEnabled( false );
		}
	}

    private String[] getAliases( String keyword, JComboBox combo ) {
		/* Get all keys */
		Vector v = new Vector();
		String s = _langResource.getString( "userPage", keyword );
		StringTokenizer st = new StringTokenizer( s );
		while( st.hasMoreTokens() ) {
			v.addElement( st.nextToken() );
		}
		String[] str = new String[v.size()];
		v.copyInto( str );
		v.removeAllElements();
		v = null;
		/* Look up all aliases, and store a reverse reference */
		Hashtable h = new Hashtable();
		String[] aliases = new String[str.length];
		for( int i = 0; i < str.length; i++ ) {
			String key = str[i];
			String lang = _langResource.getString( "userPage", key );
			if ( lang == null ) {
				lang = key;
			}
			lang = lang.trim();
			h.put( lang, key );
			aliases[i] = lang;
		}
		/* Sort the aliases */
		DSUtil.bubbleSort( aliases );
		/* Sort keys by order of aliases */
		for( int i = 0; i < str.length; i++ ) {
			str[i] = (String)h.get( aliases[i] );
			combo.addItem( aliases[i] );
		}
		h.clear();
		h = null;
		return str;
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
    }

	/**
	 * Some list component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void valueChanged(ListSelectionEvent e) {
//        Debug.println("NewAttributePanel.valueChanged" );
		AbstractDialog dlg = getAbstractDialog();
		if ( dlg != null ) {
			dlg.setOKButtonEnabled( _list.getSelectedValue() != null );
		}
    }

    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

    public void okCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		int i;

		Object[] values = _list.getSelectedValues();			
		Vector vector = new Vector();
		for (i=0; i<values.length; i++) {
			if(values[i] instanceof String) {
				Debug.println("NewAttributePanel.okCallBack(): adding "+values[i]);
				vector.addElement(values[i]);
			}
		} 	   
		_attributes = new String[vector.size()];
		vector.copyInto(_attributes);
		vector = null;

		if ( _attributes == null ) {
			return;
		}
		int ind = _comboLanguages.getSelectedIndex();
		String lang = null;
		if ( ind > 0 ) {
			lang = ";lang-" + _languages[ind-1];
		}
		ind = _comboSubtypes.getSelectedIndex();
		String type = null;
		if ( ind > 0 ) {
			type = ";" + _subtypes[ind-1];
		}
		if ( lang == null ) {
			if ( type != null ) {
				for (i=0; i< _attributes.length; i++) {
					_attributes[i] += type;
				}
			}
		} else {
			if ( type == null ) {
				for (i=0; i< _attributes.length; i++) {
					_attributes[i] += lang;
				}
			} else {
				if ( lang.compareTo( type ) < 0 ) {
					for (i=0; i< _attributes.length; i++) {
						_attributes[i] += lang + type;
					}
				} else {
					for (i=0; i< _attributes.length; i++) {
						_attributes[i] += type + lang;
					}
				}
			}
		}
		Debug.println( "NewAttributePanel.okCallback: " + _attributes );
		for (i=0; i< _attributes.length; i++) {
			if ( _used.get( _attributes[i] ) != null ) {
				DSUtil.showErrorDialog( this, "attribute-inuse", _attributes[i],
										_section, _resource );
				_attributes = null;
				return;
			}
		}				

		if ((_attributes !=null) && (_attributes.length>0)) {
			/* By default the _attribute value is the first selected attribute */
			_attribute = _attributes[0];
		}

		hideDialog();
    }

	public void setUsed(Hashtable used) {
		_used = used;
	}
	
	public JList getList() {
		return _list;
	}
	
    public String getSelectedAttribute() {
		return _attribute;
	}

	public String[] getSelectedAttributes() {
		return _attributes;
	}

    public static void main( String[] args ) {
		Vector v = new Vector();
		String[] o = { "person", "inetorgperson", "organizationalunit" };
		for( int i = 0; i < o.length; i++ )
			v.addElement( o[i] );
		Hashtable h = new Hashtable();
		h.put( "person;lang-fr", "" );
		h.put( "inetorgperson;lang-en;phonetic", "" );
		Debug.setTrace( true );
		BlankPanel child =
			new NewAttributePanel( new DefaultResourceModel(), v, h );
		SimpleDialog dlg =
			new SimpleDialog( new JFrame(), child );
		dlg.setSize(300,280);
		dlg.setLocation(200,200);
		dlg.show();
    }

	private JComboBox _comboLanguages;
	private JComboBox _comboSubtypes;
	private JList _list;
	private Vector _attrs;
	private String _attribute = null;
	private String[] _attributes;
    private String[] _languages;
    private String[] _subtypes;
	private Hashtable _used;
    private final static String _section = "newattribute";
	private static ResourceSet _resource = DSEntryPanel._resource;
	private static ResourceSet _langResource = DSUtil._langResource;
}
