/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.propedit;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import netscape.ldap.LDAPSchema;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.components.GenericDialog;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DefaultResourceModel;
import com.netscape.admin.dirserv.DSSchemaHelper;

public class ChooseObjectClassDialog extends GenericDialog
                                implements ListSelectionListener,
								           MouseListener {

	public ChooseObjectClassDialog(JFrame frame, LDAPSchema schema) {
		super(
			frame, 
			_resource.getString(_section, "addObject-title"), 
			OK|CANCEL,
			HORIZONTAL);
		
		// Extract the list of classes from the schema
		Vector v = new Vector();
		Enumeration enumObjclasses = schema.getObjectClassNames();
		while (enumObjclasses.hasMoreElements()) {
			String objectclass = (String)enumObjclasses.nextElement();
			/* Check that the objectclass is not a forbidden objectclass */
			boolean isForbidden = false;
			for (int i=0; i<DSSchemaHelper.FORBIDDEN_OBJECTCLASSES.length; i++) {
				if (objectclass.equalsIgnoreCase(DSSchemaHelper.FORBIDDEN_OBJECTCLASSES[i])) {
					isForbidden = true;
					break;
				}
			}
			if (!isForbidden) {
				v.addElement( objectclass );
			}
		}
		String[] objectClasses = new String[v.size()];
		v.toArray(objectClasses);
		DSUtil.trimAndBubbleSort( objectClasses, true );
		
		// Make a list model from objectClasses
		DefaultListModel _listModel = new DefaultListModel();
		for (int i = 0; i < objectClasses.length; i++) {
			_listModel.addElement(objectClasses[i]);
		}
		
		// Make the list
		_list = new JList(_listModel);
		_list.addListSelectionListener(this);
		_list.addMouseListener(this);
		_list.setVisibleRowCount(20);
		ListSelectionModel m = _list.getSelectionModel();
		m.setSelectionMode(m.SINGLE_SELECTION);

		// Selection is empty so...
		setOKButtonEnabled(false);
		
		// Layout everything
		layoutComponents();
	}
	
	void layoutComponents() {
    	Container contentPane = getContentPane();
    	GridBagLayout gbl = new GridBagLayout();

        contentPane.setLayout(gbl);

        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.anchor     = gbc.CENTER;
        gbc.fill       = gbc.BOTH;
        gbc.insets     = new Insets(0, 0, 0, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;

        JLabel l = new JLabel(_resource.getString(_section, "addObject-prompt"));
        contentPane.add(l, gbc) ;

		JScrollPane scrollPane = new JScrollPane(_list);
        gbc.gridy++;
        gbc.weighty = 1.0;
		contentPane.add(scrollPane, gbc);
      
		pack();
	}


	public String getSelectedValue() {
		return (String)_list.getSelectedValue();
	}

	
	/**
	 * Maintain the state of the ok button.
	 */
	public void valueChanged(ListSelectionEvent e) {
		if ( ! e.getValueIsAdjusting() ) {
			int min = _list.getMinSelectionIndex();
			setOKButtonEnabled(min != -1);
		}
	}
	
	
	/**
	 * Implements MouseListener.
	 */
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() >= 2) {
			okInvoked();
			setVisible(false);
		}
	}
	
	
	public void mouseEntered(MouseEvent e) {}
	
	public void mouseExited(MouseEvent e) {}
	
	public void mousePressed(MouseEvent e) {}
	
	public void mouseReleased(MouseEvent e) {}
	
	
	
	private JList _list;
	private ListModel _listModel;
	private final static String _section = "EntryObject";
	private final static ResourceSet _resource = DSUtil._resource;	
};
