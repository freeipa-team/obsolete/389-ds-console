/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/


package com.netscape.admin.dirserv.propedit;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.text.Collator;
import java.net.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.nmclf.SuiConstants;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSMenu;
import com.netscape.admin.dirserv.DSRadioMenu;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DefaultResourceModel;
import com.netscape.admin.dirserv.AttributeAlias;
import com.netscape.admin.dirserv.SuiScrollPane;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import com.netscape.admin.dirserv.attredit.*;
import com.netscape.admin.dirserv.DSSchemaHelper;
import com.netscape.admin.dirserv.DSEntryObject;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.admin.dirserv.panel.GroupPanel;

import netscape.ldap.client.JDAPBERTagDecoder;
import netscape.ldap.LDAPControl;
import netscape.ldap.ber.stream.*;

/**
 * The property editor panel for a Directory entry.
  *
  * @author  rweltman
  * @version 1.1, 02/18/00
  * @date	 11/11/97
  */
public class DSEntryPanel extends JPanel implements ActionListener,
	FocusListener {
		
	/**
	 *	Construct a property editor panel with a data model.
	 *
	 * @param propModel Model for the property editor.
	 */

	public DSEntryPanel( DSPropertyModel propModel ) {
		this(propModel, SHOWINGDN_NONAMING);
	}

	public DSEntryPanel( DSPropertyModel propModel, int options ) {
		this(propModel, options, null);
	}

	public DSEntryPanel( DSPropertyModel propModel, int options, LDAPConnection ldc ) {
		_dataModel = propModel;		
		_options = options;
		_ldc = ldc;

		createListOfForbiddenNamingAttributes();

		setLayout(new BorderLayout());
		
		setBorder( new EmptyBorder( 9, 9, 9, 9 ) );

		createDNLabel();
		
		// Create a scrolling list of attribute editors
		_centerPane = initialize();		
		
		JPanel actionPane = createActionPanel();				

		add("East", actionPane);

		add( "Center", _centerPane );				
		// force redraw with correct sorting
		showAllAttributes(_cbAllowedAttributes.isSelected());
	}

	private void createListOfForbiddenNamingAttributes() {
		/* 
		   Create the list of forbidden naming attributes		   
		   */		
		String[] operationalAttributes = DSSchemaHelper.getOperationalAttributes(_dataModel.getSchema());
		Vector v = new Vector();
		v.addElement("objectclass");
		for (int i=0; i<operationalAttributes.length; i++) {
			if (!operationalAttributes[i].equals("nsuniqueid")) {
				v.addElement(operationalAttributes[i]);
			}
		}
		FORBIDDEN_NAMING_ATTRIBUTES = new String[v.size()];
		v.copyInto(FORBIDDEN_NAMING_ATTRIBUTES);				
	}
	
    /**
     * Find the hosting Window
     */
    private Window getOwnerWindow() {
		for (Container p = getParent(); p != null; p = p.getParent()) {
			if (p instanceof Window) {
				return (Window)p;
			}
		}
		return null;
    }
	
    /**
     * Find the hosting Frame
     */
    private Frame getOwnerFrame() {
		for (Container p = getParent(); p != null; p = p.getParent()) {
			if (p instanceof Frame) {
				return (Frame)p;
			}
		}
		return null;
    }
	
    private IAttributeEditor getEditorForType( String name,
											   String labelText,
											   Rectangle rect ) {
		IAttributeEditor editor = AttributeEditorFactory.makeEditor(
																	name,
																	labelText,
																	rect,
																	_dataModel.isSingleValued( name ),
																	_dataModel.getAttributeType( name ) );
		if ( editor != null ) {
			editor.setParent( this );
			/* For the object class editor, restrict it to only the known
			   values */
			if ( name.equalsIgnoreCase( OBJECTCLASS ) ) {
				Vector v = new Vector();
				Enumeration e = _dataModel.getSchema().getObjectClassNames();
				while ( e.hasMoreElements() ) {
					v.addElement( e.nextElement() );
				}
				String[] sorted = new String[v.size()];
				v.copyInto( sorted );
				sortStrings( sorted );
				for( int i = 0; i < sorted.length; i++ ) {
					v.setElementAt( sorted[i], i );
				}
				editor.setAllowedValues( v );
				editor.setSchema( _dataModel.getSchema() );
			}
		}
		return editor;
	}
		
    /**
     * Utility function to create a text field with associated label
     * @param x Starting column of label
     * @param y Pixel row of label and text field
	 * @param w Width
	 * @param h Height
	 * @param name Attribute name
     * @param labelText Text of labe
	 * @param v Vector of values
     */
	private IAttributeEditor makeField( Rectangle labelRect,
										String name, String labelText,
										Vector v ) {
		IAttributeEditor editor = getEditorForType( name,
													labelText,
													labelRect );
		try {
			if ( (v == null) || (v.size() < 1) ) {
				editor.addValue( "" );				
			} else {
				for( int i = 0; i < v.size(); i++ ) {
					editor.addValue( v.elementAt( i ) );
				}
			}
		} catch ( Exception e ) {
			Debug.println( "DSEntryPanel.makeField: " + e );
			if ( v == null ) {
				Debug.println( "... name = " + name + ", labelText = " +
							   labelText + ", v = null" );
			} else {
				Debug.println( "... name = " + name + ", labelText = " +
							   labelText + ", nValues = " + v.size() );
				for( int i = 0; i < v.size(); i++ )
					Debug.println( "... " + v.elementAt(i) );
			}
		}

		return editor;
	}


	/**
	 *	Create a scrolling list with attribute editors.
	 *
	 */
	private JComponent initialize() {		
		/* How many attributes on this page? */
		int index = 0;
		int nFields = _dataModel.getItemCount( index );
		IAttributeEditor[] editors = new IAttributeEditor[nFields];

		/* Create list data model */
		int x = margin;
		int y = margin;
		int values = 0;
		Rectangle labelRect = new Rectangle( x, y, labelWidth, labelHeight );
                
                //get effective rights data for this entry, if available
                getEffectiveRights();
                
		for( int i = 0; i < nFields; i++ ) {
			String name = _dataModel.getItemName( index, i );
			String label;
			if (_showingNames) {
				label = name;
			} else {
				label = AttributeAlias.getAlias( name );
			}
			Vector v = _dataModel.getItemValues( index, i );			
			if ( v != null ) {
				values += v.size();
			}
			String value;
			editors[i] = makeField( labelRect, name, label, v );
			_entries.addElement( editors[i] );

			editors[i].setDirty( false );
			setVisible( editors[i] );
			labelRect.y += buttonHeight + 5;
		}
		Debug.println( "Added " + nFields + " attributes, " +
					   values + " values" + " height " + labelHeight);

		_page = new JPanel();
		GridBagLayout layout = new GridBagLayout();		
		_cons.insets = new Insets( 0, 0, 1, 0 );
		_cons.fill = _cons.HORIZONTAL;
		_cons.gridwidth = _cons.REMAINDER;
		_cons.weightx = 1.0;

		_cons.weighty = 0.0;
		_cons.anchor = _cons.NORTHWEST;

		_page.setLayout( layout );
		for( int i = 0; i < nFields; i++ ) {
			_page.add( (JComponent)_entries.elementAt( i ), _cons );
		}
	
		_cons.weighty = 1.0;
		_cons.fill = _cons.BOTH;
		_glue = Box.createGlue();
		_page.add(_glue, _cons);		
		_cons.weighty = 0.0;
		_cons.fill = _cons.HORIZONTAL;

		/* Allow scrolling if not all attributes are visible */
                SuiScrollPane scrollPane = new SuiScrollPane(_page);
		scrollPane.getVerticalScrollBar().setUnitIncrement( labelHeight );
		scrollPane.getHorizontalScrollBar().setUnitIncrement( 8 );
		
		scrollPane.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );	
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = gbc.BOTH;
		gbc.anchor = gbc.NORTH;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		panel.add(scrollPane, gbc);	

		gbc.weighty = 0.0;
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.SOUTHWEST;		
		// By default
		gbc.insets.top = UIFactory.getComponentSpace();
		_dnLabel.setVisible(true);
		panel.add(_dnLabel, gbc);
		_dnLabel.setLabelFor(panel);
                
                //setup entry level rights label
                _entRights = new JLabel("Entry Level Rights: " + _entLevelRights);
                _entRights.setVisible(_showingEffRights);
                _entRights.setToolTipText("dn" + parseEffRightsString(_entLevelRights));
                panel.add(_entRights, gbc);
                
		if (_options != NOSHOWINGDN) {
			updateNamingIndexes();
		}	
		
		return panel;
	}
  
        // fill attribute level rights table
        private void fillAttrRights(String[] rights)
        {
            String attrName;
            String attrValue;
            int col_index;
            for(int i=0;i<rights.length;i++)
            {
                col_index=rights[i].indexOf(':');
                attrName=rights[i].substring(0,col_index);
                attrValue=rights[i].substring(col_index+1);
                // Fixup the bogus '50' that DS7.0 servers return
                if (attrValue.equals("50"))
                {
                    attrValue = "";
                }
                _attrLevelEffRights.put(attrName,attrValue);
            }
        }
        
   private void getEffectiveRights()
   {
       String[] myArray;
       // set up search attributes
        String baseDN = _dataModel.getTitle();
        String filter = "objectclass=*";
        String[] attrs = {"entryLevelRights", "attributeLevelRights"};
        String[] all = {"*"};
        
        // create GetEffRightsControl
        LDAPGetEffRightsControl getEffRightsControl = new LDAPGetEffRightsControl(_ldc.getAuthenticationDN(),false);
        
        // register GetEffRightsControl
        try{
            LDAPControl.register("1.3.6.1.4.1.42.2.27.9.5.2",LDAPGetEffRightsControl.class);
        } catch(LDAPException e){}
        
        // add GetEffRightsControl to end of existing list of controls
        LDAPConstraints oldConstraints = _ldc.getConstraints();
        LDAPConstraints newConstraints = (LDAPConstraints)oldConstraints.clone();
        LDAPControl[] oldControls = oldConstraints.getServerControls();
        LDAPControl[] newControls;
        if (oldControls != null){
           newControls = new LDAPControl[oldControls.length + 1];
           System.arraycopy(oldControls,0,newControls,0,oldControls.length);
           newControls[oldControls.length] = getEffRightsControl;
        }
        else
        {
           newControls = new LDAPControl[1];
           newControls[0] = getEffRightsControl;
        }
        newConstraints.setServerControls(newControls);
        _ldc.setConstraints(newConstraints);
        try{
            LDAPAttribute entLevelRights;
            LDAPAttribute attrLevelRights;
            String[] vals;
            
            //search for this entry
            LDAPSearchResults res =_ldc.search(baseDN,_ldc.SCOPE_BASE,filter,all,false);
            LDAPEntry ent = res.next();
            
            //extract entry/attribute level rights from returned entry
            entLevelRights=ent.getAttribute(attrs[0]);
            attrLevelRights=ent.getAttribute(attrs[1]);
            
            //set entry level rights string
            vals = entLevelRights.getStringValueArray();
            _entLevelRights = vals[0];
            // Fix up a server bug where it returns '50' when the bound user has no rights to get the rights
            if (_entLevelRights.equals("50")) 
            {
                _entLevelRights = "";
            }
            
            vals = attrLevelRights.getStringValueArray();
            //if attrLevelRights is singular
            if (vals.length == 1)
            {
                //set one value for all attribute level rights
                _allAttrRights = vals[0].substring(vals[0].indexOf(':') + 1);
                // Again, fix up the '50' thing
                if (_allAttrRights.equals("50"))
                {
                    _allAttrRights = "";
                }
            }
            else //otherwise
            {
                //set one rights value to null
                _allAttrRights = null;
                //and set up attribute level rights individually
                fillAttrRights(vals);
            }
        } catch(LDAPException e){
            // Since effective rights are optional, we don't care if we failed to 
            // read them for some reason. This can happen for example if the entry 
            // doesn't exist yet.
        }
        
   }

	/**
	 * Get results of the dialog.	 
	 *
	 * @return Any changes implied for the entry.
	 */
    public LDAPModificationSet getChanges() {		
		LDAPModificationSet mods = null;
		for( int i = 0; i < _entries.size(); i++ ) {
			IAttributeEditor ed = (IAttributeEditor)_entries.elementAt( i );
			if ( _toAdd.indexOf( ed ) >= 0 ) {
				Debug.println( "Not replacing " + ed + " since it is new" );
				continue;
			}
			if ( ed.isDirty() ) {
				Debug.println( "Replacing " + ed  );
				if ( mods == null )
					mods = new LDAPModificationSet();
				LDAPAttribute attr = new LDAPAttribute( ed.getName() );
				Vector v = ed.getValues();
				for( int j = 0; j < v.size(); j++ ) {
					Object o = v.elementAt( j );
					if ( o instanceof String ) {
						if ( ((String)o).length() > 0 ) {
							attr.addValue( (String)o );
						}
					} else {
						attr.addValue( (byte[])o );
					}
				}
				Debug.println( "To replace: " + attr );
				mods.add( LDAPModification.REPLACE, attr );
			}
		}
		for( int i = 0; i < _toAdd.size(); i++ ) {			
			IAttributeEditor ed = (IAttributeEditor)_toAdd.elementAt( i );
			String attributeName = ed.getName();			
			if ( mods == null )
				mods = new LDAPModificationSet();
			Vector v = ed.getValues();				
			LDAPAttribute attr = new LDAPAttribute( attributeName );
			for( int j = 0; j < v.size(); j++ ) {
				Object o = v.elementAt( j );
				if ( o instanceof String ) {
					if ( ((String)o).length() > 0 ) {
						attr.addValue( (String)o );
					}
				} else {
					attr.addValue( (byte[])o );
				}
			}
			if ( attr.getByteValues().hasMoreElements() ) {
				Debug.println( "To add: " + attr );										
				mods.add( LDAPModification.ADD, attr );
			}		
		}
		for( int i = 0; i < _toDelete.size(); i++ ) {
			IAttributeEditor ed = (IAttributeEditor)_toDelete.elementAt( i );
			if ( mods == null )
				mods = new LDAPModificationSet();
			LDAPAttribute attr = new LDAPAttribute( ed.getName() );
			Debug.println( "To delete: " + attr );
			mods.add( LDAPModification.DELETE, attr );
		}

		

		return mods;
	}

	/**
	 * Get results of the dialog.
	 *
	 * @return Any attributes defined for the entry.
	 */
    public LDAPAttributeSet getAttributes() {
		LDAPAttributeSet attrs = new LDAPAttributeSet();
		for( int i = 0; i < _entries.size(); i++ ) {
			IAttributeEditor ed = (IAttributeEditor)_entries.elementAt( i );
			LDAPAttribute attr = new LDAPAttribute( ed.getName() );
			Vector v = ed.getValues();
			for( int j = 0; j < v.size(); j++ ) {
				Object o = v.elementAt( j );
				if ( o instanceof String ) {
					if ( ((String)o).length() > 0 ) {
						attr.addValue( (String)o );
					}
				} else {
					if ( ((byte[])o).length > 0 ) {
						attr.addValue( (byte[])o );
					}
				}
			}
			if ( attr.size() > 0 ) {
				attrs.add( attr );
			}
		}
		return attrs;
	}

    /**
	 * Called when a menu is activated.
	 *
	 * @param e The event causing the method to be called.
	 */
	public void actionPerformed(ActionEvent e) {
		Window w = getOwnerWindow();
		if ( w == null ) {
			Debug.println( "DSEntryPanel.actionPerformed: " +
						   "no owner window" );
			return;
		} else {
			Debug.println( "DSEntryPanel.actionPerformed: " +
						   "owner window = " + w );			
		}
		Component comp = w.getFocusOwner();
		
		// first, try to use the currently selected editor to use to perform
		// the action
		IAttributeEditor ed = getSelectedEditor();
		
		// if we could not determine the currently selected editor, try to
		// figure out which one has focus and use that one
		if ( comp != null && ed == null ) {
			if (comp instanceof AttributeEditor) {
				ed = (IAttributeEditor)comp;
			} else {
				for (Container p = comp.getParent(); p != null;
					 p = p.getParent()) {
					if (p instanceof IAttributeEditor) {
						ed = (IAttributeEditor)p;
						break;
					}
				}
			}
			Debug.println( "DSEntryPanel.actionPerformed: " +
						   comp.getClass().getName() +
						   " has focus, command = " +
						   e.getActionCommand() );
		} else {
			Debug.println( "DSEntryPanel.actionPerformed: " +
						   "no focus component" );
//			return;
		}
		
		/* Finally, on Solaris and HPUX, focus transfers to the menu when you
		   pull down a menu, so at this point we don't know what editor
		   really should have focus; if so, look for the latest one. */
		if ( ed == null ) {
			Enumeration en = _visible.keys();
			while( en.hasMoreElements() ) {
				IAttributeEditor ied =
					(IAttributeEditor)_visible.get( en.nextElement() );
				if ( ied.isSelected() ) {
					ed = ied;
					Debug.println( "DSEntryPanel.actionPerformed: " +
								   "using as focus component - " +
								   ed.getClass().getName() +
								   " has focus, command = " +
								   e.getActionCommand() );
					break;
				}
			}
		}
		
		String name = null;
		if ( ed != null ) {
			name = ed.getName();
			comp = ed.getFocusField();
			Debug.println( "DSEntryPanel.actionPerformed: focus is on <" +
						   comp + "> within " + name );
		}
		
		boolean regenerate = false;
		
		if (e.getActionCommand().equals(SHOW_DN)) {
			setDNVisible(_cbShowDN.isSelected());			
		} else if (e.getActionCommand().equals(
											   DELETE_ATTRIBUTE)) {
			if ( (ed != null) && ed.isDeletable() ) {
				boolean isNamingAttribute = false;
				String[] namingAttributes = getNamingAttributes();
				if (namingAttributes != null) {
					for (int i=0; i < namingAttributes.length; i++) {
						if (name.equals(namingAttributes[i])) {
							isNamingAttribute = true;
							break;
						}
					}
				}
				if (isNamingAttribute) {
					DSUtil.showErrorDialog(getOwnerFrame(), "namingattribute-deleted", "" );
					Debug.println("DSEntryPanel.actionPerformed: the naming attribute can't be deleted");					
				} else {
					_page.remove( (Component)ed );
					_entries.removeElement( ed );
					if (_toAdd.indexOf(ed) >= 0) {
						/* The attribute is not in the entry: don't need to remove it */
						_toAdd.removeElement( ed );
					} else {
						_toDelete.addElement( ed );
					}
					
					_visible.remove(name);				
					validate();
				}
			}
			name = null;		
		} else if (e.getActionCommand().equals(
											   DELETE_VALUE)) {
			if ( ed != null ) {
				boolean isNamingAttribute = false;
				String[] namingAttributes = getNamingAttributes();
				if (namingAttributes != null) {
					for (int i=0; i < namingAttributes.length; i++) {
						if (name.equals(namingAttributes[i])) {
							isNamingAttribute = true;
							break;
						}
					}
				}
				if ((isNamingAttribute) && (ed.getValueCount() < 2)) {					
					DSUtil.showErrorDialog(getOwnerFrame(), "namingattribute-deleted", "" );
					Debug.println("DSEntryPanel.actionPerformed: the naming attribute can't be deleted");					 					
				} else {
					int oldIndex = ed.getFieldIndex((Object)comp);
					ed.deleteValue( comp );
					if ( name.equalsIgnoreCase( OBJECTCLASS ) ) {						
						Vector v = ed.getValues();
						_dataModel.updateObjectClasses( v );	
						/* This is used to update _allAttributes */
						_allAttributes = null;
						getAllAttributes();					
						validateEditors( v );
						regenerate = true;		
						
						if (updateNamingAttributes()) {
							/* The names attributes had to be changed */
							 DSUtil.showInformationDialog( getOwnerFrame(),
														   "naming-attrs-modified", 
														   (String[]) null, 
														   _section, 
														   _resource);
						}						
					}		
					if (isNamingAttribute) {
						Integer integer = (Integer)(_namingAttributeIndexes.get(name));
						int namingAttributeIndex = 0;
						if (integer != null) {
							namingAttributeIndex = integer.intValue();
						}
						if (oldIndex <= namingAttributeIndex) {
							namingAttributeIndex = namingAttributeIndex - 1;
							if (namingAttributeIndex < 0) {
								namingAttributeIndex = 0;
							}
							_namingAttributeIndexes.put(name, new Integer(namingAttributeIndex));
						}

						updateNamingValue(name);

						if (ed.getValueCount() > 1) {
							_bDeleteValue.setEnabled(true);
						} else {
							_bDeleteValue.setEnabled(false);
						}																
					}					
					validate();
				}	
			}			
		} else if (e.getActionCommand().equals(
											   ADD_ATTRIBUTE)) {			
			addAttribute();
			
		} else if (e.getActionCommand().equals(
											   ADD_VALUE)) {
			if ( ed != null ) {
				try {
					ed.addValue();
					if ( name.equalsIgnoreCase( OBJECTCLASS ) ) {
						Vector v = ed.getValues();
						_dataModel.updateObjectClasses( v );
						/* This is used to update _allAttributes */
						_allAttributes = null;
						getAllAttributes();		
						validateEditors( v );
						regenerate = true;				
					}				
					boolean isNamingAttribute = false;
					String[] namingAttributes = getNamingAttributes();
					if (namingAttributes != null) {
						for (int i=0; i < namingAttributes.length; i++) {
							if (name.equals(namingAttributes[i])) {
								isNamingAttribute = true;
								break;
							}
						}
					}
					
					if (isNamingAttribute) {
						if (ed.getValueCount() > 1) {
							_bDeleteValue.setEnabled(true);
						}						
					}
					invalidate();
					validate();		
				} catch ( Exception ex ) {
					Debug.println( "DSEntryPanel.actionPerformed: " +
								   "ADD_VALUE, " + ex );
					ex.printStackTrace();
				}				
			} else {
				Debug.println( "DSEntryPanel.actionPerformed: no editor " +
							   "has focus for ADD_VALUE" );
			}
		} else if (e.getActionCommand().equals(
											   SHOW_ALL_ATTRIBUTES)) {
			_showingAll = !_showingAll;
			regenerate = true;
                } else if (e.getActionCommand().equals(
											   SHOW_EFF_RIGHTS)) {
			_showingEffRights = !_showingEffRights;
                        _entRights.setVisible(_showingEffRights);
			regenerate = true;
		} else if (e.getActionCommand().equals(
											   SHOW_NAMES)) {
			if ( !_showingNames ) {		
				_showingNames = true;
				Enumeration en = _entries.elements();
				while( en.hasMoreElements() ) {
					IAttributeEditor editor =
						(IAttributeEditor)en.nextElement();
					editor.setLabelText( editor.getName() );
				}
				regenerate = true;
			}

		} else if (e.getActionCommand().equals(
											   SHOW_DESCRIPTIONS)) {
			if ( _showingNames ) {
				_showingNames = false;
				Enumeration en = _entries.elements();
				while( en.hasMoreElements() ) {
					IAttributeEditor editor =
						(IAttributeEditor)en.nextElement();
					editor.setLabelText(
										AttributeAlias.getAlias( editor.getName() ) );
				}
				regenerate = true;
			}		
		} else if (e.getActionCommand().equals(REFRESH)) {			
			Thread refresher = new Thread(new Refresher());
			try {
				refresher.start();
				return;
			} catch (Exception ex) {
				Debug.println("DSEntryPanel.actionPerformed "+ex);
			}		
		} else if (e.getActionCommand().equals(CHANGE_NAMING_ATTRIBUTE)) {
			actionChangeNamingAttribute();						
		}
		if ( regenerate ) {
			showAllAttributes( _showingAll );
		}
                

		/* Restore focus, if possible */
		if ( name != null ) {
			Object o = getVisible( name );
            if ( (o != null) && (o instanceof JComponent) ) {
				JComponent jc = (JComponent)o;
				jc.requestFocus();
				Dimension dim = jc.getSize();
				jc.scrollRectToVisible(
									   new Rectangle( 0, 0, dim.width, dim.height ) );
			}
		}
		updateButtons();
	}

	private void updateSelection( Component comp ) {
		/* Find an attribute editor in the containership chain */
		Debug.println(9, "DSEntryPanel.updateSelection: comp = " +
					  comp);
		while ( comp != null ) {
			if ( comp instanceof IAttributeEditor ) {
				/* Found one. Deselect any other selected editor */
				Enumeration en = _visible.keys();
				while( en.hasMoreElements() ) {
					IAttributeEditor ed =
						(IAttributeEditor)_visible.get( en.nextElement() );
					if ( ed.isSelected() && !ed.equals( comp ) ) {
						ed.setSelected( false );
						Debug.println(9, "DSEntryPanel.updateSelection: " +
									  "deselected editor " + ed);
						break;
					}
				}
				/* Select this one */
				if ( !((IAttributeEditor)comp).isSelected() ) {
					((IAttributeEditor)comp).setSelected( true );
					Debug.println(9, "DSEntryPanel.updateSelection: " +
								  "selected editor " + comp);
				}
				break;
			} else if ( comp instanceof Container ) {
				comp = ((Container)comp).getParent();
			} else {
				break;
			}
		}
	}
				
		
		/* Used to enable/disable properly the buttons*/
	private void updateButtons() {
		IAttributeEditor ed = getSelectedEditor();		
		if (ed != null) {
			String name = ed.getName();
			_bAddValue.setEnabled(true);
			_bAddAttribute.setEnabled(true);
			boolean isNamingAttribute = false;
			String[] namingAttributes = getNamingAttributes();
			if (namingAttributes != null) {
				for (int i=0; i < namingAttributes.length; i++) {
					if (name.equals(namingAttributes[i])) {
						isNamingAttribute = true;
						break;
					}
				}
			}
			if (isNamingAttribute) {			
				if (ed.getValueCount()<2) {
					_bDeleteValue.setEnabled(false);
				} else {
					_bDeleteValue.setEnabled(true);
				}				
				_bDeleteAttribute.setEnabled(false);
			} else if(ed.getName().equalsIgnoreCase(OBJECTCLASS)) {
				Component comp = ed.getFocusField();
				String value="";
				if (comp instanceof JTextField) {
					value = ((JTextField)comp).getText();
				}

				_bDeleteAttribute.setEnabled(false);
			
				if ((ed.getValueCount() < 3) || value.equalsIgnoreCase("top")) {
					/* We make the supposition that objectclass has as value 'top' and another objectclass*/
					_bDeleteValue.setEnabled(false);					
				} else {
					_bDeleteValue.setEnabled(true);
				}
			} else {
				if (ed.getValueCount() > 0) {
					_bDeleteValue.setEnabled(true);					
				} else {
					_bDeleteValue.setEnabled(false);
				}
					
				if (ed.isDeletable()) {
					_bDeleteAttribute.setEnabled(true);
				} else {
					_bDeleteAttribute.setEnabled(false);					
					if (ed.getValueCount() < 2) {
						_bDeleteValue.setEnabled(false);									
					}
				}							
			}
		} else {
			_bDeleteValue.setEnabled(false);
			_bAddValue.setEnabled(false);
			_bAddAttribute.setEnabled(true);
			_bDeleteAttribute.setEnabled(false);
		}
	}

 
	public void focusGained(FocusEvent e) {
		/* Find an attribute editor in the containership chain */
		updateSelection( e.getComponent() );		
		updateButtons();
	}

	public void focusLost(FocusEvent e) {
	}

		 
    private IAttributeEditor getEditor( String name ) {
		String label;
		if ( _showingNames ) {
			label = name;
		} else {
			label = AttributeAlias.getAlias( name );
		}
		Rectangle rect = new Rectangle( 0, 0, labelWidth, labelHeight );
		IAttributeEditor editor = getEditorForType( name,
													label,
													rect );
		return editor;
	}

    /**
	 * Put up a dialog prompting for a new attribute, and add it to the
	 * editor
	 */
    private void addAttribute() {
		Vector allAttrs = getAllAttributes();
	
		if ( allAttrs.size() < 1 ) {
			return;
		}
		
		NewAttributePanel child =
			new NewAttributePanel( new DefaultResourceModel(),
								   allAttrs, _visible );				
		
		SimpleDialog dlg =
			new SimpleDialog( getOwnerFrame(), child );
		dlg.getAccessibleContext().setAccessibleDescription(_resource.getString(_section, 
																				"newattribute-description"));
//		child.setDialog( dlg );
		dlg.setLocationRelativeTo(getOwnerWindow());

		dlg.packAndShow();

		String[] names = child.getSelectedAttributes();
		if ( names != null ) {	
			for (int j=0; j< names.length; j++) {
				boolean isDeleted = false;
				for(int i = 0; i < _toDelete.size(); i++ ) {
					IAttributeEditor ed = (IAttributeEditor)_toDelete.elementAt( i );
					if (names[j].equals(ed.getName())) {
						isDeleted = true;
						_toDelete.removeElementAt(i);
						break;
					}
				}
				

				IAttributeEditor editor = getEditor( names[j] );
				editor.addValue( editor.getDefaultValue() );
				editor.setDirty( true );
				/* Remember to save this added attribute (if it was deleted it means
				 that's already in the entry and so don't need to be added) */		
				if (!isDeleted) { 
					_toAdd.addElement( editor );
				}
				/* Get all attribute editors and sort them */
				int nAttrs = _entries.size();
				IAttributeEditor eds[] = new IAttributeEditor[nAttrs+1];
				for( int i = 0; i < nAttrs; i++ )
					eds[i] = (IAttributeEditor)_entries.elementAt( i );
				eds[nAttrs] = editor;
				replaceSortedEditors( eds );
				validate();					
			}
		}
	}
		
    /**
	 * Show attributes lacking values as well as ones that have values
	 *
	 * @param bAll If true, show all attributes, otherwise only those that
	 * have values
	 */
    private void showAllAttributes( boolean bAll ) {
		IAttributeEditor[] eds;
		if ( bAll ) {
			Vector allAttributes = getAllAttributes();
			int nAttrs = allAttributes.size();
			/* There may be additional subtyped attributes */
			int nVisible = _entries.size();
			for( int i = 0; i < nVisible; i++ ) {
				IAttributeEditor ed =
					(IAttributeEditor)_entries.elementAt( i );
				String name = ed.getName().toLowerCase();
				if ( allAttributes.indexOf( name ) < 0 ) {
					nAttrs++;
				}
			}
			eds = new IAttributeEditor[nAttrs];
			for( int i = 0; i < nVisible; i++ ) {
				eds[i] = (IAttributeEditor)_entries.elementAt( i );
			}
//			Debug.println( "DSEntryPanel.showAllAttributes: copied " +
//						   nVisible + " editors" );
			int nExtra = 0;
			Vector v = getInvisibleAttributes();
			nExtra = v.size();
			for( int i = 0; i < nExtra; i++ ) {
				String name = (String)v.elementAt( i );
//				Debug.println( "Making new editor " + name );
				IAttributeEditor editor = getEditor( name );
				eds[i+nVisible] = editor;
			}
		} else {
			Vector v = new Vector();
			Enumeration en = _entries.elements();
			while ( en.hasMoreElements() ) {
				IAttributeEditor editor = (IAttributeEditor)en.nextElement();
				if ( editor.getValueCount() > 0 )
					v.addElement( editor );
			}
			int nEditors = v.size();
			eds = new IAttributeEditor[nEditors];
			v.copyInto( eds );
		}
		replaceSortedEditors( eds );
		validate();
		repaint();
	}

		/** 
		  * Reads from the server 
		  */

	/**
	 * Return all the attributes which are valid for this objectclass
	 */
    private Vector getAllAttributes() {
		if ( _allAttributes == null ) {
			Enumeration en = _dataModel.getAttributeNames();
			_allAttributes = new Vector();
			while( en.hasMoreElements() ) {
				_allAttributes.addElement( en.nextElement() );
			}
			String[] editableOperationalAttributes = DSSchemaHelper.getEditableOperationalAttributes( _dataModel.getSchema());
			if (editableOperationalAttributes != null) {
				for (int i=0; i<editableOperationalAttributes.length; i++) {
					_allAttributes.addElement(editableOperationalAttributes[i]);
				}
			}
			
			String[] sorted = new String[_allAttributes.size()];	
			_allAttributes.copyInto( sorted );
			sortStrings(sorted);
			for( int i = 0; i < sorted.length; i++ ) {
				_allAttributes.setElementAt( sorted[i], i );
			}
		}
		return _allAttributes;
	}

    /**
	 * Return all attributes which are valid for this objectclass but not
	 * represented (do not have values)
	 */
    private Vector getInvisibleAttributes() {
		getAllAttributes();
		Vector v = new Vector();
		for( int i = 0; i < _allAttributes.size(); i++ ) {
			if ( _visible.get( _allAttributes.elementAt( i ) ) == null ) {
				v.addElement( _allAttributes.elementAt( i ) );
			}
		}
		return v;
	}

	/**
	 * Add in any attributes not already there
	 *
	 * @param h Hashtable with attributes to consider
	 * @param present Hashtable containing attributes already visible
	 * @param entries Vector of visible editors
	 * @param addValue If true, add a default value
	 */
	private void addMissingAttributes( Hashtable h, Hashtable present,
									   Vector entries, boolean addValue ) {
		Enumeration e = h.keys();
		while( e.hasMoreElements() ) {
			String name = ((String)e.nextElement()).toLowerCase();
//			Debug.println( "DSEntryPanel.validateEditors: checking for " +
//						   "attribute " + name );
			if ( present.get(name) == null ) {
				IAttributeEditor ed = getEditor( name );
				if ( addValue ) {
					/* Check if we have to update the _toAdd list */
					boolean isDeleted = false;
					for(int i = 0; i < _toDelete.size(); i++ ) {
						IAttributeEditor deletedEd = (IAttributeEditor)_toDelete.elementAt( i );
						if (name.equals(deletedEd.getName())) {
							isDeleted = true;
							_toDelete.removeElementAt(i);
							break;
						}
					}
					/* Remember to save this added attribute (if it was deleted it means
					   that's already in the entry and so don't need to be added) */		
					if (!isDeleted) { 
						_toAdd.addElement( ed );
					}
					ed.addValue();
				}
				entries.addElement( ed );
//				Debug.println( "DSEntryPanel.validateEditors: adding " +
//							   "editor for attribute " + name );
			}
		}
	}

	

	/**
	 * Filter the editors based on a Vector containing objectclasses
	 *
	 * @param oclasses Vector of objectclasses
	 */
	private void validateEditors( Vector oclasses ) {
		/* Prepare a table of all allowed and one of all required attributes */
		Hashtable allowed = new Hashtable();
		Hashtable required = new Hashtable();
		LDAPSchema schema = _dataModel.getSchema();
		boolean hasExtensibleObject = false;
		Enumeration e = oclasses.elements();		
		while( e.hasMoreElements() ) {
			LDAPObjectClassSchema oschema =
				schema.getObjectClass( (String)e.nextElement() );
			if ( oschema != null ) {		
				DSSchemaHelper.allOptionalAttributes(oschema, schema, allowed);
				DSSchemaHelper.allRequiredAttributes(oschema, schema, required);
				/* Update the checkbox for showing attributes with values: if the entry is an extensible
				   object we don't allow to show all the attributes (only show attributes with values) */
				String name = oschema.getName();
				if (name.equalsIgnoreCase(EXTENSIBLEOBJECT)) {
					hasExtensibleObject = true;
				}
			}			
		}

		String[] editableOperationalAttributes = DSSchemaHelper.getEditableOperationalAttributes( _dataModel.getSchema());
		if (editableOperationalAttributes != null) {
			for (int i=0; i<editableOperationalAttributes.length; i++) {
				allowed.put(editableOperationalAttributes[i], editableOperationalAttributes[i]);
			}
		}

		if (hasExtensibleObject) {			
			_cbAllowedAttributes.setSelected(false);
			_cbAllowedAttributes.setEnabled(false);
			_showingAll = false;
			showAllAttributes( _showingAll );
		} else {
			_cbAllowedAttributes.setEnabled(true);
		}
		/* Make sure all editors correspond to one of the allowed or required
		   attributes */
		Vector entries = new Vector();
		e = _entries.elements();
		Hashtable present = new Hashtable();		
		while( e.hasMoreElements() ) {
			IAttributeEditor ed = (IAttributeEditor)e.nextElement();
			String attr = ed.getName();
			if ( (required.get(attr) != null) ||
				 (allowed.get(attr) != null) ) {
				entries.addElement( ed );
				present.put( ed.getName(), ed.getName() );
//				Debug.println( "DSEntryPanel.validateEditors: copied " +
//							   "editor for " + ed.getName() );
			} else {
				/* The attribute is not allowed or required: have to update _toAdd and _toDelete */
				if (_toAdd.indexOf(ed) >= 0) {
					/* The attribute is not in the entry: don't need to remove it */
					_toAdd.removeElement( ed );
				} else {					
					_toDelete.addElement( ed );
				}				
			}
		}

		/* Add in any required attributes not already there */
		addMissingAttributes( required, present, entries, true );
		/* Add in any optional attributes not already there, if showing
		   all attributes */
		if ( _showingAll ) {
			addMissingAttributes( allowed, present, entries, false );
		}

		IAttributeEditor[] eds = new IAttributeEditor[entries.size()];
		entries.copyInto( eds );
		replaceSortedEditors( eds );
		validate();
	}

    private void sortStrings( String[] s ) {
		DSUtil.bubbleSort( s, true );
	}

    private void sortEditors( IAttributeEditor[] attrs ) {
		Collator collator = Collator.getInstance(); 
		int nAttrs = attrs.length;
		for ( int i = 0; i < nAttrs - 1; i++ ) {
			for ( int j = i + 1; j < nAttrs; j++ ) {
				if ( collator.compare( attrs[i].getLabelText(),
									   attrs[j].getLabelText()) > 0 ) {
					IAttributeEditor a = attrs[i];
					attrs[i] = attrs[j];
					attrs[j] = a;
				}
			}
		}
	}

    private void removeAllEditors() {
		int nFields = _entries.size();
		for( int i = 0; i < nFields; i++ ) {
			_page.remove( (JComponent)_entries.elementAt( i ) );
		}
		_entries.removeAllElements();
		_visible.clear();	
		_page.remove(_glue);	
	}
    
    private String parseEffRightsString(String str)
    {
        String tTip = new String();
        if (str != null) {
            for(int i=0;i<str.length();i++)
            {
               switch(str.charAt(i)){
                   case 'a':
                       tTip = tTip + ", add";
                   break;
                   case 'd':
                       tTip = tTip + ", delete";
                   break;
                   case 'n':
                       tTip = tTip + ", rename DN";
                   break;
                   case 'v':
                       tTip = tTip + ", view entry";
                   break;
                   case 'r':
                       tTip = tTip + ", read";
                   break;
                   case 's':
                       tTip = tTip + ", search";
                   break;
                   case 'c':
                       tTip = tTip + ", compare";
                   break;
                   case 'w':
                       tTip = tTip + ", modify (add)";
                   break;
                   case 'o':
                       tTip = tTip + ", obliterate (delete)";
                   break;
                   case 'W':
                       tTip = tTip + ", self add";
                   break;
                   case 'O':
                       tTip = tTip + ", self delete";
                   break;

               }
            }
        }
        // Remove the leading ', ' if present.
        if (tTip.length() > 2) 
        {
            return ": " + tTip.substring(2);
        } else 
        {
            return tTip;
        }
    }

    private void addRemoveEffRightsLabel(IAttributeEditor ed)
    {
        String lText = ed.getLabelText();
        if(_showingEffRights &&(!(lText.indexOf('(')==0)))
        {
            if(_allAttrRights == null || _allAttrRights.equals(""))
            {
                String rights = (String) _attrLevelEffRights.get(ed.getName());
                if (rights != null && !rights.equals(""))
                {
                    ed.setLabelText("(" + rights + ") " + lText);
                    ((AttributeEditor)ed).setLabelToolTip(ed.getName() + "\n" + parseEffRightsString(rights));
                }
            }
            else
            {
                ed.setLabelText("(" + _allAttrRights + ") " + lText);
                ((AttributeEditor)ed).setLabelToolTip(ed.getName() + "\n" + parseEffRightsString(_allAttrRights));
            }
        }
        else if(!_showingEffRights &&(lText.indexOf('(')==0) )
        {
            ed.setLabelText(lText.substring(lText.indexOf(')') + 2));
            ((AttributeEditor)ed).setLabelToolTip(ed.getName());
        }
    }
    
    private void replaceSortedEditors( IAttributeEditor[] eds ) {
		int nAttrs = eds.length;
		sortEditors( eds );
//		Debug.println( "Sorted " + nAttrs + " editors" );
		/* Replace all editors with the sorted list */
		removeAllEditors();	
//		Debug.println( "Removed all editors" );
		for( int i = 0; i < nAttrs; i++ ) {
                        addRemoveEffRightsLabel(eds[i]);
			setVisible( eds[i] );
//			Debug.println( "Adding editor " + eds[i].getName() );
			_page.add( (JComponent)eds[i], _cons );
			_entries.addElement( eds[i] );
		}
		_cons.weighty = 1.0;
		_cons.fill = _cons.BOTH;
		_page.add(_glue, _cons);		
		_cons.weighty = 0.0;
		_cons.fill = _cons.HORIZONTAL;

		validate();	
	}

    private void setVisible( IAttributeEditor ed ) {
		String name = ed.getName().toLowerCase();
		_visible.put( name, ed );
	}

    private Object getVisible( String name ) {
		return _visible.get( name.toLowerCase() );
	}

	private IAttributeEditor getSelectedEditor() {
		IAttributeEditor iae = null;
		for (Enumeration e = _entries.elements(); iae == null &&
				 e.hasMoreElements();) {
			Object o = e.nextElement();
			if (o instanceof IAttributeEditor &&
				((IAttributeEditor)o).isSelected()) {
				iae = (IAttributeEditor)o;
			}
		}

		return iae;
	}


    protected void setDNVisible(boolean state) {
	    _dnLabel.setVisible(state);
        validate();		
    }
	
	/**
	 * Method called by the StringAttributeEditor when a
	 * value of the naming attribute has been modified and by DSEntryPanel
	 * when the value used as naming value has been deleted.
	 */
	public void updateNamingValue(String attribute) {
		if (_options != NOSHOWINGDN) {
			IAttributeEditor ed = (IAttributeEditor)_visible.get(attribute);		
			if (ed != null) {
				Integer integer = (Integer)(_namingAttributeIndexes.get(attribute));
				int namingAttributeIndex = 0;
				if (integer != null) {
					namingAttributeIndex = integer.intValue();
				}
				Vector values = ed.getValues();
				String namingValue = (String)values.elementAt(namingAttributeIndex);

				String[] namingAttributes = getNamingAttributes();				

				boolean found = false;
				boolean updated = false;
				for (int i=0; (i < namingAttributes.length) & !found; i++) {
					if (namingAttributes[i].equals(attribute)) {
						found = true;
						if (!_namingValues[i].equals(namingValue)) {
							_namingValues[i] = namingValue;							
							updated = true;
						}
					}
				}				
				if (updated) {
					/* Update the DN label */
					updateDNValue(getNamingAttributes(), getNamingValues());
				}
			}
		}
	}

    public void updateDNValue(String[] attributes, String[] values) {				
        for (int i=0; i < FORBIDDEN_NAMING_ATTRIBUTES.length; i++) {
		    for (int j=0; j<attributes.length; j++) {
			    if (attributes[j].equals(FORBIDDEN_NAMING_ATTRIBUTES[i])) {
				    DSUtil.showErrorDialog(getOwnerFrame(), "namingattribute-forbiddenattribute", attributes[j] );					                 
					return;
                }
            }			
		} 	
		
		String stringRDN = "";
		for (int i=0; i < attributes.length ; i++) {
		    if (i>0) {
			stringRDN = stringRDN + " + " +attributes[i]+"="+DSUtil.escapeDNVal(values[i]);
		    } else {
			stringRDN = attributes[i]+"="+DSUtil.escapeDNVal(values[i]);
		    }
		}
		Debug.println("DSEntryPanel.updateDNValue: rdn = "+stringRDN);				
				  
		RDN rdn = new RDN(stringRDN);
		DN parentDN = _dn.getParent();
		
		/* See if it is an object in the root */
		if (!parentDN.toRFCString().trim().equals("")) {
			parentDN.addRDN(rdn);
		} else {
			parentDN = new DN(stringRDN);
		}
		_dn = parentDN;
		String dn = stringRDN+","+_dn.getParent();
		Debug.println("DSEntryPanel.updateDNValue: dn = "+dn);
		if (dn != null) {
			_dnLabel.setText("dn: "+dn);
		}
		_namingAttributes = attributes;
		_namingValues = values;
		
		if (_lNamingAttribute != null) {
			String attributeString = attributes[0];
			for (int i=1; i< attributes.length; i++) {
				attributeString = attributeString +", " + attributes[i];
			}			
			attributeString = DSUtil.abreviateString(attributeString, 15);
			Debug.println("DSEntryPanel.updateDNValue: naming attributes = "+attributeString);
			_lNamingAttribute.setText(_resource.getString(_section, "namingattribute")+ " "+ attributeString);
		}
		
		
		validate();
		repaint();
	}						     		   

	public boolean isValidDN() {
               boolean isValid = false;
               if (getDN().trim().equals("")) {
                       return true;
               }
               String[] rdns = LDAPDN.explodeDN( getDN(), false );
               if ((rdns == null) ||
                       (rdns[0] == null) ||
                       !DN.isDN(getDN())) {
                       Debug.println("DSEntryPanel.isValidDN():  the rdn is not a good RDN");
                       DSUtil.showErrorDialog(getOwnerFrame(), "namingattribute-syntaxerror", "" );
                       return false;
               }

               RDN rdn = new RDN(rdns[0]);
               String[] values = rdn.getValues();
               String[] types = rdn.getTypes();

               LDAPAttributeSet attrs = getAttributes();

               /* We see if for each type of the rdn we have at least one value*/
               for (int i=0; i<types.length; i++) {
                       isValid = true;
                       LDAPAttribute attr = attrs.getAttribute(types[i]);
                       if ( attr != null ) {
                               boolean typeWithValue = false;
                               String value = values[i];
                               Enumeration en = attr.getStringValues();
                               if (en != null) {
                                       while (en.hasMoreElements()) {
                                               String currentValue = (String)en.nextElement();
                                               /* Just in case we have quotes... */                                               
                                               if (value.equals(currentValue) ||
                                                   DSUtil.escapeDNVal(value).equals(currentValue)) {
                                                       typeWithValue = true;
                                                       break;
                                               }
                                               // note: current versions of the directory server
                                               // use dn values as values of RDNs e.g.
                                               // cn=dc\3Dexample\2Cdc\3Dcom,cn=mapping tree, cn=config
                                               // where dc\3Dexample\2Cdc\3Dcom is dc=example,dc=com
                                               String unescval = DSUtil.unEscapeRDNVal(value);
                                               if (DSUtil.isValidDN(unescval)) {
                                            	   // compare as DNs
                                            	   if (DSUtil.equalDNs(unescval, currentValue)) {
                                            		   typeWithValue = true;
                                            		   break;
                                            	   }
                                               }
                                       }
                                       if (typeWithValue) {
                                               continue;
                                       } else {
                                               isValid = false;
                                       }
                               } else {
                                       isValid = false;
                               }
                       } else {
                               isValid = false;
                       }
                       if (!isValid) {
                               break;
                       }
               }

               if (!isValid) {
                       DSUtil.showErrorDialog(getOwnerFrame(), "namingattribute-unknowerror", "" );
               }

               return isValid;
       }

	public String[] getNamingAttributes() {		
		return _namingAttributes;		
	}

	public String[] getNamingValues() {
		return _namingValues;
	}

	public String[] getNamingAttributesFor(String dn) {
		if (dn == null) {
			return null;
		}
		DN theDN = new DN(dn);
		String rdn = theDN.explodeDN(false)[0];
		RDN theRDN = new RDN(rdn);
		return theRDN.getTypes();	
	}

	public String[] getNamingValuesFor(String dn) {
		if (dn == null) {
			return null;
		}
		DN theDN = new DN(dn);
		String rdn = theDN.explodeDN(false)[0];
		RDN theRDN = new RDN(rdn);
		return theRDN.getValues();	
	}


	public String getDN() {
		return _dn.toRFCString();
	}

	public int getOptions() {
		return _options;
	}

	/**
	  * Initializes the Label displaying the dn AND the arrays that contain the naming
	  * attributes and the naming values.
	  */
	public void createDNLabel() {
		_dn = new DN(_dataModel.getTitle());	
		if (_dataModel.getTitle() != null) {			
			_dnLabel = new JLabel("dn: "+_dataModel.getTitle());						

			_saveNamingAttributes = getNamingAttributesFor(_dataModel.getTitle());
			_namingAttributes = new String[_saveNamingAttributes.length];
			for (int i=0; i<_saveNamingAttributes.length; i++) {
				_namingAttributes[i] = _saveNamingAttributes[i];
			}

			_saveNamingValues = getNamingValuesFor(_dataModel.getTitle());
			_namingValues = new String[_saveNamingValues.length];
			for (int i=0; i<_saveNamingValues.length; i++) {
				_namingValues[i] = _saveNamingValues[i];
			}
		} else {
			_dnLabel= new JLabel("");
			_options = NOSHOWINGDN;
		}			

		if (_saveNamingAttributes == null) {
			Debug.println("DSEntryPanel.createDNLabel(): could not create the defaultNamingAttribute");
		}		
	}
	
	public JPanel createActionPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		
		JPanel viewPanel = new GroupPanel(_resource.getString(_section, "view"), true);	   	
		JPanel editPanel = new GroupPanel(_resource.getString(_section, "edit"), true);
	
	
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.fill = gbc.BOTH;
		gbc.weighty = 0;
		gbc.weightx = 0;
		gbc.gridwidth = gbc.REMAINDER;
		int component = UIFactory.getComponentSpace();
		gbc.insets = new Insets(0,component,0,component);

		gbc.gridwidth = gbc.REMAINDER;
		panel.add(viewPanel, gbc);	

		gbc.gridwidth = gbc.REMAINDER;
		panel.add(editPanel, gbc);	   		

        if (_options != NOSHOWINGDN) {
		    gbc.anchor = gbc.NORTHWEST;
            JPanel namingAttributePanel = createNamingAttributePanel();
			panel.add(namingAttributePanel, gbc);	
		}	

		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.fill= gbc.VERTICAL;
		gbc.weighty = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		panel.add(Box.createGlue(), gbc);		

		_rbAttributeName = UIFactory.makeJRadioButton(this, _section, "rbattributename", false, _resource);
		_rbAttributeName.setActionCommand(SHOW_NAMES);
		_rbAttributeName.setSelected(_showingNames);

		_rbAttributeDescription = UIFactory.makeJRadioButton(this, _section, "rbattributedescription", true, _resource);	
		_rbAttributeDescription.setActionCommand(SHOW_DESCRIPTIONS);	
		_rbAttributeDescription.setSelected(!_showingNames);

		ButtonGroup nameGroup = new ButtonGroup();

		nameGroup.add(_rbAttributeName);
		nameGroup.add(_rbAttributeDescription);

		_cbAllowedAttributes = UIFactory.makeJCheckBox(this, _section, "cballowedattributes", true, _resource);	
		_cbAllowedAttributes.setActionCommand(SHOW_ALL_ATTRIBUTES);	
		_cbAllowedAttributes.setSelected(false);

		/* Enable/Disable the checkbox for showing attributes with values: if the entry is an extensible
		   object we don't allow to show all the values */
		IAttributeEditor ed = (IAttributeEditor)_visible.get(OBJECTCLASS);
		if (ed != null) {
		    Vector v = ed.getValues();
		    if (v != null) {
		        for (int i=0; i< v.size(); i++) {
		            if (((String)v.elementAt(i)).equalsIgnoreCase(EXTENSIBLEOBJECT)) {
		                _cbAllowedAttributes.setEnabled(false);
		                break;
		            }
		        }
		    }
		}
		
		_cbShowDN = UIFactory.makeJCheckBox(this, _section, "cbshowdn", _dnLabel.isVisible(), _resource);
		_cbShowDN.setActionCommand(SHOW_DN);		
		_cbShowDN.setSelected(_dnLabel.isVisible());
                
                _cbShowEffRights = UIFactory.makeJCheckBox(this, _section, "cbshoweffrights", true, _resource);
                _cbShowEffRights.setActionCommand(SHOW_EFF_RIGHTS);
                _cbShowEffRights.setSelected(false);
	
		_bRefresh = UIFactory.makeJButton(this, _section, "brefresh", _resource);

		_bRefresh.setActionCommand(REFRESH);

		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.NORTHWEST;		
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;
		gbc.insets = new Insets(0, component , 0, component);
		viewPanel.add(_rbAttributeName, gbc);
		gbc.gridwidth = gbc.REMAINDER;
		viewPanel.add(_rbAttributeDescription, gbc);
		gbc.gridwidth = gbc.REMAINDER;
		viewPanel.add(Box.createVerticalStrut(10), gbc);
		gbc.gridwidth = gbc.REMAINDER;
		viewPanel.add(_cbAllowedAttributes, gbc);
		if (_options != NOSHOWINGDN) {
			viewPanel.add(_cbShowDN, gbc);
		}
                gbc.gridwidth = gbc.REMAINDER;
		viewPanel.add(_cbShowEffRights, gbc);

		gbc.gridwidth = gbc.REMAINDER;
		viewPanel.add(Box.createVerticalStrut(10), gbc);


		/* We only add the Refresh button if the entry exists */
		boolean entryExists = false;		
		try {
			DSUtil.readEntry(_ldc,
							 getDN(),
							 null,
							 (LDAPSearchConstraints)_ldc.getSearchConstraints().clone());
			entryExists = true;
		} catch (LDAPException e) {			
			if (e.getLDAPResultCode() != LDAPException.NO_SUCH_OBJECT) {
				Debug.println("DSEntryPanel.createActionPanel "+e);
			}
		}
		if (entryExists) {
			gbc.gridwidth = gbc.REMAINDER;
			gbc.insets = new Insets(0, 2*component , component, 2*component);
			gbc.weightx = 1.0;
			
			viewPanel.add(_bRefresh, gbc);
		}

		_bAddValue = UIFactory.makeJButton(this, _section, "baddvalue", _resource);    
		_bAddValue.setActionCommand(ADD_VALUE);
	
		_bDeleteValue = UIFactory.makeJButton(this, _section,"bdeletevalue", _resource);
		_bDeleteValue.setActionCommand(DELETE_VALUE);	
	
		_bAddAttribute = UIFactory.makeJButton(this, _section,"baddattribute", _resource);	
		_bAddAttribute.setActionCommand(ADD_ATTRIBUTE);
	
		_bDeleteAttribute = UIFactory.makeJButton(this, _section,"bdeleteattribute", _resource);	
		_bDeleteAttribute.setActionCommand(DELETE_ATTRIBUTE);	
			
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = new Insets(0, 2*component , component, 2*component);
		gbc.weightx = 1.0;

		editPanel.add(_bAddValue, gbc);	
 		editPanel.add(_bDeleteValue, gbc);	
		editPanel.add(_bAddAttribute, gbc);	
		editPanel.add(_bDeleteAttribute, gbc);	

		updateButtons();

		return panel;
	}

	private JPanel createNamingAttributePanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		panel.setBorder(new EmptyBorder(SuiConstants.COMPONENT_SPACE, 
										SuiConstants.DIFFERENT_COMPONENT_SPACE,
										SuiConstants.COMPONENT_SPACE,
										SuiConstants.DIFFERENT_COMPONENT_SPACE));

		_namingValues = getNamingValuesFor(_dataModel.getTitle());

		GridBagConstraints gbc = new GridBagConstraints();
		
		String[] attributes = getNamingAttributes();
		if (attributes != null) {
			String attributeString = attributes[0];
			for (int i=1; i< attributes.length; i++) {
				attributeString = attributeString +", " + attributes[i];
			}			
			attributeString = DSUtil.abreviateString(attributeString, 15);
			_lNamingAttribute = new JLabel(_resource.getString(_section, "namingattribute")+ " "+ attributeString);
			gbc.gridwidth = (_options == SHOWINGDN_NAMING) ?  gbc.RELATIVE: gbc.REMAINDER;
			gbc.weightx = 0.0;
			gbc.fill = gbc.NONE;
			gbc.anchor = gbc.NORTHWEST;
			panel.add(_lNamingAttribute, gbc);		
			gbc.anchor = gbc.NORTHEAST;
			gbc.insets.left = SuiConstants.COMPONENT_SPACE;
			if (_options == SHOWINGDN_NAMING) {
				_bChangeNamingAttribute =  UIFactory.makeJButton(this, _section, "bchangenamingattribute", _resource);
				_bChangeNamingAttribute.setActionCommand(CHANGE_NAMING_ATTRIBUTE);
				_lNamingAttribute.setLabelFor(_bChangeNamingAttribute);
				panel.add(_bChangeNamingAttribute, gbc);
			} else {
				_lNamingAttribute.setLabelFor(panel);
			}
		}
	
		return panel;
	}	
	

		/**
		  * Called when an objectclass is deleted: one of the attributes of the deleted objectclass
		  * might be a naming attribute.  Assumes that the editors in _visible are properly updated.
		  *
		  * @returns true if the naming attributes have been modified. False otherwise.
		  */
	public boolean updateNamingAttributes() {		
		boolean namingAttributesModified = false;

		String[] oldNamingAttributes = getNamingAttributes();
		String[] oldNamingValues = getNamingValues();
		
		Vector newNamingAttributes = new Vector(); /* This vector contains the naming attributes that
													  we got before deleting the objectclass and that are
													  in the list of candidates to be naming attribute */
		Vector newNamingValues = new Vector();
		for (int i=0; i< oldNamingAttributes.length; i++) {
			if (_visible.get(oldNamingAttributes[i]) != null) {
				newNamingAttributes.addElement(oldNamingAttributes[i]);
				newNamingValues.addElement(oldNamingValues[i]);
			}		
		}		   		
	
		/* We have to modify the naming attribute: some of the current attributes 
		   do not match the objectclass. */
		if (newNamingAttributes.size() != oldNamingAttributes.length) {
			namingAttributesModified = true;
			/* There are some attribute left... best effort */
			if (newNamingAttributes.size() > 0) {
				_namingAttributes = new String[newNamingAttributes.size()];
				newNamingAttributes.toArray(_namingAttributes);
				_namingValues = new String[newNamingValues.size()];
				newNamingValues.toArray(_namingValues);
			} else {
				/* Look for the first attribute and use it as naming attribute */
				boolean found = false;
				for (int i=0; (i<_entries.size()) && !found; i++) {
					IAttributeEditor ed = (IAttributeEditor) _entries.elementAt(i);
					if (ed instanceof StringAttributeEditor) {
						String attributeName = ed.getName();
						boolean isForbidden = false;
						for (int j=0; (j < FORBIDDEN_NAMING_ATTRIBUTES.length) && !isForbidden; j++) {
							if (attributeName.equals(FORBIDDEN_NAMING_ATTRIBUTES[j])) {
								isForbidden = true;						
							}
						}
						if (!isForbidden) {
							found = true;
							_namingAttributes = new String[1];
							_namingAttributes[0] = attributeName;
							_namingValues = new String[1];

							Vector values = ed.getValues();
							if ((values != null) &&
								(values.size() > 0)) {
								_namingValues[0] = (String)values.elementAt(0);
							} else {
								_namingValues[0] = "";
							}
						}
					}	
				}
			}
		}

		if (namingAttributesModified) {
			/* Update the DN label */
			updateDNValue(getNamingAttributes(), getNamingValues());
		}
		
		return namingAttributesModified;
	}


	class Refresher implements Runnable {
		public Refresher() {
		}
		public void run() {		
			remove(_centerPane);
			final JLabel waitingLabel = new JLabel(_resource.getString(_section, "refresh-label"));
			
			SwingUtilities.invokeLater( new Runnable() {
				public void run() {
					_bRefresh.setEnabled(false);
					if (_bChangeNamingAttribute != null) {
						_bChangeNamingAttribute.setEnabled(false);
					}
					_cbShowDN.setEnabled(false);
					_cbAllowedAttributes.setEnabled(false);
                                        _cbShowEffRights.setEnabled(false);
					_rbAttributeName.setEnabled(false);
					_rbAttributeDescription.setEnabled(false);

					_bAddValue.setEnabled(false);
					_bDeleteValue.setEnabled(false);
					_bDeleteAttribute.setEnabled(false);
					_bAddAttribute.setEnabled(false);
					
					add("Center", waitingLabel);
					validate();	
					repaint();
				}
			});
			String dn = _dataModel.getTitle();
			LDAPEntry entry = null;			
			try {
				/* Fetch all attributes of the entry */
				String[] editableAttributes = DSSchemaHelper.getOperationalAttributes(_dataModel.getSchema());				
				String[] allAttributes = null;
				if (editableAttributes == null) {
					allAttributes = new String[1];
					allAttributes[0] = "*";
				} else {
					allAttributes = new String[editableAttributes.length + 1];
					for (int i=0; i < editableAttributes.length; i++) {
						allAttributes[i] = editableAttributes[i];
					}
					allAttributes[editableAttributes.length] = "*";
                            }
				entry = DSUtil.readEntry(_ldc,
										 dn,
										 allAttributes,
										 (LDAPSearchConstraints)_ldc.getSearchConstraints().clone());
			} catch (LDAPException e) {
				Debug.println("Refresher.run() "+e);
				/* WE didn't find the entry we were editing...  someone has deleted it */
				if (e.getLDAPResultCode() == LDAPException.NO_SUCH_OBJECT) {
					DSUtil.showErrorDialog(getOwnerFrame(), "editingentrywasdeleted", "" );
					getOwnerWindow().dispose();
					getOwnerWindow().hide();
				}
			}

			if (entry != null) {
				LDAPAttributeSet set = entry.getAttributeSet();
				int i = 0;			
				Hashtable allAttrs = DSUtil.getAllAttributeList( _dataModel.getSchema(),
																 entry );
				Enumeration attrSet = set.getAttributes();
				Vector v = new Vector();
				while( attrSet.hasMoreElements() ) {
					LDAPAttribute attr = (LDAPAttribute)attrSet.nextElement();
					v.addElement( attr.getName().toLowerCase() );
				}
				allAttrs = null;
				
				String[] names = new String[v.size()];
				String[] labels = new String[v.size()];
				for( i = 0; i < names.length; i++ )
					names[i] = (String)v.elementAt( i );
				
				/* Look up a friendly presentation of each attribute */			
				AttributeAlias.getAliases( names, labels );
				
				/* Make a single page with all attributes */
				EntryPageDescription page =
					new EntryPageDescription( names, labels );
				/* Data model for property editor */
				_dataModel = new DSPropertyModel( _dataModel.getSchema(),
												  entry,
												  page );			
			} 


			SwingUtilities.invokeLater( new Runnable() {
				public void run() {
					/* Keep the selected preferences of the user */
					boolean isAttributeNameSelected = _rbAttributeName.isSelected();
					boolean isAllAttributesSelected = _cbAllowedAttributes.isSelected();
                                        boolean isShowEffRightsSelected = _cbShowEffRights.isSelected();
					boolean isShowDNSelected = _cbShowDN.isSelected();
					
					_entries = new Vector();
					_visible = new Hashtable();
					_toDelete = new Vector();
					_toAdd = new Vector();
					
					_namingAttributeIndexes.clear();			
									
					createDNLabel();		
					_centerPane = initialize();					
					
					_rbAttributeName.setSelected(isAttributeNameSelected);
					_rbAttributeDescription.setSelected(!isAttributeNameSelected);
					_showingNames = isAttributeNameSelected;
					Enumeration en = _entries.elements();
					while( en.hasMoreElements() ) {
						IAttributeEditor editor =
							(IAttributeEditor)en.nextElement();
						if (isAttributeNameSelected) {
							editor.setLabelText( editor.getName() );
						} else {
							editor.setLabelText(AttributeAlias.getAlias( editor.getName() ) );
						}
					}		
					
					_cbAllowedAttributes.setSelected(isAllAttributesSelected);
					_showingAll = isAllAttributesSelected;
					showAllAttributes( _showingAll );
                                        
                                        _cbShowEffRights.setSelected(isShowEffRightsSelected);
                                        _showingEffRights = isShowEffRightsSelected;
					
					_cbShowDN.setSelected(isShowDNSelected);
					_dnLabel.setVisible(isShowDNSelected);
					
					updateButtons();
	
					_bRefresh.setEnabled(true);	
					if (_bChangeNamingAttribute != null) {
						_bChangeNamingAttribute.setEnabled(true);
					}					
					_cbShowDN.setEnabled(true);
					_cbAllowedAttributes.setEnabled(true);
                                        _cbShowEffRights.setEnabled(true);
					_rbAttributeName.setEnabled(true);
					_rbAttributeDescription.setEnabled(true);
					
					remove(waitingLabel);		
					add( "Center", _centerPane ); 							
					validate(); 					
					repaint();			 					
				}			
			});
		}		
	}		

	private void actionChangeNamingAttribute() {
		if (_renameDialog == null) {
			_renameDialog = new RenameDialog(getOwnerFrame());
		}
		Hashtable values = getValuesForNaming();
		
		_renameDialog.display(getNamingAttributes(), 
							  getNamingValues(),
							  values);
		
		if (!_renameDialog.isCancelled() &&
			_renameDialog.isModified()) {			
			updateDNValue(_renameDialog.getNamingAttributes(),
						  _renameDialog.getNamingValues());
			updateNamingIndexes();
		}
	}

	/**
	  * Updates the hashtable with the indexes of the naming attributes.	  
	  */
	private void updateNamingIndexes() {
		_namingAttributeIndexes.clear();
		String[] namingAttributes = getNamingAttributes();
		String[] namingValues = getNamingValues();
		for (int i = 0; i<namingAttributes.length; i++) {
			int index = 0;
			IAttributeEditor ed = (IAttributeEditor)_visible.get(namingAttributes[i]);		
			if (ed != null) {
				Vector values = ed.getValues();						
				index = values.indexOf(namingValues[i]);				
			}
			if (index < 0) {
				index = 0;
			}
			_namingAttributeIndexes.put(namingAttributes[i], new Integer(index));
		}
	}
		
	/**
	 * Creates a hashtable with the values of the attributes in the panel.
	 * The values of the FORBIDDEN_NAMING_ATTRIBUTES ARE NOT INCLUDED.
	 */
	private Hashtable getValuesForNaming() {
		Hashtable values = new Hashtable();
		for( int i = 0; i < _entries.size(); i++ ) {
			IAttributeEditor ed = (IAttributeEditor)_entries.elementAt( i );
			if (ed instanceof StringAttributeEditor) {
				String attributeName = ed.getName();
				boolean isForbidden = false;
				for (int j=0; (j < FORBIDDEN_NAMING_ATTRIBUTES.length) && !isForbidden; j++) {
					if (attributeName.equals(FORBIDDEN_NAMING_ATTRIBUTES[j])) {
						isForbidden = true;						
					}
				}
				if (!isForbidden) {
					values.put(attributeName, ed.getValues());
				}				
			}
		}
		return values;
	}

	private RenameDialog _renameDialog;
        private Hashtable _attrLevelEffRights = new Hashtable();

	private Hashtable _namingAttributeIndexes = new Hashtable();  /* In this hashtable we store the position of the
																	 value for one type of the cn.																		 
																	 For example if we have																		 
																	 dn: cn=James + sn=Henry																		 
																	 cn: James
																	 cn: Jimmy																		
																	 sn: Henry																		 
																	 this hashtable contains as keys "cn" and "sn" and as elements it will have two																		 
																	 Integers indicating the index of the value that is in the rdn (sn and cn may be multivalued) */
		

	private JRadioButton _rbAttributeName;
	private JRadioButton _rbAttributeDescription;
        private JCheckBox _cbAllowedAttributes;
        private JCheckBox _cbShowEffRights;
	private JCheckBox _cbShowDN;
	private JButton _bRefresh;
	private JButton _bAddValue;
	private JButton _bDeleteValue;
	private JButton _bAddAttribute;
	private JButton _bDeleteAttribute;
	private JButton _bChangeNamingAttribute;	
	private Component _glue;

	static final String DELETE_ATTRIBUTE = "delete attribute";
	static final String DELETE_VALUE = "delete value";
	static final String ADD_ATTRIBUTE = "add attribute";
	static final String ADD_VALUE = "add value";
	static final String SHOW_ALL_ATTRIBUTES = "show all attributes";
        static final String SHOW_EFF_RIGHTS = "show effective rights";
	static final String SHOW_NAMES = "show attribute names";
	static final String SHOW_DESCRIPTIONS = "show attribute descriptions";
	static final String SHOW_DN = "show dn";
	static final String REFRESH = "refresh";
	static final String CHANGE_NAMING_ATTRIBUTE = "change naming attribute";	
		
	private JLabel _dnLabel;
        private JLabel _entRights;
	private	JLabel _lNamingAttribute;
	private String[] _namingAttributes;         /* The naming attributes (types of the rdn) chosen by the user */
	private String[] _saveNamingValues;      /* The naming values (values of the rdn) that we get from the original dn we have when initializing 
												DSEntryPanel */	
	private String[] _saveNamingAttributes;      /* The naming attributes (types of the rdn) that we get from the original dn we have when initializing 
													DSEntryPanel */
	private String[] _namingValues;                 /* The naming values (values of the rdn) of the entry */
	
	private DN _dn;
	private int _options;
	public static final int SHOWINGDN_NONAMING = 0;
	public static final int SHOWINGDN_NAMING = 1;
	public static final int NOSHOWINGDN = 2;

	private String[] FORBIDDEN_NAMING_ATTRIBUTES;
			
	final String OBJECTCLASS = "objectclass";
	final String EXTENSIBLEOBJECT = "extensibleobject";
        private String _entLevelRights = null;
        private String _allAttrRights = null;
	private Vector _entries = new Vector();
	private Vector _allAttributes = null;
	private Hashtable _visible = new Hashtable();
	private DSPropertyModel	_dataModel;	 // attribute data model
	private LDAPConnection _ldc;
	private Vector _toDelete = new Vector();
	private Vector _toAdd = new Vector();
	private JPanel _page = null;
	private JComponent _centerPane = null;
	private GridBagConstraints _cons = new GridBagConstraints();
	private boolean _showingAll = false;
	private boolean _showingNames = false;
        private boolean _showingEffRights = false;
	private static final int buttonWidth = 85;
	private static final int unitHeight = 23;
	private static final int buttonHeight = unitHeight;
	private static final int fieldWidth = 250;
	private static final int fieldHeight = unitHeight;
	private static final int labelHeight = unitHeight;
	private static final int labelWidth = 180;

		private static final int margin = 0;

	   private static final String _helpToken = "property-main-help";
	static ResourceSet _resource =
	      new ResourceSet("com.netscape.admin.dirserv.propedit.propedit");
	static final String _section = "DSEntryPanel";	
}





