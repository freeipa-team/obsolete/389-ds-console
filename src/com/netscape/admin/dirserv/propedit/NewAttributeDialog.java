/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.propedit;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import netscape.ldap.LDAPSchema;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DefaultResourceModel;


public class NewAttributeDialog extends AbstractDialog
                                implements ListSelectionListener,
								           MouseListener {

	public NewAttributeDialog(Frame frame, ConsoleInfo serverInfo) {
		super(
			frame, 
//			_resource.getString(_section, "title"), 
			"bof",
			true,
			OK|CANCEL|HELP);
			
		// Build and sort a vector containing all the attributes of the schema
		Vector allAttrNameVector = new Vector(100);
		LDAPSchema schema = DSUtil.getSchema(serverInfo);
		Enumeration e = schema.getAttributeNames();
		while (e.hasMoreElements()) {
			allAttrNameVector.addElement((String)e.nextElement());
		}
		// Sort the vector using an intermediate array
		String[] a = new String[allAttrNameVector.size()];
		allAttrNameVector.copyInto(a);
		DSUtil.bubbleSort(a, true);
		allAttrNameVector.removeAllElements();
		for (int i = 0; i < a.length; i++) {
			allAttrNameVector.addElement(a[i]);
		}

		// Create an NewAttributePanel.
		// The used table is passed an empty table: it will be 
		// initialized afterward.
		_attributePanel = new NewAttributePanel(new DefaultResourceModel(),
		                                     allAttrNameVector, new Hashtable());
		_attributePanel.init();
		JList list = _attributePanel.getList();
		list.addListSelectionListener(this);
		list.addMouseListener(this);
		
		// Add the NewAttributePanel to the current dialog
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		contentPane.add("Center", _attributePanel);
		pack();
	}

	public void setUsed(Hashtable used) {
		_attributePanel.setUsed(used);
	}
	
	public void setSingleSelection(boolean on) {
		ListSelectionModel m = _attributePanel.getList().getSelectionModel();
		m.setSelectionMode(on ? m.SINGLE_SELECTION : m.MULTIPLE_INTERVAL_SELECTION);
	}
	
	public String[] getSelectedAttributes() {
		return _attributePanel.getSelectedAttributes();
	}

	
	/**
	 * Relay ok action on _attributePanel.
	 */
	public void okInvoked() {
		_attributePanel.okCallback();
	}
	

	
	/**
	 * Relay cancel action on _attributePanel.
	 */
	public void cancelInvoked() {
		_attributePanel.resetCallback();
	}
	

	
	/**
	 * Relay help action on _attributePanel.
	 */
	public void helpInvoked() {
		_attributePanel.helpCallback();
	}
	
	/**
	 * Maintain the state of the ok button.
	 */
	public void valueChanged(ListSelectionEvent e) {
		if ( ! e.getValueIsAdjusting() ) {
			JList list = _attributePanel.getList();
			int min = list.getMinSelectionIndex();
			setOKButtonEnabled(min != -1);
		}
	}
	
	
	/**
	 * Implements MouseListener.
	 */
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() >= 2) {
			okInvoked();
			setVisible(false);
		}
	}
	
	
	public void mouseEntered(MouseEvent e) {}
	
	public void mouseExited(MouseEvent e) {}
	
	public void mousePressed(MouseEvent e) {}
	
	public void mouseReleased(MouseEvent e) {}
	
	
	
	private NewAttributePanel _attributePanel;
    private final static String _section = "newattribute";
	private final static ResourceSet _resource = DSEntryPanel._resource;
};
