/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.propedit;

/**
 * EntryPageDescription
 * 
 *
 * @version 1.0
 * @author rweltman
 **/
public class EntryPageDescription {

	public EntryPageDescription( String[] attrNames ) {
		_attrNames = attrNames;
	}

	public EntryPageDescription( String[] attrNames, String[] attrLabels ) {
		_attrNames = attrNames;
		_attrLabels = attrLabels;
	}

    /**
     * Return the number of properties on a particular page.
     *
     * @return The number of properties on a particular page.
     **/
    public int getItemCount() {
		return _attrNames.length;
	}

    /**
     * Return the name of a property on a particular page.
     *
     * @param itemIndex Which index (from 0) on the page to report on.
     * @return The name of a property on a particular page.
     **/
     public String getItemName( int itemIndex ) {
		try {
			return _attrNames[itemIndex];
		} catch ( ArrayIndexOutOfBoundsException e ) {
		}
		return null;
	 }

    /**
     * Return the label for a property on a particular page.
     *
     * @param itemIndex Which index (from 0) on the page to report on.
     * @return The label for a property on a particular page.
     **/
     public String getItemLabel( int itemIndex ) {
		try {
			if ( _attrLabels != null )
				return _attrLabels[itemIndex];
			return _attrNames[itemIndex];
		} catch ( ArrayIndexOutOfBoundsException e ) {
		}
		return null;
	 }

	/**
	 * For debugging
	 *
	 */
    public String toString() {
		String s = "EntryPageDescription(";
		for( int i = 0; i < getItemCount(); i++ ) {
			s += '(' + getItemName(i) + ',' + getItemLabel(i) + ')';
			if ( i < (getItemCount()-1) )
				s += ',';
		}
		s += ')';
		return s;
	}

	private String[] _attrLabels = null;
	private String[] _attrNames = null;
}
