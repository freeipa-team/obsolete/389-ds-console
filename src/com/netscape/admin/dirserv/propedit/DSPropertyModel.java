/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.propedit;

import java.util.*;
import java.io.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.client.util.Debug;
import netscape.ldap.*;

/**
 * DSPropertyModel
 * Provides data for displaying and editing the properties of a
 * DS entry
 *
 * @version 1.0
 * @author rweltman
 **/
public class DSPropertyModel implements IDSPropertyModel, Serializable {

    /**
     * Constructor for serialization.
     *
     **/
    protected DSPropertyModel() {
    }


    /**
     * Constructor for normal use.
     *
     * @param schema Schema of objects in a Directory.
     * @param entry A Directory entry with properties.
     **/
    public DSPropertyModel( LDAPSchema schema, LDAPEntry entry ) {
		_schema = schema;
		_entry = entry;
		checkObjectClasses();
    }

    /**
     * Constructor for normal use.
     *
     * @param schema Schema of objects in a Directory.
     * @param entry A Directory entry with properties.
     * @param page An entry page descriptor.
     **/
    public DSPropertyModel( LDAPSchema schema, LDAPEntry entry,
								   EntryPageDescription page ) {
		this( schema, entry );
		addPage( page );
    }

    /**
     * Constructor for normal use.
     *
     * @param schema Schema of objects in a Directory.
     * @param entry A Directory entry with properties.
     * @param pages A vector of entry page descriptors.
     **/
    public DSPropertyModel( LDAPSchema schema, LDAPEntry entry,
								   Vector pages ) {
		this( schema, entry );
		_pages = (Vector)pages.clone();
    }

    private void checkObjectClasses() {
        _attributes = DSUtil.getAllAttributeList( _schema, _entry );		
	}

    public void updateObjectClasses( Vector v ) {
        _attributes = DSUtil.getAllAttributeList( _schema, v );
	}

    public Enumeration getAttributeNames() {
		return _attributes.elements();
	}

    public String getTitle() {
		return _entry.getDN();
	}

    /**
     * Add a page description.
     *
     * @param page An entry page descriptor.
     **/
    public void addPage( EntryPageDescription page ) {
		_pages.addElement( page );
    }

    /**
     * Return the number of pages of properties.
     *
     * @return The number of pages of properties.
     **/
    public int getPageCount() {
		return _pages.size();
	}

    /**
     * Return the number of properties on a particular page.
     *
     * @param pageIndex Which page (from 0) to report on.
     * @return The number of properties on a particular page.
     **/
    public int getItemCount( int pageIndex ) {
		try {
			EntryPageDescription page =
				(EntryPageDescription)_pages.elementAt(pageIndex);
			return page.getItemCount();
		} catch ( ArrayIndexOutOfBoundsException e ) {
		}
		return 0;
	}

    /**
     * Return the name of a property on a particular page.
     *
     * @param pageIndex Which page (from 0) to report on.
     * @param itemIndex Which index (from 0) on the page to report on.
     * @return The name of a property on a particular page.
     **/
     public String getItemName( int pageIndex, int itemIndex ) {
		try {
			EntryPageDescription page =
				(EntryPageDescription)_pages.elementAt(pageIndex);
			return page.getItemName( itemIndex );
		} catch ( ArrayIndexOutOfBoundsException e ) {
		}
		return null;
	 }

    /**
     * Return the label for a property on a particular page.
     *
     * @param pageIndex Which page (from 0) to report on.
     * @param itemIndex Which index (from 0) on the page to report on.
     * @return The label for a property on a particular page.
     **/
     public String getItemLabel( int pageIndex, int itemIndex ) {
		try {
			EntryPageDescription page =
				(EntryPageDescription)_pages.elementAt(pageIndex);
			return page.getItemLabel( itemIndex );
		} catch ( ArrayIndexOutOfBoundsException e ) {
		}
		return null;
	 }

    /**
     * Return the number of values of a particular property.
     *
     * @param pageIndex Which page (from 0) to report on.
     * @param itemIndex Which index (from 0) on the page to report on.
     * @return The number of values of the property.
     **/
    public int getItemValueCount( int pageIndex, int itemIndex ) {
		try {
			EntryPageDescription page =
				(EntryPageDescription)_pages.elementAt(pageIndex);
			String name = page.getItemName( itemIndex );
			LDAPAttribute attr = _entry.getAttribute( name );
			if ( attr != null ) {
				return attr.size();
			} else {
				Debug.println( "DSPropertyModel.getItemValueCount: " +
							   " no attribute for " + name );
			}
		} catch ( ArrayIndexOutOfBoundsException e ) {
		}
		return 0;
	}

    /**
     * Return the type of values of a particular property.
     *
     * @param name Name of property.
     * @return The type of values of the property.
     **/
    static public int getAttributeType( String name ) {
		LDAPAttributeSchema attr = _schema.getAttribute( name );
		if ( attr == null ) {
			/* Try stripping off subtypes */
			attr = _schema.getAttribute(
				LDAPAttribute.getBaseName(name) );
		}
		if ( attr != null ) {
			return attr.getSyntax();
		}
		return -1;
	}

    /**
     * Returns true if the attribute is single-valued
     *
     * @param name Name of property.
     * @return true if the attribute is single-valued
     **/
    static public boolean isSingleValued( String name ) {
		LDAPAttributeSchema attr = _schema.getAttribute( name );
		if ( attr == null ) {
			/* Try stripping off subtypes */
			attr = _schema.getAttribute(
				LDAPAttribute.getBaseName(name) );
		}
		if ( attr != null ) {
			return attr.isSingleValued();
		}
		return false;
	}

    /**
     * Return the type of values of a particular property.
     *
     * @param name Name of property.
     * @return The type of values of the property.
     **/
    public int getPropertyType( String name ) {
		return getPropertyType(name, _schema);
	}

    /**
     * Return the type of values of a particular property.
     *
     * @param name   Name of property.
     * @param schema The LDAP schema object.
     * @return The type of values of the property.
     **/
    public static int getPropertyType(String name, LDAPSchema schema) {
		try {
			LDAPAttributeSchema attr = schema.getAttribute( name );
			if ( attr == null ) {
				/* Try stripping off subtypes */
				attr = schema.getAttribute(
					LDAPAttribute.getBaseName(name) );
			}
			if ( attr != null ) {
				if ( name.equalsIgnoreCase( "jpegphoto" ) )
					return JPEG;
				else if ( name.equalsIgnoreCase( "usercertificate;binary" ) )
					return CERTIFICATE;
				else if ( name.equalsIgnoreCase( "usercertificate" ) )
					return CERTIFICATE;
				else if ( name.equalsIgnoreCase( "cacertificate;binary" ) )
					return CERTIFICATE;
				else if ( name.equalsIgnoreCase( "cacertificate" ) )
					return CERTIFICATE;
				else if ( name.equalsIgnoreCase( "userpassword" ) )
					return PASSWORD;
				else if ( name.equalsIgnoreCase( "aci" ) )
					return STRING;
				else if ( name.equalsIgnoreCase( "mgmemmailuserpassword" ) )
					return PASSWORD;
				int type = attr.getSyntax();
				if ( (type == LDAPAttributeSchema.cis) ||
					 (type == LDAPAttributeSchema.ces) ||
					 (type == LDAPAttributeSchema.dn) ||
					 (type == LDAPAttributeSchema.telephone) ||
					 /* For the unknown syntaxes we treate them as Strings (default one) */
					 (type == LDAPAttributeSchema.unknown) ) {
					return STRING;
				} else if ( type == LDAPAttributeSchema.integer ) {
					return INTEGER;
				} else {
					return BINARY;
				}
			} else {
				Debug.println( "DSPropertyModel.getPropertyType: no known " +
							   "attribute " + name );
				return STRING;
			}
		} catch ( ArrayIndexOutOfBoundsException e ) {
		}
		return UNKNOWN;
	}

    /**
     * Return the type of values of a particular property.
     *
     * @param pageIndex Which page (from 0) to report on.
     * @param itemIndex Which index (from 0) on the page to report on.
     * @return The type of values of the property.
     **/
    public int getItemValueType( int pageIndex, int itemIndex ) {
		EntryPageDescription page =
			(EntryPageDescription)_pages.elementAt(pageIndex);
		String name = page.getItemName( itemIndex );
		return getPropertyType( name );
	}

    /**
     * Return the values of a particular property.
     *
     * @param pageIndex Which page (from 0) to report on.
     * @param itemIndex Which index (from 0) on the page to report on.
     * @return The values of a particular property.
     **/
   	public Vector getItemValues( int pageIndex, int itemIndex ) {
		int type = getItemValueType( pageIndex, itemIndex );
		if ( type == UNKNOWN ) {
			return null;
		}
		try {
			EntryPageDescription page =
				(EntryPageDescription)_pages.elementAt(pageIndex);
			String name = page.getItemName( itemIndex );
			LDAPAttribute attr = _entry.getAttribute( name );
			if ( attr != null ) {
				Vector v = new Vector();
				Enumeration e = null;
				if ( (type == STRING) ||
					 (type == INTEGER) ||
					 (type == PASSWORD) ) {
					e = attr.getStringValues();
				} else {
					e = attr.getByteValues();
				}
				while( e.hasMoreElements() ) {
					v.addElement( e.nextElement() );
				}
				return v;
			} else {
				Debug.println( "DSPropertyModel.getItemValues: " +
							   " no attribute for " + name );
			}
		} catch ( ArrayIndexOutOfBoundsException e ) {
		}
		return null;
	}

	public LDAPSchema getSchema() {
		return _schema;
	}

	public LDAPEntry getEntry() {
		return _entry;
	}

	public Vector getPages() {
		return _pages;
	}

	static private LDAPSchema _schema = null;
	private LDAPEntry _entry = null;
	private Vector _pages = new Vector();
	private Hashtable _attributes = new Hashtable();
}

