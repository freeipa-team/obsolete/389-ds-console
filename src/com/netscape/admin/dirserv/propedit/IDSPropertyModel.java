/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.propedit;

import java.util.*;

/**
 * IDSPropertyModel
 * Interface for a model which provides data for displaying and editing
 * the properties of a Directory entry.
 *
 * @version 1.0
 * @author rweltman
 **/
public interface IDSPropertyModel {
	public String getTitle();
	public int getPageCount();
	public int getItemCount( int pageIndex );
	public String getItemName( int pageIndex, int itemIndex );
	public String getItemLabel( int pageIndex, int itemIndex );
	public int getItemValueCount( int pageIndex, int itemIndex );
	public int getItemValueType( int pageIndex, int itemIndex );
	public Vector getItemValues( int pageIndex, int itemIndex );

	public final static int UNKNOWN = -1;
	public final static int BINARY = 0;
	public final static int STRING = 1;
	public final static int JPEG = 2;
	public final static int CERTIFICATE = 3;
	public final static int URL = 4;
	public final static int INTEGER = 5;
	public final static int PASSWORD = 6;
}
