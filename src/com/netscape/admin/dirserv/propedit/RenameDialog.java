/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.propedit;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Frame;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableCellRenderer;

import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.nmclf.SuiConstants;

import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.CenterAlignedHeaderRenderer;
import com.netscape.admin.dirserv.panel.CheckBoxTableCellRenderer;
import com.netscape.admin.dirserv.panel.CheckboxTableKeyListener;
import com.netscape.admin.dirserv.panel.MappingUtils;

class RenameDialog extends AbstractDialog implements TableUpdateListener {
	Frame _frame;

	Vector _namingAttributes = new Vector();
	String[] _saveNamingAttributes;	
	Vector _namingValues = new Vector();
	String[] _saveNamingValues;

	AttributeTableModel _attributeTableData;
	JTable _attributeTable;

	JLabel _rdnLabel;

	boolean _isCancelled;
	boolean _isModified;
	boolean _isValid;

	String _section = "RenameDialog";
	static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.propedit.propedit");	
	
	int MAX_ROW_NUMBER = 5;

	public RenameDialog(Frame frame) {
		super( frame, "", true, OK | CANCEL);
		_frame = frame;
		layoutComponents();
	}
	
	/**
	  * Displays the Dialog with the selected naming attributes and values.
	  * We assume that the naming attributes and values are included in the Hashtable values.
	  *
	  * The hashtable values has as key the name of the attributes and as value a Vector containing
	  * the string values for the attribute.
	  */
	public void display(String[] namingAttributes,
						String[] namingValues,
						Hashtable values) {			
		_isCancelled = false;
		_isModified = false;
		_isValid = true;

		_saveNamingAttributes = namingAttributes;
		_saveNamingValues = namingValues;			

		clearAttributePanel();		
		initValues(values);				

		pack();
		
		/* In order the dialog not to be bigger than the main window */
		int height = getHeight();
		int width = getWidth();

		int maxHeight = _frame.getHeight();
		int maxWidth = _frame.getWidth();

		boolean resize = false;
		if (maxHeight < height) {
			height = maxHeight;
			resize = true;
		}
		if (maxWidth < width) {
			width =maxWidth;
			resize = true;
		}

		if (resize) {
			setSize(width, height);
		}

		show();

	}
	

	/**
	 * Return the list of naming attributes.
	 */
	public String[] getNamingAttributes() {
		String[] namingAttributes = new String[_namingAttributes.size()];
		_namingAttributes.copyInto(namingAttributes);		
		return namingAttributes;
	}

	/**
	 * Return the list of naming values (in the same order of naming
	 * attributes).	 
	 */
	public String[] getNamingValues() {
		String[] namingValues = new String[_namingValues.size()];
		_namingValues.copyInto(namingValues);		
		return namingValues;
	}

	public boolean isCancelled() {
		return _isCancelled;
	}

	public boolean isModified() {
		return _isModified;
	}

	public void tableUpdated() {
		checkTable();
	}

	protected void cancelInvoked() {
		_isCancelled = true;		
		super.cancelInvoked();
	}

	protected void okInvoked() {
		/* To update all the variables (_isValid, _isModified...) that
		   the creator of the panel may have created. */
		checkTable();	
		super.okInvoked();
	}

	protected void layoutComponents() {
		setTitle(_resource.getString(_section, "title-label"));	
		getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "title-description"));	
		JPanel panel = new JPanel(new GridBagLayout());
		setComponent(panel);

		//Create the attribute viewing table.
		_attributeTableData = new AttributeTableModel();
		_attributeTableData.addTableUpdateListener(this);
		_attributeTable = new JTable(_attributeTableData);			
		_attributeTable.setColumnSelectionAllowed(false);
		_attributeTable.addKeyListener(new CheckboxTableKeyListener());		

		for (int i=0; i<_attributeTableData.COLUMN_NAMES.length; i++) {
			TableColumn tcol = _attributeTable.getColumn(_attributeTableData.COLUMN_NAMES[i]);
			tcol.setHeaderRenderer( new CenterAlignedHeaderRenderer ());
		}

		TableColumn tcol = _attributeTable.getColumn(_attributeTableData.COLUMN_NAMES[2]);
		tcol.setCellRenderer( new CheckBoxTableCellRenderer());

		JScrollPane scroll = new JScrollPane(_attributeTable);
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 1.0;	
		gbc.weighty = 1.0;
		gbc.anchor = gbc.NORTHWEST;	
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.BOTH;
		panel.add(scroll, gbc);				
		
		gbc.insets.top = SuiConstants.COMPONENT_SPACE;
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.SOUTHWEST;	
		gbc.weighty = 0.0;
		_rdnLabel = new JLabel(_resource.getString(_section, "rdn-label", (String[])null));
		_rdnLabel.setLabelFor(_attributeTable);
		panel.add(_rdnLabel, gbc);			   	
	}

	private String getInitRdn() {
		String rdn = "";
		for (int i=0; i< _saveNamingAttributes.length; i++) {
		    if (i>0) {
			rdn = rdn + " + " +_saveNamingAttributes[i]+"= "+DSUtil.escapeDNVal(_saveNamingValues[i]);
		    } else {
			rdn = _saveNamingAttributes[i]+"= "+DSUtil.escapeDNVal(_saveNamingValues[i]);
		    }
		}
		return rdn;
	}


	/**
	  * Calculates the value of the rdn from the values of _namingAttributes and _namingValues.
	  */
	private String getCurrentRdn() {
		String rdn = "";
		for (int i=0; i< _namingAttributes.size(); i++) {
			String namingAttribute = (String)_namingAttributes.elementAt(i);
			String namingValue = (String)_namingValues.elementAt(i);
			if (i>0) {
			    rdn = rdn + "+" +namingAttribute+"="+DSUtil.escapeDNVal(namingValue);
			} else {
			    rdn = namingAttribute+"="+DSUtil.escapeDNVal(namingValue);
			}
		}
		return rdn;
	}

	private void clearAttributePanel() {		
		_attributeTableData.cleanData();		
	}
	
	private void initValues(Hashtable v) {		
		/* In order to sort the attributes */
		int numberAttributes = v.size();
		String[] attributeNames = new String[numberAttributes];
		Enumeration keys = v.keys();
		int i=0;
		while (keys.hasMoreElements()) {			
			attributeNames[i] = (String)keys.nextElement();			
			i++;
		}
		/* Sort the attributes */
		DSUtil.bubbleSort(attributeNames, true);

		for (i=0; i<numberAttributes; i++) {	
			String attribute = attributeNames[i];
			Vector values = (Vector)v.get(attribute);
			for (int j=0; j<values.size(); j++) {
				String value = (String)values.elementAt(j);
				_attributeTableData.addRow(attribute, 
										   value, 
										   isSaveNamingAttribute(attribute, value));				
			}
		}				
		_rdnLabel.setText(_resource.getString(_section, "rdn-label", getInitRdn()));

		int rows = Math.min(MAX_ROW_NUMBER, _attributeTable.getRowCount());
		Dimension d = new Dimension((int)_attributeTable.getPreferredScrollableViewportSize().getWidth(),
									rows * _attributeTable.getRowHeight());
		_attributeTable.setPreferredScrollableViewportSize(d);

		_attributeTableData.fireTableDataChanged();
		_attributeTable.getTableHeader().resizeAndRepaint();
		getContentPane().validate();
		getContentPane().repaint();
	}

	/**
	  * Says if the pair of values attribute-value are part of the rdn the user passed.
	  */
	private boolean isSaveNamingAttribute(String attribute, String value) {
		boolean isValue = false;
		boolean isAttribute = false;

		for (int i=0; (i<_saveNamingAttributes.length) && !isAttribute; i++) {
			if (attribute.equals(_saveNamingAttributes[i])) {
				isAttribute = true;
			}
		}

		if (isAttribute) {
			for (int i=0; (i<_saveNamingValues.length) && !isValue; i++) {
				if (value.equals(_saveNamingValues[i])) {
					isValue = true;
				}
			}
		}
		return (isValue && isAttribute);
	}

	/**
	  * Updates _namingValues, _namingAttributes, _rdnLabel,  _isValid and _isModified according 
	  * to the contents of the table.
	  */
	protected void checkTable() {
		_isValid = false;
		_isModified = false;

		_namingAttributes.clear();
		_namingValues.clear();
		
		Boolean TRUE = new Boolean(true);

		for (int i=0; i<_attributeTableData.getRowCount(); i++) {
			if (_attributeTableData.getValueAt(i, 2).equals(TRUE)) {
				_namingAttributes.addElement(_attributeTableData.getValueAt(i, 0));
				_namingValues.addElement(_attributeTableData.getValueAt(i, 1));

				_isValid = true;
			}
		}

		_isModified = _saveNamingAttributes.length != _namingAttributes.size();

		for (int i=0; (i < _saveNamingAttributes.length) && !_isModified; i++) {
			int index = _namingAttributes.indexOf(_saveNamingAttributes[i]);
			if ((index < 0) ||
				(index != _namingValues.indexOf(_saveNamingValues[i]))) {
				_isModified = true;
			}
		}
		
		setOKButtonEnabled(_isValid);
					 	
		_rdnLabel.setText(_resource.getString(_section, "rdn-label", getCurrentRdn()));		
	}
}


/* Our particular table model, conceived to display attributes */

class AttributeTableModel extends AbstractTableModel {
	Vector _listeners = new Vector();
	Vector _attributes = new Vector();
	Vector _values = new Vector();
	Vector _inRDN = new Vector();
	
	String _section = "RenameDialog";
	static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.propedit.propedit");
	
	public final String[] COLUMN_NAMES = {_resource.getString(_section, "attribute-label"),
										  _resource.getString(_section, "value-label"),
										  _resource.getString(_section, "inRdn-label")};
	
	public AttributeTableModel() {			
	}
	
	public void addTableUpdateListener(TableUpdateListener listener) {
			_listeners.addElement(listener);
	}
	
	public void removeTableUpdateListener(TableUpdateListener listener) {
		_listeners.removeElement(listener);
	}
	
	public int getRowCount() {
		return _values.size();
	}

	public int getColumnCount() {
		return 3;
	}

	public boolean isCellEditable(int row, int col) {
		return (col == 2);
	}
	
	public Object getValueAt(int row, int column) {		
		if (column == 0) {
			return _attributes.elementAt(row);
		} else if (column == 1) {
			return _values.elementAt(row);
		} else {
			return _inRDN.elementAt(row);
		}
	}
	
	public void cleanData() {
		_attributes.clear();
		_values.clear();
		_inRDN.clear();
	}
	
	public void addRow(String attributeName, String value, boolean inRDN) {		
		_attributes.addElement(attributeName);
		_values.addElement(value);
		_inRDN.addElement(new Boolean(inRDN));
	}

	public void setValueAt(Object aValue, int row, int column) {
		if ((column == 2) &&
			(aValue instanceof Boolean) &&
			(row < _inRDN.size())) {
			_inRDN.setElementAt(aValue, row);
			if (((Boolean)aValue).booleanValue() == true) {
				Boolean FALSE = new Boolean(false);

				/* Have to unselect the other values of the attribute automatically */
				String attribute = (String)getValueAt(row, 0);
				boolean doneUpwards = false;
				for (int i=row-1; (i>=0) && !doneUpwards; i--) {
					if (attribute.equals(getValueAt(i, 0))) {
						_inRDN.setElementAt(FALSE, i);
					} else {
						doneUpwards = true;
					}
				}
				boolean doneDownwards = false;
				for (int i=row+1; (i < _inRDN.size()) && !doneDownwards; i++) {
					if (attribute.equals(getValueAt(i, 0))) {
						_inRDN.setElementAt(FALSE, i);
					} else {
						doneDownwards = true;
					}
				}
			}
			for (int i=0; i<_listeners.size(); i++) {
				((TableUpdateListener)_listeners.elementAt(i)).tableUpdated();
			}
		}		
	}
	
	public String getColumnName(int column) {
		if (column >= 3) {
			return "";
		} else {
			return COLUMN_NAMES[column];
		}
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}
}

interface TableUpdateListener {
	public void tableUpdated();
}
