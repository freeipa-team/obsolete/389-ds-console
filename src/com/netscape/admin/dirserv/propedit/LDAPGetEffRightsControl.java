/* BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK */

/*
 * LDAPGetEffRightsControl.java
 *
 * Created on January 20, 2005, 11:43 AM
 */

package com.netscape.admin.dirserv.propedit;

import netscape.ldap.ber.stream.*;
import netscape.ldap.LDAPControl;

/**
 *
 * @author  Cheston Williams
 */

public class LDAPGetEffRightsControl extends LDAPControl {
    public final static String GETEFFRIGHTS = "1.3.6.1.4.1.42.2.27.9.5.2";
    
    /** Constructs a new LDAPGetEffRightsControl object 
     *  with the parameters
     *  boolean crit which is the criticality of the control
     *  dn which is the distinguished name of the user for which we
     *  wish to obtain the effective rights
     */
    public LDAPGetEffRightsControl(String dn, boolean crit) {
        super(GETEFFRIGHTS,crit,null);
        m_value = establishDN(dn);
    }
    
    private byte[] establishDN(String DN)
    {
        return new BEROctetString("dn:" + DN).getValue();
    }
    
}
