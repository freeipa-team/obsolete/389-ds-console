/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.io.File;
import java.io.UnsupportedEncodingException;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ModalDialogUtil;
import com.netscape.admin.dirserv.GenericProgressDialog;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.admin.dirserv.panel.GroupPanel;
import com.netscape.admin.dirserv.panel.DatabaseImportPanel;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.management.nmclf.SuiConstants;
import netscape.ldap.*;
import netscape.ldap.util.*;
/**
 * BulkImport
 * Class used to perform a bulk import in the server.  Progress shown via an GenericProgressDialog
 *
 * @version 1.0
 * @author jvergara
 **/
public class BulkImport extends Object implements IEntryChangeListener,
	Runnable,
	ActionListener {
	
	public BulkImport(String[] backends,
					  String importPath,
					  String rejects,
					  IDSModel model,
					  GenericProgressDialog dlg) {
		_backends = backends;
		_importPath = importPath;
		_rejects = rejects;
		_model = model;
		_dlg = dlg;		
		
		setupDialog();
	}

		/* Sets up the progress dialog */
	protected void setupDialog() {		
		_dlg.addStep(_resource.getString(_section, "LDAPMode-firstStep-title"));
		_dlg.addStep(_resource.getString(_section, "LDAPMode-secondStep-title"));
		_dlg.addStep(_resource.getString(_section, "LDAPMode-thirdStep-title"));
		_dlg.addActionListener((ActionListener)this);
		_dlg.setLabelRows(2);
		_dlg.setTextInTextAreaLabel(_resource.getString(_section,"status-progressdialog-label"));
		_dlg.setTextAreaRows(5);
	}
		

	/**
	 *	Handle incoming event from button.
	 *
	 * @param e event
	 */
	public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( GenericProgressDialog.CANCEL ) ) {
			_continue = false;
			_dlg.disableCancelButton();			
			_dlg.setTextInLabel(_resource.getString(_section, "LDAPMode-cancelled-title"));
			
			if ( _done ) {
				_dlg.closeCallBack();			
			}
		} else if ( e.getActionCommand().equals(GenericProgressDialog.CLOSE ) ) {
			_continue = false;
			if ( _done ) {		
				_dlg.closeCallBack();
			}
		}
	}
															 
		/**
		 * Called when an entry changes, e.g. on export/import. Return
		 * true to continue, false to stop.
		 */
	public boolean entryChanged( String dn, String msg ) {	
		/* A message to display */
		if ( _continue && (dn != null) ) {
			if ((_ds.getCurrentEntry() % 5) == 0) {
				String[] args = {String.valueOf(_ds.getCurrentEntry()),
								 DSUtil.abreviateString(dn, 60)};				
				_dlg.setTextInLabel( _resource.getString(_section,
													"addingentrynumber-label",
													args));				
			}				
			if (msg != null) {
				if (!dn.trim().equals("")) {
					_dlg.appendTextToTextArea(DSUtil.abreviateString(dn, 30)+": "+msg+"\n");
				} else {
					_dlg.appendTextToTextArea(msg+"\n");
				}
			}
			return true;
		} else {			
			return false;
		}
	}

	public boolean entryChanged( String dn ) {
		return entryChanged( dn, null );
	}

 
	public void run() {		
		/* To let the main thread display the progress dialog */
		try {
			Thread.sleep(400);
		} catch (Exception e) {
			Debug.println("BulkImport.run: " +
						  e );
			e.printStackTrace();	
		}
		executeBulkImport(_backends, _importPath, _rejects);		
	}

		/* This method performs the Bulk Import.
		   There are three steps for a bulk import on each backend:
		   1. Send an extended operation to the backend saying to start the bulk import
		   2. Read the LDAP entries from the file and perform the LDAP Adds
		   3. Send an extended operation to the backend saying to stop the bulk import (data is committed here)
		   */

	private void executeBulkImport(String[] backends, String importPath, String rejects) {
		LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
		LDAPExtendedOperation exop;
		LDAPExtendedOperation exres;	
		String title = _resource.getString("import", "title");		
		
		int numberOfInstances = backends.length;
		
		for (int i=0; i< backends.length; i++) {
			_currentBackend = backends[i];
			_result =  DatabaseImportPanel.SUCCESS;
			/* Reset the dialog for each backend */
			_dlg.reset();
			String suffix = MappingUtils.getSuffixForBackend(ldc, _currentBackend);			
			String labelText;
			if (backends.length>1) {
				String[] args = {
					DSUtil.inverseAbreviateString(importPath, 40),
					DSUtil.abreviateString(_currentBackend,30),
					String.valueOf(i+1),
					String.valueOf(backends.length),								 
				};
				labelText = _resource.getString(_section, "LDAPMode-progresslabel-title", args);				
			} else {
				String[] args = {
					DSUtil.inverseAbreviateString(importPath, 40),
					DSUtil.abreviateString(_currentBackend,30)
				};
				labelText = _resource.getString(_section, "LDAPMode-progresslabel-one-partition-title", args);
			}		
			_dlg.setTextInLabel(labelText);

			try {
				byte[] vals = suffix.getBytes( "UTF8" );
				
				/* Create the extended operation*/
				exop = new LDAPExtendedOperation(START_BULK_IMPORT_OID, vals );	
				exres = ldc.extendedOperation( exop );

				/* Check the answer to the extended operation*/
				if (exres == null) {
					Debug.println( "BulkImport.executeBulkImport(): the answer to the extended operation is null");
					_dlg.appendTextToTextArea(_resource.getString(
																  _section, 
																  "BulkAddError-ExtendedOperationError-NoResponse-msg")
											  +"\n");
					_result =  DatabaseImportPanel.ERROR;
				}
				
				if (!exres.getID().equals(START_BULK_IMPORT_OID)) {				
					Debug.println( "BulkImport.executeBulkImport(): the answer to the extended operation is NO GOOD");
					_dlg.appendTextToTextArea(_resource.getString(
																		 _section,
																		 "BulkAddError-ExtendedOperationError-NoGoodResponse-msg")
											  +"\n");
					_result =  DatabaseImportPanel.ERROR;
				}
			} catch (LDAPException lde) {
				Debug.println( "BulkImport.executeBulkImport(): LDAPException" + lde );	
				_dlg.appendTextToTextArea(lde.errorCodeToString()+"\n");				
				_result =  DatabaseImportPanel.ERROR;
			}
			catch( UnsupportedEncodingException exc ) {
				Debug.println( "BulkImport.executeBulkImport(): Error: UTF8 not supported" );			
			}
			
			/* If there was an error we ask the user if he wants to continue (if it's not the last backend).
			   If it is the last backend, we just quit */
			if (_result ==  DatabaseImportPanel.ERROR) {
				if (i < (backends.length -1)) {
					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {
								int wantToContinue = DSUtil.showConfirmationDialog( _dlg,
																					"LDAPMode-unsuccessful-wantToCancel", 
																					_currentBackend,
																					_section);
								if (wantToContinue != JOptionPane.YES_OPTION) {
									_continue = false;								
								}
							}
						});
					} catch (Exception e) {

					}
					if (!_continue) {	
						_dlg.closeCallBack();
						break;
					}
					continue;
				} else {
					_dlg.setTextInLabel(_resource.getString("import", "LDAPMode-endError-title"));
					_continue = false;
					_dlg.closeCallBack();
					break;
				}				
			}
		
			_dlg.stepCompleted(0);
			
			String[] auxSuffixes = new String[1];
			auxSuffixes[0] = suffix;
			
			if (auxSuffixes[0] != null) {
				/* We make the adds using LDAP */
				boolean continuous = true;							
				
				_ds = new DSExportImport( ldc,
										  importPath,
										  true,
										  continuous,
										  rejects);
				_ds.addEntryChangeListener(this); 
				
				_ds.run();					

				boolean status = _ds.getStatus();
			
				if ( !status ) {			 
					if ( _ds.getError() == _ds.STATUS_UNWRITABLE ) {
						Debug.println("BulkImport.executeBulkImport(): Error: status unwritable)");
						String[] args = {_rejects,
										 Integer.toString(_ds.STATUS_UNWRITABLE)};
						_dlg.appendTextToTextArea( _resource.getString(_section,
															"unwritable-msg",
																 args)
												   +"\n");

					} else if (_ds.getError() == _ds.LDIF_SYNTAX_ERROR) {
						String[] args = {DSUtil.abreviateString(_ds.getLastDN(), 45),
										 Integer.toString(_ds.LDIF_SYNTAX_ERROR)};
						_dlg.appendTextToTextArea( _resource.getString( "general" ,
																		"BulkAddError-ldifSyntaxError-msg", 
																		args)
												   +"\n");							

					} else if (_ds.getError() == _ds.MALFORMED_EXPRESSION_ERROR) {						
						int position = _ds.getEntryCount() + _ds.getRejectCount() +1;
						String lastDN = _ds.getLastDN();
						
						if (lastDN == null) {
							String[] args = {Integer.toString(position),
											 Integer.toString(_ds.MALFORMED_EXPRESSION_ERROR)};
							_dlg.appendTextToTextArea( _resource.getString(_section, 
																		   "malformed-expression-error-number-msg", 
																		   args)
													   +"\n");
						} else {
							String[] args = {DSUtil.abreviateString(lastDN, 45),
											 Integer.toString(_ds.MALFORMED_EXPRESSION_ERROR)};
							_dlg.appendTextToTextArea( _resource.getString(_section,
																"malformed-expression-error-dn-msg", 
																args)
													   +"\n");
						}											
					} else {
						Debug.println("BulkImport.executeBulkImport(): Error in status");
						String[] args = {Integer.toString(_ds.getError())};
						_dlg.appendTextToTextArea( _resource.getString(_section,
															"failed-msg",
															args));
					}
					_result =  DatabaseImportPanel.ERROR;
					_dlg.setTextInLabel(_resource.getString(_section, "error-partition-wait-title"));					
				}			
				_dlg.stepCompleted(1);
			} else {
				String args[] = {_currentBackend};
				_dlg.appendTextToTextArea(_resource.getString(_section,
															   "suffix-not-found-error-msg", 
															   args));
				_dlg.setTextInLabel(_resource.getString(_section, "error-partition-wait-title"));
				_result =  DatabaseImportPanel.ERROR;
			}
			
			/* We send the extended operation to stop the import */
			boolean errorStopping = false;
			try {
				byte[] vals = suffix.getBytes( "UTF8" );
				exop = new LDAPExtendedOperation(FINISH_BULK_IMPORT_OID, vals );
				exres = ldc.extendedOperation( exop );
				/* If we have no answer we have to drop the connection (we have to force it in order to be able to perform other LDAP operations) */
				if (exres == null) {
					Debug.println( "BulkImport.executeBulkImport()): the answer to the STOP extended operation is null");
					_dlg.appendTextToTextArea(_resource.getString(
																  _section,
																  "failed"));
					_result =  DatabaseImportPanel.ERROR;
					errorStopping = true;
				} else if (!exres.getID().equals(FINISH_BULK_IMPORT_OID)) {
					Debug.println( "BulkImport.executeBulkImport(): the answer to the STOP extended operation is NO GOOD");
					_dlg.appendTextToTextArea(_resource.getString(
																  _section,
																  "failed"));
					_result =  DatabaseImportPanel.ERROR;
					errorStopping = true;
				}
			} catch (LDAPException lde) {
				Debug.println( "BulkImport.executeBulkImport(): in STOP LDAPException " + lde );				
				_dlg.appendTextToTextArea(lde.errorCodeToString());
				_result =  DatabaseImportPanel.ERROR;
				errorStopping = true;
			}
			catch( UnsupportedEncodingException e ) {
				Debug.println( "BulkImport.executeBulkImport(): Error: UTF8 not supported" );
				_dlg.appendTextToTextArea(_resource.getString(
															  _section,
															  "failed"));
				_result =  DatabaseImportPanel.ERROR;
				errorStopping = true;
			}
			if (!errorStopping) {
				_dlg.stepCompleted(1);
				_dlg.stepCompleted(2);			 
			}

			/* If there was an error we ask the user if he wants to continue (if it's not the last backend).
			   If it is the last backend, we just quit */
			if ((_result != DatabaseImportPanel.SUCCESS) && _continue) {
				if (i < (backends.length -1)) {
					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {
								int wantToContinue = DSUtil.showConfirmationDialog( _dlg,
																					"LDAPMode-unsuccessful-wantToCancel",
																					_currentBackend,
																					_section);
								if (wantToContinue != JOptionPane.YES_OPTION) {														
									_continue = false;
								}
							}
						});
					} catch (Exception ex) {
					}
					if (!_continue) {
						_dlg.setTextInLabel(_resource.getString("import", "LDAPMode-cancelled-title"));
					}
				}
			} else if ((_result == DatabaseImportPanel.SUCCESS) && !_continue) {
				/* If there was no error and _continue is false it means that the user clicked 'Stop' and that
				   everything is fine so far: clean up the backend */
				try {
					final String[] arg = {_currentBackend};
					/* Inform we are doing the cleanup of the backend... */
					_dlg.setTextInLabel(_resource.getString("import", 
															"LDAPMode-backend-cleanup-label", 
															arg));
					boolean cleanUpDone = cleanUpBackend(ldc, suffix);
					if (!cleanUpDone) {
						/* The cleanup could not be done: display an error dialog*/
						try {
							SwingUtilities.invokeAndWait(new Runnable() {
								public void run() {
									DSUtil.showErrorDialog( _dlg,
															"LDAPMode-backend-cleanup-error-title",
															"LDAPMode-backend-cleanup-error-msg",
															arg,
															"import");								
								}
							});
						} catch (Exception ex) {
							ex.printStackTrace(); // Never should happen: we are out the event thread
						}						
					}
				} catch (LDAPException e) {
					/* The cleanup could not be done and there was an exception (the common case): display an error dialog */
					String ldapError;
					String ldapMessage;
					
					ldapError =  e.errorCodeToString();
					ldapMessage = e.getLDAPErrorMessage();
					if ((ldapMessage != null) &&
						(ldapMessage.length() > 0)) {
						ldapError = ldapError + ". "+ldapMessage;
					}
					final String[] args = {_currentBackend, ldapError};
					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {
								DSUtil.showErrorDialog( _dlg,
														"LDAPMode-backend-cleanup-ldaperror-title",
														"LDAPMode-backend-cleanup-ldaperror-msg",
														args,
														"import");								
							}
						});		
					} catch (Exception ex) {
						ex.printStackTrace(); // Never should happen: we are out the event thread
					}			
				}
			}
			
			if (!_continue) {
				_dlg.closeCallBack();
				break;
			}
		}
		_done = true;

		_dlg.waitForClose();
		if (_result == DatabaseImportPanel.SUCCESS) {
			_dlg.setTextInLabel(_resource.getString("import", "LDAPMode-finished-title"));			
		} else {
			_dlg.setTextInLabel(_resource.getString("import", "LDAPMode-endError-title"));
		}			
	}

	public int getResult() {
		return _result;
	}

	private IDSModel getModel() {
		return _model;
	}

	
		/**
		 * This method tries to erase the contents of a backend with a given suffix.
		 *
		 * @param suffix the suffix of the backend to clean up
		 * @param ldc the LDAPConnection to the server
		 * @return If the cleanUp is successful returns true. If it fails returns false.
		 * @throws LDAPException if an LDAPException occurs during the cleanup
		 */
	private boolean cleanUpBackend(LDAPConnection ldc, String suffix) throws LDAPException {		
		if ((suffix == null) || (ldc == null)) {
			throw new IllegalArgumentException("The suffix of the connection are null");
		}
		boolean cleanUpDone = false;
		byte[] vals = null;
		try {
			vals = suffix.getBytes( "UTF8" );
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace(); // This shouldn't happen: UTF8 encoding is supported
		}
		if (vals != null) {
			/* Create the extended operation*/
			LDAPExtendedOperation exop = new LDAPExtendedOperation(START_BULK_IMPORT_OID, vals );	
			LDAPExtendedOperation exres = ldc.extendedOperation( exop );
			if ((exres != null) && exres.getID().equals(START_BULK_IMPORT_OID)) {
				exop = new LDAPExtendedOperation(FINISH_BULK_IMPORT_OID, vals );
				exres = ldc.extendedOperation( exop );
				if ((exres != null) && exres.getID().equals(FINISH_BULK_IMPORT_OID)) {
					cleanUpDone = true;
				}
			}
		}
		return cleanUpDone;
	}

	String _currentBackend;
	private GenericProgressDialog _dlg;
	private String[] _backends;
	private String _importPath;
	private String _rejects;
	private boolean _continue = true;
	private boolean _done = false;
	private DSExportImport _ds;
	private int _result = DatabaseImportPanel.SUCCESS;
	private IDSModel _model;
	private static final ResourceSet _resource = DSUtil._resource;
	private static final String CANCEL = "cancel";
	private static final String CLOSE = "close";
	private final String START_BULK_IMPORT_OID = "2.16.840.1.113730.3.5.7";    
	private final String FINISH_BULK_IMPORT_OID = "2.16.840.1.113730.3.5.8";
	private static final String _section = "import";
}
