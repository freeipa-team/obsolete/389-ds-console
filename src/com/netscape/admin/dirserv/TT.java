/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.Vector;
import java.io.*;
import com.netscape.management.client.util.Debug;

/**
 * TT
 * For encoding/decoding Easter Egg text
 *
 * @version 1.0
 * @author rweltman
 **/
public class TT {
    static Vector read ( InputStream is ) {
		Vector v = new Vector();
        try {
			BufferedReader reader =
				new BufferedReader( new InputStreamReader( is ) );
			StringBuffer buf = new StringBuffer();
			while( true ) {
				int ch = reader.read();
				if ( ch == -1 ) {
					break;
				}
				ch = ch ^ 0x58;
				if ( ch == '\n' ) {
					v.addElement( buf.toString() );
					buf = new StringBuffer();
				} else if ( ch != '\r' ) {
					buf.append( (char)ch );
				}
			}
		} catch ( Exception e ) {
		    Debug.println( "TT.read: " + e );
		}
		return v;
    }

    static void write ( InputStream is, OutputStream os ) {
        try {
			BufferedReader reader =
				new BufferedReader( new InputStreamReader( is ) );
			BufferedWriter writer =
				new BufferedWriter( new OutputStreamWriter( os ) );
			boolean inComment = false;
			int	lastCh = '\n';
			while( true ) {
				int ch = reader.read();
				if ( ch == -1 ) {
					break;
				}
				if ( !inComment ) {
					if ( lastCh == '\n' && ch == '#' ) {
						inComment = true;
					} else {
						lastCh = ch;
						ch = ch ^ 0x58;
						writer.write( ch );
					}
				} else if ( ch == '\n' ) {
					inComment = false;
				}
			}
			reader.close();
			writer.flush();
			writer.close();
		} catch ( Exception e ) {
		    Debug.println( "TT.write: " + e );
		}
    }

	/* Read a file and XOR it into another file */
    public static void main( String[] args ) {
		Debug.setTrace( true );
		if ( args.length < 2 ) {
			System.err.println( "Usage: TT infile outfile" );
			System.exit( 1 );
		}
		try {
			InputStream is = new FileInputStream( args[0] );
			OutputStream os = new FileOutputStream( args[1] );
			write( is, os );
		} catch ( Exception e ) {
			System.err.println( e );
		}
		System.exit( 0 );
    }
}
