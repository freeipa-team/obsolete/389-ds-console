/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.Enumeration;
import java.util.Vector;
import java.awt.event.FocusEvent;
import javax.swing.JFrame;
import com.netscape.management.client.IFramework;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IMenuItem;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.ResourceModel;
import com.netscape.management.client.console.ConsoleInfo;
import netscape.ldap.*;

/**
 *	Netscape Directory Server 4.0 empty resource model.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	11/1/97
 */

public class DefaultResourceModel extends ResourceModel
                                  implements IDSModel {

	/**
	 * Constructor - initialize all internal variables
	 *
	 * @param info	Global console connection information
	 * @param serverInfo Server instance connection information
	 */
	public DefaultResourceModel() {
	}


    public void initialize( Object root ) {
	}

	public void removeElement( IDSEntryObject deo ) {
	}

    /**
     * Return the global console information reference.
     *
     * @return The global console information reference.
     **/
    public ConsoleInfo getConsoleInfo() {
		return null;
    }

    /**
     * Return the Directory-specific information reference.
     *
     * @return The Directory-specific information reference.
     **/
    public ConsoleInfo getServerInfo() {
		return null;
    }
    
    /**
	 * Get new authentication credentials from a dialog, reauthenticate
	 * the connection in _serverInfo.
	 *
	 * @return <CODE>true</CODE> if the connection was reauthenticated.
	 */
    public boolean getNewAuthentication() {
		return false;
	}
	
    /**
	 * Get new authentication credentials from a dialog, reauthenticate
	 * the connection in _serverInfo.
	 *
	 * @param notifyListeners If true, the authentication change listeners
	 *                        will be updated immediately.  If false, the
	 *                        caller will have to call the
	 *                        notifyAuthChangeListeners() method of this
	 *                        class.  This is useful if the reauthentication
	 *                        happens during some extended operation, such as
	 *                        subtree deletion, and it is not desirable to
	 *                        notify the listeners until the operation is
	 *                        completed.
	 *
	 * @return <CODE>true</CODE> if the connection was reauthenticated.
	 */
	public boolean getNewAuthentication(boolean notifyListeners) {
		return getNewAuthentication(true);
	}
	
	/**
	 * If getNewAuthentication was called with the notifyListeners set to false,
	 * this method must be called.
	 */
	public void notifyAuthChangeListeners() {
	}

	public JFrame getFrame() { return DSUtil.getDefaultFrame(); }
	
    public void setSelectedNode( IResourceObject deo ) {
	}

	/**
	 * Default action does nothing
	 */
	void actionSelected( String cmd, IResourceObject[] selection,
						 IPage viewInstance ) {
	}

	public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
	}

	/**
	 * Default action does nothing
	 */
	public void focusGained( FocusEvent e ) {
	}

	/**
	 * Default action does nothing
	 */
	public void setSelected( IPage viewInstance,
							 IResourceObject[] selection,
							 IResourceObject[] previousSelection) {
	}

    /**
      * Adds a listener that is interested in receiving selection events.
	  * Called by panels
      */
	public void addIDSResourceSelectionListener(
	                                    IDSResourceSelectionListener l) {
	}

    /**
      * Removes previously added IDSResourceSelectionListener.
	  * Called by panels
      */
	public void removeIDSResourceSelectionListener(
	                                    IDSResourceSelectionListener l) {
    }	                                        

	/**
     *   Returns list of listeners for this model.
	 */
	public Enumeration getSelectionListeners() {
		return null;
	}

	/**
	 * Get the schema of the Directory instance
	 *
	 * @return A reference to a schema object.
	 */
	public LDAPSchema getSchema() {
		return _schema;
	}

	/**
	 * Sets a reference to the schema of the Directory instance
	 *
	 * @param schema A reference to a schema object.
	 */
	public void setSchema( LDAPSchema schema ) {
		_schema = schema;
	}

	/**	
	 * Add an object to be notified on authentication changes.
	 *
	 * @param l Object to be notified.
	 */
	public void addAuthenticationChangeListener(
		                        IAuthenticationChangeListener l ) {
	}

	/**	
	 * Set the list of objects to be notified on authentication changes.
	 *
	 * @param v vector of objects to be notified.
	 */
	public void setAuthenticationChangeListener(Vector v) {
	}

    public IPage getSelectedPage() {
		return null;
	}

    /**
      * Adds a client who can report unsaved changes.
	  * Called by panels
      */
    public void addChangeClient( IChangeClient client ) {
	}

    /**
      * Removes previously added Change client.
	  * Called by panels
      */
    public void removeChangeClient( IChangeClient client ) {
	}

    public void setWaitCursor( boolean on ) {
	}

    /**
      * Called internally when page is selected
      */
	public void pageSelected( IFramework framework, IPage viewInstance ) {
	}

	/**
	 * Notifies that the root DN changed
	 */
    public void rootDNChanged( String rootDN ) {
	}

	/**
	 * Notifies that directory contents have changed
	 */
	public void contentChanged() {
	}

	public void refreshView() {
		return;
	}

	private LDAPSchema _schema = null;
}
