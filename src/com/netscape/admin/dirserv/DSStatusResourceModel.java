/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.Component;
import java.awt.event.*;
import java.util.Enumeration;
import java.util.Vector;
import com.netscape.management.client.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.node.*;

/**
 *	Netscape Directory Server 4.0 resource model.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	11/1/97
 */

public class DSStatusResourceModel extends DSBaseModel implements IDSContentListener {

    /**
     * Constructor - create all panels.
     *
     * @param info	Global console connection information
     * @param serverInfo Server instance connection information
     */
    public DSStatusResourceModel( ConsoleInfo info,
				  ConsoleInfo serverInfo ) {
	super( info, serverInfo );
	ResourceObject root = new StatusResourceObject( this );
	setRoot( root );
    }

    /**
     * Returns supported menu categories
     */
    public String[] getMenuCategoryIDs() {
		if (_categoryID == null) {
			_categoryID = new String[] {
				Framework.MENU_FILE,
					Framework.MENU_VIEW,
					ResourcePage.MENU_CONTEXT,			
					ResourcePage.MENU_OBJECT,	
					};
		}
		return _categoryID;
    }
	
    /**
     * add menu items for this page.
     */
    public IMenuItem[] getMenuItems(String category) {
		if(category.equals(Framework.MENU_FILE)) {
			if (_menuFile == null) {
				_menuFile = new IMenuItem[] {
					new MenuItemText( AUTHENTICATE,
									  _resource.getString("menu", "authenticate"),
									  _resource.getString("menu",
														  "authenticate-description")),
						new MenuItemSeparator(),
						};
			}
			return _menuFile;
		} else if(category.equals(Framework.MENU_VIEW)) {
			if (_menuView == null) {
				_menuView = new IMenuItem[] {
					new MenuItemSeparator(),
					new MenuItemText( REFRESH_ALL,
									  _resource.getString("menu", "refresh-all"),
									  _resource.getString("menu",
														  "refresh-all-description"))
						};
			}
			return _menuView;
		} else if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_menuContext == null) {
				_menuContext = new IMenuItem[] {
					new MenuItemText( REFRESH,
									  _resource.getString("menu", "refresh"),
									  _resource.getString("menu",
													  "refresh-description"))
						};
			}
			return _menuContext;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) {
			if (_menuObject == null) {
				_menuObject = new IMenuItem[] {
					new MenuItemText( REFRESH,
									  _resource.getString("menu", "refresh"),
									  _resource.getString("menu",
														  "refresh-description"))
						};
			}
			return _menuObject;
		}
		return null;
    }
	
    /**
     * Notification that a menu item has been selected.
     */
    public void actionMenuSelected(IPage viewInstance, IMenuItem item)
    {
		setWaitCursor(true);
		super.actionMenuSelected(viewInstance, item);
		if (item.getID().equals(OPEN)) {
			actionObjectRun(viewInstance, _selection);
			
		} else if(item.getID().equals(AUTHENTICATE)) {
			boolean result = getNewAuthentication();
			
		} else if(item.getID().equals(REFRESH_ALL)) {
			setSelectedNode((IResourceObject)getRoot());
			refreshObject();
		} else if (item.getID().equals(REFRESH)) {
		refreshObject();
		}
		setWaitCursor(false);
		
    }

    public void actionObjectSelected( IPage viewInstance,
				      IResourceObject[] selection,
				      IResourceObject[] previousSelection) {
	super.actionObjectSelected(viewInstance, selection, previousSelection);
	_selection = selection;
	if ( _selection == null )
	    _selection = new IResourceObject[0];
	Vector selected = new Vector();
	Vector toNotify = new Vector();
	/* Signal all selected objects, keep track of which ones */
	for( int i = 0; i < _selection.length; i++ ) {
	    IResourceObject sel = _selection[i];
	    Component c = sel.getCustomPanel(); 
	    if ( (c != null) && _selectionListeners.contains( c ) ) {
		toNotify.addElement( sel );
	    }
	    selected.addElement( c );
	}

	/* All other listeners must be unselected */
	if ( previousSelection != null ) {
	    for( int i = 0; i < previousSelection.length; i++ ) {
		IResourceObject sel = previousSelection[i];
		Component c = sel.getCustomPanel();
		if ( (c != null) &&
		     _selectionListeners.contains( c ) &&
		     !selected.contains( c ) ) {
		    try {
			IDSResourceSelectionListener l = 
			    (IDSResourceSelectionListener)c;
			l.unselect( sel, viewInstance );
		    } catch ( Exception e ) {
			Debug.println(
				      "DSResourceModel.actionObjectSelected: " +
				      "c = " + c + ", " + e.toString() );
		    }
		}
	    }
    	}

	for( int i = 0; i < toNotify.size(); i++ ) {
	    IResourceObject sel =
		(IResourceObject)toNotify.elementAt( i );
	    Component c = sel.getCustomPanel(); 
	    IDSResourceSelectionListener l =
		(IDSResourceSelectionListener)c;
	    l.select( sel, viewInstance );
	}
    }


    /**
     * Notification that the framework window is closing.
     * Called by ResourcePage
     */
    public void actionViewClosing(IPage viewInstance)
	throws CloseVetoException {
	/* Check if any clients have unsaved changes */
	Enumeration e = _changeClients.elements();
	while( e.hasMoreElements() ) {
	    if ( ((IChangeClient)e.nextElement()).isDirty() ) {
				/* Yes, notify the framework */
		throw new CloseVetoException();
	    }
	}
    }

    public void refreshView() {
	refreshObject();
    }

    private void refreshObject() {
	if ( (_selection != null) && (_selection.length > 0) ) {
	    IResourceObject sel = _selection[0];
		if (sel.equals(getRoot())) {
			setSchema( null ); /* Refresh schema if we are refreshing the root */
		}
	    Component panel = sel.getCustomPanel();
	    if ( sel instanceof ActionListener )
		((ActionListener)sel).actionPerformed(
						      new ActionEvent( this,
								       ActionEvent.ACTION_PERFORMED,
								       REFRESH ) );
	    if ( (panel != null) && (panel instanceof ActionListener) )
		((ActionListener)panel).actionPerformed(
							new ActionEvent( this,
									 ActionEvent.ACTION_PERFORMED,
									 REFRESH ) );
	}
    }

	private String[] _categoryID = null;
	private IMenuItem[] _menuFile = null;
	private IMenuItem[] _menuContext = null;
	private IMenuItem[] _menuView = null;
	private IMenuItem[] _menuEdit = null;
	private IMenuItem[] _menuObject = null;
    static public final String REFRESH = "refresh";
	static final String REFRESH_ALL = "refresh all";
}
