/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import com.netscape.management.client.IPage;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.*;
import netscape.ldap.*;

/**
 *	Netscape Admin Server 4.0 task for stopping the directory server.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.task.CGITask
 */
public class Stop extends CGITask {
	/**
	 *	Constructor for the Stop Directory Task
	 */
	public Stop() {
		super();
		_sCmd = "Tasks/Operation/Stop";
		_section = "Stop";
		setName(DSUtil._resource.getString("dirtask",_section));
		setDescription(DSUtil._resource.getString(
			"dirtask",_section+"-description"));
	}

	/**
	 * Send an http request to the server and then popup a dialog if the
	 * operation is successful.
	 *
	 * @param viewInstance The calling page
	 * @param cmd Command to execute
	 */
	boolean run(IPage viewInstance, String cmd) {
		JFrame frame = viewInstance.getFramework().getJFrame();
		String instanceName = findInstanceName(viewInstance);
		LDAPConnection ldc = _consoleInfo.getLDAPConnection();

		/* Don't bother if the server is already down */		
		boolean status = !DSUtil.reconnect( ldc );
		if ( !status ) {
			boolean confirm = DSUtil.requiresConfirmation(
				GlobalConstants.PREFERENCES_CONFIRM_STOP_SERVER );
			if ( confirm ) {
				int option = DSUtil.showConfirmationDialog(
					                         frame,
											 "confirmStopServer",
											 instanceName,
											 "general");
				if ( option != JOptionPane.YES_OPTION ) {
					return false;
				}
			}
			status = super.run( viewInstance, cmd );
			/* Don't trust the return code - check for really down */
			status = !DSUtil.reconnect( ldc );
			if ( !status ) {
				try {
					ldc.read( "" );
					Debug.println( "Stop.run: server still running" );
				} catch ( LDAPException e ) {
					status = true;
				}
			}
		}
        else {			
            // it's already stopped
			String[] args = { instanceName };
            DSUtil.showInformationDialog( frame,
										  "Already-Stopped", args, "dirtask" );
            return status;
        }
		if ( status ) {
			DSUtil.showInformationDialog( frame, "success",
										  instanceName, "dirtask-Stop");
		} else {			
			DSUtil.showErrorDialog( frame,"failed-title", "failed-msg",
									instanceName, "dirtask-Stop" );
		}
		return status;
	}
}

