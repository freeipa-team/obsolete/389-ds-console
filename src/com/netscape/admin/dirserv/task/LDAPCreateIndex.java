/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.util.Enumeration;
import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.task.LDAPTask;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.GenericProgressDialog;
import netscape.ldap.*;

/**
 *	Netscape Directory Server 5.0 LDAP CreateIndex task
 *
 * @author  jvergara
 * @version %I%, %G%
 * @date	 	02/09/2000
 * @see     com.netscape.admin.client.dirserv.DSAdmin
 * @see     com.netscape.admin.client.dirserv
 */

public class LDAPCreateIndex extends LDAPTask {
	public LDAPCreateIndex(IDSModel model, 
						   Hashtable attributes) {
		super(model, attributes);
		_taskType = INDEX;

		_taskSection = "index";

		start();
	}

	public LDAPCreateIndex(IDSModel model) {		
		this(model, (Hashtable)null);
	}

	public LDAPCreateIndex(IDSModel model, 
						   Hashtable attributes, 
						   GenericProgressDialog progressDialog, 
						   LDAPBasicProgressDialog statusProgressDialog, 
						   boolean isVLVIndex) {
		super(model, attributes);

		_isVLVIndex = isVLVIndex;
		_taskType = INDEX;
		_taskSection = "index";
		_progressDialog = progressDialog;
		((GenericProgressDialog)_progressDialog).addActionListener(this);
		_statusProgressDialog = statusProgressDialog;
		_dontWaitForClose = true;
		if (initTask()) {
			run();
		}
	}


	protected boolean initTask() {
		_taskCancelled = false;

		createTaskDn();
		LDAPAttributeSet attrSet = createTaskEntryAttributes();
		if (attrSet != null)
			_entry = new LDAPEntry(_taskDn, attrSet);
		else {
			Debug.println("LDAPCreateIndex.initTask():  Error, insufficient arguments to perform the task "+_taskType);
			((GenericProgressDialog)_progressDialog).setTextInLabel(DSUtil._resource.getString("general",
																							   "LDAPTaskError-notenoughargs-label"));
			return false;
		} 				
		
		LDAPConnection ldc = getLDAPConnection();  		
		
		Debug.println("DN = " +_entry.getDN());
		Enumeration enumAttrs = attrSet.getAttributes();
		while ( enumAttrs.hasMoreElements() ) {
			LDAPAttribute anAttr = (LDAPAttribute)enumAttrs.nextElement();
			String attrName = anAttr.getName();
			Debug.println( attrName );
			Enumeration enumVals = anAttr.getStringValues();
			
			while ( enumVals.hasMoreElements() ) {
				String aVal = ( String )enumVals.nextElement();	
			}
		}
		
		try {
			Debug.println("LDAPCreateIndex.initTask(): adding entry "+_entry);
			ldc.add(_entry);
		} catch (LDAPException lde) {
			Debug.println ("LDAPCreateIndex.initTask(): LDAP error code = " + lde.getLDAPResultCode() + " error=" + lde);
			String ldapError =  lde.errorCodeToString();
			String ldapMessage = lde.getLDAPErrorMessage();
			
			if ((ldapMessage != null) &&
				(ldapMessage.length() > 0)) {
				ldapError = ldapError + ". "+ldapMessage;
			} 
			String[] args = {ldapError};
			((GenericProgressDialog)_progressDialog).setTextInLabel(DSUtil._resource.getString(
																							   "general",
																							   "LDAPTaskError-starting-label",
																							   args));
			return false;
		}
		return true;
	}
	
	public void run() {
		if (!_dontWaitForClose) {
			((GenericProgressDialog)_progressDialog).enableButtons(true);
		}	
		((GenericProgressDialog)_progressDialog).disableCancelButton();		
		updateProgressDialog();
	}

	protected LDAPAttributeSet createTaskEntryAttributes() {
		LDAPAttributeSet attributeSet = super.createTaskEntryAttributes();
		String value;
		LDAPAttribute attr;

		for( int i = 0; i < MANDATORY_ARGUMENTS.length; i++ ) {
			if ((attr = (LDAPAttribute)_attributes.get(MANDATORY_ARGUMENTS[i])) == null) {
				return null;
			} else {
				attributeSet.add(attr);
			}
		}
		
		for( int i = 0; i < OPTIONAL_ARGUMENTS.length; i++) {
			if ((attr = (LDAPAttribute)_attributes.get(OPTIONAL_ARGUMENTS[i])) != null) {
				attributeSet.add(attr);
			}	
		}
		return attributeSet;
	}

	
	protected void updateProgressDialog(Hashtable updates) {
		String text = (String) updates.get(LDAPTask.TASK_LOG);
				
		if (_isVLVIndex) {
			/* This is the message from the server*/			
			int index = text.lastIndexOf("Indexing VLV:");
			if (index >= 0) {				
				String[] args = {DSUtil.abreviateString(text.substring(index+14), 40)};			
				String labelText = DSUtil._resource.getString("CreateVLVIndex", "LDAPMode-addingVLVIndex-title", args);
				((GenericProgressDialog)_progressDialog).setTextInLabel(labelText);
			}			
		} else {			
			int index = text.lastIndexOf("Indexing attribute:");
			if (index >= 0) {
				String[] args = {DSUtil.abreviateString(text.substring(index+20), 40)};			
				String labelText = DSUtil._resource.getString(_taskSection, "LDAPMode-addingIndex-title", args);
				((GenericProgressDialog)_progressDialog).setTextInLabel(labelText);
			}				
		}	
	
		_statusProgressDialog.update(updates);
	}

	public boolean cancelTask() {
		return super.cancelTask();
	}

	protected void waitForClose() {	
		if (!_dontWaitForClose) {
			((GenericProgressDialog)_progressDialog).waitForClose();
			/* This is necessary because when we call waitForClose the _progressDialog goes to the front */
			if (_statusProgressDialog.isVisible()) {
				_statusProgressDialog.toFront();
			}
		}
		if ((getResult() != TASK_SUCCESSFULLY_COMPLETED) &&
			(getResult() != LDAPTask.TASK_COMPLETED)) {
			((GenericProgressDialog)_progressDialog).setTextInLabel(DSUtil._resource.getString(_taskSection, "LDAPMode-endError-title"));
		} else {
			((GenericProgressDialog)_progressDialog).setTextInLabel(DSUtil._resource.getString(_taskSection, "LDAPMode-finished-title"));
		}
	}	

	public void actionPerformed(ActionEvent e) {
		if (_dontWaitForClose) {
			if ( e.getActionCommand().equals( GenericProgressDialog.CANCEL ) ) {
				Debug.println("LDAPCreateIndex.actionPerformed: CANCEL");				
				cancelTask();								
			}
		} else {
			super.actionPerformed(e);
		}
	}

	private final String INDEX = "index";

	public final static String INSTANCE = "nsInstance";
	public final static String INDEX_ATTRIBUTE = "nsIndexAttribute";
	public final static String INDEX_VLV_ATTRIBUTE = "nsIndexVLVAttribute";
	public String[] MANDATORY_ARGUMENTS = {INSTANCE};
	public String[] OPTIONAL_ARGUMENTS = {INDEX_ATTRIBUTE, INDEX_VLV_ATTRIBUTE};
	boolean _isVLVIndex = false;
	boolean _dontWaitForClose = false;
}
