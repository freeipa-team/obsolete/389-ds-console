/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import com.netscape.management.client.*;
import com.netscape.management.client.TaskObject;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.security.CertificateDialog;
import com.netscape.admin.dirserv.*;
import netscape.ldap.util.DN;

/**
 *	Netscape Directory Server 4.0 task for setting up keys & certificates
 *
 * @author  rweltman
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.DSAdmin
 */
public class KeyCert extends TaskObject {
	/**
	 *	Constructor for the KeyCert Directory Task
	 */
	public KeyCert() {
		super();
		setName(DSUtil._resource.getString("dirtask","KeyCert"));
		setDescription(DSUtil._resource.getString(
			"dirtask","KeyCert-description"));
	}

	/**
	 * Call the frame work class.
	 *
	 * @param viewInstance The calling page
	 */
	public boolean run(IPage viewInstance) {
		ConsoleInfo info = getConsoleInfo();
		Framework f = (Framework) viewInstance.getFramework();
		try {
			f.setBusyCursor(true);

			CertificateDialog dlg = new CertificateDialog(
				viewInstance.getFramework().getJFrame(),
				info,
				(String)info.get("SIE"));
			dlg.pack();
			dlg.setVisible(true);
		}
		finally {
			f.setBusyCursor(false);
		}
		return true;
	}
}

