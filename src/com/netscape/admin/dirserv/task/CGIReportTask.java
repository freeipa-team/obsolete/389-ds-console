/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.net.URL;
import java.util.*;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import com.netscape.management.client.util.AdmTask;
import com.netscape.management.client.comm.CommManager;
import com.netscape.management.client.comm.HttpManager;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.URLByteEncoder;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.IEntryChangeListener;
import com.netscape.admin.dirserv.DSBaseModel;
import com.netscape.admin.dirserv.GlobalConstants;

/**
 *	Netscape Directory Server 4.0 generic task that returns output in a
 *  Vector, and/or calls a listener for each line read from the CGI. The
 *  call is executed in "asynchronous response" mode, unlike AdmTask, so
 *  you can display progress, for example, during the CGI execution.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.task.CGITask
 */
public class CGIReportTask extends AdmTask {
	public CGIReportTask( URL url, ConsoleInfo info, Vector v ) {
		super( url, getAuthenticationDN(info),
			   getAuthenticationPwd(info) );
		_lines = v;
		_url = url;
	}
	public CGIReportTask( URL url, ConsoleInfo info ) {
		this( url, info, null );
	}
    public int exec() {
	    _responseString = new StringBuffer();
		int status = 0;
		try {
			int flags = CommManager.FORCE_BASIC_AUTH|
				CommManager.ASYNC_RESPONSE;
			// Execute the request
			HttpManager httpManager = new HttpManager();
			// tell the http manager to use UTF8 encoding
			httpManager.setSendUTF8(true);
			InputStream data = null;
			Hashtable args = getArguments();
			if (args != null && !args.isEmpty())
				data = encode(args);
			Debug.println("CGIReportTask.exec(): username=" +
						  username(null, null) +
						  " password=" + password(null, null));
			if (data == null)
				httpManager.post(_url, this, null, null, 0,
								 flags);
			else
				httpManager.post(_url, this, null, data, data.available(),
								 flags);
			waitForFinish();
		} catch (Exception e) {
			System.err.println(e);
			status = -1;
		}
		return status;
	}
    public void parse(String s) {
		super.parse( s );
		if ( _lines != null ) {
			_lines.addElement( s );
		} else {
			Debug.println( "<- " + s );
		}
		if ( _listener != null ) {
			if ( !_listener.entryChanged( s ) ) {
				/* In this context, CANCEL is
				   considered success */
				return;
			}
		}
	}
    public void addEntryChangeListener( IEntryChangeListener listener ) {
		_listener = listener;
	}

	static private String getAuthenticationDN(ConsoleInfo info) {
		String dn = (String)info.get(GlobalConstants.TASKS_AUTH_DN);
		if (dn == null)
			dn = info.getAuthenticationDN();
		return dn;
	}

	static private String getAuthenticationPwd(ConsoleInfo info) {
		String pwd = (String)info.get(GlobalConstants.TASKS_AUTH_PWD);
		if (pwd == null)
			pwd = info.getAuthenticationPassword();
		return pwd;
	}

   /**
    * Translates a hashtable into <code>x-www-form-urlencoded</code> format.
	* Values are converted from Unicode to UTF8 before URL encoding.
    *
    * @param   args   <code>Hashtable</code> containing name/value pairs to be translated.
    * @return  a ByteArrayInputStream to the translated <code>Hashtable</code> contents.
    */
   public static ByteArrayInputStream encode(Hashtable args)
   {
      if ((args == null) || (args.size() == 0))
         return (null);

      String      p = "";
      Enumeration e = args.keys();

      while (e.hasMoreElements())
      {
         String name  = (String)e.nextElement();
         String value = URLByteEncoder.encodeUTF8(args.get(name).toString());
         p += URLByteEncoder.encodeUTF8(name) + "=" +
			 value + (e.hasMoreElements()?"&":"");
      }

      return new ByteArrayInputStream(p.getBytes());
   }

    private URL     _url;
	private IEntryChangeListener _listener = null;
    private Vector _lines;
}
