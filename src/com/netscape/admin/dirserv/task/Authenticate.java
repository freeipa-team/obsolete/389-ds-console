/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Vector;
import javax.swing.*;
import com.netscape.management.client.*;
import com.netscape.management.client.TaskObject;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSTaskModel;
import com.netscape.admin.dirserv.GlobalConstants;
import netscape.ldap.LDAPConnection;

/**
 *	Netscape Admin Server 4.0 task for authenticating to the directory server.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.DSAdmin
 */
public class Authenticate extends TaskObject {
	/**
	 *	Constructor for the Authenticate Directory Task
	 */
	public Authenticate() {
		super();
		setName(DSUtil._resource.getString("dirtask","Authenticate"));
		setDescription(DSUtil._resource.getString(
			"dirtask","Authenticate-description"));
	}

	/**
	 * Authenticate and then popup a dialog if the
	 * operation is successful.
	 *
	 * @param viewInstance The calling page
	 */
	public boolean run(IPage viewInstance) {
		JFrame frame = viewInstance.getFramework().getJFrame();
		ConsoleInfo info = getConsoleInfo();

		/* Notify any listeners */
		Vector listeners = null;
		Object o = info.get( DSUtil.AUTH_CHANGE_LISTENERS );
		if ( (o != null) && (o instanceof Vector) )
			listeners = (Vector)o;

		// ldapconnection may be null if we have never established a connection
		// before
		LDAPConnection ldc = info.getLDAPConnection();

        /* save current credentials to use them if auth attempt fails */
        String authDN  = info.getAuthenticationDN();
        String authPwd = info.getAuthenticationPassword ();
        String dn = (String)info.get( "rootdn" );
		ConsoleInfo ci = (ConsoleInfo)info.get( GlobalConstants.CONSOLE_INFO ); 		
		Debug.println(9, "Authenticate.run: listeners=" + listeners);

		DSTaskModel model = (DSTaskModel)info.get( "dstaskmodel" );
		boolean result = false;
		try {
			if (model != null) {
				model.setWaitCursor(true); // this may take a while . . .
			}

			result = DSUtil.reauthenticate(ldc, frame,
										   listeners, dn, null);
		} finally {
			if (result) {
				info.setLDAPConnection( ldc );
			}

			if (model != null) {
				model.setWaitCursor(false);
			}
		}

		return result;
	}
}

