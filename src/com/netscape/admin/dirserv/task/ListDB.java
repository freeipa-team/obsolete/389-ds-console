/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.*;
import java.io.*;
import java.net.*;
import javax.swing.JFrame;
import com.netscape.management.client.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.comm.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;

/**
 *	Netscape Admin Server 4.0 task for getting list of backup
 *  directories on the directory server.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.task.CGITask
 */
public class ListDB extends CGITask {
	/**
	 *	Constructor for the ListBackups Directory Task
	 */
	public ListDB() {
		super();
		_sCmd = "Tasks/Operation/ListBackups";
		setName(DSUtil._resource.getString("dirtask","ListDB"));
		setDescription(DSUtil._resource.getString(
			"dirtask","ListDB-description"));
	}

	/**
	 *	Constructor for the ListBackups Directory Task
	 */
    public ListDB( ConsoleInfo info ) {
		this();
		setConsoleInfo( info );
	}

	public String[] getBackupList() {
		/* What are the known backups? */
		String sAdminURL = _consoleInfo.getAdminURL();
		String fullCmd = sAdminURL + _consoleInfo.get( "ServerInstance" ) +
			"/" + _sCmd;
		String[] dirList = new String[0];
		try {
			URL url = new URL(fullCmd);
			Vector lines = new Vector();
			CGIReportTask task = new CGIReportTask(
				url, getConsoleInfo(), lines );
			int execStatus = task.exec();
			Debug.println( "CGIReportTask(ListBackups) returned " +
						   execStatus +
						   ", " + lines.size() + " values" );
			if ( lines.size() > 0 ) {
				dirList = new String[lines.size()];
				lines.copyInto( dirList );
			}
		} catch ( Exception ex ) {
			Debug.println( "ListDB.getBackupList: " + ex );
		}
		return dirList;
	}
}

