/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Enumeration;
import java.util.Hashtable;
import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.GenericProgressDialog;
import com.netscape.admin.dirserv.task.LDAPTask;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import netscape.ldap.*;

/**
 *	Netscape Directory Server 5.0 LDAP Import task
 *
 * @author  jvergara
 * @version %I%, %G%
 * @date	 	02/09/2000
* @see     com.netscape.admin.client.dirserv.DSAdmin
 * @see     com.netscape.admin.client.dirserv
 */

public class LDAPImport extends LDAPTask {
	public LDAPImport(IDSModel model, Hashtable attributes) {
		super(model, attributes);
		_taskType = IMPORT;
		_taskSection = "import";	   

		LDAPAttribute instanceAttribute = (LDAPAttribute)(_attributes.get(INSTANCE));		
		String[] instanceValues = instanceAttribute.getStringValueArray();
		
		_numberOfInstances = instanceValues.length;
		try {
			createProgressDialog();
			Thread th = new Thread(this);
			th.start();
			((GenericProgressDialog)_progressDialog).packAndShow();
		} catch ( Exception e ) {
			Debug.println("LDAPTask.start(): " +
						  e );
			e.printStackTrace();	
		}
	}

	public LDAPImport(IDSModel model) {
		this(model, (Hashtable)null);
	}

	public void run() {
		LDAPAttribute instanceAttribute = (LDAPAttribute)(_attributes.get(INSTANCE));
		LDAPAttribute importFileAttribute = (LDAPAttribute)(_attributes.get(FILENAME));
		String importFile = importFileAttribute.getStringValueArray()[0];
		String[] instanceValues = instanceAttribute.getStringValueArray();

		for (int i=0; i< instanceValues.length; i++) {			
			_currentInstance = instanceValues[i];
			_currentInstanceNumber = i+1;

			((GenericProgressDialog)_progressDialog).reset();
			String labelText;
			if (_numberOfInstances > 1) {				
				String[] args = {
					DSUtil.inverseAbreviateString(importFile, 40),
					DSUtil.abreviateString(_currentInstance, 30),
					String.valueOf(_currentInstanceNumber),
					String.valueOf(_numberOfInstances)
				};
				labelText = DSUtil._resource.getString(_taskSection, "LDAPMode-progresslabel-title", args);								
			}  else {
				String[] args = {
					DSUtil.inverseAbreviateString(importFile, 40),
					DSUtil.abreviateString(_currentInstance,30)
				};
				labelText = DSUtil._resource.getString(_taskSection, "LDAPMode-progresslabel-one-partition-title", args);
			}
			((GenericProgressDialog)_progressDialog).setTextInLabel(labelText);
		
			instanceAttribute = new LDAPAttribute(INSTANCE, instanceValues[i]);
			_attributes.put(INSTANCE, instanceAttribute);			
			
			if (!initTask()) {
				((GenericProgressDialog)_progressDialog).waitForClose();
				break;
			}
			((GenericProgressDialog)_progressDialog).enableButtons(true);
			updateProgressDialog();
			
			_previousInstanceLogs = _statusProgressDialog.getLogFieldText();

			if (_taskCancelled) {
				break;
			}
		}	
	}

	protected boolean initTask() {
		_taskCancelled = false;

		createTaskDn();
		LDAPAttributeSet attrSet = createTaskEntryAttributes();
		if (attrSet != null)
			_entry = new LDAPEntry(_taskDn, attrSet);
		else {
			Debug.println("LDAPImport.initTask():  Error, insufficient arguments to perform the task "+_taskType);
			((GenericProgressDialog)_progressDialog).setTextInLabel(DSUtil._resource.getString("general",
																							   "LDAPTaskError-notenoughargs-label"));
			return false;
		} 				
		
		LDAPConnection ldc = getLDAPConnection();  		
		
		Debug.println("DN = " +_entry.getDN());
		Enumeration enumAttrs = attrSet.getAttributes();
		while ( enumAttrs.hasMoreElements() ) {
			LDAPAttribute anAttr = (LDAPAttribute)enumAttrs.nextElement();
			String attrName = anAttr.getName();
			Debug.println( attrName );
			Enumeration enumVals = anAttr.getStringValues();
			
			while ( enumVals.hasMoreElements() ) {
				String aVal = ( String )enumVals.nextElement();	
			}
		}
		
		try {
			Debug.println("LDAPImport.initTask(): adding entry "+_entry);
			ldc.add(_entry);
		} catch (LDAPException lde) {
			Debug.println ("LDAPImport.initTask(): LDAP error code = " + lde.getLDAPResultCode() + " error=" + lde);
			String ldapError =  lde.errorCodeToString();
			String ldapMessage = lde.getLDAPErrorMessage();
			
			if ((ldapMessage != null) &&
				(ldapMessage.length() > 0)) {
				ldapError = ldapError + ". "+ldapMessage;
			} 
			String[] args = {ldapError};
			((GenericProgressDialog)_progressDialog).setTextInLabel(DSUtil._resource.getString(
																							   "general",
																							   "LDAPTaskError-starting-label",
																							   args));
			return false;
		}
		return true;
	}


	protected LDAPAttributeSet createTaskEntryAttributes() {
		LDAPAttributeSet attributeSet = super.createTaskEntryAttributes();
		String value;
		LDAPAttribute attr;

		for( int i = 0; i < MANDATORY_ARGUMENTS.length; i++ ) {
			if ((attr = (LDAPAttribute)_attributes.get(MANDATORY_ARGUMENTS[i])) == null) {
				return null;
			} else {
				attributeSet.add(attr);
			}
		}
		
		for( int i = 0; i < OPTIONAL_ARGUMENTS.length; i++) {
			if ((attr = (LDAPAttribute)_attributes.get(OPTIONAL_ARGUMENTS[i])) != null) {
				attributeSet.add(attr);
			}	
		}
		return attributeSet;
	}

	protected void createProgressDialog() {		
		String title = getTitle();
		_progressDialog = new GenericProgressDialog(getModel().getFrame(), 
													true, 
													GenericProgressDialog.CANCEL_AND_LOG_BUTTON_OPTION, 
													title,
													null,
													this);

		((GenericProgressDialog)_progressDialog).enableButtons(false);		
		
		((GenericProgressDialog)_progressDialog).addStep(DSUtil._resource.getString(_taskSection, "LDAPMode-firstStep-title"));
		((GenericProgressDialog)_progressDialog).addStep(DSUtil._resource.getString(_taskSection, "LDAPMode-secondStep-title"));
		((GenericProgressDialog)_progressDialog).addStep(DSUtil._resource.getString(_taskSection, "LDAPMode-thirdStep-title"));
		
		_statusProgressDialog = new LDAPBasicProgressDialog(getModel().getFrame(), 
															 DSUtil._resource.getString(_taskSection, "LDAPMode-Status-title"), 
															 true, 
															 null,
															 this);
				
		_statusProgressDialog.waitForClose();		
	}

	public String getTitle() {
		String title;	
		if (_numberOfInstances == 1) {
			LDAPAttribute instanceAttribute = (LDAPAttribute)(_attributes.get(INSTANCE));		
			String[] instanceValues = instanceAttribute.getStringValueArray();		
			title = DSUtil._resource.getString(_taskSection, "initializebackend-title", instanceValues);
		} else {
			title = DSUtil._resource.getString(_taskSection, "title");
		}
		return title;
	}

	protected void updateProgressDialog(Hashtable htUpdates) {
		String text = (String)htUpdates.get(LDAPTask.TASK_LOG);
		
		/* This is what appears in the server logs (this code has to be in sync with the server's output) */
		if (text.lastIndexOf("Processing file") >= 0) {
			((GenericProgressDialog)_progressDialog).stepCompleted(0);
		}
		if (text.lastIndexOf("Finished scanning file") >= 0) {
			((GenericProgressDialog)_progressDialog).stepCompleted(1);
		}
		if (text.lastIndexOf("Import complete") >= 0) {
			((GenericProgressDialog)_progressDialog).stepCompleted(2);
		}
		text = _previousInstanceLogs + "\n\n\n" + text;
		htUpdates.put(LDAPTask.TASK_LOG, text);

		_statusProgressDialog.update(htUpdates);
	}


	protected void finalizeTask(int result) {
		Debug.println("LDAPImport.finalizeTask result "+result);		
		
		switch (result) {
		case LDAPTask.INSUFFICIENT_ACCESS_RIGHTS_ERROR:
			_taskCancelled = true;
			break;
		case LDAPTask.SERVER_UNAVAILABLE_ERROR:
			_taskCancelled = true;
			break;
		case LDAPTask.SERVER_UPDATING_ERROR:
			_taskCancelled = true;
			break;
		case LDAPTask.TASK_COMPLETED:				
			break;
		case LDAPTask.TASK_SUCCESSFULLY_COMPLETED:
			((GenericProgressDialog)_progressDialog).stepCompleted(0);
			((GenericProgressDialog)_progressDialog).stepCompleted(1);
			((GenericProgressDialog)_progressDialog).stepCompleted(2);
			break;
		case LDAPTask.TASK_UNSUCCESSFULLY_COMPLETED:
			if (_currentInstanceNumber == _numberOfInstances) {
				String labelText = DSUtil._resource.getString("general", "LDAPTaskCompleted-unsuccessful-label");
				((GenericProgressDialog)_progressDialog).setTextInLabel(labelText);
			} else {
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							int wantToContinue = DSUtil.showConfirmationDialog( _progressDialog,
																				"LDAPMode-unsuccessful-wantToCancel",
																				_currentInstance,
																				_taskSection);
							if (wantToContinue != JOptionPane.YES_OPTION) {
								_taskCancelled = true;
							}
						}
					});
				} catch (Exception e) {
				}
				if (!_taskCancelled) {
					waitForClose();
				}					
			}
			break;
		}
		_result = result;
		waitForClose();
	}


	private final String IMPORT = "import";

	protected int _currentInstanceNumber;
	protected int _numberOfInstances;
	protected String _currentInstance;
	protected String _previousInstanceLogs = "";

	public String[] MANDATORY_ARGUMENTS = {FILENAME, INSTANCE};
	public String[] OPTIONAL_ARGUMENTS = {INCLUDE_SUFFIX, EXCLUDE_SUFFIX, INDEX_ATTRIBUTES};
	public final static String FILENAME = "nsFilename";
	public final static String INSTANCE = "nsInstance";
	public final static String INCLUDE_SUFFIX = "nsIncludeSuffix";
	public final static String EXCLUDE_SUFFIX = "nsExcludeSuffix";
	public final static String INDEX_ATTRIBUTES = "nsImportIndexAttrs";

	/* The following attributes are not used (for the moment) */
	public final static String CHUNK_SIZE = "nsImportChunkSize";
	public final static String UNIQUE_ID_GENERATOR = "nsUniqueIdGenerator";
	public final static String UNIQUE_ID_GENERATOR_NAMESPACE = "nsUniqueIdGeneratorNamespace";
}
