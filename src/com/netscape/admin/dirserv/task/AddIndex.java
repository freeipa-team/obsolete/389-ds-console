/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.awt.Component;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.ExportImportProgressDialog;
import com.netscape.admin.dirserv.GenericProgressDialog;
import netscape.ldap.*;

/**
 *	Netscape Directory Server 4.0 task for adding one or more indexes
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	03/09/98
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.task.CGIReportTask
 */
public class AddIndex extends CGITask {
	/**
	 *	Constructor for the AddIndex Directory Task
	 */
	public AddIndex( ConsoleInfo info ) {
		_consoleInfo = info;
		_sCmd = "Tasks/Operation/AddIndex";
		_section = "AddIndex";
		setName(DSUtil._resource.getString(_section,"title"));
	}

	/**
	 * Create one or more indexes
	 *
	 * @param attrs Space-delimited list of attributes to index
	 * @return true if the indexing succeeded.
	 */
    public boolean exec( String attrs, String backendName, Component comp) {
		return this.exec(attrs, backendName, comp, DN_PREFIX);
	}

    public boolean exec( String attrs, String backendName, Component comp, String dn ) {
		IDSModel model =
			(IDSModel)_consoleInfo.get( "dsresmodel" );
		boolean status = true;
		/* Get read-only state of database */
		boolean wasReadOnly = false;
		/* Get read-only state of database */
		try {
			wasReadOnly = ReadOnly.isReadOnly( _consoleInfo, dn );
		} catch ( LDAPException e ) {
			status = false;
		}
		if ( status && !wasReadOnly ) {
			/* Make server read-only */
			status = ReadOnly.setReadOnly( _consoleInfo, true, dn );
		}
		if ( status ) {
			Hashtable args = new Hashtable();
			args.put("attributes", attrs);
			args.put("backendID", backendName);
			String fullCmd = _consoleInfo.getAdminURL() +
				_consoleInfo.get( "ServerInstance" ) +
				"/" + _sCmd;
			Debug.println(" ********** AddIndex.exec fullCmd =" + fullCmd );
			CGIThread task = new CGIThread( fullCmd, args, model, _section );
			String title = _resource.getString("dirtask", _section+"-title");
			JFrame frame = model.getFrame();
			ExportImportProgressDialog dlg =
				new ExportImportProgressDialog(
					frame, task, title,
					false, comp );
			task.addEntryChangeListener( dlg );
			dlg.setModal( true );
			/* Let the dialog run the thread */			
			if (SwingUtilities.isEventDispatchThread()) {
				dlg.setVisible( true );
			} else {
				final ExportImportProgressDialog fDlg = dlg;
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							fDlg.setVisible(true);
						}
					});
				} catch (Exception ex) {
					ex.printStackTrace();
				}		
			}
			status = task.getStatus();
			showResultDialog( task );
			if ( !wasReadOnly ) {
				ReadOnly.setReadOnly( _consoleInfo, false, dn );
			}
		}
		return status;
	}

   public boolean exec( String[] attrs,
						String backendName,
						Component comp,
						String dn,
						GenericProgressDialog progressDialog,
						LDAPBasicProgressDialog statusProgressDialog) {
		IDSModel model =
			(IDSModel)_consoleInfo.get( "dsresmodel" );
		boolean status = true;
		/* Get read-only state of database */
		boolean wasReadOnly = false;
		/* Get read-only state of database */
		try {
			wasReadOnly = ReadOnly.isReadOnly( _consoleInfo, dn );
		} catch ( LDAPException e ) {
			status = false;
		}
		if ( status && !wasReadOnly ) {
			/* Make server read-only */
			status = ReadOnly.setReadOnly( _consoleInfo, true, dn );
		}
		if ( status ) {
			Hashtable ldapEntryAttributes = new Hashtable();
			
			ldapEntryAttributes.put(LDAPCreateIndex.INSTANCE, new LDAPAttribute(LDAPCreateIndex.INSTANCE, backendName));
			ldapEntryAttributes.put(LDAPCreateIndex.INDEX_VLV_ATTRIBUTE, new LDAPAttribute(LDAPCreateIndex.INDEX_ATTRIBUTE, attrs));
			
			/* In task we complete the updating of the progress dialog*/
			LDAPCreateIndex task = new LDAPCreateIndex(model,
													   ldapEntryAttributes,
													   progressDialog,
													   statusProgressDialog,
													   false);
			if ((task.getResult() != LDAPTask.TASK_SUCCESSFULLY_COMPLETED) &&
				(task.getResult() != LDAPTask.TASK_COMPLETED)) {
				
				status = false;
			}
		
			if ( !wasReadOnly ) {
				ReadOnly.setReadOnly( _consoleInfo, false, dn );
			}
		}
		return status;
   }

	static final private String DN_PREFIX = "cn=config,cn=ldbm database,cn=plugins,cn=config";
	private String _dnIndex = null;
}
