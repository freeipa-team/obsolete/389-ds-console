/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.io.*;
import java.net.URL;
import javax.swing.JFrame;
import netscape.ldap.*;
import com.netscape.management.client.TaskPage;
import com.netscape.management.client.TaskObject;
import com.netscape.management.client.ResourcePage;
import com.netscape.management.client.IPage;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.topology.ServerNode;
import com.netscape.management.client.comm.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.DSAdmin;
import com.netscape.admin.dirserv.DSFramework;
import com.netscape.admin.dirserv.DSBaseModel;
import com.netscape.admin.dirserv.DSTaskModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.GlobalConstants;

/**
 *	Netscape Directory Server 4.0 CGI base task
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.client.dirserv.DSAdmin
 * @see     com.netscape.admin.client.dirserv
 */
public class CGITask extends TaskObject implements CommClient {
    public CGITask() {
	}

	/**
	 * Send an http request to the server and then popup a dialog if the
	 * operation is successful.
	 *
	 * @param viewInstance The calling page
	 */
	public boolean run(IPage viewInstance) {
//		if ( _sCmd == null )
//			_sCmd = getCommand();
		if ( _sCmd == null ) {
			Debug.println( "Could not get execref for " + getDN() );
			return false;
		}

		return run( viewInstance, _sCmd );
	}

	/**
	 * Send an http request to the server. Return true if we're sure it
	 * succeeded, otherwise false.
	 *
	 * @param viewInstance The calling page
	 * @param cmd Command to execute
	 */
	boolean run(IPage viewInstance, String cmd) {
		/* Kludge for 3.5 Admin server, which needs the execref rather
		   than the entry class */
		if ( _consoleInfo.get( "admin35" ) != null ) {
			cmd = getCommand();
		}

		// get the admin URL location first
		_sAdminURL = _consoleInfo.getAdminURL();
		if ( _sAdminURL == null ) {
			Debug.println( "Could not get adminURL for " + getDN() );
			return false;
		}
		// Allow specifying e.g. "slapd-install" for instance
		String instance = (String)_consoleInfo.get( cmd );
		if ( instance == null )
			instance = (String)_consoleInfo.get( "ServerInstance" );
		String fullCmd = _sAdminURL + instance + "/" + cmd;

		DSTaskModel model = (DSTaskModel)_consoleInfo.get( "dstaskmodel" );
		if ( model != null ) {
			model.setWaitCursor( true );
		} else {
			Debug.println( "CGITask.run: no dstaskmodel in ConsoleInfo" );
		}

		DSBaseModel bmodel = (DSBaseModel)_consoleInfo.get( "dsresmodel" );
		if ( bmodel != null ) {
			bmodel.setWaitCursor( true );
		} else {
			Debug.println( "CGITask.run: no dsresmodel in ConsoleInfo" );
		}

		try {
			_errorCode = 0;
			_success = false;
			_fFinished = false;

			// _consoleInfo.get("arguments") is a hashtable of key/value pairs
			// to use as the arguments to the CGI
			Hashtable args = (Hashtable)_consoleInfo.get("arguments");
			ByteArrayInputStream data = null;
			if (args != null && !args.isEmpty())
				data = CGIReportTask.encode(args);
			Debug.println( "Posting " + fullCmd );
			// tell the http manager to notify us immediately of replies
			// if we're using async mode
			int flags = CommManager.FORCE_BASIC_AUTH;
			if (_async)
				flags |= CommManager.ASYNC_RESPONSE;
			HttpManager httpManager = new HttpManager();
			// For DS CGI we use a 2 minutes response timeout
			httpManager.setResponseTimeout(2 * 60 * 1000); 
			Debug.println("CGITask.run(): response timeout is " 
			               + httpManager.getResponseTimeout() + " ms");
			// tell the http manager to use UTF8 encoding
			httpManager.setSendUTF8(true);

			Debug.println("CGITask.run(): before post this=" + this + " cmd="
						  + fullCmd);
			if (data == null)
				httpManager.post(new URL(fullCmd), this, null, null, 0,
								 flags);
			else
				httpManager.post(new URL(fullCmd), this, null, data, data.available(),
								 flags);
			Debug.println("CGITask.run(): after post this=" + this + " cmd="
						  + fullCmd);
			awaitSuccess();
			Debug.println( "Command executed: " + fullCmd );
		} catch (Exception e)
		{
			if ( e instanceof java.net.ConnectException ) {
				DSUtil.showErrorDialog( null, "admin-down", "",
										"dirtask" );
			}
			/* Each task shows a result dialog, so no need to do anything
			   more here */
			Debug.println( "Command " + fullCmd  + " failed: " + e );
		}
		if ( model != null ) {
			model.setWaitCursor( false );
		}
		if ( bmodel != null ) {
			bmodel.setWaitCursor( false );
		}
		return _success;
	}

	/**
	 *	pass the username to the admin server
	 */
	public String username(Object authObject, CommRecord cr) {
//		Debug.println( "username = " +
//					   (String)_consoleInfo.get( GlobalConstants.TASKS_AUTH_DN ) );
		return (String)_consoleInfo.get( GlobalConstants.TASKS_AUTH_DN );
	}

	/**
	 *	pass the user password to the admin server
	 */
	public String password(Object authObject, CommRecord cr) {
//		Debug.println( "password = " +
//					   (String)_consoleInfo.get( GlobalConstants.TASKS_AUTH_PWD ) );
		return (String)_consoleInfo.get( GlobalConstants.TASKS_AUTH_PWD );
	}

	/**
	 *	waiting for the http transaction to be finished.
	 */
   public synchronized void awaitSuccess() {
      while (!_fFinished) {
		  try {wait();}
		  catch (Exception e) { }
      }
   }

	/**
	 *	http transaction finished, notify the process
	 */
    public synchronized void finish() {
		_fFinished = true;
		Debug.println("CGITask.finish(): before notifyAll this=" + this + " cmd=" + _sCmd + " adminURL=" + _sAdminURL);
		notifyAll();
		Debug.println("CGITask.finish(): after notifyAll this=" + this + " cmd=" + _sCmd + " adminURL=" + _sAdminURL);
	}

	/**
	 *	the operation is finished after we receive the http stream
	 */
    public void replyHandler(InputStream response, CommRecord cr) {
		Debug.println("CGITask.replyHandler(): BEGIN this=" + this + " cr=" + cr);
		try {
			BufferedReader rspStream =
				new BufferedReader(new InputStreamReader(response, "UTF8"));

			boolean done=false;
			while (!done && (_reply = rspStream.readLine()) != null ) {
				Debug.println("CGITask.replyHandler: read [" + _reply + "]");
				if ( _reply.startsWith("NMC_Status:") ||
					 _reply.startsWith("NMC_STATUS:") ) {
					int index = _reply.indexOf(":");
					String sValue = _reply.substring(index+1).trim();
					_errorCode = Integer.parseInt(sValue);
					_success = (_errorCode == 0);
					done = true;
				}
			}
		} catch ( Exception e ) {
			Debug.println( "CGITask.replyHandler: " + e.toString() );
		}
		Debug.println("CGITask.replyHandler(): END this=" + this + " cr=" + cr);
		finish();
	}

	/**
	 *	this function will be called if error occurs
	 */
    public void errorHandler(Exception error, CommRecord cr) {
		Debug.println("CGITask.errorHandler: " + error );
		if ( error instanceof InterruptedIOException ) {
			// The response timeout has elapsed.
			// Either the CGI crashed either the value
			// httpManager.getResponseTimeout() is not big enough.
			Debug.println(0, "CGITask.errorHandler: response timeout has elapsed");
			Debug.println(0, "CGITask.errorHandler: it might be not big enough");
		}
		finish();
	}


    public String getDN() {
		return _consoleInfo.getCurrentDN();
	}

    public String getReply() {
		return _reply;
	}

	/**
	 *	Return the command, which should have been stored in the info.
	 */
	private String getCommand() {
		String s = (String)_consoleInfo.get( "execref" );
		if ( s != null )
			return "bin/" + s;
		return null;
	}

    protected void showDialog( JFrame frame, String msg, String item,
							 boolean error  ) {
		// display a message
		if ( error ) {
			DSUtil.showErrorDialog( frame, msg, item, "dirtask" );
		} else {
			DSUtil.showInformationDialog( frame, msg, item, "dirtask" );
		}
	}

    protected void showResultDialog( boolean success ) {
		// popup a dialog
		if ( success ) {
			showDialog( null, _section+"-success", "",
						false );
		} else {
			showDialog( null, _section+"-failed", "",
						true );
		}
	}

    protected void showResultDialog( int errorCode, String arg ) {
		// popup a dialog
		String error = "error-" + Integer.toString( errorCode ) + "-msg";
		String title = _section + "-failed-title";
		DSUtil.showErrorDialog( null,
								title,
								error,
								arg, "dirtask" );
	}

    protected void showResultDialog( CGIThread thread ) {
		CGIReportTask task = thread.getTask();
		if ( task.getStatus() != 0 ) {
			showResultDialog( task.getStatus(),
							  (String)task.getResult("NMC_ErrInfo") );
		} else {
			showResultDialog( task.getStatus() == 0 );
		}
	}

	protected void setAsync(boolean val) {
		_async = val;
	}


	/**
	 * Find the server object to which this CGITask applies.
	 * A CGITask may be run from the directory window (task tab)
	 * or the topology window. In the first case, viewInstance
	 * is a TaskPage. In the second case, viewInstance is
	 * TopologyResourcePage (but this is private to console sdk
	 * so we simply test on ResourcePage).
	 */
	protected DSAdmin findServerObject(IPage viewInstance) {
		DSAdmin result = null;
	
		if (viewInstance instanceof TaskPage) {
			DSFramework fmk = (DSFramework)viewInstance.getFramework();
			result = fmk.getServerObject();
		}
		else if (viewInstance instanceof ResourcePage) {
			// In that case, there is one selected object in the topology page.
			// This selected object is a ServerNode. The IServerObject attached
			// to this node is *our DSAdmin*.
			ResourcePage topoInstance = (ResourcePage)viewInstance;
			ServerNode serverNode = (ServerNode)topoInstance.getSelection()[0];
			result = (DSAdmin)serverNode.getServerObject();
		}
		
		if (result == null) {
			// It's really unlikely to happen.
			// It would means our CGITask objects are also included
			// in a container page which is neither a TaskPage or a ResourcePage.
			Debug.println(0, "CGITask.findServerObject: cannot find server object from " 
			                 + viewInstance);
		}
		
		return result;
	}


	/**
	 * Determines if security is enabled on the current server.
	 * A simple encapsulation which finds the server object 
	 * and get the securityState property from it.
	 */
	protected boolean isSSLEnabled(IPage viewInstance) {
		DSAdmin serverObj = findServerObject(viewInstance);
		int securityState;
		try {
			if (serverObj != null)
				securityState = serverObj.getSecurityState();
			else
				securityState = DSAdmin.SECURITY_DISABLE;
		}
		catch(LDAPException x) {
			Debug.println(0, "CGITask.isSSLEnabled: can't read nsslapd-security");
			Debug.println(0, "CGITask.isSSLEnabled: considering SSL as disabled");
			securityState = DSAdmin.SECURITY_DISABLE;
		}
		return (securityState == DSAdmin.SECURITY_ENABLE);
	}


	/**
	 * Returns the name of the current server.
	 */
	protected String findInstanceName(IPage viewInstance) {
		DSAdmin serverObj = findServerObject(viewInstance);
		String result;
		
		if (serverObj != null) {
			result = serverObj.getInstanceName();
		}
		else {
			Debug.println(0, "CGITask.findInstanceName: can't find name from " +
			                 viewInstance);
			result = "no-name";
		}
		
		return result;
	}

	public String toString() {
		return "CGITask cmd=" + _sCmd + " adminURL=" + _sAdminURL + " async=" + _async;
	}

	private boolean _fFinished = false;
	protected String _sCmd = null;
	private String _sAdminURL = null;
	protected boolean _success = false;
	protected int _errorCode = 0;
	private String _reply = null;
	protected String _section = "";
	private boolean _async = true;
	static final ResourceSet _resource = DSUtil._resource;
}
