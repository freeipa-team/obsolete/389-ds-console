/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/**
 *	Netscape Admin Server 4.0 Directory Server restart task.
 *
 * @author  prasanta
 * @version %I%, %G%
 * @date	 2/15/98
 * @see     com.netscape.admin.dirserv
 * @see     com.netscape.admin.dirserv.task.CGITask
 */
package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import com.netscape.management.client.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.util.Debug;


public class Remove extends CGITask
{
	/**
	 * send an http request to the server
	 * @param viewInstance The calling page
	 * @param instance Instance to remove
	 */
	boolean remove( IPage viewInstance, String instance) {
		Hashtable args = new Hashtable();
		args.put("InstanceName", instance);
		_consoleInfo.put("arguments", args);
		return super.run ( viewInstance, "Tasks/Operation/Remove");
	}

	/**
	 * @param viewInstance The calling page
	 */
	public boolean run(IPage viewInstance) {
		Debug.println("Remove.run(): this=" + this);
		/* Check if the server is already down */
		boolean status = DSUtil.reconnect( _consoleInfo.getLDAPConnection() );

		/* If not, stop it */
		if ( !status ) {
			_consoleInfo.remove("arguments");
			run( viewInstance, "Tasks/Operation/Stop" );
			status = !DSUtil.reconnect( _consoleInfo.getLDAPConnection() );
		}
		status = remove (null, (String) _consoleInfo.get( "ServerInstance") );
		return status;
	}
}
