/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.util.Enumeration;
import java.lang.System;
import java.io.*;
import java.net.URL;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import java.awt.event.*;
import java.awt.Component;
import com.netscape.management.client.TaskObject;
import com.netscape.management.client.IPage;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.comm.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.DSBaseModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.GlobalConstants;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.GenericProgressDialog;
import netscape.ldap.*;

/**
 *	Netscape Directory Server 5.0 LDAP base task
 *
 * @author  jvergara
 * @version %I%, %G%
 * @date	 	02/09/2000
 */
public class LDAPTask extends TaskObject implements ActionListener, Runnable {
    public LDAPTask(IDSModel model) {
		_model = model; 
	}

	public LDAPTask(IDSModel model, Hashtable attributes) {
		this(model);
		_attributes = attributes;
	}

	protected void start() {
		try {
			createProgressDialog();
			if (!initTask()) {
				return;
			}
			Thread th = new Thread(this);
			th.start();
			((GenericProgressDialog)_progressDialog).packAndShow();
		} catch ( Exception e ) {
			Debug.println("LDAPTask.start(): " +
						  e );
			e.printStackTrace();	
		}
	}

	protected String getTaskCn() {
		return _taskCn;
	}

	protected boolean initTask() {
		_taskCancelled = false;

		createTaskDn();
		LDAPAttributeSet attrSet = createTaskEntryAttributes();
		if (attrSet != null)
			_entry = new LDAPEntry(_taskDn, attrSet);
		else {
			Debug.println("LDAPTask.initTask():  Error, insufficient arguments to perform the task "+_taskType);
			return false;
		} 				
	
		LDAPConnection ldc = getLDAPConnection();  		

		Debug.println("DN = " +_entry.getDN());
		Enumeration enumAttrs = attrSet.getAttributes();
		while ( enumAttrs.hasMoreElements() ) {
			LDAPAttribute anAttr = (LDAPAttribute)enumAttrs.nextElement();
			String attrName = anAttr.getName();
			Debug.println( attrName );
			Enumeration enumVals = anAttr.getStringValues();
			
			while ( enumVals.hasMoreElements() ) {
				String aVal = ( String )enumVals.nextElement();	
			}
		}
		
		try {
			Debug.println("LDAPTask.initTask(): adding entry "+_entry);
			ldc.add(_entry);
		} catch (LDAPException lde) {
			Debug.println ("LDAPTask.initTask(): LDAP error code = " + lde.getLDAPResultCode() + " error=" + lde);
			Component comp;
			if (_progressDialog != null) {
				comp = _progressDialog;
			} else {
				comp = getModel().getFrame();
			}					
				
			LDAPErrorMessageDisplayer displayer = new LDAPErrorMessageDisplayer(lde, ldc, _progressDialog);
			displayer.run();
									
			return false;
		}
		return true;
	}
	
	protected void createProgressDialog() {
		String title = DSUtil._resource.getString(_section, _taskType+"-label");
		_progressDialog = new LDAPTaskProgressDialog(getModel().getFrame(), title, true, null, this);
	}

	public void run() {		
		((GenericProgressDialog)_progressDialog).enableButtons(true);
		updateProgressDialog();
	}

	/* To be overwritten by the superclasses */
	protected LDAPAttributeSet createTaskEntryAttributes() {
		LDAPAttributeSet attributeSet = new LDAPAttributeSet();
		LDAPAttribute ldapAttribute = new LDAPAttribute(OBJECTCLASS, OBJECTCLASS_VALUE);
		attributeSet.add(ldapAttribute);
		
		ldapAttribute = new LDAPAttribute(CN, getTaskCn());
		attributeSet.add(ldapAttribute);
	
		ldapAttribute = new LDAPAttribute(TASK_TTL, _ttl+"");
		attributeSet.add(ldapAttribute);
	     	
		
		return attributeSet;

	}


	public void updateProgressDialog() {
		long time1 = System.currentTimeMillis();
		long time2;
		int result;
		while ((result = testTaskEntry()) == TASK_IN_PROGRESS) {
			if (_taskCancelled) {
				return;
			}
			time2 = System.currentTimeMillis();
			if ( (time2 - time1) > (1000 *_ttl)/TTL_RATIO) {
				Debug.println("LDAPTask.updateProgressDialog(): ttl time is too low to ensure proper update of progress dialog");
			} else {
				try {
					Thread.sleep(time1 - time2 + ((1000 * _ttl) / TTL_RATIO));					
				} catch (Exception e) {
					Debug.println("LDAPTask.updateProgressDialog: " + e);
				}
			}
			time1 = System.currentTimeMillis();
		}
		if (!_taskCancelled) {
			finalizeTask(result);
		}
	}
	
	protected void finalizeTask(int result) {	
		Debug.println("LDAPTask.finalizeTask");
		

		switch (result) {
		case LDAPTask.INSUFFICIENT_ACCESS_RIGHTS_ERROR:		  			
			break;
		case LDAPTask.SERVER_UNAVAILABLE_ERROR: 					
			break;
		case LDAPTask.SERVER_UPDATING_ERROR: 				
			break;
		case LDAPTask.TASK_COMPLETED:				
			break;
		case LDAPTask.TASK_SUCCESSFULLY_COMPLETED:				
			break;
		case LDAPTask.TASK_UNSUCCESSFULLY_COMPLETED:
			String labelText = DSUtil._resource.getString("general", "LDAPTaskCompleted-unsuccessful-label");
			((GenericProgressDialog)_progressDialog).setTextInLabel(labelText);			
			break;
			}
		
		_result = result;
		waitForClose();
	}
		
	protected void waitForClose() {		
		((GenericProgressDialog)_progressDialog).waitForClose();
		/* This is necessary because when we call waitForClose the _progressDialog goes to the front */
		if (_statusProgressDialog.isVisible()) {
			_statusProgressDialog.toFront();
		}
		if ((getResult() != TASK_SUCCESSFULLY_COMPLETED) &&
			(getResult() != LDAPTask.TASK_COMPLETED)) {
			((GenericProgressDialog)_progressDialog).setTextInLabel(
																	DSUtil._resource.getString(
																							   _taskSection,
																							   "LDAPMode-endError-title"));
		} else {
			((GenericProgressDialog)_progressDialog).setTextInLabel(
																	DSUtil._resource.getString(
																							   _taskSection,
																							   "LDAPMode-finished-title"));
		}
	}	

	protected int testTaskEntry() {
		Debug.println("LDAPTask.testTaskEntry");
		LDAPConnection ldc = getLDAPConnection();
		LDAPEntry entry;
		try {
			entry = ldc.read(_taskDn);
		} catch (LDAPException lde) {
			Debug.println ("LDAPTask.testTaskEntry(): LDAP error code = " + lde.getLDAPResultCode() + " error=" + lde);  		  
			if (lde.getLDAPResultCode() == lde.INSUFFICIENT_ACCESS_RIGHTS){
				((GenericProgressDialog)_progressDialog).setTextInLabel(
																		DSUtil._resource.getString(
																								   "authenticate",
																								   "101-msg", 
																								   ldc.getAuthenticationDN()));
				return INSUFFICIENT_ACCESS_RIGHTS_ERROR;
			} else if ( lde.getLDAPResultCode() == lde.UNAVAILABLE ) {
				((GenericProgressDialog)_progressDialog).setTextInLabel(
																		DSUtil._resource.getString(
																								   "general",
																								   "updating-server-unavailable-msg"));				
				return SERVER_UNAVAILABLE_ERROR;
			} else if (lde.getLDAPResultCode() == lde.NO_SUCH_OBJECT) {
				return TASK_COMPLETED;
			} else {
				String ldapError =  lde.errorCodeToString();
				String ldapMessage = lde.getLDAPErrorMessage();
				
				if ((ldapMessage != null) &&
					(ldapMessage.length() > 0)) {
					ldapError = ldapError + ". "+ldapMessage;
				} 
				String[] args = {ldapError};
				
				String msg = DSUtil._resource.getString("general", "LDAPTaskError-serverReadingError-label", args);
				((GenericProgressDialog)_progressDialog).setTextInLabel(msg);
				return SERVER_UPDATING_ERROR;
			}
		}
		
		if (entry != null) {
			_entry = entry;
		}
		
		Hashtable htLDAPEntry = new Hashtable();
		htLDAPEntry.put(TASK_STATUS, getAttrVal(TASK_STATUS));	
		htLDAPEntry.put( TASK_LOG, getAttrVal(TASK_LOG));		
		htLDAPEntry.put( TASK_CURRENT_ITEM, getAttrVal(TASK_CURRENT_ITEM));	
		htLDAPEntry.put( TASK_TOTAL_ITEMS, getAttrVal(TASK_TOTAL_ITEMS));				

		updateProgressDialog(htLDAPEntry);
		
		if (((getAttrVal(TASK_CURRENT_ITEM).trim().equals(getAttrVal(TASK_TOTAL_ITEMS).trim())) &&  !getAttrVal(TASK_CURRENT_ITEM).equals("")) ||
			(!getAttrVal(TASK_EXIT_CODE).trim().equals(""))) {
			if (getAttrVal(TASK_EXIT_CODE).trim().equals(SUCCESSFUL_TASK)) {
				return TASK_SUCCESSFULLY_COMPLETED;
			} else {
				return TASK_UNSUCCESSFULLY_COMPLETED;
			}
		} else {
			return TASK_IN_PROGRESS;
		}
	}

	protected void updateProgressDialog(Hashtable updates) {
		if (_progressDialog instanceof LDAPTaskProgressDialog) {
			((LDAPTaskProgressDialog)_progressDialog).update(updates);
		}
	}		

	public boolean cancelTask() {
		Debug.println("LDAPTask.cancelTask()");
		LDAPConnection ldc = getLDAPConnection();
		
		LDAPAttribute attr = new LDAPAttribute(TASK_CANCEL, "TRUE");
		LDAPModification mod = new LDAPModification(LDAPModification.REPLACE, attr);

		try {
			ldc.modify(_entry.getDN(), mod);
		} catch (LDAPException lde) {
			Debug.println ("LDAPTask.cancelTask(): LDAP error code = " + lde.getLDAPResultCode() + " error=" + lde);
			/*We are in the event thread we use directly the class LDAPErrorMessageDisplayer */
			LDAPErrorMessageDisplayer dlg = new LDAPErrorMessageDisplayer(lde, ldc, _progressDialog);
			dlg.run();				
			return true;
		}
		return true;
	}				 
			

	/* We use the current time to generate unique cn for the task */
	protected void createTaskDn() {
		_taskCn = _taskType+System.currentTimeMillis();
		_taskDn = "cn="+_taskCn+", cn="+_taskType+", "+TASK_ROOT;
	}

	protected IDSModel getModel() { 
		return _model;
	}

	protected LDAPConnection getLDAPConnection() {
		return getModel().getServerInfo().getLDAPConnection();
	}

	private String getAttrVal( String attrName ) {
		if ( _entry != null ) {
			LDAPAttribute attr = _entry.getAttribute( attrName );
			if ( attr != null ) {
				Enumeration en = attr.getStringValues();
				if ( (en != null) && en.hasMoreElements() )
					return (String)en.nextElement();
			}
		}
		return "";
    }

	public int getResult() {
		return _result;
	}

	/**
	 *	Handle incoming event from button.
	 *
	 * @param e event
	 */
	public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( GenericProgressDialog.CANCEL ) ) {
			Debug.println("LDAPTask.actionPerformed: CANCEL");
			((GenericProgressDialog)_progressDialog).disableCancelButton();			
			((GenericProgressDialog)_progressDialog).setTextInLabel(DSUtil._resource.getString(_taskSection, "LDAPMode-cancelled-title"));
			_taskCancelled = true;
			cancelTask();
			_statusProgressDialog.hide();
			((GenericProgressDialog)_progressDialog).closeCallBack();					
		} else if ( e.getActionCommand().equals( GenericProgressDialog.CLOSE ) ) {			
			Debug.println("LDAPTask.actionPerformed: CLOSE");
			_statusProgressDialog.hide();
			((GenericProgressDialog)_progressDialog).closeCallBack();	
		} else if (	e.getActionCommand().equals( GenericProgressDialog.SHOW_LOGS)) {
			Debug.println("LDAPTask.actionPerformed: SHOW_LOGS");
			
			_statusProgressDialog.pack();
			_statusProgressDialog.setLocationRelativeTo((Component)_progressDialog);
			_statusProgressDialog.setLocation(_progressDialog.getWidth(), _progressDialog.getHeight());
			_statusProgressDialog.show();
		} else if ( e.getActionCommand().equals( LDAPTask.CLOSE ) ) {
			_statusProgressDialog.hide();
			_statusProgressDialog.dispose();			
		}
	}
   	
	public static final int INSUFFICIENT_ACCESS_RIGHTS_ERROR = 0;
	public static final int SERVER_UNAVAILABLE_ERROR = 1;
	public static final int SERVER_UPDATING_ERROR = 2;
	public static final int TASK_COMPLETED = 3;
	public static final int TASK_SUCCESSFULLY_COMPLETED = 4;
	public static final int TASK_UNSUCCESSFULLY_COMPLETED = 5;
	public static final int TASK_IN_PROGRESS = 6;

	public static final String CANCEL = "Cancel";
	public static final String CLOSE = "Close";
	
	
	protected int _result = TASK_IN_PROGRESS;
	protected final int TTL_RATIO = 4;

	protected String _taskCn = "";
	protected String _taskType = "";
	protected String _taskDn = "";
	protected IDSModel _model;
	protected int _ttl = 4; // I take this value by default
	protected LDAPEntry _entry;
	protected Hashtable _attributes;
	protected LDAPTaskProgressDialog _statusProgressDialog;

	protected JDialog _progressDialog;

	public static final String TASK_ROOT = "cn=tasks, cn=config";
	public static final String OBJECTCLASS = "objectclass";
	public static final String[] OBJECTCLASS_VALUE = {"top", "extensibleObject"};
	public static final String CN = "cn";
	public static final String TASK_STATUS = "nsTaskStatus";
	public static final String TASK_LOG = "nsTaskLog";
	public static final String TASK_EXIT_CODE = "nsTaskExitCode";
	public static final String TASK_CURRENT_ITEM = "nsTaskCurrentItem";
	public static final String TASK_TOTAL_ITEMS = "nsTaskTotalItems";
	public static final String TASK_CANCEL = "nsTaskCancel";
	public static final String CANCEL_VALUE = "FALSE";
	public static final String TASK_TTL = "ttl";

	protected static final String SUCCESSFUL_TASK = "0";

	protected boolean _taskCancelled = false;
	
	protected final String _section = "LDAPTask";
	protected String _taskSection;
}

/* The Runnable class that allows to display an error dialog when we are not in the event thread... */
class LDAPErrorMessageDisplayer implements Runnable {
	public LDAPErrorMessageDisplayer(LDAPException lde, LDAPConnection ldc, Component comp) {
		_lde = lde;
		_ldc = ldc;
		_comp = comp;
	}

	public void run() {
		if (_lde.getLDAPResultCode() == _lde.INSUFFICIENT_ACCESS_RIGHTS){
			DSUtil.showPermissionDialog(_comp, _ldc);
		} else if ( _lde.getLDAPResultCode() == _lde.UNAVAILABLE ) {
			DSUtil.showErrorDialog(_comp, "updating-server-unavailable", "" );
		} else if ( _lde.getLDAPResultCode() == _lde.NO_SUCH_OBJECT ) {				
		} else {
			DSUtil.showLDAPErrorDialog(_comp, _lde, "updating-directory-title" );
		}
	}
	Component _comp;
	LDAPException _lde;
	LDAPConnection _ldc;
}
