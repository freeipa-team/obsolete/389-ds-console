/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import javax.swing.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.*;
import netscape.ldap.LDAPConnection;
import java.util.*;

/**
 *	Netscape Admin Server 4.0 task for stopping the directory server.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.task.CGITask
 */
public class Start extends CGITask
{
	/**
	 *	Constructor for the Start Directory Task
	 */
	public Start() {
		super();
		_sCmd = getTaskName();
		_section = "Start";
		setName(DSUtil._resource.getString("dirtask",_section));
		setDescription(DSUtil._resource.getString(
			"dirtask","Start-description"));
	}

	/**
	 * Send an http request to the server and then popup a dialog if the
	 * operation is successful.
	 *
	 * @param viewInstance The calling page
	 * @param cmd Command to execute
	 */
	boolean run(IPage viewInstance, String cmd) {	
		/* We get the name of the instance we want to start */
		DSBaseModel bmodel = (DSBaseModel)_consoleInfo.get( "dsresmodel" );	
		String instanceName = findInstanceName(viewInstance);
		LDAPConnection ldc = _consoleInfo.getLDAPConnection();
		JFrame frame = viewInstance.getFramework().getJFrame();

		/* Don't bother if the server is already up */	   
		boolean status = DSUtil.reconnect( ldc );
		if ( !status ) {
			if ( isSSLEnabled(viewInstance) ) {
				String msg = DSUtil.isNT(_consoleInfo) ? "Start-NT-SSL" : "Start-UNIX-SSL";
				int response = DSUtil.showConfirmationDialog(
					frame,
					msg,
					instanceName,
					"dirtask" );
				if ( response != JOptionPane.YES_OPTION ) {
					return false;
				}
			}
			status = super.run( viewInstance, cmd );
 			/* Don't trust the return code - check for really up */
			int tries = 10;
			do {
				status = DSUtil.reconnect( ldc );
				tries--;
				if ( status || (tries == 0) ) {
					break;
				}
 				try {
 					Thread.currentThread().sleep( 1000 );
 					status = DSUtil.reconnect( ldc );
 				} catch ( Exception e ) {
 				}
 			} while( true );
			if ( status ) {
				DSUtil.showInformationDialog( frame, "success",
											  instanceName, "dirtask-Start");
			} else {
				DSUtil.showErrorDialog( frame,"failed-title", "failed-msg",
										instanceName, "dirtask-Start" );
			}			
			if ( status ) {
				/* The contents of the directory may have changed */				
				if ( bmodel != null ) {
					bmodel.contentChanged();
				}
			}
			return status;
		}
        else {
            // it's already running
			String[] args = { instanceName };
            DSUtil.showInformationDialog( frame,
										  "Already-Started", args, "dirtask" );
            return status;
        }
	}
	static String getTaskName() { return "Tasks/Operation/Start"; }
}
