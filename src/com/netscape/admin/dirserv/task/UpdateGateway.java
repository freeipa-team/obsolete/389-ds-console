/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.awt.Component;
import javax.swing.JFrame;
import com.netscape.management.client.IPage;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.ExportImportProgressDialog;

/**
 *	Netscape Admin Server 4.0 task for updating DS Gateway config files
 *
 * @author	rweltman
 * @version %I%, %G%
 * @date	03/09/98
 * @see		com.netscape.admin.dirserv.DSAdmin
 * @see		com.netscape.admin.dirserv.task.CGIReportTask
 */
public class UpdateGateway extends CGITask {
	/**
	 *	Constructor for the UpdateGateway Directory Task
	 */
	public UpdateGateway( ConsoleInfo info ) {
		this(info, true);
	}
	/**
	 *	Constructor for the UpdateGateway Directory Task.
	 *  showMessages is a boolean used to 
	 */
	public UpdateGateway( ConsoleInfo info, boolean showMessages) {
		_consoleInfo = info;
		_sCmd = "Tasks/Operation/UpdateGateway";
		_section = "UpdateGateway";
		setName(DSUtil._resource.getString("dirtask-"+_section,"title"));
		_showMessages = showMessages;
	}

	/**
	 * Do the updates
	 *
	 * @param oldAuth Previous rootDN
	 * @param newAuth New rootDN
	 * @param oldHost Previous host
	 * @param newHost New host
	 * @param oldPort Previous port
	 * @param newPort New port
	 * @param oldBase Previous baseDN
	 * @param newBase New BaseDN
	 * @return true if the updates succeeded.
	 */
	public boolean exec( String oldAuth, String newAuth,
						 String oldHost, String newHost,
						 int oldPort, int newPort,
						 String oldBase, String newBase ) {
		String sAdminURL = _consoleInfo.getAdminURL();
		String fullCmd = sAdminURL + 
                         (String)_consoleInfo.get( "ServerInstance" )
                         + "/" + _sCmd;
		Hashtable args = new Hashtable();
		if ( (oldAuth != null) && (newAuth != null) ) {
			args.put( "old_dirmgr", oldAuth );
			args.put( "new_dirmgr", newAuth );
		}
		if ( (oldBase != null) && (newBase != null) ) {
			args.put( "old_suffix", oldBase );
			args.put( "new_suffix", newBase );
		}
		if ( oldHost == null ) {
			oldHost = _consoleInfo.getHost();
		}
		args.put( "old_host", oldHost );
		if ( newHost != null ) {
			args.put( "new_host", newHost );
		}
		if ( oldPort <= 0 ) {
			oldPort = _consoleInfo.getPort();
		}
		args.put( "old_port", Integer.toString(oldPort) );
		if ( newPort > 0 ) {
			args.put( "new_port", Integer.toString(newPort) );
		}

		_consoleInfo.put("arguments", args);
		IDSModel model =
			(IDSModel)_consoleInfo.get( "dsresmodel" );
		CGIThread task = new CGIThread( fullCmd, args, model, _section );
		if (_showMessages) {
			String title = _resource.getString("dirtask", _section+"-title");
			JFrame frame = model.getFrame();
			ExportImportProgressDialog dlg =
				new ExportImportProgressDialog(
											   frame, task, title,
											   false, frame );
			task.addEntryChangeListener( dlg );
			dlg.setModal( true );
			/* Let the dialog run the thread */			
			dlg.setVisible( true );		
			/* Only show dialog if there is an error, or if something really
			   changed */
			String msg = (String)task.getTask().getResult( "NMC_ErrInfo" );
			if ( !task.getStatus() ||
				 ((msg != null) && (msg.length() > 0)) ) {			
				showResultDialog( task );
			}
		} else {
			task.run();
		}
		return task.getStatus();
//		return run( null, _sCmd );
	}

	/**
	 * Do the updates
	 *
	 * @param oldAuth Previous rootDN
	 * @param newAuth New rootDN
	 */
	public boolean updateAuthenticationDN( String oldAuth, String newAuth ) {
		return exec( oldAuth, newAuth,
					 null, null,
					 0, 0,
					 null, null );
	}

	/**
	 * Do the updates
	 *
	 * @param oldHost Previous host
	 * @param newHost New host
	 * @param oldPort Previous port
	 * @param newPort New port
	 * @return true if the updates succeeded.
	 */
	public boolean updateHostPort( String oldHost, String newHost,
								   int oldPort, int newPort ) {
		return exec( null, null,
					 oldHost, newHost,
					 oldPort, newPort,
					 null, null );
	}

	/**
	 * Do the updates
	 *
	 * @param oldBase Previous baseDN
	 * @param newBase New BaseDN
	 * @return true if the updates succeeded.
	 */
	public boolean updateBaseDN( String oldBase, String newBase ) {
		return exec( null, null,
					 null, null,
					 0, 0,
					 oldBase, newBase );
	}
	boolean _showMessages = true;
}
