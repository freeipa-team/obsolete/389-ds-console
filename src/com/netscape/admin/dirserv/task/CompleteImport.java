/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;
import java.lang.*;
import com.netscape.management.client.*;
import com.netscape.management.client.TaskObject;
import com.netscape.management.client.console.ConsoleInfo;

import com.netscape.admin.dirserv.panel.LDAPAddPanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.util.Debug;
/**
 *	Netscape Directory Server 4.0 task for setting up keys & certificates
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.DSAdmin
 */
public class CompleteImport extends TaskObject {
	/**
	 *	Constructor for the KeyCert Directory Task
	 */
	public CompleteImport() {
		super();
		setName(DSUtil._resource.getString("dirtask","Import-title"));
		setDescription(DSUtil._resource.getString(
			"dirtask","Import-description"));
	}

	/**
	 * Call the frame work class.
	 *
	 * @param viewInstance The calling page
	 */
	public boolean run(IPage viewInstance) {
		databaseImport();
		return true;
	}

	private void databaseImport() {		
		IDSModel model =
			(IDSModel)getConsoleInfo().get( "dsresmodel" );
		LDAPAddPanel child = new LDAPAddPanel(model);
		
		SimpleDialog dlg = new SimpleDialog( model.getFrame(),
											 child.getTitle(),
											 SimpleDialog.OK |
											 SimpleDialog.CANCEL |
											 SimpleDialog.HELP,
											 child );
		dlg.setComponent( child );
 		dlg.setOKButtonEnabled( false );
		dlg.setDefaultButton( SimpleDialog.OK );

		dlg.packAndShow();

		dlg = null;		
    }

}

