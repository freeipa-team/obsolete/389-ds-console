/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.net.URL;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.AdmTask;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.*;

/**
 *	Netscape Admin Server 4.0 task for controlling SNMP subagent
 *
 * @author  stevross
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.task.CGITask
 */
public class SnmpCtrl extends CGITask {
	/**
	 *	Constructor for the SnmpCtrl Directory Task
	 */
	public SnmpCtrl( String action, ConsoleInfo info ) {
		Debug.println("SnmpCtrl, action is " + action);
		_action = action;
		_info = info;
		_section = "SNMP-"+action;
	}

    public boolean exec() {
		String sAdminURL = _info.getAdminURL();
		String sCmd = "Tasks/Operation/SNMPCtrl";
		String fullCmd = sAdminURL + _info.get( "ServerInstance" ) +
			"/" + sCmd;
		Hashtable args = new Hashtable();
		args.put("ACTION", _action);
		try {
			URL url = new URL(fullCmd);
			CGIReportTask task = new CustomCGIReportTask( url, _info );
			task.setArguments(args);
			int execStatus = task.exec();
			/* For the moment the only error we have is -1: this does
			   not provide a lot of information... display a custom message */
			if (_action.equals("STOP")) {
				String detail = (String) task.getResult().get("NMC_Description");
				if (detail != null && detail.equals("NOT_RUNNING")) {
					showDialog( null, _section+"-notrunning", "", false );
				}
				else {
					showResultDialog( task.getStatus() == 0 );
				}
			}
			else {
				showResultDialog( task.getStatus() == 0 );
			}
			return ( task.getStatus() == 0 );
		} catch ( Exception ex ) {
			Debug.println( "ServerSNMPSettingsPanel.runSnmpCtrl: " + ex );
		}
		return false;
	}

    private ConsoleInfo _info;
	private String _action;   
}

/**
  * This class is used to avoid waiting until the end of the http connection.
  * We know that when we receive NMC_DESCRIPTION the task is over!!
  */
class CustomCGIReportTask extends CGIReportTask {
	public CustomCGIReportTask(URL url, ConsoleInfo info) {
		super(url, info);
	}
	public void parse(String s) {
		super.parse(s);
		if (s.startsWith("NMC_Description") ||
			s.startsWith("NMC_DESCRIPTION")) {
			finish();
		}						
	}
}
