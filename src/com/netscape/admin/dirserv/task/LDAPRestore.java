/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.task.LDAPTask;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.GenericProgressDialog;
import netscape.ldap.*;

/**
 *	Netscape Directory Server 5.0 LDAP Restore task
 *
 * @author  jvergara
 * @version %I%, %G%
 * @date	 	02/09/2000
 * @see     com.netscape.admin.client.dirserv.DSAdmin
 * @see     com.netscape.admin.client.dirserv
 */

public class LDAPRestore extends LDAPTask {
	public LDAPRestore(IDSModel model, Hashtable attributes) {
		super(model, attributes);
		_taskType = RESTORE;
		_numberOfCurrentFile = 1;
		_taskSection = "restore";

		start();
	}

	public LDAPRestore(IDSModel model) {		
		this(model, (Hashtable)null);
	}

	protected LDAPAttributeSet createTaskEntryAttributes() {
		LDAPAttributeSet attributeSet = super.createTaskEntryAttributes();
		String value;
		LDAPAttribute attr;

		for( int i = 0; i < MANDATORY_ARGUMENTS.length; i++ ) {
			if ((attr = (LDAPAttribute)_attributes.get(MANDATORY_ARGUMENTS[i])) == null) {
				return null;
			} else {
				attributeSet.add(attr);
			}
		}
		
		for( int i = 0; i < OPTIONAL_ARGUMENTS.length; i++) {
			if ((attr = (LDAPAttribute)_attributes.get(OPTIONAL_ARGUMENTS[i])) != null) {
				attributeSet.add(attr);
			}	
		}
		return attributeSet;
	}

	protected void createProgressDialog() {
		String title = DSUtil._resource.getString(_section, _taskType+"-label");
		_progressDialog = new GenericProgressDialog(getModel().getFrame(), 
													true, 
													GenericProgressDialog.CLOSE_AND_LOG_BUTTON_OPTION, 
													title,
													null,
													this);
		
		((GenericProgressDialog)_progressDialog).addStep(DSUtil._resource.getString(_taskSection, "LDAPMode-firstStep-title"));		
		((GenericProgressDialog)_progressDialog).addStep(DSUtil._resource.getString(_taskSection, "LDAPMode-secondStep-title"));
		((GenericProgressDialog)_progressDialog).addStep(DSUtil._resource.getString(_taskSection, "LDAPMode-thirdStep-title"));
		((GenericProgressDialog)_progressDialog).enableButtons(false);

		_statusProgressDialog = new LDAPBasicProgressDialog(getModel().getFrame(), 
															 DSUtil._resource.getString(_taskSection, "LDAPMode-Status-title"), 
															 true, 
															 null,
															 this);
		_statusProgressDialog.waitForClose();
	}
	
	protected void updateProgressDialog(Hashtable updates) {
		String text = (String) updates.get(LDAPTask.TASK_LOG);
		/* This is the message from the server*/
		if (text.lastIndexOf("Beginning restore") >= 0) {
			((GenericProgressDialog)_progressDialog).stepCompleted(0);
		}
		/* This is the message from the server*/
		while (text.lastIndexOf("Restoring file "+_numberOfCurrentFile) >= 0) {
			((GenericProgressDialog)_progressDialog).stepCompleted(1);

			String[] args = {String.valueOf(_numberOfCurrentFile)};
			String labelText = DSUtil._resource.getString(_taskSection, "LDAPMode-progresslabel-title", args);
				
			((GenericProgressDialog)_progressDialog).setTextInLabel(labelText);
			_numberOfCurrentFile++;
		}
		/* This is the message from the server*/
		if (text.lastIndexOf("Restore finished") >= 0) {
			((GenericProgressDialog)_progressDialog).stepCompleted(2);
		}
	
		_statusProgressDialog.update(updates);
	}

	public void run() {		
		((GenericProgressDialog)_progressDialog).enableButtons(true);
		((GenericProgressDialog)_progressDialog).disableCancelButton();
		updateProgressDialog();
	}

	private int _numberOfCurrentFile;

	private final String RESTORE = "restore";

	public final static String ARCHIVE_DIR = "nsArchiveDir";
	public final static String DATABASE_TYPE = "nsDatabaseType";
	public String[] MANDATORY_ARGUMENTS = {ARCHIVE_DIR};
	public String[] OPTIONAL_ARGUMENTS = {DATABASE_TYPE};
}
