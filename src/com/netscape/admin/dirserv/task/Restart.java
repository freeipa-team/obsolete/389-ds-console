/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/**
 *	Netscape Admin Server 4.0 Directory Server restart task.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 9/15/97
 * @see     com.netscape.admin.dirserv
 * @see     com.netscape.admin.dirserv.task.CGITask
 */

/**
 */

package com.netscape.admin.dirserv.task;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import com.netscape.management.client.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.console.ConsoleInfo;

public class Restart extends CGITask {
	/**
	 *	constructor
	 */
	public Restart() {
		_section = "Restart";
		setName(DSUtil._resource.getString("dirtask",_section));
		setDescription(DSUtil._resource.getString(
			"dirtask",_section+"-description"));
		_sCmd = _CMD;
	}

	/**
	 * @param viewInstance The calling page
	 */
	public boolean run(IPage viewInstance) {
		JFrame frame = viewInstance.getFramework().getJFrame();
		/* We get the name of the instance we want to start */
		DSBaseModel bmodel = (DSBaseModel)_consoleInfo.get( "dsresmodel" );
		String instanceName = findInstanceName(viewInstance);		

		if ( isSSLEnabled(viewInstance) ) {
			String msg = DSUtil.isNT(_consoleInfo) ? "Start-NT-SSL" : "Start-UNIX-SSL";
			int response = DSUtil.showConfirmationDialog(
				frame,
				msg,
				instanceName,
				"dirtask" );
			if ( response != JOptionPane.YES_OPTION ) {
				return false;
			}
		} else {
			boolean confirm = DSUtil.requiresConfirmation(
				GlobalConstants.PREFERENCES_CONFIRM_STOP_SERVER );
			if ( confirm ) {
				int option = DSUtil.showConfirmationDialog(
					frame,
					"confirmRestartServer",
					instanceName,
					"general");
				if ( option != JOptionPane.YES_OPTION ) {
					return false;
				}
			}
	    }
		boolean status = super.run( viewInstance, _sCmd );
		if ( status ) {
			DSUtil.showInformationDialog( frame, "success",
										  instanceName, "dirtask-Restart");
		} else {
			DSUtil.showErrorDialog( frame,"failed-title", "failed-msg",
									instanceName, "dirtask-Restart" );
		}
		if ( status ) {
			/* The contents of the directory may have changed */			
			if ( bmodel != null ) {
				bmodel.contentChanged();
			}
		}
		return status;
	}
	private static final String _CMD = "Tasks/Operation/Restart";
	static String getTaskName() { return _CMD; }
}



