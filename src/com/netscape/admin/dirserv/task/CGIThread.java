/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.net.URL;
import javax.swing.JFrame;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.IEntryChangeListener;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;

/**
 *	Runnable shell for executing a CGI task as a thread.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.task.CGITask
 */
public class CGIThread implements Runnable {
	public CGIThread( String cmd, IDSModel model, String section ) {
		try {
			URL url = new URL( cmd );
			_task = new CGIReportTask( url, model.getServerInfo() );
		} catch ( Exception ex ) {
			Debug.println( "CGIThread: " + ex );
		}
		_model = model;
		_section = section;
	}

	public CGIThread( String cmd, Hashtable args, IDSModel model, String section ) {
		this(cmd, model, section);
		_task.setArguments(args);
	}

    public void run() {
		if ( _task == null ) {
			return;
		}
		_model.setWaitCursor( true );
		try {
			int execStatus = _task.exec();
			_status = ( (execStatus == 0) && (_task.getStatus() == 0) );
			Debug.println( "CGIThread.run: execStatus = " + execStatus +
				", taskStatus = " + _task.getStatus() );
			if ( _listener != null ) {
				_listener.entryChanged( null );
			}
		} catch ( Exception ex ) {
			Debug.println( "CGIThread.run: " + ex );
		} finally {
			_model.setWaitCursor( false );
		}
	}
    public void addEntryChangeListener( IEntryChangeListener listener ) {
		_listener = listener;
		if ( _task != null ) {
			_task.addEntryChangeListener( listener );
		}
	}
    public boolean getStatus() {
		return _status;
	}
	CGIReportTask getTask() {
		return _task;
	}

	private IEntryChangeListener _listener = null;
    private CGIReportTask _task = null;
    private boolean _status = false;
	private String _section;
	private IDSModel _model;
}
