/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;
import java.lang.*;
import java.util.Hashtable;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.nmclf.SuiConstants;
/**
 * LDAPBasicProgressDialog
 * Simple cancellable export/import progress dialog
 *
 * @version 1.0
 * @author jvergara
 * @see LDAPTaskProgressDialog
 **/
public class LDAPBasicProgressDialog extends LDAPTaskProgressDialog {	
		public LDAPBasicProgressDialog( JFrame parent,
									   String title ) {
		this( parent, title, true );
	}

	public LDAPBasicProgressDialog( JFrame parent,
									   String title,
									   boolean allowCancel ) {
		this( parent, title, allowCancel, null );
	}

	public LDAPBasicProgressDialog( JFrame parent,
									   String title,
									   boolean allowCancel,
									   Component comp ) {
		this( parent, title, allowCancel, comp, null);
	}

	public LDAPBasicProgressDialog( JFrame parent,
									   String title,
									   boolean allowCancel,
									   Component comp,
								       ActionListener listener) {
		super( parent, title, allowCancel, comp, listener);
	}	

	protected JScrollPane createFieldsPanel() {	
		_logField = new JTextArea();
		_logField.setColumns(COLUMNS);
		_logField.setRows(ROWS);		

		_logField.setEditable(false);
				
		JScrollPane jscroll = new JScrollPane( _logField );
		jscroll.setBorder( BorderFactory.createLoweredBevelBorder());
		
		return jscroll;
	}	

	public void update(Hashtable htUpdates) {
		/* If something changed, we update */
		String text = (String)htUpdates.get(LDAPTask.TASK_LOG);
		LogFieldUpdater doUpdate = new LogFieldUpdater(text);
		try {
			SwingUtilities.invokeLater(doUpdate);
		} catch ( Exception e ) {
		}
	}

	public static void main( String[] args ) {
		LDAPBasicProgressDialog dialog = new LDAPBasicProgressDialog(null, "TEST", true);
		dialog.pack();
		dialog.show();
	}
}
