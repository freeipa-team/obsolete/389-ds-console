/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import javax.swing.JFrame;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import netscape.ldap.*;

/**
 *	Netscape Directory Server 4.0 task for setting database read-only
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	03/09/98
 * @see     com.netscape.admin.dirserv.DSAdmin
 */
public class ReadOnly {
	/**
	 * Report if the database is read-only
	 *
	 * @param info ConsoleInfo for server instance
	 * @return true if the database is read-only
	 * @throws LDAPException if the server could not be read
	 */    
    static public boolean isReadOnly( ConsoleInfo info, String dnIndex ) throws LDAPException {
		IDSModel model = (IDSModel)info.get( "dsresmodel" );
		LDAPConnection ld = info.getLDAPConnection();
		String wasReadOnly = "off";
		try {
			//			LDAPEntry entry = ld.read( DB_SETTINGS_DN, attrs );
			LDAPEntry entry = ld.read( dnIndex, attrs );
			LDAPAttribute attr = entry.getAttribute( attrs[0] );
			if ( attr != null ) {
				wasReadOnly = (String)attr.getStringValues().nextElement();
			}
		} catch ( LDAPException e ) {
			DSUtil.showLDAPErrorDialog( model.getFrame(), e,
										dnIndex );
										//										DB_SETTINGS_DN );
			Debug.println( "ReadOnly.isReadOnly: reading " + attrs[0] +
						   ", " + e );
			throw e;
		}
		return wasReadOnly.equals("on");
    }

	/**
	 * Set read-only or read-write
	 *	 * @param info ConsoleInfo for server instance
	 * @param readOnly true if the database is to be set to read-only
	 * @return true if the operation succeeded.
	 */
    static public boolean setReadOnly( ConsoleInfo info, boolean readOnly, String dnIndex ) {
		IDSModel model = (IDSModel)info.get( "dsresmodel" );
		LDAPConnection ld = info.getLDAPConnection();
		String wasReadOnly = "off";
		if ( readOnly ) {
			/* Make server read-only */
			LDAPModification mod = new LDAPModification(
				LDAPModification.REPLACE,
				new LDAPAttribute( attrs[0], "on" ) );
			try {
				//				ld.modify( DB_SETTINGS_DN, mod );
				ld.modify( dnIndex, mod );
				return true;
			} catch ( LDAPException ex ) {
				DSUtil.showLDAPErrorDialog( model.getFrame(), ex,
											dnIndex );
											//											DB_SETTINGS_DN );
				return false;
			}
		} else {
			/* Make server read-writeable */
			LDAPModification mod = new LDAPModification(
				LDAPModification.REPLACE,
				new LDAPAttribute( attrs[0], "off" ) );
			try {
				//				ld.modify( DB_SETTINGS_DN, mod );
				ld.modify( dnIndex, mod );
				return true;
			} catch ( LDAPException ex ) {
				DSUtil.showLDAPErrorDialog( model.getFrame(), ex,
											dnIndex );
											//											DB_SETTINGS_DN );
				return false;
			}
		}
	}

	//	private static final String DB_SETTINGS_DN = "cn=config,cn=ldbm";
	private static final String READ_ONLY_ATTR_NAM = "readonly";
    private static final String[] attrs = { READ_ONLY_ATTR_NAM };
}
