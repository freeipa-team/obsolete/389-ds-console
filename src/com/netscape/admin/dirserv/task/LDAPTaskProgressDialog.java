/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.lang.*;
import java.util.Hashtable;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ModalDialogUtil;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.admin.dirserv.IEntryChangeListener;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.nmclf.SuiConstants;
import com.netscape.admin.dirserv.DSUtil;
/**
 * LDAPTaskProgressDialog
 * Simple cancellable export/import progress dialog
 *
 * @version 1.0
 * @author jvergara
 **/
public class LDAPTaskProgressDialog extends JDialog implements SuiConstants {
	public LDAPTaskProgressDialog( JFrame parent,
									   String title ) {
		this( parent, title, true );
	}

	public LDAPTaskProgressDialog( JFrame parent,
									   String title,
									   boolean allowCancel ) {
		this( parent, title, allowCancel, null );
	}

	public LDAPTaskProgressDialog( JFrame parent,
									   String title,
									   boolean allowCancel,
									   Component comp ) {
		this( parent, title, allowCancel, comp, null);
	}

	public LDAPTaskProgressDialog( JFrame parent,
									   String title,
									   boolean allowCancel,
									   Component comp,
								       ActionListener listener) {		
		super(parent, true);
		ModalDialogUtil.setDialogLocation(this, parent);

		setTitle(title);

		_panel = new GroupPanel("");
		_panel.setLayout(new GridBagLayout());
		
		_listener = listener;		
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = gbc.BOTH;
		gbc.anchor = gbc.NORTH;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		_panel.add(Box.createVerticalStrut(PAD), gbc);	
		gbc.weighty = 1.0;
		_panel.add(createFieldsPanel(), gbc);
		
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weighty = 0.0;

		_panel.add(Box.createVerticalStrut(PAD), gbc);
		gbc.gridwidth = gbc.REMAINDER;
		_panel.add(Box.createGlue(), gbc);
		
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.BOTH;
		gbc.weightx = 1.0;
		gbc.anchor = gbc.SOUTH;
		_panel.add(createButtonPanel(allowCancel), gbc);		
		
		setContentPane(_panel);

	}

	protected JScrollPane createFieldsPanel() {
		JPanel fieldsPanel = new JPanel();

		fieldsPanel.setLayout(new GridBagLayout());

		JLabel label;

		_statusField = new JLabel();
		
		label = new JLabel("Status");  // TO COMPLETE
		addEntryField(fieldsPanel, label, _statusField);

		_logField = new JTextArea();
		_logField.setColumns(COLUMNS);
		_logField.setRows(ROWS);
		
		_logField.setEditable(false);
		JScrollPane scroll = new JScrollPane(_logField);	

		label = new JLabel("Log");

		addEntryField(fieldsPanel, label, scroll);

		_progressBar = new JProgressBar();
		/* Values by Default */
		_progressBar.setMinimum(0);
		_progressBar.setMaximum(1);
		
		_progressLabel = new JLabel("0 %");
		addEntryField(fieldsPanel, _progressLabel, _progressBar);
		
		JScrollPane jscroll = new JScrollPane( fieldsPanel );
		jscroll.setBorder( new EmptyBorder( COMPONENT_SPACE, 
											DIFFERENT_COMPONENT_SPACE,
											COMPONENT_SPACE,
											DIFFERENT_COMPONENT_SPACE) );

		return jscroll;
	}

	protected void addEntryField(JPanel panel, JLabel label, JComponent field) {
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridwidth = 3;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHEAST;
		panel.add(label, gbc);
		
		gbc.gridwidth = gbc.RELATIVE;
		panel.add(Box.createHorizontalStrut(PAD), gbc);

		gbc.fill = gbc.BOTH;
		gbc.gridwidth= gbc.REMAINDER;
		gbc.anchor = gbc.NORTH;
		panel.add(field, gbc);
		
		gbc.gridwidth = gbc.REMAINDER;
		panel.add(Box.createVerticalStrut(PAD), gbc);

	}

	public void update(Hashtable htUpdates) {
		String statusText = (String)htUpdates.get(LDAPTask.TASK_STATUS);
		StatusFieldUpdater doStatusUpdate = new StatusFieldUpdater(statusText);
		try {
			SwingUtilities.invokeLater(doStatusUpdate);
		} catch ( Exception e ) {
		}

		String logFieldText = (String)htUpdates.get(LDAPTask.TASK_LOG);
		LogFieldUpdater doLogUpdate = new LogFieldUpdater(logFieldText);
		try {
			SwingUtilities.invokeLater(doLogUpdate);
		} catch ( Exception e ) {
		}

		
/*
		if (!_statusField.getText().equals((String)htUpdates.get(LDAPTask.TASK_STATUS))) {
			_statusField.setText((String)htUpdates.get(LDAPTask.TASK_STATUS));
		}
		if (!_logField.getText().equals((String)htUpdates.get(LDAPTask.TASK_LOG))) {
			_logField.setText((String)htUpdates.get(LDAPTask.TASK_LOG));
		}
*/
		Integer min = new Integer((String)htUpdates.get(LDAPTask.TASK_CURRENT_ITEM));
		Integer max = new Integer((String)htUpdates.get(LDAPTask.TASK_TOTAL_ITEMS));
			
		int minValue = min.intValue();
		int maxValue = max.intValue();
		
		ProgressBarUpdater doProgressBarUpdate = new ProgressBarUpdater(minValue, maxValue);
		try {
			SwingUtilities.invokeLater(doProgressBarUpdate);
		} catch ( Exception e ) {
		}

/*
		try {
			Integer min = new Integer((String)htUpdates.get(LDAPTask.TASK_CURRENT_ITEM));
			Integer max = new Integer((String)htUpdates.get(LDAPTask.TASK_TOTAL_ITEMS));
			
			int minValue = min.intValue();
			int maxValue = max.intValue();
			
			if ((minValue <= maxValue)&&(maxValue != 0)) {
				if (_progressBar.getMinimum()!=minValue) {
					_progressBar.setMinimum(minValue);
				}
				if (_progressBar.getMaximum()!=maxValue) {
					_progressBar.setMaximum(maxValue);
				}
				
				int ratio = (100 * minValue) / maxValue;
				
				if (!_progressLabel.getText().equals(ratio + " %")) {
					_progressLabel.setText(ratio + " %");
				}
				
				if (minValue == maxValue) {					
				} else {
					Debug.println("LDAPTaskProgressDialog.update(): a problem with the values");
				}
			}
		} catch (Exception e) {			
			Debug.println("LDAPTaskProgressDialog.update(): " + e);
			e.printStackTrace();
		}
		*/
	}		

		
	private JPanel createButtonPanel(boolean allowCancel) {
		_bCancel = UIFactory.makeJButton(this, "general", "Cancel");
				
		_bCancel.setActionCommand( LDAPTask.CANCEL );
		_bCancel.setOpaque( true );
		_bCancel.setVisible(allowCancel);
		if (_listener != null) {
			_bCancel.addActionListener(_listener);
		}
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());	

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE );
		gbc.weightx = 1.0;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.fill = gbc.HORIZONTAL;
		panel.add(Box.createHorizontalGlue(), gbc);
		
		JButton[] buttons = {_bCancel};

		gbc.weightx = 0.0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.NONE;
		panel.add(UIFactory.makeJButtonPanel(buttons), gbc);	

		return panel;		
	}


	public void finalize() {		
		try {
			super.finalize();		
		} catch (Throwable e) {
			Debug.println("LDAPTaskProgressDialog.finalize(): Exception: " +e);
		}
	}

	public void packAndShow() {		
		Debug.println("LDAPTaskProgressDialog.packAndShow");	   
		pack();
		super.show();
	}

	public void waitForClose() {
		_bCancel.setEnabled(true);
		_bCancel.setVisible(true);
		_bCancel.setText(DSUtil._resource.getString("general","Close-label"));
		_bCancel.setActionCommand(LDAPTask.CLOSE);
	}

	public String getLogFieldText() {
		return _logField.getText();
	}

	/* TO COMPLETE?? */
	class winAdapter extends WindowAdapter {
	    public void windowClosing(WindowEvent e) {
			Debug.println( "winAdapter.windowClosing: ");			
		}
    }

	class StatusFieldUpdater implements Runnable {
		StatusFieldUpdater(String text) {
			_text = text;
		}
		public void run() {
			if (!_statusField.getText().equals(_text)) {
				_statusField.setText(_text);
			}
		}
		String _text;
	}

	class LogFieldUpdater implements Runnable {
		LogFieldUpdater(String text) {
			_text = text;
		}
		public void run() {
			if (!_logField.getText().equals(_text)) {
				_logField.setText(_text);
			}
		}
		String _text;
	}

	class ProgressBarUpdater implements Runnable {
		ProgressBarUpdater(int minValue, int maxValue) {
			_minValue = minValue;
			_maxValue = maxValue;
		}
		public void run() {
			if ((_minValue <= _maxValue)&&(_maxValue != 0)) {
				if (_progressBar.getMinimum()!=_minValue) {
					_progressBar.setMinimum(_minValue);
				}
				if (_progressBar.getMaximum()!=_maxValue) {
					_progressBar.setMaximum(_maxValue);
				}
				
				int ratio = (100 * _minValue) / _maxValue;
				
				if (!_progressLabel.getText().equals(ratio + " %")) {
					_progressLabel.setText(ratio + " %");
				}
				
				if (_minValue == _maxValue) {					
				} else {
					Debug.println("ProgressBarUpdater.run(): a problem with the values");
				}
			}
		}
		int _minValue;
		int _maxValue;
	}


	public static void main( String[] args ) {
		LDAPTaskProgressDialog dialog = new LDAPTaskProgressDialog(null, "TEST", true);
		dialog.pack();
		dialog.show();
	}

	protected final int ROWS = 10;
	protected final int COLUMNS = 30;
	private static final int PAD = 10;

	protected JButton _bCancel;
	protected JPanel _panel;
	protected JLabel _statusField;
	protected JTextArea _logField;
	protected JProgressBar _progressBar;
	protected JLabel _progressLabel;
	protected ActionListener _listener;
}
