/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.Component;
import com.netscape.management.client.IPage;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.panel.RestorePanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.ExportImportProgressDialog;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.TaskObject;
import com.netscape.management.client.util.ModalDialogUtil;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import netscape.ldap.*;

/**
 *	Netscape Admin Server 4.0 task for backing up the directory server.
 *
 * @author  jvergara
 * @version %I%, %G%
 * @date	 	28/03/2000
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.task.CGITask
 */
public class Restore extends TaskObject {
	/**
	 *	Constructor for the Start Directory Task
	 */
	public Restore() {
		super();
		setName(DSUtil._resource.getString("dirtask","Restore"));
		setDescription(DSUtil._resource.getString(
			"dirtask","restore-description"));
	}
	
	/**
	 * Send an http request to the server and then popup a dialog if the
	 * operation is successful.
	 *
	 * @param viewInstance The calling page
	 * @param cmd Command to execute
	 */

	public boolean run(IPage viewInstance) {
		restore();
		return true;
	}

	public boolean run(IPage viewInstance, String cmd) {
		restore();
		return true;
	}

	/**
	 * Restore a particular database file.
	 *
	 * @param filename Full path of file to restore.
	 * @return true if the restore succeeded.
	 */
    public boolean restore( IPage viewInstance, String filename ) {
			/* Do it as a dialog */
		IDSModel model =
			(IDSModel)getConsoleInfo().get( "dsresmodel" );
		
		RestorePanel child = new RestorePanel( model );
		child.init();
		child.setFileName(filename);
		SimpleDialog dlg = new SimpleDialog( model.getFrame(),
											 child.getTitle(),
											 SimpleDialog.OK |
											 SimpleDialog.CANCEL |
											 SimpleDialog.HELP,
											 child );
		dlg.setComponent( child );		
		dlg.setDefaultButton( SimpleDialog.OK );
		ModalDialogUtil.setWindowLocation(dlg);
		dlg.getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString(
																					   "dirtask","restore-description"));
		dlg.packAndShow();
		if ( dlg.isCancel() )
			return false;
		else {
			String fileName = child.getFilename();
			String databaseType = LDBM_DATABASE;

			Hashtable attributes = new Hashtable();
			attributes.put(LDAPRestore.ARCHIVE_DIR, new LDAPAttribute(LDAPRestore.ARCHIVE_DIR, fileName));
			attributes.put(LDAPRestore.DATABASE_TYPE, new LDAPAttribute(LDAPRestore.DATABASE_TYPE, databaseType));
		
			LDAPRestore task = new LDAPRestore(model, attributes);
			return true;
		}		
	}	

	private void restore() {		
		/* Do it as a dialog */
		IDSModel model =
			(IDSModel)getConsoleInfo().get( "dsresmodel" );

		RestorePanel child = new RestorePanel( model );
		SimpleDialog dlg = new SimpleDialog( model.getFrame(),
											 child.getTitle(),
											 SimpleDialog.OK |
											 SimpleDialog.CANCEL |
											 SimpleDialog.HELP,
											 child );
		dlg.setComponent( child );
		dlg.setOKButtonEnabled( true );
		dlg.setDefaultButton( SimpleDialog.OK );		
		dlg.getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString(
																					   "dirtask","restore-description"));
		dlg.packAndShow();
		
		if ( dlg.isCancel() )
			return;
		else {
			String fileName = child.getFilename();
			String databaseType = LDBM_DATABASE;

			Hashtable attributes = new Hashtable();
			attributes.put(LDAPRestore.ARCHIVE_DIR, new LDAPAttribute(LDAPRestore.ARCHIVE_DIR, fileName));
			attributes.put(LDAPRestore.DATABASE_TYPE, new LDAPAttribute(LDAPRestore.DATABASE_TYPE, databaseType));
		
			LDAPRestore task = new LDAPRestore(model, attributes);
		}
	}
	final String LDBM_DATABASE = "ldbm database";
}
