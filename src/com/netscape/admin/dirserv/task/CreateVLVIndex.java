/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.util.Vector;
import java.awt.Component;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import com.netscape.management.client.IPage;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.admin.dirserv.panel.BackupPanel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.ExportImportProgressDialog;
import com.netscape.admin.dirserv.GenericProgressDialog;
import com.netscape.admin.dirserv.GlobalConstants;
import com.netscape.admin.dirserv.DSContentPage;
import netscape.ldap.*;
import com.netscape.admin.dirserv.panel.MappingUtils;
import netscape.ldap.util.DN;
import java.util.Enumeration;
/**
 *	Netscape Directory Server 4.0 task for creating a VLV index
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	03/09/98
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.task.CGIReportTask
 */
public class CreateVLVIndex extends Object implements ActionListener {
	/**
	 *	Constructor for the CreateVLVIndex Directory Task
	 */
	public CreateVLVIndex( ConsoleInfo info ) {
		_consoleInfo = info;				
	}

	/**
	 * Create VLV index for one-level search
	 *
	 * To be called from the event thread.
	 *
	 * @param dn Distinguished name of container
	 * @return true if the import succeeded.
	 */
    public boolean execute( String dn ) {
		_dn = dn;
		createProgressDialog();
		Thread thread = new Thread(new Runnable() {
			public void run() {
				createVLVIndex();
			}
		});
		thread.start();
		_progressDialog.packAndShow();		
		return getResult();
	}	

	public void createVLVIndex() {
		LDAPConnection ld = _consoleInfo.getLDAPConnection();		
		
		String indexName;		
		String instanceName = getInstanceName(_dn, _consoleInfo);

		IDSModel model =
				(IDSModel)_consoleInfo.get( "dsresmodel" );	

		if ((instanceName==null) || instanceName.equals(COULD_NOT_FIND_BACKEND)) {
			/* As we are NOT in the event thread we use the method SwingUtilities.invokeAndWait */
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					public void run() {
						DSUtil.showErrorDialog(_progressDialog, "CouldNotFindBackendForVLV-title", "CouldNotFindBackendForVLV-msg", null, "dirtask-CreateVLVIndex", _resource);
					}
				});
			} catch (Exception e) {
			}
			_progressDialog.closeCallBack();
			_result = false;			
			return;
		} else if(instanceName.equals(MORE_THAN_ONE_BACKEND)) {
			/* As we are NOT in the event thread we use the method SwingUtilities.invokeAndWait */
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					public void run() {
						DSUtil.showErrorDialog(_progressDialog, "multipleBackendForVLV-title", "multipleBackendForVLV-msg", null, "dirtask-CreateVLVIndex", _resource);			
					}
				});
			} catch (Exception e) {
			}
			_progressDialog.closeCallBack();
			_result = false;
			return;
		}
						
		int result = createIndex( ld, _dn );
		boolean status = (result == INDEX_CREATED);					

		if (_taskCancelled) {
			_progressDialog.setTextInLabel(_resource.getString(_section, "LDAPMode-cancelled-title"));
			cleanupCreatedIndex();
			_statusProgressDialog.hide();
			_progressDialog.closeCallBack();			
			_result = false;
			return;
		}

		if (status) {
			_progressDialog.stepCompleted(0);
			
			if (_attributeTags == null) {
				_progressDialog.setTextInLabel(_resource.getString(_section, "LDAPMode-couldNotCreateTags-title"));
				_progressDialog.waitForClose();
				_result = false;
			}
			Hashtable attributes = new Hashtable();
			
			attributes.put(LDAPCreateIndex.INSTANCE, new LDAPAttribute(LDAPCreateIndex.INSTANCE, instanceName));			
			attributes.put(LDAPCreateIndex.INDEX_VLV_ATTRIBUTE, new LDAPAttribute(LDAPCreateIndex.INDEX_VLV_ATTRIBUTE, _attributeTags));			
		
			/* In task we complete the updating of the progress dialog*/
			LDAPCreateIndex task = new LDAPCreateIndex(model, attributes, _progressDialog, _statusProgressDialog, true);

			if (_taskCancelled) {
				_progressDialog.setTextInLabel(_resource.getString(_section, "LDAPMode-cancelled-title"));
				cleanupCreatedIndex();
				_statusProgressDialog.hide();
				_progressDialog.closeCallBack();			
				_result = false;
				return;
			}

			/* See if everything went OK */
			if ((task.getResult() != LDAPTask.TASK_SUCCESSFULLY_COMPLETED) &&
				(task.getResult() != LDAPTask.TASK_COMPLETED)) {
				_progressDialog.disableCancelButton();			
				_progressDialog.setTextInLabel(_resource.getString(_section, "LDAPMode-cancelled-title"));				
				cleanupCreatedIndex();				
				_progressDialog.setTextInLabel(_resource.getString(_section, "LDAPMode-endError-title"));
				_progressDialog.waitForClose();
				status = false;
			} else {
				_progressDialog.stepCompleted(1);
				_progressDialog.waitForClose();
			}			
		} else {
			_progressDialog.setTextInLabel(_resource.getString(_section, "LDAPMode-endError-title"));
			_progressDialog.waitForClose();
			
		}
		_result = status; 		
	}

	public boolean getResult() {
		return _result;
	}

	/* It gives the entry root where we put the VLV Index in the backend of the entry 'dn' */
	static public String getBackendConfigRoot (String dn, ConsoleInfo consoleInfo) {		
	   	String instanceName = getInstanceName(dn, consoleInfo);
		if (instanceName == null) {
			return null;
		} else if (instanceName.equals(MORE_THAN_ONE_BACKEND) || 
				   instanceName.equals(COULD_NOT_FIND_BACKEND)) {
			return instanceName;
		} else {
			/* We usually just give the entry that corresponds to the backend in which the entry is */
			String backendConfigRoot = "cn="+createVlvSearchTag(dn)+", cn="+instanceName+", "+LDBM_PLUGIN_ROOT;
			Debug.println("CreateVLVIndex.getBackendConfigRoot(): backend config root for "+dn+" is "+backendConfigRoot);
			return backendConfigRoot;			
		} 
	}


	/* Gives the name of the instance   
	   */

	static public String getInstanceName( String dn, ConsoleInfo consoleInfo) {
		LDAPConnection ldc = consoleInfo.getLDAPConnection();
		String[] orderedSuffixes = MappingUtils.getOrderedSuffixList(ldc, MappingUtils.LDBM);
		if (orderedSuffixes == null) {
			return COULD_NOT_FIND_BACKEND;
		}
		String suffix=null;
		String[] suffixes = new String[1];
		/* We go through ALL the suffixes to see if the entry is in a distributed suffix*/
		for (int i=0; i<orderedSuffixes.length; i++) {
			suffixes[0] = orderedSuffixes[i];
			if (MappingUtils.isEntryInSuffixes(dn, suffixes, orderedSuffixes)) {
				if (suffix != null) {
					return MORE_THAN_ONE_BACKEND;
				}
				suffix = orderedSuffixes[i];
			}
		}

		String instanceName = MappingUtils.getBackendForSuffix(ldc, suffix);
		if (instanceName == null) {
			return COULD_NOT_FIND_BACKEND;			
		}
		return instanceName;
	}

	/* It gives the suffix of the backend of 'dn'. */
	static public String getSuffix(String dn, ConsoleInfo consoleInfo) {
	   	String instanceName = getInstanceName(dn, consoleInfo);
		if (instanceName == null) {
			return null;
		} else if (instanceName.equals(MORE_THAN_ONE_BACKEND) ||  
				   instanceName.equals(COULD_NOT_FIND_BACKEND)) {			
			return instanceName;
		} else {								
			String suffix = MappingUtils.getSuffixForBackend(consoleInfo.getLDAPConnection(), instanceName);			
			return suffix;
		}	   
	}	   

	private int createIndex( LDAPConnection ld,
								String base) {
		int status = ERROR_CREATING_INDEX;

		/* Create an entry with this DN and these attributes */		
		String backendConfigRoot = getBackendConfigRoot(base, _consoleInfo);
		if ((backendConfigRoot==null) || backendConfigRoot.equals(COULD_NOT_FIND_BACKEND)) {
			/* As we are NOT in the event thread we use the method SwingUtilities.invokeAndWait */
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					public void run() {
						DSUtil.showErrorDialog(_progressDialog, "CouldNotFindBackendForVLV-title", "CouldNotFindBackendForVLV-msg", null, "dirtask-CreateVLVIndex", _resource);
					}
				});
			} catch (Exception e) {
			}
			return ERROR_CREATING_INDEX;
		} else if(backendConfigRoot.equals(MORE_THAN_ONE_BACKEND)) {
			/* As we are NOT in the event thread we use the method SwingUtilities.invokeAndWait */
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					public void run() {	
						DSUtil.showErrorDialog(_progressDialog, "multipleBackendForVLV-title", "multipleBackendForVLV-msg", null, "dirtask-CreateVLVIndex", _resource);
					}
				});
			} catch (Exception e) {
			}
			return ERROR_CREATING_INDEX;
		}
		
		
		LDAPAttributeSet vlvSearchAttrs = new LDAPAttributeSet();			

		LDAPAttribute attr = new LDAPAttribute( "objectclass" );
		attr.addValue( "top" );
		attr.addValue( "vlvSearch" );
		vlvSearchAttrs.add( attr );			
		attr = new LDAPAttribute( "cn", createVlvSearchTag(base));
		vlvSearchAttrs.add( attr );
		String vlvBaseValue = base;				
	
		attr = new LDAPAttribute( "vlvbase", vlvBaseValue );
		vlvSearchAttrs.add( attr );
		
		attr = new LDAPAttribute( "vlvscope", "1");
		vlvSearchAttrs.add( attr );
		attr = new LDAPAttribute( "vlvfilter", FILTER_VLV);
		vlvSearchAttrs.add( attr );
		/* Create an entry with this DN and these attributes */			
		LDAPEntry vlvSearchEntry = new LDAPEntry( backendConfigRoot, vlvSearchAttrs );
				
		// add vlv search entry
		if (_taskCancelled) {
			_result = false;
			return ERROR_CREATING_INDEX;
		}
		/* Now add the entry to the directory */ 		
		try {			
			Debug.println("CreateVLVIndex.createIndex: adding vlvSearchEntry:"+ vlvSearchEntry);
			String arg = DSUtil.abreviateString(vlvSearchEntry.getDN(), 30);
			_progressDialog.setTextInLabel(_resource.getString(_section, "LDAPMode-addingEntry-title", arg));
			LDAPConstraints constraints = (LDAPConstraints)ld.getSearchConstraints().clone();
			constraints.setServerControls(DSContentPage._manageDSAITControl);			
			ld.add( vlvSearchEntry, constraints );
			status = INDEX_CREATED;
			Debug.println( "CreateVLVIndex.createIndex: index entry added" );
		} catch( LDAPException e ) {
			if ( e.getLDAPResultCode() == e.ENTRY_ALREADY_EXISTS ) {
				status = INDEX_CREATED;
				Debug.println( "CreateVLVIndex.createIndex: index entry <" +
							   base + "> already exists" );				
			} else {
				Debug.println( "CreateVLVIndex.createIndex: " +
							   e.toString() + ", <" + base + ">" );
				String msg = "\nException adding index: "+vlvSearchEntry.getDN()+"\n"+e.toString();
				/* As we are NOT in the event thread we use the method SwingUtilities.invokeAndWait */
				try {
					SwingUtilities.invokeAndWait(new ErrorDialogDisplayer(msg, _progressDialog));
				} catch (Exception exc) {
				}
			}
		}
			
		if( status == INDEX_CREATED ) {
			// add vlv index entries
			/* Attributes contains the attributes to index */
			String suffix = getSuffix(base, _consoleInfo);
			String[] attributes = DEFAULT_ATTRIBUTES;
			if ((attributes==null) || (attributes.length < 1)) {
				return ERROR_CREATING_INDEX;				
			}
			status = addVlvIndexEntries(base, attributes);		
		}					   
	

		return status;
	}

	private void cleanupCreatedIndex() {
		if (!hasIndex(_dn, _consoleInfo)) {
			return;
		} else {
			String backendConfigRoot = getBackendConfigRoot(_dn, _consoleInfo);
			if ((backendConfigRoot == null) || 
				(backendConfigRoot.equals(COULD_NOT_FIND_BACKEND)) ||
				(backendConfigRoot.equals(MORE_THAN_ONE_BACKEND))) {
				return;
			}
			try {
				Thread.sleep(600);
			} catch (Exception e) {
			}
			LDAPConnection ldc = _consoleInfo.getLDAPConnection();
			DSUtil.deleteTree(backendConfigRoot, ldc, true);			
		}	
	}


	/* Deletes the index for this entry */
	public static void deleteIndex(String dn, ConsoleInfo consoleInfo) {		
		if (!hasIndex(dn, consoleInfo)) {
			return;
		} else {			
			final String backendConfigRoot = getBackendConfigRoot(dn, consoleInfo);
			if ((backendConfigRoot == null) || 
				(backendConfigRoot.equals(COULD_NOT_FIND_BACKEND)) ||
				(backendConfigRoot.equals(MORE_THAN_ONE_BACKEND))) {
				return;
			}
			final LDAPConnection ldc = consoleInfo.getLDAPConnection();
			
			IDSModel model =
					(IDSModel)consoleInfo.get( "dsresmodel" );
			if ( DSUtil.requiresConfirmation(
											 GlobalConstants.PREFERENCES_CONFIRM_DELETE_INDEX ) ) {					
				int response = DSUtil.showConfirmationDialog(model.getFrame(),
															 "confirm-delete-index",
															 "",
															 "browser" );
				if ( response != JOptionPane.YES_OPTION ) {
					return;
				}
			}

			String title = _resource.getString(_section, "deleteIndex-title");
			final GenericProgressDialog dlg = new GenericProgressDialog(model.getFrame(), 
																		true, 
																		GenericProgressDialog.NO_BUTTON_OPTION, 
																	  title,
																		null,
																		null);
			try {					
				Thread th = new Thread(new Runnable() {
					public void run() {
						DSUtil.deleteTree(backendConfigRoot, ldc, true, dlg);
						dlg.closeCallBack();
					}
				});
				th.start();
				dlg.packAndShow();
			} catch ( Exception e ) {
				Debug.println("CreateVLVIndex.deleteIndex(): " +
							  e );
				e.printStackTrace();	
			}								
			return;		
		}
	}
		
	/* Tells if this entry has an associated VLVIndex or not*/
	public static boolean hasIndex(String dn, ConsoleInfo consoleInfo) {
		/* We look for the vlvSearch entry of the backend of the entry with 'dn' */
		String backendConfigRoot = getBackendConfigRoot(dn, consoleInfo);
		if ((backendConfigRoot == null) || 
			(backendConfigRoot.equals(COULD_NOT_FIND_BACKEND)) ||
			(backendConfigRoot.equals(MORE_THAN_ONE_BACKEND))) {
			return false;
		}
		LDAPConnection ldc = consoleInfo.getLDAPConnection();
		LDAPEntry entry;
		
		try {
			entry = ldc.read(backendConfigRoot);
			if (entry == null) {
				return false;
			}
			return true;
		} catch (LDAPException e) {
			return false;
		}		
	}

	public static int indexStatus(String dn, ConsoleInfo consoleInfo) throws LDAPException {
		Debug.println("CreateVLVIndex.indexStatus(): "+dn);
		int status = CAN_NOT_HAVE_INDEX;
		/* We look for the vlvSearch entry of the backend of the entry with 'dn' */
		String backendConfigRoot = getBackendConfigRoot(dn, consoleInfo);
		if ((backendConfigRoot == null) || 
			(backendConfigRoot.equals(COULD_NOT_FIND_BACKEND)) ||
			(backendConfigRoot.equals(MORE_THAN_ONE_BACKEND))) {
			status = CAN_NOT_HAVE_INDEX;
		} else {
			LDAPConnection ldc = consoleInfo.getLDAPConnection();
			LDAPEntry entry;
			
			try {
				String[] attrs = {"dn"};
				entry = ldc.read(backendConfigRoot, attrs);
				if (entry == null) {
					status = NO_INDEX;
				} else {
					status = HAS_INDEX;
				}				
			} catch (LDAPException e) {
				if (e.getLDAPResultCode() != LDAPException.NO_SUCH_OBJECT) {
					throw e;
				} else {
					status = NO_INDEX;
				}
			}
		}
		return status;
	}


	private int addVlvIndexEntries(String dn, String[] attributes) {
		LDAPConnection ldc = _consoleInfo.getLDAPConnection();
		String backendConfigRoot = getBackendConfigRoot(dn, _consoleInfo);
		if ((backendConfigRoot==null) ||
			backendConfigRoot.equals(COULD_NOT_FIND_BACKEND) ||
			backendConfigRoot.equals(MORE_THAN_ONE_BACKEND)) {
			return ERROR_CREATING_INDEX;
		}
		
		if (_taskCancelled) {
			_result = false;
			return ERROR_CREATING_INDEX;
		}

		/* We just use the first attribute to create the tag */
		String cnValue = TAG+" "+createVlvSearchTag(dn);
		_attributeTags = new String[1];
		_attributeTags[0] = cnValue;
		String indexDN = "cn="+cnValue+","+backendConfigRoot;
		
		try {					
			LDAPAttributeSet vlvIndexAttrs = new LDAPAttributeSet();		
			
			LDAPAttribute attr = new LDAPAttribute( "objectclass" );
			attr.addValue( "top" );
			attr.addValue( "vlvIndex" );
			vlvIndexAttrs.add( attr );
			
			attr = new LDAPAttribute( "cn", cnValue);
			vlvIndexAttrs.add( attr );	
			
			String attributeString = new String("");
			for (int i=0; i< attributes.length; i++) {
				attributeString = attributeString + attributes[i]+" ";
			}
                        attributeString = attributeString.trim();
			
	                attr = new LDAPAttribute( "vlvsort", attributeString);
			vlvIndexAttrs.add( attr );
			
			LDAPEntry vlvIndexEntry = new LDAPEntry(indexDN, vlvIndexAttrs);	
			
			Debug.println("CreateVLVIndex.addVlvIndexEntries: adding vlvIndexEntry:"+ vlvIndexEntry);
			String arg = DSUtil.abreviateString(vlvIndexEntry.getDN(), 30);
			_progressDialog.setTextInLabel(_resource.getString(_section, "LDAPMode-addingEntry-title", arg));
			ldc.add(vlvIndexEntry);						
		} catch (LDAPException e) {
			if ( e.getLDAPResultCode() == e.ENTRY_ALREADY_EXISTS ) {	
				Debug.println( "CreateVLVIndex.addVlvIndexEntries: index entry <" +
							   indexDN + "> already exists" );								
			} else {
				Debug.println( "CreateVLVIndex.addVlvIndexEntries: " +
							   e.toString() + ", <" + indexDN + ">" );
				String msg = "\nException adding index: "+indexDN+"\n"+e.toString();
				/* As we are NOT in the event thread we use the method SwingUtilities.invokeAndWait */
				try {
					SwingUtilities.invokeAndWait(new ErrorDialogDisplayer(msg, _progressDialog));
				} catch (Exception exc) {
				}
				return ERROR_CREATING_INDEX;
			}
		}
	
		return INDEX_CREATED;
	}


	/* This method gives the list of displaying attributes that are not the default ones, in the backend corresponding to 'suffix' and
	   under the entry 'dn'.
	   These are the attributes that have to be indexed */
	private String[] getAttributesToIndex(String dn, String suffix) {		
		String filter="(&";
		Hashtable htAttributes = new Hashtable();
		for (int i=0; i<DEFAULT_ATTRIBUTES.length; i++) {
			filter = filter + "(!("+DEFAULT_ATTRIBUTES[i]+"=*))";
			htAttributes.put(DEFAULT_ATTRIBUTES[i], DEFAULT_ATTRIBUTES[i]);
		}
		filter = filter+")";
		
		LDAPConnection ldc = _consoleInfo.getLDAPConnection();
		String[] attrs = {"*"};		

		try {
			LDAPSearchConstraints cons = ldc.getSearchConstraints();
			cons.setMaxResults( 0 );
			LDAPSearchResults res =
				ldc.search( dn, ldc.SCOPE_ONE, filter, attrs, false, cons );
			int count = 0;
			
			/* Loop on results until finished */			
		
			while ( res.hasMoreElements() ) {
				LDAPEntry entry = res.next();
				if ((entry == null) || (entry.getDN() == null)) {
					continue;
				}		
				String entryDN = entry.getDN();
				String attribute = entryDN.substring(0, entryDN.indexOf('=')).trim().toLowerCase();
				htAttributes.put(attribute, attribute);					
			}			
		} catch (LDAPException e) {
			Debug.println("CreateVLVIndex.getNamingAttributes(); "+e);			
		}
		
		
		if (htAttributes.size() > 0) {
			String[] attributes = new String[htAttributes.size()];
			Enumeration e = htAttributes.elements();
			int i=0;
			while (e.hasMoreElements()) {
				attributes[i] = (String)e.nextElement();
				i++;
			}
			return attributes;
		}
		return null;
	}

	/* Tells us if the entry 'dn' is in the backend of suffix 'suffix'.  Suffixes is the set of suffix of all the backends where
	   the entry can be */
	private boolean inBackend(String dn, String suffix, String[] suffixes) {
		DN entryDN = new DN(dn);
		DN suffixDN = new DN(suffix);
		if (entryDN.isDescendantOf(suffixDN)) {
			/* The entry is under 'suffix'... it may be in the backend
			 We look in the other suffixes */
			for (int i=0 ; i< suffixes.length; i++) {
				DN currentSuffixDN = new DN(suffixes[i]);
				if (entryDN.isDescendantOf(currentSuffixDN)) {
					if (currentSuffixDN.isDescendantOf(suffixDN)) {
						return false;
					}
				} else if (entryDN.equals(currentSuffixDN)) {
					/* The entry is the root of the backend of currentSuffix; and 'currentSuffix' is not the same as 'suffix' 
					   (dn is descendant of 'suffix' and not of 'currentSuffix') */
					return false;
				}
			}
			return true;
			/* The entry is the root of the backend */
		} else if(entryDN.equals(suffixDN)) {
			return true;
			/* The entry is not under the suffix and is not root*/
		} else {
			return false;
		}
	}

	private static String createVlvSearchTag(String dn) {
		String vlvSearchTag = "MCC "+ dn.replace(',',' ').replace('"','-').replace('+','_');
		return vlvSearchTag;
	}
	
	protected void createProgressDialog() {
		String title = _resource.getString(_section,"title");
		IDSModel model =
					(IDSModel)_consoleInfo.get( "dsresmodel" );	
		_progressDialog = new GenericProgressDialog(model.getFrame(), 
													true, 
													GenericProgressDialog.CLOSE_AND_LOG_BUTTON_OPTION, 
													title,
													null,
													this);			   

		_progressDialog.addStep(DSUtil._resource.getString(_section, "LDAPMode-firstStep-title"));		
		_progressDialog.addStep(DSUtil._resource.getString(_section, "LDAPMode-secondStep-title"));
		
		_statusProgressDialog = new LDAPBasicProgressDialog(model.getFrame(), 
															DSUtil._resource.getString(_section, "LDAPMode-Status-title"),
															true,
															null,
															this);
		_statusProgressDialog.waitForClose();
	}

	/**
	 *	Handle incoming event from button.
	 *
	 * @param e event
	 */
	public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( GenericProgressDialog.CANCEL ) ) {
			Debug.println("CreateVLVIndex.actionPerformed: CANCEL");
			_progressDialog.disableCancelButton();
			_taskCancelled = true;	
		} else if ( e.getActionCommand().equals( GenericProgressDialog.CLOSE ) ) {
			Debug.println("CreateVLVIndex.actionPerformed: CLOSE");
			_statusProgressDialog.hide();
			_progressDialog.closeCallBack();
		} else if (	e.getActionCommand().equals( GenericProgressDialog.SHOW_LOGS)) {
			Debug.println("CreateVLVIndex.actionPerformed: SHOW_LOGS");
			
			_statusProgressDialog.pack();
			_statusProgressDialog.setLocationRelativeTo((Component)_progressDialog);
			_statusProgressDialog.setLocation(_progressDialog.getWidth(), _progressDialog.getHeight());
			_statusProgressDialog.show();
		} else if ( e.getActionCommand().equals( LDAPTask.CLOSE ) ) {
			_statusProgressDialog.hide();
			_statusProgressDialog.dispose(); 			
		}
	}

    String[] _attributeTags = null;

	public static int NO_INDEX = 0;
	public static int HAS_INDEX = 1;
	public static int CAN_NOT_HAVE_INDEX = 2;
										   
	final static String LDBM_PLUGIN_ROOT = "cn=ldbm database, cn=plugins, cn=config";
	final static String COULD_NOT_FIND_BACKEND = "Could not find entry";
	final static String MORE_THAN_ONE_BACKEND = "More than one backend";
	final static String SEPARATOR = ";";
	final static String FILTER_VLV = "(|(objectclass=*)(objectclass=ldapsubentry))";
	final static String TAG = "by";
	final static String REVERSE_TAG = "d";

	public static final String[] DEFAULT_ATTRIBUTES = com.netscape.admin.dirserv.browser.BrowserController.SORT_ATTRIBUTES;
	final int INDEX_CREATED = 0;
	final int ERROR_CREATING_INDEX = -1;
	final int SUB = 2;

	boolean _taskCancelled = false;
	LDAPBasicProgressDialog _statusProgressDialog;
	GenericProgressDialog _progressDialog;

    ConsoleInfo _consoleInfo;
	String _dn;
	private boolean _result = false;
	final static String _section = "CreateVLVIndex";	
	static ResourceSet _resource = DSUtil._resource;
}

/* The Runnable class that allows to display an error dialog when we are not in the event thread... */
class ErrorDialogDisplayer implements Runnable {
	public ErrorDialogDisplayer(String msg, Component comp) {
		_msg = msg;
		_comp = comp;
	}
	public void run() {		
		String[] args = {_msg};
		DSUtil.showErrorDialog(_comp, 
							   "ErrorCreatingIndex-title", 
							   "ErrorCreatingIndex-msg", 
							   args, 
							   "dirtask-CreateVLVIndex", DSUtil._resource);				
	}
	String _msg;
	Component _comp;
}
