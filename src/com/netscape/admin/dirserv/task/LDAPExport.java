/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.task;

import java.util.Hashtable;
import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.GenericProgressDialog;
import com.netscape.admin.dirserv.task.LDAPTask;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import netscape.ldap.*;

/**
 *	Netscape Directory Server 5.0 LDAP Export task
 *
 * @author  jvergara
 * @version %I%, %G%
 * @date	 	02/09/2000
 * @see     com.netscape.admin.client.dirserv.DSAdmin
 * @see     com.netscape.admin.client.dirserv
 */

public class LDAPExport extends LDAPTask {
	public LDAPExport(IDSModel model, Hashtable attributes) {
		super(model, attributes);
		_taskType = EXPORT;
		_taskSection = "export";

		LDAPAttribute instanceAttr = (LDAPAttribute)(attributes.get(INSTANCE));

		_instanceValues = instanceAttr.getStringValueArray();
		
		_currentInstanceNumber = 1;
		_currentInstance = _instanceValues[_currentInstanceNumber - 1];
		_numberOfInstances = _instanceValues.length;
		
		start();
	}

	public LDAPExport(IDSModel model) {		
		this(model, (Hashtable)null);
	}
	

	protected LDAPAttributeSet createTaskEntryAttributes() {
		LDAPAttributeSet attributeSet = super.createTaskEntryAttributes();
		String value;
		LDAPAttribute attr;

		for( int i = 0; i < MANDATORY_ARGUMENTS.length; i++ ) {
			if ((attr = (LDAPAttribute)_attributes.get(MANDATORY_ARGUMENTS[i])) == null) {
				return null;
			} else {
				attributeSet.add(attr);
			}
		}
		
		for( int i = 0; i < OPTIONAL_ARGUMENTS.length; i++) {
			if ((attr = (LDAPAttribute)_attributes.get(OPTIONAL_ARGUMENTS[i])) != null) {
				attributeSet.add(attr);
			}	
		}
		return attributeSet;
	}
	
	protected void createProgressDialog() {
		String title = getTitle();
		_progressDialog = new GenericProgressDialog(getModel().getFrame(), 
													true, 
													GenericProgressDialog.CLOSE_AND_LOG_BUTTON_OPTION, 
													title,
													null,
													this);
		((GenericProgressDialog)_progressDialog).enableButtons(false);				
		((GenericProgressDialog)_progressDialog).addStep(DSUtil._resource.getString(_taskSection, "LDAPMode-firstStep-title"));		
		((GenericProgressDialog)_progressDialog).addStep(DSUtil._resource.getString(_taskSection, "LDAPMode-secondStep-title"));
		LDAPAttribute exportFileAttribute = (LDAPAttribute)(_attributes.get(FILENAME));
		String exportFile = exportFileAttribute.getStringValueArray()[0];
		String labelText;
		if (_numberOfInstances > 1) {				
			String[] args = {				
				DSUtil.abreviateString(_currentInstance, 30),
				String.valueOf(_currentInstanceNumber),
				String.valueOf(_numberOfInstances),
				DSUtil.inverseAbreviateString(exportFile, 40)
			};
			labelText = DSUtil._resource.getString(_taskSection, "LDAPMode-progresslabel-title", args);								
		}  else {
			String[] args = {				
				DSUtil.abreviateString(_currentInstance,30),
				DSUtil.inverseAbreviateString(exportFile, 40)
				};
			labelText = DSUtil._resource.getString(_taskSection, "LDAPMode-progresslabel-one-partition-title", args);
		}

		((GenericProgressDialog)_progressDialog).setTextInLabel(labelText);		

		_statusProgressDialog = new LDAPBasicProgressDialog(getModel().getFrame(), 
															 DSUtil._resource.getString(_taskSection, "LDAPMode-Status-title"), 
															 true, 
															 null,
															 this);
		_statusProgressDialog.waitForClose();
		
	}

	public String getTitle() {
		String title;
		LDAPAttribute exportReplica = (LDAPAttribute)_attributes.get(EXPORT_REPLICA);
		if (exportReplica != null) {
			String value = (String) exportReplica.getStringValues().nextElement();
			if (value.equalsIgnoreCase("TRUE")) {
				if (_numberOfInstances == 1) {
					String[] args = {_currentInstance};
					title = DSUtil._resource.getString("export-replica", "title", args);
				} else {
					title = DSUtil._resource.getString("export", "title");
				}
				return title;				
			}
		} 
		if (_numberOfInstances == 1) {
			String[] args = {_currentInstance};
			title = DSUtil._resource.getString("export-partition", "title", args);
		} else {
			title = DSUtil._resource.getString("export", "title");
		}
		return title;
	}


	protected void updateProgressDialog(Hashtable updates) {
		String text = (String) updates.get(LDAPTask.TASK_LOG);
		/* This is the message from the server*/
		if (text.lastIndexOf("Beginning export of '"+_currentInstance) >= 0) {
			((GenericProgressDialog)_progressDialog).stepCompleted(0);
		}
		/* This is the message from the server*/
		if (text.lastIndexOf(_currentInstance+": Processed") >= 0) {
			((GenericProgressDialog)_progressDialog).stepCompleted(1);			
			if (_currentInstanceNumber < _numberOfInstances) {
				try {
					Thread.sleep(400);
				} catch ( InterruptedException e ) {
				}
				_currentInstanceNumber++;
				_currentInstance = _instanceValues[_currentInstanceNumber-1];
				((GenericProgressDialog)_progressDialog).reset();
			}
		}

		if (_numberOfInstances > 1) {
			LDAPAttribute exportFileAttribute = (LDAPAttribute)(_attributes.get(FILENAME));
			String exportFile = exportFileAttribute.getStringValueArray()[0];
			String[] args = {
				DSUtil.abreviateString(_currentInstance, 30),
				String.valueOf(_currentInstanceNumber),
				String.valueOf(_numberOfInstances),
				DSUtil.inverseAbreviateString(exportFile, 40)
			};
			String labelText = DSUtil._resource.getString(_taskSection, "LDAPMode-progresslabel-title", args);
			((GenericProgressDialog)_progressDialog).setTextInLabel(labelText);
		}
				
		_statusProgressDialog.update(updates);
	}
	
	protected void finalizeTask(int result) {
		Debug.println("LDAPExport.finalizeTask");		
		
		switch (result) {
		case LDAPTask.INSUFFICIENT_ACCESS_RIGHTS_ERROR:
			_taskCancelled = true;
			break;
		case LDAPTask.SERVER_UNAVAILABLE_ERROR:
			_taskCancelled = true;
			break;
		case LDAPTask.SERVER_UPDATING_ERROR:
			_taskCancelled = true;
			break;
		case LDAPTask.TASK_COMPLETED:
			break;
		case LDAPTask.TASK_SUCCESSFULLY_COMPLETED:
			((GenericProgressDialog)_progressDialog).stepCompleted(0);
			((GenericProgressDialog)_progressDialog).stepCompleted(1);
			break;
		case LDAPTask.TASK_UNSUCCESSFULLY_COMPLETED:
			String labelText = DSUtil._resource.getString("general", "LDAPTaskCompleted-unsuccessful-label");
			((GenericProgressDialog)_progressDialog).setTextInLabel(labelText);					
			break;
		}
		_result = result;
		waitForClose();
	}
	
	public void run() {		
		((GenericProgressDialog)_progressDialog).enableButtons(true);
		((GenericProgressDialog)_progressDialog).disableCancelButton();
		updateProgressDialog();
	}

	private final String EXPORT = "export";

	protected int _currentInstanceNumber;
	protected int _numberOfInstances;
	protected String _currentInstance;
	protected String[] _instanceValues;

	public String[] MANDATORY_ARGUMENTS = {FILENAME, INSTANCE};
	public String[] OPTIONAL_ARGUMENTS = {INCLUDE_SUFFIX, EXCLUDE_SUFFIX, USE_ONE_FILE, EXPORT_REPLICA};
	public final static String FILENAME = "nsFilename";
	public final static String INSTANCE = "nsInstance";
	public final static String INCLUDE_SUFFIX = "nsIncludeSuffix";
	public final static String EXCLUDE_SUFFIX = "nsExcludeSuffix";	
	public final static String USE_ONE_FILE = "nsUseOneFile";
	public final static String EXPORT_REPLICA = "nsExportReplica";
	

	/* The following attributes are not used (for the moment) */
        public final static String PRINT_KEY = "nsPrintKey";
	public final static String USE_ID2ENTRY = "nsUseId2Entry";
	public final static String NO_WRAP = "nsNoWrap";
	public final static String DUMP_UNIQUE_ID = "nsDumpUniqId";	
}
