/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import javax.swing.JPasswordField;
import javax.swing.JLabel;

/* This class implements confirm password part of the password/confirm pair
   of fields. It should be used in combination with DSEntryPassword class 
   which implements the new password part.

   This class' main purpose is to insure that information in password and
   confirm password fields match.
 */

public class DSEntryConfirmPassword extends DSEntryText{

    /**
     * This method constructs the DSEntryConfirmPassword object
     *
     * @param model - initial entry model
     * @param pfConfirm - confirm password field
     * @param lConfirm  - label for the above field
     * @param pfPwd - password field
     * @param minLength - minimum password length
     */

    public DSEntryConfirmPassword (String model, 
                                   JPasswordField pfConfirm, 
                                   JLabel lConfirm, 
                                   JPasswordField pfPwd,
                                   int minLength){
        super (model, pfConfirm, lConfirm);

        _pfPwd = pfPwd;
        _minLength = minLength;
    }

    /**
     * This method validates the content of the DSEntry
     * The entry is considered valid if the corresponding 
     * password field contains at list 8 characters and
     * matches the content of the password field.
     *
     * @return - 0 - if the entry is valid
     *           1 - if password is too short
     *           2 - if passwords mismatch
     */

    public int validate() {
        String strPwd = _pfPwd.getText();
        JPasswordField pfConfirm = (JPasswordField)getView (0);
        String strConfirm = pfConfirm.getText ();

        if ( strPwd.length() < _minLength ) {
            /* length less than minimum */
            return 1;
        } else if (strPwd.equals(strConfirm)) {
            return 0;
        }
        /* password mismatch */
        return 2;
      
    }

    public void setMinLength (int minLength){
        _minLength = minLength;
    }
   
    private JPasswordField _pfPwd;  /* new password field */
    private int _minLength;         /* minimum password length */
}
