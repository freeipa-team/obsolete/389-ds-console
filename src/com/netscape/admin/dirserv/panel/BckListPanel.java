/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.UITools;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DefaultResourceModel;

/**
 * BckListPanel
 * 
 *
 * @version 1.0
 * @author rweltman
 **/
public class BckListPanel extends BlankPanel {
	public BckListPanel( IDSModel model,
						 String[] bckList,
						 String[] inAlready,
						 String helpToken ) {
		super( model, _section, false );
		// setTitle( title );
		_bList = bckList;
		_bIn = inAlready;
		_helpToken = helpToken;
	}


	public void init() {
        _myPanel.setLayout( new GridBagLayout() );
		GridBagConstraints gbc = (GridBagConstraints)getGBC().clone();
		gbc.insets = new Insets( 0, 0, 0, 0 );
		gbc.fill = gbc.HORIZONTAL;
		gbc.gridwidth = gbc.REMAINDER;
		if ( _label != null ) {
			_myPanel.add( new JLabel( _label ), gbc );
			_myPanel.add(
				Box.createVerticalStrut(UIFactory.getComponentSpace()), gbc);
		}
		gbc.fill = gbc.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;

		String[] BAList =  getBckAvailable() ;
		if (BAList != null) {
			_list = new JList( BAList );
			_list.setCellRenderer( new BackendCellRenderer() );
			_list.setSelectedIndex( 0 );
			JScrollPane scroll = new JScrollPane(_list);
			scroll.setBorder( UITools.createLoweredBorder() );
			_myPanel.add( scroll, gbc );
		}
	}

	/**
	 * Some list component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void valueChanged(ListSelectionEvent e) {
        Debug.println(7, "BckListPanel.valueChanged: " +
					  _list.getSelectedValue());
		AbstractDialog dlg = getAbstractDialog();
		if ( dlg != null ) {
			dlg.setOKButtonEnabled( _list.getSelectedValue() != null );
		}
    }

	protected String[] getBckAvailable() {
		Vector v = new Vector(1);
		for(int i=0; i < _bList.length; i++) {			
			Debug.println("BckListPanel.getBckAvailable() bck = " +  _bList[i]);
			if( notIn( _bList[i])){
				v.addElement( _bList[i] );
			}
		}
		String[] nL = new String[v.size()];
		v.toArray( nL );
		return ( nL );
	}
	
	protected boolean notIn( String BName ) {
		if ( _bIn == null ) {
			return( true );
		}
		Debug.println("BckListPanel.notIn()");
		Debug.println("------------------->" + BName );
		boolean b = true;
		for(int i =0; (i < _bIn.length) && b ; i++ ) {
			Debug.println("------------------->" + _bIn[i] );
			b =  BName.compareToIgnoreCase( _bIn[i] ) != 0;
		}
		return ( b );
	}
		

    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

    public void okCallback() {
		/* No state to preserve */
		Object OVals[] = _list.getSelectedValues();
		Vector v = new Vector(1);

		if (( OVals == null ) || (OVals.length == 0)) {
			_item = null;
		}

		String[] bcList = new String[OVals.length];
		int i=0;
		

		for(i =0; i <  OVals.length; i++) {			
			v.addElement( OVals[i].toString() );
		}
	    v.toArray(bcList);				
		_item = (String[])bcList;
		if ( _item == null ) {
			return;
		}
		clearDirtyFlag();
		hideDialog();
    }

    public Object getSelectedItem () {
		return _item;
	}

	class BackendCellRenderer extends DefaultListCellRenderer {  
		// This is the only method defined by ListCellRenderer.  We just
		// reconfigure the Jlabel each time we're called.
		
		public Component getListCellRendererComponent(
													  JList list,
													  Object value,            // value to display
													  int index,               // cell index
													  boolean isSelected,      // is the cell selected
													  boolean cellHasFocus)    // the list and the cell have the focus
			{
				AbstractDialog dlg = getAbstractDialog();
				if ( dlg != null ) {
					if (list.isSelectionEmpty()) {
						dlg.setOKButtonEnabled( false );
					} else {
						dlg.setOKButtonEnabled( true );
					}
				}

				return super.getListCellRendererComponent(list,
														  value,
														  index,
														  isSelected,
														  cellHasFocus);
			}
	}

	private JList _list;
	private String[] _bList;
	private String[] _bIn;
	private String _label = null;
	private String[] _item = null;
	private final static String _section = "mappingtree-bcklist";
}
