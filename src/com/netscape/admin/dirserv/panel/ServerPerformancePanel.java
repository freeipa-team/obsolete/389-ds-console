/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import netscape.ldap.LDAPException;

/**
 *	Settings for server performance tuning.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ServerPerformancePanel extends BlankPanel {

	public ServerPerformancePanel(IDSModel model) {
		super(model, _section);
		_helpToken = "configuration-system-performance-help";
		_refreshWhenSelect = false;
	}

	public void init() {
		if (_isInitialized) {
			return;
		}
        boolean useMaxDescriptors = true;

        String osTest = getModel().getServerInfo().getAdminOS();
        if (osTest.startsWith("Windows NT") ||
            (osTest.indexOf("AIX") != -1)) {
            useMaxDescriptors = false;
        }

		_tfSizeLimit = makeNumericalJTextField(_section, "sizeLimit");
		JLabel _lSizeLimit = makeJLabel(_section, "sizeLimit");
		_lSizeLimit.setLabelFor(_tfSizeLimit);
		JLabel lSizeLimitUnit = makeJLabel(_section, "sizeLimit-unit");
		lSizeLimitUnit.setLabelFor(_tfSizeLimit);

		_tfTimeLimit = makeNumericalJTextField(_section, "timeLimit");
		_lTimeLimit = makeJLabel(_section, "timeLimit");
		_lTimeLimit.setLabelFor(_tfTimeLimit);
		JLabel lTimeLimitUnit = makeJLabel(_section, "timeLimit-unit");
		lTimeLimitUnit.setLabelFor(_tfTimeLimit);

		_tfIdleTimeOut = makeNumericalJTextField(_section, "idletimeout");
		JLabel _lIdleTimeOut = makeJLabel(_section, "idletimeout");
		_lIdleTimeOut.setLabelFor(_tfIdleTimeOut);
		JLabel lIdleTimeOutUnit = makeJLabel(_section, "idletimeout-unit");
		lIdleTimeOutUnit.setLabelFor(_tfIdleTimeOut);

        if (useMaxDescriptors) {
			/* rmarco : bug 312207 */
			_tfMaxDescriptors = makeJTextField(_section, "maxdescriptors");
			_tfMaxDescriptors.setHorizontalAlignment(JTextField.LEFT);
			_lMaxDescriptors = makeJLabel(_section, "maxdescriptors"); 
			_lMaxDescriptors.setLabelFor(_tfMaxDescriptors);
			
        }

		DSEntrySet entries = getDSEntrySet();

        DSEntryInteger sizeDSEntry = new DSEntryInteger(null, _tfSizeLimit,
            _lSizeLimit, -1, INT_MAX_VAL, 1);
        entries.add(SERVERSETTINGS_DN, SIZE_LIMIT_ATTR_NAM, sizeDSEntry);
        setComponentTable(_tfSizeLimit, sizeDSEntry);

        DSEntryInteger timeDSEntry = new DSEntryInteger(null, _tfTimeLimit,
            _lTimeLimit, -1, INT_MAX_VAL, 1);
        entries.add(SERVERSETTINGS_DN, TIME_LIMIT_ATTR_NAM, timeDSEntry);
        setComponentTable(_tfTimeLimit, timeDSEntry);

        DSEntryInteger idleDSEntry = new DSEntryInteger(null, _tfIdleTimeOut,
            _lIdleTimeOut, 0, INT_MAX_VAL, 1);
        entries.add(SERVERSETTINGS_DN, IDLE_TIME_OUT_ATTR_NAM, idleDSEntry);
        setComponentTable(_tfIdleTimeOut, idleDSEntry);

        if (useMaxDescriptors) {
            _maxDescDSEntry =
				new DSEntryInteger(null,
								   _tfMaxDescriptors, _lMaxDescriptors,
								   1, MAX_DESCRIPTORS, 1);
            entries.add(SERVERSETTINGS_DN, MAX_DESCRIPTORS_ATTR_NAM,
						_maxDescDSEntry);
            setComponentTable(_tfMaxDescriptors, _maxDescDSEntry);
        }

        JPanel panel = _myPanel;
        panel.setLayout( new GridBagLayout() );

		addEntryField( panel, _lSizeLimit, _tfSizeLimit, lSizeLimitUnit );
		addEntryField( panel, _lTimeLimit, _tfTimeLimit, lTimeLimitUnit );
		addEntryField( panel, _lIdleTimeOut, _tfIdleTimeOut, lIdleTimeOutUnit );

        if (useMaxDescriptors) {
			addEntryField( panel, _lMaxDescriptors, _tfMaxDescriptors );
        }
		addBottomGlue();
		
		_isInitialized = true;
	}

    /**
	 * Called on errors saving entry changes to the directory.
	 * Process locally if the error is because the max file descriptors of the
	 * server was exceeded.
	 *
	 * @param lde LDAPException on saving
	 */
    protected boolean processSaveErrors( LDAPException lde ) {
		if ( lde.getLDAPResultCode() == lde.UNWILLING_TO_PERFORM ) {
			String msg = lde.getLDAPErrorMessage();
			if ( (msg != null) && (msg.indexOf("maxdescriptors") > 0) ) {
				/* Find the max descriptor token */
				int endIndex = msg.indexOf( " (the current process limit)" );
				if ( endIndex >= 0 ) {
					int startIndex = endIndex;
 					while( msg.charAt( startIndex ) == ' ' ) {
 						startIndex--;
 					}
					while( msg.charAt( startIndex ) != ' ' ) {
						startIndex--;
					}
					String value = msg.substring( startIndex+1, endIndex );
					try {
						MAX_DESCRIPTORS = Integer.parseInt( value );
						_maxDescDSEntry.setMaxValue( MAX_DESCRIPTORS );
						/* Force revalidation and repaint of controls */
						actionPerformed( new ActionEvent( this, 1, "VALIDATE" ) );
						setDirtyFlag();
					} catch ( Exception e ) {
						Debug.println( "ServerPerformancePanel.processSaveErrors: " +
									   "parsing <" + value + ">, " + e );
					}
					String[] args = { _tfMaxDescriptors.getText().trim(), value };
					DSUtil.showErrorDialog( getModel().getFrame(),
											"104", args );
					return false;
				}
			}
		}
		return super.processSaveErrors( lde );
	}

    private JTextField _tfSizeLimit;
    private JLabel _lSizeLimit;
	
    private JTextField _tfTimeLimit;
    private JLabel _lTimeLimit;
	
    private JTextField _tfIdleTimeOut;
    private JLabel _lIdleTimeOut;
	
    private JTextField _tfMaxDescriptors;
    private JLabel _lMaxDescriptors;
	
	private JCheckBox	_cbIsReadOnly;

	private DSEntryInteger _maxDescDSEntry;

	private static final String SERVERSETTINGS_DN = "cn=config";
	private static final int INT_MAX_VAL = 2147483647;
	private static final String TIME_LIMIT_ATTR_NAM = "nsslapd-timeLimit";
	private static final String SIZE_LIMIT_ATTR_NAM = "nsslapd-sizelimit";
	private static final String LOOK_LIMIT_ATTR_NAM = "nsslapd-looklimit";
	private static final String IDLE_TIME_OUT_ATTR_NAM = "nsslapd-idletimeout";
	private static final String MAX_DESCRIPTORS_ATTR_NAM =
	                                                  "nsslapd-maxdescriptors";
	private static final String READ_ONLY_ATTR_NAM = "nsslapd-readonly";
	private int MAX_DESCRIPTORS = 65535;
    private static final int PORT_NUM_MIN_VAL = 1;
    private static final int PORT_NUM_MAX_VAL = 65535;

	private ResourceSet _resource = DSUtil._resource;
    private static final String _section = "serverperformance";
}

