/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.border.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;


/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class LDBMPluginSettingPanel extends BlankPanel {

    public LDBMPluginSettingPanel(IDSModel model, String dnEntry) {
		super(model, "pldbmsetting",true);
		_helpToken = "configuration-database-plugin-setting-help";
		_dnEntry = dnEntry;
		_refreshWhenSelect = false;
    }
    
	public void init() {
	
	if (_isInitialized) {
		return;
	}
	/* DB cache memory available for all entries/indexes */
	_tfMaxCacheSize = makeNumericalJTextField( _section, "maxCacheSize" );
	_lMaxCacheSize = makeJLabel( _section,"maxCacheSize" );
	_lMaxCacheSize.setLabelFor(_tfMaxCacheSize);
	_cbMaxCacheSizeUnit = makeJComboBox(_section, "maxCacheSizeUnits", null);

	/* Max entries the server checks in response to a search */
	_tfLookLimit = makeNumericalJTextField( _section, "lookLimit" );
	_lLookLimit = makeJLabel(_section, "lookLimit");
	_lLookLimit.setLabelFor(_tfLookLimit);
	JLabel lLookLimitUnit = makeJLabel(_section, "lookLimit-unit");
	lLookLimitUnit.setLabelFor(_tfLookLimit);
	
	/* Specifies database files permissions */
	_tfModeFile = makeNumericalJTextField( _section, "fileMode" );
	_lModeFile = makeJLabel(_section, "fileMode");
	_lModeFile.setLabelFor(_tfModeFile);
	JLabel lModeFileUnit = makeJLabel(_section, "fileMode-unit");
	lModeFileUnit.setLabelFor(_tfModeFile);

	/* LDBM DB location */
	_tfRootDatabaseLoc = makeJTextField(_section,"RootDatabaseLoc");
	_lRootDatabaseLoc = makeJLabel(_section,"RootDatabaseLoc");
	_lRootDatabaseLoc.setLabelFor(_tfRootDatabaseLoc);	

	/* add check box for autocache size */
	_cbAutoCacheSize = makeJCheckBox(_section,"autoCacheSize", false);
	
	/* import cache size field */
	_tfImportCacheSize = makeJTextField(_section,"importCacheSize");
	_lImportCacheSize = makeJLabel( _section,"importCacheSize" );
	_lImportCacheSize.setLabelFor(_tfImportCacheSize);
	_cbImportCacheSizeUnit = makeJComboBox(_section, 
	                                       "importCacheSizeUnits", null);

	
	Debug.println("LDBMPluginSettingPanel.init() _dnEntry :" + _dnEntry);

	DSEntrySet entries = getDSEntrySet();	
	
	// Pull down menu for the dbcache size unit
	_cacheSizeDSEntry = new DSEntryLong(null, _tfMaxCacheSize, _lMaxCacheSize,
	                          CACHE_SIZE_NUM_MIN_VAL, CACHE_SIZE_NUM_MAX_VAL, 1);
	entries.add(_dnEntry, PLUGINDB_CACHE_SIZE_ATTR_NAM, _cacheSizeDSEntry);
	setComponentTable(_tfMaxCacheSize, _cacheSizeDSEntry);

        DSEntryLong lookDSEntry = new DSEntryLong(null, 
            _tfLookLimit, _lLookLimit,
            LIMIT_MIN_VAL, LIMIT_MAX_VAL, 1);
		entries.add(_dnEntry, LOOK_LIMIT_ATTR_NAM, lookDSEntry);
        setComponentTable(_tfLookLimit, lookDSEntry);

        DSEntryInteger modeDSEntry = new DSEntryInteger(null, 
            _tfModeFile, _lModeFile,
            MODE_MIN_VAL, MODE_MAX_VAL, 1);
		entries.add(_dnEntry, FILE_MODE_ATTR_NAME, modeDSEntry);
        setComponentTable(_tfModeFile, modeDSEntry);

        DSEntryFile fileEntry = new DSEntryFile(null, _tfRootDatabaseLoc,
            _lRootDatabaseLoc, false, isLocal());
        entries.add(_dnEntry, DB_FILE_LOC_ATTR_NAME, fileEntry); 
        setComponentTable(_tfRootDatabaseLoc, fileEntry);
        
        DSEntryBoolean autoDSEntry = new DSEntryBoolean(null, _cbAutoCacheSize, "-1", "0");
        entries.add(_dnEntry, IMPORT_AUTO_CACHE_SIZE_ATTR_NAME, autoDSEntry);
        setComponentTable(_cbAutoCacheSize, autoDSEntry);

        // Pull down menu for the import cache size unit
        _importCacheSizeDSEntry = new DSEntryLong(null, 
                                           _tfImportCacheSize, _lImportCacheSize,
                                           CACHE_SIZE_NUM_MIN_VAL,
                                           CACHE_SIZE_NUM_MAX_VAL, 1);
        entries.add(_dnEntry, DB_IMPORT_CACHE_SIZE_ATTR_NAM,
                    _importCacheSizeDSEntry);
        setComponentTable(_tfImportCacheSize, _importCacheSizeDSEntry);

        JPanel panel = _myPanel;
        panel.setLayout( new GridBagLayout() );

		addEntryField( panel, _lMaxCacheSize, _tfMaxCacheSize, 
		               _cbMaxCacheSizeUnit );
		addEntryField( panel, _lLookLimit, _tfLookLimit, lLookLimitUnit );
		addEntryField( panel, _lModeFile, _tfModeFile, lModeFileUnit );
		addEntryField( panel, new JLabel(""), _cbAutoCacheSize, new JLabel("") );
		addEntryField( panel, _lImportCacheSize, _tfImportCacheSize,
		               _cbImportCacheSizeUnit );
		addBottomGlue();
		_isInitialized = true;
	}

    private void enableCacheSize() {
        boolean enabled = !_cbAutoCacheSize.isSelected();
        _tfImportCacheSize.setEnabled(enabled);
        _lImportCacheSize.setEnabled(enabled);       
    }

    private void setCacheSizeUnit(JComboBox unit, DSEntryLong dsentry) {
        String selectedUnit = (String)unit.getSelectedItem();
        Debug.println("actionPerformed: selected MaxCacheSizeUnit: " +
                      selectedUnit);
        if (selectedUnit.equalsIgnoreCase("bytes")) {
            dsentry.setScaleFactor(1);
            dsentry.show();
        } else if (selectedUnit.equalsIgnoreCase("KB")) {
            dsentry.setScaleFactor(1024);
            dsentry.show();
        } else if (selectedUnit.equalsIgnoreCase("MB")) {
            dsentry.setScaleFactor(1024*1024);
            dsentry.show();
        } else if (selectedUnit.equalsIgnoreCase("GB")) {
            dsentry.setScaleFactor(1024*1024*1024);
            dsentry.show();
        } else {
            Debug.println("actionPerformed: ignore unknown unit: " + 
                          selectedUnit);
        }
    }

    /* (non-Javadoc)
     * @see com.netscape.admin.dirserv.panel.BlankPanel#refresh()
     */
    public boolean refresh() {
        // TODO Auto-generated method stub
        boolean status = super.refresh();
        enableCacheSize();
        return status;
    }

    /* (non-Javadoc)
     * @see com.netscape.admin.dirserv.panel.BlankPanel#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(_cbAutoCacheSize)) {
            enableCacheSize();
        } else if (e.getSource().equals(_cbMaxCacheSizeUnit)) {
            setCacheSizeUnit(_cbMaxCacheSizeUnit, _cacheSizeDSEntry);
        } else if (e.getSource().equals(_cbImportCacheSizeUnit)) {
            setCacheSizeUnit(_cbImportCacheSizeUnit, _importCacheSizeDSEntry);
        }
        super.actionPerformed(e);
    }

    private JTextField _tfMaxCacheSize;
    private JLabel _lMaxCacheSize;
    private JComboBox _cbMaxCacheSizeUnit;
    private static final String[] UCOMBO_ENTRIES = { "bytes", "KB", "MB", "GB" };
    private DSEntryLong _cacheSizeDSEntry;

    private JTextField _tfLookLimit;
    private JLabel _lLookLimit;

    private JTextField _tfModeFile;
    private JLabel _lModeFile;
	
    private JTextField _tfRootDatabaseLoc;
    private JLabel _lRootDatabaseLoc;
    
    private JCheckBox _cbAutoCacheSize;

    private JTextField _tfImportCacheSize;
    private JLabel _lImportCacheSize;
    private JComboBox  _cbImportCacheSizeUnit;
    private DSEntryLong _importCacheSizeDSEntry;

    private static final String PLUGINDB_CACHE_SIZE_ATTR_NAM = "nsslapd-dbcachesize";
    private static final String LOOK_LIMIT_ATTR_NAM = "nsslapd-lookthroughlimit";
    private static final String FILE_MODE_ATTR_NAME = "nsslapd-mode";
    private static final String DB_FILE_LOC_ATTR_NAME = "nsslapd-directory";
    private static final String DB_IMPORT_CACHE_SIZE_ATTR_NAM = "nsslapd-import-cachesize";
    private static final String IMPORT_AUTO_CACHE_SIZE_ATTR_NAME = "nsslapd-import-cache-autosize";
    private static final long LIMIT_MIN_VAL = -1;
    private static final long LIMIT_MAX_VAL = Long.MAX_VALUE;
    private static final long CACHE_SIZE_NUM_MIN_VAL = 1; // e.g., 1MB
    private static final long CACHE_SIZE_NUM_MAX_VAL = LIMIT_MAX_VAL;
    private static final int MODE_MIN_VAL = 0;
    private static final int MODE_MAX_VAL = 777;
    
	private ResourceSet _resource = DSUtil._resource;
    private static final String _section = "pldbmsetting";
	private String _dnEntry;
}
