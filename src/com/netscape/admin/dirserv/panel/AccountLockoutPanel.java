/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Panel for Directory Server account lockout settings page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date        9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class AccountLockoutPanel extends BlankPanel {
    public AccountLockoutPanel(IDSModel model) {
        super(model, "accountlockout");
		_helpToken = "configuration-database-accountlockout-help";
		_refreshWhenSelect = false;
		_policyspecdn = CONFIG_DN;
    }

    public AccountLockoutPanel(IDSModel model, String dn, int mode) {
   		super(model, "accountlockout");
		_helpToken = "configuration-browser-accountlockout-help";
		_refreshWhenSelect = false;

		_dn = dn;
		_mode = mode;
		_parentdn = new DN(dn).getParent().toString();

		_containerdn = ( _mode == FINEGRAINED_USER ?
							("cn=" + CONTAINER_CN + "," + _parentdn) :
							("cn=" + CONTAINER_CN + "," + _dn) );

		_policyspecdn = "cn=\"cn=" + USER_SPEC_CN + "," +
						_dn + "\"," + _containerdn;

		_costemplatedn = "cn=\"cn=" + COS_TEMPL_CN + "," +
						_dn + "\"," + _containerdn;

		_cosspecdn = "cn=" + COS_SPEC_CN + "," + _dn;

		// Use a DSEntrySet that doesn't complain if entry/attr are non-existant
		setDSEntrySet(new DSEntrySet(false, true));
	}

    public void init() {

		if (_isInitialized) {
			return;
		}
        _myPanel.setLayout(new GridBagLayout());
        createLockoutArea((Container)_myPanel);

		addBottomGlue();		
		_isInitialized = true;
	}

	public void select(IResourceObject parent, IPage viewInstance) {
		if (_isInitialized && (_mode == FINEGRAINED_USER || _mode == FINEGRAINED_SUBTREE) ) {
			_fineGrainedCheckbox = verifyFGCheckbox();
			_cbLockout.setEnabled( _fineGrainedCheckbox );
			enableLockout();
			enableDuration();
		} 
		super.select(parent, viewInstance);
	} 

	private boolean verifyFGCheckbox() {
		boolean enabled = false;
		DSTabbedPanel tabContainer = (DSTabbedPanel)getTabbedPanel();
		PasswordPolicyPanel passwordPanel = (PasswordPolicyPanel)tabContainer._tabbedPane.getComponentAt(0);
		enabled = passwordPanel.getFGCheckboxStatus();
		return enabled;
	} 

    protected void createLockoutArea( Container myContainer) {
		_items = new Vector();

        // No Lockout checkbox
		_cbLockout = makeJCheckBox(_section , "lockout", false);

        JLabel lockoutAfterLabel = makeJLabel(_section, "lockoutafter");
		_items.addElement( lockoutAfterLabel );

        // Lockout value
        _tfLockoutValue = makeNumericalJTextField(_section, "lockoutafter2");
		lockoutAfterLabel.setLabelFor(_tfLockoutValue);
		_items.addElement( _tfLockoutValue );

        JLabel lockoutLabel2 = makeJLabel(_section, "lockoutafter2");
		_items.addElement( lockoutLabel2 );

        JLabel resetAfterLabel1 = makeJLabel(_section, "resetafter");
		_items.addElement( resetAfterLabel1 );		

        _tfResetAfterValue = makeNumericalJTextField(_section, "resetafter");
		resetAfterLabel1.setLabelFor(_tfResetAfterValue);
		_items.addElement( _tfResetAfterValue );

        JLabel resetAfterLabel2 = makeJLabel(_section, "resetafter2");
		_items.addElement( resetAfterLabel2 );

        ButtonGroup durationButtonGroup = new ButtonGroup();
        // Lockout Forever Radio Button
        _rbLockoutForever = makeJRadioButton(_section, "lockoutforever");
        durationButtonGroup.add(_rbLockoutForever);
		_items.addElement( _rbLockoutForever );

        // Lockout Minutes Radio Button
        _rbLockoutMinutes = makeJRadioButton(_section, "lockoutduration");
        durationButtonGroup.add(_rbLockoutMinutes);
		_items.addElement( _rbLockoutMinutes );

        _tfLockoutMinutesValue = makeNumericalJTextField(_section,
                                                "lockoutduration", null, 8);
		_items.addElement( _tfLockoutMinutesValue );

        _lockoutMinutesLabel = 
			makeJLabel(_section, "lockoutduration2");
		_items.addElement( _lockoutMinutesLabel );

        DSEntrySet entries = getDSEntrySet();

		DurationEntryBoolean lockDSEntry = new DurationEntryBoolean(null, _cbLockout);
		entries.add( _policyspecdn, LOCKOUT_ATTR_NAME,
		            lockDSEntry);
		setComponentTable(_cbLockout, lockDSEntry);

        DSEntryInteger lockoutDSEntry =
			new DSEntryInteger(null, _tfLockoutValue,
            lockoutAfterLabel, LOCKOUT_NUM_MIN_VAL, LOCKOUT_NUM_MAX_VAL, 1);
        entries.add( _policyspecdn, LOCKOUT_NUM_ATTR_NAME, lockoutDSEntry );
        setComponentTable(_tfLockoutValue, lockoutDSEntry);

        DSEntryInteger resetDSEntry =
			new DSEntryInteger(null, _tfResetAfterValue,
							   resetAfterLabel1,
							   LOCKOUT_RESET_NUM_MIN_VAL,
							   DSUtil.epochConstraint(LOCKOUT_RESET_NUM_MAX_VAL, MIN_TO_DAY_FACTOR), 
                               MIN_TO_SEC_FACTOR);
        entries.add( _policyspecdn, LOCKOUT_RESET_NUM_ATTR_NAME,
            resetDSEntry );
        setComponentTable(_tfResetAfterValue, resetDSEntry);

		LockoutEntryBoolean lo =
			new LockoutEntryBoolean("off", _rbLockoutMinutes);
        entries.add( _policyspecdn, LOCKOUT_FOREVER_ATTR_NAME, lo );
		setComponentTable(_rbLockoutForever, lo);

        DSEntryInteger lockoutDSEntry1 = new DSEntryInteger(null,
            _tfLockoutMinutesValue, _lockoutMinutesLabel,  
            LOCKOUT_DURATION_NUM_MIN_VAL, DSUtil.epochConstraint(LOCKOUT_DURATION_NUM_MAX_VAL, MIN_TO_DAY_FACTOR), 
            MIN_TO_SEC_FACTOR);
        entries.add( _policyspecdn, LOCKOUT_DURATION_NUM_ATTR_NAME,
            lockoutDSEntry1 );
        setComponentTable(_tfLockoutMinutesValue, lockoutDSEntry1);

        // create containers and layouts
        JPanel lockoutPanel = (JPanel)myContainer;
        GridBagConstraints gbc = getGBC();
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
        lockoutPanel.add(_cbLockout, gbc);
        gbc.fill = gbc.HORIZONTAL;

        // Lockout Area Container
        JPanel lockoutSubPanel =
			new GroupPanel( _resource.getString(_section,
												"lockout-title"));
		lockoutSubPanel.setLayout( new GridBagLayout() );
        lockoutPanel.add(lockoutSubPanel, gbc);
		_items.addElement( lockoutSubPanel );

		int between = UIFactory.getComponentSpace();
		int apart = UIFactory.getSeparatedSpace();

        // Lockout Radio Button Sub Area
        JPanel lockoutSubSubPanel =
            new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        lockoutSubPanel.add(lockoutSubSubPanel, gbc);
        lockoutSubSubPanel.add(lockoutAfterLabel);
        lockoutSubSubPanel.add(Box.createHorizontalStrut(between));
        lockoutSubSubPanel.add(_tfLockoutValue);
        lockoutSubSubPanel.add(Box.createHorizontalStrut(between));
        lockoutSubSubPanel.add(lockoutLabel2);

        // Reset Failure Count Sub Area
        JPanel resetAfterPanel =
			new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        lockoutSubPanel.add(resetAfterPanel, gbc);
        resetAfterLabel1.setHorizontalAlignment(SwingConstants.LEFT);
        resetAfterPanel.add(resetAfterLabel1);
        resetAfterPanel.add(Box.createHorizontalStrut(between));
        resetAfterPanel.add(_tfResetAfterValue);
        resetAfterPanel.add(Box.createHorizontalStrut(between));
        resetAfterLabel2.setHorizontalAlignment(SwingConstants.LEFT);
        resetAfterPanel.add(resetAfterLabel2);

        _rbLockoutForever.setHorizontalAlignment(SwingConstants.LEFT);
        gbc.fill = gbc.NONE;
        lockoutSubPanel.add(_rbLockoutForever, gbc);
        gbc.fill = gbc.HORIZONTAL;

        // Lockout Minutes Radio Button Sub Area
        JPanel minutesSubPanel = new JPanel(
            new FlowLayout(FlowLayout.LEFT, 0, 0)
        );
		gbc.insets.bottom = UIFactory.getComponentSpace();
        lockoutSubPanel.add(minutesSubPanel, gbc);
        _rbLockoutMinutes.setHorizontalAlignment(SwingConstants.LEFT);
        minutesSubPanel.add(_rbLockoutMinutes);
        minutesSubPanel.add(Box.createHorizontalStrut(apart));
        minutesSubPanel.add(_tfLockoutMinutesValue);
        minutesSubPanel.add(Box.createHorizontalStrut(between));
        minutesSubPanel.add(_lockoutMinutesLabel);
    }

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(_cbLockout)) {
			enableLockout();
			enableDuration();
        } else if (e.getSource().equals(_rbLockoutForever) || 
                   e.getSource().equals(_rbLockoutMinutes)) {
            enableDuration ();
        }
    

		super.actionPerformed(e);  // Always want to pass event to parent
		                           // to enable buttons.
    }

    private void enableLockout() {
		boolean enable = ( _cbLockout.isSelected() && _fineGrainedCheckbox);
		Enumeration e = _items.elements();
		while( e.hasMoreElements() ) {
			JComponent comp = (JComponent)e.nextElement();
			comp.setEnabled(enable);
			comp.repaint();
		}
    }

    private void enableDuration (){
        boolean enable = ( _rbLockoutMinutes.isSelected() && _fineGrainedCheckbox);

        _tfLockoutMinutesValue.setEnabled (enable);
        _tfLockoutMinutesValue.repaint ();
    
        _lockoutMinutesLabel.setEnabled (enable);
        _lockoutMinutesLabel.repaint ();
    }
	class LockoutEntryBoolean extends DSEntryBoolean {
		LockoutEntryBoolean(String s, AbstractButton c) {
			super( s, c );
		}
	    public void show() {
			String val = getModel(0);
		    if (!val.equals (_trueValue)){
                _rbLockoutForever.setSelected (true);
                _rbLockoutMinutes.setSelected (false);
            } else{
                _rbLockoutForever.setSelected (false);
                _rbLockoutMinutes.setSelected (true);
            }

            viewInitialized ();
			enableLockout();
		}
	}
    class DurationEntryBoolean extends DSEntryBoolean {
        DurationEntryBoolean(String s, AbstractButton c) {
			super( s, c );
		}
	    public void show() {
			super.show();
			enableDuration();
		}

    }

	// Flag that tells us if the PWP spec entry exists or not
	private boolean _spec_exists = false;

	// Target dn for fine grained password policy
	private String _dn;

	// Target dn's parent
	private String _parentdn;

	// dn of password policy container for target dn
	private String _containerdn;

	// dn of password specification object for target dn
	private String _policyspecdn;

	// dn of CoS template, for subtree password policy
	private String _costemplatedn;

	// dn of CoS specification, for subtree password policy
	private String _cosspecdn;

	// What mode the panel is in: SERVER or FINEGRAINED
	private int _mode;

	// Indicates whether fine-grained checkbox checked in PasswordPolicyPanel
	private boolean _fineGrainedCheckbox = true;

	private static final int SERVER = 1;
	private static final int FINEGRAINED_USER = 2;
	private static final int FINEGRAINED_SUBTREE = 3;

	private static final String CONTAINER_CN = "nsPwPolicyContainer";
	private static final String COS_SPEC_CN = "nsPwPolicy_CoS";
	private static final String USER_SPEC_CN = "nsPwPolicyEntry";
	private static final String COS_TEMPL_CN = "nsPwTemplateEntry";
	private static final String PWPSUBENTRY_ATTR_NAME = "pwdpolicysubentry";

    private JCheckBox       _cbLockout;
    private JTextField      _tfLockoutValue;
    private JTextField      _tfResetAfterValue;
    private JRadioButton    _rbLockoutForever;
    private JRadioButton    _rbLockoutMinutes;
    private JTextField      _tfLockoutMinutesValue;
    private Vector          _items;
    private JLabel          _lockoutMinutesLabel;

    private static final String CONFIG_DN = "cn=config";

    private static final String LOCKOUT_ATTR_NAME = "passwordLockout";
    private static final String LOCKOUT_NUM_ATTR_NAME = "passwordMaxFailure";
    private static final int LOCKOUT_NUM_MIN_VAL = 1;
    private static final int LOCKOUT_NUM_MAX_VAL = 32767;
    private static final String LOCKOUT_RESET_NUM_ATTR_NAME = 
	                                  "passwordResetFailureCount";
    private static final int LOCKOUT_RESET_NUM_MIN_VAL = 1;
    private static final int LOCKOUT_RESET_NUM_MAX_VAL = 35791394; 
    private static final String LOCKOUT_FOREVER_ATTR_NAME = "passwordUnlock";
    private static final String LOCKOUT_DURATION_NUM_ATTR_NAME =
	                                  "passwordLockoutDuration";
    private static final int LOCKOUT_DURATION_NUM_MIN_VAL = 1;
    private static final int LOCKOUT_DURATION_NUM_MAX_VAL = 35791394;

    private static final int MIN_TO_SEC_FACTOR = 60;     
    private static final int MIN_TO_DAY_FACTOR = 1440;
	
    // name of panel in resource file
    static final public String _section = "passwordpolicy";
	static final private ResourceSet _resource = DSUtil._resource;
}

