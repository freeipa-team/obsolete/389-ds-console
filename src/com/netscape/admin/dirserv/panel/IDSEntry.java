/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.Enumeration;
import javax.swing.JComponent;

/**
 * This class represents the application's "view" of an attribute stored in
 * an entry of a directory server.  A DSEntry consists of two parts: a model
 * and a view.  The model consists of a list of values contained in the
 * corresponding attribute in the directory.  The values are stored in the
 * local representation, which is not necessarily the same as the
 * representation in the directory, but it may be, especially for string
 * values.  The view consists of one or more GUI widgets derived from
 * JComponent.  DSEntry is an abstract class, and the abstract part is
 * how the user interacts with the attribute.  Thus, the show(), validate(),
 * and store() methods must be supplied by the application programmer.
 * The DSEntry and DSEntrySet take care of the database interaction for the
 * programmer.  Here is an example of using DSEntrySet.add() for an
 * attribute which is represented in the directory by a text string and
 * in the GUI by three radio buttons.  The radio buttons are created
 * separately.
 * <PRE>
 * ...
 * JComponent[] views = {_rbReset, _rbMayChange, _rbMayNotChange};
 * entries.add(CHANGE_DN, CHANGE_ATTR_NAME,
 *     new DSEntry("must", views) {
 *         public void show() {
 *             String val = getModel(0);
 *             JRadioButton rb0 = (JRadioButton)getView(0);
 *             JRadioButton rb1 = (JRadioButton)getView(1);
 *             JRadioButton rb2 = (JRadioButton)getView(2);
 *             if (val.equals("must")) {
 *                 rb0.setSelected(true);
 *             } else if (val.equals("may")) {
 *                 rb1.setSelected(true);
 *             } else { // no
 *                 rb2.setSelected(true);
 *             }
 *         }
 *         public void store() {
 *             JRadioButton rb0 = (JRadioButton)getView(0);
 *             JRadioButton rb1 = (JRadioButton)getView(1);
 *             if (rb0.isSelected()) {
 *                 setModelAt("must", 0);
 *             } else if (rb1.isSelected()) {
 *                 setModelAt("may", 0);
 *             } else {
 *                 setModelAt("no", 0);
 *             }
 *         }
 *         // its pretty hard to invalidate a radio button . . .
 *         public boolean validate() {return true;}
 *     }
 * );
 * </PRE>
 * There are also several concrete subclasses of DSEntry which implement
 * the model/view functionality for various types of common widget/value
 * combinations.  The DSEntryBoolean class implements a DSEntry for a
 * boolean DS value represented in the GUI by some sort of toggle button.
 * The DSEntryInteger class implements a DSEntry for an integer DS value
 * represented in the GUI by a text entry field with some built in
 * validation.
 *
 * @version %I%, %G%
 * @author Richard Megginson
 * @see com.netscape.admin.dirserv.panel.DSEntrySet
 * @see com.netscape.admin.dirserv.panel.DSEntryBoolean
 * @see com.netscape.admin.dirserv.panel.DSEntryInteger
 * @see com.netscape.admin.dirserv.panel.DSFileField
 */
public interface IDSEntry { // no scope; used only in this package

    /**
     * This method updates the view to reflect the contents of the
     * local model.  This must be defined for the particular model/view combo
     *
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#show
     */
    public abstract void show();

    /**
     * This method updates the model to reflect changes in the view.
     * This must be defined for the particular model/view combo
     *
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#store
     */
    public abstract void store();

    /**
     * This method checks the view to see if the values entered by the
     * user are valid.  This method will be called before store().  If
     * the return value is true, this means that the view is valid.  If
     * the return value is false, the view is invalid, and no further
     * processing of other DSEntries in this set should be done.  This method
     * is responsible for displaying error messages to the user, setting the
     * focus in the offending field, etc.
     *
     * @return true - view is valid; false - view is invalid, do not continue
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#store
     */
	public abstract boolean dsValidate();

    /**
     * This method converts the String representation of the attribute
     * stored in the Directory Server to the local model representation
     *
     * @param String remoteValue This is the value as stored in the DS
     */
    public abstract void remoteToLocal(String remoteValue);
    
    /**
     * This method is used to convert multivalued attributes
     * stored in the Directory Server to the local model representation
     *
     * @param remoteValue An enumeration of Strings
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#show
     */
    public abstract void remoteToLocal(Enumeration remoteValues);
    
    /**
     * This method is used to get the local values in the form used to
     * store the values in the Directory Server
     *
     * @return An array of String values
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#store
     */
    public abstract String[] localToRemote();

}
