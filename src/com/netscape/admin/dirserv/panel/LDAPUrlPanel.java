/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.management.nmclf.*;
import java.net.URL;
import netscape.ldap.*;
import netscape.ldap.util.*;


/**
 * LDAPUrlPanel
 * 
 *
 * @version 1.0
 * @author rmarco
 **/
public class LDAPUrlPanel extends BlankPanel 
    implements SuiConstants {
    public LDAPUrlPanel( IDSModel model ) {
	super( model, _section );
	_helpToken = "configuration-construct-new-url-dbox-help";
	_model = model;
	setTitle( DSUtil._resource.getString( _section, "title" ));
    }

    public void init() {

	GridBagLayout Mabag = new GridBagLayout();
	GridBagConstraints Magbc = new GridBagConstraints() ;
        Magbc.gridx      = 0;
        Magbc.gridy      = 0;
	//        Magbc.gridwidth  = Magbc.REMAINDER;
        Magbc.gridwidth	 = 1;
        Magbc.gridheight = 1;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
        Magbc.fill       = Magbc.BOTH;
        Magbc.anchor     = Magbc.CENTER;
        Magbc.insets     = new Insets(0,0,0,0);
        Magbc.ipadx = 0;
        Magbc.ipady = 0;
	   	
	_myPanel.setLayout(Mabag);


	_refNewHostLabel = makeJLabel(_section, "new-host");
	_refNewHostLabel.resetKeyboardActions();
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.NONE;
	Magbc.anchor = Magbc.EAST; 
	Magbc.gridy++;
	_myPanel.add( _refNewHostLabel,Magbc);		

	_refNewHostText = makeJTextField(_section, "new-host");
	Magbc.gridx = 1;
        Magbc.weightx    = 10;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.BOTH;
	Magbc.insets     = new Insets( 0,
				       0,
				       COMPONENT_SPACE,
				       0);
	_refNewHostText.setColumns( 16 );
	_myPanel.add( _refNewHostText,Magbc);		

	_refNewPortLabel = makeJLabel(_section, "new-port");
	_refNewPortLabel.resetKeyboardActions();
	Magbc.gridx = 2;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.NONE;
	Magbc.anchor = Magbc.EAST;
	Magbc.insets     = new Insets( 0,
				       DIFFERENT_COMPONENT_SPACE,
				       COMPONENT_SPACE,
				       0);		
	_myPanel.add( _refNewPortLabel,Magbc);		

	_refNewPortText = makeJTextField(_section, "new-port");
	Magbc.gridx = 3;
        Magbc.weightx    = 1;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.BOTH;
	_refNewPortText.setColumns( 5 );
	_myPanel.add( _refNewPortText,Magbc);		

	_refTargetDNLabel =  makeJLabel(_section, "new-targetDN");
	_refTargetDNLabel.resetKeyboardActions();
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.NONE;
	Magbc.anchor = Magbc.EAST;		
	Magbc.gridy++;
	Magbc.insets     = new Insets( 0,
				       0,
				       COMPONENT_SPACE,
				       0);
	_myPanel.add( _refTargetDNLabel,Magbc);		
		
	_refTargetDNText = makeJTextField(_section, "new-targetDN");
	Magbc.gridx = 1;
        Magbc.weightx    = 1;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.BOTH;
	// Magbc.anchor = Magbc.WEST;		
	Magbc.insets     = new Insets( 0,
				       0,
				       COMPONENT_SPACE,
				       0);
	Magbc.gridwidth  = Magbc.REMAINDER;
	_myPanel.add( _refTargetDNText,Magbc);

	JPanel Mapanel = new GroupPanel(
					DSUtil._resource.getString( _section, "result-title" ));
	Magbc.gridx = 0;
	Magbc.gridy++;
	Magbc.insets     = new Insets( 0,
				       0,
				       0,
				       0);

	Magbc.gridwidth = Magbc.REMAINDER;
	_myPanel.add( Mapanel, Magbc );

	_refResult = new JLabel(" ");
	_refResult.setToolTipText(DSUtil._resource.getString("construct-ldap-url","ttip"));
	Mapanel.add( _refResult );

	addBottomGlue();
    }

    private void setOkay( boolean ok ) {
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( ok );
	}
    }

    private void checkOkay() {
		String dn = _refTargetDNText.getText();
		boolean isDnOk = false;
		if (!DSUtil.isValidDN(dn)) {
			setChangeState(_refTargetDNLabel, CHANGE_STATE_ERROR );
		} else {			
			setChangeState(_refTargetDNLabel, CHANGE_STATE_UNMODIFIED);
			isDnOk = true;
		}
		String aa =  _refNewHostText.getText();
		boolean ok = ( aa != null) && ( aa.trim().length() > 0 ) && isDnOk;
		setOkay( ok );
    }
		
    public void actionPerformed(ActionEvent e) {
	super.actionPerformed(e);
	checkOkay();
    }

    public void resetCallback() {
	/* No state to preserve */
	clearDirtyFlag();
	hideDialog();
    }

    public void okCallback() {
	/* No state to preserve */
	clearDirtyFlag();
	_item = getVal();		
	Debug.println( "LDAPUrlPanel.okCallback: " + _item );
	hideDialog();
    }

	

    public void changedUpdate(DocumentEvent e) {
	updRes();
	super.changedUpdate( e );
	checkOkay();
    }

    public void removeUpdate(DocumentEvent e) {
	updRes();
	super.removeUpdate( e );
	checkOkay();
    }
    
    public void insertUpdate(DocumentEvent e) {
	updRes();
	super.insertUpdate( e );
	checkOkay();
    }

    private void updRes(){		
	netscape.ldap.LDAPUrl u = getVal();
	if( u != null ) {
	    _refResult.setText( u.toString() );
	} else {
	    _refResult.setText( DSUtil._resource.getString( _section, "invalid-url-label")); 
	}
    }

    public Object getLDAPUrl() {
	return _item;
    }

    protected  netscape.ldap.LDAPUrl  getVal() {
	String host = _refNewHostText.getText();
	String port = _refNewPortText.getText();
	String tdn = _refTargetDNText.getText();
	StringBuffer lu = new StringBuffer( "ldap://" ); 
	
	if (( host != null ) && ( port.trim().length() > 0)) {
	    lu.append( host );
	}

	if (( port != null ) && ( port.trim().length() > 0)) {
	    lu.append( ":" );
	    lu.append( port );
	}
	
	if (( tdn != null ) && ( tdn.trim().length() > 0)) {		
		if (!DSUtil.isValidDN(tdn)) {
			return( null );
		}
	    lu.append( "/" );
	    lu.append( tdn );
	}
	
	try {
	    netscape.ldap.LDAPUrl nu = 
		new  netscape.ldap.LDAPUrl( lu.toString() );
	    return ( nu ) ;
	} catch ( Exception e ) {
	    return( null );
	}		
    }


    private IDSModel			_model = null;

    private JLabel				_refNewHostLabel;
    private JTextField			_refNewHostText;
    private JLabel				_refNewPortLabel;
    private JTextField			_refNewPortText;
    private JLabel				_refTargetDNLabel;
    private JTextField			_refTargetDNText;
    private JLabel				_refResult;
    private Object				_item = null;



    private final static String _section = "construct-ldap-url";
    private static ResourceSet resource = DSUtil._resource;
}
