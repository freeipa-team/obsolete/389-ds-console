/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import netscape.ldap.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class SchemaMatchingRulesPanel extends SchemaAttributesPanel
    implements ListSelectionListener {
	public SchemaMatchingRulesPanel(IDSModel model) {
		super( model, _section, false );
		_helpToken = "configuration-schema-mrule-help";
		_htSyntaxStrings = super.getSyntaxStrings(getModel().getSchema());
		_refreshWhenSelect = false;
	}

	public void init() {
		getModel().setWaitCursor( true );
        EmptyBorder border = new EmptyBorder(getBorderInsets());
        _myPanel.setBorder(border);
        _myPanel.setLayout(new GridBagLayout());
		resetGBC();

		JLabel introLabel = makeJLabel(_section, "intro");
        _gbc.gridwidth = _gbc.REMAINDER;
		_gbc.insets = new Insets( 0, 0, UIFactory.getDifferentSpace(), 0 );
		_myPanel.add(introLabel, _gbc);
//  		_myPanel.add( Box.createVerticalStrut(UIFactory.getDifferentSpace()),
//  					  _gbc );

		JComponent scroll = createAttributeListArea((Container)_myPanel);
		introLabel.setLabelFor(_stdTable);
		populateTables();
		JComponent buttonPanel = createButtonsPanel();
		resetGBC();
		_gbc.fill = _gbc.HORIZONTAL;
		_gbc.gridwidth = _gbc.REMAINDER;
		_gbc.weightx = 1.0;
		int different = UIFactory.getDifferentSpace();
		_gbc.weighty = 0.0;
		_gbc.insets.top = different;
        _myPanel.add(buttonPanel, _gbc);
		getModel().setWaitCursor( false );
	}

	protected JComponent createAttributeListArea(Container myContainer) {
        // layout the widgets
		resetGBC();
		_gbc.gridwidth = _gbc.REMAINDER;
		_gbc.anchor = GridBagConstraints.WEST;
		_gbc.fill = GridBagConstraints.HORIZONTAL;

		String dummy = new String();
		Object[][] colHeaders = {
			{DSUtil._resource.getString(_section, "namecolumn-label"), dummy.getClass()},
		    {DSUtil._resource.getString(_section, "oidcolumn-label"), dummy.getClass()},
		    {DSUtil._resource.getString(_section, "syntaxcolumn-label"), dummy.getClass()},
		    {DSUtil._resource.getString(_section, "description-label"), dummy.getClass()}
		};

        _stdTableModel = new AttrTableModel( colHeaders );		
	    _stdTable = new JTable( _stdTableModel );
		setColumnWidths( _stdTable, _widths );
        Dimension d = new Dimension();
        d.height = _stdTable.getRowHeight() * TABLE_ROWS + 8;
		d.width = 0;
		for( int i = 0; i < _widths.length; i++ ) {
			d.width += _widths[i];
		}
		_gbc.fill = GridBagConstraints.BOTH;
		_gbc.weighty = 0.7;
		_gbc.weightx = 1.0;
		JScrollPane scrollPane =
			addTableInScrollPane( _stdTable, myContainer, d );
		return scrollPane;
	}

    protected void populateTables() {
		_stdTableModel.removeAllRows();
		LDAPSchema sch = getModel().getSchema();
		if (sch == null)
		    return;
		synchronized (sch) {
            for (Enumeration e = sch.getMatchingRules();
				 e.hasMoreElements();) {
                LDAPMatchingRuleSchema las =
					(LDAPMatchingRuleSchema)e.nextElement();
				addAttributeRow( las );
            }
        }
		setColumnWidths( _stdTable, _widths );
		_stdTable.repaint();
	}

    private void addAttributeRow( LDAPMatchingRuleSchema las ) {
		Object row[] = new Object[4];
		row[0] = las.getName();
		row[1] = las.getID();
		if ( _htSyntaxStrings != null ) {			
			row[2] = (String)_htSyntaxStrings.get(las.getSyntaxString());
		} else {
			row[2] = "";
		}
		row[3] = las.getDescription();
		//Debug.println( "Adding row <" + row[0] + "> <" + row[1] +
		//					"> <" + row[2] + "> <" + row[3] + ">" );
		_stdTableModel.addRow(row);
	}

	protected JComponent createButtonsPanel() {
        _helpButton = makeJButton("general", "Help");

		JButton[] buttons = { _helpButton };
		return  UIFactory.makeJButtonPanel( buttons );
    }

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source.equals(_helpButton)) {
			helpCallback();
        }
    }
	
	protected void setColumnWidths( JTable table, int[] widths ) {
		if (table == null)
			return;

		Enumeration en = table.getColumnModel().getColumns();
		int i = 0;
		while( en.hasMoreElements() ) {
			TableColumn col = (TableColumn)en.nextElement();
			col.setWidth( widths[i] );
			col.setResizable( true );			
			i++;
		}
	}



    private JButton _helpButton = null;
    private JTable _stdTable = null;
    private AttrTableModel _stdTableModel = null;

	private static int _widths[] = { 190, 155, 90, 65 };
	private static int TABLE_ROWS = 18;
    // name of panel in resource file
	static final private String _section = "matchingrules"; 
	private static Hashtable _htSyntaxStrings = null;
}
