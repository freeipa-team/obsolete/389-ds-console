/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.net.MalformedURLException;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.event.*;

import netscape.ldap.*;

import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.nmclf.SuiConstants;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.PasswordDialog;
import com.netscape.admin.dirserv.browser.LDAPConnectionPool;


public class ReferralEditor extends AbstractDialog 
implements DocumentListener, ActionListener, ListSelectionListener, SuiConstants {
	public ReferralEditor(JFrame frame,
						  LDAPConnection ldc,
						  String dn,
						  LDAPConnectionPool cpool) {
		super(frame, "", true, OK | CANCEL | HELP);
		_frame = frame;
		_ldc = ldc;
		_dn = dn;
		_save = true;
		_connectionPool = cpool;
		_errorDuringInit = false;
		init();
	}

	public void packAndShow() {
		if (!_errorDuringInit) {
			pack();
			show();
		}
	}

	public boolean isObjectModified() {
		return _isObjectModified;
	}

	public boolean isAuthModified() {
		return _isAuthModified;
	}

	/* Returns the LDAPModifications to do to the entry
	   If the modifications have been done returns null.
	   */
	public LDAPModificationSet getModifications() {
		return _set;
	}

	public void actionPerformed(ActionEvent e) {		
		Object source = e.getSource();
		
		if (source.equals(_bConstruct)) {
			actionConstructReferral();
		} else if (source.equals(_bAdd)) {
			actionAddOrChangeReferral(false /* add */);
		} else if (source.equals(_bDelete)) {
			actionDeleteReferral();
		} else if (source.equals(_bChange)) {
			actionAddOrChangeReferral(true /* change */);
		} else if (source.equals(_bAuthentication)) {
			actionAuthentication();
		} 
		checkEnablingState();
			
		// Move the focus to _tfNewReferral when 
		// _cbReferralsEnabled is selected. 
		if (source.equals(_cbReferralsEnabled)) {
			if (_cbReferralsEnabled.isSelected()) {
				_tfNewReferral.requestFocus();
			}
		}
	}

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void changedUpdate(DocumentEvent e) {
		insertUpdate(e);
    }
	
	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void removeUpdate(DocumentEvent e) {
		insertUpdate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void insertUpdate(DocumentEvent e) {
		checkEnablingState();
    }

	protected void init() {		
		setTitle(_resource.getString(_section, "title-label", _dn));
		getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "title-description"));
		layoutComponents();
		Thread thread = new Thread(new Runnable() {
			public void run() {
				initValuesFromServer();				
			}
		});
		thread.start();		
	}

	protected void layoutComponents() {
		JPanel panel = new JPanel(new GridBagLayout());
		setComponent(panel);
		
		_cbReferralsEnabled = UIFactory.makeJCheckBox(this,
													 _section,
													 "enable",
													 false,
													 _resource);
		_lReferrals = new JLabel(_resource.getString(_section,
													"values-label"));
		_lNewReferral = new JLabel(_resource.getString(_section,
													"new-value-label"));
		_lBindDN = new JLabel();
		_tfNewReferral = UIFactory.makeJTextField(this,
											   -1);
		_bDelete = UIFactory.makeJButton(this,
											_section,
											"delete",
											_resource);
		_bConstruct = UIFactory.makeJButton(this,
											_section,
											"construct",
											_resource);
		_bAdd = UIFactory.makeJButton(this,
											_section,
											"add",
											_resource);
		_bChange = UIFactory.makeJButton(this,
											_section,
											"change",
											_resource);
		_bAuthentication = UIFactory.makeJButton(this,
											_section,
											"authentication",
											_resource);
		/* List of referrals */				
		_referralListData = new DefaultListModel();	
		_referralList = new JList( _referralListData );
		_lBindDN.setLabelFor(_referralList);
		_referralList.setVisibleRowCount(REFERRAL_LIST_ROWS);
		_referralList.addListSelectionListener(this);

		_lReferrals.setLabelFor(_referralList);
		_lNewReferral.setLabelFor(_tfNewReferral);

		JScrollPane scrollReferral = new JScrollPane();
		scrollReferral.getViewport().setView(_referralList);		

        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
		gbc.insets     = new Insets(0, 0, COMPONENT_SPACE, COMPONENT_SPACE);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;

		panel.add(_cbReferralsEnabled, gbc);

		gbc.gridy++;
		gbc.insets.bottom = 0;
		panel.add(_lReferrals, gbc);		

		gbc.gridy++;
		gbc.gridheight = 2;
		gbc.weightx=1.0;
		gbc.weighty=1.0;
        gbc.fill = gbc.BOTH;
		gbc.insets.bottom = COMPONENT_SPACE;
		panel.add(scrollReferral, gbc);		

		gbc.gridx++;
		gbc.gridheight = 1;
		gbc.weightx=0.0;
		gbc.weighty=0.0;
        gbc.fill = gbc.HORIZONTAL;
		gbc.insets.right = 0;
		panel.add(_bDelete, gbc);

		gbc.gridy++;
		panel.add(_bChange, gbc);

		gbc.gridx = 0;
		gbc.gridy++;
		gbc.insets.bottom = 0;
		gbc.insets.right = COMPONENT_SPACE;
		panel.add(_lNewReferral, gbc);
		
		gbc.gridy++;
		gbc.weightx=1.0;
		panel.add(_tfNewReferral, gbc);

		gbc.gridx++;
		gbc.weightx=0.0;
		gbc.insets.bottom = COMPONENT_SPACE;
		gbc.insets.right = 0;
		panel.add(_bAdd, gbc);
		
		gbc.gridy++;
		panel.add(_bConstruct, gbc);

		gbc.gridx=0;
		gbc.gridy++;
		gbc.weightx=1.0;
		gbc.anchor=gbc.WEST;
		gbc.insets.right = COMPONENT_SPACE;
		panel.add(_lBindDN, gbc);
		
		gbc.gridx++;
		gbc.weightx=0.0;
		gbc.insets.right = 0;
		panel.add(_bAuthentication, gbc);
		
		setOKButtonEnabled(false);
		setDefaultButton(_bAdd);

		_cbReferralsEnabled.setEnabled(false);
		_tfNewReferral.setEnabled(false);
		_bConstruct.setEnabled(false);		
	}



	protected void initValuesFromServer() {
		_saveReferralsEnabled = false;
		_saveReferralValues = new Vector();
		String[] attrs = {"ref", "objectclass"};
		LDAPSearchConstraints searchConstraints = (LDAPSearchConstraints)_ldc.getSearchConstraints().clone();
		searchConstraints.setServerControls(new LDAPControl( LDAPControl.MANAGEDSAIT, true, null ));
		try {
			LDAPEntry entry = _ldc.read(_dn, attrs, searchConstraints);
			if (entry != null) {
				LDAPAttribute attr = entry.getAttribute(attrs[0]);
				if (attr != null) {
					Enumeration values = attr.getStringValues();
					if (values.hasMoreElements()) {												
						_saveReferralsEnabled = true;						
						_hasReferralObjectclass = true;						
					}
					while(values.hasMoreElements()) {
						_saveReferralValues.addElement(values.nextElement());
					}
				}
				if (!_saveReferralsEnabled) {
					/* Check if it has the objectclass value "referral" */
					attr = entry.getAttribute(attrs[1]);
					if (attr != null) {
						Enumeration values = attr.getStringValues();
						while (values.hasMoreElements() &&
							   !_hasReferralObjectclass) {
							String value = (String)values.nextElement();						
							if (value.equalsIgnoreCase("referral")) {
								_hasReferralObjectclass = true;
							}
						}
					}
				}
			}
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					_cbReferralsEnabled.setEnabled(true);
					if (_saveReferralsEnabled) {
						_cbReferralsEnabled.setSelected(true);					
						_referralListData.clear();
						for (int i=0; i<_saveReferralValues.size(); i++) {
							_referralListData.addElement(_saveReferralValues.elementAt(i));
						}
					}
					checkEnablingState();
				}
			});
		} catch (LDAPException ex) {		
			final LDAPException e = ex;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					/* The entry could not be read... */
					String[] args = {
						_dn,
						getLDAPErrorMessage(e)};
					_errorDuringInit = true;
					setVisible(false);					
					DSUtil.showErrorDialog( _frame,
											"reading-object-error-title",
											"reading-object-error-msg",
											args,
											_section );
				}
			});
		}				
	}

	protected void okInvoked() {		
		boolean toDelete = !_cbReferralsEnabled.isSelected() ||
			(_referralListData.getSize()< 1);
		
		LDAPModificationSet set = new LDAPModificationSet();				

		if (!_hasReferralObjectclass &&
			!toDelete) {			   																	   
			set.add(LDAPModification.ADD, new LDAPAttribute("objectclass", "referral"));			
		}

		int modType;
		LDAPAttribute attr;
		if (toDelete) {
			modType = LDAPModification.DELETE;
			attr = new LDAPAttribute("ref");
		} else if (_saveReferralsEnabled) {
			modType = LDAPModification.REPLACE;
			attr = new LDAPAttribute("ref");
			Enumeration e = _referralListData.elements();
			while( e.hasMoreElements() ) {			   
				attr.addValue((String)e.nextElement());					
			}
		} else {
			modType = LDAPModification.ADD;
			attr = new LDAPAttribute("ref");
			Enumeration e = _referralListData.elements();
			while( e.hasMoreElements() ) {			   
				attr.addValue((String)e.nextElement());					
			}
		}									
		set.add(modType, attr);
		
		if (_hasReferralObjectclass &&
			toDelete) {
			set.add(LDAPModification.DELETE, new LDAPAttribute("objectclass", "referral"));
		}
		
		if (_save) {
			LDAPSearchConstraints searchConstraints = (LDAPSearchConstraints)_ldc.getSearchConstraints().clone();
			searchConstraints.setServerControls(new LDAPControl( LDAPControl.MANAGEDSAIT, true, null ));
			try {
				_ldc.modify(_dn, set, (LDAPConstraints)searchConstraints);
				_isObjectModified = true;
			} catch (LDAPException lde) {									
				/* The entry could not be modified... */
				String[] args = {
					_dn,
					getLDAPErrorMessage(lde)};
				
				DSUtil.showErrorDialog( this,
										"modifying-object-error-title",
										"modifying-object-error-msg",
										args,
										_section );
			}						
		} else {
			if (set.size() > 0) {
				_isObjectModified = true;
				_set = set;
			}
		}
		
		super.okInvoked();
	}

	protected void helpInvoked() {		
		DSUtil.help("configuration-set-referral");
	}

	protected void checkEnablingState() {
		if (_cbReferralsEnabled.isSelected()) {			
			_tfNewReferral.setEnabled(true);			
			_bDelete.setEnabled(true);
			_bConstruct.setEnabled(true);
			_bAdd.setEnabled(true);
			_tfNewReferral.setEnabled(true);

			boolean isEnableDirty = !_saveReferralsEnabled;			
								
			boolean isReferralListDirty = isReferralListDirty();			
			
			_bAdd.setEnabled(!_tfNewReferral.getText().trim().equals(""));

			boolean isDirty = isEnableDirty || isReferralListDirty || _isAuthModified;

			boolean isValid = _referralListData.getSize() > 0;
			
			setOKButtonEnabled(isDirty && isValid);			
		} else {						

			_tfNewReferral.setEnabled(false);			
			_bDelete.setEnabled(false);
			_bConstruct.setEnabled(false);
			_bAdd.setEnabled(false);
			_tfNewReferral.setEnabled(false);
			
			boolean isDirty = _saveReferralsEnabled;
			setOKButtonEnabled(isDirty);			
		}
		_referralList.setBackground(_tfNewReferral.getBackground());
		checkDeleteReferralButton();
		checkChangeReferralButton();
		checkAuthReferralButton();				
	}

	/**
	 * This method tells wether the user has modified or not the list of additional referrals
	 * for this replica.  It compares the size of what is in the server and what we have in the
	 * list, and after that, it compares the content.
	 */
	private boolean isReferralListDirty() {
		boolean isDirty = false;
		if (_referralList != null) {	
			isDirty = _referralListData.getSize() != _saveReferralValues.size();
			if (!isDirty) {
				Enumeration e = _referralListData.elements();
				while (e.hasMoreElements() &&
					   !isDirty) {
					isDirty = !_saveReferralValues.contains(e.nextElement());
				}		
			}
		}
		return isDirty;
	}

	/**
	  * Method called when the user clicks on 'Delete' button.
	  * It removes the selected referrals from the list of referrals
	  */
	private void actionDeleteReferral() {
		Object[] referralToDelete = _referralList.getSelectedValues();
		if (referralToDelete != null) {
			for (int i=0; i < referralToDelete.length; i++) {
				_referralListData.removeElement(referralToDelete[i]);
			}
		}
		_tfNewReferral.setText("");
	}

	/**
	  * Method called when the user clicks on 'Add' or 'Change' button.
	  * It adds the LDAP URL in the list of URLs.
	  */
	private void actionAddOrChangeReferral(boolean change) {
		String newReferral = _tfNewReferral.getText().trim();
		boolean addReferral = true;
		/* IF the URL provided has not a recognised syntax, we display a warning dialog */
		if (!DSUtil.isValidLDAPUrl(newReferral)) {
			int wantToContinue = DSUtil.showConfirmationDialog( _frame,
																"LDAPUrl-no-good", 
																(String)null,
																"mappingtree-referral");
			if (wantToContinue != JOptionPane.YES_OPTION) {
				addReferral = false;								
			}
		}
		if (addReferral) {
			if (change) {
				int i = _referralList.getSelectedIndex(); // Cannot be -1
				_referralListData.setElementAt(newReferral, i);
			}
			else { // add
				_referralListData.addElement(newReferral);
			}
			_tfNewReferral.setText("");
			_bAdd.setEnabled(false);
			_bChange.setEnabled(false);
		}
	}

	/**
	  * Method called when the user clicks on 'Construct...' button.
	  * It displays the LDAP URL editor, if the user creates an URL (does click on 'OK' in the editor),
	  * we update the text field that contains the candidate LDAP URL referral (we don't update directly the referral
	  * list).
	  */
	private void actionConstructReferral() {		
		LDAPUrlDialog dlg =
			new LDAPUrlDialog( _frame );	   		
		dlg.packAndShow();
		
		LDAPUrl url = dlg.getLDAPUrl();
		
	    if ( url != null ) {
			_tfNewReferral.setText( url.getUrl() );			
		}
	}

	/**
	  * Method called when the user clicks on 'Authentication' button.
	  */
	private void actionAuthentication() {
		// Make an LDAPUrl from the selected url
		// We can assume that the selected URL is well-formed.
		int i = _referralList.getSelectedIndex(); // Cannot be -1
		String selectedUrl = (String)_referralListData.elementAt(i);
		LDAPUrl selectedUrlObj;
		try {
			selectedUrlObj = new LDAPUrl(selectedUrl);
		}
		catch(MalformedURLException x) {
			throw new IllegalStateException("This URL should be correct: " + selectedUrl);
		}
		
		// Query the connection pool to know which authentication
		// is used for the selected URL and setup a dialog.
		String authDN = _connectionPool.getAuthDN(selectedUrlObj);
		String authPassword = _connectionPool.getAuthPassword(selectedUrlObj);
		boolean again;
		do {
			PasswordDialog dlg = new PasswordDialog(_frame,
					authDN, authPassword,
					_resource.getString(_section, "authentication-dialog-title"));
			dlg.show();
			if (dlg.isCancel()) {
				again = false;
			}
			else {
				setBusyCursor(true);
				try {
					_connectionPool.registerAuth(
								selectedUrlObj,
								dlg.getUsername(),
								dlg.getPassword(),
								true);
					again = false;
					_isAuthModified = true;
				}
				catch(LDAPException x) {
					again = true;
					DSUtil.showLDAPErrorDialog( _frame, x, "109-title" );
				}
				setBusyCursor(false);
			}
		}
		while (again);
	}

	public void valueChanged(ListSelectionEvent e) {
		checkNewReferralTextField();
		checkDeleteReferralButton();				
		checkChangeReferralButton();
		checkAuthReferralButton();
	}
	
	/**
	  * Method used to enable/disable the delete bind dn button.
	  * The button is enabled if there is something selected in the list
	  */
	protected void checkDeleteReferralButton() {
		_bDelete.setEnabled( ! _referralList.isSelectionEmpty());
	}

	/**
	  * Method used to enable/disable the change bind dn button.
	  * The button is enabled if there is a *single* item selected in the list
	  */
	protected void checkChangeReferralButton() {
		int min = _referralList.getMinSelectionIndex();
		int max = _referralList.getMaxSelectionIndex();
		int selectionSize = (min == -1) ? 0 : (max - min + 1);
		int textLength = _tfNewReferral.getText().trim().length();
		_bChange.setEnabled((selectionSize == 1) && (textLength >= 1));
	}

	/**
	  * Method used to enable/disable the change authentication button.
	  * The button is enabled if there is a *single* item selected in the list.
	  * Also keep the bind dn up to date.
	  */
	protected void checkAuthReferralButton() {
		int min = _referralList.getMinSelectionIndex();
		int max = _referralList.getMaxSelectionIndex();
		int selectionSize = (min == -1) ? 0 : (max - min + 1);
		boolean enableAuth;
		String bindDNText;
		
		if (selectionSize == 1) {
			try {
				String urlString = (String)_referralListData.elementAt(min);
				LDAPUrl url = new LDAPUrl(urlString);
				String bindDN = _connectionPool.getAuthDN(url);
				if (bindDN == null) {
					bindDNText = _resource.getString(_section, "anonymous-dn-label");
				}
				else {
					bindDNText = _resource.getString(_section, "bind-dn-label", bindDN);
				}
				enableAuth = true;
			}
			catch(MalformedURLException x) {
				enableAuth = false;
				bindDNText = _resource.getString(_section, "non-standard-url-label");
			}
		}
		else {
			enableAuth = false;
			bindDNText = "";
		}
		_bAuthentication.setEnabled(enableAuth);
		_lBindDN.setText(bindDNText);
	}

	/**
	  * Method used to align _tfNewReferral with the selected item.
	  */
	protected void checkNewReferralTextField() {
		int min = _referralList.getMinSelectionIndex();
		int max = _referralList.getMaxSelectionIndex();
		int selectionSize = (min == -1) ? 0 : (max - min + 1);
		String newReferralText;
		
		if (selectionSize == 1) {
			newReferralText = (String)_referralListData.elementAt(min);
		}
		else {
			newReferralText = "";
		}
		_tfNewReferral.setText(newReferralText);
	}

    private String getLDAPErrorMessage(LDAPException e) {
	    String ldapError;
		String ldapMessage;

		ldapError =  e.errorCodeToString();
		ldapMessage = e.getLDAPErrorMessage();
		if ((ldapMessage != null) &&
			(ldapMessage.length() > 0)) {
			ldapError = ldapError + ". "+ldapMessage;
		}
		return ldapError;
	}

	boolean _errorDuringInit;

	boolean _isObjectModified = false;
	boolean _isAuthModified = false;
	JCheckBox _cbReferralsEnabled;
	JTextField _tfNewReferral;

	JButton _bConstruct;
	JButton _bAdd;
	JButton _bDelete;
	JButton _bChange;
	JButton _bAuthentication;

	JLabel _lReferrals;
	JLabel _lNewReferral;
	JLabel _lBindDN;

	JList _referralList;	
	DefaultListModel _referralListData;
	
	ResourceSet _resource = DSUtil._resource;
	Vector _saveReferralValues;
	boolean _saveReferralsEnabled;
	boolean _hasReferralObjectclass = false;

	LDAPConnection _ldc;
	JFrame _frame;
	String _dn;	
	boolean _save;
	LDAPConnectionPool _connectionPool;

	LDAPModificationSet _set;

	String _section = "ReferralEditor";
	final static int REFERRAL_LIST_ROWS = 5;
}


