/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.*;
import java.io.File;
import java.util.Date;
import javax.swing.*;
import com.netscape.management.client.Framework;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.preferences.Preferences;
import com.netscape.management.client.preferences.PreferenceManager;
import com.netscape.admin.dirserv.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	03/08/98
 * @see     com.netscape.admin.dirserv
 */
public class ConfirmationPreferencesPanel extends BlankPanel {

	public ConfirmationPreferencesPanel(IDSModel model) {
		super( model, _section );
		_helpToken = "preferences-confirmation-help";
	}

	public void init() {
        _myPanel.setLayout( new GridBagLayout() );
        GridBagConstraints gbc = getGBC();
        gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = (Insets)gbc.insets.clone();
		gbc.insets.left = 0;
		gbc.insets.top = 0;
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;

		JTextArea area =
			makeMultiLineLabel(3, 30,
							   _resource.getString(_section,"intro-label"));
 		_myPanel.add(area, _gbc);
		_myPanel.add(
			Box.createVerticalStrut(UIFactory.getDifferentSpace()), _gbc);

		gbc.fill = gbc.NONE;

		/* See if there is a personal preference set */
		PreferenceManager pm =
			PreferenceManager.getPreferenceManager( Framework.IDENTIFIER,
													Framework.VERSION );
		_confirmationPreferences =
			pm.getPreferences( GlobalConstants.PREFERENCES_CONFIRM );

		_cbPrefs = new JCheckBox[_prefs.length];
		for( int i = 0; i < _prefs.length; i++ ) {
			_cbPrefs[i] = UIFactory.makeJCheckBox( this, _section,
												   _prefs[i], true );
			if ( _confirmationPreferences != null ) {
				_cbPrefs[i].setSelected(
					_confirmationPreferences.getBoolean( _prefs[i], true ) );
			}
			_myPanel.add( _cbPrefs[i], gbc );
		}

		AbstractDialog dlg = getAbstractDialog();
		if ( dlg != null ) {
			dlg.setOKButtonEnabled( false );
		}
	}
	
	public void actionPerformed(ActionEvent e) {		
		/* See if something has been modified */
		boolean isDirty = false;		
		for (int i=0; (i<_cbPrefs.length) && !isDirty; i++) {
			isDirty = _confirmationPreferences.getBoolean(_prefs[i], true) != _cbPrefs[i].isSelected();
		}

		AbstractDialog dlg = getAbstractDialog();
		if ( dlg != null ) {
			dlg.setOKButtonEnabled( isDirty );
		}			
	}


    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

    public void okCallback() {
		if ( _confirmationPreferences != null ) {
			for( int i = 0; i < _prefs.length; i++ ) {
				_confirmationPreferences.set(
					_prefs[i], _cbPrefs[i].isSelected() );
			}
		}
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

    public int getPreferenceCount() {
		return _cbPrefs.length;
	}

    public String getPreferenceName( int index ) {
		return _prefs[index];
	}

    public boolean getPreferenceState( int index ) {
		return _cbPrefs[index].isSelected();
	}

    public static void main( String[] args ) {
		Debug.setTrace( true );
		try { 
			UIManager.setLookAndFeel(
				"com.netscape.management.nmclf.SuiLookAndFeel" ); 
		} catch (Exception e) { 
			System.err.println("Cannot load nmc look and feel."); 
		} 
		BlankPanel child =
			new ConfirmationPreferencesPanel( new DefaultResourceModel() );
		SimpleDialog dlg =
			new SimpleDialog( new JFrame(), child );
	    dlg.packAndShow();		
    }	

	static final private String[] _prefs = {
	GlobalConstants.PREFERENCES_CONFIRM_DELETE_OBJECTCLASS,
	GlobalConstants.PREFERENCES_CONFIRM_DELETE_ATTRIBUTE,
	GlobalConstants.PREFERENCES_CONFIRM_DELETE_ENTRY,
	GlobalConstants.PREFERENCES_CONFIRM_DELETE_SUBTREE,
	GlobalConstants.PREFERENCES_CONFIRM_DELETE_INDEX,
	GlobalConstants.PREFERENCES_CONFIRM_DELETE_AGREEMENT,
	GlobalConstants.PREFERENCES_CONFIRM_MODIFY_CHANGELOG,
	GlobalConstants.PREFERENCES_CONFIRM_STOP_SERVER,
	GlobalConstants.PREFERENCES_CONFIRM_OVERWRITE_DATABASE};

	private JCheckBox[] _cbPrefs;
    private Preferences _confirmationPreferences = null;

	private ResourceSet _resource = DSUtil._resource;
	private final static String _section = "confirmation";
}
