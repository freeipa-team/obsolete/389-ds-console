/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.Vector;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;

/**
 * NewObjectClassPanel
 * 
 *
 * @version 1.0
 * @author rweltman
 **/
public class NewEncAttrPanel extends SelectionPanel {
	public NewEncAttrPanel( IDSModel model, Vector attrs ) {
		super( model, section, attrs,
			   resource.getString(section, "title"),
//			   resource.getString(section, "msg"),
			   null,
			   HELPTOKEN );
	}
    private final static String section = "attr-enc-attr-add-box";
	private static ResourceSet resource = DSUtil._resource;
	private static final String HELPTOKEN =
	    "configuration-database-attrenc-add-dbox-help";
}
