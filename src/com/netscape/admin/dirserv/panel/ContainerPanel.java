/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IChangeClient;
import com.netscape.admin.dirserv.IDSResourceSelectionListener;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.Debug;

/**
 * Container Panel for Directory Server resource page. Provides optional
 * Save/Reset/Help buttons. Used to house a client panel, or extended
 * by DSTabbedPanel to house several client panels.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ContainerPanel extends JPanel
                           implements IChangeClient,
	                                  ActionListener,
                                      IDSResourceSelectionListener,
                                      ChangeListener {
	public ContainerPanel(IDSModel model, boolean doButtons) {
		super();
        setLayout( new BorderLayout( 0, 0 ) );
		if ( doButtons ) {
			JPanel bottom = createButtonPanel();
			add( bottom, "South" );
			_bOK.setEnabled(false);
			_bReset.setEnabled(false);
		}
		if ( System.getProperty( "ShowColors" ) != null )
			setBackground( Color.yellow );

        model.addIDSResourceSelectionListener(this);
        model.addChangeClient(this);
		_dsModel = model;
    }
    
	public ContainerPanel(IDSModel model) {
		this( model, true );
	}

	public ContainerPanel(IDSModel model, BlankPanel panel,
						  boolean doButtons) {
		this( model, doButtons );
		add( panel, "Center" );
		_selectedPanel = panel;
	}

    /*
     * Support for IDSResourceListenerModel
     *
     */

    /**
      * Called when the TabbedPane is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
		if ( _verbose ) {
			Debug.println("ContainerPanel.select: " + getClass().getName() );
		}
        _selectedPanel = (BlankPanel)getSelectedPanel();
    }
    
    /**
      * Called when the object is unselected.
      */
    public void unselect(IResourceObject parent, IPage viewInstance) {
    }

	public void stateChanged(ChangeEvent e) {
	    JTabbedPane tabs = (JTabbedPane)e.getSource();
		Component c = tabs.getSelectedComponent();
		if ( _verbose ) {
			Debug.println("ContainerPanel.stateChanged: " +
						  getClass().getName() +
						  " to " + c.getClass().getName() );
		}
		BlankPanel p = (BlankPanel)getSelectedPanel();
        if ( c instanceof BlankPanel ) {
			_selectedPanel = (BlankPanel)c;
			if ( p != c )
				p.unselect( null, null );
			_selectedPanel.select(null, null);
    	}
	}

    protected BlankPanel getSelectedPanel() {
		return _selectedPanel;
	}

    protected JPanel createButtonPanel() {
		_bOK = UIFactory.makeJButton(this, "general", "Apply");
		_bReset = UIFactory.makeJButton(this, "general", "Reset");
		
		// A workaroud for the bug 539754, disable requestFocus() 
		// calls on Save and Reset buttons.
		_bOK.setRequestFocusEnabled(false);
		_bReset.setRequestFocusEnabled(false);

		_bHelp = UIFactory.makeJButton(this, "general", "Help");

		JButton[] buttons = { _bOK, _bReset, _bHelp };
		_buttonPanel = UIFactory.makeJButtonPanel( buttons, true );
		JPanel p = new JPanel();
		p.setLayout( new BorderLayout() );
		p.add( "Center", _buttonPanel );
		p.add( "North",
			   Box.createVerticalStrut(UIFactory.getDifferentSpace()) );
		return p;
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( "refresh" ) ) {
			BlankPanel p = (BlankPanel)getSelectedPanel();
			if ( p != null )
				 p.refresh();
		} else if (e.getSource().equals(_bOK)) {
			okCallback();
		} else if (e.getSource().equals(_bReset)) {
			resetCallback();
		} else if (e.getSource().equals(_bHelp)) {
			helpCallback();
		} else {
			// do nothing
			if ( _verbose ) {
				Debug.println( "ContainerPanel.actionPerformed: " + e );
			}
		}
    }

    public void okCallback() {
		BlankPanel p = (BlankPanel)getSelectedPanel();
		if ( p._isInitialized ) {
			p.okCallback();
		}
        
        if ( !isDirty() ) { // all panels successfully updated
            // disable Apply/Reset buttons
    		_bOK.setEnabled(false);
    		_bReset.setEnabled(false);
        } else {
			if ( _verbose ) {
				Debug.println("ContainerPanel.okCallback(): " +
							  "panels are still dirty after ok");
			}
        }
    }

    public void resetCallback() {
		BlankPanel p = (BlankPanel)getSelectedPanel();
		if ( p._isInitialized ) {
			p.resetCallback();
		}
        
        if ( !isDirty() ) { // all panels successfully reset
            // disable Apply/Reset buttons
    		_bOK.setEnabled(false);
    		_bReset.setEnabled(false);
        } else {
			if ( _verbose ) {
				Debug.println("ContainerPanel.resetCallback(): " +
							  "panels are still dirty after reset");
			}
        }
    }

	/**
	 * Report if any managed panels have unsaved data.
	 * @return true if any panels have unsaved data.
	 */
    public boolean isDirty() {
		BlankPanel p = (BlankPanel)getSelectedPanel();
		if (p.isDirty()) {
			return true;
		}
        return false;
    }

    protected void helpCallback() {
        _selectedPanel = (BlankPanel)getSelectedPanel();
        if ( _selectedPanel != null )
            _selectedPanel.helpCallback();
    }

    // package scope; used by BlankPanel
    public void setDirtyFlag() {
		if ( _verbose ) {
			Debug.println( "ContainerPanel.setDirtyFlag: " +
				getClass().getName() );
		}
		// enable Reset button
		if ( _bReset != null ) {
			_bReset.setEnabled(true);
		}
    }

    public void setDirtyFlag(JPanel p) {
        setDirtyFlag();
	}

    public void clearDirtyFlag() {
		// disable Reset button
		if ( ( _bReset != null ) && ( !isDirty() ) ) {
			_bReset.setEnabled(false);
		}
        // since it is not dirty, you can't save it
        if ( _bOK != null ) {
			_bOK.setEnabled(false);
        }
    }

    public void clearDirtyFlag(JPanel p) {
		clearDirtyFlag();
	}

	public void setValidFlag() {
		if ( _verbose ) {
			Debug.println( "ContainerPanel.setValidFlag: " +
				getClass().getName() );
		}
		// enable Save button
		if ( _bOK != null ) {
			_bOK.setEnabled(true);
		}
    }

	public void setValidFlag(JPanel p) {
		setValidFlag ();
	}

	public void clearValidFlag() {
		// disable Save button
		if ( _bOK != null ) {
			_bOK.setEnabled(false);
		}
    }

	public void clearValidFlag(JPanel p) {
		clearValidFlag ();
	}

	public JButton getOKButton() {
		return _bOK;
	}

	public JButton getCancelButton() {
		return _bReset;
	}

	public void updateButtons() {
		if ( _buttonPanel != null ) {
			_buttonPanel.invalidate();
			_buttonPanel.validate();
			_buttonPanel.repaint();
		}
	}

    protected IDSModel getModel() {
		return _dsModel;
	}
    
	protected static final boolean _verbose = false;
	protected BlankPanel _selectedPanel = null;
	private IDSModel _dsModel;
    protected JButton _bOK = null;              // apply/ok button
    protected JButton _bReset = null;           // reset button
    protected JButton _bHelp = null;            // help button
	protected JPanel _buttonPanel = null;
    private JPanel _clientPanel = null;
    private RemoteImage _markImage = null;
}
