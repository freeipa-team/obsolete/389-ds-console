/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	03/08/98
 * @see     com.netscape.admin.dirserv
 */
public class RestorePanel extends BlankPanel {

	public RestorePanel(IDSModel model) {
		super( model, _section );
		_helpToken = "tasks-restore-help";
	}

	public void init() {
		if (!_initialized) {
			/* What are the known backups? */
			ListDB task = new ListDB( getModel().getServerInfo() );
			getModel().setWaitCursor( true );
			String[] dirList = task.getBackupList();
			getModel().setWaitCursor( false );
			
			_myPanel.setLayout( new GridBagLayout() );
			createFileArea( _myPanel, dirList );
			
                        String _baseDir = DSUtil.getDefaultBackupPath(getModel().getServerInfo());
			
			SimpleDialog dlg = getSimpleDialog();
			if ( dlg != null ) {
				dlg.setFocusComponent(_tfExport);
			}
			_initialized = true;
		}
	}	

    protected void createFileArea( JPanel grid, String[] dirList ) {
        GridBagConstraints gbc = getGBC();
		gbc.insets.bottom = _gbc.insets.top;
        gbc.gridwidth = gbc.REMAINDER;


		
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;
		grid.add(panel, gbc);
		/* Now substitute grid by panel*/
		

	    _comboNames = new JComboBox();
        _comboNames.addItemListener(this);
        _comboNames.addActionListener(this);

		if ( dirList != null ) {
			for( int i = 0; i < dirList.length; i++ )
                _comboNames.addItem( dirList[i] );
			if ( dirList.length > 0 )
				_comboNames.setSelectedIndex( 0 );
		}
        String ttip = DSUtil._resource.getString(_section, "backups-ttip");
        if (ttip != null) {
            _comboNames.setToolTipText(ttip);
        }
		JLabel comboLabel = makeJLabel( _section, "backups" );
		gbc.gridwidth = 2;
		gbc.fill = gbc.NONE;
  		gbc.weightx = 0.0;
        panel.add( comboLabel, gbc );
		comboLabel.setLabelFor(_comboNames);
        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx    = 1.0;
		gbc.fill = gbc.HORIZONTAL;
        panel.add( _comboNames, gbc );		

		JLabel filenameLabel = makeJLabel( _section, "filename" );
		gbc.gridwidth = 3;
		gbc.fill = gbc.NONE;
  		gbc.weightx = 0.0;
        panel.add( filenameLabel, gbc );

        _tfExport = makeJTextField( _section, "filename" );
		filenameLabel.setLabelFor(_tfExport);
		if ( dirList.length > 0 ) {
			_tfExport.setText( (String)_comboNames.getSelectedItem() );
			setOkay( true );
		} else {
			setOkay( false );
		}
		gbc.fill = gbc.HORIZONTAL;
  		gbc.weightx = 1.0;
        panel.add(_tfExport,gbc);

		_bExport = makeJButton( _section, "browse-file" );
		gbc.fill = gbc.NONE;
		gbc.weightx = 0.0;
		
        gbc.gridwidth = gbc.REMAINDER;
        panel.add(_bExport,gbc);

        _bExport.setEnabled( isLocal() );

		

		gbc.weighty= 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.VERTICAL;
		grid.add(Box.createGlue(), gbc);
	}

	/**
	 * Enable/disable OK button
	 *
	 * @param ok true to enable the OK button
	 */
    private void setOkay( boolean ok ) {
		SimpleDialog dlg = getSimpleDialog();
		if ( dlg != null ) {
			dlg.setOKButtonEnabled( ok );
		} else {
		}
	}

    private void checkOkay() {
		String path = getFilename();
		boolean ok = ( (path != null) &&
					   (path.length() > 0) );
		setOkay( ok );
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if ( e.getSource().equals(_bExport) ) {
    	    String file = getFilename().trim();
			if ((file != null) && (file.length() > 1)) {
				/* See if it is an absolute path */
				File theFile = new File(file);
				if (!theFile.isAbsolute()) {			
					/* If no path was provided, use the default path */
					file = _baseDir + java.io.File.separator + file;	
				}
			} else {
				file = _baseDir;
			}
    	    file = DSFileDialog.getDirectoryName(file, true, this);
    	    if (file != null)
    	        _tfExport.setText(file.trim());
        } else if ( e.getSource().equals( _comboNames ) ) {
			if ( _tfExport != null ) { // Startup condition
				_tfExport.setText( (String)_comboNames.getSelectedItem() );
				checkOkay();
			}
        } else {
			super.actionPerformed(e);
		}
    }

    public void resetCallback() {		
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

    public void okCallback() {
		/* No state to preserve */
		int wantToContinue = DSUtil.showConfirmationDialog( getModel().getFrame(),"offlinePartition", (String[])null, "LDAPTask");
		if (wantToContinue != JOptionPane.YES_OPTION) {
			return;
		}
				
		/* If no path was provided, use the default path.  We do it only in the case we are local (if we are
		 remote the user is obliged to provide the full path: we can't determine wether it is an absolute or relative
		 path because we don't know the OS of the server) */
		if (isLocal()) {
			String name = getFilename();
			if (name != null) {
				File file = new File(name);
				if (!file.isAbsolute()) {
					name = _baseDir + java.io.File.separator + name;
				}
				_tfExport.setText(name);
			}		
		}		

		clearDirtyFlag();
		hideDialog();
    }

    public String getFilename() {
		return _tfExport.getText().trim();
	}
	
	public void setFileName(String fileName) {
		_tfExport.setText(fileName);
	}

   protected SimpleDialog getSimpleDialog() {		
	   for (Container p = getParent(); p != null; p = p.getParent()) {
		   if (p instanceof SimpleDialog) {
			   return (SimpleDialog)p;
		   }
	   }
	   return null;
   }
	

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void changedUpdate(DocumentEvent e) {
		if (_tfExport.getText().trim().equals("")) {
			setOkay(false);
		} else {
			if ( isLocal() ){
				File f = new File(_tfExport.getText());
				if (!f.exists()) { // Restore path must exist
					setOkay(false);
				} else if (!f.isDirectory()) { // Restore path must be a directory
					setOkay(false);
				} else {
					setOkay(true);
				}
			} else {
				// This is a remote session, let the server validate the path
				setOkay(true);
			}
		}
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void removeUpdate(DocumentEvent e) {
		changedUpdate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void insertUpdate(DocumentEvent e) {
		changedUpdate(e);
    }

	private JComboBox _comboNames;
	private JTextField _tfExport;
	private JButton _bExport;
	private String _baseDir = "";
	private boolean _initialized = false;

	private final static String _section = "restore";
}
