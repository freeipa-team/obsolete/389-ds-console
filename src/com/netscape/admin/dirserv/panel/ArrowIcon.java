/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 
-*- 
 * 
 * The contents of this file are subject to the Netscape Public License 
 * Version 1.0 (the "License"); you may not use this file except in 
 * compliance with the License.  You may obtain a copy of the License at 
 * http://www.mozilla.org/NPL/ 
 * 
 * Software distributed under the License is distributed on an "AS IS" 
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.  See 
 * the License for the specific language governing rights and limitations 
 * under the License. 
 * 
 * The Original Code is Netscape Console.
 * 
 * The Initial Developer of the Original Code is Netscape Communications 
 * Corporation.  Portions created by Netscape are Copyright (C) 1998 
 * Netscape Communications Corporation.  All Rights Reserved. 
 */ 
package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.util.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;

public class ArrowIcon implements Icon
{
	int _direction = SwingConstants.NORTH;
	int _size = 5;

	/**
	 * constructor of the icon.
	 *
	 * @param direction	Direction of the icon. It is default to SwingConstants.NORTH.
	 */
	public ArrowIcon(int direction)
	{
		_direction = direction;
	}

	/**
	 * return the icon width
	 *
	 * @return icon width
	 */
	public int getIconWidth()
	{
		return _size;
	}

	/**
	 * return the icon height
	 *
	 * @param return the icon height
	 */
	public int getIconHeight()
	{
		return _size;
	}

	/**
	 * paint the icon
	 *
	 * @param c component itself
	 * @param g graphics to be paint on
	 * @param x x location
	 * @param y y location
	 */
	public void paintIcon(Component c, Graphics g, int x, int y) 
	{
		boolean isEnabled = ((JButton)c).isEnabled();
		int mid, i, j;
		Color oldColor = g.getColor();

		y--;
		j = 0;
		_size = Math.max(_size, 2);
		mid = _size / 2;


		g.translate(x, y);
		if (isEnabled)
			g.setColor(UIManager.getColor("controlDkShadow"));
		else
			g.setColor(UIManager.getColor("controlShadow"));

		switch (_direction)
		{
		case SwingConstants.NORTH:
			for (i = 0; i < _size; i++)
			{
				g.drawLine(mid-i, i, mid+i, i);
			}
			if (!isEnabled)
			{
				g.setColor(UIManager.getColor("controlHighlight"));
				g.drawLine(mid-i+2, i, mid+i, i);
			}
			break;
		case SwingConstants.SOUTH:
			if (!isEnabled)
			{
				g.translate(1, 1);
				g.setColor(UIManager.getColor("controlHighlight"));
				for (i = _size-1; i >= 0; i--)
				{
					g.drawLine(mid-i, j, mid+i, j);
					j++;
				}
				g.translate(-1, -1);
				g.setColor(UIManager.getColor("controlShadow"));
			}

			j = 0;
			for (i = _size-1; i >= 0; i--)
			{
				g.drawLine(mid-i, j, mid+i, j);
				j++;
			}
			break;
		case SwingConstants.WEST:
			for (i = 0; i < _size; i++)
			{
				g.drawLine(i, mid-i, i, mid+i);
			}
			if (!isEnabled)
			{
				g.setColor(UIManager.getColor("controlHighlight"));
				g.drawLine(i, mid-i+2, i, mid+i);
			}
			break;
		case SwingConstants.EAST:
			if (!isEnabled)
			{
				g.translate(1, 1);
				g.setColor(UIManager.getColor("controlHighlight"));
				for (i = _size-1; i >= 0; i--)
				{
					g.drawLine(j, mid-i, j, mid+i);
					j++;
				}
				g.translate(-1, -1);
				g.setColor(UIManager.getColor("controlShadow"));
			}

			j = 0;
			for (i = _size-1; i >= 0; i--)
			{
				g.drawLine(j, mid-i, j, mid+i);
				j++;
			}
			break;
		}
		g.translate(-x, -y);  
		g.setColor(oldColor);
	}
}
