/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.Vector;
import java.util.Enumeration;
import java.awt.Color;
import javax.swing.*;
import javax.swing.JTextField;
import com.netscape.management.client.util.Debug;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server
 *
 * @version
 * @author
 */
public class DSEntryLong extends DSEntry {
    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle text entry fields which accept long values.  One
     * model value, one widget.  The user also supplies a minimum
     * and maximum value for the field for validate().
     *
     * @param model the initial value, if any, or just null
     * @param view the JTextField used as the text entry field
     * @param minValue the minimum value of the field for validation
     * @param maxValue the maximum value of the field for validation
     * @param scaleFactor Often, the value stored in the database is in the
     *  smallest units available, but it is desired to display to the user
     *  in some larger unit e.g. password expiration is stored in seconds, but
     *  we want to display it in days.  The scale factor in this case would be
     *  60*60*24 = 86400 seconds per day.  Use a value of 1 for no scaling.
	 * @param blankOkay true if it the value may be the empty string
     */
    public DSEntryLong(
        String model,
        JTextField view,
        long minValue,
        long maxValue,
        long scaleFactor,
		boolean blankOkay
    ) {
        super(model, view);
        _minValue = minValue;
        _maxValue = maxValue;
        _scaleFactor = scaleFactor;
		_blankOkay = blankOkay;
    }
    
    public DSEntryLong(
        String model,
        JTextField view,
        long minValue,
        long maxValue,
        long scaleFactor
    ) {
	this( model, view, minValue, maxValue, scaleFactor, false );
    }
    
    public DSEntryLong(
        String model,
        JComponent view1, JComponent view2,
        long minValue,
        long maxValue,
        long scaleFactor
    ) {
	this(model, view1, view2, minValue, maxValue, scaleFactor, false );
    }

    public DSEntryLong(
        String model,
        JComponent view1, JComponent view2,
        long minValue,
        long maxValue,
        long scaleFactor,
        boolean blankOkay
    ) {
        super(model, view1, view2);
        _minValue = minValue;
        _maxValue = maxValue;
        _scaleFactor = scaleFactor;
        _blankOkay = blankOkay;
    }

    /**
     * First, get the model as an long.  If the model is not an long,
     * just set the value to 0.  Next, convert the value using the given
     * scale factor.  Last, update the view.
     */
    public void show() {
        // get the model as an long
		String s = getModel(0);
		if ( !_blankOkay || (s.length() > 0) ) {
			long val = 0;
			try {
				val = Long.parseLong(s);
			} catch (NumberFormatException nfe) {
				val = 0;
			}
			// convert using the scaleFactor
			if (_scaleFactor != 0)
				val /= _scaleFactor;
			s = Long.toString(val);
		}
        // update the view
		JTextField tf = (JTextField)getView(0);
		tf.setText(s);

        viewInitialized ();
	}

	/**
	 * Since data is dynamically updated when it is modified
     * thought a call to updateModified, nothing needs ro be
     * done here
     */
	public void store() {
	    
    }
	
	/**
	 * First, call validate to see if the value is valid.  If not valid, don't
	 * do anything.  validate() will usually be called before store() by
	 * DSEntrySet.  If the field is valid, we can assume it is an long.
	 * Get the value as an long and apply the scale factor, then update
	 * the model.
	 */
	protected void updateModel() {
		JTextField tf = (JTextField)getView(0);
		String     s = tf.getText();
		long        val;
	
		if (s.length () > 0){
			try{
				val = Long.parseLong(s);
			}
			catch (NumberFormatException e) {
				return;
			}
		
			val *= _scaleFactor;
			s = Long.toString(val);
		}
    	setModelAt(s, 0);
    }

    public int validate() {
        JTextField tf = (JTextField)getView(0);
        String val = tf.getText();

		/* disable fields are always ok. */
		if (!tf.isEnabled ())
			return 0;

        if ( _blankOkay && (val.length() < 1) ) { 
            return 0; 
        } 

        long ival = 0;
        if ((val == null) || (val.length() == 0)) {
             return 101;
        }
        try {
            ival = Long.parseLong(val);
        } catch (NumberFormatException e) {
			return 102;
        }

        if (ival < _minValue) {
            // value is less than recommended minimum
            return 103;
        }
        if (ival > _maxValue) {
            // value is greater than recommended maximum
            return 104;
        }
        return 0;
    }
    
    /**
     * several types of validation are done; first, the text field is checked
     * to see if the user has typed in anything.  Next, the value is checked
     * to see if it is an long.  Then, the long value is compared
     * against the given min and max values.  If any of these conditions fail,
     * false is returned, otherwise, true is returned.
     *
     * @return false if the text field does not contain a valid long
     */
	public boolean dsValidate() {
		JTextField tf = (JTextField)getView(0);
		String val = tf.getText();
        String err = null; // error number
        String args[] = null; // additional arguments for error messages

        switch (validate()) {

            case 101:
                err = "101";
                break;
            case 102:
                err = "102";
                args = new String[1];
                args[0] = val;
                break;
            case 103:
                err = "103";
                args = new String[2];
                args[0] = val;
                args[1] = Long.toString(_minValue);
                break;
            case 104:
                err = "104";
                args = new String[2];
                args[0] = val;
                args[1] = Long.toString(_maxValue);
                break;
                default: return true;
        }
        reportError(err, args, tf);
        return false;
	}

   /**
    *  getValue
    * @return the value of the data typed in the text box
    */

    public long getValue (){
        JTextField tf  = (JTextField)getView(0);    
        String     s   = tf.getText ().trim ();
        long        val;

        if (s.length() > 0) {
			try {
				val = Long.parseLong(s);
			} catch (NumberFormatException nfe) {
				val = 0;
			}                
		}
		else 
			val = 0;

        return val;
    }

   /**
    * getModelValue
    * @return the value of the data stored in the entry
    */

    public long getModelValue () {
        String     s = getModel(0);
        long        val;

        if (s.length() > 0) {
			try {
				val = Long.parseLong(s);
			} catch (NumberFormatException nfe) {
				val = 0;
			}                
		}
		else 
			val = 0;

        return val;
    }

	/**
	 * Set the min value allowed.
	 *
	 * @param minValue Minimum value allowed for this field
	 */
	public void setMinValue( long minValue ) {
		_minValue = minValue;
	}

	/**
	 * Report the min value allowed.
	 *
	 * @returns Minimum value allowed for this field
	 */
	public long getMinValue() {
		return _minValue;
	}

	/**
	 * Set the max value allowed.
	 *
	 * @param maxValue Maximum value allowed for this field
	 */
	public void setMaxValue( long maxValue ) {
		_maxValue = maxValue;
	}

	/**
	 * Report the max value allowed.
	 *
	 * @returns Maximum value allowed for this field
	 */
	public long getMaxValue() {
		return _maxValue;
	}

	/**
	 * Set the scale factor.
	 *
	 * @param scaleFactor scale factor for this field
	 */
	public void setScaleFactor( long scaleFactor ) {
		_scaleFactor = scaleFactor;
	}

	/**
	 * Report the scale factor.
	 *
	 * @returns Scale factor for this field
	 */
	public long getScaleFactor() {
		return _scaleFactor;
	}

    protected long _minValue;
    protected long _maxValue;
    protected long _scaleFactor = 1;
    protected boolean _blankOkay;
}
