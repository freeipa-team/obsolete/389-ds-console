/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import netscape.ldap.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class MonitorServerPanel extends MonitorBasePanel {
    /**
     * Standard panel constructor.
     *
     * @param model The resource model for this panel.
     */
    public MonitorServerPanel(IDSModel model) {
        super(model, _section);
	_helpToken = "status-perfcounters-server-help";
    }

    /**
     * Initialize, called once when the panel is first selected.
     */
    public void init() {
        _myPanel.setLayout(new GridBagLayout());
	resetGBC();

	_entry = reload();
	_configEntry = reload( CONFIG_ENTRY, CONFIG_ATTRS );

        createVersionArea( _myPanel );

        JPanel panel = createRefreshArea();
	GridBagConstraints gbc = getGBC();
	gbc.gridx = 0;
	gbc.insets = new Insets(0,9,0,9);
        gbc.gridwidth = gbc.REMAINDER;
	_myPanel.add( panel, gbc );

        createSummaryResourceArea( _myPanel );
        createCurrentResourceArea( _myPanel );
        createConnectionResourceArea( _myPanel );

	_ldbmEntry = reload( LDBM_MONITOR_ENTRY );
        createCacheArea( _myPanel );

	validate();
    }

    /**
     * Refetch data from the Directory.
     */
    private LDAPEntry reload() {
	return( reload( _entryName ));
    }

    private LDAPEntry reload(String entryName) {
    	return( reload( entryName, null ) );
    }
    
    private LDAPEntry reload(String entryName, String[] attrs) {
	LDAPEntry entry = null;
	if ( _ldc == null )
	    _ldc = getModel().getServerInfo().getLDAPConnection();
        try {
	    if (attrs == null)
            	entry = _ldc.read( entryName );
	    else
	    	entry = _ldc.read( entryName, attrs );
        } catch (LDAPException e) {
            Debug.println("MonitorServerPanel.reload: " + _entryName +
                          ", " + e);
        }
	Debug.println(7, "MonitorServerPanel.reload: dn=" + entryName + " nattrs=" +
				  ((attrs != null) ? attrs.length : 0) + " entry=" + _entry );
	return( entry );
    }

    /**
     * Refetch data from the directory and update the panel.
     */
    public boolean refresh() {
	/* Get new data from the server */
	_entry = reload();
	_configEntry = reload( CONFIG_ENTRY, CONFIG_ATTRS );

	updateSummaryAndUsage();

	/* Parse the connection information */
	updateConnectionInfo();

	updateCacheInfo();
	/* Now pass the changes on to the UI */
	return super.refresh();
    }

    private void updateConnectionInfo() {
	_currentTimeLabel.setText(
			_resource.getString( _section, "currentTime-label" ) +
			" " + DSUtil.formatDateTime( getAttrValue( "currenttime" )));
	/* Parse the connection info */
	_connections.removeAllElements();
	String val = null;
	LDAPAttribute attr = (_entry != null) ?
	    _entry.getAttribute( "connection" ) : null;
	Debug.println(7, "MonitorServerPanel.updateConnectionInfo: _entry=" +
				  ((_entry != null) ? _entry.getDN() : "null") + " attr=" +
				  attr);
	if ( attr != null ) {
	    Enumeration e = attr.getStringValues();
	    while( e.hasMoreElements() ) {
		val = (String)e.nextElement();
		StringTokenizer st = new StringTokenizer( val, ":" );
		ConnectionInfo info = new ConnectionInfo();
		try {
		    /* First is the index */
		    info.setIndex(
				  Integer.parseInt( st.nextToken() ) );
		    /* Then the time started */
		    info.setTime( st.nextToken() );
		    /* Then the operations started */
		    info.setStartedCount(
					 Integer.parseInt( st.nextToken() ) );
		    /* Then the operations completed */
		    info.setCompletedCount(
					   Integer.parseInt( st.nextToken() ) );
		    /* Then the read/write state */
		    info.setReadWriteState( st.nextToken() );
		    /* Then the bind DN */
		    info.setBindDN( st.nextToken() );
		} catch ( NoSuchElementException ex ) {
		    /* This happens if there is no read/write state */
		    info.setBindDN( info.getReadWriteState() );
		    info.setReadWriteState( null );
		}
		_connections.addElement( info );
		Debug.println(7, "MonitorServerPanel.updateConnectionInfo: connection info=" + info.toString());
	    }
	}
	/* Update the connections table viewable port size */
	Debug.println(7, "MonitorServerPanel.updateConnectionInfo: _connectionTable=" + _connectionTable);
	if (_connectionTable != null) {
		int height = (_connectionTable.getRowMargin() + _connectionTable.getRowHeight()) * _connectionTable.getRowCount();
		int width = _connectionTable.getPreferredScrollableViewportSize().width;
		_connectionTable.setPreferredScrollableViewportSize(new Dimension(width, height));

		_connectionTable.revalidate();
		_connectionTable.repaint();
	}
    }

    /**
     * Create the static version panel.
     *
     * @param myContainer The parent component.
     */
    private void createVersionArea( JComponent myContainer ) {
	GridBagConstraints gbc = getGBC();

	JPanel panel =
	    new GroupPanel( _resource.getString(_section,
						"versionGroup-title") );
	gbc.gridx = 0;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
	myContainer.add( panel, gbc );

	JLabel label;
	String s;
	s = _resource.getString( _section, "serverVersion-label" );
	s += " " + getAttrValue( "version" );
	label = new JLabel( s );
	label.setLabelFor(panel);
	panel.add( label, gbc );

	s = _resource.getString( _section, "startTime-label" );
	s += " " + DSUtil.formatDateTime( getAttrValue( "starttime" ) );
	label = new JLabel( s );
	label.setLabelFor(panel);
	panel.add( label, gbc );

	s = _resource.getString( _section, "currentTime-label" );
	s += " " + DSUtil.formatDateTime( getAttrValue( "currenttime" ) );
	_currentTimeLabel = new JLabel( s );
	_currentTimeLabel.setLabelFor(panel);
	panel.add( _currentTimeLabel, gbc );
    }

    /**
     * Create the summary resource panel.
     *
     * @param myContainer The parent component.
     */
    private void createSummaryResourceArea( JComponent myContainer ) {
	String[] dataNames = { "totalconnections",
			       "opsinitiated",
			       "opscompleted",
			       "entriessent",
			       "bytessent" };
	int NROWS = dataNames.length;
	String[] labels = new String[NROWS];
	for( int i = 0; i < NROWS; i++ )
	    labels[i] = _resource.getString( _section,
					     "summaryTable" +
					     i + "-label");
	int NCOLS = 3;
	String[] headers = new String[NCOLS];
	for( int i = 0; i < NCOLS; i++ )
	    headers[i] = _resource.getString( _section,
					      "summaryHeader" +
					      i + "-label" );

	PerfTableModel model = new PerfTableModel( labels,
						   headers,
						   dataNames ) {
		public Object getValueAt(int row, int col) {
		    String val = (String)super.getValueAt( row, col );
		    if ( (val == null) || (val.length() < 1) ) {
			return "";
		    }
		    /* The third column is the average of the second column */
		    if ( col == 2 ) {
			Date start =
			    DSUtil.getDateTime( getAttrValue( "starttime" ) );
			Date now =
			    DSUtil.getDateTime( getAttrValue( "currenttime" ) );
			/* Get delta in minutes */
			long delta =
			    (now.getTime() - start.getTime()) / (60 * 1000);
			if ( delta < 1 )
			    delta = 1;
			float value =
			    Float.valueOf( val ).floatValue() / (float)delta;
			/* Truncate to one decimal */
			value =
			    (float)((float)((long)((value + 0.05) * 10)) / 10.0);
			val = Float.toString( value );
		    }
		    return val;
		}
	    };
        JTable table = new JTable( model );
	_summaryModel = model;
	/* Numerical fields should be right-aligned */
	rightAlignColumns( table, 1, NCOLS-1 );

	_tables.addElement( table );
	int[] widths = { 150, 100, 100 };
	setColumnWidths( table, widths );

	addTable( table, myContainer,
		  _resource.getString(_section, "summaryGroup-title") );
		table.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		table.getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "summaryGroup-title") );
    }

    /**
     * Create the current resource usage panel.
     *
     * @param myContainer The parent component.
     */
    private void createCurrentResourceArea( JComponent myContainer ) {
	_notApplicable = _resource.getString( _section,
					      "notApplicable-label" );

	String[] dataNames = { "threads",
			       "currentconnections",
			       "dtablesize",			       
			       "readwaiters",			       
			       "nbackends" };
	int NROWS = dataNames.length;
	String[] labels = new String[NROWS];
	for( int i = 0; i < NROWS; i++ )
	    labels[i] = _resource.getString( _section,
					     "resourceTable" +
					     i + "-label");

	int NCOLS = 2;
	String[] headers = new String[NCOLS];
	for( int i = 0; i < NCOLS; i++ )
	    headers[i] = _resource.getString( _section,
					      "resourceHeader" +
					      i + "-label" );

	PerfTableModel model = new PerfTableModel( labels,
						   headers,
						   dataNames ) {
		public Object getValueAt(int row, int col) {
		    String val = (String)super.getValueAt( row, col );
		    if ( (val == null) || (val.length() < 1) ) {
			return "";
		    }
		    if ( col == 1 ) {
			if ( row == 2 ) {
			    val = Integer.toString( computeRemainingConnections() );
			} else if ( row == 5 ) {
			    if ( (val == null) || (val.length() < 1) )
				val = _notApplicable;
			} else if ( (val == null) || (val.length() < 1) ) {
			    val = Integer.toString( 0 );
			}
		    }
		    return val;
		}
	    };	
        JTable table = new JTable( model );
	_currentModel = model;

	/* Numerical fields should be right-aligned */
	rightAlignColumns( table, 1, NCOLS-1 );

	_tables.addElement( table );
	int[] widths = { 250, 100 };
	setColumnWidths( table, widths );
	setColumnHeaders( table, headers );

	addTable( table, myContainer,
		  _resource.getString(_section, "resourceGroup-title") );
	table.getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "resourceGroup-title") );
    }


    /**
     * The server does not provide the number of remaining
     * connections (sic). It must be computed using the following
     * attributes:
     *		dtablesize			(cn=monitor,cn=config)
     *		currentconnections		(cn=monitor,cn=config)
     *		nsslapd-maxdescriptors		(cn=config)
     *		nsslapd-reserveddescriptors	(cn=config)
     */
    int computeRemainingConnections() {
    	String dTableSizeStr = getAttrValue("dtablesize");
	String currentConnStr = getAttrValue("currentconnections");
	String maxDescStr = DSUtil.getAttrValue(_configEntry, "nsslapd-maxdescriptors");
	String reservedDescStr = DSUtil.getAttrValue(_configEntry, "nsslapd-reservedescriptors");

	// nsslapd-maxdescriptors is not always available.
	// When it's not, we use dtablesize.
	int dTableSize = Integer.parseInt(dTableSizeStr);
	int currentConn = Integer.parseInt(currentConnStr);
	int maxDesc = (maxDescStr.length() == 0) ? dTableSize : Integer.parseInt(maxDescStr);
	int reservedDesc = (reservedDescStr.length() == 0) ? 0 : Integer.parseInt(reservedDescStr);
	int result = maxDesc - reservedDesc - currentConn;
	
	Debug.println("MonitorServerPanel.computeRemainingConnections: dTableSizeStr = " + dTableSizeStr);
	Debug.println("MonitorServerPanel.computeRemainingConnections: currentConnStr = " + currentConnStr);
	Debug.println("MonitorServerPanel.computeRemainingConnections: maxDescStr = " + maxDescStr);
	Debug.println("MonitorServerPanel.computeRemainingConnections: reservedDescStr = " + reservedDescStr);
	
	// Normally it should be positive but...
	if (result < 0) result = 0;
	
	return result;
    }
    
    
    /**
     * Create the Current Connection info panel.
     *
     * @param myContainer The parent component.
     */
    private void createConnectionResourceArea( JComponent myContainer ) {
		updateConnectionInfo();
	_notBound = _resource.getString( _section,
                                         "notBound-label" );
	_notBlocked = _resource.getString( _section,
                                           "notBlocked-label" );

	String[] dataNames = { "connection" };
	int NROWS = dataNames.length;
	String[] labels = new String[NROWS];
	labels[0] = "";

	int NCOLS = 5;
	String[] headers = new String[NCOLS];
	for( int i = 0; i < NCOLS; i++ )
	    headers[i] = _resource.getString( _section,
					      "connectionHeader" +
					      i + "-label" );

	PerfTableModel model = new PerfTableModel( labels,
						   headers,
						   dataNames ) {
		public int getRowCount() {			
			LDAPAttribute attr = (_entry != null) ?
			_entry.getAttribute( _dataNames[0] ) : null;			
		    return (attr != null) ? attr.size() : 0;
		}
		public Object getValueAt(int row, int col) {
		    if ( row >= _connections.size() )
			return "";
		    String val = null;
		    ConnectionInfo info =
			(ConnectionInfo)_connections.elementAt( row );
		    if ( col == 0 ) {
			Date d = info.getDate();
			val = d.toString();
		    }
		    else if ( col == 1 )
			val = Integer.toString( info.getStartedCount() );
		    else if ( col == 2 )
			val = Integer.toString( info.getCompletedCount() );
		    else if ( col == 3 ) {
			val = info.getBindDN();
			if ( val.equals( "NULLDN" ) )
			    val = _notBound;
		    }
		    else if ( col == 4 ) {
			val = info.getReadWriteState();
			if ( val == null )
			    val = _notBlocked;
		    }
		    if ( val == null )
			return "";
		    //				Debug.println( " table value " + row + "," +
		    //									col + ": " + val );
		    return val;
		}
	    };
        _connectionTable = new JTable( model );
	/* Numerical fields should be right-aligned */
	rightAlignColumns( _connectionTable, 1, 2 );

	_tables.addElement( _connectionTable );
	int[] widths = { 100, 45, 60, 100, 75 };
	setColumnWidths( _connectionTable, widths );
	setColumnHeaders( _connectionTable, headers );

	addTable( _connectionTable, myContainer,
			  _resource.getString(_section, "connectionGroup-title") );
	_connectionTable.getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "connectionGroup-title") );
	_connectionTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
    }

    private void createCacheArea( JComponent myContainer ) {
	/* Commented out fields are ones not considered interesting
	   other than for us */
	String[] dataNames = { "dbcachehits",
			       "dbcachetries",
			       "dbcachehitratio",
			       // "dbcachemap",
			       // "dbcachepagecreate",
			       "dbcachepagein",
			       "dbcachepageout",
			       "dbcacheroevict",
			       "dbcacherwevict"
			       // "dbcachehachbuckets",
			       // "dbcachehashsearches",
			       // "dbcachehashlongest",
			       // "dbcachehashexamined"
	};
	int NROWS = dataNames.length;
	String[] labels = new String[NROWS];
	for( int i = 0; i < NROWS; i++ )
	    labels[i] = _resource.getString( _section,
					     "cacheTable-" +
					     dataNames[i] + "-label");
	
	int NCOLS = 2;
	String[] headers = new String[NCOLS];
	for( int i = 0; i < NCOLS; i++ )
	    headers[i] = _resource.getString( _section,
					      "cacheHeader" +
					      i + "-label" );
	int[] widths = { 250, 100 };
	
        _cacheTable = makeTable( labels,
				  headers,
				  dataNames,
				  widths,
				  _ldbmEntry );
	/* Numerical fields should be right-aligned */
	rightAlignColumns( _cacheTable, 1, NCOLS-1 );

	addTable( _cacheTable, myContainer,
		  _resource.getString(_section, "cacheGroup-title") );
	_cacheTable.getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "cacheGroup-title") );
    }
    
    private JTable makeTable( String[] labels, String[] headers,
			      String[] dataNames, int[] widths ) {
	
	return ( makeTable(labels, headers, dataNames, widths, _entry ));
    }

     private JTable makeTable( String[] labels, String[] headers,
			      String[] dataNames, int[] widths,
			       LDAPEntry entry2Monitor) {
	PerfTableModel model = new PerfTableModel( labels,
						   headers,
						   dataNames,
						   entry2Monitor );
        JTable table = new JTable( model );
	_tables.addElement( table );
	setColumnWidths( table, widths );
	setColumnHeaders( table, headers );
	return table;
    }

     private void updateCacheInfo() {
	 PerfTableModel model = (PerfTableModel) _cacheTable.getModel();
	 _ldbmEntry = reload( LDBM_MONITOR_ENTRY );       	 
	 model.updEntry( _ldbmEntry );
    }

     private void updateSummaryAndUsage() {
	 PerfTableModel model = null;
	 if(  _summaryModel != null ) {
	     _summaryModel.updEntry( _entry );
	 }
	 if( _currentModel != null ){
	     _currentModel.updEntry( _entry );
	 }
    }

    /**
     * Information on the state and history of a particular connection.
     */
    private class ConnectionInfo {
	ConnectionInfo() {}
	void setIndex( int index ) { _index = index; }
	void setTime( String time ) { _time = time;
	_date = DSUtil.getDateTime( time ); }
	void setStartedCount( int count ) { _started = count; }
	void setCompletedCount( int count ) { _completed = count; }
	void setReadWriteState( String state ) { _state = state; }
	void setBindDN( String dn ) { _dn = dn; }
	int getIndex() { return _index; }
	String getTime() { return _time; }
	Date getDate() { return _date; }
	int getStartedCount() { return _started; }
	int getCompletedCount() { return _completed; }
	String getReadWriteState() { return _state; }
	String getBindDN() { return _dn; }
	public String toString() {
	    String s = "ConnectionInfo: ";
	    s += _index + ", start time = " + _time + ", ops = " + _started +
		", completed = " + _completed + ", state = " + _state +
		", bindDN = " + _dn;
	    return s;
	}

        private int _index = 0;
	String _time = null;
	int _started = 0;
	int _completed = 0;
	String _state = null;
	String _dn = null;
	Date _date = null;
    }

    private Vector _connections = new Vector();
    private String _notBound;
    private String _notApplicable;
    private String _notBlocked;
    private JLabel _currentTimeLabel;
    private String CONFIG_ENTRY = "cn=config";
    private String[] CONFIG_ATTRS = {"nsslapd-maxdescriptors", "nsslapd-reservedescriptors"};
    private String LDBM_MONITOR_ENTRY = "cn=monitor, cn=ldbm database, cn=plugins, cn=config";
    private LDAPEntry _configEntry = null;
    private LDAPEntry _ldbmEntry = null;
    private  JTable _cacheTable = null;
	private JTable _connectionTable = null;
    private PerfTableModel _currentModel = null;
    private PerfTableModel _summaryModel = null;

    // name of panel in resource file
    final static private String _section = "monitorserver";
}
