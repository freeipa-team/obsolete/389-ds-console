/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;
/*
import java.io.File;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.Enumeration;
import java.util.Hashtable;
*/
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.admin.dirserv.panel.MappingNodeSettingPanel;
import com.netscape.admin.dirserv.panel.MappingNodeBckPanel;
import com.netscape.admin.dirserv.panel.MappingNodeRefPanel;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @see     com.netscape.admin.dirserv
 */

public class MappingNodePanel extends DSTabbedPanel {
    /**
     * Standard pane constructor.
     *
     * @param model The resource model for this panel.
     */    
    public MappingNodePanel(IDSModel model, LDAPEntry entry) {
		super( model );	
		_tabbedPanel = _tabbedPane;
		_model = model;
		_nodeTab = new MappingNodeSettingPanel( model, entry );
		addTab( _nodeTab );
		_bckTab = new MappingNodeBckPanel( model, entry );
		addTab( _bckTab );
		_refTab = new MappingNodeRefPanel( model, entry );
		addTab( _refTab );
		_entry = entry;
		/*
		_tabbedPane.setSelectedIndex(0);
		_nodeTab.invalidate();
		_nodeTab.validate();
		*/
    }

    public void reload(){
	LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	try{
	    _entry = ldc.read( _entry.getDN());
	    _bckTab.reloadDBList( _entry );
	    _nodeTab.reloadStatus( _entry );

	} catch( LDAPException e){
	    String[] args_m = { _entry.getDN(), e.toString() };	    
	    DSUtil.showErrorDialog( _model.getFrame(),
				    "error-fail-read-entry",
				    args_m,
				    _section );
	}


    }

    public void okCallback(){
	boolean isBckOK  = true;
	boolean isSetOK = true;
	boolean isRefOK = true;

	if(_bckTab.isDirty()) {
	    isBckOK = isBckOK && _bckTab.doAdd();
	}
	if( _refTab.isDirty()){
	    isRefOK = isRefOK && _refTab.doAdd();
	}

	if( _nodeTab.isDirty()){
	    isSetOK = isSetOK && _nodeTab.write2server(_bckTab, _refTab); 
	}
	if(_bckTab.isDirty()) {
	    isBckOK = isBckOK && _bckTab.doDel();
	}
	if( _refTab.isDirty()){
	    isRefOK = isRefOK && _refTab.doDel();
	}
	if( isRefOK ){
	    _refTab.clearDirtyFlag();
	}
	if( isBckOK ){
	     _bckTab.clearDirtyFlag();
	}
	if( isSetOK ){
	    _nodeTab.clearDirtyFlag();
	}
	
    }
    private final static String _section = "mappingtree-node";
    private MappingNodeSettingPanel _nodeTab;
    private MappingNodeBckPanel _bckTab;
    private MappingNodeRefPanel _refTab;
    private LDAPEntry _entry;
    JTabbedPane _tabbedPanel;
    IDSModel _model;
}
