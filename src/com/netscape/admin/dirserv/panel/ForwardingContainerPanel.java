/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IChangeClient;
import com.netscape.admin.dirserv.IDSResourceSelectionListener;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.Debug;

/**
 * Container Panel for Directory Server resource page. Provides optional
 * Save/Reset/Help buttons. Used to house a client panel, or extended
 * by DSTabbedPanel to house several client panels.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ForwardingContainerPanel extends ContainerPanel {

	public ForwardingContainerPanel(IDSModel model, BlankPanel panel,
						  boolean doButtons) {
		super( model, panel, doButtons );
        panel.setParent(this);
	}

    /*
     * Support for IDSResourceListenerModel
     *
     */

    /**
      * Called when the TabbedPane is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
		if ( _verbose ) {
			Debug.println("ForwardingContainerPanel.select: " + getClass().getName() );
		}
        _selectedPanel.select(parent, viewInstance);
    }
    
    /**
      * Called when the object is unselected.
      */
    public void unselect(IResourceObject parent, IPage viewInstance) {
		if ( _verbose ) {
			Debug.println("ForwardingContainerPanel.unselect: " + getClass().getName() );
		}
        _selectedPanel.unselect(parent, viewInstance);
    }

}
