/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.Frame;
import java.awt.Cursor;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.panel.BlankPanel;


/**
 * SimpleDialog
 *
 * A convenient Dialog shell for a BlankPanel descendant, with OK, Cancel,
 * and Help buttons that are forwarded to callbacks of the BlankPanel.
 * Note: this means the BlankPanel is responsible for dismissing the dialog.
 *
 * @version 1.0
 * @author rweltman
 **/
public class SimpleDialog extends AbstractDialog {
	/**
	  * Modal SimpleDialog with OK,CANCEL and HELP.
	  * The title is taken from the BlankPanel.
	  */
	public SimpleDialog( Frame parentFrame, BlankPanel panel) {
		this( parentFrame, panel.getTitle(), OK|CANCEL|HELP, panel, true );
	}
	/**
	  * Modal SimpleDialog with the specified buttons.
	  * The title is taken from the BlankPanel.
	  */
	public SimpleDialog( Frame parentFrame, int buttons, BlankPanel panel) {
		this( parentFrame, panel.getTitle(), buttons, panel, true );
	}
	/**
	  * Modal SimpleDialog with the specified buttons and title.
	  */
	public SimpleDialog( Frame parentFrame, String title, int buttons,
						 BlankPanel panel ) {
		this( parentFrame, title, buttons, panel, true );
	}
	/**
	  * General constructor for SimpleDialog.
	  */
	public SimpleDialog( Frame parentFrame, String title, int buttons,
						 BlankPanel panel, boolean modal ) {
		super( parentFrame, title, modal, buttons );
		_basePanel = panel;
		_autoInit = true;
		if (panel != null)
			setBasePanel(panel);
	}
	/**
	 * Enable/disable automatic init() invocation.
	 */
	protected void okInvoked() {
		_basePanel.okCallback();
    }
    protected void helpInvoked() {
		_basePanel.helpCallback();
	}
	/**
	 * Invokes BlankPanel.init(), pack() and super.show().
	 * If autoInit is false, BlankPanel.init() is skipped.
	 */
	public void packAndShow() {
		if (_autoInit) _basePanel.init();
		pack();
		super.show();
	}
	/**
	 * Invokes BlankPanel.init(), and super.show().
	 * If autoInit is false, BlankPanel.init() is skipped.
	 */
    public void show() {
		if (_autoInit) _basePanel.init(); 		
		super.show();
	}
	/**
	  * This method is deprecated.  Using setModal() after the dialog has
	  * been constructed causes unpredictable errors on some Unix systems
	  * with the current version of jdk (1.1.6 and 1.1.7b).  The dialog
	  * must be constructed modal or non-modal, and once constructed, it
	  * cannot be changed.
	  *
	  * @deprecated The dialog must be set modal or non-modal when it is
	  *		        constructed.  Setting modal or non-modal after
	  *             construction causes errors on some systems.
	  */
	public void setModal( boolean state ) {
		Debug.println(1, "SimpleDialog.setModal(): state=" + state +
					  ": ignoring");
	}
	public void setAutoInit(boolean yes) {
		_autoInit = yes;
	}
	protected void setBasePanel( BlankPanel panel ) {
		_basePanel = panel;
		_basePanel.setAbstractDialog(this);
		setComponent(_basePanel);
	}
	/**
	 * Set/unset the waiting cursor.
	 */
	public void setWaitCursor(boolean busy) {
		int c = busy ? Cursor.WAIT_CURSOR : Cursor.DEFAULT_CURSOR;
		setCursor(Cursor.getPredefinedCursor(c));
	}
	
	private boolean _autoInit;
	private BlankPanel _basePanel;
}

