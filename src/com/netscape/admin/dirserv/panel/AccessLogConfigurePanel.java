/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 *
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class AccessLogConfigurePanel extends LogPanel
{

	public AccessLogConfigurePanel(IDSModel model) {
		super(model, "accesslog-configure");
		_helpToken = "configuration-logs-access-help";
	}
	
	public void init() {
		LOG_ATTR_NAME = "nsslapd-accesslog";
		LOG_MODE_ATTR_NAME = "nsslapd-accesslog-mode";
		LOG_PER_DIR_ATTR_NAME = "nsslapd-accesslog-maxlogsperdir";
		LOG_SIZE_ATTR_NAME = "nsslapd-accesslog-maxlogsize";
		ROTATION_SYNC_ENABLED_ATTR_NAME = "nsslapd-accesslog-logrotationsync-enabled";
		ROTATION_SYNCHOUR_ATTR_NAME = "nsslapd-accesslog-logrotationsynchour";
		ROTATION_SYNCMIN_ATTR_NAME = "nsslapd-accesslog-logrotationsyncmin";
		ROTATION_TIME_ATTR_NAME = "nsslapd-accesslog-logrotationtime";
		ROTATION_UNITS_ATTR_NAME = "nsslapd-accesslog-logrotationtimeunit";
		MAX_DISK_SPACE_ATTR_NAME = "nsslapd-accesslog-logmaxdiskspace";
		MIN_FREE_SPACE_ATTR_NAME = "nsslapd-accesslog-logminfreediskspace";
		MAX_DAYS_OLD_ATTR_NAME = "nsslapd-accesslog-logexpirationtime";
		EXPIRATION_UNITS_ATTR_NAME = "nsslapd-accesslog-logexpirationtimeunit";
		ENABLED_ATTR_NAME = "nsslapd-accesslog-logging-enabled";
		LOG_BASE_NAME = "access";
		super.init();
		createEnableArea();
		createConfigArea();
		createLevelArea();
		addBottomGlue ();
		enableFields( _cbEnabled.isSelected() );
		super.postInit();
	}

	private void createLevelArea() {
		_liLogLevel = makeJList("log","accesslogLevel", "");
		JScrollPane spLogLevel = new JScrollPane(_liLogLevel);
		DSEntrySet entries = getDSEntrySet();
		DSEntryBitList logLevelDSEntry = new DSEntryBitList(_liLogLevel, _masks);

		entries.add(LOG_DN, ACCESS_LEVEL_ATTR_NAME, logLevelDSEntry);
		setComponentTable(_liLogLevel, logLevelDSEntry);

		JPanel grid = new GroupPanel(_resource.getString( _section,
			"logLevel-title" ));
		grid.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill      = gbc.HORIZONTAL;
		gbc.anchor    = gbc.CENTER;
		gbc.ipady     = 0;
		gbc.weightx   = 1.0;
		gbc.weighty   = 0.0;
		gbc.gridx     = gbc.RELATIVE;
		gbc.gridy     = gbc.RELATIVE;
		gbc.insets    = getComponentInsets();
		gbc.insets.bottom = UIFactory.getDifferentSpace();
		_myPanel.add(grid, gbc);

		gbc.gridwidth  = 1;
		gbc.gridheight = 1;
		gbc.anchor     = gbc.CENTER;
		gbc.ipady      = 0;
		gbc.weighty    = 0.0;
		gbc.insets     = getComponentInsets();
		gbc.anchor = gbc.WEST;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx    = 1.0;
		gbc.fill = gbc.HORIZONTAL;
		grid.add(spLogLevel,gbc);
	}

	protected void enableFields( boolean enable ) {
		if ( _liLogLevel != null )
			_liLogLevel.setEnabled(enable);
		super.enableFields( enable );
	}

	LogContentPanel getViewerPanel() {
		return new AccessLogContentPanel( getModel() );
	}

	static final String ACCESS_LEVEL_ATTR_NAME = "nsslapd-accesslog-level";
	private JList _liLogLevel;

        // The log level to mask mapping is sparse, unfortunately
	private static final int[] _masks = { 0x0004,
						0x0100,
						0x0200,
						0x20000 };
}
