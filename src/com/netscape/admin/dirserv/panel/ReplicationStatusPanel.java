/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.Enumeration;
import java.util.Vector;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSStatusResourceModel;
import com.netscape.admin.dirserv.panel.BlankPanel;
import com.netscape.admin.dirserv.panel.replication.ReplicationTool;
import com.netscape.admin.dirserv.panel.replication.ReplicationAgreement;
import com.netscape.admin.dirserv.panel.replication.AgreementReader;
import com.netscape.admin.dirserv.panel.replication.AgreementTable;
import com.netscape.admin.dirserv.panel.replication.LabelCellRenderer;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.components.DetailTable;
import javax.swing.table.TableColumn;
import javax.swing.table.AbstractTableModel;

/**
 * A panel to show the replication status of the directory server.
 *
 * @author  jpanchen
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	1/06/97
 * @see     com.netscape.admin.dirserv
 */
 public class ReplicationStatusPanel extends RefreshablePanel {

    /*==========================================================
     * constructors
     *==========================================================*/
     
    /**
     * public constructor. Actual construction is delayed.
     * @param title page title
     */ 
    public ReplicationStatusPanel( IDSModel model, String title ) {
        super( model, title, false );
		_helpToken = "status-replication-help";
        _agreementTable = new AgreementTable();		
        init ();
    }

    /*==========================================================
	 * public methods
     *==========================================================*/
    
    /**
     * Actual page construction
     */
    public void init() {
		if (_isInitialized) {
			return;
		}
		Debug.println( "ReplicationStatusPanel.init" );
		
		_myPanel.setLayout(new BorderLayout());
        
        //======== Outside Panel =========================
        JPanel panel = new JPanel();
        panel.setLayout( new GridBagLayout() );
        Font oldFont = panel.getFont();
        Font font = new Font(oldFont.getName(), Font.BOLD, oldFont.getSize());
        panel.setFont(font);
        _myPanel.add("Center",panel);
        
        //======== Replication Panel =========================
        JPanel repPanel = panel;
        GridBagConstraints gbc = new GridBagConstraints();

        JPanel panelRefresh = createRefreshArea();
        ReplicationTool.resetGBC(gbc);
		gbc.gridx = 0;
		int different = UIFactory.getDifferentSpace();
		gbc.insets = new Insets(0,0,0,0);
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.anchor = gbc.WEST;
		repPanel.add( panelRefresh, gbc );

        //agreement table
        _repModel = new AgreementTableModel();
        for (int i=0; i<HEADERS.length; i++) {
            _repModel.addColumn(HEADERS[i]);
        }

        _repTable = new CustomDetailTable(_repModel);
        JTable theTable = _repTable.getTable();
	    theTable.getTableHeader().setReorderingAllowed(false);
		theTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		theTable.setRequestFocusEnabled(true);
		theTable.setRowSelectionAllowed(true);
		theTable.setColumnSelectionAllowed(false);		
		theTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		theTable.getAccessibleContext().setAccessibleDescription(_resource.getString("replication-statusTab",
																					"table-description"));
		
		/* Only show the first three columns of the table.  This has to be in sync with the String[]
		   HEADERS and TABLE_WIDTHS */
		for (int i = 0; i < 3; i++) {
			TableColumn column = theTable.getColumn(HEADERS[i]);
			column.setCellRenderer(new DefaultTableCellRenderer());
			column.setPreferredWidth( TABLE_WIDTHS[i] );
			column.setResizable( true );
		}
		for (int i = 3; i<HEADERS.length; i++) {
			TableColumn column = theTable.getColumn(HEADERS[i]);
			column.setCellRenderer(new DefaultTableCellRenderer());
			column.setResizable(false);
			column.setMinWidth(0);
			column.setMaxWidth(0);
			column.setPreferredWidth(0);
		}
    
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.fill = gbc.BOTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.weighty=1.0;
        repPanel.add(_repTable, gbc);        
        
		refreshAgreement();
		
		/* We Select the first row (if it exists) by default */
		if (theTable.getRowCount() > 0) {
			theTable.setRowSelectionInterval(0, 0);			
			_repTable.updateRow(0);
		}
		_isInitialized = true;
   }
    

    public boolean refresh() {
		getModel().setWaitCursor( true );

        Debug.println (6, "ReplicationStatusPanel.refresh()");

        if (ReplicationTool.authenticate(getModel())) {
			
			/* We try to keep the same agreement selected we had before we did the refresh,
			 for doing this we base on the name of the agreement (the object in the first column
			 of each row) */
			JTable theTable = _repTable.getTable();
			int selectedRow = theTable.getSelectedRow();
			Object selectedRowName = null;
			if (selectedRow >= 0) {
				selectedRowName = _repModel.getValueAt(selectedRow, 0);
			}
			refreshAgreement();
			int modelRowCount = _repModel.getRowCount();	
			/* If we don't find the agreement that was selected we choose by default the row '0' */
			selectedRow = 0;
			if (selectedRowName != null) {
				for (int i=0; i < modelRowCount; i++) {
					if (selectedRowName.equals(_repModel.getValueAt(i, 0))) {
						selectedRow = i;
						break;
					}
				}
			}
			if (theTable.getRowCount() > selectedRow) {
				theTable.setRowSelectionInterval(selectedRow, selectedRow);
				_repTable.updateRow(selectedRow);
			}
		}

		getModel().setWaitCursor( false );
		return true;
	}
    

	// Override listener methods so we don't get BlankPanel validation
    //== DocumentListener ==
    public void insertUpdate(DocumentEvent e) {
    }
    public void removeUpdate(DocumentEvent e) {
    }
    public void changedUpdate(DocumentEvent e) {
    }

    //== ItemListener ==
    public void itemStateChanged(ItemEvent e) {
    }

    //== ChangeListener ==
    public void stateChanged(ChangeEvent e) {
    }

    //== ListSelectionListener ==
    public void valueChanged(ListSelectionEvent e) {
    }
    
    private void refreshAgreement() {
        AgreementReader reader =
			new AgreementReader(getModel().getServerInfo(),
								_agreementTable,
								DSUtil.MAPPING_TREE_BASE_DN);			 
        _agreementTable.removeAllElements();
        _repModel.removeAllRows();
		
        reader.readAgreements();
        if (reader.isSuccess()) {
            for (Enumeration e = _agreementTable.elements() ;
				 e.hasMoreElements() ;) {
                ReplicationAgreement agreement =
					(ReplicationAgreement) e.nextElement();
                _repModel.addRow(createRow(agreement));			
            }
        }        
    }
    	 
	 /**
	  * Create row to be displayed in the status tabel
	  */
    private Vector createRow(ReplicationAgreement agreement) {
        agreement.updateReplicaStatus();
        Vector _row = new Vector();
        Icon icon = null;
        boolean isError = false;
	String statusString = null;

	// figure out what the error state is
        boolean setColor = false;
        //which icon & color	
        if (isError) {
            icon = DSUtil.getPackageImage(_taskImage);
            setColor = true;
	    statusString = _resource.getString("replication-statusTab-unknown",
					       "label");
        } else {
	    if (agreement.getInProgress().equalsIgnoreCase("true")) {
                icon = DSUtil.getPackageImage(_taskImage);
                setColor = true;
            } else {
                icon = DSUtil.getPackageImage(_okImage);
            }
        }

        //name
        JLabel name =
			new JLabel(agreement.getNickname(), icon, JLabel.LEFT);
        name.setToolTipText(agreement.getNickname());        
        _row.addElement(name);
        
        //subtree
        JLabel subtreeInfo = new JLabel(agreement.getReplicatedSubtree(), 
										icon,
										JLabel.LEFT);
        subtreeInfo.setToolTipText(_resource.getString(
			"replication-content-replicate",
			"label")+agreement.getReplicatedSubtree());        
        _row.addElement(subtreeInfo);        
            
        //consumer
        JLabel consumerInfo = new JLabel(
            agreement.getConsumerHost()+":"+agreement.getConsumerPort(),
            DSUtil.getPackageImage(_hostImage),
            JLabel.LEFT);
        consumerInfo.setToolTipText(_resource.getString(
			"replication-content-replicate",
			"label")+agreement.getReplicatedSubtree());    
        _row.addElement(consumerInfo);   
  
		//supplier
        JLabel supplierInfo = new JLabel(
            agreement.getSupplierHost()+":"+agreement.getSupplierPort(),
            DSUtil.getPackageImage(_hostImage),
            JLabel.LEFT);
        supplierInfo.setToolTipText(_resource.getString(
			"replication-content-replicate",
			"label")+agreement.getReplicatedSubtree());        
        _row.addElement(supplierInfo);
        
        //N changes last update
        JLabel nChangesLabel =
			new JLabel(agreement.getNChangesLast(),JLabel.LEFT);        
        if (setColor)
            nChangesLabel.setForeground(Color.red);
        _row.addElement(nChangesLabel);
        
        //Last update begin time
        JLabel updateBeganLabel =
			new JLabel(agreement.getLastUpdateBegin(),JLabel.LEFT);        
        if (setColor)
            updateBeganLabel.setForeground(Color.red);   
        _row.addElement(updateBeganLabel);
        
        //Last update end time
        JLabel updateEndedLabel =
			new JLabel(agreement.getLastUpdateEnd(),JLabel.LEFT);        
        if (setColor)
            updateEndedLabel.setForeground(Color.red);
        _row.addElement(updateEndedLabel);
        
        //status info
        JLabel status = new JLabel(ReplicationTool.convertStatusMessage(agreement.getLastUpdateStatus()),
								   JLabel.LEFT); 
        _row.addElement(status);     

		//consumer initialization
		JLabel consumerInitializationInProgress = new JLabel(agreement.getConsumerInitializationInProgress(),JLabel.LEFT);
		_row.addElement(consumerInitializationInProgress);
		
		JLabel consumerInitializationStatus = new JLabel(ReplicationTool.convertStatusMessage(agreement.getConsumerInitializationStatus()),
														 JLabel.LEFT);
		_row.addElement(consumerInitializationStatus);

		JLabel consumerInitializationBegin = new JLabel(agreement.getConsumerInitializationBegin(),JLabel.LEFT);
		_row.addElement(consumerInitializationBegin);
		
		JLabel consumerInitializationEnd = new JLabel(agreement.getConsumerInitializationEnd(),JLabel.LEFT);	
		_row.addElement(consumerInitializationEnd);			
	
        return _row;
    }
    
    class AgreementTableModel extends AbstractTableModel {
        
        AgreementTableModel() {}
        
        public int getColumnCount() {
            return _columnNames.size();
        }

        public int getRowCount() {
            if (getColumnCount() > 0 ) {
                Vector v = (Vector)_tableColumns.elementAt(0);
                return v.size();
            }
            return 0;
        }

        public String getColumnName(int column) {
            if (column >= _columnNames.size())
                return "";
            return (String)_columnNames.elementAt(column);
        }

        public boolean isCellEditable(int row, int col) {
            return false;
        }

        public void setValueAt(Object aValue, int row, int column) {
                Vector col = (Vector)_tableColumns.elementAt(column);
                col.setElementAt(aValue, row);
        }

        public Object getValueAt(int row, int col) {
            if ( getColumnCount() > 0 ) {
				if (col < _tableColumns.size()) {
					Vector v = (Vector)_tableColumns.elementAt(col);
					if (row < v.size()) {
						Object o = v.elementAt(row);
						if (o != null && o instanceof JLabel) {
							return ((JLabel)o).getText();
						}
						return o;
					}					
				}
			}
            return null;
        }

        void removeAllRows() {
            for (int i=0; i<_tableColumns.size(); i++) {
                Vector v = (Vector)_tableColumns.elementAt(i);
                v.removeAllElements();
            }
        }

        void addRow(Vector values) {
            int row = 0;
            for (int i=0; i < values.size(); i++) {
                Vector v = (Vector)_tableColumns.elementAt(i);
                v.addElement(values.elementAt(i));
                if (i == 0)
                    row = v.size() - 1;
            }           
        }

        void addColumn(String name) {
            _columnNames.addElement(name);
            _tableColumns.addElement(new Vector());
        }

        public Class getColumnClass(int c) {
            Class cl = null;
            // need to handle the case where getColumnClass is called before
            // any rows have been added
            if (getColumnCount() > 0 && _tableColumns != null) {
                Vector v = (Vector)_tableColumns.elementAt(c);
                if (v != null && v.size() > 0) {
                    cl = v.elementAt(0).getClass();
                }
            }
            if (cl == null) {
                String dummy = "";
                cl = dummy.getClass();
            }

            return cl;
        }

        private Vector _columnNames = new Vector();
        private Vector _tableColumns = new Vector();
    };
    
    /*==========================================================
     * variables
     *==========================================================*/   
    private AgreementTableModel _repModel;
    private JScrollPane _scrollPane;
    private CustomDetailTable _repTable;
    private AgreementTable _agreementTable; 
    private ImageIcon _icon;
	/* We use  this TABLE_WIDTHS to show the first 3 columns.  This has to be in sync with the String[]
	   HEADERS */
	static private final int[] TABLE_WIDTHS = { 120, 120, 120, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	//get resource bundle
    private static ResourceSet _resource =
	    new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");     
    private static final String _errorImage = "red-ball-small.gif";
    private static final String _taskImage = "task.gif";
    private static final String _okImage = "green-ball-small.gif";
    private static final String _hostImage = "DirectoryServer.gif";

    private static final String[] HEADERS = {
        _resource.getString("replication-statusTab-columnName-Name",
							"label"),
        _resource.getString("replication-statusTab-columnName-Subtree",
							"label"),
        _resource.getString("replication-statusTab-columnName-Consumer",
							"label"),
		_resource.getString("replication-statusTab-columnName-Supplier",
							"label"),
        _resource.getString("replication-statusTab-columnName-nChangesLast",
							"label"),
        _resource.getString("replication-statusTab-columnName-lastUpdateBegin",
							"label"),
        _resource.getString("replication-statusTab-columnName-lastUpdateEnd",
							"label"),
        _resource.getString("replication-statusTab-columnName-Status",
							"label"),
		_resource.getString("replication-statusTab-columnName-consumerInitializationInProgress",
							"label"),
		_resource.getString("replication-statusTab-columnName-consumerInitializationStatus",
							"label"),
		_resource.getString("replication-statusTab-columnName-consumerInitializationBegin",
							"label"),
		_resource.getString("replication-statusTab-columnName-consumerInitializationEnd",
							"label")};
 }

/**
  * This class has been constructed to bypass bug 525630 in the Admin Console
  */

class CustomDetailTable extends DetailTable {
	/**
     * Constructs a CustomDetailTable which is initialized with <i>dm</i> as the
     * data model, a default column model, and a default selection
     * model.
     *
     * @param dm        The data model for the table
     * @see JTable#createDefaultColumnModel
     * @see JTable#createDefaultSelectionModel
     */
    public CustomDetailTable(TableModel dm)  {
		super(dm, null, null);
		initialize();
    }

	protected void initialize() {
		if(!_isInitialized) {
            super.initialize();
			_isInitialized = true;			
		}
    }

	protected void sizeDetailPanel() {
		int newLocation = (int)getSize().getHeight();
		newLocation = newLocation / 3;

        if(newLocation != 0) {
			Component[] comps = getComponents();
			for (int i=0; i < comps.length; i++) {
				if (comps[i] instanceof JSplitPane) {
					((JSplitPane)comps[i]).setDividerLocation(newLocation);
					break;
				}
			}
		}
	}

	/**
	  * This method updates the contents of the panel for a certain row-agreement.
	  * It is used because updateRow is only called by the ListListeners and TableListeners
	  * of the DetailTable class when we change the selected row.
	  * As we want to refresh the contents of each row-agreement after we refresh this 
	  * method is necessary.	  
	  */
	public void updateRow(int rowIndex) {
		super.rowSelected(getTable(), null, rowIndex);
	}
	
	private boolean _isInitialized = false;
}
