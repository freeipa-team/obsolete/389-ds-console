/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.File;
import javax.swing.*;
import javax.swing.border.*;
//import javax.swing.event.*;
//import javax.swing.table.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import com.netscape.management.nmclf.SuiConstants;
/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class LDBMSettingsPanel extends BlankPanel implements  SuiConstants{
     
    public LDBMSettingsPanel(IDSModel model, LDAPEntry InstEntry) {
		super(model, _section, true );
		_helpToken = "configuration-database-settings-help";
		_dnEntry = InstEntry.getDN();
		_configEntry = InstEntry;
		_refreshWhenSelect = false;
	}
	
	public void init() {
		if (_isInitialized) {
			return;
		}
		/* database suffix */
		_lSuffixLabel =  makeJLabel( _section, "suffix" );
		
		_lSuffixText =  new JLabel();
		_lSuffixLabel.setLabelFor(_lSuffixText);
		if ( _configEntry.getAttribute( SUFFIX_ATTR_NAM ) != null ) {
			_lSuffixText.setText( _configEntry.getAttribute( SUFFIX_ATTR_NAM ).getStringValueArray()[0]);
		} else {
			_lSuffixText.setText( SUFFIX_ATTR_NULL );
		}
		
	
		/* Extract monitor info : database location/read only status */
		_lLDBMLoc = makeJLabel(_section,"DatabaseLoc");
		_tfLDBMLoc = new JLabel ();		
		_lLDBMLoc.setLabelFor(_tfLDBMLoc);
	    _cbIsReadOnly = makeJCheckBox(_section,"isReadOnly");

		// hooo I hate what I gone do ... sorry 
		// The idea is : as we caan't have the db loc from the conf. I have to 
		// retreive the db loc from monitor info. But attributes which may contain it
		// are never the same !!!
		try {
			LDAPEntry monitorEntry = null;
            LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
            monitorEntry = ldc.read( "cn=monitor," + _dnEntry);
			if ( monitorEntry != null ){
				Debug.println("LDBMSettingsPanel.init monitor entry =" + monitorEntry.getDN());				
				boolean bfind = false;
				int i = 0;

				while (( !bfind ) && (i<50)) {
					Debug.println("---> let's try " + DBFILE_ATTR_NAM + i );
					LDAPAttribute attrdb = monitorEntry.getAttribute( DBFILE_ATTR_NAM + i );
					i++;
					if ( attrdb != null ) {						
						bfind = true;
						Enumeration e = attrdb.getStringValues();
						while( e.hasMoreElements() ) {
							File tmpF = new File( (String)e.nextElement());
							String aa = tmpF.getParent();
							if ( aa != null ){
								_tfLDBMLoc.setText( tmpF.getParent() );
							}
						}
						
					}

				}
				/* Now the status */
				/*
				_cbIsReadOnly.setEnabled( false );
				LDAPAttribute attrro =  monitorEntry.getAttribute( READ_ONLY_ATTR_NAM );
				_cbIsReadOnly.setSelected( false );
				if ( attrro != null ){
					Enumeration e = attrro.getStringValues();
					if( e.hasMoreElements() ) {
						boolean bstate = ((String)e.nextElement()).equalsIgnoreCase("on");
						_cbIsReadOnly.setSelected( bstate );
					}
				}
				*/
			}
		} catch ( LDAPException lde ) {
			Debug.println("LDBMSettingsPanel.init monitor entry not found");
		}

		DSEntrySet entries = getDSEntrySet();

		/*
		if ( canChangeDBDirectory ) {
			grid.add(_lLDBMLoc,gbc);
        
			gbc.gridwidth = 2;
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
			gbc.anchor = gbc.WEST;
			grid.add(_tfLDBMLoc,gbc);

			gbc.fill = gbc.NONE;
			gbc.weightx = 0.0;
			gbc.gridwidth = gbc.REMAINDER;
			grid.add(_bLDBMLoc,gbc);
		} else {
			Box box = Box.createHorizontalBox();
			box.add( _lLDBMLoc );
			box.add( box.createHorizontalStrut( 3 ) );
			box.add( _tfLDBMLoc );
			box.add( box.createGlue() );
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
			grid.add( box, gbc );
		}
		*/

		
		DSEntryBoolean roDSEntry = new DSEntryBoolean("off", _cbIsReadOnly);
		entries.add(_dnEntry, READ_ONLY_ATTR_NAM, roDSEntry);
		setComponentTable(_cbIsReadOnly, roDSEntry);

		/* Cache info */
		_cbcachememsizeUnits = makeJComboBox(_section, "cachememsizeunits", null);
		_tfcachememsize = makeNumericalJTextField( _section, "cachememsize" );
		_lcachememsize = makeJLabel(_section, "cachememsize");
		_lcachememsize.setLabelFor(_tfcachememsize);

		_cachememsizeDSEntry = new DSEntryLong(null,
		                                       _tfcachememsize, _lcachememsize,
		                                       CACHEMEM_SIZE_NUM_MIN_VAL,
		                                       CACHEMEM_SIZE_NUM_MAX_VAL, 1);
		entries.add(_dnEntry, DB_CACHEMEMSIZE_ATTR_NAM, _cachememsizeDSEntry);
		setComponentTable(_tfcachememsize, _cachememsizeDSEntry);

		/* Now let's build this */
		GridBagLayout Mabag = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
		//        Magbc.gridwidth  = Magbc.REMAINDER;
        gbc.gridwidth	 = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 0;
        gbc.weighty    = 0;
        gbc.fill       = gbc.BOTH;
        gbc.anchor     = gbc.CENTER;
        gbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
									0,DEFAULT_PADDING);
        gbc.ipadx = 0;
        gbc.ipady = 0;
	 
        JPanel grid = _myPanel;
        grid.setLayout( Mabag );

		gbc.gridx      = 0;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
		grid.add( _lSuffixLabel, gbc);

		gbc.gridx      = 1;
		gbc.anchor = gbc.WEST; 
		grid.add( _lSuffixText, gbc);
		gbc.gridy++;

		gbc.gridx = 0;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
		grid.add( _lLDBMLoc, gbc);

		gbc.gridx      = 1;
		gbc.anchor = gbc.WEST; 
		grid.add( _tfLDBMLoc, gbc);
		gbc.gridy++;		

		gbc.gridx = 0;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
		grid.add( _lcachememsize, gbc);

		gbc.gridx = 1;
		gbc.anchor = gbc.CENTER;
		gbc.fill = gbc.HORIZONTAL;
		grid.add( _tfcachememsize, gbc);
		
		gbc.gridx = 2;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.WEST;
		grid.add(_cbcachememsizeUnits, gbc);
		gbc.gridy++;

		gbc.gridx = 0;
		gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;		
        grid.add(_cbIsReadOnly, gbc);
		gbc.gridy++;


        addBottomGlue ();		
		_isInitialized = true;
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(_cbcachememsizeUnits)) {
            String selectedUnit = (String)_cbcachememsizeUnits.getSelectedItem();
            Debug.println("actionPerformed: selected cachememsizeUnit: " + selectedUnit);
            if (selectedUnit.equalsIgnoreCase("bytes")) {
                _cachememsizeDSEntry.setScaleFactor(1);
                _cachememsizeDSEntry.show();
            } else if (selectedUnit.equalsIgnoreCase("KB")) {
                _cachememsizeDSEntry.setScaleFactor(1024);
                _cachememsizeDSEntry.show();
            } else if (selectedUnit.equalsIgnoreCase("MB")) {
                _cachememsizeDSEntry.setScaleFactor(1024*1024);
                _cachememsizeDSEntry.show();
            } else if (selectedUnit.equalsIgnoreCase("GB")) {
                _cachememsizeDSEntry.setScaleFactor(1024*1024*1024);
                _cachememsizeDSEntry.show();
            } else {
                Debug.println("actionPerformed: ignore unknown unit: " + selectedUnit);
            }
        }
        super.actionPerformed(e);
    }

	private JCheckBox	_cbIsReadOnly;
	private JLabel		_tfLDBMLoc;
	private JLabel		_lLDBMLoc;
	private JButton		_bLDBMLoc;
	private LDAPEntry	_configEntry = null;
	private String		_dnEntry;

	private JLabel		_lSuffixText;
	private JLabel		_lSuffixLabel;

	private JTextField	_tfcachememsize;
	private JLabel		_lcachememsize;
	private DSEntryLong _cachememsizeDSEntry;
	private JComboBox   _cbcachememsizeUnits;

	private static final long CACHEMEM_SIZE_NUM_MIN_VAL = 1; // e.g., allow 1GB
	private static final long CACHEMEM_SIZE_NUM_MAX_VAL = Long.MAX_VALUE;
	private static final String DB_CACHEMEMSIZE_ATTR_NAM = "nsslapd-cachememsize";

	private static final String _section = "dbsettings";
	private static final String DB_FILE_LOC_ATTR_NAME = "nsslapd-directory";
	private static final String SUFFIX_ATTR_NAM= "nsslapd-suffix";
	private static final String SUFFIX_ATTR_NULL="";
	private static final String DBFILE_ATTR_NAM = "dbfilename-";
	//	private static final String READ_ONLY_ATTR_NAM = "nsslapd-readonly";
	private static final String READ_ONLY_ATTR_NAM = "nsslapd-readonly";

    //copy from BlankPanel
    final static int DEFAULT_PADDING = 6;
    final static Insets DEFAULT_EMPTY_INSETS = new Insets(0,0,0,0);
    final static Insets BOTTOM_INSETS = new Insets(6,6,6,6);
}

