/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.nmclf.*;
import netscape.ldap.*;
import netscape.ldap.util.*;


/**
 * confirmDeleteSuffixPanel
 * 
 *
 * @version 1.0
 * @author rmarco
 **/
public class confirmDeleteSuffixPanel extends BlankPanel 
    implements SuiConstants {
    public confirmDeleteSuffixPanel( IDSModel model, String dnSuffix2delete ) {
	super( model, _section );
	_helpToken = "configuration-confirm-delete-suffix-dbox-help";
	_model = model;
	setTitle( DSUtil._resource.getString( _section, "title"));
	_dnSuffix2delete = dnSuffix2delete;
    }

    public void init() {
	
	GridBagLayout Mabag = new GridBagLayout();
	GridBagConstraints Magbc = new GridBagConstraints() ;
        Magbc.gridx      = 0;
        Magbc.gridy      = 0;
	//        Magbc.gridwidth  = Magbc.REMAINDER;
        Magbc.gridwidth	 = 1;
        Magbc.gridheight = 1;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
        Magbc.fill       = Magbc.BOTH;
        Magbc.anchor     = Magbc.WEST;
        Magbc.insets     = new Insets(0,DIFFERENT_COMPONENT_SPACE,
				      COMPONENT_SPACE,0);
        Magbc.ipadx = 0;
        Magbc.ipady = 0;
	   	
	_myPanel.setLayout(Mabag);
	
	String[] args = {_dnSuffix2delete};
	_introLabel = new MultilineLabel(resource.getString( _section,
	                                 "intro-label", args ));
	_introLabel.setLineWrap(false);
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.REMAINDER;
	Magbc.gridy++;
	_myPanel.add( _introLabel,Magbc);		


	ButtonGroup deleteGroup = new ButtonGroup();

	_rbDeleteAll =  new JRadioButton( resource.getString( _section, 
							      "delete-all"));
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.REMAINDER;
	Magbc.gridy++;
	_rbDeleteAll.setSelected( INIT_STATUS );
	deleteGroup.add( _rbDeleteAll );
	_myPanel.add( _rbDeleteAll, Magbc);

	_rbDeleteOne =  new JRadioButton( resource.getString( _section, 
							      "delete-one"));
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.REMAINDER;
	Magbc.gridy++;
	_rbDeleteOne.setSelected( ! INIT_STATUS );
	deleteGroup.add( _rbDeleteOne );
	_myPanel.add( _rbDeleteOne, Magbc);
	addBottomGlue();
    }


    public void okCallback() {
	/* No state to preserve */
	if( _rbDeleteAll.isSelected()) {
	    _return = ALL;
	} else if( _rbDeleteOne.isSelected()) {
	    _return = ONE;
	} else {
	    _return = NO;
	}
	Debug.println( "confirmDeleteSuffixPanel.okCallback: " + _return );
	hideDialog();
    }

       
    public int getReturn() {
	return _return;
    }


    private IDSModel			_model = null;
    private int				_return = NO;
    private MultilineLabel		_introLabel;
    private JRadioButton		_rbDeleteAll;
    private JRadioButton		_rbDeleteOne;
    
    private String			_dnSuffix2delete;
    
    private static final boolean	INIT_STATUS = true;
    private final static String _section = "confirmdeletesuffix";
    private static ResourceSet resource = DSUtil._resource;

    public static final int NO  = 0;
    public static final int ALL = 1;
    public static final int ONE = 2;

}
