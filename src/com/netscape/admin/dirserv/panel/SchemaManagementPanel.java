/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class SchemaManagementPanel extends DSTabbedPanel
{

	public SchemaManagementPanel(IDSModel model)
	{
		super( model, false );

		addTab( _objectClassesTab = new SchemaObjectClassesPanel( model ) );

		addTab( _attributesTab = new SchemaAttributesPanel( model ) );

		addTab( _matchingRulesTab = new SchemaMatchingRulesPanel( model ) );

		_tabbedPane.setSelectedIndex( 0 );
	}

	private BlankPanel _objectClassesTab;
	private BlankPanel _attributesTab;
	private BlankPanel _matchingRulesTab;
}
