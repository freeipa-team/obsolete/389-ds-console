/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

/* ??? The functionality here should be moved into BlankPanel,
   as a makeFileField method
*/

package com.netscape.admin.dirserv.panel;

import java.io.File;
import java.awt.Insets;
import java.util.Vector;
import java.util.Enumeration;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.swing.*;

import com.netscape.admin.dirserv.*;
import com.netscape.management.client.util.*;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server
 *
 * @version
 * @author
 */
class DSFileField extends JTextField implements IDSEntry {
    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle text entry fields which accept file and directory names.  One
     * model value, one widget.  This class verifies that the path and/or
     * file name is valid.
     *
     * @param model the initial value, if any, or just null
     * @param view the JTextField used as the text entry field
     * @param isFile sets whether the path should be a file or directory
	 * (used when validating)
     */
    public DSFileField(
        BlankPanel panel,
        String panelname,
        String keyword,
        String defval,
        int defcols,
        int fileType ) {
        super();
        _model = DSUtil._resource.getString(panelname,
                                                 keyword + "-default");
        if (_model == null)
            _model = defval;
        int columns = 0;
        try {
            columns = Integer.parseInt(
                DSUtil._resource.getString(panelname, keyword + "-columns")
            );
        } catch (Exception e) {
            // if invalid integer, just use default
        }
        if (columns < 1)
            columns = defcols;
        String ttip = DSUtil._resource.getString(panelname, keyword + "-ttip");
        if (ttip != null) {
            setToolTipText(ttip);
        }
        if (_model != null)
            setText(_model);
        if (columns!= -1)
            setColumns(columns);

		setMargin( UIFactory.getTextInsets() );

        addActionListener(panel);
        getDocument().addDocumentListener(panel);  //detect text changes
        this.fileType = fileType;
        
        // Now try to determine if the MCC is running on the server.
		isLocal = panel.isLocal();
    }
    
    public DSFileField(
        BlankPanel panel,
        String panelname,
        String keyword,
        String defval,
        int defcols ) {
        this(panel, panelname, keyword,  defval, defcols, FILE);
    }

    public void show() {
        // update the view
		setText(_model);
	}

	public void store() {
	    _model = getText();
    }

    /**
     * several types of validation are done; first, the text field is checked
     * to see if the user has typed in anything.  Next, the file name is
	 * checked to see if it is a directory and returns
     * true if isFile = false.  Then it varifies that  the parent of the file
     * is a directory.  If any of these conditions fail,
     * false is returned, otherwise, true is returned.
     *
     * @return false if the text field does not contain a valid integer
     */
	public boolean dsValidate() {

		String name = getText();
        File file = new File(name);
        String err = null; // error number
        String args[] = null; // additional arguments for error message
        
        // Cannot do any validation remotely, assume valid
        if (!isLocal())
            return true;
            
        if (file.isDirectory() && fileType == FILE) {
            // We are only looking for valid files
            err = "directoryButNeedFile";
            args = new String[1];
            args[0] = name;
            Debug.println("You need to enter a file name as well as " +
						  "a dir: " + file.getPath()); 
        } else if (file.isFile() && fileType == DIRECTORY) {
            // Want a directory but got a file 
            err = "fileButNeedDirectory";
            args = new String[1];
            args[0] = name;
        } else if (file.isFile()) {
            // Make sure that parent dir is valid
    	    String parentDir = file.getParent();
			if ( parentDir == null ) {
				Debug.println( "The parent directory of " + name +
									" is null" );
				err = "notValidDirectory";
				args = new String[1];
				args[0] = name;
			} else {
				File parent = new File(file.getParent());
				if (!parent.isDirectory()) {
					err = "notValidDirectory";
					args = new String[1];
					args[0] = parent.getPath();
					Debug.println("Not a valid directory!");
				}
	        }
	    }
	    
        if (err != null) { // popup an error dialog
			DSUtil.showErrorDialog( null, err, args, "general" );
			grabFocus(); // put the keyboard focus in this field
        }
        
		return (err == null); // if err was set, there must have been an error
	}

    /**
     * This method converts the String representation of the attribute
     * stored in the Directory Server to the local model representation
     *
     * @param String remoteValue This is the value as stored in the DS
     */
    public void remoteToLocal(String remoteValue) {
        // removes the previously set default value, if any
        _model = remoteValue;
    }
    
    /**
     * This method is used to convert multivalued attributes
     * stored in the Directory Server to the local model representation
     *
     * @param remoteValue An enumeration of Strings
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#show
     */
    public void remoteToLocal(Enumeration remoteValues) {
       _model = (String)remoteValues.nextElement();
    }

    /**
     * This method is used to get the local values in the form used to
     * store the values in the Directory Server
     *
     * @return An array of String values
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#store
     */
    public String[] localToRemote() {
        String retval[] = null; 
        if (_model != null) {
            retval = new String[1];
            retval[0] = _model;
        }

        return retval;
    }
     
    /**
     * Use to determine if file operations like using JFileChooser
     * are available becuase MCC is running on the same machine as
     * the server.  File operations are not available for clients running
     * remotely.
     *
     * @return true if client is the same machine as the server
     */
    public boolean isLocal() { return isLocal;}
    
    String _model;
   
    // is a directory when false
    private int fileType = FILE;
    private boolean isLocal = false;
    
    static public int DIRECTORY = 0;
    static public int FILE = 1;
    static public int BOTH = 2;
}
