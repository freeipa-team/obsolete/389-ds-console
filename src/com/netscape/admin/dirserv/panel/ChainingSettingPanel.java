/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.File;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
// import javax.swing.table.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.*;
import com.netscape.admin.dirserv.*;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import com.netscape.management.nmclf.SuiConstants;
import com.netscape.admin.dirserv.DSUtil;
/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ChainingSettingPanel extends BlankPanel 
    implements  SuiConstants{
     
    public ChainingSettingPanel(IDSModel model, LDAPEntry InstEntry) {
		super( model, _section, true );
		_helpToken = "configuration-chaining-settings-help";
		_dnEntry = InstEntry.getDN();
		_configEntry = InstEntry;
		_model = model;			
		_refreshWhenSelect = false;
    }
	
    public void init() {
		if (_isInitialized) {
			return;
		}
	GridBagLayout bag = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth	 = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 0;
        gbc.weighty    = 0;
        gbc.fill       = gbc.BOTH;
        gbc.anchor     = gbc.CENTER;
        gbc.insets     = new Insets(0, DIFFERENT_COMPONENT_SPACE,
				    0, 0);
        gbc.ipadx = 0;
        gbc.ipady = 0;
	 

        _myPanel.setLayout( bag );

	prepData();		

	_ctrlListLabel = makeJLabel ( _section, "ctrl-list" );
	gbc.gridy++;
	gbc.gridx = 0;
        gbc.weightx    = 0;
        gbc.weighty    = 0;
	gbc.fill= gbc.NONE;
	gbc.anchor = gbc.WEST;		
	_myPanel.add( _ctrlListLabel, gbc );		  

	_ctrlData = new DefaultListModel();
	_ctrlList = new JList( _ctrlData );
	_ctrlListLabel.setLabelFor(_ctrlList);
	_ctrlList.setVisibleRowCount(ROWS);
	_ctrlList.setCellRenderer( new ControlCellRenderer() );
	for(int i=0;
	    ( _transControls != null) && 
		i < _transControls.length; 
	    i++ ){
	    _ctrlData.addElement( _transControls[i]);
	}
	JScrollPane scrollCtrl = new JScrollPane();
	scrollCtrl.getViewport().setView(_ctrlList);
	gbc.gridx = 0;
	gbc.gridy++;
	gbc.fill= gbc.BOTH;		
        gbc.weightx    = 1;
        gbc.weighty    = 1;
	gbc.insets     = new Insets(0, 
				    DIFFERENT_COMPONENT_SPACE, 
				    COMPONENT_SPACE,
				    0);
	_myPanel.add( scrollCtrl, gbc);		
	
	JPanel buttonCtrlPanel = new JPanel( new GridBagLayout ());
	gbc.gridwidth  = 1;
	gbc.gridx = 1;
	gbc.weightx    = 0;
        gbc.weighty    = 0;
	gbc.insets     = new Insets(0, 
				    DIFFERENT_COMPONENT_SPACE, 
				    COMPONENT_SPACE,
				    0);
	gbc.fill= gbc.HORIZONTAL;	
	gbc.anchor = gbc.NORTHEAST;
	_myPanel.add( buttonCtrlPanel, gbc );

	GridBagConstraints bctgbc = new GridBagConstraints() ;
        bctgbc.gridx      = 0;
        bctgbc.gridy      = 0;
        bctgbc.gridwidth	 = 1;
        bctgbc.gridheight = 1;
        bctgbc.weightx    = 0;
        bctgbc.weighty    = 0;
        bctgbc.fill       = bctgbc.HORIZONTAL;
        bctgbc.anchor     = bctgbc.CENTER;
        bctgbc.insets     = new Insets(0, DIFFERENT_COMPONENT_SPACE,
				    COMPONENT_SPACE, 0);
        bctgbc.ipadx = 0;
        bctgbc.ipady = 0;




	_bAddControl = UIFactory.makeJButton(this, _section, "addctrl", (ResourceSet)null);
	_bAddControl.setActionCommand(ADD_CTRL);		
	_bAddControl.setEnabled( true );

	buttonCtrlPanel.add( _bAddControl, bctgbc);
	
	
	_bDelControl = UIFactory.makeJButton(this, _section, "delctrl", (ResourceSet)null);
	_bDelControl.setActionCommand(DELETE_CTRL);
	_bDelControl.setEnabled(false);
	bctgbc.gridy++;
	buttonCtrlPanel.add( _bDelControl,bctgbc);


	_compListLabel = makeJLabel ( _section, "comp-list" );
	gbc.gridy++;
	gbc.gridx = 0;
        gbc.weightx    = 0;
        gbc.weighty    = 0;
	gbc.fill= gbc.NONE;
	gbc.anchor = gbc.WEST;		
	_myPanel.add( _compListLabel, gbc );		

	_compData = new DefaultListModel();
	_compList = new JList( _compData );
	_compListLabel.setLabelFor(_compList);
	_compList.setVisibleRowCount(ROWS);
	_compList.setCellRenderer( new ControlCellRenderer() );
	for(int i=0;
	    ( _activeComponent != null) && 
		i < _activeComponent.length; 
	    i++ ){
	    _compData.addElement( _activeComponent[i]);
	}
	JScrollPane scrollComp = new JScrollPane();
	scrollComp.getViewport().setView(_compList);
	gbc.gridx = 0;
	gbc.gridy++;
	gbc.fill= gbc.BOTH;		
        gbc.weightx    = 1;
        gbc.weighty    = 1;
	gbc.insets     = new Insets(0, 
				    DIFFERENT_COMPONENT_SPACE, 
				    COMPONENT_SPACE,
				    0);
	_myPanel.add( scrollComp, gbc);	


	JPanel buttonCompoPanel = new JPanel( new GridBagLayout ());
	gbc.gridwidth  = 1;
	gbc.gridx = 1;
	gbc.weightx    = 0;
        gbc.weighty    = 0;
	gbc.insets     = new Insets(0, 
				    DIFFERENT_COMPONENT_SPACE, 
				    COMPONENT_SPACE,
				    0);
	gbc.fill= gbc.HORIZONTAL;	
	gbc.anchor = gbc.NORTHEAST;
	_myPanel.add( buttonCompoPanel, gbc );

	GridBagConstraints bcpgbc = new GridBagConstraints() ;
        bcpgbc.gridx      = 0;
        bcpgbc.gridy      = 0;
        bcpgbc.gridwidth	 = 1;
        bcpgbc.gridheight = 1;
        bcpgbc.weightx    = 0;
        bcpgbc.weighty    = 0;
        bcpgbc.fill       = bcpgbc.HORIZONTAL;
        bcpgbc.anchor     = bcpgbc.CENTER;
        bcpgbc.insets     = new Insets(0, DIFFERENT_COMPONENT_SPACE,
				    COMPONENT_SPACE, 0);
        bcpgbc.ipadx = 0;
        bcpgbc.ipady = 0;


	_bAddComponent = UIFactory.makeJButton(this, _section, "addcomponent",
					       (ResourceSet)null);
	_bAddComponent.setActionCommand(ADD_COMP);		
	_bAddComponent.setEnabled( true );
	buttonCompoPanel.add( _bAddComponent, bcpgbc);


	_bDelComponent = UIFactory.makeJButton(this, _section, "delcomponent",
					       (ResourceSet)null);
	_bDelComponent.setActionCommand(DELETE_COMP);
	_bDelComponent.setEnabled(false);
	bcpgbc.gridy++;
	buttonCompoPanel.add( _bDelComponent,bcpgbc);

	_isInitialized = true;
    }



    /**
     *  handle incoming event
     *
     * @param e event
     */

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals( _bAddControl )) {
	    String[] nctrl = addControls();
	    if ( nctrl != null ) {
		for (int i = nctrl.length - 1; i>= 0;i--) {					
		    _ctrlData.addElement(nctrl[i]);
		}
		checkCtrlDirty();
	    }			
	} else if (e.getSource().equals( _bDelControl )) {
	    int[] indices = _ctrlList.getSelectedIndices();
	    for (int i = indices.length - 1; i>= 0;i--) {					
		_ctrlData.removeElementAt(indices[i]);
	    }
	    checkCtrlDirty();		
        } else if (e.getSource().equals( _bAddComponent )) {
	    String[] ncomp = addComponents();
	    if ( ncomp != null ) {
		for (int i = ncomp.length - 1; i>= 0;i--) {					
		    _compData.addElement(ncomp[i]);
		}
		checkCompDirty();
	    }	
	} else if (e.getSource().equals( _bDelComponent )) {
	    int[] indices = _compList.getSelectedIndices();
	    for (int i = indices.length - 1; i>= 0;i--) {					
		_compData.removeElementAt(indices[i]);
	    }
	    checkCompDirty();
	}
	super.actionPerformed(e);
    }

    private String[] addControls() {
	ListChooserPanel child = new ListChooserPanel( 
						      _model,
						      _possibleControls,
						      getAllItemInJList( _ctrlList ),
						      _section + "-ctrl" );
	SimpleDialog dlg =
	new SimpleDialog( getModel().getFrame(),
			  child.getTitle(),
			  SimpleDialog.OK |
			  SimpleDialog.CANCEL |
			  SimpleDialog.HELP,
			  child );
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	dlg.setDefaultButton( SimpleDialog.OK );
	dlg.getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString(_section,
																				   "ctrl-chooser-dbox-description"));
	dlg.packAndShow();

	String[] ctrl = (String[])child.getSelectedItem();
	if ( ctrl != null ) {
	    return ( ctrl );
	} else {
	    return ( null );
	}
    }


    private String[] addComponents() {
	ListChooserPanel child = new ListChooserPanel( 
						      _model,
						      _possibleComponent,
						      getAllItemInJList( _compList ),
						      _section + "-comp" );
	SimpleDialog dlg =
	new SimpleDialog( getModel().getFrame(),
			  child.getTitle(),
			  SimpleDialog.OK |
			  SimpleDialog.CANCEL |
			  SimpleDialog.HELP,
			  child );
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	dlg.setDefaultButton( SimpleDialog.OK );
	dlg.getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString(_section,
																				   "ctrl-chooser-dbox-description"));
	dlg.packAndShow();

	String[] comp = (String[])child.getSelectedItem();
	if ( comp != null ) {
	    return ( comp );
	} else {
	    return ( null );
	}
    }

    private void checkCtrlDirty() {
	String[] ctrl = getAllItemInJList( _ctrlList );
	String[] diffMore = MappingUtils.whatsMoreInThisList( _transControls, ctrl);
	String[] diffLess = MappingUtils.whatsMoreInThisList( ctrl , _transControls );
	_isCtrlDirty = ((( diffMore != null) && ( diffMore.length > 0 )) || 
			(( diffLess != null) &&( diffLess.length > 0 )));
	if( _isCtrlDirty ) {
	    setChangeState( _ctrlListLabel, CHANGE_STATE_MODIFIED);
	} else {
	    setChangeState( _ctrlListLabel, CHANGE_STATE_UNMODIFIED );
	}		
	checkDirty();
    }

    private void checkCompDirty() {
	String[] comp  = getAllItemInJList( _compList );
	String[] diffMore = MappingUtils.whatsMoreInThisList( _activeComponent, comp);
	String[] diffLess = MappingUtils.whatsMoreInThisList( comp , _activeComponent );
	_isCompDirty = ((( diffMore != null) && ( diffMore.length > 0 )) || 
			(( diffLess != null) &&( diffLess.length > 0 )));
	if( _isCompDirty ) {
	    setChangeState( _compListLabel, CHANGE_STATE_MODIFIED);
	} else {
	    setChangeState( _compListLabel, CHANGE_STATE_UNMODIFIED );
	}
	checkDirty();
    }

    private void checkDirty() {		
	if( _isCompDirty || _isCtrlDirty ){
	    setDirtyFlag();
	} else {
	    clearDirtyFlag();
	}
    }
	
    private String[] getAllItemInJList( JList list ) {
	Vector v = new Vector();

	for (int i = 0;
	     i < list.getModel().getSize();
	     i++) {
	    v.addElement( list.getModel().getElementAt( i ));
	}
	if( v.size()  == 0 ) {
	    return ( null );
	} else {
	    String nlist[] = new String[ v.size() ];
	    v.toArray( nlist );
	    return ( nlist );
	}
    }
									  
    private void prepData() {
	LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		
	_activeComponent = DSUtil.getAttrStringValueArray( 
							  ldc,				   
							  CONFIG_PREFIX + _dnEntry, 
							  ACTIVE_COMPONENT_ATTR + "=*",
							  ACTIVE_COMPONENT_ATTR );
		
	_possibleComponent = DSUtil.getAttrStringValueArray( 
							    ldc,
							    CONFIG_PREFIX + _dnEntry,
							    POSSIBLE_COMPONENT_ATTR + "=*",
							    POSSIBLE_COMPONENT_ATTR );

	if (( _possibleComponent == null ) ||
	    ( _possibleComponent.length == 0)) {
	    // So we propose the all list of plugin
	    _possibleComponent = DSUtil.getAttrStringValueArray( 
								ldc,
								DSUtil.PLUGIN_CONFIG_BASE_DN,
								"objectclass=nsSlapdPlugin",
								"dn" );
													  
	}

	_transControls = DSUtil.getAttrStringValueArray(
							ldc,
							CONFIG_PREFIX + _dnEntry,
							TRANS_CONTROL_ATTR + "=*",
							TRANS_CONTROL_ATTR);

	_possibleControls =  DSUtil.getAttrStringValueArray ( 
							     ldc,
							     "",
							     POSSIBLE_CTRL_ATTR );
	// there are some controls we know we do not support so
	// just remove them from the list
	Vector tmpControls = new Vector(_possibleControls.length);
	for (int ii = 0; ii < _knownUnsupportedControls.length; ++ii) {
		for (int jj = 0; jj < _possibleControls.length; ++jj) {
			if (!_possibleControls[jj].equals(_knownUnsupportedControls[ii])) {
				tmpControls.addElement(_possibleControls[jj]);
			}
		}
    }
	_possibleControls = new String[tmpControls.size()];
	tmpControls.copyInto(_possibleControls);
	}

    class ControlCellRenderer extends DefaultListCellRenderer {  
	// This is the only method defined by ListCellRenderer.  We just
	// reconfigure the Jlabel each time we're called.
		
	public Component getListCellRendererComponent(
						      JList list,
						      Object value,            // value to display
						      int index,               // cell index
						      boolean isSelected,      // is the cell selected
						      boolean cellHasFocus)    // the list and the cell have the focus
	{
	    if ( list == _ctrlList ) {
		if (list.isSelectionEmpty()) {
		    _bDelControl.setEnabled(false);
		} else {
		    _bDelControl.setEnabled(true);
		}
	    } else if ( list == _compList ) {
		if (list.isSelectionEmpty()) {
		    _bDelComponent.setEnabled(false);
		} else {
		    _bDelComponent.setEnabled(true);
		}
	    }
	    checkOkay();
	    return super.getListCellRendererComponent(list,
						      value,
						      index,
						      isSelected,
						      cellHasFocus);
	}
    }

    /**
     * Enable/disable OK button
     *
     * @param ok true to enable the OK button
     */
    private void setOkay( boolean ok ) {
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( ok );
	} else {
	}
    }
									  

    private void checkOkay() {
	setOkay( _isCtrlDirty || _isCompDirty );	
	modelUpdate();
    }

    private void modelUpdate( ) {
	if (  _isCtrlDirty || _isCompDirty ){
	    setDirtyFlag();
	    setValidFlag();
	}
    }
    public void resetCallback() {
		prepData();

		populateFromServerData();
				
		clearListChanges( _compListLabel, _compData, _activeComponent );
		clearListChanges( _ctrlListLabel, _ctrlData, _transControls );
		_isCompDirty = false;
		_isCtrlDirty = false;
		clearDirtyFlag();
    }


	private void populateFromServerData() {
		_compData.clear();
		for(int i=0;
			( _activeComponent != null) && 
				i < _activeComponent.length; 
			i++ ){
			_compData.addElement( _activeComponent[i]);
		}
		
		_ctrlData.clear();
		for(int i=0;
			( _transControls != null) && 
				i < _transControls.length; 
			i++ ){
			_ctrlData.addElement( _transControls[i]);
		}
	}

  /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
	public boolean refresh () {
		resetCallback();
		return true;
	}		

    private void clearListChanges( JComponent view, 
				   DefaultListModel _listMod, 
				   String[] _saveList ) {		
	for (int i = _listMod.getSize() - 1; i>= 0;i--) {					
	    _listMod.removeElementAt(i);
	}
	for(int i=0;( _saveList != null ) && ( i < _saveList.length ); i++) {
	    Debug.println("ChainingSettingPanel.clearListChanges()  add : " + 
			  _saveList[i] );
	    _listMod.addElement( _saveList[i]);
	}
	setChangeState( view, CHANGE_STATE_UNMODIFIED );
    }


    public void okCallback() {
	if( _isCtrlDirty || _isCompDirty ) {
	    LDAPModificationSet attrs = new LDAPModificationSet();
	    prepSaveCtrl( attrs );
	    prepSaveComponents( attrs );
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try {
		Debug.println(" +++++++++ dn = " + "cn=config," + _dnEntry );
		ldc.modify( CONFIG_PREFIX +  _dnEntry, attrs );
		_activeComponent = getAllItemInJList( _compList );
		_transControls = getAllItemInJList( _ctrlList );
		checkCtrlDirty();
		checkCompDirty();
	    } catch ( LDAPException e) {
		String[] args_m = { _configEntry.getDN(), e.toString() };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"update-error",
					args_m,
					_section );
	    }
	}
    }

    private void prepSaveCtrl( LDAPModificationSet attrs ) {
	String[] CtrlInList = getAllItemInJList( _ctrlList ); 
	Debug.println("ChainingSettingPanel.prepSaveCtrl :");
	String[] newCtrl = MappingUtils.whatsMoreInThisList( _transControls,
							     CtrlInList );
	String[] removeCtrl = MappingUtils.whatsMoreInThisList( CtrlInList,
								_transControls );
	if(( newCtrl != null) &&
	   ( newCtrl.length != 0 )) {
	    // debug
	    for(int i=0;i<newCtrl.length;Debug.println(" ++ newCtrl["+i+"]="+newCtrl[i++]));
	    attrs.add( LDAPModification.ADD,
		       new LDAPAttribute( TRANS_CONTROL_ATTR,
					  newCtrl));
	}

	if(( removeCtrl != null) &&
	   ( removeCtrl.length != 0 )) {
	    attrs.add( LDAPModification.DELETE,
		       new LDAPAttribute(TRANS_CONTROL_ATTR,
					 removeCtrl));
	}
    }

    private void prepSaveComponents( LDAPModificationSet attrs ) {
	String[] compInList = getAllItemInJList( _compList );

	Debug.println("ChainingSettingPanel.prepSaveComponents :");
	String[] newComp = MappingUtils.whatsMoreInThisList( _activeComponent,
							     compInList );
	String[] removeComp = MappingUtils.whatsMoreInThisList( compInList,
								_activeComponent );

	if(( newComp != null ) &&
	   ( newComp.length != 0 )) {
			
	    for ( int i=0;
		  i < newComp.length;
		  i++ ) {
				/*
				  newComp[i] = "cn=" +
				  newComp[i] +
				  "," +
				  DSUtil.PLUGIN_CONFIG_BASE_DN;
				*/
		Debug.println(" add  newComp[" + i + "]=" +
			      newComp[i]);				
	    }
			
	    attrs.add( LDAPModification.ADD,
		       new LDAPAttribute( ACTIVE_COMPONENT_ATTR, newComp));
	}
	if(( removeComp != null ) &&
	   ( removeComp.length != 0 )) {
	    for ( int i=0;
		  i < removeComp.length;
		  i++ ) {
				/*
				  removeComp[i] = "cn=" +
				  removeComp[i] + 
				  "," + 
				  DSUtil.PLUGIN_CONFIG_BASE_DN;
				*/
		Debug.println(" delete  removeComp[" + i + "]=" +
			      removeComp[i]);				
	    }
	    attrs.add( LDAPModification.DELETE,
		       new LDAPAttribute(ACTIVE_COMPONENT_ATTR ,removeComp));

	}
    }


    private IDSModel	_model = null;
    private JButton		_bAddControl;
    private JButton		_bDelControl;
    private JButton		_bAddComponent;
    private JButton		_bDelComponent;

    private JLabel		_compListLabel;
    private JLabel		_ctrlListLabel;

    private DefaultListModel	_ctrlData;
    private JList				_ctrlList;
    private DefaultListModel	_compData;
    private JList				_compList;
									  
    private LDAPEntry	_configEntry = null;
    private String		_dnEntry;
    private String[]	_possibleComponent;
    private String[]	_activeComponent;
    private String[]	_transControls;
    private String[]	_possibleControls;

    private boolean		_isCtrlDirty = false;
    private boolean		_isCompDirty = false;

    private static final String TRANS_CONTROL_ATTR = "nsTransmittedControls";
    private static final String POSSIBLE_CTRL_ATTR = "supportedcontrol" ;
    private static final String ACTIVE_COMPONENT_ATTR = "nsActiveChainingComponents";
    private static final String POSSIBLE_COMPONENT_ATTR = "nsPossibleChainingComponents";
    private static final String CONFIG_PREFIX = "cn=config,";	  

	// richm 600427 - the persistent search control is known to be unsupported
	// by the chaining backend
    private static final String[] _knownUnsupportedControls =
		{"2.16.840.1.113730.3.4.3"};

    private static final String _section = "chaining-settings";
    private final int ROWS = 4;
    //copy from BlankPanel
    final static int DEFAULT_PADDING = 6;
    final static Insets DEFAULT_EMPTY_INSETS = new Insets(0,0,0,0);
    final static Insets BOTTOM_INSETS = new Insets(6,6,6,6);

    static final String DELETE_CTRL = "deleteControl";	
    static final String ADD_CTRL = "addControl";
    static final String DELETE_COMP = "deleteComponent";	
    static final String ADD_COMP = "addComponent";


}
