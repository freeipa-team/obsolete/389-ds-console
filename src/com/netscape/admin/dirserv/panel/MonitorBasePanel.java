/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import com.netscape.management.nmclf.SuiTableHeaderRenderer;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import netscape.ldap.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class MonitorBasePanel extends RefreshablePanel {

	public MonitorBasePanel(IDSModel model, String title) {
		super( model, title, true );
	}


	/* Various methods used by subclasses */

    /**
      * Called when the panel is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
        if (!_isInitialized) { // first time called
            init();
            _isInitialized = true;
		}		
		_isSelected = true;
		if ( (_refreshCheckbox != null) &&
			 _refreshCheckbox.isSelected() ) {
				startUpdater();
		}
	}

    /**
      * Called when the panel is deselected.
      */
    public void unselect(IResourceObject parent, IPage viewInstance) {
		//Debug.println( "MonitorBasePanel: unselect" );
		_isSelected = false;
		stopUpdater();
	}

	/**
	 * This method is overridden in subclasses.
	 */
    public boolean refresh() {
		/* Have the data models notify listeners that they need updating */
		Enumeration en = _tables.elements();
		while( en.hasMoreElements() ) {
			JTable table = (JTable)en.nextElement();
//			((PerfTableModel)table.getModel()).notifyChanges();
			table.repaint();
		}
		return true;
	}

    protected String getAttrValue( String attrName ) {
		return DSUtil.getAttrValue( _entry, attrName );
	}

    protected void setColumnWidths( JTable table, int[] widths ) {
		Enumeration en = table.getColumnModel().getColumns();
		int i = 0;
		while( en.hasMoreElements() ) {
			TableColumn col = (TableColumn)en.nextElement();
			col.setPreferredWidth( widths[i] );			
			col.setResizable( true );
			i++;
		}
	}

    protected void setColumnHeaders( JTable table, String[] headers ) {
		Enumeration en = table.getColumnModel().getColumns();
		int i = 0;
		while( en.hasMoreElements() ) {
			TableColumn col = (TableColumn)en.nextElement();
			col.setHeaderValue( headers[i] );
			i++;
		}
	}

    protected int getTotalColumnWidth( JTable table ) {
		Enumeration en = table.getColumnModel().getColumns();
		int width = 0;
		while( en.hasMoreElements() ) {
			TableColumn col = (TableColumn)en.nextElement();
			width += col.getWidth();
		}
		return width;
	}

    private Dimension getOptimalDimension( JTable table,
										   JComponent container ) {
		/* What is the initial total width of all columns? */
		int width = getTotalColumnWidth( table );
		/* Make the minimum and the current size of the table fit all
		   columns */
        Dimension d =
			new Dimension( width,
						   (table.getRowMargin() + table.getRowHeight()) * table.getRowCount());
		Insets ins = container.getInsets();
		width = container.getWidth() - ins.left - ins.right;
		//Debug.println( "getOptimalDimensionTable: " + d + ", container: " + width );
		d.width = Math.max( d.width, width );
		return d;
	}

    private void setupTable( JTable table,
							 JComponent container ) {
		Dimension d = getOptimalDimension( table, container );
		table.setMinimumSize( d );
		table.setSize( d );
		table.getTableHeader().setReorderingAllowed(false);
		table.setRowSelectionAllowed(false);
		table.setColumnSelectionAllowed(false);
		table.setAutoResizeMode( table.AUTO_RESIZE_NEXT_COLUMN );
		table.setRequestFocusEnabled(false);
		/* If necessary, adjust column widths */
		table.sizeColumnsToFit( true );
	}

	protected void addTable( JTable table, JComponent myContainer,
						   String key ) {
		/* The column headers only show up if I put this in a scrollpane
		   in Swing 0.7 or 1.0... */
//		addTableInGroupPane( table, myContainer, key );
		addTableInScrollPane( table, myContainer, key );
	}

	protected void addTableInScrollPane( JTable table,
										 JComponent container,
										 String titleString ) {
		JScrollPane scrollpane = new JScrollPane( table );		
		JPanel outer = new GroupPanel(titleString);		
        scrollpane.setHorizontalScrollBarPolicy(
			scrollpane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollpane.setVerticalScrollBarPolicy(
			scrollpane.VERTICAL_SCROLLBAR_NEVER);
        _gbc.gridwidth = _gbc.REMAINDER;
        _gbc.fill = GridBagConstraints.BOTH;
//        ((Container)container).add( scrollpane, _gbc );
        outer.add( scrollpane, _gbc );
        _gbc.fill = GridBagConstraints.HORIZONTAL;
        ((Container)container).add( outer, _gbc );
//        ((Container)container).add( scrollpane, _gbc );
		setupTable( table, container );
		Dimension d = getOptimalDimension( table, container );		
        table.setPreferredScrollableViewportSize(new Dimension(10, d.height));
	}



	protected void addTableInGroupPane( JTable table,
									    JComponent container,
										String titleString ) {
		JPanel panel = new GroupPanel( titleString);
		panel.add( table, _gbc );
		setupTable( table, container );
		_gbc.insets = new Insets(10,0,2,2);
        _gbc.gridwidth = _gbc.REMAINDER;
        _gbc.fill = GridBagConstraints.HORIZONTAL;
		((Container)container).add( panel, _gbc );
	}

	protected void rightAlignColumns( JTable table,
					  int first, int last ) {
		for( int i = first; i <= last; i++ ) {
			table.getColumnModel().getColumn(i).setCellRenderer(
				new RightAlignedCellRenderer() );
			SuiTableHeaderRenderer renderer = (SuiTableHeaderRenderer)
				table.getColumnModel().getColumn(i).getHeaderRenderer();
			renderer.setHorizontalAlignment(SwingConstants.RIGHT);
			renderer.setHorizontalTextPosition(SwingConstants.RIGHT);
		}
	}

	class RightAlignedCellRenderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(
			JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
			setHorizontalAlignment(SwingConstants.RIGHT);
			return super.getTableCellRendererComponent(
				table, value, isSelected, hasFocus, row, column );
		}
	}

    /**
	 * Table model to provide data from the cached directory entry.
	 */
    class PerfTableModel extends AbstractTableModel {
	
	PerfTableModel( String[] labels, String[] headers,
			String[] dataNames ) {
	    _labels = labels;
	    _headers = headers;
	    _dataNames = dataNames;
	    _monitorEntry = _entry;
	}

	PerfTableModel( String[] labels, String[] headers,
			String[] dataNames, LDAPEntry monitorEntry ) {
	    _labels = labels;
	    _headers = headers;
	    _dataNames = dataNames;
	    _monitorEntry = monitorEntry;
	}

	public void updEntry( LDAPEntry updEntry ) {
	    updEntry( updEntry, true );
	}


	public void updEntry( LDAPEntry updEntry, boolean notifChanges ) {
	    _monitorEntry = updEntry;
	    if ( notifChanges) fireTableDataChanged();
	}

	public void updDataNames( String[] dataNames, boolean notifChanges ) {
	    _dataNames = dataNames;
	    if( notifChanges) fireTableDataChanged();
	}    

        public void notifyChanges() {
	    fireTableDataChanged();
	}
        public int getColumnCount() {
	    return _headers.length;
	}
        public int getRowCount() {
			return _labels.length;
        }
        public Object getValueAt(int row, int col) {
	    String val;
	    if ( col == 0 ) {
		val = _labels[row];
	    } else {
		val = DSUtil.getAttrValue( _monitorEntry, _dataNames[row] );
		if ( (val == null) || (val.length() < 1) ) {
		    return "";
		}
	    }
	    // Debug.println( " table value " + row + "," +
	    //				col + ": " + val );
	    return val;
	}
        public String getColumnName(int columnIndex) {
	    return _headers[columnIndex];
	}
	
	protected String[] _labels; // Row labels
	protected String[] _headers; // Column labels
	protected String[] _dataNames; // Attributes to provide data 
	protected LDAPEntry _monitorEntry = null;
    }
    
    protected LDAPEntry _entry;
    protected LDAPConnection  _ldc;
    protected String _entryName = "cn=monitor";
	protected Vector _tables = new Vector();
    private boolean _isSelected = false;
    protected static final int PREFERRED_WIDTH = 500;
    
	static final ResourceSet _resource = DSUtil._resource;
}

