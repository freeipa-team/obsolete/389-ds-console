/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import netscape.ldap.*;

public class AttributeDialog extends SimpleDialog {
    AttributeDialog( IDSModel model ) {
        this ( model, null );
    }
	AttributeDialog( IDSModel model, LDAPAttributeSchema las ) {
        super( model.getFrame(), "", OK | CANCEL | HELP, null );
		_panel = new AttributePanel( model, las, this );
		setTitle( _panel.getTitle() );
		getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString(AttributePanel.PANEL_NAME,
																				   "description"));
		setBasePanel( _panel );
		setFocusComponent(_panel.getDefaultFocusComponent());
		pack();		
		validate();		
	}
	LDAPAttributeSchema getAttribute() {
		return _panel.getAttribute();
	}
	public void okInvoked() {
		setBusyCursor(true);
		super.okInvoked();
		setBusyCursor(false);
	}
	AttributePanel _panel;
}

class AttributePanel extends BlankPanel {
    public AttributePanel( IDSModel model, LDAPAttributeSchema las,
						   SimpleDialog dlg ) {
        super(model, PANEL_NAME);

		_las = las;
		_create = ( _las == null );
		_dlg = dlg;

        _myPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.anchor = gbc.NORTHWEST;
        gbc.fill = gbc.BOTH;
        gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = new Insets( 0, 0, 0, 0 );
        JPanel panel = createNewAttributeArea();
		_myPanel.add( panel, gbc );
		gbc.weighty = 1.0;
		_helpToken = "configuration-schema-attr-create-dbox-help";

		checkOkay();
    }

    public void init() {}

	public void okCallback() {
	    String name = _tfAttributeName.getText();		
		String[] aliases = DSSchemaHelper.stringToArray(
						   	_tfAttributeAliases.getText().trim());
		boolean stop = false;
		/* If we are creating the attribute, see if there's already one with the same name */				
		if (_create) {
			if (getModel().getSchema().getAttribute(name) != null) {		
				DSUtil.showErrorDialog( this,
										"exists",
										name,
										PANEL_NAME );
				stop = true;
			}
		
		}
		if (!stop &&
			(aliases != null) &&
			(aliases.length > 0)) {
			/* Check if we want to add an alias with the same name of an attribute (which is not
			   the attribute we are editing).
			   If it is the case, display a dialog asking for confirmation (the existing attribute's
			   definition will be deleted) */							
			for (int i=0; (i < aliases.length) && !stop; i++) {
				if ((getModel().getSchema().getAttribute(aliases[i]) != null) &&
					(!aliases[i].equalsIgnoreCase(name))) {
					int result = DSUtil.showConfirmationDialog( this,
																"exists-alias-attribute",
																aliases[i],
																PANEL_NAME );
					if (result != JOptionPane.YES_OPTION) {
						stop = true;
					}
				}
			}		
		}

		if (!stop) {
			/* Check if we want to add an attribute with the same name of an alias (which is not
			   the attribute we are editing).
			   If it is the case, display a dialog asking for confirmation (the existing attribute's
			   definition will be deleted) */			
			Enumeration attributes = getModel().getSchema().getAttributes();
			while (attributes.hasMoreElements() && !stop) {
				LDAPAttributeSchema currentAttribute = (LDAPAttributeSchema)attributes.nextElement();
				String[] currentAliases = currentAttribute.getAliases();
				if (currentAliases != null) {
					for (int i=0; (i<currentAliases.length) && !stop; i++) {
						if (currentAliases[i].equalsIgnoreCase(name)) {
							String attributeName = currentAttribute.getName();
							if (!attributeName.equalsIgnoreCase(name)) {
								String[] attrs = {attributeName, currentAliases[i]};
								int result = DSUtil.showConfirmationDialog( this,
																			"exists-attribute-alias",
																			attrs,
																			PANEL_NAME );
								if (result != JOptionPane.YES_OPTION) {
									stop = true;
								}
							}
							/* Check the aliases we define with the existing aliases */
						} else if ((aliases != null) &&
								   (aliases.length > 0)) {									
							for (int j=0; (j < aliases.length) && !stop; j++) {
								if (currentAliases[i].equalsIgnoreCase(aliases[j])) {
									String attributeName = currentAttribute.getName();
									if (!attributeName.equalsIgnoreCase(name)) {
										String[] attrs = {attributeName, currentAliases[i]};
										int result = DSUtil.showConfirmationDialog( this,
																					"exists-alias-alias",
																					attrs,
																					PANEL_NAME );
										if (result != JOptionPane.YES_OPTION) {
											stop = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (!stop) {
			/* Create default OID if none is entered */
			String oid = _tfAttributeOID.getText();
			if (oid == null || oid.length() == 0) {
				oid = name + "-oid";
			}
			String desc = _tfAttributeDesc.getText();
			if (desc == null) {
				desc = "User defined attribute";
			}
			boolean multi = _ckbAttributeIsMulti.isSelected();
			
			String selectedSyntax = (String)_cmbAttributeSyntax.getSelectedItem();
			Enumeration keys = _htSyntaxStrings.keys();
			String syntaxOID = null;
			while (keys.hasMoreElements()) {
				syntaxOID = (String)keys.nextElement();
				if (selectedSyntax.equals(_htSyntaxStrings.get(syntaxOID))) {
					break;
				}
			}

			String superior = null;
			LDAPAttributeSchema las =
				new LDAPAttributeSchema(name, oid, desc,
										syntaxOID,
										!multi, superior, aliases);
			las.setQualifier("X-ORIGIN", "user defined");

			// Set the matching rules if any were selected
			String selectedEqMatchingRule = (String)_cmbAttributeMrEquality.getSelectedItem();
			if ((selectedEqMatchingRule != null) && (!selectedEqMatchingRule.equalsIgnoreCase("none"))) {
				las.setQualifier("EQUALITY", selectedEqMatchingRule);
			}

			String selectedOrdMatchingRule = (String)_cmbAttributeMrOrdering.getSelectedItem();
			if ((selectedOrdMatchingRule != null) && (!selectedOrdMatchingRule.equalsIgnoreCase("none"))) {
				las.setQualifier("ORDERING", selectedOrdMatchingRule);
			}

			String selectedSubMatchingRule = (String)_cmbAttributeMrSubstring.getSelectedItem();
			if ((selectedSubMatchingRule != null) && (!selectedSubMatchingRule.equalsIgnoreCase("none"))) {
				las.setQualifier("SUBSTR", selectedSubMatchingRule);
			}

			boolean done = false;
			boolean deleted = false;
			boolean status = false;
			boolean hideDialog = true;
			String msg = null;
			IDSModel model = getModel();
			while (!done) {
				try {
					if ( !_create && !deleted ) {
						try {
							Debug.println( "AttributeDialog.okCallback: " +
										   "deleting " +
										   _las + ", " + _las.getValue() );
							_las.remove(model.getServerInfo().getLDAPConnection());
							deleted = true;
						} catch ( LDAPException e ) {
							Debug.println( "AttributeDialog.okCallback(): " + e );
						}
					}					
					
					Debug.println( "AttributeDialog.okCallback: updating " +
								   las + ", " + las.getValue() );				
					las.add(model.getServerInfo().getLDAPConnection());
					_las = las;
					done = true;
					status = true;
				} catch (LDAPException lde) {
					if ( lde.getLDAPResultCode() != lde.INSUFFICIENT_ACCESS_RIGHTS ) {
						restoreAttr(model, _las);
					}
					Debug.println("AttributeDialog.okCallback(): " +
								  lde + " new attr=" + las.getValue());
					// if the modify failed due to permissions, notify the user
					// via a dialog and ask the user to authenticate using a
					// different user id/password.  If the user selects OK,
					// popup the password dialog asking for another user id and
					// password.  If the user hits OK, update the userid and
					// password in the _ldc object and retry the modify.  Keep
					// looping until: 1. The modify succeeds 2. The user hits
					// Cancel in either the confirm dialog or the password
					// dialog 3. we get some other type of LDAP error
					if (lde.getLDAPResultCode() ==
						lde.INSUFFICIENT_ACCESS_RIGHTS) {
						// display a message
						DSUtil.showPermissionDialog( model.getFrame(),
													 model.getServerInfo().
													 getLDAPConnection() );
						if (!model.getNewAuthentication(false)) {
							done = true; // error, just punt
							status = false;
						}
					} else if ( (lde.getLDAPResultCode() ==
								 lde.ATTRIBUTE_OR_VALUE_EXISTS) ||
								(lde.getLDAPResultCode() ==
								 lde.ENTRY_ALREADY_EXISTS)) {
						done = true;
						status = false;
						hideDialog = false;
						/* 
						   In this case, the exception means that the OID is already in use OR that the attribute name is already in use.
						   We have already a control for the second case (if we are creating the attribute we see if an attribute with the same
						   name exists in the schema), so the problem is that the chosen OID is in use 
						   */					 
						DSUtil.showErrorDialog( model.getFrame(),
												"oidexists",
												las.getID(),
												PANEL_NAME );
						return;
					} else if ( lde.getLDAPResultCode() ==
								lde.OPERATION_ERROR ) {
						done = true;
						status = false;
						hideDialog = false;
						String em = lde.getLDAPErrorMessage();
						if ( em != null && 
							 (em.indexOf("must begin and end with a digit") > 0)) {					  
							DSUtil.showErrorDialog( model.getFrame(),
													"invalid-oid-digit",
													las.getID(),
													PANEL_NAME );
						}
						else if ( em != null &&
								  (em.indexOf("contains an invalid character") > 0)) {					  
							DSUtil.showErrorDialog( model.getFrame(),
													"invalid-oid-char",
													las.getID(),
													PANEL_NAME );
						}
						else {
							DSUtil.showErrorDialog( model.getFrame(),
													"failed-add",
													lde.toString(),
													PANEL_NAME );
						}
					}
					else { // some other LDAP error, bad network connection, etc.				  
						done = true;
						status = false;
						DSUtil.showErrorDialog( model.getFrame(),
												"failed-add",
												lde.toString(),
												PANEL_NAME );
					}
				}
			}		
			model.notifyAuthChangeListeners(); // in case we changed auth above
			if ( done || deleted ) {
				invalidateSchema();
			}
			
			if (status) {
				if ( _create ) {
					msg = "successful-add";
				} else {
					msg = "successful-update";
				}
				LDAPSchema s = getModel().getSchema();
				if (s != null) {
					synchronized (s) {
						// TODO: do we need to replace this call or remove it ? [ elp ]
						//		            s.addAttribute(_las);
					}
				}
				clearDirtyFlag();
				hideDialog();
				name = "'"+DSUtil.abreviateString(name, 20)+"'";
				DSUtil.showInformationDialog( model.getFrame(),
											  msg,
											  name,
											  PANEL_NAME );
			} else if (hideDialog) {
				hideDialog();
			}
		}
	}

    public void resetCallback() {
		_las = null;
        // don't do anything; just dismiss the window
		hideDialog();
    }

    private void checkOkay() {
		/* CHECK THE NAME */
		boolean isNameValid = true;
		boolean isNameModified = false;
		String s = _tfAttributeName.getText();
		/* Is the name correct ? */
		if ((s != null) 
			&& (s.length() > 0) 
			&& isValidSchemaSyntax(s)) {
			/* The name is correct.  See if it has been modified (the made comparison is case 
			 insensitive */
			s = s.trim();
			if ( _las != null ) {
				if (!s.equalsIgnoreCase(_las.getName())) {
					isNameModified = true;
				}
			}
		} else {
			isNameValid = false;
		}

		/* CHECK THE ALIAS.  Note: as this method is called each time we add a char, we don't check
		 if one of the given alias has the same value as an existing attribute */
		boolean isAliasValid = true;
		boolean isAliasModified = false;
		String aliasText = _tfAttributeAliases.getText();
		if (aliasText == null) {
			aliasText = "";
		} else {
			aliasText = aliasText.trim();
		}
		
		if ((aliasText.length() > 0) &&
			(s != null)) {
			String[] aliases = DSSchemaHelper.stringToArray(aliasText);
			for (int i=0; (i < aliases.length) && isAliasValid; i++) {
				/* Check that the (possibly existing) alias is not the same as the name of the attribute */				
				if (aliases[i].equalsIgnoreCase(s)) {
					isAliasValid = false;
				} 
			}
		}
		/* Check if the alias has been modified */
		String originalAliasValue = "";
		if (_las != null) {
			originalAliasValue = DSSchemaHelper.arrayToString(_las.getAliases());
			if (originalAliasValue == null) {
				originalAliasValue = "";
			} else {
				originalAliasValue = originalAliasValue.trim();
			}
			if (!aliasText.equals(originalAliasValue)) {
				isAliasModified = true;
			}
		}		

		/* CHECK THE OID*/
		boolean isOIDValid = true;
		boolean isOIDModified = false;
		String oidText = _tfAttributeOID.getText().trim();
		if (oidText == null) {
			oidText = "";
		}
		/* Check if the oid is valid */
		if ((s!=null) && (oidText.length() > 0)) {
			if (!oidText.equalsIgnoreCase(s+"-oid")) {
				String first =  oidText.substring(0, 1);
				String last =  oidText.substring(oidText.length()-1, oidText.length());
				try {
					int firstInt = Integer.valueOf(first).intValue();
					int lastInt = Integer.valueOf(last).intValue();
					if ((0 > firstInt) ||
						(firstInt > 9) || 
						(0 > lastInt) ||
						(lastInt > 9)) {
						isOIDValid = false;
					}
					if (isOIDValid) {
						for (int i=1; i< oidText.length() -1; i++) {
							String character = oidText.substring(i, i+1);
							if (!character.equals(".")) {
								int value = Integer.valueOf(character).intValue();
								if ((0 > value) ||
									(value > 9)) {
									isOIDValid = false;
								}
							}
						}
					}
				} catch (Exception e) {
					isOIDValid = false;
				}
			}			
		}
		/* Check if the oid has been modified */
		String originalOIDValue = "";
		if (_las != null) {
			originalOIDValue = _las.getID();
			if (originalOIDValue == null) {
				originalOIDValue = "";
			} else {
				originalOIDValue = originalOIDValue.trim();
			}
			if (!oidText.equalsIgnoreCase(originalOIDValue)) {
				isOIDModified = true;
			}
		}

		/* Check DESCRIPTION (always valid, only modification) */		
		boolean isDescriptionModified = false;
		String descriptionText = _tfAttributeDesc.getText().trim();
		if (descriptionText == null) {
			descriptionText = "";
		}
		String originalDescriptionValue = "";
		if (_las != null) {
			originalDescriptionValue = _las.getDescription();
			if (originalDescriptionValue == null) {
				originalDescriptionValue = "";
			} else {
				originalDescriptionValue = originalDescriptionValue.trim();
			}
			if (!descriptionText.equals(originalDescriptionValue)) {			
				isDescriptionModified = true;
			}
		}
		

		/* CHECK SYNTAX (always valid, only modification) */	
		boolean isSyntaxModified = false;
		String selectedSyntax = (String)_cmbAttributeSyntax.getSelectedItem();
		String originalSyntax = null;
		if (_las != null) {
			originalSyntax = (String)_htSyntaxStrings.get(_las.getSyntaxString());
			if ((originalSyntax != null) && (selectedSyntax != null)) {
				if (!originalSyntax.equals(selectedSyntax)) {
					isSyntaxModified = true;
				}
			}
		}

		/* check if the EQUALITY matching rule can be used with the syntax */
		boolean isEqMatchingRuleValid = true;
		boolean isEqMatchingRuleModified = false;
		String selectedEqMatchingRule = (String)_cmbAttributeMrEquality.getSelectedItem();
		String originalEqMatchingRule = null;
		String selectedEqSyntax = null;

		if (_las != null) {
			String [] mr_eq_vals = _las.getQualifier("EQUALITY");
			if ((mr_eq_vals != null) && (mr_eq_vals.length > 0)) {
				originalEqMatchingRule = mr_eq_vals[0];
			}

			if (selectedEqMatchingRule != null) {
				// was the matching rule modified?
				if ((originalEqMatchingRule != null) && !originalEqMatchingRule.equals(selectedEqMatchingRule)) {
					isEqMatchingRuleModified = true;
				}
			}
		}

		// get the name of the syntax used in the matching rule
		if ((selectedEqMatchingRule != null) && (_htMatchingRuleStrings.get(selectedEqMatchingRule) != null)) {
			selectedEqSyntax = (String)_htSyntaxStrings.get(_htMatchingRuleStrings.get(selectedEqMatchingRule));
		}

		// is the matching rule valid?
		if ((selectedEqSyntax != null) && !selectedEqSyntax.equals(selectedSyntax)) {
			isEqMatchingRuleValid = false;
		}

		/* check if the ORDERING matching rule can be used with the syntax */
		boolean isOrdMatchingRuleValid = true;
		boolean isOrdMatchingRuleModified = false;
		String selectedOrdMatchingRule = (String)_cmbAttributeMrOrdering.getSelectedItem();
		String originalOrdMatchingRule = null;
		String selectedOrdSyntax = null;

		if (_las != null) {
			String [] mr_ord_vals = _las.getQualifier("ORDERING");
			if ((mr_ord_vals != null) && (mr_ord_vals.length > 0)) {
				originalOrdMatchingRule = mr_ord_vals[0];
			}

			if (selectedOrdMatchingRule != null) {
				// was the matching rule modified?
				if ((originalOrdMatchingRule != null) && !originalOrdMatchingRule.equals(selectedOrdMatchingRule)) {
					isOrdMatchingRuleModified = true;
				}
			}
		}

		// get the name of the syntax used in the matching rule
		if ((selectedOrdMatchingRule != null) && (_htMatchingRuleStrings.get(selectedOrdMatchingRule) != null)) {
			selectedOrdSyntax = (String)_htSyntaxStrings.get(_htMatchingRuleStrings.get(selectedOrdMatchingRule));
		}

		// is the matching rule valid?
		if ((selectedOrdSyntax != null) && !selectedOrdSyntax.equals(selectedSyntax)) {
			isOrdMatchingRuleValid = false;
		}

		/* check if the SUBSTR matching rule can be used with the syntax */
		boolean isSubMatchingRuleValid = true;
		boolean isSubMatchingRuleModified = false;
		String selectedSubMatchingRule = (String)_cmbAttributeMrSubstring.getSelectedItem();
		String originalSubMatchingRule = null;
		String selectedSubSyntax = null;

		if (_las != null) {
			String [] mr_sub_vals = _las.getQualifier("SUBSTR");
			if ((mr_sub_vals != null) && (mr_sub_vals.length > 0)) {
				originalSubMatchingRule = mr_sub_vals[0];
			}

			if (selectedSubMatchingRule != null) {
				// was the matching rule modified?
				if ((originalSubMatchingRule != null) && !originalSubMatchingRule.equals(selectedSubMatchingRule)) {
					isSubMatchingRuleModified = true;
				}
			}
		}

		// get the name of the syntax used in the matching rule
		if ((selectedSubMatchingRule != null) && (_htMatchingRuleStrings.get(selectedSubMatchingRule) != null)) {
			selectedSubSyntax = (String)_htSyntaxStrings.get(_htMatchingRuleStrings.get(selectedSubMatchingRule));
		}

		// is the matching rule valid?
		if ((selectedSubSyntax != null) && !selectedSubSyntax.equals(selectedSyntax)) {
			isSubMatchingRuleValid = false;
		}

		/* CHECK MULTIVALUED (always valid, only modification) */		
		boolean isMultiValuedModified = false;
		boolean isMulti = _ckbAttributeIsMulti.isSelected();
		if (_las != null) {
			if (isMulti == _las.isSingleValued()) {
				isMultiValuedModified = true;
			}
		}

		if (!isNameValid) {
			setChangeState(_lAttributeName, CHANGE_STATE_ERROR);
		} else if (isNameModified) {
			setChangeState(_lAttributeName, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState(_lAttributeName, CHANGE_STATE_UNMODIFIED);
		}

		if (!isAliasValid) {
			setChangeState(_lAttributeAliases, CHANGE_STATE_ERROR);
		} else if (isAliasModified) {
			setChangeState(_lAttributeAliases, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState(_lAttributeAliases, CHANGE_STATE_UNMODIFIED);
		}

		if (!isOIDValid) {
			setChangeState(_lAttributeOID, CHANGE_STATE_ERROR);
		} else if (isOIDModified) {
			setChangeState(_lAttributeOID, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState(_lAttributeOID, CHANGE_STATE_UNMODIFIED);
		}

		if (isDescriptionModified) {
			setChangeState(_lAttributeDesc, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState(_lAttributeDesc, CHANGE_STATE_UNMODIFIED);		
		}

		if (isSyntaxModified) {
			setChangeState(_lAttributeSyntax, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState(_lAttributeSyntax, CHANGE_STATE_UNMODIFIED);		
		}

		if (!isEqMatchingRuleValid) {
			setChangeState(_lAttributeMrEquality, CHANGE_STATE_ERROR);
		} else if (isEqMatchingRuleModified) {
			setChangeState(_lAttributeMrEquality, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState(_lAttributeMrEquality, CHANGE_STATE_UNMODIFIED);
		}

		if (!isOrdMatchingRuleValid) {
			setChangeState(_lAttributeMrOrdering, CHANGE_STATE_ERROR);
		} else if (isOrdMatchingRuleModified) {
			setChangeState(_lAttributeMrOrdering, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState(_lAttributeMrOrdering, CHANGE_STATE_UNMODIFIED);
		}

		if (!isSubMatchingRuleValid) {
			setChangeState(_lAttributeMrSubstring, CHANGE_STATE_ERROR);
		} else if (isSubMatchingRuleModified) {
			setChangeState(_lAttributeMrSubstring, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState(_lAttributeMrSubstring, CHANGE_STATE_UNMODIFIED);
		}

		if (isMultiValuedModified) {			
			setChangeState(_ckbAttributeIsMulti, CHANGE_STATE_MODIFIED);			
		} else {
			setChangeState(_ckbAttributeIsMulti, CHANGE_STATE_UNMODIFIED);			
		}

		boolean isValid = isNameValid &&
			isAliasValid &&
			isOIDValid && isEqMatchingRuleValid &&
			isOrdMatchingRuleValid && isSubMatchingRuleValid;
		
		_dlg.setOKButtonEnabled( isValid );		
	}

    public void changedUpdate(DocumentEvent e) {
		super.changedUpdate( e );
		checkOkay();
    }
    public void removeUpdate(DocumentEvent e) {
		super.removeUpdate( e );
		checkOkay();
    }
    public void insertUpdate(DocumentEvent e) {
		super.insertUpdate( e );
		checkOkay();
    }

	protected JPanel createNewAttributeArea() {
		/* In _htSyntaxStrings we have as key the OID of the syntax, and as value, the Description of the Syntax */
                if ( _htSyntaxStrings == null ) {
                        _htSyntaxStrings = SchemaAttributesPanel.getSyntaxStrings(getModel().getSchema());
                }

                // In _htMatchingRuleStrings we have as key the name of the MR, and as value, the AVA syntax OID
                if ( _htMatchingRuleStrings == null ) {
                        _htMatchingRuleStrings = SchemaAttributesPanel.getMatchingRuleStrings(getModel().getSchema());
                }

		// create the widgets
		_lAttributeName = makeJLabel(PANEL_NAME, "newname");
		_tfAttributeName = makeJTextField(PANEL_NAME, "newname",
	                                      null, 20);
		_lAttributeName.setLabelFor(_tfAttributeName);

		_lAttributeAliases = makeJLabel(PANEL_NAME, "newaliases");
		_tfAttributeAliases = makeJTextField(PANEL_NAME, "newaliases",
											 null, 20);
		_lAttributeAliases.setLabelFor(_tfAttributeAliases);

		_lAttributeOID = makeJLabel(PANEL_NAME, "newoid");
		_tfAttributeOID = makeJTextField(PANEL_NAME, "newoid",
	                                     null, 20);
		_lAttributeOID.setLabelFor(_tfAttributeOID);

		_lAttributeDesc = makeJLabel(PANEL_NAME, "newdesc");
		_tfAttributeDesc = makeJTextField(PANEL_NAME, "newdesc",
	                                      null, 20);
		_lAttributeDesc.setLabelFor(_tfAttributeDesc);

		_ckbAttributeIsMulti = makeJCheckBox(PANEL_NAME, "newmulti",
	                                         false);
		// Value by default
		_ckbAttributeIsMulti.setSelected(true);
		_ckbAttributeIsMulti.addActionListener(this);

		_lAttributeSyntax = makeJLabel(PANEL_NAME, "newsyntax");
		_cmbAttributeSyntax = makeJComboBox(PANEL_NAME, "newsyntax",
	                                        null);
		_lAttributeSyntax.setLabelFor(_cmbAttributeSyntax);
		_cmbAttributeSyntax.addItemListener(this);

		/* We construct a combo box */
		_cmbAttributeSyntax = UIFactory.makeJComboBox((Object)this, null);
		UIFactory.setToolTip(PANEL_NAME, "newsyntax", _cmbAttributeSyntax);
		/* All text fields are supposed to have a 6 pixel inset */
		ComboBoxEditor ed = _cmbAttributeSyntax.getEditor();
		if ( ed != null ) {
			Component c = ed.getEditorComponent();
			if ( c instanceof JTextField )
				((JTextField)c).setMargin( getTextInsets() );
		}

		// Equality matching rules
		_lAttributeMrEquality = makeJLabel(PANEL_NAME, "newmrequality");
		_cmbAttributeMrEquality = makeJComboBox(PANEL_NAME, "newmrequality",
							null);
		_lAttributeMrEquality.setLabelFor(_cmbAttributeMrEquality);
		_cmbAttributeMrEquality.addItemListener(this);
		UIFactory.setToolTip(PANEL_NAME, "newmrequality", _cmbAttributeMrEquality);
		ed = _cmbAttributeMrEquality.getEditor();
		if ( ed != null ) {
			Component c = ed.getEditorComponent();
			if ( c instanceof JTextField )
				((JTextField)c).setMargin( getTextInsets() );
		}

		// Ordering matching rules
		_lAttributeMrOrdering = makeJLabel(PANEL_NAME, "newmrordering");
		_cmbAttributeMrOrdering = makeJComboBox(PANEL_NAME, "newmrordering",
						null);
		_lAttributeMrOrdering.setLabelFor(_cmbAttributeMrOrdering);
		_cmbAttributeMrOrdering.addItemListener(this);


		// Substring matching rules
		_lAttributeMrSubstring = makeJLabel(PANEL_NAME, "newmrsubstring");
		_cmbAttributeMrSubstring = makeJComboBox(PANEL_NAME, "newmrsubstring",
						null);
		_lAttributeMrSubstring.setLabelFor(_cmbAttributeMrSubstring);
		_cmbAttributeMrSubstring.addItemListener(this);

		/* We add the different syntaxes to the combobox */
		Enumeration syntaxes = _htSyntaxStrings.elements();
		while (syntaxes.hasMoreElements()) {
			String item = (String)syntaxes.nextElement();
			if (item != null) {
				_cmbAttributeSyntax.addItem(item);
			}
		}

		// Fill in the combo boxes with a sorted list of matching rules
		_cmbAttributeMrEquality.insertItemAt("none", 0);
		_cmbAttributeMrOrdering.insertItemAt("none", 0);
		_cmbAttributeMrSubstring.insertItemAt("none", 0);
		Vector vMatchingRules = new Vector (_htMatchingRuleStrings.keySet());
		Collections.sort(vMatchingRules);
		Enumeration matchingrules = vMatchingRules.elements();
		while (matchingrules.hasMoreElements()) {
			String item = (String)matchingrules.nextElement();
			// Don't add the matching rule if it's syntax isn't in the hashtable of
			// syntax strings.  This is to avoid including matching rules that use the
			// substring assertion syntax, which we don't support.
			if ((item != null) && (_htSyntaxStrings.get(_htMatchingRuleStrings.get(item)) != null)) {
				_cmbAttributeMrEquality.addItem(item);
				_cmbAttributeMrOrdering.addItem(item);
				_cmbAttributeMrSubstring.addItem(item);
			}
		}

		// layout the widgets
		// create a panel to hold things
		JPanel panel = new JPanel(new GridBagLayout());
		panel.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );
		GridBagConstraints gbc = new GridBagConstraints();	
		int different = UIFactory.getDifferentSpace();
		Insets left = new Insets(0,0,different,0);
		Insets right = new Insets(0,0,different,0);
		
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHEAST;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.insets = left;
		gbc.weightx= 0.0;
		panel.add(_lAttributeName, gbc);
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.WEST;
		gbc.weightx = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = right;
		panel.add(_tfAttributeName, gbc);

		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHEAST;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx = 0;
		gbc.insets = left;
		panel.add(_lAttributeOID, gbc);
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.WEST;
		gbc.weightx = 1;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = right;		
		panel.add(_tfAttributeOID, gbc);

		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHEAST;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx = 0;
		gbc.insets = left;
		panel.add(_lAttributeAliases, gbc);
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.WEST;
		gbc.weightx = 1;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = right;
		panel.add(_tfAttributeAliases, gbc);

		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHEAST;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx = 0;
		gbc.insets = left;
		panel.add(_lAttributeDesc, gbc);
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.WEST;
		gbc.weightx = 1;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = right;
		panel.add(_tfAttributeDesc, gbc);

		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHEAST;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx = 0;
		gbc.insets = left;
		panel.add(_lAttributeSyntax, gbc);
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.WEST;
		gbc.weightx = 1;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = right;
		panel.add(_cmbAttributeSyntax, gbc);

		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHEAST;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx = 0;
		gbc.insets = left;
		panel.add(_lAttributeMrEquality, gbc);
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.WEST;
		gbc.weightx = 1;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = right;
		panel.add(_cmbAttributeMrEquality, gbc);

		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHEAST;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx = 0;
		gbc.insets = left;
		panel.add(_lAttributeMrOrdering, gbc);
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.WEST;
		gbc.weightx = 1;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = right;
		panel.add(_cmbAttributeMrOrdering, gbc);

		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHEAST;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx = 0;
		gbc.insets = left;
		panel.add(_lAttributeMrSubstring, gbc);
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.WEST;
		gbc.weightx = 1;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = right;
		panel.add(_cmbAttributeMrSubstring, gbc);

		gbc.fill = gbc.NONE;
		gbc.weightx = 0.0;
		gbc.anchor = gbc.NORTHEAST;
		gbc.gridwidth = gbc.RELATIVE;
		panel.add(Box.createGlue(), gbc);
		gbc.fill = gbc.HORIZONTAL;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;		
		gbc.anchor = gbc.WEST;
		gbc.insets = left;
		gbc.insets.bottom = 0;
		panel.add(_ckbAttributeIsMulti, gbc);

		if ( _las != null ) {
		    _tfAttributeName.setText( _las.getName() );
		    _tfAttributeOID.setText( _las.getID() );
			_tfAttributeAliases.setText( DSSchemaHelper.arrayToString(
											_las.getAliases()) );
		    _tfAttributeDesc.setText( _las.getDescription() );
			
			String syntax = (String)_htSyntaxStrings.get(_las.getSyntaxString());
			if (syntax != null) {
				_cmbAttributeSyntax.setSelectedItem( syntax );
			}

			String [] mr_eq_vals = _las.getQualifier("EQUALITY");
			if ((mr_eq_vals != null) && (mr_eq_vals.length > 0)) {
				String mr_eq = mr_eq_vals[0];
				if (mr_eq != null) {
					_cmbAttributeMrEquality.setSelectedItem( mr_eq );
				} else {
					_cmbAttributeMrEquality.setSelectedIndex(0);
				}
			} else {
				_cmbAttributeMrEquality.setSelectedIndex(0);
			}

			String [] mr_ord_vals = _las.getQualifier("ORDERING");
			if ((mr_ord_vals != null) && (mr_ord_vals.length > 0)) {
				String mr_ord = mr_ord_vals[0];
				if (mr_ord != null) {
					_cmbAttributeMrOrdering.setSelectedItem( mr_ord );
				} else {
					_cmbAttributeMrOrdering.setSelectedIndex(0);
				}
			} else {
				_cmbAttributeMrOrdering.setSelectedIndex(0);
			}

			String [] mr_sub_vals = _las.getQualifier("SUBSTR");
			if ((mr_sub_vals != null) && (mr_sub_vals.length > 0)) {
				String mr_sub = mr_sub_vals[0];
				if (mr_sub != null) {
					_cmbAttributeMrSubstring.setSelectedItem( mr_sub );
				} else {
					_cmbAttributeMrSubstring.setSelectedIndex(0);
				}
			} else {
				_cmbAttributeMrSubstring.setSelectedIndex(0);
			}

			_ckbAttributeIsMulti.setSelected( !_las.isSingleValued() );
		} else {
			/* Give a default value */
			_tfAttributeName.setText(DSUtil._resource.getString(PANEL_NAME, "newname-default"));

			/* Select none for all matching rules */
			_cmbAttributeMrEquality.setSelectedIndex(0);
			_cmbAttributeMrOrdering.setSelectedIndex(0);
			_cmbAttributeMrSubstring.setSelectedIndex(0);
		}
		return panel;
	}

	private boolean attributeExists(String attrName) {
	    LDAPAttributeSchema las = null;
	    LDAPSchema s = getModel().getSchema();
		if (s != null) {
		    synchronized (s) {
		        las = s.getAttribute(attrName);
		    }
		}
	    return (las != null);
	}

    private void invalidateSchema() {
		/* Force a refetch on next use of schema */
		getModel().setSchema( null );
	}

	/* restoreAttr: add the attribute back to the schmema */
	private void restoreAttr( IDSModel model, LDAPAttributeSchema a ) {
		if ( a == null ) {
			return;
		}
		try {
			a.add ( model.getServerInfo().getLDAPConnection() );
		} catch ( LDAPException lde ) {
			DSUtil.showErrorDialog( model.getFrame(),"failed-add",
									lde.toString(), PANEL_NAME );
		}
	}	

    LDAPAttributeSchema getAttribute() {
		return _las;
	}

	public JComponent getDefaultFocusComponent() {
		return (JComponent)_tfAttributeName;
	}

	/**
     * Handle incoming event. The derived classes do most of the work.
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(_ckbAttributeIsMulti)) {
			checkOkay();
		}
	}

	/**
	 * Some list component changed
	 *
	 * @param e Event indicating what changed
	 */
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource().equals(_cmbAttributeSyntax) ||
		    e.getSource().equals(_cmbAttributeMrEquality) ||
		    e.getSource().equals(_cmbAttributeMrOrdering) ||
		    e.getSource().equals(_cmbAttributeMrSubstring)) {
			checkOkay();
		}
	}


	private JLabel _lAttributeAliases;
	private JLabel _lAttributeOID;
	private JLabel _lAttributeName;
	private JLabel _lAttributeDesc;
	private JLabel _lAttributeSyntax;
	private JLabel _lAttributeMrEquality;
	private JLabel _lAttributeMrOrdering;
	private JLabel _lAttributeMrSubstring;
	private JTextField _tfAttributeName; // new attribute name
	private JTextField _tfAttributeAliases; // aliases
	private JTextField _tfAttributeOID; // oid for new attribute (optional)
	private JTextField _tfAttributeDesc; // attribute description (optional)
	private JCheckBox _ckbAttributeIsMulti; // true if attr is multivalued
	private JComboBox _cmbAttributeSyntax; // syntax for new attribute
	private JComboBox _cmbAttributeMrEquality; //equality matching rule
	private JComboBox _cmbAttributeMrOrdering; //ordering matching rule
	private JComboBox _cmbAttributeMrSubstring; //substring matching rule
	private LDAPAttributeSchema _las = null;
	private boolean _create = false;
	private SimpleDialog _dlg;
	private Hashtable _htSyntaxStrings = null;
	private Hashtable _htMatchingRuleStrings = null;

	static final public String PANEL_NAME = "schemaattributes"; // name of panel in resource file
}
