/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import netscape.ldap.LDAPUrl;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.management.nmclf.SuiConstants;

/**
 * Panel for Create a new chaining database for Dir Server
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	06/29/00
 * @see     com.netscape.admin.dirserv.panel
 */
public class NewChainingInstancePanel extends BlankPanel  
    implements SuiConstants {

    public NewChainingInstancePanel(IDSModel model) {
	super( model, _section );
	_helpToken = "configuration-new-chaining-instance-dbox-help";
	_model = model;
    }

    public void init() {

	_myPanel.setLayout( new GridBagLayout() );
	GridBagConstraints gbc = getGBC();

	// Physical DB info
	createChainArea( _myPanel );
		
	// Mapping Tree info
	createMappingArea( _myPanel );

	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( false );
	}

	_isInitialized = true;

    }

    protected void createChainArea( JPanel grid ) {

	GridBagConstraints ugbc = getGBC(); 
	JPanel HostPanel = new GroupPanel(
					  DSUtil._resource.getString( _section, "new-chaining-title" ), true);

        ugbc.gridwidth = ugbc.REMAINDER;
	ugbc.fill = ugbc.HORIZONTAL;		
	grid.add( HostPanel, ugbc );

	GridBagLayout HLbag = new GridBagLayout();
	GridBagConstraints HLgbc = new GridBagConstraints();

        HLgbc.gridx      = 0;
        HLgbc.gridy      = 0;
	// HLgbc.gridwidth  = HLgbc.REMAINDER;
        HLgbc.gridwidth	 = 1;
        // HLgbc.gridheight = 1;
        HLgbc.weightx    = 0;
        HLgbc.weighty    = 0;
        HLgbc.fill       = HLgbc.BOTH;
        HLgbc.anchor     = HLgbc.CENTER;
        HLgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
				      0,DEFAULT_PADDING);
        HLgbc.ipadx = 0;
        HLgbc.ipady = 0;

	HostPanel.setLayout(HLbag);


	_instanceNameLabel = makeJLabel( _section, "instance-name" );
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.weightx	= 0;
	HLgbc.gridx		= 0;
	HLgbc.anchor	= HLgbc.EAST;
	HostPanel.add( _instanceNameLabel, HLgbc );
		  
	_instanceNameText = makeJTextField( _section, "instance-name" );
	HLgbc.weightx	= 1;
	HLgbc.gridx		= 1;
	HLgbc.gridwidth = 1;
	HLgbc.anchor	= HLgbc.CENTER;
	HLgbc.fill		= HLgbc.HORIZONTAL;
	_instanceNameText.setColumns( 10 );
	_instanceNameText.setText( "New Chaining" );
	HostPanel.add( _instanceNameText, HLgbc );

	_authMechLabel = makeJLabel( _section, "authmech" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth = 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.weightx	= 0;
	HLgbc.anchor	= HLgbc.EAST;
	HostPanel.add( _authMechLabel, HLgbc );
		  
	_bindDNLabel =  makeJLabel( _section, "bind-DN" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth = 1;
	HLgbc.fill = HLgbc.NONE;
	HLgbc.weightx = 0;
	HLgbc.anchor	= HLgbc.EAST;
	HostPanel.add( _bindDNLabel, HLgbc );
		
	_bindDNText = makeJTextField( _section, "bind-DN" );
	HLgbc.gridx		= 1;
	HLgbc.fill = HLgbc.HORIZONTAL;
	HLgbc.gridwidth = HLgbc.REMAINDER;
	HLgbc.weightx = 1;
	HostPanel.add( _bindDNText, HLgbc );
		
		
	_bindPasswdLabel =  makeJLabel( _section, "bind-Passwd" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth = 1;
	HLgbc.fill = HLgbc.NONE;
	HLgbc.weightx = 0;
	HLgbc.anchor	= HLgbc.EAST;
	HostPanel.add( _bindPasswdLabel, HLgbc );
		
	_bindPasswdText = makeJPasswordField(20 );
	HLgbc.gridx		= 1;
	HLgbc.fill = HLgbc.HORIZONTAL;
	HLgbc.gridwidth = 1;
	HLgbc.weightx = 1;
	HostPanel.add( _bindPasswdText, HLgbc );
	/*
	  _cbUrlLdapSecureMode = makeJCheckBox( _section, "ldap-url-secu" );
	  HLgbc.gridy++;
	  HLgbc.gridx		= 0;
	  HLgbc.anchor	= HLgbc.WEST;
	  HLgbc.fill		= HLgbc.NONE;
	  HLgbc.gridwidth	= HLgbc.REMAINDER;
	  HLgbc.weightx	= 0;
	  _cbUrlLdapSecureMode.setSelected( false );
	  HostPanel.add( _cbUrlLdapSecureMode, HLgbc );		
		
	  _urlLdapHostLabel = makeJLabel( _section, "ldap-url-host" );
	  HLgbc.gridy++;
	  HLgbc.gridx		= 0;
	  HLgbc.gridwidth	= 1;
	  HLgbc.fill		= HLgbc.NONE;
	  HLgbc.anchor	= HLgbc.EAST;
	  HLgbc.weightx	= 0;
	  HostPanel.add( _urlLdapHostLabel, HLgbc );
		
	  _urlLdapHostText = makeJTextField( _section, "ldap-url-host" );
	  HLgbc.fill		= HLgbc.HORIZONTAL;
	  HLgbc.gridx		= 1;
	  HLgbc.gridwidth	= 1;
	  HLgbc.weightx	= 1;
	  _urlLdapHostText.setColumns( 12 );
	  HostPanel.add( _urlLdapHostText, HLgbc );		

	  _urlLdapPortLabel = makeJLabel( _section, "ldap-url-port" );
	  HLgbc.gridx		= 2;
	  HLgbc.gridwidth	= 1;
	  HLgbc.fill		= HLgbc.NONE;
	  HLgbc.anchor	= HLgbc.EAST;
	  HLgbc.weightx	= 0;
	  HostPanel.add( _urlLdapPortLabel, HLgbc );
		
	  _urlLdapPortText = makeJTextField( _section, "ldap-url-port" );
	  HLgbc.fill		= HLgbc.HORIZONTAL;
	  HLgbc.gridx		= 3;
	  HLgbc.gridwidth	= 1;
	  HLgbc.weightx	= 0; // doesn't need to extend when screen is resized
	  _urlLdapPortText.setColumns( 5 );
	  _urlLdapPortText.setText( _sDefaultPort );
	  HostPanel.add( _urlLdapPortText, HLgbc );
	*/
	createRemoteServers( HostPanel, HLgbc );

	createURLArea( HostPanel, HLgbc );
	_bHostModified = false;
	_bAlterModified = false;


    }

    private void createURLArea( JPanel grid, GridBagConstraints ugbc ) {

	JPanel UrlPanel = new GroupPanel(
					 DSUtil._resource.getString( _section,
								     "new-url-title" ), 
					 true);

	ugbc.gridx		= 0;
	ugbc.gridy++;
        ugbc.gridwidth = ugbc.REMAINDER;
	ugbc.fill = ugbc.HORIZONTAL;		
	grid.add( UrlPanel, ugbc );

	GridBagLayout ubag = new GridBagLayout();
	GridBagConstraints urlgbc = new GridBagConstraints();

        urlgbc.gridx      = 0;
        urlgbc.gridy      = 0;
	urlgbc.gridwidth  = ugbc.REMAINDER;
        // urlgbc.gridwidth	 = 1;
        // urlgbc.gridheight = 1;
        urlgbc.weightx    = 0;
        urlgbc.weighty    = 0;
        urlgbc.fill       = urlgbc.BOTH;
        urlgbc.anchor     = urlgbc.CENTER;
        urlgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
				       0,DEFAULT_PADDING);
        urlgbc.ipadx = 0;
        urlgbc.ipady = 0;

	UrlPanel.setLayout(ubag);


	_urlValueLabel = makeJLabel( _section, "ldap-url-value" );
	urlgbc.gridx		= 0;
	urlgbc.gridwidth	= 1;
	urlgbc.fill		= urlgbc.NONE;
	urlgbc.anchor	= urlgbc.EAST;
	urlgbc.insets     = new Insets(0 ,0,
				       0, DEFAULT_PADDING);
	urlgbc.weightx	= 0;
	UrlPanel.add( _urlValueLabel, urlgbc );

		
	urlgbc.gridx		= 1;
	urlgbc.gridwidth	= urlgbc.REMAINDER;
	urlgbc.fill			= urlgbc.HORIZONTAL;
	urlgbc.anchor		= urlgbc.WEST;
	urlgbc.weightx		= 1;
	urlgbc.insets	    = new Insets(0, DEFAULT_PADDING, 0, 0);
	_urlValue = new JLabel();
	_sURL = new StringBuffer( "ldap://" );
	_urlValue.setText( _sURL.toString() );
	UrlPanel.add( _urlValue, urlgbc );

		

    }


    private void createRemoteServers( JPanel grid, GridBagConstraints ugbc ) {

	JPanel AlternatePanel = new GroupPanel(
					       DSUtil._resource.getString( _section,
									   "new-alternate-host-title" ), 
					       true);

	ugbc.gridx		= 0;
	ugbc.gridy++;
        ugbc.gridwidth = ugbc.REMAINDER;
	ugbc.fill = ugbc.HORIZONTAL;		
	grid.add( AlternatePanel, ugbc );

	GridBagLayout HLbag = new GridBagLayout();
	GridBagConstraints HLgbc = new GridBagConstraints();

        HLgbc.gridx      = 0;
        HLgbc.gridy      = 0;
	// HLgbc.gridwidth  = HLgbc.REMAINDER;
        HLgbc.gridwidth	 = 1;
        // HLgbc.gridheight = 1;
        HLgbc.weightx    = 0;
        HLgbc.weighty    = 0;
        HLgbc.fill       = HLgbc.BOTH;
        HLgbc.anchor     = HLgbc.CENTER;
	/*
	  HLgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
	  0,DEFAULT_PADDING);
	*/
	HLgbc.insets	= new Insets(0,
				     DIFFERENT_COMPONENT_SPACE,
				     COMPONENT_SPACE,
				     0);
        HLgbc.ipadx = 0;
        HLgbc.ipady = 0;

	AlternatePanel.setLayout(HLbag);

	//
	_cbUrlLdapSecureMode = makeJCheckBox( _section, "ldap-url-secu" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.anchor	= HLgbc.WEST;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.gridwidth	= HLgbc.REMAINDER;
	HLgbc.weightx	= 0;
	_cbUrlLdapSecureMode.setSelected( false );
	AlternatePanel.add( _cbUrlLdapSecureMode, HLgbc );		

	_urlLdapHostLabel = makeJLabel( _section, "ldap-url-host" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth	= 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.anchor	= HLgbc.EAST;
	HLgbc.insets	= new Insets(0, 0, COMPONENT_SPACE, 0);
	HLgbc.weightx	= 0;
	AlternatePanel.add( _urlLdapHostLabel, HLgbc );
		
	_urlLdapHostText = makeJTextField( _section, "ldap-url-host" );
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 1;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 1;
	HLgbc.insets	= new Insets(0, DEFAULT_PADDING, 
				     COMPONENT_SPACE,
				     DIFFERENT_COMPONENT_SPACE);
	_urlLdapHostText.setColumns( 12 );
	AlternatePanel.add( _urlLdapHostText, HLgbc );		

	_urlLdapPortLabel = makeJLabel( _section, "ldap-url-port" );
	HLgbc.gridx		= 2;
	HLgbc.gridwidth	= 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.anchor	= HLgbc.EAST;
	HLgbc.weightx	= 0;
	HLgbc.insets	= new Insets(0, 0, 
				     COMPONENT_SPACE, 
				     0);
	AlternatePanel.add( _urlLdapPortLabel, HLgbc );
		
	_urlLdapPortText = makeJTextField( _section, "ldap-url-port" );
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 3;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 0; // doesn't need to extend when screen is resized
	_urlLdapPortText.setColumns( 5 );
	_urlLdapPortText.setText( _sDefaultPort );
	HLgbc.insets	= new Insets(0, DEFAULT_PADDING,
				     COMPONENT_SPACE, 0);
	AlternatePanel.add( _urlLdapPortText, HLgbc );
	//

	_urlAltHostLabel = makeJLabel( _section, "alt-url-host" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth	= 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.anchor	= HLgbc.EAST;
	HLgbc.insets	= new Insets(0, 0, COMPONENT_SPACE, 0);
	HLgbc.weightx	= 0;
	AlternatePanel.add( _urlAltHostLabel, HLgbc );
		
	_urlAltHostText = makeJTextField( _section, "alt-url-host" );
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 1;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 1;
	HLgbc.insets	= new Insets(0, DEFAULT_PADDING, 
				     COMPONENT_SPACE,
				     DIFFERENT_COMPONENT_SPACE);
	_urlAltHostText.setColumns( 12 );
	AlternatePanel.add( _urlAltHostText, HLgbc );		

	_urlAltPortLabel = makeJLabel( _section, "alt-url-port" );
	HLgbc.gridx		= 2;
	HLgbc.gridwidth	= 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.anchor	= HLgbc.EAST;
	HLgbc.weightx	= 0;
	HLgbc.insets	= new Insets(0, 0, 
				     COMPONENT_SPACE, 
				     0);
	AlternatePanel.add( _urlAltPortLabel, HLgbc );
		
	_urlAltPortText = makeJTextField( _section, "alt-url-port" );
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 3;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 0; // doesn't need to extend when screen is resized
	HLgbc.insets	= new Insets(0, DEFAULT_PADDING,
				     COMPONENT_SPACE, 0);
	_urlAltPortText.setColumns( 5 );
	if ( ! _bAlterModified ) {
	    _urlAltPortText.setText( _sDefaultPort );
	}
	AlternatePanel.add( _urlAltPortText, HLgbc );

	_bAddAlternateServer = makeJButton( _section, "ldap-add-alternate" );
	_bAddAlternateServer.setActionCommand(ADD_ALT);
	_bAddAlternateServer.setEnabled(false);
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 4;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 0;
	HLgbc.insets	= new Insets(0,
				     DIFFERENT_COMPONENT_SPACE,
				     COMPONENT_SPACE,
				     0);
	AlternatePanel.add( _bAddAlternateServer, HLgbc );


	_urlAltData = new DefaultListModel();
	_urlAltList = new JList( _urlAltData );
	_urlAltList.setVisibleRowCount( ROWS );
	_urlAltList.setCellRenderer( new AlternateCellRenderer() );
	JScrollPane scrollAlt = new JScrollPane();
	scrollAlt.getViewport().setView( _urlAltList );
	HLgbc.gridwidth  = 4;
	HLgbc.gridx = 0;
	HLgbc.gridy++;
	HLgbc.insets = new Insets(0, 0, 0, 0);
	HLgbc.fill= HLgbc.BOTH;		
        HLgbc.weightx    = 1;
        HLgbc.weighty    = 1;
	AlternatePanel.add( scrollAlt, HLgbc);	

	_bDelAlternateServer = makeJButton( _section, "ldap-del-alternate" );
	_bDelAlternateServer.setActionCommand(DELETE_ALT);
	_bDelAlternateServer.setEnabled(false);
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 4;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 0;
	HLgbc.insets	= new Insets(0,
				     DIFFERENT_COMPONENT_SPACE,
				     COMPONENT_SPACE,
				     0);
	AlternatePanel.add( _bDelAlternateServer, HLgbc );

    }


    protected void createMappingArea( JPanel grid ) {
        GridBagConstraints gbc = getGBC();
	gbc.insets.bottom = _gbc.insets.top;
        gbc.gridwidth = gbc.REMAINDER;
		
	String[] mapList = MappingUtils.getMappingNode( 
						       _model.getServerInfo().getLDAPConnection() );

	JPanel Mapanel = new GroupPanel(
					DSUtil._resource.getString( _section, "new-mapping-title" ));
        gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
        grid.add( Mapanel, gbc );

    	ButtonGroup group = new ButtonGroup();
        _rbExistingNode = makeJRadioButton( _section, "existing-node", true );
    	group.add(_rbExistingNode);
	gbc.fill = gbc.NONE;
	gbc.gridwidth = 3;
        Mapanel.add( _rbExistingNode, gbc );
        _rbNewNode = makeJRadioButton( _section, "new-node", false );
    	group.add(_rbNewNode);
        Mapanel.add( _rbNewNode, gbc );
	_rbNoNode =  makeJRadioButton( _section, "no-node", false );
    	group.add(_rbNoNode);
	Mapanel.add( _rbNoNode, gbc );

	_rbNewNode.setSelected( true );

	gbc.fill = gbc.HORIZONTAL;
        gbc.gridwidth = gbc.REMAINDER;
        Mapanel.add( Box.createGlue(), gbc);

	_comboMapping = new JComboBox();
        _comboMapping.addItemListener(this);
        _comboMapping.addActionListener(this);

	if ( mapList != null ) {
	    for( int i = 0; i < mapList.length; i++ )
                _comboMapping.addItem( mapList[i] );
	    if ( mapList.length > 0 )
		_comboMapping.setSelectedIndex( 0 );
	}
        String ttip = DSUtil._resource.getString(_section, "select-Mapping-ttip");
        if (ttip != null) {
            _comboMapping.setToolTipText(ttip);
        }		

	_comboLabel = makeJLabel( _section, "mapping-node" );
	gbc.gridwidth = 3; // 2
	gbc.fill = gbc.NONE;
	gbc.weightx = 0.0;
        Mapanel.add( _comboLabel, gbc );
        gbc.gridwidth = gbc.REMAINDER;
	gbc.weightx    = 1.0;
	gbc.fill = gbc.HORIZONTAL;
        Mapanel.add( _comboMapping, gbc );

	_NewNodeLabel = makeJLabel( _section, "node-suffix" );
	gbc.gridwidth = 3;
	gbc.fill = gbc.NONE;
	gbc.weightx = 0.0;
        Mapanel.add( _NewNodeLabel, gbc );

        _NewNodeText = makeJTextField( _section, "node-suffix" );
	gbc.weightx = 1.0;
	gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
        Mapanel.add( _NewNodeText, gbc );		

	_comboNewNodeMapping = new JComboBox();
        _comboNewNodeMapping.addItemListener(this);
        _comboNewNodeMapping.addActionListener(this);

	if ( mapList != null ) {
	    _comboNewNodeMapping.addItem( ROOT_MAPPING_NODE );
	    for( int i = 0; i < mapList.length; i++ )
                _comboNewNodeMapping.addItem( mapList[i] );
	    if ( mapList.length > 0 )
		_comboNewNodeMapping.setSelectedIndex( 0 );
	}

	_comboNewNodeLabel = makeJLabel( _section, "new-node-subordination" );
	gbc.gridwidth = 3; // 2
	gbc.fill = gbc.NONE;
	gbc.weightx = 0.0;
        Mapanel.add( _comboNewNodeLabel, gbc );
        gbc.gridwidth = gbc.REMAINDER;
	gbc.weightx    = 1.0;
	gbc.fill = gbc.HORIZONTAL;
        Mapanel.add( _comboNewNodeMapping, gbc );

	setMappingState();
    }	
		
    /**
     * Retreive db default folder from Directory server
     */
    private void getChainingLoc() {

	// Search default databse directory 
	try {
	    _model.setWaitCursor( true );
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    LDAPSearchResults res =
		ldc.search( CONFIG_BASEDN,
			    ldc.SCOPE_ONE,
			    "nsslapd-pluginid=chaining database",
			    null,
			    false );
	    if(res.hasMoreElements() ) {
		LDAPEntry bentry = (LDAPEntry)res.nextElement();
		_PluginLoc = bentry.getDN();
		Debug.println( "NewChainingInstancePanel.getChainingLoc() {");
		Debug.println( "*** plugin db: " + _PluginLoc );	
	    }
	} catch ( LDAPException e ) {
	    Debug.println( "NewChainingInstancePanel.getChainingLoc() " + e );
	} finally {
	    _model.setWaitCursor( false );
	}
		
	return;
    } // getChainingLoc



    /**
     * Enable/disable OK button
     *
     * @param ok true to enable the OK button
     */
    private void setOkay( boolean ok ) {
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( ok );
	} else {
	}
    }

    private void checkOkay() {
	String chname = getInstancename();
	String chserver = _urlLdapHostText.getText();
		
	boolean ok = ( (chname != null) && 
		       (chname.length() > 0) &&
		       _isNewMappingNodeValid && 
		       _isBindDNValid &&
		       (chserver != null) &&
		       (chserver.length() > 0));
	setOkay( ok );
    }

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals( _rbExistingNode)) {
	    setMappingState( EXISTING_NODE );
        } else if (e.getSource().equals( _rbNewNode)) {
	    setMappingState( NEW_NODE );
	} else if (e.getSource().equals( _rbNoNode)) {
	    setMappingState( NO_NODE );	
	} else if (e.getSource().equals( _cbUrlLdapSecureMode )) {
	    if ( _cbUrlLdapSecureMode.isSelected() ) {
		_sDefaultPort = DEFAULT_LDAPS_PORT;
	    } else {
		_sDefaultPort = DEFAULT_LDAP_PORT;
	    }
	    if ( ! _bHostModified ) {
		_urlLdapPortText.setText( _sDefaultPort );
	    }
	    if ( ! _bAlterModified ) {
		_urlAltPortText.setText( _sDefaultPort );
	    }
	} else if (e.getSource().equals( _bAddAlternateServer )) {
	    StringBuffer nalt = new StringBuffer();
	    nalt.append( _urlAltHostText.getText() );

	    if (( _urlAltPortText.getText() != null ) &&
		( _urlAltPortText.getText().trim().length() > 0)) {
		nalt.append( ":" );
		nalt.append( _urlAltPortText.getText().trim() );
	    }

	    _urlAltData.addElement( nalt.toString() );
	    _urlAltHostText.setText("");
	    _bAddAlternateServer.setEnabled( false );

	} else if (e.getSource().equals( _bDelAlternateServer )) {
	    int[] indices = _urlAltList.getSelectedIndices();
	    for (int i = indices.length - 1; i>= 0;i--) {					
		_urlAltData.removeElementAt(indices[i]);
	    }						
	    _bDelAlternateServer.setEnabled( false );
	} 
	super.actionPerformed(e);
	checkUrl();
	checkOkay();
    }


    private String getUrlVal() {
	StringBuffer nurl = new StringBuffer();

	if ( _cbUrlLdapSecureMode.isSelected() ) {
	    nurl.append( "ldaps://");
	} else {
	    nurl.append( "ldap://");
	}

	// Don't need to test if it's null the OK is not activated otherwise
	if(( _urlLdapHostText.getText() != null ) && 
	   ( _urlLdapHostText.getText().trim().length() > 0 )){
	    nurl.append( _urlLdapHostText.getText().trim());
	    if(( _urlLdapPortText.getText() != null ) && 
	       ( _urlLdapPortText.getText().trim().length() > 0 )){
		nurl.append(":");
		nurl.append( _urlLdapPortText.getText().trim());
	    }
	}				

	// Loop on alternate servers		
	for (int i = 0;
	     i < _urlAltList.getModel().getSize();
	     i++) {
	    nurl.append(" ");
	    nurl.append( _urlAltList.getModel().getElementAt( i ));
	}
				
	// Must finish by a '/'
	nurl.append("/");
	return( nurl.toString());
    }

    private void checkUrl() {

	String nurl = getUrlVal();
	_urlValue.setText( nurl );

	try {
	    setChangeState( _urlValue, CHANGE_STATE_UNMODIFIED );
	    LDAPUrl nu = new LDAPUrl( nurl );
	} catch ( java.net.MalformedURLException e ) {
	    setChangeState( _urlValue, CHANGE_STATE_ERROR );
	}
    }									 

    public int addChainingBackend(String _MappingNode ) {

	String _instName = _instanceNameText.getText().trim();
	getChainingLoc();

	LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	Debug.println("NewChainingInstancePanel.addChainingBackend()");
	// Retreive the ldbm location in config

	// Add Instance Entry
	String dn_dbInst = "cn=" + _instName + "," + _PluginLoc;
	Debug.println("****** dn = " + dn_dbInst);

	LDAPAttributeSet attrs = new LDAPAttributeSet();

	String objectclass_dbInst[] = { "top", "extensibleObject", "nsBackendInstance" };
	LDAPAttribute attr = new LDAPAttribute( "objectclass", objectclass_dbInst );
	attrs.add( attr );

	String cn_dbInst[] = { _instName };
	attrs.add( new LDAPAttribute( "cn", cn_dbInst ) );

				   
	String suffix_dbInstConfig[] = { _MappingNode };
	attrs.add( new LDAPAttribute ( "nsslapd-suffix", suffix_dbInstConfig ));

	String nsBindDN = _bindDNText.getText();

	if(( nsBindDN != null ) &&
	   ( nsBindDN.trim().length() > 0)) {
			
	    String nsMultiplexorBindDn[] = { nsBindDN.trim() };
	    attrs.add( new LDAPAttribute ( "nsMultiplexorBindDn",
					   nsMultiplexorBindDn ));

	    String nsPasswd = _bindPasswdText.getText();

	    if(( nsPasswd != null ) &&
	       ( nsPasswd.trim().length() > 0)) {
		Debug.println("****** pass= " + nsPasswd);
		String nsMultiplexorCredentials[] = { nsPasswd.trim() };
		attrs.add( new LDAPAttribute ( "nsMultiplexorCredentials",
					       nsMultiplexorCredentials ));
	    }

	}

	String ldapurl = getUrlVal();
	if(( ldapurl != null ) &&
	   ( ldapurl.trim().length() > 0)) {
			
	    String nsFarmServerURL[] = { ldapurl.trim() };
	    attrs.add( new LDAPAttribute ( "nsFarmServerURL",
					   nsFarmServerURL ));
	}

	LDAPEntry dbInst = new LDAPEntry( dn_dbInst, attrs );
	_model.setWaitCursor( true );
	try {
	    ldc.add( dbInst );
	    Debug.println("****** add:" + dn_dbInst);
	} catch (LDAPException e) {
	    // Comment until LDAP JDK support this code
	    // if( e.getLDAPResultCode() == LDAPException.LOCAL_ERROR ) {
	    if( e.getLDAPResultCode() == 82 ) {
		String[] args_m = { dn_dbInst,
				    _bindDNText.getText().trim() };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"error-bad-user",
					args_m,
					_section);		
	    } else {
		String[] args_m = { dn_dbInst, e.toString()} ;
		DSUtil.showErrorDialog( getModel().getFrame(),
					"error-add-mapping",
					args_m,
					_section);
	    }
	    Debug.println("****** error adding " +
			  dn_dbInst +
			  ". Error is : " +
			  e.toString() );
	    return (0);
	} finally {
	    _model.setWaitCursor( false );
	}
	return (1);
		
    }

    public boolean checkUnique() {
	_model.setWaitCursor( true );
	try {
	    String InstanceName = _instanceNameText.getText();
	    Debug.println("NewChainingInstancePanel.checkUnique(). check if " + InstanceName + " is unique");
	    _model.setWaitCursor( true );
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    if ( InstanceName != null) {
		LDAPSearchResults res =
		    ldc.search( CONFIG_BASEDN,
				ldc.SCOPE_SUB ,
				"(&(cn=" + InstanceName + ")(objectclass=nsBackendInstance))",
				null,
				false );
		_model.setWaitCursor( false );
		if (res.hasMoreElements()) {
		    DSUtil.showErrorDialog( getModel().getFrame(),
					    "backendname-exist",
					    InstanceName );
		    _instanceNameText.selectAll();
		    return (false);
		} //if (res.hasMoreElements())
	    } // if( InstanceName != null)
	} catch (LDAPException e) { 
	    _model.setWaitCursor( false );
	    DSUtil.showErrorDialog( getModel().getFrame(),
				    "backendname-can-create",
				    e.toString() );
	    return( false );
	} finally {
	    _model.setWaitCursor( false );
	} 
	return( true );
    }

    public void resetCallback() {
	/* No state to preserve */
	clearDirtyFlag();
	hideDialog();
    }

    /**
     * Enable the button and attached field.
     */
    private void setMappingState() {
	setMappingState( NEW_NODE );
    }

    private void setMappingState( int isSelected ) {
	switch ( isSelected ) {
	case EXISTING_NODE : {
	    _NewNodeText.setEnabled( false );
	    _NewNodeLabel.setEnabled( false );
	    _comboNewNodeMapping.setEnabled( false );
	    _comboNewNodeLabel.setEnabled( false );
	    _comboMapping.setEnabled( true );
	    _comboLabel.setEnabled( true );
	    _isNewMappingNodeValid = true;
	    break;
	}
	case NEW_NODE : {
	    _comboMapping.setEnabled( false );
	    _comboLabel.setEnabled( false );
	    _comboNewNodeMapping.setEnabled( true );
	    _comboNewNodeLabel.setEnabled( true );
	    _NewNodeText.setEnabled( true );
	    _NewNodeLabel.setEnabled( true );
	    _isNewMappingNodeValid = false;
	    break;
	}
	case NO_NODE : {
	    _NewNodeText.setEnabled( false );
	    _NewNodeLabel.setEnabled( false );
	    _comboMapping.setEnabled( false );
	    _comboLabel.setEnabled( false );
	    _comboNewNodeMapping.setEnabled( false );
	    _comboNewNodeLabel.setEnabled( false );
	    _isNewMappingNodeValid = true;
	    break;
	}
	}
	repaint();

    }

    public void okCallback() {
	/* No state to preserve */
	if(checkUnique()) {
	    int ret_add = 0;
	    if ( _rbExistingNode.isSelected() ) { // mapping node exist				
		ret_add = addChainingBackend( (String) _comboMapping.getSelectedItem() );
	    } else if ( _rbNewNode.isSelected() ) { // new mapping node
		ret_add = addChainingBackend( (String) _NewNodeText.getText() );
	    } else { // no mapping node 
		ret_add = addChainingBackend( "" );
	    }
	    if( ret_add == 0 ) {
		return;
	    }
	    String backends[] = { _instanceNameText.getText().trim() };
	    if( _rbNewNode.isSelected() ) {
		if(( _NewNodeText.getText() != null ) &&
		   ( _NewNodeText.getText().trim().length() > 0 )) {
		    if( MappingUtils.addMappingNode( 
						    _model.getServerInfo().getLDAPConnection(),
						    _section,
						    _NewNodeText.getText(),
						    (String) _comboNewNodeMapping.getSelectedItem(),
						    "backend",
						    backends,
						    null )) return;
					   
		} else {
		    DSUtil.showErrorDialog( getModel().getFrame(),
					    "new-mapping-node","" );
		    return;
		}				
	    } else if ( _rbExistingNode.isSelected() ) {
		// Add backend instance in selected Mapping Node				   
		LDAPModificationSet mods = new LDAPModificationSet();
			
		LDAPAttribute backend_instMapping = new LDAPAttribute( "nsslapd-backend",
								       backends );
		mods.add( LDAPModification.ADD, backend_instMapping );
		_model.setWaitCursor( true );
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		String dn_instMapping = MappingUtils.getMappingNodeForSuffix(ldc,
			(String) _comboMapping.getSelectedItem());
		Debug.println("okCallback updating :" + dn_instMapping);
		try {
		    ldc.modify(dn_instMapping,  mods );					
		} catch (LDAPException e) {
		    String[] args_m = { dn_instMapping, e.toString()} ;
		    DSUtil.showErrorDialog( getModel().getFrame(),
					    "error-mod-mapping",
					    args_m,
					    _section);
					
		    Debug.println("okCallback() error modifying " +
				  dn_instMapping +
				  ". Error is : " +
				  e.toString() );
		    return;
		} finally {
		    _model.setWaitCursor( false );
		}
	    }			

	    clearDirtyFlag();
	    if (( _bindDNText.getText() == null ) ||
		( _bindPasswdText.getText() == null ) ||
		( _bindDNText.getText().trim().length() == 0 ) ||
		( _bindPasswdText.getText().trim().length() == 0 )) {
				
		String[] args_m = { _urlLdapHostText.getText() };
		DSUtil.showInformationDialog(getModel().getFrame(),
					     "refresh-console-anon",
					     args_m,
					     _section);
	    } else {
		String[] args_m = { _bindDNText.getText(), _urlLdapHostText.getText() };
		DSUtil.showInformationDialog(getModel().getFrame(),
					     "refresh-console-user",
					     args_m,
					     _section);
	    }
	    hideDialog();
	}
    }

	
    private boolean isPortValid() {
	String nport = _urlLdapPortText.getText();
	boolean res = true;

	if(( nport != null) &&
	   ( nport.trim().length() > 0)) {
	    try {
		int p = Integer.parseInt( nport );
	    } catch (NumberFormatException nfe) {
		res = false ;
	    }
	}
	return ( res );
    }

    private void checkLabels( DocumentEvent e ) {
	if( e.getDocument() == _NewNodeText.getDocument() ) {
	    if ( DN.isDN ( _NewNodeText.getText() )) {
		setChangeState( _NewNodeLabel, CHANGE_STATE_UNMODIFIED );
		_isNewMappingNodeValid = true;
	    } else {
		setChangeState( _NewNodeLabel, CHANGE_STATE_ERROR );
		_isNewMappingNodeValid = false;
	    }
	} else if( e.getDocument() == _bindDNText.getDocument() ) {
	    if(( _bindDNText.getText() != null) && 
	       ( _bindDNText.getText().trim().length() > 0 )){
		if ( DN.isDN ( _bindDNText.getText() )) {
		    setChangeState( _bindDNLabel, CHANGE_STATE_UNMODIFIED );
		    _isBindDNValid = true;
		} else {
		    setChangeState( _bindDNLabel, CHANGE_STATE_ERROR );
		    _isBindDNValid = false;
		}
	    } else {
		setChangeState( _bindDNLabel, CHANGE_STATE_UNMODIFIED );
		_isBindDNValid = true;
	    }
	} else if( e.getDocument() == _urlAltHostText.getDocument() ) {
	    boolean addok = false;
	    if(( _urlAltHostText.getText() != null) && 
	       ( _urlAltHostText.getText().trim().length() > 0)) {
		addok = true;
	    }
	    _bAddAlternateServer.setEnabled( addok && isPortValid());

	} else if( e.getDocument() == _urlAltPortText.getDocument() ) {
	    if( isPortValid() ) {
		setChangeState( _urlLdapPortLabel, CHANGE_STATE_UNMODIFIED );
	    } else {
		setChangeState( _urlLdapPortLabel,
			  CHANGE_STATE_ERROR);
		_bAddAlternateServer.setEnabled( false );
	    }
	}
    }


    public void changedUpdate(DocumentEvent e) {
	if( ! _isInitialized ) return;
	checkLabels( e );
	checkUrl();
	super.changedUpdate( e );
	checkOkay();
    }


    public void removeUpdate(DocumentEvent e) {
	if( ! _isInitialized ) return;
	checkLabels( e );
	checkUrl();
	super.removeUpdate( e );
	checkOkay();
    }

    public void insertUpdate(DocumentEvent e) {
	if( ! _isInitialized ) return;
	checkLabels( e );
	checkUrl();
	super.insertUpdate( e );
	checkOkay();
    }

    public String getInstancename() {
	return _instanceNameText.getText().trim();
    }

    class AlternateCellRenderer extends DefaultListCellRenderer {  
	// This is the only method defined by ListCellRenderer.  We just
	// reconfigure the Jlabel each time we're called.
		
	public Component getListCellRendererComponent(
						      JList list,
						      Object value,            // value to display
						      int index,               // cell index
						      boolean isSelected,      // is the cell selected
						      boolean cellHasFocus)    // the list and the cell have the focus
	{
	    if ( list == _urlAltList ){
		if (list.isSelectionEmpty()) {
		    _bDelAlternateServer.setEnabled(false);
		} else {
		    _bDelAlternateServer.setEnabled(true);
		}
	    }
	    checkOkay();
	    return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
    }

    private JTextField	_instanceNameText;
    private JLabel		_instanceNameLabel, _authMechLabel;
    private JTextField	_mappingNameText;
    private JLabel		_mappingNameLabel;
    private JTextField	_NewNodeText;
    private JLabel		_NewNodeLabel;

    private IDSModel	_model = null;


    private JComboBox _comboMapping;
    private JLabel _comboLabel;
    private JButton _browseButton;
    private String _PluginLoc = "";
    private JRadioButton _rbExistingNode;
    private JRadioButton _rbNewNode;
    private JRadioButton _rbNoNode;


    private JCheckBox	_cbUrlLdapSecureMode;
    private JTextField	_urlLdapHostText;
    private JLabel		_urlLdapHostLabel;
    private JTextField	_urlLdapPortText;
    private JLabel		_urlLdapPortLabel;
	
    private JTextField	_urlAltHostText;
    private JLabel		_urlAltHostLabel;
    private JTextField	_urlAltPortText;
    private JLabel		_urlAltPortLabel;

    private JLabel		_bindDNLabel;
    private JTextField	_bindDNText;
    private JLabel		_bindPasswdLabel;
    private JTextField	_bindPasswdText;

    private JComboBox	_comboNewNodeMapping;
    private JLabel		_comboNewNodeLabel;

    private JButton		_bAddAlternateServer;
    private JButton		_bDelAlternateServer;
    private JButton		_bTestUrl;

    private JLabel		_urlValue;
    private JLabel		_urlValueLabel;
    private JList		_urlAltList;
    private DefaultListModel	_urlAltData;

    private boolean		_isBindDNValid = true;
    private boolean		_isNewMappingNodeValid = false;    
    private boolean		_bHostModified = false;
    private boolean		_bAlterModified = false;
    private final int ROWS = 4;

    private StringBuffer _sURL;
    private String _sDefaultPort = DEFAULT_LDAP_PORT;

    private final static String _section = "newchaining";
    static final String CONFIG_BASEDN = "cn=plugins, cn=config" ;
    static final String CONFIG_MAPPING = "cn=mapping tree, cn=config" ;
    static final String ROOT_MAPPING_NODE = "is root suffix";
    static final String HELP_REF = "helpReferral";
    static final int EXISTING_NODE = 0 ;
    static final int NEW_NODE = 1;
    static final int NO_NODE = 2;
    static final String ADD_ALT = "addAlternate";
    static final String DELETE_ALT = "deleteAlternate";
    static final String DEFAULT_LDAP_PORT = "389";
    static final String DEFAULT_LDAPS_PORT = "636";

 
    //copy from BlankPanel
    final static int DEFAULT_PADDING = 6;
    final static Insets DEFAULT_EMPTY_INSETS = new Insets(0,0,0,0);
    final static Insets BOTTOM_INSETS = new Insets(6,6,6,6);
}
