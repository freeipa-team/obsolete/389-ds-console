/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.io.File;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.admin.dirserv.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
abstract public class FilePanel extends BlankPanel {
	public FilePanel(IDSModel model, String section) {
		super(model, section);
		_section = section;
	}


	public FilePanel(IDSModel model, String section, boolean onlyLocal, boolean onlyRemote) {
		super(model, section);
		_section = section;
		_onlyLocal = onlyLocal;
		_onlyRemote = onlyRemote;
	}


    protected void createFileArea( JPanel grid ) {
        GridBagConstraints gbc = getGBC();
        gbc.gridwidth = gbc.REMAINDER;

		/* File selection group */
        JPanel panel = new JPanel( new GridBagLayout() );
        gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;	
        grid.add(panel, gbc);
        
		gbc.weightx = 0.0;
    	ButtonGroup group = new ButtonGroup();
        _rbLocal = makeJRadioButton( _section, "local-file", true );
    	group.add(_rbLocal);
		gbc.fill = gbc.NONE;
		gbc.gridwidth = 3;
        _rbRemote = makeJRadioButton( _section, "remote-file", false );
    	group.add(_rbRemote);

		JLabel label;
		
		if (_onlyLocal) {
			if (!super.isLocal()) {
				label = makeJLabel( _section, "filename-onlylocal" );
			} else {
				label = makeJLabel( _section, "filename" );
			}
		} else if (_onlyRemote) {
			if (!isLocal()) {
				label = makeJLabel( _section, "filename-onlyremote" );
			} else {
				label = makeJLabel( _section, "filename" );
			}
		} else {		
			label = makeJLabel( _section, "filename" );
		}
		gbc.gridwidth = 3;
		gbc.fill = gbc.HORIZONTAL;
  		gbc.weightx = 0.0;	
        panel.add( label, gbc );

		gbc.gridwidth--;
		_tfExport = new DSFileField((BlankPanel)this, _section, "filename",
									"", 25, DSFileField.FILE);
		label.setLabelFor(_tfExport);
  		gbc.weightx = 1.0;
        panel.add(_tfExport,gbc);

		_bExport = makeJButton( _section, "browse-file" );
        gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 0.0;	
        panel.add(_bExport,gbc);
				
		if ( !isLocal() && 
			 !_onlyRemote &&
			 !_onlyLocal) {
			JPanel buttonPanel = new JPanel(new GridBagLayout());			
			gbc.fill = gbc.NONE;
			gbc.gridwidth = 3;
			buttonPanel.add( _rbLocal, gbc );
			gbc.gridwidth--;
			buttonPanel.add( _rbRemote, gbc );
			gbc.weightx = 1.0;
			gbc.fill = gbc.HORIZONTAL;
			gbc.gridwidth = gbc.REMAINDER;
			buttonPanel.add(Box.createHorizontalGlue(), gbc);
			gbc.insets = new Insets(0,0,0,0);
			panel.add(buttonPanel, gbc);
		}
	}

	protected void createSeparator( JPanel grid ) {
        GridBagConstraints gbc = getGBC();        
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;	
        grid.add(new JSeparator(), gbc);
	}

	protected boolean isLocal() {
		return (super.isLocal() || _onlyLocal);
	}
        
    protected void checkOkay() {
	}

    protected String getFilename() {
	    return _tfExport.getText().trim();
	}

    protected void setFilename( String name ) {
	    _tfExport.setText( name.trim() );
	}

    protected boolean validateFilename() {
	    return _tfExport.dsValidate();
	}

    protected boolean getLocalState() {
        return !_rbRemote.isSelected();
    }
    
    protected void setLocalState( boolean state ) {
		if ( _bExport != null ) {
			_bExport.setEnabled( state );
			repaint();
		}
    }
    
    public void changedUpdate(DocumentEvent e) {
		super.changedUpdate( e );
		checkOkay();
	}

    public void removeUpdate(DocumentEvent e) {
		super.removeUpdate( e );
		checkOkay();
	}

    public void insertUpdate(DocumentEvent e) {
		super.insertUpdate( e );
		checkOkay();
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if ( e.getSource().equals( _rbLocal ) ) {
            setLocalState( true );
        } else if ( e.getSource().equals( _rbRemote ) ) {
            setLocalState( false );
        } else {
			super.actionPerformed(e);
		}
    }

    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
    }

	protected String getDefaultPath(IDSModel model ) {
		return DSUtil.getDefaultLDIFPath(model.getServerInfo());
	}

	protected DSFileField _tfExport;
	protected JButton _bExport;
	protected JButton _browseButton;
    protected JRadioButton _rbLocal;
    protected JRadioButton _rbRemote;
	protected ResourceSet _resource = DSUtil._resource;
    protected String _section = null;
	protected boolean _onlyLocal = false;
	protected boolean _onlyRemote = false;
}
