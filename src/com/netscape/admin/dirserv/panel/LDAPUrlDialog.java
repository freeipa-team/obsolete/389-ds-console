/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.management.nmclf.*;
import java.net.URL;
import netscape.ldap.*;
import netscape.ldap.util.*;


/**
 * LDAPUrlDialog
 * 
 *
 * @version 1.0
 * @author jvergara
 **/
public class LDAPUrlDialog extends AbstractDialog 
implements DocumentListener, SuiConstants {	


	/**
	  * The int editableOptions allows to specify which are the URL parameters (host, port and DN)
	  * that can be editable in this dialog.
	  */
    public LDAPUrlDialog( JFrame frame, String ldapUrl, int editableOptions ) {
		super(frame,
			  _resource.getString(_section, "title"), 
			  true, 
			  OK | CANCEL);
		_editableOptions = editableOptions;
		getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "description"));
		setDefaultButton( OK );
		setOKButtonEnabled(false);
		layoutComponents();
		initWithValue(ldapUrl);		
	}

	public LDAPUrlDialog( JFrame frame, String ldapUrl ) {
		this(frame, ldapUrl, ALL_EDITABLE);
	}	

	public LDAPUrlDialog( JFrame frame ) {
		this(frame, null);
    }
	
	public LDAPUrl getLDAPUrl() {
		return _url;
	}

	public void packAndShow() {
		pack();
		show();
	}	   

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void changedUpdate(DocumentEvent e) {
		insertUpdate(e);
    }
	
	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void removeUpdate(DocumentEvent e) {
		insertUpdate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void insertUpdate(DocumentEvent e) {
		notifyUpdate();
    }

	protected void initWithValue(String ldapUrl) {
		if (ldapUrl != null) {			
			try {
				LDAPUrl url = new LDAPUrl(ldapUrl);
				_refNewHostText.setText(url.getHost());
				_refNewPortText.setText(String.valueOf(url.getPort()));
				_refTargetDNText.setText(url.getDN());
				notifyUpdate();
			} catch(Exception e) {
				_refResult.setText( INVALID_URL );
			}
		}    
	}

    protected void layoutComponents() {
		JPanel panel = new JPanel(new GridBagLayout());
		setComponent(panel);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets     = new Insets(COMPONENT_SPACE, COMPONENT_SPACE, 0, COMPONENT_SPACE);
		
		_refNewHostLabel = UIFactory.makeJLabel(_section,
												"new-host",
												_resource);
		_refNewHostLabel.resetKeyboardActions();		
		gbc.fill= gbc.NONE;
		gbc.anchor = gbc.EAST; 
		gbc.gridwidth = 4;
		gbc.insets.right = 0;
		panel.add( _refNewHostLabel,gbc);				
		
		_refNewHostText = UIFactory.makeJTextField(this, 16);
		_refNewHostText.setEnabled((_editableOptions & EDITABLE_HOST) != 0);
		_refNewHostLabel.setLabelFor(_refNewHostText);
		gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
		gbc.insets.right = COMPONENT_SPACE;
		gbc.gridwidth--;
		panel.add( _refNewHostText,gbc);		

		_refNewPortLabel = UIFactory.makeJLabel(_section,
												"new-port",
												_resource);
		_refNewPortLabel.resetKeyboardActions();		
		gbc.weightx = 0.0;
		gbc.fill = gbc.NONE;		
		gbc.insets.right = 0;
		gbc.gridwidth = gbc.RELATIVE;
		panel.add( _refNewPortLabel,gbc);		

		_refNewPortText = UIFactory.makeJTextField(this,
												   5);
		_refNewPortText.setEnabled((_editableOptions & EDITABLE_PORT) != 0);
		_refNewPortLabel.setLabelFor(_refNewPortText);
		gbc.insets.right = COMPONENT_SPACE;
		gbc.gridwidth = gbc.REMAINDER;
		panel.add( _refNewPortText,gbc);		

				
		JPanel dnPanel = new JPanel(new GridBagLayout());
		_refTargetDNLabel =  UIFactory.makeJLabel(_section,
												  "new-targetDN",
												  _resource);
		_refTargetDNLabel.resetKeyboardActions();
		gbc.gridwidth = gbc.RELATIVE;
		gbc.fill= gbc.HORIZONTAL;
		gbc.anchor = gbc.NORTHWEST;
		gbc.insets.right = 0;
		dnPanel.add( _refTargetDNLabel,gbc);		
		
		_refTargetDNText = UIFactory.makeJTextField(this, -1);				
		_refTargetDNText.setEnabled((_editableOptions & EDITABLE_DN) != 0);
		_refTargetDNLabel.setLabelFor(_refTargetDNText);
		gbc.weightx = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets.right = COMPONENT_SPACE;
		dnPanel.add( _refTargetDNText,gbc);
		
		JPanel urlPanel = new GroupPanel(
										_resource.getString( _section, "result-title" ));			

		_refResult = new JLabel(" ");
		_refResult.setLabelFor(urlPanel);
		_refResult.setToolTipText(_resource.getString("construct-ldap-url","ttip"));
		urlPanel.add( _refResult );

		gbc.insets = new Insets(COMPONENT_SPACE,0,0,0);
		panel.add( dnPanel, gbc );
		panel.add( urlPanel, gbc );

		gbc.weighty = 1.0;
		gbc.fill = gbc.VERTICAL;				
		gbc.insets.bottom = COMPONENT_SPACE;
		panel.add(Box.createVerticalGlue(), gbc);
    }

    private void notifyUpdate() {
		String dn = _refTargetDNText.getText();
		boolean isDnValid = DSUtil.isValidDN(dn);		

		boolean isPortValid = false;
		try {
			Integer.parseInt(_refNewPortText.getText());
			isPortValid = true;
		} catch(Exception e) {
		}		

		boolean isHostValid = (_refNewHostText.getText().trim().length() > 0);		
		
		boolean isValid = isDnValid && 
			isPortValid && 
			isHostValid;		
		
		if (isValid) {
			String host = _refNewHostText.getText().trim();
			String port = _refNewPortText.getText().trim();
			String tdn = _refTargetDNText.getText().trim();
			String sUrl = "ldap://" + host + ":" + port;
			if (tdn.length() > 0) {
				sUrl += "/" + LDAPUrl.encode(tdn);
			}
			try {
				_url = new  LDAPUrl( sUrl );
				_refResult.setText( _url.toString() );
			} catch (Exception e) {
				isValid = false;
			} 			
		}
		if (!isValid) {
			_refResult.setText( INVALID_URL );
			_url = null;
		}
		setOKButtonEnabled( isValid );
    } 	


	/**
	 * If the user makes a cancel the _url returned is null.
	 */
	protected void cancelInvoked() {
		_url = null;
		super.cancelInvoked();
	}



	private JLabel				_refNewHostLabel;
    private JTextField			_refNewHostText;
    private JLabel				_refNewPortLabel;
    private JTextField			_refNewPortText;
    private JLabel				_refTargetDNLabel;
    private JTextField			_refTargetDNText;
    private JLabel				_refResult;	
	
	private LDAPUrl _url;

	private int _editableOptions;

    private final static String _section = "construct-ldap-url";
    private static ResourceSet _resource = DSUtil._resource;
	
	static String INVALID_URL = _resource.getString( _section, "invalid-url-label");
	public final static int EDITABLE_DN = 0x01;
	public final static int EDITABLE_HOST = 0x02;
	public final static int EDITABLE_PORT = 0x04;
	public final static int ALL_EDITABLE = EDITABLE_DN | EDITABLE_HOST | EDITABLE_PORT; 
}
