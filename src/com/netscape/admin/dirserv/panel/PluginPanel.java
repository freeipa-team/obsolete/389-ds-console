/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.io.File;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import javax.swing.event.*;
import java.util.Enumeration;
import java.util.Vector;
import java.util.Hashtable;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import javax.swing.text.Document;

import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSEntryObject;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.LDAPUtil;
import com.netscape.management.client.util.ResourceSet;
import netscape.ldap.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version 1.1, 02/18/00
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */

public class PluginPanel extends ContainerPanel {
    /**
     * Standard pane constructor.
     *
     * @param model The resource model for this panel.
     */    
    public PluginPanel(IDSModel model, LDAPEntry entry) {
	super( model, true );	
	_panel = new PluginStatusPanel( model, entry );
	
	add( _panel, "Center" );
	
	_panel.setParent( this );
	_selectedPanel = _panel;	
    }
    public PluginPanel(IDSModel model) {
	this( model, null );
    }

    public void init() {
	_panel.init();
    }

    public void updateFromEntry( LDAPEntry entry ) {
		_panel.updateFromEntry( entry );
    }

    public void editPluginEntry(LDAPEntry entry, ChangeListener changeListener) {
        _panel.editPluginEntry( entry, changeListener );        
    }
    
	public void refreshFromServer () {
		_panel.refreshFromServer();
	}
	
	protected JPanel createButtonPanel() {
		_bOK = UIFactory.makeJButton(this, "general", "Apply");
		_bReset = UIFactory.makeJButton(this, "general", "Reset");
		_bHelp = UIFactory.makeJButton(this, "general", "Help");
		_bAdvanced = UIFactory.makeJButton(this, "plugins", "Advanced");
 
		JButton[] buttons = { _bOK, _bReset, _bAdvanced, _bHelp };
		_buttonPanel = UIFactory.makeJButtonPanel( buttons, true );
		JPanel p = new JPanel();
		p.setLayout( new BorderLayout() );
		p.add( "Center", _buttonPanel );
		p.add( "South",
			   Box.createVerticalStrut(UIFactory.getDifferentSpace()) );
		p.add( "North",
			   Box.createVerticalStrut(UIFactory.getDifferentSpace()) );
		p.add( "East",
			   Box.createHorizontalStrut(UIFactory.getDifferentSpace()) );
		return p;
	}	

    public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(_bAdvanced)) {
			advancedCallBack();
			/* The case of the refresh is already managed by the pluginresourceobject
			   node */
		} else if (!e.getActionCommand().equals(DSResourceModel.REFRESH)) {		
			super.actionPerformed(e);
		}
	}

	public void advancedCallBack() {
		if (_panel != null) {
			_panel.advancedCallBack();
		}
	}
	public static String ENTRY = "entry";

	JButton _bAdvanced;
    private PluginStatusPanel _panel;
}

class PluginStatusPanel extends BlankPanel implements FocusListener {

    public PluginStatusPanel( IDSModel model,
							  LDAPEntry entry ) {
		super( model, _section );
		_helpToken = "configuration-plugins-help";
		_entry = entry;	       
    }
    
    private String getAttrVal( String attrName ) {
		if ( _entry != null ) {
			LDAPAttribute attr = _entry.getAttribute( attrName );
			if ( attr != null ) {
				Enumeration en = attr.getStringValues();
				if ( (en != null) && en.hasMoreElements() )
					return (String)en.nextElement();
			}
		}
		return "";
    }
	
    void updateFromEntry( LDAPEntry entry ) {	
		_fieldWithFocus = null;	
		/* This flag is used to avoid processing Document Events */
		_disableDocumentEvents = true;
		_entry = entry;
		
		init();
		
		updatePanel();
		
		_disableDocumentEvents = false;	
    }

    void editPluginEntry( LDAPEntry entry, ChangeListener changeListener ) {	
        _changeListener = changeListener;
        updateFromEntry(entry);
    }

   /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
    public void refreshFromServer () {
		resetCallback();		
    }

    public void init() {
		/* First time we call init() */
		if (_htEnabled == null) {
			_htEnabled = new Hashtable();		    
			_htLabels = new Hashtable();
			_htdsEntries = new Hashtable();
			_fields = new Hashtable();
			_htArguments = new Hashtable();	    
			
			createPanel();
		}
    }
	
	
    public void createPanel() {	    	
		_cbEnabled = makeJCheckBox( _section, "enabled");
	    
		GridBagConstraints gbc = getGBC();
		
		_attributesPanel = new GroupPanel(DSUtil._resource.getString(_section, ATTRS+"-label"));
		_attributesPanel.setLayout( new GridBagLayout() );
		
		EDITABLE_FIELDS = false;
		
		addEntry( _attributesPanel, "id", ID );
		addEntry( _attributesPanel, "description", DESCRIPTION );
		addEntry( _attributesPanel, "version", VERSION );
		addEntry( _attributesPanel, "vendor", VENDOR );	    	    
		addEntry( _attributesPanel, "type", TYPE );
	
		EDITABLE_FIELDS = true;
		
		addEntry( _attributesPanel, "initfunc", INITFUNC );
		addEntry( _attributesPanel, "path", PATH );
		
		_argumentsButtonsPanel = createArgumentsButtonsPanel();
				
		_fieldsPanel = new JPanel();
		_fieldsPanel.setLayout(new GridBagLayout());
		_argumentsPanel= new GroupPanel(DSUtil._resource.getString(_section, ARGS+"-label"));
		
		gbc.gridwidth = gbc.RELATIVE;
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor= gbc.NORTHEAST;
		gbc.insets.top = 0;
		gbc.insets.bottom = UIFactory.getComponentSpace();
		_argumentsPanel.add(_fieldsPanel, gbc);	 
		
		gbc.gridwidth = gbc.REMAINDER;
		gbc.anchor = gbc.NORTHEAST;
		gbc.fill = gbc.NONE;
		_argumentsPanel.add(_argumentsButtonsPanel, gbc);
		
		_myPanel.setLayout( new GridBagLayout() );
		
		gbc.gridwidth = 2;
		gbc.weightx    = 0;
		gbc.fill = gbc.NONE;
		int different = UIFactory.getDifferentSpace();
		int separate = UIFactory.getSeparatedSpace();
		gbc.insets = new Insets(0,separate,0,different);
		
		_myPanel.add(_cbEnabled,gbc);
		gbc.weightx    = 1.0;
		gbc.fill = gbc.HORIZONTAL;
		gbc.gridwidth = gbc.REMAINDER;
		_myPanel.add(Box.createGlue(),gbc);	    
		
		gbc.anchor = gbc.WEST;
		gbc.fill = gbc.BOTH;
		gbc.gridwidth = gbc.REMAINDER;
		_myPanel.add(_attributesPanel, gbc);
		
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.BOTH;
		gbc.anchor = gbc.WEST;
		
		_myPanel.add(_argumentsPanel, gbc);	 
		
		addBottomGlue();	    
		validate();
		
		_isInitialized = true;	
    }
    
    public void actionPerformed(ActionEvent e) {		
	if (e.getSource().equals(_cbEnabled)) {
	    validateCheckBox();
	    validateButtons();

	    _fieldWithFocus = null;
	} 	
	else if (e.getActionCommand().equals(ADD_ARGUMENT)) {	    	    
	    int numberArguments = _arguments.size();
	    
	    JTextComponent tf = null;
	    
	    DSEntryTextStrict dsEntryText = (DSEntryTextStrict)(_dsEntryFields.get(ARG+numberArguments));
	  
	    if (dsEntryText != null) {
		tf = (JTextComponent)(dsEntryText.getView(0));
	    }

	    if (tf==null) {		   		    
		tf = makeCustomJTextField("");		
	    } else {
		tf.setText("");
	    }
	    _arguments.put(ARG+numberArguments, tf);		
	    
	    if (numberArguments==0) 
		_fieldsPanel.removeAll();

	    addEntryField(_fieldsPanel, getLabelForArgument(numberArguments), tf);
		
	    refresh();
	    
	    validateFields();
	    validateButtons();

	    _fieldWithFocus = null;
	} else if (e.getActionCommand().equals(DELETE_ARGUMENT)) {
	    int numberArguments = _arguments.size();
	    if (numberArguments<1) {		
			return;	    
	    } else if (numberArguments==1) {
			/* If there's one argument, we add a NO ARGUMENTS label */		
			_fieldsPanel.removeAll();
			_fieldsPanel.add(new JLabel(NO_ARGS));	
			_arguments.remove(ARG+"0");
			
			refresh();
			_myPanel.revalidate();
			_myPanel.repaint();
			
			validateFields();
			validateButtons();
	    } else {		
			JTextComponent tf;
			int i = 0;
			boolean argumentToDeleteFound = false;
			if (_fieldWithFocus!=null) {
				while((tf=(JTextComponent)_arguments.get(ARG+i))!=null) {
					if (tf.equals(_fieldWithFocus)) {			    
						while(i < (numberArguments - 1)) {
							i++;
							JTextComponent nextTf=(JTextComponent)_arguments.get(ARG+i);
							_disableDocumentEvents = true;
							tf.setText(nextTf.getText());
							_disableDocumentEvents = false;
							tf = nextTf;
						}
						argumentToDeleteFound = true;
						break;
					}		    
					i++;					
				}		
				
				if (argumentToDeleteFound) {
					_fieldsPanel.remove(getLabelForArgument(numberArguments-1));
					_fieldsPanel.remove((JTextComponent)_arguments.get(ARG+(numberArguments-1)));
					
					_arguments.remove(ARG+(numberArguments-1));
					
					refresh();
					
					validateFields();
					validateButtons();							
				}
			}
			_fieldWithFocus = null;
		}
	} else{ 
		super.actionPerformed(e);
	}
    }
    
    public void okCallback() {
        boolean status = true;
        boolean requiresRestart = false;
        String dn = _entry.getDN();

        LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();	        		
	        
		if (isModified()) {			
			status &= saveEntry(ldc, dn);
			if (status){
				requiresRestart |= DSUtil.requiresRestart(dn, ENABLED);
			}
		}

        if (requiresRestart){
            DSUtil.showInformationDialog( getModel().getFrame(), "requires-restart",
										  (String)null ) ;
        }
		getModel().setWaitCursor(true);
        if (status){        
            clearDirtyFlag();
			clearValidFlag();

            // Clear the cache
			_htEnabled.clear();
			_htdsEntries.clear();

            // Update the plugin resource obeject
            if (_changeListener != null) {
                _changeListener.stateChanged(new ChangeEvent(_entry));
            }
        }
		updatePanel();
		getModel().setWaitCursor(false);
    }

    public void resetCallback() {
		getModel().setWaitCursor(true);

		boolean status = true;
		String dn = _entry.getDN();
		
		LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
						
		status &= resetEntry(ldc, dn);				 
		
		updateArgumentsPanel();
		
		validateFields();
		validateCheckBox();
		
		refresh();
		
        if (status){
            clearDirtyFlag();	  
			clearValidFlag();
        }
		getModel().setWaitCursor( false );
    } 

    private void validateCheckBox() {
		if (!_dsEnableState.isEditable()) {
			return;
		}
	JComponent view =
	    _dsEnableState.getView( _dsEnableState.getViewCount() - 1 );
        switch( _dsEnableState.doValidate() ) {
		case DSEntry.DSE_VALID_MOD:
			setChangeState( view, CHANGE_STATE_MODIFIED );
			break;
		case DSEntry.DSE_INVALID_MOD:
			setChangeState( view, CHANGE_STATE_ERROR );
			break;
		case DSEntry.DSE_VALID_NOMOD:
			setChangeState( view, CHANGE_STATE_UNMODIFIED );
			break;
		case DSEntry.DSE_INVALID_NOMOD:
			setChangeState( view, CHANGE_STATE_ERROR );
			break;
		}
	}

    private void validateButtons() {
		if (isModified()){
			setDirtyFlag();
			if (panelIsValid())
				setValidFlag();
			else
				clearValidFlag();			
		} else {
			clearDirtyFlag();
			clearValidFlag();
		}
		/* See if any of the arguments fields has the focus*/
		setDeleteButtonEnabled(false);
		if (_fieldWithFocus != null) {
			if (_fieldWithFocus.hasFocus()) {
				setDeleteButtonEnabled(true);
				
			}
		}
	}

    private void validateFields() {
	JTextField tf;

	int result;
	DSEntryTextStrict dsEntryText;
	
	for( int i = 0; i < EDITABLE_ATTRNAMES.length; i++ ) {	
	    dsEntryText = (DSEntryTextStrict)_dsEntryFields.get(EDITABLE_ATTRNAMES[i]);
	    if (dsEntryText != null) {
		JComponent view = dsEntryText.getView(dsEntryText.getViewCount() - 1 );

		switch (dsEntryText.doValidate() ) {
		    case DSEntry.DSE_VALID_MOD:
			setChangeState( view, CHANGE_STATE_MODIFIED );
			break;
		case DSEntry.DSE_INVALID_MOD:
			setChangeState( view, CHANGE_STATE_ERROR );
			break;
		case DSEntry.DSE_VALID_NOMOD:
			setChangeState( view, CHANGE_STATE_UNMODIFIED );
			break;
		case DSEntry.DSE_INVALID_NOMOD:
			setChangeState( view, CHANGE_STATE_ERROR );
			break;
		}
	    }
	}

	int i = 0;
	while ((dsEntryText = (DSEntryTextStrict)_dsEntryFields.get(ARG+i))!=null) {
	    JComponent view = dsEntryText.getView(dsEntryText.getViewCount() - 1 );

	    switch (dsEntryText.doValidate() ) {
	    case DSEntry.DSE_VALID_MOD:
		setChangeState( view, CHANGE_STATE_MODIFIED );
		break;
	    case DSEntry.DSE_INVALID_MOD:
		setChangeState( view, CHANGE_STATE_ERROR );
		break;
	    case DSEntry.DSE_VALID_NOMOD:
		setChangeState( view, CHANGE_STATE_UNMODIFIED );
		break;
	    case DSEntry.DSE_INVALID_NOMOD:
		setChangeState( view, CHANGE_STATE_ERROR );
		break;
	    }
	    i++;
	}

	    
	/* We validate the attribute fields with no dsEntry associated */
	i = _dsEntryFields.size() - ATTRNAMES.length;
	while (i < _arguments.size()) {
	    tf = (JTextField)_arguments.get(ARG+i);
	    if (tf.getText().trim().equals("")) {
		setChangeState(getLabelForArgument(i), CHANGE_STATE_ERROR);
	    } else {
		setChangeState(getLabelForArgument(i), CHANGE_STATE_MODIFIED);
	    }
	    i++;
	}
    }
    
 
    
    private boolean saveEntry (LDAPConnection ldc, String dn) {
        boolean status = false; 
        boolean done   = false;
		String fieldText = "";	
		LDAPModificationSet mods = null;		
		
		if (_dsEnableState.isModified()) {
			mods = new LDAPModificationSet();
			mods.add(LDAPModification.REPLACE,
					 new LDAPAttribute(ENABLED, _dsEnableState.localToRemote()));
		}
		
        while (!done){
            try {
				_entry = ldc.read(dn);

				DSEntryTextStrict dsEntryText;
				
				for( int i = 0; i < EDITABLE_ATTRNAMES.length; i++ ) {
					fieldText = ((JTextComponent)_fields.get(EDITABLE_ATTRNAMES[i])).getText();
					
					if (!fieldText.equals(getAttrVal( EDITABLE_ATTRNAMES[i] ))) {					
						if (mods == null)
							mods = new LDAPModificationSet();
						mods.add(LDAPModification.REPLACE, new LDAPAttribute(EDITABLE_ATTRNAMES[i], fieldText));
						
						dsEntryText = (DSEntryTextStrict)_dsEntryFields.get(EDITABLE_ATTRNAMES[i]);
						
						if (dsEntryText != null) {
							dsEntryText.reset();
						}
					}		    
				}
				
				JTextComponent tf;
				int i = 0;		
				
				while ((tf=(JTextComponent)_arguments.get(ARG+i))!=null) {		    
					fieldText = tf.getText();
					
					if (!fieldText.equals(getAttrVal(ARG+i))) {				
						if (mods==null)
							mods = new LDAPModificationSet();
						mods.add(LDAPModification.REPLACE, new LDAPAttribute(ARG+i, fieldText));
					}
					i++;		    
				}
				
				/* We delete the remaining arguments in the LDAP entry */
				while( !(getAttrVal(ARG+i).equals(""))) {		    
					if (mods==null)
						mods = new LDAPModificationSet();
					mods.add(LDAPModification.DELETE, new LDAPAttribute(ARG+i));
					
					i++;
				}
				
				if (mods !=null) 
					ldc.modify(dn, mods);
				
				_entry = ldc.read( dn );
				
				/* We update the DSEntries for the Arguments and the _arguments Hashtable */ 				
				String value;
				i = 0;
				while( !(value = getAttrVal(ARG+i)).equals("") ) {
					tf=(JTextComponent)_arguments.get(ARG+i);
					if (tf == null) {			
						tf = makeCustomJTextField(value);
					}
					dsEntryText = new DSEntryTextStrict(value, tf, getLabelForArgument(i));		    
					_dsEntryFields.put(ARG+i, dsEntryText);
					
					dsEntryText.show();
					i++;
				}	    
				
				int argumentNumber = i;
				
				while (_arguments.get(ARG+i)!=null) {		    
					_arguments.remove(ARG+i);
					i++;
				}
				
				i = argumentNumber;
				
				while (_dsEntryFields.get(ARG+i)!=null) {
					_dsEntryFields.remove(ARG+i);
					i++;
				}
				
				
				if (isModified())
					_dsEnableState.reset();
				
				status = true;
				done   = true;
            } catch (LDAPException lde){
                IDSModel model = getModel();
                Debug.println ("PluginPanel.saveEntry(): LDAP error code = " +
                               lde.getLDAPResultCode() + " error=" + lde); 
				
                if (lde.getLDAPResultCode() == lde.INSUFFICIENT_ACCESS_RIGHTS){
                    /* check if this is already root dn */
                    String rootdn = (String)model.getServerInfo().get("rootdn");
                    if (rootdn.equalsIgnoreCase(ldc.getAuthenticationDN()))
                        done = true;
                    else{
                        DSUtil.showPermissionDialog(model.getFrame(), ldc);
						if (!model.getNewAuthentication(false)) {
							done = true; // error, just punt
						}
                    }
                } else {
                    status = processSaveErrors(lde);
                    done = true;
                }
            }
        }
		
		getModel().notifyAuthChangeListeners();
		
        return status;
    }		
	
    private boolean resetEntry (LDAPConnection ldc, String dn){
        boolean status = false;
        boolean done = false;
        
        String value;        

		DSEntryTextStrict dsEntryText;
                
        while (!done){
            try {
                _entry = ldc.read(dn);
                if (_entry != null){		    
                    value   = getAttrVal(ENABLED);
                    if (value != null){
                        _dsEnableState.remoteToLocal(value);
						_dsEnableState.show();	
                        status = true;
                    }
		    
		    for( int i = 0; i < EDITABLE_ATTRNAMES.length; i++ ) {
			value   = getAttrVal(EDITABLE_ATTRNAMES[i]);
			if (value != null){
			    dsEntryText = (DSEntryTextStrict)_dsEntryFields.get(EDITABLE_ATTRNAMES[i]);
			    if (dsEntryText != null) {
				dsEntryText.remoteToLocal(value);
				dsEntryText.show();
				status = true;
			    }
			}
		    }
		    
		    int i = 0;

		    while (!(value = getAttrVal(ARG+i)).equals("")) {
			dsEntryText = (DSEntryTextStrict)_dsEntryFields.get(ARG+i);
			if (dsEntryText == null) {
			    JTextField tf = makeCustomJTextField(value);
			    
			    dsEntryText = new DSEntryTextStrict(value, tf, getLabelForArgument(i));
			    _dsEntryFields.put(ARG+i, dsEntryText);			    			    
			}
			dsEntryText.remoteToLocal(value);
			dsEntryText.show();
			/* Update the _arguments Hashtable */
			_arguments.put(ARG+i, dsEntryText.getView(0));						

			status = true;

			i++;
		    }

		    JTextComponent tf;
		    
		    while ((tf=(JTextComponent)_arguments.get(ARG+i))!=null) {
			_arguments.remove(ARG+i);
			i++;
		    }
		    
		    done = true;
		}
            } catch (LDAPException lde) {
                IDSModel model = getModel();
                Debug.println ("PluginPanel.resetEntry(): LDAP error code = " +
                               lde.getLDAPResultCode() + " error=" + lde); 
                if (lde.getLDAPResultCode() == lde.INSUFFICIENT_ACCESS_RIGHTS){
                    /* check if this is already root dn */
                    String rootdn = (String)model.getServerInfo().get("rootdn");
                    if (rootdn.equalsIgnoreCase(ldc.getAuthenticationDN()))
                        done = true;
                    else{
                        DSUtil.showPermissionDialog(model.getFrame(), ldc);
				        if (!model.getNewAuthentication(false)) {
					        done = true; // error, just punt
				        }
                    }
                } else {
                    status = processReadErrors(lde);
                    done = true;
                }
            } 
        }

	getModel().notifyAuthChangeListeners();

        return status;
    }

    private void addEntry( JPanel grid,
			   String labelName, String attrName ) {
	JLabel label = makeJLabel( _section, labelName );
	addEntry(grid, label, attrName);	
    }

    private void addEntry( JPanel grid, JLabel label, String attrName ) {			
	final int columns = 26;
	JTextComponent tf;
	if ( EDITABLE_FIELDS ) {
	    tf = makeCustomJTextField( "" );	    	
	} else {
	    tf = UIFactory.makeMultiLineLabel( 1, columns );		
	}
	addEntryField( grid, label, tf );
	if ( !STRETCH_FIELDS ) {
	    tf.setMargin( getTextInsets() );
	    tf.invalidate();
	}		
	_fields.put( attrName, tf );
	
	if (label instanceof JLabel) {
	    if (!((JLabel)label).getText().equals(""))
		_htLabels.put(attrName, label);	
	    
	}
    }

	/* This is a customized version of the addEntryField method in BlankPanel: in this one
	   the field is anchored in the UPPER side instead of being ANCHORED in the center*/
    protected void addEntryField( JPanel panel, JComponent label,
				  JComponent field, JLabel label2 ) {
		if (label instanceof JLabel) {
			((JLabel)label).setLabelFor(field);
		}
	Component endGlue = STRETCH_FIELDS ? null : Box.createGlue();
	Component lastItem =
	    STRETCH_FIELDS ? ((label2 != null) ? label2 : field) : endGlue;
	GridBagConstraints gbc = getGBC();
	gbc.fill = gbc.NONE;
	gbc.weightx = 0.0;
	gbc.gridwidth = 1;
	gbc.gridx = 0;
	gbc.anchor = gbc.NORTHEAST;
	int space = UIFactory.getComponentSpace();
	gbc.insets = new Insets( space, space, 0, space/2 );
	panel.add( label, gbc );
	
	gbc.gridx++;
	gbc.anchor = gbc.WEST;
	gbc.insets = new Insets( space, 0, 0, 0 );
	if ( STRETCH_FIELDS ) {
	    gbc.fill = gbc.HORIZONTAL;
	    gbc.weightx = 1.0;
	}
	gbc.gridwidth = (lastItem == field) ? gbc.REMAINDER : 1;
	panel.add( field, gbc );
	
	if ( label2 != null ) {
	    gbc.gridx++;
	    gbc.fill = gbc.NONE;
	    gbc.weightx = 0.0;
	    gbc.insets = new Insets( space, space/2, 0, 0 );
	    gbc.gridwidth = (lastItem == label2) ? gbc.REMAINDER : 1;
	    panel.add( label2, gbc );
	}
	
	if ( !STRETCH_FIELDS ) {
	    gbc.gridx++;
	    gbc.anchor = gbc.EAST;
	    gbc.fill = gbc.HORIZONTAL;
	    gbc.weightx = 1.0;
	    gbc.gridwidth = gbc.REMAINDER;
	    panel.add( endGlue, gbc );
	}	   
    }

    private JTextField  makeCustomJTextField(String string) {
	JTextField tf;
	tf = makeJTextField(string);
	final int columns = 26;
	((JTextField)tf).setColumns( columns );
	tf.getDocument().addDocumentListener(this);
	tf.addFocusListener(this);
	return tf;
    }
    
    /**
     * Check if a plugin is a core plugin (it must be always enabled). 
     * Lookup the plugin in the NON_ENABLABLE_ list by its init function name.
     */     
    private boolean isCorePlugin(String initFn) {    
        String[] fnList = NON_ENABLABLE_PLUGINS_INIT_FUNCTION;
        boolean isCore = false;
        for (int i = 0; initFn != null && i<fnList.length; i++) {
            if (initFn.equals(fnList[i])) {
                isCore = true;
                break;
            }
        }
        Debug.println("PluginPanel.isCorePlugin(" + initFn + ")=" +isCore);                      
        return isCore;        
    }

    /**
     * Check recursively if a plugin has any core subordinates. That will make
     * the plugin in question also a core plugin (must be always enabled)
     */
    private boolean hasCoreSubordinates(String plugin, Vector dependents, int recursionDepth) {
        
        // Prevent an endless recursion, just in case there are plugins with
        // cyclic dependencies.
        if (recursionDepth >= 10) {
            return false;
        }
        
        recursionDepth++;

        for (int i=0; i < dependents.size(); i++) {

            LDAPEntry entry = (LDAPEntry)dependents.elementAt(i);
            LDAPAttribute nameAttr = entry.getAttribute(NAME_ATTR);
            if ( nameAttr != null ) {
                String name = nameAttr.getStringValueArray()[0];
                LDAPAttribute initFnAttr = entry.getAttribute(INITFUNC);                
                if (initFnAttr != null) {
                    String initFn = initFnAttr.getStringValueArray()[0];
                    if (isCorePlugin(initFn)) {
                        return true;
                    }
                }

                if (hasCoreSubordinates(name, getSubordinates(name), recursionDepth)) {    
                    return true;
                }
            }

        }
        return false;
    }

    /**
     * Return a list of disabled plugins that this plugin depends on.
     * A plugin can not be enabled if it has disabled superiors.
     * @return Vector of plugin names as Strings 
     */
    private Vector getDisabledSuperiors(LDAPEntry entry) {
        LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
        Vector disabledSup = new Vector();
        
        LDAPAttribute superiorsAttr = entry.getAttribute(DEPENDS_ON_NAMED);
        if (superiorsAttr == null) {
            Debug.println("PluginPanel.getDisabledSuperiors: attr " +
                          DEPENDS_ON_NAMED + " not found in " + entry.getDN());
            return disabledSup;
        }

        String[] superiors = superiorsAttr.getStringValueArray();
        for (int i=0; i < superiors.length; i++) {
            
            try {
                String filter = "&(" + NAME_ATTR + "=" + superiors[i] +")(" + 
                            ENABLED + "=" + OFF + ")";
                Debug.println(6, "PluginPanel.getDisabledSuperiors: the filter is " + filter);
                String[] attrs = { NAME_ATTR };
                
                LDAPSearchConstraints cons =
                    (LDAPSearchConstraints)ldc.getSearchConstraints();
                cons.setMaxResults( 0 );

                LDAPSearchResults res = ldc.search(DSUtil.PLUGIN_CONFIG_BASE_DN,
                                                     ldc.SCOPE_SUB,
                                                     filter, attrs, false);

                if (res.hasMoreElements()) {
                    disabledSup.addElement(superiors[i]);
                }
            }
            catch (LDAPException e) {
                Debug.println( "PluginPanel.getDisabledSuperiors: " + e);
            }
        }

        return disabledSup;
    }
    
    /**
     * Return a list of enabled plugins that depend on this one.
     * A plugin can not be disabled if it has enabled subordinates.
     * @return Vector of plugin names as Strings
     */
    private Vector getEnabledSubordinates(String plugin) {
        LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
        Vector enabledSub = new Vector();
        try {                                
            String filter = "&(" + DEPENDS_ON_NAMED + "=" + plugin + ")(" +
                            ENABLED + "=" + ON + ")";
            Debug.println(6, "PluginPanel.getEnabledSubordinates: the filter is "+filter);
            String[] attrs = { NAME_ATTR };
                
            LDAPSearchConstraints cons =
                (LDAPSearchConstraints)ldc.getSearchConstraints();
            cons.setMaxResults( 0 );
                
            LDAPSearchResults res = ldc.search(DSUtil.PLUGIN_CONFIG_BASE_DN,
                                                 ldc.SCOPE_SUB,
                                                 filter, attrs, false, cons);

            while(res.hasMoreElements()) {
                LDAPEntry entry = (LDAPEntry) res.nextElement();
                String subordinate = entry.getAttribute(NAME_ATTR).getStringValueArray()[0];
                enabledSub.addElement(subordinate);
            }
        }
        catch (LDAPException e) {
            Debug.println( "PluginPanel.getEnabledSubordinates: " + e);
        }
        
        return enabledSub;
    }

    /**
     * Return a list of plugins that depend on this one
     * @return Vector of LDAPEntry
     */
    private Vector getSubordinates(String plugin) {
        LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
        Vector subordinates = new Vector();
        try {                                
            String filter = "(" + DEPENDS_ON_NAMED + "=" + plugin +")";
            Debug.println(6, "PluginPanel.getSubordinates: the filter is " + filter);
            String[] attrs = { NAME_ATTR, INITFUNC };
                
            LDAPSearchConstraints cons =
                (LDAPSearchConstraints)ldc.getSearchConstraints();
            cons.setMaxResults( 0 );
                
            LDAPSearchResults res = ldc.search(DSUtil.PLUGIN_CONFIG_BASE_DN,
                                                 ldc.SCOPE_SUB,
                                                 filter, attrs, false, cons);

            while(res.hasMoreElements()) {
                LDAPEntry entry = (LDAPEntry)res.nextElement();
                String dependentName = entry.getAttribute(NAME_ATTR).getStringValueArray()[0];
                Debug.println("PluginPanel: Plugin \"" + dependentName +
                              "\" depends on plugin \"" + plugin + "\"");                    
                subordinates.addElement(entry);
            }                
        } catch (LDAPException e) {
            Debug.println( "PluginPanel.getSubordinates: " + e);
        }
        return subordinates;
    }


    private void updateEnabledCheckBox() {

        _dsEnableState = (EnableStateControl)_htEnabled.get(_entry.getDN()); 

        if (_dsEnableState == null) {

            // This is the first time we see this plugin.
            // Check and cache plugin dependencies
            
            _dsEnableState = new EnableStateControl(getAttrVal(ENABLED), _cbEnabled );

            /* Check if we can enable/disable this plugin */ 
            String initFunction = getAttrVal(INITFUNC).trim();
            String name = getAttrVal(NAME_ATTR);
            if (isCorePlugin(initFunction)) {
                _dsEnableState.setEditable(false);
            }
            else {
                Vector subordinates = getSubordinates(name);
                _dsEnableState.setEditable(!hasCoreSubordinates(name, subordinates, 0));
            }

            if (_dsEnableState.isEditable()) {
                _dsEnableState.setDisabledSuperiors(getDisabledSuperiors(_entry));
                _dsEnableState.setEnabledSubordinates(getEnabledSubordinates(name));
            }
            _htEnabled.put(_entry.getDN(), _dsEnableState);
        }

        _cbEnabled.setEnabled(_dsEnableState.isEditable());
        _dsEnableState.show();

    }

    private void updateFields() {
	_dsEntryFields = (Hashtable)_htdsEntries.get(_entry.getDN());
	_arguments = (Hashtable)_htArguments.get(_entry.getDN());
	DSEntryTextStrict dsEntryText;

	if (_dsEntryFields == null) {
	    _dsEntryFields = new Hashtable();
	    _htdsEntries.put(_entry.getDN(), _dsEntryFields);

	    _arguments = new Hashtable();
	    _htArguments.put(_entry.getDN(), _arguments);	    
	
	    JTextComponent tf;
	    	    
	    for( int i = 0; i < ATTRNAMES.length; i++ ) {
		tf = (JTextComponent)_fields.get(ATTRNAMES[i]);
		dsEntryText = new DSEntryTextStrict(getAttrVal(ATTRNAMES[i]), tf, (JLabel)_htLabels.get(ATTRNAMES[i]));
		_dsEntryFields.put(ATTRNAMES[i], dsEntryText);		
	    }

	    String value;
	    int i = 0;
	    while( !(value = getAttrVal(ARG+i)).equals("") ) {
		if ((tf=(JTextComponent)_arguments.get(ARG+i))==null) {
		    tf = makeCustomJTextField(value);
		    
		    _arguments.put(ARG+i, tf);
		}
		dsEntryText = new DSEntryTextStrict(value, tf, getLabelForArgument(i));
		_dsEntryFields.put(ARG+i, dsEntryText);
		
		i++;
	    }	    
	}

	Enumeration enumAttrs = _dsEntryFields.keys();

	while (enumAttrs.hasMoreElements()) {
	    String attributeName = (String)enumAttrs.nextElement();
	    ((DSEntryTextStrict)_dsEntryFields.get(attributeName)).show();	      
	}
    }
	
	/* We call this method to set properly the state (enabled/disabled) of the buttons 
	   and the color of the fields (not modified/ modified and correct/ modified and not correct)*/
    private void updatePanel() {
		updateEnabledCheckBox();
		updateFields();
		
		updateArgumentsPanel();
		
		refresh();
		
		validateCheckBox();
		validateFields();
		validateButtons();	
    }
    
    private void updateArgumentsPanel () {
	JTextComponent tf;
	int i = 0;

	_fieldsPanel.removeAll();
	if (_arguments.size() < 1) {
	    _fieldsPanel.add(new JLabel(NO_ARGS));
	}
	else {
	    while ((tf = (JTextComponent)_arguments.get(ARG+i)) != null) {
			addEntryField(_fieldsPanel, getLabelForArgument(i), tf);
			i++;
	    }
	}
	_fieldsPanel.setMaximumSize(_fieldsPanel.getSize());
    }
    
    private boolean isModified() {
	if (_dsEnableState.isModified())
	    return true;	

	Enumeration enumAttrs = _dsEntryFields.keys();

	while (enumAttrs.hasMoreElements()) {
	    String attributeName = (String)enumAttrs.nextElement();
	    if (((DSEntryTextStrict)_dsEntryFields.get(attributeName)).isModified())
		return true;
	}

	/* We see if the number of arguments we display (in _arguments) is the same we had (number of arguments in _dsEntryFields) */
	if (_arguments.size() != (_dsEntryFields.size() - ATTRNAMES.length))
	    return true;
	
        return false;
    }

    public boolean panelIsValid() {
	for( int i = 0; i < EDITABLE_ATTRNAMES.length; i++ ) {
	    if (((DSEntryTextStrict)_dsEntryFields.get(EDITABLE_ATTRNAMES[i])).validate()==DSEntryTextStrict.ERROR_EMPTY_FIELD)
		return false;
	}

	DSEntryTextStrict dsEntryText;



	/* We see in the arguments we display (that can be different in number to those we have in _dsEntryField) if the arguments are valid or not*/
	JTextComponent tf;
	int i = 0;
	while ((tf = (JTextComponent)_arguments.get(ARG+i)) != null) {
	    if (tf.getText().trim().equals(""))
		return false;
	    i++;
	}

	return true;
    }
	

    private JPanel createArgumentsButtonsPanel() {		
	JPanel editArgumentsPanel = new JPanel();		
	editArgumentsPanel.setLayout(new GridBagLayout());
	
	GridBagConstraints gbc = getGBC();						
	

	_bAdd = makeJButton(_section, "badd", (ResourceSet)null);
	setMnemonic(_bAdd, _section, "badd", DSUtil._resource);	
	_bAdd.setActionCommand(ADD_ARGUMENT);


	_bDelete = makeJButton(_section, "bdelete", (ResourceSet)null);
	setMnemonic(_bDelete, _section, "bdelete", DSUtil._resource);	
	_bDelete.setActionCommand(DELETE_ARGUMENT);

	gbc.fill = gbc.HORIZONTAL;
	gbc.gridwidth = gbc.REMAINDER;
	editArgumentsPanel.add(_bAdd, gbc);
	gbc.fill = gbc.HORIZONTAL;
	gbc.gridwidth = gbc.REMAINDER;
	editArgumentsPanel.add(_bDelete, gbc);
	
	JPanel fReturn = new JPanel();
	fReturn.setLayout(new GridBagLayout());
	gbc.gridwidth = gbc.RELATIVE;
	fReturn.add(editArgumentsPanel, gbc);
	gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.BOTH;
	fReturn.add(Box.createHorizontalStrut(20), gbc);
	
	return fReturn;
    }

    private JLabel getLabelForArgument(int i) {
	JLabel label = (JLabel)_htLabels.get(ARG+i);
	if (label == null) {
	    label = new JLabel((i+1)+" ");
	    _htLabels.put(ARG+i, label);
	}
	return label;
    }

	private void setDeleteButtonEnabled(boolean state) {
		_bDelete.setEnabled(state);
	}
    

    public void changedUpdate(DocumentEvent e) {
	if (!_disableDocumentEvents) {		
	    validateFields();
	    validateButtons();
	}
    }

    public void removeUpdate(DocumentEvent e) {
	changedUpdate(e);
    }

    public void insertUpdate(DocumentEvent e) {
	changedUpdate(e);			
    }

    public void focusGained(FocusEvent e) {
		if (e.getComponent() instanceof JTextComponent) {
			_fieldWithFocus = (JTextComponent)e.getComponent();			
			if (_fieldsPanel.isAncestorOf(_fieldWithFocus)) {
				setDeleteButtonEnabled(true);
			} else {
				setDeleteButtonEnabled(false);
			}
		} else {
			setDeleteButtonEnabled(false);
		}
    }

    public void focusLost(FocusEvent e) {
	if (!isShowing()) {
	    /* Lost focus outside the panel */
	    _fieldWithFocus = null;
		setDeleteButtonEnabled(false);
	}	
    } 

	public void advancedCallBack() {
		LDAPEntry oldEntry = _entry;
		String oldDN = _entry.getDN();
		String oldName = getAttrVal(NAME_ATTR);
		DSEntryObject entryObject = new DSEntryObject(getModel(), _entry, true);
		if (entryObject.editGeneric(true, true) == null) {
			return;
		}		
		_entry = entryObject.getEntry();
        if (_entry != null) {

            if  (_changeListener != null) {
                _changeListener.stateChanged(new ChangeEvent(_entry));
            }

			_htEnabled.remove(oldDN);
			_htdsEntries.remove(oldDN);
			_htArguments.remove(oldDN);

			updateFromEntry( _entry );

			String dn = _entry.getDN();
			boolean requiresRestart = false;
			requiresRestart |= DSUtil.requiresRestart(dn, ENABLED);
			 		        	 
			getModel().setWaitCursor(false);				
			
			if (requiresRestart){
				DSUtil.showInformationDialog( getModel().getFrame(), "requires-restart",
											  (String)null ) ;
			}
		}
	}

	private void setMnemonic(JButton button, String section, String key, ResourceSet resource) {
		String mnemonic = resource.getString(section, key+"-mnemonic");
		if ((mnemonic != null) &&
			(mnemonic.length() > 0)) {
			button.setMnemonic(mnemonic.charAt(0));
		}
	}

    /**
     * EnableStateControl caches info relevant to enabling plugins and
     * validates dependencies on enable state change.
     */
    class EnableStateControl extends DSEntryBoolean {
        Vector disabledSuperiors, enabledSubordinates;
        boolean editable = true;
        
        public EnableStateControl(String model, AbstractButton view) { 
            super(model, view);
        }

        void setDisabledSuperiors(Vector v) {
            disabledSuperiors = v;
        }
        void setEnabledSubordinates(Vector v) {
            enabledSubordinates = v;
        }

        void setEditable(boolean b) {
            editable = b;
        }
        
        boolean isEditable() {
            return editable;
        }

        public int doValidate() {

            updateModel ();

            if (!this.isModified()) {
                return DSEntry.DSE_VALID_NOMOD;
            }
            
            JCheckBox checkBox = (JCheckBox)getView(0);
            boolean enableAction = checkBox.isSelected();

            // Can not enable plugins with disabled superior plugins
            if (enableAction && disabledSuperiors != null &&
                disabledSuperiors.size() != 0) {

                String plugin = (String)disabledSuperiors.elementAt(0);
                DSUtil.showErrorDialog(null, "enable-dependency",
                                       new String[] {plugin}, _section);

                // restore the previous checkbox state
                checkBox.setSelected(false);
                setModelAt(_falseValue, 0);
                return DSEntry.DSE_VALID_NOMOD;
            }
            
            // Can not disable plugins with enabled subordinate plugins
            else if (!enableAction && enabledSubordinates != null &&
                     enabledSubordinates.size() != 0) {
                
                String plugin = (String)enabledSubordinates.elementAt(0);
                DSUtil.showErrorDialog(null, "disable-dependency",
                                       new String[] {plugin}, _section);

                // restore the previous checkbox state
                checkBox.setSelected(true);
                setModelAt(_trueValue, 0);
                return DSEntry.DSE_VALID_NOMOD;
            }

            return DSEntry.DSE_VALID_MOD;
        }
    }
		
    private JPanel _attributesPanel;
    private JPanel _argumentsPanel;
    private JPanel _fieldsPanel;
    private JPanel _argumentsButtonsPanel;
 
    private JTextComponent _fieldWithFocus;

    private static final String ADD_ARGUMENT = "add argument";
    private static final String DELETE_ARGUMENT = "delete argument";
    
    private boolean _disableDocumentEvents = false;
    
    private Hashtable _htdsEntries = null;
    private Hashtable _htLabels = null; 
    private Hashtable _htArguments = null; 
    private Hashtable _htEnabled = null;

    private JButton _bAdd;
    private JButton _bDelete;

    private final String NO_ARGS = DSUtil._resource.getString(_section, "noargs-label");

    private LDAPEntry _entry;
    
    private ChangeListener _changeListener;

    private Hashtable _fields = null;
    private Hashtable _arguments = null;
    private Hashtable _dsEntryFields = null;

    private JCheckBox _cbEnabled = null;
    private EnableStateControl _dsEnableState = null;

    private static boolean EDITABLE_FIELDS = true;

    private final String[] EDITABLE_ATTRNAMES = {INITFUNC, PATH };
    private final String[] NON_EDITABLE_ATTRNAMES = { ID, DESCRIPTION, VERSION, VENDOR, TYPE};
    private final String[] ATTRNAMES = { ID, DESCRIPTION, VERSION, VENDOR, TYPE, INITFUNC, PATH};

	private final String[] NON_ENABLABLE_PLUGINS_INIT_FUNCTION = {
		"bin_init",
		"boolean_init",
		"ces_init",
		"cis_init",
//		"cos_init",      On request by bug #605550 COS can be disabled
		"country_init",
		"dn_init",
		"time_init",
		"int_init",
		"ldbm_back_init",
		"octetstring_init",
		"postal_init",
//		"roles_init",    On request by bug #605550 Roles can be disabled
		"tel_init",
		"uri_init",
		"des_init",
		"clear_pwd_storage_scheme_init",
		"crypt_pwd_storage_scheme_init",
		"ns_mta_md5_pwd_storage_scheme_init",
		"sha_pwd_storage_scheme_init",
		"ssha_pwd_storage_scheme_init"		
	};

    private static final String PATH = "nsslapd-pluginpath";
    private static final String ID = "nsslapd-pluginid";
    private static final String ENABLED = "nsslapd-pluginenabled";
    private static final String INITFUNC = "nsslapd-plugininitfunc";
    private static final String TYPE = "nsslapd-plugintype";
    private static final String VERSION = "nsslapd-pluginversion";
    private static final String VENDOR = "nsslapd-pluginvendor";
    private static final String DESCRIPTION = "nsslapd-plugindescription";
    private static final String ARG = "nsslapd-pluginarg";
    private static final String ARGS = "arguments";
    private static final String ATTRS = "attributes";
	private static final String DEPENDS_ON_NAMED = "nsslapd-plugin-depends-on-named";
	private static final String NAME_ATTR = "cn"; 
	private static final String ON = "on";
	private static final String OFF = "off";
    private static final String _section = "plugins";
	
}
