/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.Debug;
import netscape.ldap.*;
import com.netscape.admin.dirserv.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class MonitorPanel extends DSTabbedPanel {
	/**
	 * Standard tabbed pane constructor.
	 *
	 * @param model The resource model for this panel.
	 */
	public MonitorPanel(IDSModel model) {
		super( model, true );
		getOKButton().setVisible( false );
		getCancelButton().setVisible( false );
	}

    /**
      * Called when the TabbedPane is selected. Override DSTabbedPane.
	  *
	  * @param parent Tree node of this panel.
	  * @param viewInstance A selectable object, e.g. a menu item.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
		if ( !_monInitialized ) {

			int index = 0;

			/* Find out how many backends there are, and what their
			   names are */
			LDAPConnection ldc =
				getModel().getServerInfo().getLDAPConnection();
			try {
				LDAPEntry entry = ldc.read( "cn=monitor" );
				if ( entry == null ) {
					Debug.println( "MonitorPanel.select: unable to " +
										"read cn=monitor" );
					return;
				}
				addTab( new MonitorServerPanel( getModel() ) );
				_monInitialized = true;
				LDAPAttribute attr = entry.getAttribute( "backendmonitordn" );
				int nDatabases = attr.size();
				Enumeration en = attr.getStringValues();
				while( en.hasMoreElements() ) {
					MonitorDatabasePanel panel;
					String dn = (String)en.nextElement();
					if ( nDatabases > 1 )
						panel = new MonitorDatabasePanel( getModel(), dn );
					else
						panel = new MonitorDatabasePanel( getModel() );
					addTab( panel );
					index++;
					/* If there are multiple backends, give each one a
					   unique title */
					if ( nDatabases > 1 )
						_tabbedPane.setTitleAt( index, panel.getTitle() );
				}
			} catch ( LDAPException e ) {
				Debug.println( "MonitorPanel.select: " + e );
			}
		}

		_tabbedPane.setSelectedIndex( 0 );
		super.select( parent, viewInstance );
    }

    private boolean _monInitialized = false;
}
