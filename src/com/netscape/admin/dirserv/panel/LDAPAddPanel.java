/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.net.URL;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.border.EmptyBorder;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.LDAPUtil;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.*;
import netscape.ldap.*;
import netscape.ldap.util.*;
import java.text.SimpleDateFormat;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class LDAPAddPanel extends FilePanel {
	public LDAPAddPanel(IDSModel model) {
		super(model, "import", true, false);
		_helpToken = "configuration-database-import-ldap-dbox-help";
	}

	public void init() {
        JPanel grid = _myPanel;
        grid.setLayout( new GridBagLayout() );
		createFileArea( grid );
		createOptionsArea( grid );
		addBottomGlue();
		setLocalState( true );
		getAbstractDialog().setFocusComponent(_tfExport);
		getAbstractDialog().getAccessibleContext().setAccessibleDescription(_resource.getString(_section,
																								"description"));
	}

    private void createOptionsArea( JPanel grid ) {        
		GridBagConstraints gbc = getGBC();

		/* Options group */
        JPanel optionsPanel = new JPanel(new GridBagLayout());
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.NORTHWEST;
		gbc.weightx = 1.0;
		gbc.insets = getComponentInsets();
		grid.add(optionsPanel, gbc);			   
				
		_cbAddOnly = makeJCheckBox( _section, "addonly" );
		/* The factory method doesn't seem to pick up the default
		   state from the properties file */
		_cbAddOnly.setSelected( false );

		gbc.anchor = gbc.NORTHWEST;
		gbc.fill = gbc.NONE;
		gbc.weightx = 0;
		gbc.weighty = 0;
        gbc.gridwidth = gbc.REMAINDER;
		optionsPanel.add( _cbAddOnly, gbc );		

	    _cbContinuous = makeJCheckBox( _section, "continuous" );
		_cbContinuous.setSelected( true );
		gbc.gridwidth = gbc.REMAINDER;
        optionsPanel.add( _cbContinuous, gbc );	

		JLabel lblRejects = makeJLabel( _section, "rejects" );
        gbc.gridwidth = 4;
        optionsPanel.add( lblRejects, gbc );
        _tfRejects = makeJTextField( _section, "rejects", "", 30 );
		lblRejects.setLabelFor(_tfRejects);
		gbc.gridwidth = 3;
		optionsPanel.add( Box.createGlue(), gbc );
		gbc.fill = gbc.HORIZONTAL;
		
		gbc.weightx = 1.0;
        optionsPanel.add( _tfRejects, gbc );
		gbc.weightx = 0.0;
		_bRejects = makeJButton( "import", "browse-rejects-file" );
		gbc.fill = gbc.NONE;
        gbc.gridwidth = gbc.REMAINDER;
        optionsPanel.add(_bRejects,gbc);
	}

    protected void setLocalState( boolean state ) {
		if ( isLocal() ) {
			return;
		}
		super.setLocalState( state );
    }
    
    
    protected void checkOkay() {
	    String path = getFilename();
		String rejects = _tfRejects.getText().trim();
		AbstractDialog dlg = getAbstractDialog();
		if ( dlg != null ) {
			boolean state = false;
			if (path != null) {
				if (path.length() > 0) {
					state = true;
					if (rejects != null) {
						File rejectsFile = new File(rejects);
						File pathFile = new File(path);
						if (rejectsFile.equals(pathFile)) {
							state = false;
						}
					}
				}
			} 
			dlg.setOKButtonEnabled( state );
		} else {
		}
	}

    public void changedUpdate(DocumentEvent e) {
		super.changedUpdate( e );
		checkOkay();
	}

    public void removeUpdate(DocumentEvent e) {
		super.removeUpdate( e );
		checkOkay();
	}

    public void insertUpdate(DocumentEvent e) {
		super.insertUpdate( e );
		checkOkay();
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if ( e.getSource().equals(_bExport) ) {
    	    String file = getFilename();
			String[] extensions = { "ldif" };
			String[] descriptions = { _resource.getString( "filefilter", "ldif-label" ) };
			String defaultPath = getDefaultPath(getModel());
			if ( (file == null) || (file.trim().length() < 1) ) {				
				file = DSFileDialog.getFileName(false, extensions,
												descriptions, (Component)this, "*.ldif", defaultPath);
			} else {
				File theFile = new File(file);
				if (theFile.isAbsolute()) {
					file = DSFileDialog.getFileName(file, false, extensions,
													descriptions, (Component)this);
				} else {
					file = DSFileDialog.getFileName(false, extensions,
												descriptions, (Component)this, file, defaultPath);
				}
			}
			if (file != null)
				setFilename(file);
        } else if ( e.getSource().equals(_bRejects) ) {
    	    String file = _tfRejects.getText();
			String defaultPath = getDefaultPath(getModel());
			if ( (file == null) || (file.trim().length() < 1) ) {
				file = DSFileDialog.getFileName(false, null,
												null, (Component)this, "rejects", defaultPath);
			} else {
				File theFile = new File(file);
				if (theFile.isAbsolute()) {
					file = DSFileDialog.getFileName(file, false, (Component)this);
				} else {
					file = DSFileDialog.getFileName(false, null,
													null, (Component)this, file, defaultPath);
				}
			}
    	    if (file != null) {
    	        _tfRejects.setText(file);
			}
        } else {
			super.actionPerformed(e);
		}
    }

    public void okCallback() {
    	boolean status = false;

	    String importPath = getFilename();

		/*We only test if the file name is not empty*/
		if (importPath.trim().equals("")) {
			if (!validateFilename())
				return;
		}

		String base = null;
		ConsoleInfo info = getModel().getServerInfo();
        LDAPConnection ldc = info.getLDAPConnection();
	    try {
			File importPathFile = new File( importPath );
			/* If no path was provided, we try first with the path of the DSFileDialog and then with the default path of the console */				
			if ( !importPathFile.isAbsolute() ) {
				if (DSFileDialog.getPath() != null) {
					importPath = DSFileDialog.getPath() + importPath;
				} else {
					importPath = getDefaultPath(getModel()) + importPath;
				}
			}
			/* Check if the file can be read */
			importPathFile = new File( importPath );			
			try {
				FileInputStream test = new FileInputStream(importPathFile);
			} catch (Exception e) {
				DSUtil.showErrorDialog( getModel().getFrame(),
										"cantopen", importPath, _section );
				return;
			}

			importPath = importPathFile.getCanonicalPath();
			
			String rejects = _tfRejects.getText();
			if ( rejects.length() < 1 ) {
				rejects = null;
			} else {
				File rejectsFile = new File( rejects );
				/* If no path was provided, we try first with the path of the DSFileDialog and then with the default path of the console */				
				if ( !rejectsFile.isAbsolute() ) {
					if (DSFileDialog.getPath() != null) {
						rejects = DSFileDialog.getPath() + rejects;
					} else {					
						rejects = getDefaultPath(getModel()) + rejects;
					}
				}
				rejectsFile = new File( rejects );
				
				/* Check if the rejects file AND the import file are the same */
				if (rejectsFile.equals(importPathFile)) {
					DSUtil.showErrorDialog( getModel().getFrame(),
											"rejectsfileisimportfile", 
											"",
											_section );
					return;
				}

				if (DSUtil.fileExists(rejectsFile)) {
					int response = DSUtil.showConfirmationDialog(getModel().getFrame(),
																 "confirm-delete-reject-file",
																 DSUtil.inverseAbreviateString(rejects, 30),
																 _section );
					if ( response != JOptionPane.YES_OPTION ) {
						return;
					}
				}
				
				
				/* Check if we can write in the rejects file */				
				try {
					FileOutputStream test =new FileOutputStream( rejectsFile );
				} catch (Exception e) {
					String[] args = {DSUtil.inverseAbreviateString(rejects, 40)};
					DSUtil.showErrorDialog( getModel().getFrame(),
											"unwritable-check", args,
											_section );
					return;
				}				
			}
			boolean addOnly = _cbAddOnly.isSelected();
			boolean continuous = _cbContinuous.isSelected();			
			DSExportImport ds = new DSExportImport( ldc,
													importPath,
													addOnly,
													continuous,
													rejects );
			String title = _resource.getString("import", "title");
			LDAPAddProgressDialog dlg = new LDAPAddProgressDialog(getModel().getFrame(),																  
																  title,
																  this,
																  ds,
																  rejects);
			ds.addEntryChangeListener( dlg );
			hideDialog();
			try {
				Thread th = new Thread(ds);
				th.start();			
			} catch ( Exception e ) {
				Debug.println( "LDAPAddPanel.okCallback: " +
							   e );
				e.printStackTrace();
				return;
			}			
			dlg.packAndShow();
			status = ds.getStatus();
			
			/* We display a result message if the operation has been cancelled */
			if (dlg.hasBeenCancelled()) {
				if ( status ) {
					String[] args = { Integer.toString(ds.getEntryCount()),
									  Integer.toString(ds.getRejectCount()) };
					DSUtil.showErrorDialog( getModel().getFrame(),
											"succeeded", args,
											_section );
				} else {					
					if ( ds.getError() == ds.STATUS_UNWRITABLE ) {
						String[] args = {DSUtil.inverseAbreviateString(rejects, 40),
										 Integer.toString(ds.STATUS_UNWRITABLE)};
						DSUtil.showErrorDialog( getModel().getFrame(),
												"unwritable", args,
												_section );

					} else if (ds.getError() == ds.LDIF_SYNTAX_ERROR) {
						String[] args = {DSUtil.abreviateString(ds.getLastDN(), 30),
										 Integer.toString(ds.LDIF_SYNTAX_ERROR)};
						DSUtil.showErrorDialog( getModel().getFrame(), "BulkAddError-ldifSyntaxError", args, "general" );

					} else if (ds.getError() == ds.MALFORMED_EXPRESSION_ERROR) {
						int position = ds.getEntryCount() + ds.getRejectCount() +1;
						String lastDN = ds.getLastDN();
						
						if (lastDN == null) {
							String[] args = {Integer.toString(position),
											 Integer.toString(ds.MALFORMED_EXPRESSION_ERROR)};
							DSUtil.showErrorDialog( getModel().getFrame(),
													"malformed-expression-error-number", 
													args,
													_section  );
						} else {
							String[] args = {DSUtil.abreviateString(lastDN, 30),
											 Integer.toString(ds.MALFORMED_EXPRESSION_ERROR)};
							DSUtil.showErrorDialog( getModel().getFrame(),
													"malformed-expression-error-dn", 
													args,
													_section  );
						}				
					} else {
						DSUtil.showErrorDialog( getModel().getFrame(),
												"failed", Integer.toString(ds.getError()) , _section );
					}
				}							   
			}
		} catch ( Exception e ) {
			Debug.println( "LDAPAddPanel.okCallback: import " +
						   "failed - " + e );
			e.printStackTrace();
		}		
		if ( status ) {
			getModel().contentChanged();
		}

		/* No state to preserve */
		clearDirtyFlag();				
    }

    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

	class LDAPAddProgressDialog extends GenericProgressDialog implements IEntryChangeListener, ActionListener {
		public LDAPAddProgressDialog(JFrame parent,									 
									 String title, 
									 Component comp,
									 DSExportImport ds,
									 String rejects) {
			super(parent, true, TEXT_FIELD_AND_CANCEL_BUTTON_OPTION, title, comp);
			addActionListener(this);
			setLabelRows(2);
			setTextInTextAreaLabel(_resource.getString(_section,"rejects-progressdialog-label"));
			_ds = ds;
			_rejects = rejects;
		}
			
		/**
		 * Called when an entry changes on import. Return
		 * true to continue, false to stop.
		 */
		public boolean entryChanged( String dn, String msg ) {
			if ( dn == null ) {
				_done = true;
			}
			/* A message to display */
			if ( _continue && (dn != null) ) {
				if ((_ds.getCurrentEntry() % 5) == 0) {
					String[] args = {String.valueOf(_ds.getCurrentEntry()),
									 DSUtil.abreviateString(dn, 60)};				
					setTextInLabel( _resource.getString(_section,
														"addingentrynumber-label",
														args));				
				}
				if (msg != null) {
					if (!dn.trim().equals("")) {
						appendTextToTextArea(dn+": "+msg+"\n");
					} else {
						appendTextToTextArea(msg+"\n");
					}
				}
				return true;
				/* Import is over*/
			} else if (_continue ) {
				boolean status = _ds.getStatus();
				if ( status ) {
					String[] args = { Integer.toString(_ds.getEntryCount()),
									  Integer.toString(_ds.getRejectCount()) };					
					setTextInLabel( _resource.getString(_section,
														"succeeded-msg", 
														args));					
				} else {
					if ( _ds.getError() == _ds.STATUS_UNWRITABLE ) {
						String[] args = {_rejects,
										 Integer.toString(_ds.STATUS_UNWRITABLE)};
						setTextInLabel( _resource.getString(_section,
															"unwritable-msg",
															args));
					} else if (_ds.getError() == _ds.LDIF_SYNTAX_ERROR) {
						String[] args = {DSUtil.abreviateString(_ds.getLastDN(), 45),
										 Integer.toString(_ds.LDIF_SYNTAX_ERROR)};
						setTextInLabel( _resource.getString( "general" ,
															 "BulkAddError-ldifSyntaxError-msg", 
															 args));	
					} else if (_ds.getError() == _ds.MALFORMED_EXPRESSION_ERROR) {
						int position = _ds.getEntryCount() + _ds.getRejectCount() +1;
						String lastDN = _ds.getLastDN();
						
						if (lastDN == null) {
							String[] args = {Integer.toString(position),
											 Integer.toString(_ds.MALFORMED_EXPRESSION_ERROR)};
							setTextInLabel( _resource.getString(_section, 
																"malformed-expression-error-number-msg", 
																args));
						} else {
							String[] args = {DSUtil.abreviateString(lastDN, 45),
											 Integer.toString(_ds.MALFORMED_EXPRESSION_ERROR)};
							setTextInLabel( _resource.getString(_section,
																"malformed-expression-error-dn-msg", 
																args));
						}				
					} else {
						String[] args = {Integer.toString(_ds.getError())};
						setTextInLabel( _resource.getString(_section,
															"failed-msg",
															args));
					}
				}
				waitForClose();			
				return false;
				/* Operation cancelled */
			} else  {
				closeCallBack();	   
			}
			return false;			
		}
		
		public boolean entryChanged( String dn ) {
			return entryChanged( dn, null );
		}
		
		/**
		 *	Handle incoming event from button.
		 *
		 * @param e event
		 */
		public void actionPerformed(ActionEvent e) {
			if ( e.getActionCommand().equals( GenericProgressDialog.CANCEL ))  {
				_continue = false;
				if ( _done )  {
					closeCallBack();
				}					
			} else if (e.getActionCommand().equals( GenericProgressDialog.CLOSE )) {
				closeCallBack();
			}
		}

		public boolean hasBeenCancelled() {
			return !_continue;
		}
		
		private String _rejects;
		private DSExportImport _ds;
		private boolean _continue = true;
		private boolean _done = false;		
	}

	private JCheckBox _cbContinuous;
	private JCheckBox _cbAddOnly;
	private JLabel _lblRejects;
	private JButton _bRejects;
	private JTextField _tfRejects;	
    private static final String _section = "import";
}
