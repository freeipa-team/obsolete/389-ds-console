/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.panel;

import javax.swing.*;
import java.util.*;
import com.netscape.admin.dirserv.DSUtil;

public abstract class DSEntryTextArea extends DSEntryText {
	public DSEntryTextArea(String model, JTextArea view1, 
						   JComponent view2){
		super (model, view1, view2);
	}

	/**
	 * displays the data stored in the entry
	 */
	public void show() {
		// update the view
		JTextArea ta = (JTextArea)getView(0);
		// Convert array of strings into newline-delimited
		// single string
		String val = "";
		int i = 0;
		while( i < getModelSize() ) {
			val += getModel(i);
			if (i < (getModelSize()-1) ) {
				val += "\n";
			}
			i++;
		}
		ta.setText(val);

		viewInitialized ();
	}

	private Vector parse() {
		// Convert newline-delimited single string into array
		// of strings
		JTextArea ta = (JTextArea)getView(0);
		String val = ta.getText().trim();
		StringTokenizer st = new StringTokenizer( val, "\n" );
		Vector v = new Vector();
		while( st.hasMoreTokens() ) {
			String s = st.nextToken();
			if ( s.length() > 0 )
				v.addElement(s);
		}

		return v;
	}

	/**
	 * performs dynamic update of model when input data changes
	 * 
	 */
	protected void updateModel() {
		// Convert newline-delimited single string into array
		// of strings
		Vector v = parse();
		clearModel();

		/* model needs to contain an empty element for 
		   changes to be sent to the server */
		if (v.size() == 0){
			setModelAt ("", 0);
			return;
		}

		for( int i = 0; i < v.size(); i++ ) {
			setModelAt((String)v.elementAt(i), i);
		}
	}

	/**
	 * called when the data is about to be saved to the server
	 * to update the model based on the input data
	 * left empty because of dynamic update performed by 
	 * updateModel
	 */
	public void store (){
	}

	/**
	 * performs dynamic input data validation when it is changed
	 */
	public int validate() {
        JTextArea ta = (JTextArea)getView(0);

		/* disabled fields are always ok. */
		if (!ta.isEnabled ())
			return 0;

		Vector v = parse();
		for( int i = 0; i < v.size(); i++ ) {
			String s = (String)v.elementAt(i);

			if (!lineIsValid(s))
				return 1;
		}

		return 0;
	}

	/**
	 * This function is provided by the subclass.  It is given a line of
	 * input from the text area and it returns true if the line is a valid
	 * representation of whatever is being represented e.g. a DN, an LDAP
	 * URL, etc.
	 */
	abstract protected boolean lineIsValid(String line);
}
