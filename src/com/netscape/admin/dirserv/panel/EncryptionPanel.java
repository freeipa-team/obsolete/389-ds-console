/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import com.netscape.admin.dirserv.DSAdmin;
import com.netscape.admin.dirserv.DSAdminEvent;
import com.netscape.admin.dirserv.IDSAdminEventListener;
import com.netscape.admin.dirserv.DSFramework;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.GlobalConstants;
import com.netscape.management.client.security.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Help;
import com.netscape.management.client.util.Debug;
import com.netscape.management.nmclf.SuiConstants;
import netscape.ldap.*;

/**
 * This panel displays configuration data for the encryption.
 * It embeds the standard EncryptionPane from the console sdk.
 */
public class EncryptionPanel extends BlankPanel 
                             implements EncryptionOptions,
                                        IDSAdminEventListener,
							            SuiConstants {

	public EncryptionPanel(IDSModel model) {
		super(model, "encryption" );
		_helpToken = "configuration-system-encryption-help";
		_refreshWhenSelect = false;
	}


	/**
	 * Overrides BlankPanel.
	 */
	public void init() {
		if (_isInitialized) {
			return;
		}
		DSResourceModel model = (DSResourceModel)getModel();
		ConsoleInfo serverInfo = model.getServerInfo();
		ConsoleInfo consoleInfo = model.getConsoleInfo();

		// Listen to DSAdminEvent
		if (model.getSelectedPage() != null) {
			DSFramework fmk = (DSFramework)model.getSelectedPage().getFramework();
			fmk.getServerObject().addDSAdminEventListener(this);
		}
		else {
			Debug.println(0, "EncryptionPanel.init: can't listen to DSAdminEvent");
		}
		
		// Read config data from the directory
		// This must be done before instanciating security.EncryptionPanel.
		try {
			LDAPConnection ldc = serverInfo.getLDAPConnection();
			LDAPConnection sieldc = consoleInfo.getLDAPConnection();
			String sieDn = consoleInfo.getCurrentDN();
			_configData = new EncryptionConfigData();
			_configData.readFromDirectory(ldc, sieldc, sieDn);
		}
		catch(LDAPException x) {
			Debug.println(0, "EncryptionPanel.init: failure while reading config data");
			if (Debug.getTrace()) {
				x.printStackTrace();
			}
		}
		
		// Creates the components
		_encrypt = new com.netscape.management.client.security.EncryptionPanel( 
			consoleInfo , (String)serverInfo.get("SIE"), this);
		_encryptParent = new JPanel();
		_rbOff = makeJRadioButton(_section, "clientAuth-off", true);
		_rbAllowed = makeJRadioButton(_section, "clientAuth-allowed");
		_rbRequired = makeJRadioButton(_section, "clientAuth-required");
		_cbConsoleSSL = makeJCheckBox(_section, "consoleSSL" );
		_cbSSLCheckHostName = makeJCheckBox(_section, "SSLCheckHostName" );
		layoutComponents();

		/* initialize DSEntries - they will make sure that controls get
		   correct coloring when modified. Since some of the controls
		   are not ours, this will not help with OK and Reset buttons   */

		_offDSEntry = new DSEntryBoolean ("off", _rbOff);
		setComponentTable(_rbOff, _offDSEntry);

		_allowDSEntry = new DSEntryBoolean ("off", _rbAllowed);
		setComponentTable(_rbAllowed, _allowDSEntry);

		_requireDSEntry = new DSEntryBoolean ("off", _rbRequired);
		setComponentTable(_rbRequired, _requireDSEntry);

		_consoleSSLDSEntry = new DSEntryBoolean ("off", _cbConsoleSSL);
		setComponentTable(_cbConsoleSSL, _consoleSSLDSEntry);

		_SSLCheckHostNameDSEntry = new DSEntryBoolean ("off", _cbSSLCheckHostName);
		setComponentTable(_cbSSLCheckHostName, _SSLCheckHostNameDSEntry);


		// And align the components on the config data
		switch(_configData.clientAuth) {
			case EncryptionConfigData.CLIENT_AUTH_ALLOWED:
				_offDSEntry.fakeInitModel ("off");
				_allowDSEntry.fakeInitModel ("on");
				_requireDSEntry.fakeInitModel ("off");
				break;
			case EncryptionConfigData.CLIENT_AUTH_REQUIRED:
				_offDSEntry.fakeInitModel ("off");
				_allowDSEntry.fakeInitModel ("off");
				_requireDSEntry.fakeInitModel ("on");
				break;
			case EncryptionConfigData.CLIENT_AUTH_DISABLED:
			default:
				_offDSEntry.fakeInitModel ("on");
				_allowDSEntry.fakeInitModel ("off");
				_requireDSEntry.fakeInitModel ("off");
				break;
		}
		_consoleSSLDSEntry.fakeInitModel (_configData.sslConsoleOn ? "on" : "off");
		_SSLCheckHostNameDSEntry.fakeInitModel (_configData.sslCheckHostName ? "on" : "off");
		showAll();
		
		// Enable/disable the switches
		updateComponentState();						
		_isInitialized = true;
	}


	/**
	 * Layout the components.
	 */
	void layoutComponents() {
    	GridBagLayout gbl = new GridBagLayout();
        _myPanel.setLayout(gbl);
        
		_encryptParent.setLayout(new GridLayout());
		_encryptParent.add("Center", _encrypt);
		
        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(COMPONENT_SPACE, COMPONENT_SPACE, 0, COMPONENT_SPACE);
        gbc.ipadx = 0;
        gbc.ipady = 0;
        
		int magic = 2;
		gbc.insets.left += magic; // _encrypt has a different internal margin, apparently
		gbc.insets.right += magic;
        _myPanel.add(_encryptParent, gbc);
	
		JPanel panel = new GroupPanel(_resource.getString(
			_section, "clientAuth-title" ), true);
		int space = UIFactory.getComponentSpace();
		GridBagLayout cli_gbl = new GridBagLayout();
		panel.setLayout( cli_gbl ); //(4, 1, space, space/2) );

		GridBagConstraints cli_gbc = new GridBagConstraints() ;
		cli_gbc.gridx      = 0;
		cli_gbc.gridy      = GridBagConstraints.RELATIVE;
		cli_gbc.gridwidth  = 1;
		cli_gbc.gridheight = 1;
		cli_gbc.weightx    = 1;
		cli_gbc.weighty    = 0;
		cli_gbc.fill       = gbc.NONE;
		cli_gbc.anchor     = gbc.NORTHWEST;
		cli_gbc.ipadx = 0;
		cli_gbc.ipady = 0;

		panel.add( _rbOff, cli_gbc );
		panel.add( _rbAllowed, cli_gbc );
		panel.add( _rbRequired, cli_gbc );
		panel.add( _cbConsoleSSL, cli_gbc );

		gbc.gridy++;
		gbc.insets.left -= magic;
		gbc.insets.right -= magic;
		_myPanel.add( panel ,gbc);

		gbc.gridy++;
		gbc.fill = gbc.NONE;
		_myPanel.add( _cbSSLCheckHostName, gbc );
		gbc.fill = gbc.HORIZONTAL;
		
		Component glue = Box.createVerticalGlue();
		gbc.gridy++;
        gbc.weighty = 1;
		_myPanel.add( glue, gbc);

		ButtonGroup group = new ButtonGroup();
		group.add(_rbOff);
		group.add(_rbAllowed);
		group.add(_rbRequired);
	}
	

	/**
	 * Overrides BlankPanel.
	 */
	public void okCallback() {
		DSFramework fmk = (DSFramework)getModel().getSelectedPage().getFramework();
		JFrame frame = fmk.getJFrame();
		
		// Is everything ok ?
		if (!validateEntries()) return;
		
		// Ok, let's commit everything
		super.okCallback();
		try {
			ConsoleInfo serverInfo = getModel().getServerInfo();
			ConsoleInfo consoleInfo = getModel().getConsoleInfo();

			// Align _configData on the components
			if (_rbOff.isSelected()) {
				_configData.clientAuth = _configData.CLIENT_AUTH_DISABLED;
			}
			else if (_rbAllowed.isSelected()) {
				_configData.clientAuth = _configData.CLIENT_AUTH_ALLOWED;
			}
			else if (_rbRequired.isSelected()) {
				_configData.clientAuth = _configData.CLIENT_AUTH_REQUIRED;
			}
			_configData.sslConsoleOn = _configData.sslServerOn && _cbConsoleSSL.isSelected();
			_configData.sslCheckHostName = _cbSSLCheckHostName.isSelected();

			// Write the config data to the directories
			LDAPConnection ldc = serverInfo.getLDAPConnection();
			LDAPConnection sieldc = consoleInfo.getLDAPConnection();
			String sieDn = consoleInfo.getCurrentDN();
			_configData.writeToDirectory(ldc, sieldc, sieDn);

			// Record the current state of the server, for possible restart
			if (_configData.sslServerOn)
				fmk.getServerObject().setSecurityState(DSAdmin.SECURITY_ENABLE);
			else
				fmk.getServerObject().setSecurityState(DSAdmin.SECURITY_DISABLE);

			// Update the components
			_encrypt.setSaved();
			setDirty(false);
			resetAll();

			// Warn the user he needs to restart the server
			DSUtil.showInformationDialog(frame, "requires-restart", (String)null);
			
			// If they are enabling encryption, warn them to trust the CA chain too
			if (_configData.sslServerOn) {
				DSUtil.showInformationDialog(frame, "warn-ca-chain-trust", (String)null, _section);
			}

			/* Warn the user: port below 1024 require root install on unix */
			if (_configData.sslServerOn && !DSUtil.isNT(consoleInfo)) {
				PortConfigData portConfig = new PortConfigData();
				try {
					portConfig.readFromDirectory(ldc);
					if ((portConfig.port >= 1024) && (portConfig.securePort < 1024)) {
							DSUtil.showInformationDialog(
								frame,
								"confirm-sslport",
								Integer.toString(portConfig.securePort),
								_section );
					}
				}
				catch(LDAPException x) {
					Debug.println(0, "EncryptionPanel.okCallback: caught " + x);
					Debug.println(0, "EncryptionPanel.okCallback: skipping the SSL port checking");
				}
			}

		} catch (LDAPException e) {
			Debug.println ("LDAPEncryptionPanel.okCallback: exception " + e );
			if (Debug.getTrace()) 
				e.printStackTrace();
			DSUtil.showLDAPErrorDialog(frame, e, "111-title");
		}
	}


	/**
	 * Overrides BlankPanel.
	 */
	public void resetCallback() {
		ConsoleInfo serverInfo = getModel().getServerInfo();
		ConsoleInfo consoleInfo = getModel().getConsoleInfo();

		// Re-read config data from the directory
		try {
			LDAPConnection ldc = serverInfo.getLDAPConnection();
			LDAPConnection sieldc = consoleInfo.getLDAPConnection();
			String sieDn = consoleInfo.getCurrentDN();
			_configData.readFromDirectory(ldc, sieldc, sieDn);
		}
		catch(LDAPException x) {
			Debug.println(0, "EncryptionPanel.resetCallback: failure while reading config data");
			if (Debug.getTrace()) {
				x.printStackTrace();
			}
		}
		
		// Realign the components
		_encrypt.reset();
		super.resetCallback();
		updateComponentState();
	}

	/**
	 * Overrides BlankPanel.
	 */
	public boolean refresh() {

		// Skip the first refresh because it's unnecessary;
		// Recreation of _encrypt is very time consuming
		if (_firstRefresh) {
			_firstRefresh = false;
			return super.refresh();
		}

		ConsoleInfo serverInfo = getModel().getServerInfo();
		ConsoleInfo consoleInfo = getModel().getConsoleInfo();

		// Re-read config data from the directory
		try {
			LDAPConnection ldc = serverInfo.getLDAPConnection();
			LDAPConnection sieldc = consoleInfo.getLDAPConnection();
			String sieDn = consoleInfo.getCurrentDN();
			_configData.readFromDirectory(ldc, sieldc, sieDn);
		}
		catch(LDAPException x) {
			Debug.println(0, "EncryptionPanel.resetCallback: failure while reading config data");
			if (Debug.getTrace()) {
				x.printStackTrace();
			}
		}
		
		// Re-create _encrypt
		_encryptParent.remove(_encrypt);
		_encrypt = new com.netscape.management.client.security.EncryptionPanel( 
			consoleInfo , (String)serverInfo.get("SIE"), this);
		_encryptParent.add("Center", _encrypt);
		_encryptParent.revalidate();
		
		return super.refresh();
	}

	/**
	 * Override BlankPanel.
	 * WARNING : this extra call to setDirty() is required to solve
	 * the following problem:
	 * When our DSEntry members (_offDSEntry, _allowDSEntry...) are unmodified, 
	 * this switch of the dirty flag even if _encrypt is modified.
	 */
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		setDirty(true);
	}

	/**
	 * Extends BlankPanel.
	 * Here we detect if the user requires 'client auth'
	 * and we disable the usage of SSL in the console.
	 */
    public void itemStateChanged(ItemEvent e) {
		super.itemStateChanged(e);
		if (e.getSource() == _rbRequired) {
			boolean noConsoleSSL = _rbRequired.isSelected();
			_cbConsoleSSL.setEnabled( ! noConsoleSSL );
			if (noConsoleSSL && _cbConsoleSSL.isSelected()) {
				_cbConsoleSSL.setSelected(false);
				DSUtil.showInformationDialog(
					getModel().getFrame(),
					"confirm-clientauth",
					"",
					_section );
				
			}
		}
    }


	/**
	 * Implements EncryptionOptions
	 */
	public void showCipherPreferenceDialog(String cipher) {
		Frame f = getModel().getFrame();
		CipherPreferenceDialog dlg;

		Debug.println(4, "EncryptionPanel.showCipherPreferenceDialog: cipher = " + cipher);
		Debug.println(4, "EncryptionPanel.showCipherPreferenceDialog: TLS_OFF = " + _configData.CIPHER_TLS_OFF);
		dlg = new CipherPreferenceDialog(f, null, _configData.CIPHER_SSL3_OFF, _configData.CIPHER_TLS_OFF);
		dlg.setCipherEnabled(dlg.SSL_V3, _configData.cipherPrefs);
		dlg.setCipherEnabled(dlg.SSL_TLS, _configData.cipherPrefs);
		dlg.showModal();
		if (!dlg.isCancel()) {
			_configData.cipherPrefs = dlg.getCipherPreference(dlg.SSL_V3) + "," + 
			                          dlg.getCipherPreference(dlg.SSL_TLS);
			setDirty(true);
			Debug.println("EncryptionPanel.showCipherPreferenceDialog: preferences = "+_configData.cipherPrefs);
		}
	}


	/**
	 * Implements EncryptionOptions
	 */
	public boolean isSecurityEnabled() {
		Debug.println(4, 
			"EncryptionPanel.isSecurityEnabled: _configData.sslServerOn = " + _configData.sslServerOn);
		return _configData.sslServerOn;
	}


	/**
	 * Implements EncryptionOptions
	 */
	public void securityEnabledChanged(boolean enable) {
		Debug.println(4, 
			"EncryptionPanel.securityEnabledChanged: enable = " + enable);
		_configData.sslServerOn = enable;
		_configData.sslConsoleOn = enable;

		// We must enable/disable the components accordingly.
		updateComponentState();
		setDirty(true);
	}


	/**
	 * Implements EncryptionOptions
	 */
	public boolean isCipherFamilyEnabled(String cipher) {
		Debug.println(4, 
			"EncryptionPanel.isCipherFamilyEnabled: cipher = " + cipher);
		CipherSetup setup = (CipherSetup)_configData.cipherSetupTable.get(cipher);
		return ((setup != null) && (setup.enabled));
	}


	/**
	 * Implements EncryptionOptions
	 */
	public void cipherFamilyEnabledChanged(String cipher, boolean enabled) {
		Debug.println(4, 
			"EncryptionPanel.cipherFamilyEnabledChanged: cipher = " + cipher +
			", enabled = " + enabled);
		CipherSetup setup = findOrCreateCipherSetup(cipher);
		setup.enabled = enabled;
		setDirty(true);
	}


	/**
	 * Implements EncryptionOptions
	 */
	public String getSelectedCertificate(String cipher) {
		Debug.println(4, 
			"EncryptionPanel.getSelectedCertificate: cipher = " + cipher);
		CipherSetup setup = (CipherSetup)_configData.cipherSetupTable.get(cipher);
		return (setup == null) ? null : setup.selectedCertificate;
	}


	/**
	 * Implements EncryptionOptions
	 */
	public void selectedCertificateChanged(String cipher, String cert) {
		Debug.println(4, 
			"EncryptionPanel.selectedCertificateChanged: cipher = " + cipher +
			", cert = " + cert);
		CipherSetup setup = findOrCreateCipherSetup(cipher);
		setup.selectedCertificate = cert;
		setDirty(true);
	}


	/**
	 * Implements EncryptionOptions
	 */
	public String getSelectedDevice(String cipher) {
		Debug.println(4, 
			"EncryptionPanel.getSelectedDevice: cipher = " + cipher);
		CipherSetup setup = (CipherSetup)_configData.cipherSetupTable.get(cipher);
		return (setup == null) ? null : setup.selectedDevice;
	}

	/**
	 * Implements EncryptionOptions
	 */
	public void selectedDeviceChanged(String cipher, String dev) {
		Debug.println(4, 
			"EncryptionPanel.selectedDeviceChanged: cipher = " + cipher +
			", dev = " + dev);
		CipherSetup setup = findOrCreateCipherSetup(cipher);
		setup.selectedDevice = dev;
		setDirty(true);
	}


	/**
	 * Implements EncryptionOptions
	 */
	public void setSecurityIsDomestic(boolean yes) {
		Debug.println(4, 
			"EncryptionPanel.setSecurityIsDomestic: yes = " + yes);
		_securityIsDomestic = yes;
	}


	/**
	 * Implements IDSAdminEventListener.
	 * When we receive DSAdminEvent.IMPORT_CRETIFICATE,
	 * we refresh the panel.
	 */
	public void processDSAdminEvent(DSAdminEvent e) {
		refresh();
	}
	
	/**
	 * Validates the user choices
	 */
	private void setDirty(boolean state) {
		if (state) {
			setDirtyFlag();
			setValidFlag();
		}
		else {
			clearDirtyFlag();
		}
	}


	/**
	 * Validates the user choices
	 */
	private boolean validateEntries() {
		String error = null;
		
		// Update _configData.clientAuth from radio buttons
		if (_rbOff.isSelected())
			_configData.clientAuth = _configData.CLIENT_AUTH_DISABLED;
		else if (_rbAllowed.isSelected()) 
			_configData.clientAuth = _configData.CLIENT_AUTH_ALLOWED;
		else if (_rbRequired.isSelected()) 
			_configData.clientAuth = _configData.CLIENT_AUTH_REQUIRED;
		
		if (_configData.sslServerOn) {
		
			// Count the problems
			int howManyEnabled = 0;
			int howManyNoDevice = 0;
			int howManyNoCertif = 0;
			Enumeration e = _configData.cipherSetupTable.keys();
			while (e.hasMoreElements()) {
				Object k = e.nextElement();
				CipherSetup setup = (CipherSetup)_configData.cipherSetupTable.get(k);
				if (setup.enabled) {
					howManyEnabled++;
					if (setup.selectedDevice.length() == 0) howManyNoDevice++;
					if (setup.selectedCertificate.length() == 0) howManyNoCertif++;
				}
			}
			// Select the error message according the problem
			if (howManyEnabled == 0) {
				error = "nosslpreferences";
			}
			else if (howManyNoDevice >= 1) {
				error = "nosslfamily";
			}
			else if (howManyNoCertif >= 1) {
				error = "nocertificate";
			}
			
		}
		
		if (error != null) {
			DSUtil.showErrorDialog(getModel().getFrame(), error, "", _section);
		}
		return (error == null);
	}


	/**
	 * Update components according the state variables
	 */
	private void updateComponentState() {
	
		// Disable components when security is off.
		_rbOff.setEnabled(_configData.sslServerOn);
		_rbAllowed.setEnabled(_configData.sslServerOn);
		_rbRequired.setEnabled(_configData.sslServerOn);
		_cbConsoleSSL.setEnabled(_configData.sslServerOn && !_rbRequired.isSelected());
		_cbSSLCheckHostName.setEnabled(_configData.sslServerOn);

		// Get secure port
		DSResourceModel model = (DSResourceModel)getModel();
		ConsoleInfo serverInfo = model.getServerInfo();
		LDAPConnection ldc = serverInfo.getLDAPConnection();
		int securePort = -1; 
		// Read cn=config
		String configDn = "cn=config";
		String[] configAttrs = { "nsslapd-secureport" };
		LDAPEntry configEntry = null;
		try {
		    configEntry = ldc.read(configDn, configAttrs);
		}
		catch(LDAPException x) {
		    Debug.println(0, 
		        "EncryptionPanel.updateComponentState: failure while reading config data");
		    if (Debug.getTrace()) {
		        x.printStackTrace();
		    }
		}

		// Parse configEntry
		if (configEntry != null) {
		    String securePortStr = DSUtil.getAttrValue(configEntry, configAttrs[0]);
		    try {
		        securePort = Integer.parseInt(securePortStr);
		    }
		    catch(NumberFormatException x) {
		        Debug.println(0, 
		            "EncryptionPanel.updateComponentState: cannot convert nsslapd-secureport to an int !");
		    }

		    // If securePort is not a default value, set it to NetscapeRoot
		    if (636 != securePort) {
		        ConsoleInfo consoleInfo = model.getConsoleInfo();
		        LDAPConnection sieldc = consoleInfo.getLDAPConnection();
		        String sieDn = consoleInfo.getCurrentDN();
		        LDAPAttribute attr = 
		                 new LDAPAttribute("nssecureserverport", securePortStr);
		        try {
		            sieldc.modify(sieDn, 
		                 new LDAPModification(LDAPModification.REPLACE, attr));
		        }
		        catch(LDAPException xx) {
		            Debug.println(0, 
		                "EncryptionPanel.updateComponentState: failure while modifying console config data");
		            if (Debug.getTrace()) {
		                xx.printStackTrace();
		            }
		        }
		    }
		}
	}
	
	
	/**
	 * Searches for a CipherSetup in _cipherSetupTable.
	 * If it's not found, creates it.
	 */
	private CipherSetup findOrCreateCipherSetup(String s) {
		CipherSetup setup = (CipherSetup)_configData.cipherSetupTable.get(s);
		if (setup == null) {
			setup = new CipherSetup();
			setup.family = s;
			_configData.cipherSetupTable.put(setup.family, setup);
		}
		
		return setup;
	}



	// State variables
	private EncryptionConfigData _configData;
	private boolean _securityIsDomestic;
	private DSEntryBoolean _offDSEntry, _allowDSEntry, _requireDSEntry;
	private DSEntryBoolean _consoleSSLDSEntry;
	private DSEntryBoolean _SSLCheckHostNameDSEntry;
	private boolean _firstRefresh = true;


	
	// Components
	private com.netscape.management.client.security.EncryptionPanel _encrypt;
	private JPanel _encryptParent;
	private JRadioButton _rbOff;
	private JRadioButton _rbAllowed;
	private JRadioButton _rbRequired;
	private JCheckBox _cbConsoleSSL;
	private JCheckBox _cbSSLCheckHostName;

	// I18N
	private static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "encryption";

}



/**
 * This class is a record containing all the encryption config data.
 * Reading and writing are encapsulated in readFromDirectory()
 * and writeToDirectory().
 */
class EncryptionConfigData {

	// The config data and their default value
	public boolean sslConsoleOn = false;			// nsserversecurity (from SIE)
	public int clientAuth = CLIENT_AUTH_ALLOWED;	// nssslclientauth
	public boolean sslServerOn = false;				// nsssl3
	public boolean sslCheckHostName = false;		// nsslapd-ssl-check-hostname
	public String cipherPrefs = CIPHER_PREFS;	    // nsssl3ciphers
	public Hashtable cipherSetupTable = new Hashtable(5); // of CipherSetup

	// Values for clientAuth
	public static final int CLIENT_AUTH_DISABLED = 0;
	public static final int CLIENT_AUTH_ALLOWED = 1;
	public static final int CLIENT_AUTH_REQUIRED = 2;

	// The default value for cipherPrefs.
	// This value is used when the nsssl3ciphers attribute is not
	// present in the directory. It must match the default of slapd.
	static final String CIPHER_PREFS = "-rsa_null_md5," +
	                                   "-rsa_null_sha," +
	                                   "+rsa_rc4_128_md5," +
	                                   "+rsa_rc4_40_md5," +
									   "+rsa_rc2_40_md5," +
									   "+rsa_des_sha," +
									   "+rsa_fips_des_sha," +
	                                   "+rsa_3des_sha," +
									   "+rsa_fips_3des_sha," +
									   "+fortezza," +
									   "+fortezza_rc4_128_sha," +
									   "+fortezza_null," +
									   "+tls_rsa_export1024_with_rc4_56_sha," +
	                                   "+tls_rsa_export1024_with_des_cbc_sha," +
	                                   "+tls_rsa_aes_128_sha," +
	                                   "+tls_rsa_aes_256_sha";
	
	// Cipher list to build the SSL3 tab in the cipher dialog
	static final String CIPHER_SSL3_OFF = "-rsa_null_md5," +
	                                      "-rsa_null_sha," +
										  "-rsa_rc2_40_md5," +
	                                      "-rsa_rc4_40_md5," +
	                                      "-rsa_rc4_128_md5," +
										  "-rsa_des_sha," +
										  "-rsa_fips_des_sha," +
	                                      "-rsa_3des_sha," +
										  "-rsa_fips_3des_sha," +
										  "-fortezza," +
										  "-fortezza_rc4_128_sha," +
										  "-fortezza_null";

	// Cipher list to build the TLS tab in the cipher dialog
	static final String CIPHER_TLS_OFF =  "+all," +
	                                      "-tls_rsa_export1024_with_rc4_56_sha," +
	                                      "-tls_rsa_export1024_with_des_cbc_sha," +
	                                      "-tls_rsa_aes_128_sha," +
	                                      "-tls_rsa_aes_256_sha," +
	                                      "-TLS_RSA_WITH_AES_128_GCM_SHA256";

	// Reads the directory and initializes this instance
	public void readFromDirectory(LDAPConnection ldc, LDAPConnection sieldc, String sieDn)
	throws LDAPException {

		// Read the following entries:
		//		sieDn		(IN THE CONFIGURATION SERVER)
		//		cn=config
		//		cn=encryption,cn=config
		//		cn=*,cn=encryption,cn=config
		String configDn = "cn=config";
		String encryptionDn = "cn=encryption,cn=config";
		String[] configAttrs = { "nsslapd-security", "nsslapd-ssl-check-hostname" };
		String[] sieAttrs = { "nsserversecurity" };
		LDAPEntry sieEntry = sieldc.read(sieDn, sieAttrs);
		LDAPEntry configEntry = ldc.read(configDn, configAttrs);
		LDAPEntry encryptionEntry = ldc.read(encryptionDn);
		LDAPSearchResults cipherResults = ldc.search(
			encryptionDn, ldc.SCOPE_ONE, "objectclass=*", null, false );

		// Parse sieEntry
		if (sieEntry != null) {
			String v = DSUtil.getAttrValue(sieEntry, sieAttrs[0]);
			sslConsoleOn = v.equalsIgnoreCase("on");
		}
		else {
			Debug.println(0, "EncryptionConfigData.readFromDirectory: sieEntry is null");
		}

		// Parse configEntry
		if (configEntry != null) {
			String v = DSUtil.getAttrValue(configEntry, configAttrs[0]);
			sslServerOn = v.equalsIgnoreCase("on");

			v = DSUtil.getAttrValue(configEntry, configAttrs[1]);
			sslCheckHostName = v.equalsIgnoreCase("on");

		}
		else {
			Debug.println(0, "EncryptionConfigData.readFromDirectory: configEntry is null");
		}

		// Parse encryptionEntry
		if (encryptionEntry != null) {

			// clientAuth
			String v = DSUtil.getAttrValue( encryptionEntry, "nssslclientauth");
			if (v.equalsIgnoreCase("allowed"))
				clientAuth = CLIENT_AUTH_ALLOWED;
			else if (v.equalsIgnoreCase("required"))
				clientAuth = CLIENT_AUTH_REQUIRED;
			else
				clientAuth = CLIENT_AUTH_DISABLED;

			// cipherPrefs
			cipherPrefs = DSUtil.getAttrValue(encryptionEntry, "nsssl3ciphers");
			// Note: just after installation, nsssl3ciphers is not defined.
			// So cipherPrefs is empty. In that case, we use the default preferences.
			if ((cipherPrefs == null) || (cipherPrefs.length() == 0)) {
				cipherPrefs = CIPHER_PREFS;
			}
		}
		else {
			Debug.println(0, "EncryptionConfigData.readFromDirectory: encryptionEntry is null");
		}

		// Parse cipherResults
		if (cipherResults != null) {
			while( cipherResults.hasMoreElements() ) {
				LDAPEntry cipherEntry = (LDAPEntry)cipherResults.nextElement();
				CipherSetup setup = new CipherSetup();

				// setup.family
				String s = cipherEntry.getDN();
				int ind = s.indexOf( ',' );
				setup.family = s.substring( s.indexOf('=')+1, ind );

				// setup.enabled
				String val = DSUtil.getAttrValue( cipherEntry, "nssslactivation" );
				setup.enabled = val.equalsIgnoreCase("on");

				// setup.selectedDevice
				setup.selectedDevice = DSUtil.getAttrValue(cipherEntry, "nsssltoken");

				// setup.selectedCertificate
				setup.selectedCertificate =
					DSUtil.getAttrValue( cipherEntry, "nssslpersonalityssl" );

				// And populate cipherSetupTable
				if (cipherSetupTable.put(setup.family, setup) != null) {
					Debug.println(0, "EncryptionConfigData.readFromDirectory: cipher conflict ignored");
				}
				Debug.println("EncryptionConfigData.readFromDirectory: read prefs for " + setup.family);
			}
		}
		else {
			Debug.println("EncryptionConfigData.readFromDirectory: encryptionEntry is null");
		}
	}


	// Writes this instance data to the directory
	public void writeToDirectory(LDAPConnection ldc, LDAPConnection sieldc, String sieDn)
	throws LDAPException {

		// Modify the following entries:
		//		cn=*,cn=encryption,cn=config
		//		cn=encryption,cn=config
		//		cn=config
		//		sieDn			(IN THE CONFIGURATION SERVER)

		//		cn=*,cn=encryption,cn=config
		Enumeration e = cipherSetupTable.keys();
		while (e.hasMoreElements()) {
			CipherSetup setup = (CipherSetup)cipherSetupTable.get(e.nextElement());
			LDAPAttributeSet attrs = new LDAPAttributeSet();
			if (setup.selectedDevice.length() >= 1)
				attrs.add(new LDAPAttribute("nsssltoken", setup.selectedDevice));
			if (setup.selectedCertificate.length() >= 1) {                        

				// Strip off the "<token>:" prefix for certs on external tokens
				String cert = setup.selectedCertificate;
				String tokenPrefix = setup.selectedDevice + ":";
				if (cert.startsWith(tokenPrefix)) {
					cert = cert.substring(tokenPrefix.length());
				}

				attrs.add(new LDAPAttribute("nssslpersonalityssl", cert));
			}
			String s = (setup.enabled && sslServerOn) ? "on" : "off";
			attrs.add(new LDAPAttribute("nssslactivation", s));
			String[] oclasses = { "top", "nsEncryptionModule" };
			attrs.add(new LDAPAttribute("objectclass", oclasses));
			String dn = "cn=" + setup.family + ",cn=encryption,cn=config";
			LDAPEntry entry = new LDAPEntry(dn, attrs);

			// First we try to add; if it exists, we try to modify
			addOrModifyLDAPEntry(ldc, entry);
		}

		//		cn=encryption,cn=config
		LDAPModificationSet modSet = new LDAPModificationSet();
		String v = sslServerOn ? "on" : "off";
		modSet.add(LDAPModification.REPLACE, new LDAPAttribute("nsTLS1", v));
		switch(clientAuth) {
			case CLIENT_AUTH_ALLOWED:
				v = "allowed";
				break;
			case CLIENT_AUTH_REQUIRED:
				v = "required";
				break;
			case CLIENT_AUTH_DISABLED:
			default:
				v = "off";
				break;
		}
		modSet.add(LDAPModification.REPLACE, new LDAPAttribute("nssslclientauth", v));
		modSet.add(LDAPModification.REPLACE, new LDAPAttribute("nsssl3ciphers", cipherPrefs));
		String db = LDAPDN.explodeDN(sieDn, true)[0];
		v = "alias/" + db.toLowerCase() + "-key3.db";
		modSet.add(LDAPModification.REPLACE, new LDAPAttribute("nskeyfile", v));
		v = "alias/" + db.toLowerCase() + "-cert8.db";
		modSet.add(LDAPModification.REPLACE, new LDAPAttribute("nscertfile", v));
		ldc.modify("cn=encryption,cn=config", modSet);

		//		cn=config
		modSet = new LDAPModificationSet();
		v = sslServerOn ? "on" : "off";
		modSet.add(LDAPModification.REPLACE, new LDAPAttribute("nsslapd-security", v));
		v = sslCheckHostName ? "on" : "off";
		modSet.add(LDAPModification.REPLACE, new LDAPAttribute("nsslapd-ssl-check-hostname", v));
		ldc.modify("cn=config", modSet);

		//		sieDn		(IN THE CONFIGURATION SERVER)
		v = sslConsoleOn ? "on" : "off";
		LDAPAttribute attr = new LDAPAttribute("nsserversecurity", v);
		sieldc.modify(sieDn, new LDAPModification(LDAPModification.REPLACE, attr));
	}


	/**
	 * Try to add the specified entry ; if it already exists, 
	 * then try to modify it.
	 */
	static void addOrModifyLDAPEntry(LDAPConnection ldc, LDAPEntry entry)
	throws LDAPException {
		try {
			ldc.add(entry);
		}
		catch(LDAPException x) {
			if (x.getLDAPResultCode() != x.ENTRY_ALREADY_EXISTS) {
				Debug.println(0, "EncryptionPanel.addOrModifyEntry: exception for " + entry);
				throw x;
			}
			else {
				// Let's create an LDAPModificationSet from the entry
				LDAPModificationSet modSet = new LDAPModificationSet();
				Enumeration e = entry.getAttributeSet().getAttributes();
				while (e.hasMoreElements()) {
					LDAPAttribute attr = (LDAPAttribute)e.nextElement();
					modSet.add(LDAPModification.REPLACE, attr);
				}
				// And modify
				ldc.modify(entry.getDN(), modSet);
			}
		}
	}

} ;


/*
 * This class is a record containing the regular and secure ports.
 * Reading is encapsulated in readFromDirectory().
 */
class PortConfigData {

	int port; // RFCs say 389
	int securePort; // RFCs say 636
	
	// Reads the directory and initializes this instance
	public void readFromDirectory(LDAPConnection ldc)
	throws LDAPException {

		// Defaults
		port = -1;
		securePort = -1; 

		// Read cn=config
		String configDn = "cn=config";
		String[] configAttrs = { "nsslapd-port", "nsslapd-secureport" };
		LDAPEntry configEntry = ldc.read(configDn, configAttrs);

		// Parse configEntry
		if (configEntry != null) {
			String portStr = DSUtil.getAttrValue(configEntry, configAttrs[0]);
			String securePortStr = DSUtil.getAttrValue(configEntry, configAttrs[1]);
			try {
				port = Integer.parseInt(portStr);
				port = 3000;
			}
			catch(NumberFormatException x) {
				Debug.println(0, "EncryptionPanel.PortConfigData.readFromDirectory: cannot convert nsslapd-port to an int !");
			}
			try {
				securePort = Integer.parseInt(securePortStr);
			}
			catch(NumberFormatException x) {
				Debug.println(0, "EncryptionPanel.PortConfigData.readFromDirectory: cannot convert nsslapd-secureport to an int !");
			}
			 
		}
		else {
			Debug.println(0, "EncryptionConfigData.readFromDirectory: configEntry is null");
		}

	}
}






/**
 * Record containing the config data of a single cipher.
 */
class CipherSetup {
	public String family = null;			// cn
	public boolean enabled = false;			// nssslactivation
	public String selectedDevice = "";		// nsssltoken
	public String selectedCertificate = "";	// nssslpersonalityssl
} ;


