/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.IDSModel;
import java.awt.event.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class RootPanel extends DSTabbedPanel
{

	public RootPanel(IDSModel model)
	{
		super( model );

		addTab( new ServerSettingsPanel( model ) );

		addTab( new ServerPerformancePanel( model ) );

		addTab( new EncryptionPanel( model ) );

		addTab( new ServerSNMPSettingsPanel( model ) );

		addTab( new ServerManagerPanel( model ) );
                
                addTab( new ServerSASLPanel ( model ) );

		_tabbedPane.setSelectedIndex( 0 );
	}

	/**
     *  handle incoming event
     *	 
	 * Overwrites DSTabbedPane to call the proper method to 
	 * refresh the Server manager panel
	 * 
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {
			int nTabs = _tabbedPane.getTabCount();
			for (int ii = 0; ii < nTabs; ++ii) {
				BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);				
				if (( p != null ) &&
					p.isInitialized()) {
					if (p instanceof ServerManagerPanel) {
						((ServerManagerPanel)p).refreshFromServer();
					} else {
						p.refresh();
					}
				}
			}					
		} else {
			super.actionPerformed(e);
		}
	}
}
