/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.File;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import netscape.ldap.*;
import netscape.ldap.util.*;
import netscape.ldap.util.DN;


/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date                 03/08/98
 * @see     com.netscape.admin.dirserv
 */
public class NewLDBMPanel extends BlankPanel {

    public NewLDBMPanel(IDSModel model, LDAPEntry SuffixEntry) {
        super( model, _section );
        _helpToken = "configuration-new-ldbm-instance-dbox-help";
        _model = model;
        _entrySuffix = SuffixEntry;
    }

    public void init() {

        _myPanel.setLayout( new GridBagLayout() );
        GridBagConstraints gbc = getGBC();

        // Physical DB info
        createFileArea( _myPanel );

        _baseDir = DSUtil.getDefaultDBPath( getModel().getServerInfo() );
                
        AbstractDialog dlg = getAbstractDialog();
        if ( dlg != null ) {
            dlg.setOKButtonEnabled( false );
        }

    }

    protected void createFileArea( JPanel grid ) {
        GridBagConstraints gbc = getGBC();
        gbc.insets.bottom = _gbc.insets.top;
        gbc.gridwidth = gbc.REMAINDER;

        _dbLoc =  getDatabaseLoc();
        String[] dirList = { _dbLoc };
        JPanel panel = new GroupPanel(
                   DSUtil._resource.getString( _section, "new-ldbm-db-title" ));
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        grid.add( panel, gbc );

        _suffix = MappingUtils.getMappingNodeSuffix(_entrySuffix);
        _suffixLabel =  makeJLabel( _section, "suffix-name" );
        gbc.gridwidth = 3;
        gbc.fill = gbc.NONE;
        gbc.weightx = 0.0;
        panel.add( _suffixLabel, gbc );

        _suffixValue = new JLabel(  _suffix );
        _suffixLabel.setLabelFor(_suffixValue);
        gbc.weightx = 1.0;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;        
        panel.add( _suffixValue, gbc );


        // DB Name 
        _instanceNameLabel = makeJLabel( _section, "instance-name" );
        gbc.gridwidth = 3;
        gbc.fill = gbc.NONE;
        gbc.weightx = 0.0;
        panel.add( _instanceNameLabel, gbc );

        _instanceNameText = makeJTextField( _section, "instance-name" );
        _instanceNameLabel.setLabelFor(_instanceNameText);

        gbc.weightx = 1.0;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        panel.add( _instanceNameText, gbc );


        _comboNames = new JComboBox();
        _comboNames.addItemListener(this);
        _comboNames.addActionListener(this);

        if ( dirList != null ) {
            for( int i = 0; i < dirList.length; i++ )
                _comboNames.addItem( dirList[i] );
            if ( dirList.length > 0 )
                _comboNames.setSelectedIndex( 0 );
        }
        String ttip = DSUtil._resource.getString(_section, "select-dir-ttip");
        if (ttip != null) {
            _comboNames.setToolTipText(ttip);
        }

        JLabel filenameLabel = makeJLabel( _section, "filename" );
        gbc.gridwidth = 3;
        gbc.fill = gbc.NONE;
        gbc.weightx = 0.0;
        panel.add( filenameLabel, gbc );

        _tfDirDB = makeJTextField( _section, "filename" );
        filenameLabel.setLabelFor(_tfDirDB);
        //                _tfDirDB = makeJLabel( _section, "filename" );

        if ( dirList.length > 0 ) {
            _tfDirDB.setText( (String)_comboNames.getSelectedItem() );
            setOkay( true );
        } else {
            setOkay( false );
        }
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        panel.add(_tfDirDB,gbc);
        _bDirDB = makeJButton( _section, "browse-file" );
        gbc.fill = gbc.NONE;
        gbc.gridwidth = gbc.REMAINDER;
        panel.add(_bDirDB,gbc);
        _bDirDB.setEnabled( true );
                
        _dbLocChanged = false;
    }


    /**
     * Retreive db default folder from Directory server
     */
    private String getDatabaseLoc() {

        String DBLoc = null;

        // Search default databse directory 
        try {
            _model.setWaitCursor( true );
            LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
            LDAPSearchResults res =
                ldc.search( CONFIG_BASEDN,
                            ldc.SCOPE_ONE,
                            "nsslapd-pluginid=ldbm-backend",
                            null,
                            false );
            if(res.hasMoreElements() ) {
                LDAPEntry bentry = (LDAPEntry)res.nextElement();
                _PluginLoc = bentry.getDN();
                Debug.println( "NewLDBMPanel.getDatabaseLoc() {");
                Debug.println( "*** plugin db: " + _PluginLoc );
                // Now retreive the config
                LDAPSearchResults res_conf = 
                    ldc.search( bentry.getDN(),
                                ldc.SCOPE_ONE,
                                "cn=config",
                                null,
                                false );
                if( res_conf.hasMoreElements() ) {
                    LDAPEntry centry = (LDAPEntry)res_conf.nextElement();
                    LDAPAttribute attr = centry.getAttribute( "nsslapd-directory" );
                    Debug.println( "*** nsslapd-directory =" + attr);
                    Enumeration en = attr.getStringValues();
                    if ( en.hasMoreElements() ) {
                        DBLoc =  (String)en.nextElement() +"/";
                    }
                }                                
            }
        } catch ( LDAPException e ) {
            Debug.println( "NewLDBMPanel.init() " + e );
        } finally {
            _model.setWaitCursor( false );
        }
                
        return DBLoc;
    } // getDatabaseLoc


    /**
     * Enable/disable OK button
     *
     * @param ok true to enable the OK button
     */
    private void setOkay( boolean ok ) {
        AbstractDialog dlg = getAbstractDialog();
        if ( dlg != null ) {
            dlg.setOKButtonEnabled( ok );
        } 
    }
        
    private void checkOkay() {                
        String bckName = _instanceNameText.getText();
        String path = getInstancename();
                        
        Debug.println("NewLDBMPanel.checkOkay() path =" + path);
        boolean ok = ( (path != null) &&
                       (path.length() > 0) &&
                       ( bckName != null ) &&
                       ( DSUtil.isValidBckName ( bckName )));

        setOkay( ok );
    }

    private int checkInstanceWithPath() {                
        String bckName = _instanceNameText.getText();
        String dbDir = _tfDirDB.getText();

        Debug.println("NewLDBMPanel.checkInstanceWithPath() = " + bckName +
                      ": " + dbDir);

        File dbDirFile = new File(dbDir);
        String dbDirName = dbDirFile.getName();

        if (null != dbDirName && 0 != bckName.compareTo(dbDirName))
        {
            Debug.println("NewLDBMPanel.checkInstanceWithPath(): " +
                bckName + " != " + dbDirName);
            String[] args_wn = { dbDir, bckName };
            DSUtil.showInformationDialog( _model.getFrame(),
                                          "invalid-dbdir",
                                          args_wn, "general" );
            return -1;
        }

        File dbDirParent = dbDirFile.getParentFile();

        if (null != dbDirParent)
        {
            File dbLocFile = new File(_dbLoc);
            if (0 != dbDirParent.compareTo(dbLocFile))
            {
                Debug.println("NewLDBMPanel.checkInstanceWithPath(): " +
                    _dbLoc + " != " + dbDirParent);
                String[] args_wn = { dbDirParent.getPath(), _dbLoc };
                DSUtil.showInformationDialog( _model.getFrame(),
                                              "nondefault-dbdir",
                                              args_wn, "general" );
            }
        }

        return 0;
    }

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if ( e.getSource().equals(_bDirDB) ) {
                String file = getDBLoc().trim();
            if ( file.length() < 1 ) {
                /* If no path was provided, use the default path */
                file = _baseDir;
            }

            file = DSFileDialog.getDirectoryName(file, false, this);
            if (file != null) {
                    _tfDirDB.setText(file.trim());
                _dbLocChanged = true;
            }
        } else if ( e.getSource().equals( _comboNames ) ) {
            if ( _tfDirDB != null ) { // Startup condition
                _tfDirDB.setText( (String)_comboNames.getSelectedItem() );
                checkOkay();
            }
        } else if (e.getSource().equals( _rbExistingNode)) {
            setMappingState( EXISTING_NODE );
        } else if (e.getSource().equals( _rbNewNode)) {
            setMappingState( NEW_NODE );
        } else if (e.getSource().equals( _rbNoNode)) {
            setMappingState( NO_NODE );        
        } else {
            super.actionPerformed(e);
        }
    }

    public int addLDBMBackend(String suffix ) {

        String _instName = _instanceNameText.getText().trim();

        LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
        Debug.println("NewLDBMPanel.addLDBMBackend()");
        // Retreive the ldbm location in config

        if (checkInstanceWithPath() < 0)
        {
            return (0);
        }

        // Add Instance Entry
        String dn_dbInst = "cn=" + _instName + "," + _PluginLoc;

        LDAPAttributeSet attrs = new LDAPAttributeSet();

        String objectclass_dbInst[] = { "top", "extensibleObject", "nsBackendInstance" };
        LDAPAttribute attr = new LDAPAttribute( "objectclass", objectclass_dbInst );
        attrs.add( attr );

        String cn_dbInst[] = { _instName };
        attrs.add( new LDAPAttribute( "cn", cn_dbInst ) );

                                   
        String suffix_dbInstConfig[] = { suffix };
        attrs.add( new LDAPAttribute ( "nsslapd-suffix", suffix_dbInstConfig ));

        String cachesize_dbInstConfig[] = { "-1" };
        attrs.add( new LDAPAttribute ( "nsslapd-cachesize",
                                       cachesize_dbInstConfig ));

        String cachememsize_dbInstConfig[] = { "10485760" };
        attrs.add( new LDAPAttribute ( "nsslapd-cachememsize",
                                       cachememsize_dbInstConfig ));

        String ldbm_loc = _tfDirDB.getText().trim();

        if (ldbm_loc.length() > 0 ) {
            String ldbm_directory[] = { ldbm_loc };
            attrs.add( new LDAPAttribute ( "nsslapd-directory",
                                           ldbm_directory ));
        }
 

        LDAPEntry dbInst = new LDAPEntry( dn_dbInst, attrs );
        _model.setWaitCursor( true );
        try {
            ldc.add( dbInst );
            Debug.println("****** add:" + dn_dbInst);
        } catch (LDAPException e) {
            String[] args_m = { dn_dbInst, e.toString()} ;
            DSUtil.showErrorDialog( getModel().getFrame(),
                                    "error-add-mapping",
                                    args_m,
                                    _section);
            return (0);
        } finally {
            _model.setWaitCursor( false );
        }
        return (1);
                
    }

    public boolean checkUnique() {
        _model.setWaitCursor( true );
        try {
            String InstanceName = _instanceNameText.getText();
            _model.setWaitCursor( true );
            LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
            if ( InstanceName != null) {
                LDAPSearchResults res =
                    ldc.search( CONFIG_BASEDN,
                                ldc.SCOPE_SUB ,
                                "(&(cn=" + InstanceName + ")(objectclass=nsBackendInstance))",
                                null,
                                false );
                _model.setWaitCursor( false );
                if (res.hasMoreElements()) {
                    DSUtil.showErrorDialog( getModel().getFrame(),
                                            "backendname-exist",
                                            InstanceName );
                    _instanceNameText.selectAll();
                    return (false);
                } //if (res.hasMoreElements())
            } // if( InstanceName != null)
        } catch (LDAPException e) { 
            _model.setWaitCursor( false );
            DSUtil.showErrorDialog( getModel().getFrame(),
                                    "backendname-can-create",
                                    e.toString() );
            return( false );
        } finally {
            _model.setWaitCursor( false );
        } 
        return( true );
    }

    public void resetCallback() {
        /* No state to preserve */
        clearDirtyFlag();
        hideDialog();
    }

    /**
     * Enable the button and attached field.
     */
    private void setMappingState() {
        setMappingState( NEW_NODE );
    }

    private void setMappingState( int isSelected ) {
        switch ( isSelected ) {
        case EXISTING_NODE : {
            _NewNodeText.setEnabled( false );
            _NewNodeLabel.setEnabled( false );
            _comboNewNodeMapping.setEnabled( false );
            _comboNewNodeLabel.setEnabled( false );
            _comboMapping.setEnabled( true );
            _comboLabel.setEnabled( true );
            _isNewMappingNodeValid = true;
            break;
        }
        case NEW_NODE : {
            _comboMapping.setEnabled( false );
            _comboLabel.setEnabled( false );
            _comboNewNodeMapping.setEnabled( true );
            _comboNewNodeLabel.setEnabled( true );
            _NewNodeText.setEnabled( true );
            _NewNodeLabel.setEnabled( true );
            _isNewMappingNodeValid = false;
            break;
        }
        case NO_NODE : {
            _NewNodeText.setEnabled( false );
            _NewNodeLabel.setEnabled( false );
            _comboMapping.setEnabled( false );
            _comboLabel.setEnabled( false );
            _comboNewNodeMapping.setEnabled( false );
            _comboNewNodeLabel.setEnabled( false );
            _isNewMappingNodeValid = true;
            break;
        }
        }
        repaint();
        checkOkay();

    }

    public void okCallback() {
        /* No state to preserve */
        if(checkUnique()) {
            // reload the entry anyway
            LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
            try {
                _entrySuffix = ldc.read(_entrySuffix.getDN());
            } catch (LDAPException e){
                String[] args = { _entrySuffix.getDN(), e.toString() };
                DSUtil.showErrorDialog( getModel().getFrame(),
                                        "error-reload-suffix",
                                        args,
                                        _section );
                return;
            }
            /* Add backend in cn=ldbm database,cn=plugins,cn=config */            
             if( addLDBMBackend( (String) _suffix ) > 0 ) {
                if(MappingUtils.addBackendInSuffix( _model,
                                       _entrySuffix,
                                       _instanceNameText.getText(),
                                       _section)){
                    hideDialog();
                }
            }
        }
    }


    public void changedUpdate(DocumentEvent e) {
        if( e.getDocument() == _instanceNameText.getDocument() ) {
            if ( DSUtil.isValidBckName( _instanceNameText.getText()) ) {
                setChangeState( _instanceNameLabel, CHANGE_STATE_UNMODIFIED );
                if ( ! _dbLocChanged ) {
                    _isAutoLoc = true;
                    _tfDirDB.setText( _dbLoc + 
                                      _instanceNameText.getText() );
                    _isAutoLoc = false;
                }
            } else {
                setChangeState( _instanceNameLabel, CHANGE_STATE_ERROR );
            }
        } else if( e.getDocument() == _tfDirDB.getDocument() ) {
            if ( ! _isAutoLoc ) {                            
                _dbLocChanged = true;
            }
        } else if( e.getDocument() == _NewNodeText.getDocument() ) {
            if ( DN.isDN ( _NewNodeText.getText() )) {
                setChangeState( _NewNodeLabel, CHANGE_STATE_UNMODIFIED );
                _isNewMappingNodeValid = true;
            } else {
                setChangeState( _NewNodeLabel, CHANGE_STATE_ERROR );
                _isNewMappingNodeValid = false;
            }
        } else {
            super.changedUpdate( e );
        }
        checkOkay();
    }

    public void removeUpdate(DocumentEvent e) {
        if( e.getDocument() == _instanceNameText.getDocument() ) {
            if ( DSUtil.isValidBckName( _instanceNameText.getText()) ) {
                setChangeState( _instanceNameLabel, CHANGE_STATE_UNMODIFIED );
                if ( ! _dbLocChanged ) {
                    _isAutoLoc = true;
                    _tfDirDB.setText( _dbLoc + 
                                      _instanceNameText.getText() );
                    _isAutoLoc = false;
                }
            } else {
                setChangeState( _instanceNameLabel, CHANGE_STATE_ERROR );
            }
        } else if( e.getDocument() == _tfDirDB.getDocument() ) {
            if ( ! _isAutoLoc ) {                                                
                _dbLocChanged = true;
            }
        } else if( e.getDocument() == _NewNodeText.getDocument() ) {
            if ( DN.isDN ( _NewNodeText.getText() )) {
                setChangeState( _NewNodeLabel, CHANGE_STATE_UNMODIFIED );
                _isNewMappingNodeValid = true;
            } else {
                setChangeState( _NewNodeLabel, CHANGE_STATE_ERROR );
                _isNewMappingNodeValid = false;
            }
        } else {
            super.removeUpdate( e );
        }
        checkOkay();
    }

    public void insertUpdate(DocumentEvent e) {
        if( e.getDocument() == _instanceNameText.getDocument() ) {
            if ( DSUtil.isValidBckName( _instanceNameText.getText()) ) {
                setChangeState( _instanceNameLabel, CHANGE_STATE_UNMODIFIED );
                if ( ! _dbLocChanged ) {
                    _isAutoLoc = true;
                    _tfDirDB.setText( _dbLoc +
                                      _instanceNameText.getText() );
                    _isAutoLoc = false;
                }
            } else {
                setChangeState( _instanceNameLabel, CHANGE_STATE_ERROR );
            } 
        } else if( e.getDocument() == _tfDirDB.getDocument() ) {
            if ( ! _isAutoLoc ) {
                _dbLocChanged = true;
            }
        } else if( e.getDocument() == _NewNodeText.getDocument() ) {
            if ( DN.isDN ( _NewNodeText.getText() )) {
                setChangeState( _NewNodeLabel, CHANGE_STATE_UNMODIFIED );
                _isNewMappingNodeValid = true;
            } else {
                setChangeState( _NewNodeLabel, CHANGE_STATE_ERROR );
                _isNewMappingNodeValid = false;
            }
        } else {
            super.insertUpdate( e );
        }
        checkOkay();
    }

    public String getDBLoc() {
        return _tfDirDB.getText().trim();
    }

    public String getInstancename() {
        return _instanceNameText.getText().trim();
    }

    private JLabel       _suffixLabel;
    private JLabel       _suffixValue;
    private JTextField   _instanceNameText;
    private JLabel       _instanceNameLabel;
    private JTextField   _mappingNameText;
    private JLabel       _mappingNameLabel;
    private JTextField   _NewNodeText;
    private JLabel       _NewNodeLabel;

    private IDSModel     _model = null;


    private JComboBox    _comboNames;
    private JComboBox    _comboMapping;
    private JLabel       _comboLabel;
    //private JLabel     _tfDirDB;
    private JTextField   _tfDirDB;

    private JButton      _bDirDB;
    private JButton      _browseButton;
    private String       _baseDir = "";
    private String       _PluginLoc = "";
    private JRadioButton _rbExistingNode;
    private JRadioButton _rbNewNode;
    private JRadioButton _rbNoNode;

    private JComboBox    _comboNewNodeMapping;
    private JLabel       _comboNewNodeLabel;

    private boolean      _dbLocChanged = false;
    private boolean      _isAutoLoc = false;
    private boolean      _isNewMappingNodeValid = false;
    private String       _dbLoc;
    private LDAPEntry    _entrySuffix;
    private String       _suffix = "";

    private final static String _section  = "newldbminst";
    static final String CONFIG_BASEDN     = "cn=plugins, cn=config" ;
    static final String CONFIG_MAPPING    = "cn=mapping tree, cn=config" ;
    static final String ROOT_MAPPING_NODE = "is root suffix";
    static final int EXISTING_NODE        = 0 ;
    static final int NEW_NODE             = 1;
    static final int NO_NODE              = 2;
}
