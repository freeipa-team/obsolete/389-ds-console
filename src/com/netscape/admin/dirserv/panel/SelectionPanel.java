/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.UITools;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DefaultResourceModel;

/**
 * SelectionPanel
 * 
 *
 * @version 1.0
 * @author rweltman
 **/
public class SelectionPanel extends BlankPanel
                            implements MouseListener {
	public SelectionPanel( IDSModel model, String section, Vector items,
						   String title, String helpToken ) {
		super( model, section, false );
		setTitle( title );
		_items = items;
		_helpToken = helpToken;
	}

	public SelectionPanel( IDSModel model, String section, Vector items,
						   String title, String label, String helpToken ) {
		this( model, section, items, title, helpToken );
		_label = label;
	}

	public SelectionPanel( IDSModel model, String section, Vector items,
						   String title, String label, String helpToken,
						   boolean selectOne ) {
		this( model, section, items, title, label, helpToken );
		_selectMode = selectOne;
	}

	public void init() {
        _myPanel.setLayout( new GridBagLayout() );
		GridBagConstraints gbc = (GridBagConstraints)getGBC().clone();
		gbc.insets = new Insets( 0, 0, 0, 0 );
		gbc.fill = gbc.HORIZONTAL;
		gbc.gridwidth = gbc.REMAINDER;
		JLabel theLabel = null;
		if ( _label != null ) {
			theLabel = new JLabel( _label );
			_myPanel.add( new JLabel( _label ), gbc );
			_myPanel.add(
				Box.createVerticalStrut(UIFactory.getComponentSpace()), gbc);
		}
		gbc.fill = gbc.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;

		_list = makeJList( _items );
		if (theLabel != null) {
			theLabel.setLabelFor(_list);
		}
		if (_list.getModel().getSize() > 0) {
			_list.setSelectedIndex( 0 );
		}
		if (_selectMode) {
			_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		_list.addMouseListener(this);
		_list.setVisibleRowCount(20);
        JScrollPane scroll = new JScrollPane(_list);
		scroll.setBorder( UITools.createLoweredBorder() );
        _myPanel.add( scroll, gbc );
	}

	/**
	 * Some list component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void valueChanged(ListSelectionEvent e) {
        Debug.println(7, "SelectionPanel.valueChanged: " +
					  _list.getSelectedValue());
		AbstractDialog dlg = getAbstractDialog();
		if ( dlg != null ) {
			dlg.setOKButtonEnabled( _list.getSelectedValue() != null );
		}
    }

    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

    public void okCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		_item = (String)_list.getSelectedValue();
		_selectedItems = _list.getSelectedValues();
		if ( _item == null ) {
			return;
		}
		Debug.println( "SelectionPanel.okCallback: " + _item );
		hideDialog();
    }

    public Object getSelectedItem() {
		return _item;
	}

	public Object[] getSelectedItems() {
		return _selectedItems;
	}

	/**
	 * Implements MouseListener.
	 */
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() >= 2) {
			okCallback();
			hideDialog();
		}
	}
	
	public void mouseEntered(MouseEvent e) {}
	
	public void mouseExited(MouseEvent e) {}
	
	public void mousePressed(MouseEvent e) {}
	
	public void mouseReleased(MouseEvent e) {}



    public static void main( String[] args ) {
		Vector v = new Vector();
		String[] o = { "person", "inetorgperson", "organizationalunit" };
		for( int i = 0; i < o.length; i++ )
			v.addElement( o[i] );
			Debug.setTrace( true );
		try { 
			UIManager.setLookAndFeel(
				"com.netscape.management.nmclf.SuiLookAndFeel" ); 
		} catch (Exception e) { 
			System.err.println("Cannot load nmc look and feel."); 
		} 
		ResourceSet _resource =
	      new ResourceSet("com.netscape.admin.dirserv.propedit.propedit");
		String section = "newobjectclass";
		String title = _resource.getString(section, "title");
		String helpToken = "property-new-objectclass-dbox-help";
		BlankPanel child =
			new SelectionPanel( new DefaultResourceModel(), section, v,
								title, helpToken );
		SimpleDialog dlg =
			new SimpleDialog( new JFrame(),
							  child.getTitle(),
							  SimpleDialog.OK |
							  SimpleDialog.CANCEL |
							  SimpleDialog.HELP,
							  child );
		dlg.setComponent( child );
		dlg.setOKButtonEnabled( false );
		dlg.setDefaultButton( SimpleDialog.OK );
		dlg.setSize(320,280);
		dlg.show();
    }

	private JList _list;
	private Vector _items;
	private Object _item = null;
	private Object[] _selectedItems;
	private String _label = null;
	private boolean _selectMode = false;
}
