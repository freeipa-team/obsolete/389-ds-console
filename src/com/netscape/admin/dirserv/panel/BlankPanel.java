/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.net.*;
import java.text.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.*;
import com.netscape.admin.dirserv.GlobalConstants;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.IDSResourceSelectionListener;
import com.netscape.management.nmclf.SuiLookAndFeel;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.admin.dirserv.panel.DSEntry;
import netscape.ldap.*;

/**
 * BlankPanel
 *
 * Base panel for all right-hand panels in Directory console. Also used
 * for many dialog panels. It provides content management and validation for
 * data stored in the directory.
 * 
 * @version
 * @author
 **/
public class BlankPanel extends JPanel 
                        implements ActionListener,
	                               DocumentListener,
	                               ItemListener,
                                   ChangeListener,
	                               ListSelectionListener,
                                   IDSResourceSelectionListener {
    
    public BlankPanel(IDSModel model, boolean scroll) {
        this(model, null, scroll);
    }

    public BlankPanel(IDSModel model) {
		this( model, true );
	}

    /**
     * Sets the panel title using the panel in the properties file after
     * other construction.  Prevents title from being null if value is
     * missing from the property file and breaking tab creation.  The
     * title of the panel should be stored in the properties file with
	 * the following format:
     *
     * panelname-title=Panel Title
     *
     * If the title is missing from the properties file, the constructor
	 * defaults the panel title to "Missing Title."
     *
     * @param model IDSModel
     * @param panelName string that corresponds to the panel name in the
	 * properties file
     */
    public BlankPanel( IDSModel model, String panelName,
					   boolean scroll) {
		super();
        _dsModel = model;

		prepareClientPanel( scroll );

//      _dsModel.addIDSResourceSelectionListener(this);
        _dsEntrySet = new DSEntrySet();
        
        String title = null;
        if (panelName != null) {
            title = DSUtil._resource.getString(panelName, "title");
		}
        if (title == null)
            title = "Missing Title";
		setTitle(title);
    }

    public BlankPanel(IDSModel model, String panelName) {
		this( model, panelName, true );
	}


    private void prepareClientPanel( boolean scroll ) {
		setBorder( new EmptyBorder(0, 0, 0, 0) );

        // create the panel which will contain the actual goods; the
        // subpanel is responsible for setting the layout inside here
        _myPanel = new JPanel();
        _myPanel.setBorder( new EmptyBorder(getBorderInsets()) );

		/* The current layout thinking is that the panel contents should
		   be anchored at the upper left, rather than centered or with
		   1/3 of the vertical empty space at the top, and 2/3 at the
		   bottom - 07/02/98.
		*/
        GridBagConstraints gbc = getGBC();
        setLayout( new GridBagLayout() );
        gbc = getGBC();

		final boolean squish = false;
		/* 0.3 at the top */
		if ( squish ) {
			gbc.fill = gbc.BOTH;
			gbc.weightx = 1.0;
			gbc.weighty = 0.3;
			gbc.gridwidth = gbc.REMAINDER;
			gbc.insets = new Insets( 0, 0, 0, 0 );
			add( Box.createGlue(), gbc );
		}

		gbc.insets = new Insets( 0, 0, 0, 0 );
		gbc.fill = gbc.BOTH;
 		gbc.weightx = 1.0;
		gbc.weighty = 0.01;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.anchor = gbc.NORTHWEST;
		if ( scroll ) {
			JScrollPane jscroll = new JScrollPane( _myPanel );
			jscroll.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );
			jscroll.getVerticalScrollBar().setUnitIncrement(20);
			add( jscroll, gbc );
		} else {
			add( _myPanel, gbc );
		}

		/* 0.7 at the bottom */
		if ( squish ) {
			gbc.fill = gbc.BOTH;
			gbc.weightx = 1.0;
			gbc.weighty = 0.7;
			add( Box.createGlue(), gbc );
		}
	}

	protected void addBottomGlue() {
        GridBagConstraints gbc = getGBC();
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = new Insets( 0, 0, 0, 0 );
		gbc.fill = gbc.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		_myPanel.add( Box.createGlue(), gbc );
	}

    public void setTitle( String title ) {
		_title = title;
    }

    public String getTitle() {
		return _title;
	}

    /**
     * Handle incoming event. The derived classes do most of the work.
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( _verbose ) {
			Debug.println( "BlankPanel.actionPerformed: source = " +
						   e.getSource() );
		}
		
		validateAll ();
    }

	/**
	 * The tab for this panel was selected
	 *
	 * @param e Event indicating what changed
	 */
	public void stateChanged(ChangeEvent e) {
	    if ( !(e.getSource() instanceof JTabbedPane) ) {
			return;
		}
	    JTabbedPane tabs = (JTabbedPane)e.getSource();
		if ( _verbose ) {
			Debug.println("BlankPanel.stateChanged: " +
						  getClass().getName() +
						  " to " +
						  tabs.getSelectedComponent().getClass().getName() );
		}
        if (tabs.getComponentAt(tabs.getSelectedIndex()) == this)
	    	select(null, null);
	    else if (this.isDirty()) {
	        unselect(null, null);
	
    	}
	}

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void changedUpdate(DocumentEvent e) {
		if ( _verbose ) {
			Debug.println("BlankPanel.changeUpdate: event={" + e +
						  "} this={" + this.getClass() + "}");
		}
        
		validate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void removeUpdate(DocumentEvent e) {
		if ( _verbose ) {
			Debug.println("BlankPanel.removeUpdate: event={" + e +
						  "} this={" + this.getClass() + "}");
		}
        validate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void insertUpdate(DocumentEvent e) {
		if ( _verbose ) {
			Debug.println("BlankPanel.insertUpdate: event={" + e +
						  "} this={" + this.getClass() + "}");
		}
        validate(e);
    }

	/**
	 * Some list component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void itemStateChanged(ItemEvent e) {
		if ( _verbose ) {
			Debug.println("BlankPanel.itemStateChanged: event={" + e +
						  "} this={" + this.getClass() + "}");
		}
       validate(e);
    }
    
	/**
	 * Some list component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void valueChanged(ListSelectionEvent e) {
		if ( _verbose ) {
			Debug.println("BlankPanel.valueChanged: event={" + e +
						  "} this={" + this.getClass() + "}");
		}
		validateAll ();
    }

    /**
	 * Validate text component, and set appropriate visual feedback
	 *
	 * @param e Event indicating what changed
	 */
    protected void validate(DocumentEvent e) {
		validateAll ();
    }

    /**
	 * Validate non-text component, and set appropriate visual feedback
	 *
	 * @param e Event indicating what changed
	 */
    protected void validate(ItemEvent e) {
		validateAll ();
    }

    /**
     * Resets all DSEntries registered with the panel 
     * Mainly used by the panels that us DSEntry for
     * display management rather than data transfer
     */

	protected void resetAll () {	
		Enumeration values = _componentMapDSEntryTable.elements();
        while ( values.hasMoreElements() ) {
            Vector v = (Vector)values.nextElement();
			Enumeration entries = v.elements();
			while( entries.hasMoreElements() ) {
				DSEntry entry = (DSEntry)entries.nextElement();
				if (entry != null) {
					entry.reset ();
				}
			}
		}

		clearDirtyFlag ();
	}

    /**
     * Shows all DSEntries registered with the panel 
     * Mainly used by the panels that us DSEntry for
     * display management rather than data transfer
     */
    protected void showAll () {	
		Enumeration values = _componentMapDSEntryTable.elements();
        while ( values.hasMoreElements() ) {
            Vector v = (Vector)values.nextElement();
			Enumeration entries = v.elements();
			while( entries.hasMoreElements() ) {
				DSEntry entry = (DSEntry)entries.nextElement();
				if (entry != null) {
					entry.show();
				}
			}
		}
    }

    /**
     * Reverts all DSEntries registered with the panel 
     * Mainly used by the panels that us DSEntry for
     * display management rather than data transfer
     * Usually, the remoteToLocal is used to refresh
     * the model from the server, but this takes the
     * short cut of using revert to just replace the
     * model with the original model stored in the DSEntry
     * then to use show to "flush" the model to the view
     */
    protected void revertAll () { 
        Enumeration values = _componentMapDSEntryTable.elements();
        while ( values.hasMoreElements() ) {
            Vector v = (Vector)values.nextElement();
            Enumeration entries = v.elements();
            while( entries.hasMoreElements() ) {
                DSEntry entry = (DSEntry)entries.nextElement();
                if (entry != null) {
                    entry.revert();
                    entry.show();
                }
            }
        }
    }

	/* Validates all elements of the panel */
	private void validateAll () {
		int result;
		int totalResult = DSEntry.DSE_VALID_NOMOD;

        if (_verbose){		
            Debug.println ("BlankPanel.validateAll");
        }

		Enumeration components = _componentMapDSEntryTable.keys();
		if (_verbose) {
			Debug.println("BlankPanel.validateAll: components " +
						  (components != null && components.hasMoreElements()));
		}
        while (components.hasMoreElements()) {
			int thisResult = DSEntry.DSE_VALID_NOMOD;
            JComponent comp = (JComponent)components.nextElement();
			Vector v = (Vector)_componentMapDSEntryTable.get( comp );
			Enumeration entries = v.elements();
			if (_verbose) {
				Debug.println("BlankPanel.validateAll: entries " +
							  (entries != null && entries.hasMoreElements()));
			}
			while (entries.hasMoreElements()) {
				DSEntry entry = (DSEntry)entries.nextElement();
				if (entry != null) {
					result = entry.doValidate ();
					if (_verbose) {
						Debug.println("BlankPanel.validateAll: result = " + result);
					}
					/* invalid bit is set - accumulate results */
					if ((result & entry.DSE_INVALID) == entry.DSE_INVALID) {
						thisResult |= entry.DSE_INVALID;
					}
				
					/* modified bit is set - accumulate results */
					if ((result & entry.DSE_MOD) == entry.DSE_MOD) {
						thisResult |= entry.DSE_MOD;
					}
				} else if (_verbose) {
					Debug.println("BlankPanel.validateAll: entry is null");
				}
			}

            /* set appropriate color for the control's label */
			switch( thisResult ) {
			case DSEntry.DSE_VALID_MOD:
				setChangeState( comp, CHANGE_STATE_MODIFIED );
				break;
			case DSEntry.DSE_INVALID_MOD:
				setChangeState( comp, CHANGE_STATE_ERROR );
				break;
			case DSEntry.DSE_VALID_NOMOD:
				resetChangeState( comp );
				break;
			case DSEntry.DSE_INVALID_NOMOD:
				setChangeState( comp, CHANGE_STATE_ERROR );
				break;
			}
			/* invalid bit is set - need to propagate to total */
			if ((thisResult & DSEntry.DSE_INVALID) == DSEntry.DSE_INVALID)
				totalResult |= DSEntry.DSE_INVALID;
				
			/* modified bit is set - need to propagate to total */
			if ((thisResult & DSEntry.DSE_MOD) == DSEntry.DSE_MOD)
				totalResult |= DSEntry.DSE_MOD;
		}

        /* provide information to the DSTabbedPanel so that
           it can decide to enable/disable Save and Reset
           buttons. The Reset button is enabled if at list
           one panel in the Tab is modified. The Save button
           is enabled if At least one panel is modified and
           all panels are valid. That's why we call setValidFlag
           for unmodified panel even if some of its fields are not
           valid. (If it is not modified it should not prevent 
           modified panels from being saved. */		

		switch (totalResult) {
			case DSEntry.DSE_VALID_NOMOD:	clearDirtyFlag ();
                                            setValidFlag ();
						                    break;

            /* we call ***NoRepaint function sice we don't want to mark
               fields on unmodified panel as invalid 
             */
			case DSEntry.DSE_INVALID_NOMOD:	clearDirtyFlagNoRepaint ();
                                            setValidFlag ();
							                break;

			case DSEntry.DSE_VALID_MOD:	    setDirtyFlag ();
						                    setValidFlag ();
						                    break;

			case DSEntry.DSE_INVALID_MOD:	setDirtyFlag ();
						                    clearValidFlag ();
						                    break;
		}
	}

    /**
     * This method modifies the appearance of the component
	 * according the change state. It uses color or font style
	 * depending of _changeIndicator settings.
     */
    public void setChangeState(JComponent view, int changeState) {
		if (_changeIndicator == CHANGE_INDICATOR_COLOR) {
			Color color;
			switch(changeState) {
				case CHANGE_STATE_UNMODIFIED:
					color = ORIG_COLOR;
					break;
				case CHANGE_STATE_MODIFIED:
					color = (Color)UIManager.get("Label.modified");
					break;
				case CHANGE_STATE_ERROR:
					color = (Color)UIManager.get("Label.error");
					break;
				default:
					throw new IllegalStateException("Bug");
			}
			setColor(view, color);
		}
		else if (_changeIndicator == CHANGE_INDICATOR_FONT) {
			int fontStyle;
			switch(changeState) {
				case CHANGE_STATE_UNMODIFIED:
					fontStyle = Font.PLAIN;
					break;
				case CHANGE_STATE_MODIFIED:
					fontStyle = Font.BOLD;
					break;
				case CHANGE_STATE_ERROR:
					fontStyle = Font.ITALIC;
					break;
				default:
					throw new IllegalStateException("Bug");
			}
			setFontStyle(view, fontStyle);
		}
    }


    /**
     * This method resets the change state of a component.
     */
    public void resetChangeState(JComponent view) {
		setChangeState(view, CHANGE_STATE_UNMODIFIED);
	}
	
    /**
     * This method modifies the color of a component
	 * to indicate changes.
     */
    private void setColor(JComponent view, Color c) {
        if (view == null || c == null){
            return;
        }
		Color currColor;
		if (view instanceof GroupPanel)
			currColor = ((GroupPanel)view).getTitleColor();
		else
			currColor = view.getForeground();
        if (!c.equals (currColor)){
			
			if ((view instanceof JCheckBox) ||
			    (view instanceof JRadioButton))
			{
				// handle toggle buttons
				JToggleButton button = (JToggleButton)view;
				button.setForeground(c);
				button.repaint();
			}
			else if (view instanceof GroupPanel)
			{
				GroupPanel g = (GroupPanel)view;
				g.setTitleColor(c);
				g.repaint();
			}
			else if (view instanceof JLabel)
			{
				// label is always the last component
				JLabel l = (JLabel)view;
				l.setForeground(c);
				l.repaint();
			}
        }
    }


    /**
     * This method modifies the font style of a component
	 * to indicate changes.
     */
    private void setFontStyle(JComponent view, int fontStyle) {
        if (view == null){
            return;
        }
		Font currFont;
		if (view instanceof GroupPanel)
			currFont = ((GroupPanel)view).getTitleFont();
		else
			currFont = view.getFont();
			
        if (currFont.getStyle() != fontStyle){
		
			Font newFont = currFont.deriveFont(Font.PLAIN).deriveFont(fontStyle);
			
			if ((view instanceof JCheckBox) ||
			    (view instanceof JRadioButton))
			{
				// handle toggle buttons
				JToggleButton button = (JToggleButton)view;
				button.setFont(newFont);
				button.repaint();
			}
			else if (view instanceof GroupPanel)
			{
				GroupPanel g = (GroupPanel)view;
				g.setTitleFont(newFont);
				g.repaint();
			}
			else if (view instanceof JLabel)
			{
				// label is always the last component
				JLabel l = (JLabel)view;
				l.setFont(newFont);
				l.repaint();
			}
        }
    }


    /**
     *  see if the contents of the panel have been changed but not applied
     *
	 * @return true if dirty
     */
    public boolean isDirty() {
        return _dirtyFlag;
    }

    /**
     *  see if the contents of the panel is valid
     *
	 * @return true if valid
     */
    public boolean panelIsValid() {
        return _validFlag;
    }


    /**
	 * Called on hitting the Apply or Okay button
	 */
    public void okCallback() {
        if (_verbose){
            Debug.println ("BlankPanel.okCallback");
        }

        if (isDirty()) {
	  	boolean status = false;
		try {
			getModel().setWaitCursor( true );
			status = _dsEntrySet.store(_dsModel);
			if (status) {
				if ( _verbose ) {
					Debug.println("BlankPanel.okCallback: " +
								  "Changes were saved.");
				}
			} else {
				Debug.println("BlankPanel.okCallback: Error saving changes.");
				/* If there was a validation error, DSEntrySet.store() has
				   already shown it to the user */
            }
		} catch (LDAPException lde) {
			status = processSaveErrors( lde );
		} finally {
			getModel().setWaitCursor( false );
		}
		
		// if an error occurred, leave the page dirty
		if (status)
	      	clearDirtyFlag();
      }
    }

    /**
	 * Called on errors saving entry changes to the directory
	 *
	 * @param lde LDAPException on saving
	 */
    protected boolean processSaveErrors( LDAPException lde ) {
		JFrame frame = getModel().getFrame();
		if ( lde.getLDAPResultCode() == lde.UNAVAILABLE ) {
			DSUtil.showErrorDialog( frame,
									"updating-server-unavailable", "" );
		} else {
			DSUtil.showLDAPErrorDialog( frame, lde,
										"updating-directory-title" );
		}
		Debug.println("BlankPanel.processSaveErrors: " + lde);
		return false;
	}

    protected boolean processReadErrors( LDAPException lde ) {
        JFrame frame = getModel().getFrame();
		if ( lde.getLDAPResultCode() == lde.UNAVAILABLE ) {
			DSUtil.showErrorDialog(
				frame, "fetching-server-unavailable", "" );
		} else {
			DSUtil.showLDAPErrorDialog(
				frame, lde, "fetching-directory" );
		}

        Debug.println("BlankPanel.processReadErrors: " + lde);
        return false;
    }

    /**
	 * Called on hitting the Reset button
	 */
    public void resetCallback() {
        if (isDirty()) {
			if ( _verbose ) {
				Debug.println("BlankPanel.resetCallback: Changes were lost.");
			}
			if ( refresh() ){
		        clearDirtyFlag();
			}
		}
    }

    /**
	 * Called on hitting the Help button
	 */
    public void helpCallback() {
		DSUtil.help( _helpToken );
    }

    protected void setDirtyFlag() {
		if ( _verbose ) {
			Debug.println( "BlankPanel.setDirtyFlag: " +
						   getClass().getName() );
		}
        _dirtyFlag = true;
		if ( _parent != null )
			_parent.setDirtyFlag(this);
    }

    /**
	 * Set the owner panel, if any
	 *
	 * @param parent A container panel to be the new parent
	 */
	public void setParent( ContainerPanel parent ) {
		_parent = parent;
	}

    protected ContainerPanel getParentPanel() {
		return _parent;
	}

    protected DSTabbedPanel getTabbedPanel() {
		return (DSTabbedPanel)_parent;
	}

    protected void clearDirtyFlag() {
        _dirtyFlag = false;
        Enumeration values = _componentMapDSEntryTable.elements();

        while (values.hasMoreElements()) {
			Vector v = (Vector)values.nextElement();
			Enumeration en = v.elements();
			while (en.hasMoreElements()) {
				DSEntry entry = (DSEntry)en.nextElement();
				resetChangeState( entry.getView( entry.getViewCount() - 1 ) );
			}
        }

        if ( _parent != null )
			_parent.clearDirtyFlag(this);
    }

	protected void clearDirtyFlagNoRepaint () {
		_dirtyFlag = false;

        if ( _parent != null )
			_parent.clearDirtyFlag(this);
	}

	protected void setValidFlag () {
        _validFlag = true;
		if ( _parent != null )
			_parent.setValidFlag(this);
	}

	protected void clearValidFlag () {
        _validFlag = false;
		if ( _parent != null )
			_parent.clearValidFlag(this);
	}

    protected void setIconExist(boolean b) {
        _iconExist = b;
    }

    protected boolean getIconExist() {
        return _iconExist;
    }

    /*
     * Support for IDSResourceListenerModel
     *
     */

    /**
      * Called when the object is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
		if ( _verbose ) {
			Debug.println("BlankPanel.select: " + getClass().getName() );
		}
		boolean firstTimeCalled = !_isInitialized;
        if (!_isInitialized) { // first time called
			getModel().setWaitCursor( true );
            init();
			
			getModel().setWaitCursor( false );
            _isInitialized = true;
			clearDirtyFlag();
        } else if (isDirty()) { // not first time called, don't show()
            return;
        }
		if ((_refreshWhenSelect) || (firstTimeCalled)) { // The first time we call we refresh ALWAYS
			if ( refresh() ) {
				clearDirtyFlag(); // entries have been reset
			}
		}
    }

    /**
     * Only called when object is first selected.  Should only be
     * called once.
    */
    public void init() {
    }
    
    /**
      * Called when the object is unselected.
      */
    public void unselect(IResourceObject parent, IPage viewInstance) {
		if ( _verbose ) {
			Debug.println("BlankPanel.unselect: " + this );
		}
    }

	/**
	 * Set the owner dialog, if any   [New Generation]
	 *
	 * @param dialog The owner dialog.
	 */
    public void setAbstractDialog( AbstractDialog dialog ) {
		_abstractDlg = dialog;
	}


    /**
     * Update on-screen data from Directory.
     *
     **/
    public boolean refresh () {
		Debug.println( "BlankPanel.refresh:" +
								   "refreshed panel data. Class "+getClass().getName() );
		DSEntrySet set = getDSEntrySet();
		boolean status = true;
		if ( set != null ) {
		    try {		
				getModel().setWaitCursor( true );
				set.show( _dsModel );
				//if ( _verbose ) {
				//	Debug.println( "BlankPanel.refresh:" +
				//				   "refreshed panel data. Class "+getClass().getName() );
				//}
				clearDirtyFlag();
			} catch (LDAPException lde) {
				Debug.println("BlankPanel.refresh: could not" +
								   " get attributes from directory for " +
								   set.getConfigDN() + ": " + lde);
				
				status = processReadErrors (lde);
			} finally {
				getModel().setWaitCursor( false );
			}
		}
		return status;
    }

	/**
	 *
	 * Set the help token
	 */

	public void SetHelpToken(String token)
	{
		_helpToken = token;
	}
	
	
	/**
	 * Set the change indicator.
	 */
	public static void setChangeIndicator(int indicator) {
		_changeIndicator = indicator;
	}

    /**
	 * Add a label and a textfield to a panel, assumed to be using
	 * GridBagLayout.
	 */
    protected JPanel layoutHorizontalComponents( JComponent[] components ) {
		JPanel boxPanel = new JPanel();
        boxPanel.setLayout( new BoxLayout(boxPanel, BoxLayout.X_AXIS));
        boxPanel.setAlignmentX(LEFT_ALIGNMENT);
        boxPanel.setAlignmentY(TOP_ALIGNMENT);

		for( int i = 0; i < components.length; i++ ) {
			boxPanel.add( components[i] );
			if( i < (components.length-1) ) {
				boxPanel.add(Box.createHorizontalStrut(6));
			}
		}
        boxPanel.add(Box.createGlue());
		return boxPanel;
	}

    /**
	 * Add a label and a textfield to a panel, assumed to be using
	 * GridBagLayout.
	 */
    protected void addEntryField( JPanel panel, JComponent label,
								  JComponent field, JLabel label2 ) {
		Component endGlue = STRETCH_FIELDS ? null : Box.createGlue();
		Component lastItem =
			STRETCH_FIELDS ? ((label2 != null) ? label2 : field) : endGlue;
		GridBagConstraints gbc = getGBC();
		gbc.fill = gbc.NONE;
		gbc.weightx = 0.0;
        gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.anchor = gbc.EAST;
		int space = UIFactory.getComponentSpace();
		gbc.insets = new Insets( space, space, 0, space/2 );
		panel.add( label, gbc );

		gbc.gridx++;
		gbc.anchor = gbc.WEST;
		gbc.insets = new Insets( space, 0, 0, 0 );
		if ( STRETCH_FIELDS ) {
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
		}
		gbc.gridwidth = (lastItem == field) ? gbc.REMAINDER : 1;
		panel.add( field, gbc );

		if ( label2 != null ) {
			gbc.gridx++;
			gbc.fill = gbc.NONE;
			gbc.weightx = 0.0;
			gbc.insets = new Insets( space, space/2, 0, 0 );
			gbc.gridwidth = (lastItem == label2) ? gbc.REMAINDER : 1;
			panel.add( label2, gbc );
		}

		if ( !STRETCH_FIELDS ) {
			gbc.gridx++;
			gbc.anchor = gbc.EAST;
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
			gbc.gridwidth = gbc.REMAINDER;
			panel.add( endGlue, gbc );
		}
	}

    /**
	 * Add a label and a textfield to a panel, assumed to be using
	 * GridBagLayout.
	 */
	protected void addEntryField( JPanel panel, JComponent label,
	                              JComponent field, JComponent unit ) {
		Component endGlue = STRETCH_FIELDS ? null : Box.createGlue();
		Component lastItem =
		              STRETCH_FIELDS ? ((unit != null) ? unit : field) : endGlue;
		GridBagConstraints gbc = getGBC();
		gbc.fill = gbc.NONE;
		gbc.weightx = 0.0;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.anchor = gbc.EAST;
		int space = UIFactory.getComponentSpace();
		gbc.insets = new Insets( space, space, 0, space/2 );
		panel.add( label, gbc );

		gbc.gridx++;
		gbc.anchor = gbc.WEST;
		gbc.insets = new Insets( space, 0, 0, 0 );
		if ( STRETCH_FIELDS ) {
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
		}
		gbc.gridwidth = (lastItem == field) ? gbc.REMAINDER : 1;
		panel.add( field, gbc );

		if ( unit != null ) {
			gbc.gridx++;
			gbc.fill = gbc.NONE;
			gbc.weightx = 0.0;
			gbc.insets = new Insets( space, space/2, 0, 0 );
			gbc.gridwidth = (lastItem == unit) ? gbc.REMAINDER : 1;
			panel.add( unit, gbc );
		}

		if ( !STRETCH_FIELDS ) {
			gbc.gridx++;
			gbc.anchor = gbc.EAST;
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
			gbc.gridwidth = gbc.REMAINDER;
			panel.add( endGlue, gbc );
		}
	}

    /**
	 * Add a label and a textfield to a panel, assumed to be using
	 * GridBagLayout.
	 */
    protected void addEntryField( JPanel panel, JComponent label,
								  JComponent field ) {
		addEntryField( panel, label, field, null );
	}

    protected void setDSEntrySet( DSEntrySet set ) { _dsEntrySet = set; }
    protected DSEntrySet getDSEntrySet() {return _dsEntrySet;}
    
    protected IDSModel getModel() {return _dsModel;}
    
	public void setComponentTable( Object key, Object value ) {
		if ( value instanceof DSEntry ) {
			DSEntry entry = (DSEntry)value;
			JComponent comp = entry.getView( entry.getViewCount() - 1 );
			if ( comp == null ) {
				Debug.println( "BlankPanel.setComponentTable: no view " +
							   "for " + entry );
				return;
			}
			Vector v = (Vector)_componentMapDSEntryTable.get( comp );
			if ( v == null ) {
				v = new Vector();
			}
			if ( !v.contains( value ) ) {
				v.addElement( value );
			}
			_componentMapDSEntryTable.put( comp, v );
		}
	}

    protected void hideDialog() {
 		hideAbstractDialog();
 	}

    protected AbstractDialog getAbstractDialog() {
		if ( _abstractDlg == null ) {
			for (Container p = getParent(); p != null; p = p.getParent()) {
				if (p instanceof AbstractDialog) {
					_abstractDlg = (AbstractDialog)p;
					break;
				} else {
					Debug.println(8, "BlankPanel.getAbstractDialog: class " +
								  "of p = " + p.getClass());
				}
			}
		}
		return _abstractDlg;
    }

    protected void hideAbstractDialog() {
		AbstractDialog d = getAbstractDialog();
		if ( d != null ) {
			d.setVisible(false);
			d.dispose();
//			DSUtil.dialogCleanup();
		}
	}

    protected GridBagConstraints getGBC() {
        resetGBC();
		return _gbc;
	}

	/**
	 * Reset the shared GridBagConstraints to a known state.
	 */
    protected void resetGBC() {
		if ( _gbc == null )
			_gbc = new GridBagConstraints();
  		_gbc.gridwidth  = 1;
  		_gbc.gridheight = 1;
  		_gbc.fill       = _gbc.NONE;
  		_gbc.ipady      = 0;
  		_gbc.weightx    = 1.0;
  		_gbc.weighty    = 0.0;
  		_gbc.insets     = getComponentInsets();
		_gbc.insets.right = _gbc.insets.bottom = 0;
		_gbc.anchor     = _gbc.WEST;
   		_gbc.gridx      = _gbc.RELATIVE;
      	_gbc.gridy      = _gbc.RELATIVE;
	}

	public boolean isInitialized() {
		return _isInitialized;
	}
									   
	/**
	 * Is console running on the same machine as the server?
	 */
	protected boolean isLocal() {
		return DSUtil.isLocal(getModel().getServerInfo().getHost());		
	}

    protected boolean isValidSchemaSyntax(String tok) {
        StringCharacterIterator iter = new StringCharacterIterator(tok, 0);
        for (char c = iter.first(); c != CharacterIterator.DONE; c = iter.next()) {
            if (_validsyntax.indexOf(Character.toLowerCase(c)) == -1)
                return false;
        }
        return true;
    }

    static protected boolean requiresConfirmation( String item ) {
		return DSUtil.requiresConfirmation( item );
	}

	static protected SimpleDialog createDialog( JComponent comp,
												BlankPanel child ) {
		SimpleDialog dlg =
			new SimpleDialog( null,
							  child.getTitle(),
							  SimpleDialog.OK,
							  child );
		dlg.setComponent( comp );
		dlg.setDefaultButton( SimpleDialog.OK );
		dlg.pack();
		dlg.setLocation(200,200);
		return dlg;
	}

	static protected SimpleDialog createDialog( BlankPanel child ) {
		return createDialog( child, child );
	}

    /*==========================================================
	 * Factory methods to produce JComponents
     *==========================================================*/
    protected JLabel makeJLabel(Icon i, String s, int a) {
		return UIFactory.makeJLabel( i, s, a );
      }

    protected JLabel makeJLabel() {
		return makeJLabel((Icon)null, null, -1);
	}

    protected JLabel makeJLabel(Icon i) {
        return makeJLabel(i, null, -1);
    }

    protected JLabel makeJLabel(Icon i, int a) {
        return makeJLabel(i, null, a);
    }

    protected JLabel makeJLabel(String s) {
        return makeJLabel((Icon)null, s, -1);
    }

    protected JLabel makeJLabel(String s, int a) {
        return makeJLabel((Icon)null, s, a);
    }

    protected JLabel makeJLabel(String panelname, String keyword, int a,
								ResourceSet resource) {
		return UIFactory.makeJLabel( panelname, keyword, a, resource );
    }

    protected JLabel makeJLabel(String panelname, String keyword,
								ResourceSet resource) {
        return makeJLabel(panelname, keyword, -1, resource);
    }

    protected JLabel makeJLabel(String panelname, String keyword) {
        return makeJLabel(panelname, keyword, -1, (ResourceSet)null);
    }

    /**
     * Word-wrappable multi-line label
	 *
	 * @param rows Number of rows
	 * @param cols Number of columns
	 * @return a JTextArea
     */
    protected JTextArea makeMultiLineLabel( int rows, int cols ) {
		return UIFactory.makeMultiLineLabel( rows, cols );
	}

    /**
     * Word-wrappable multi-line label
	 * @param rows Number of rows
	 * @param cols Number of columns
	 * @param text Initial text
	 *
	 * @return a JTextArea
     */
    protected JTextArea makeMultiLineLabel( int rows, int cols,
											String text ) {
		return UIFactory.makeMultiLineLabel( rows, cols, text );
	}

    protected JTextField makeJTextField(Document d, String s, int len) {
		return UIFactory.makeJTextField( this, d, s, len );
    }

    protected JTextField makeJTextField() {
        return makeJTextField(null, null, -1);
    }

    protected JTextField makeJTextField(int len) {
        return makeJTextField(null, null, len);
    }

    protected JTextField makeJTextField(String s) {
        return makeJTextField(null, s, -1);
    }

    protected JTextField makeJTextField(String s, int len) {
        return makeJTextField(null, s, len);
    }

    protected JTextField makeJTextField( String panelname,
										 String keyword,
										 String defval,
										 int defcols,
										 ResourceSet resource ) {
		return UIFactory.makeJTextField( this, panelname, keyword,
										 defval, defcols, resource );
    }

    protected JTextField makeJTextField(
        String panelname, String keyword, String defval, int defcols
    ) {
		return makeJTextField( panelname, keyword, defval, defcols, null );
	}

    protected JTextField makeJTextField(String panelname, String keyword,
										ResourceSet resource) {
		return makeJTextField( panelname, keyword, null, -1, resource );
	}

    protected JTextField makeJTextField(String panelname, String keyword) {
		return makeJTextField( panelname, keyword, (ResourceSet)null );
	}

    protected JTextField makeNumericalJTextField(Document d, String s, int len) {
		return UIFactory.makeNumericalJTextField( this, d, s, len );
    }

    protected JTextField makeNumericalJTextField() {
        return makeNumericalJTextField(null, null, -1);
    }

    protected JTextField makeNumericalJTextField(int len) {
        return makeNumericalJTextField(null, null, len);
    }

    protected JTextField makeNumericalJTextField(String s) {
        return makeNumericalJTextField(null, s, -1);
    }

    protected JTextField makeNumericalJTextField(String s, int len) {
        return makeNumericalJTextField(null, s, len);
    }

    protected JTextField makeNumericalJTextField( String panelname,
												  String keyword,
												  String defval,
												  int defcols,
												  ResourceSet resource ) {
		return UIFactory.makeNumericalJTextField( this, panelname, keyword,
												  defval, defcols, resource );
    }

    protected JTextField makeNumericalJTextField(
        String panelname, String keyword, String defval, int defcols
    ) {
		return makeNumericalJTextField( panelname, keyword, defval, defcols,
										null );
	}

    protected JTextField makeNumericalJTextField(String panelname,
												 String keyword,
												 ResourceSet resource) {
		return makeNumericalJTextField( panelname, keyword, null, -1,
										resource );
	}

    protected JTextField makeNumericalJTextField(String panelname,
												 String keyword) {
		return makeNumericalJTextField( panelname, keyword, (ResourceSet)null );
	}

    protected JPasswordField makeJPasswordField() {
        return makeJPasswordField(null, null, -1);
    }

    protected JPasswordField makeJPasswordField(Document d, String s,
												int len) {
		return UIFactory.makeJPasswordField( this, d, s, len );
    }

    protected JPasswordField makeJPasswordField(int len) {
        return makeJPasswordField(null, null, len);
    }

    protected JPasswordField makeJPasswordField(String s) {
        return makeJPasswordField(null, s, -1);
    }

    protected JPasswordField makeJPasswordField(String s, int len) {
        return makeJPasswordField(null, s, len);
    }

    protected JPasswordField makeJPasswordField( String panelname,
												 String keyword,
												 String defval,
												 int defcols,
												 ResourceSet resource ) {
		return UIFactory.makeJPasswordField( this, panelname, keyword,
											 defval, defcols, resource );
    }

    protected JPasswordField makeJPasswordField(
        String panelname, String keyword, String defval, int defcols
    ) {
        return makeJPasswordField(panelname, keyword, defval,
								  defcols, (ResourceSet)null);
	}

    protected JButton makeJButton(String s, Icon i) {
		return UIFactory.makeJButton( this, s, i );
    }

    protected JButton makeJButton() {return makeJButton(null, (Icon)null);}

    protected JButton makeJButton(String s) {
        return makeJButton(s, (Icon)null);
    }

    protected JButton makeJButton(Icon i) {
        return makeJButton(null, i);
    }

    protected JButton makeJButton(String panelname, String keyword,
								  ResourceSet resource) {
		return UIFactory.makeJButton( this, panelname, keyword, resource );
    }

    protected JButton makeJButton(String panelname, String keyword) {
		return makeJButton( panelname, keyword, (ResourceSet)null );
	}

	protected JCheckBox makeJCheckBox(String s, Icon i, boolean b) {
		return UIFactory.makeJCheckBox( this, s, i, b );
    }

    protected JCheckBox makeJCheckBox() {
        return makeJCheckBox(null, (Icon)null, false);
    }

    protected JCheckBox makeJCheckBox(Icon i) {
        return makeJCheckBox(null, i, false);
    }

    protected JCheckBox makeJCheckBox(Icon i, boolean b) {
        return makeJCheckBox(null, i, b);
    }

    protected JCheckBox makeJCheckBox(String s) {
        return makeJCheckBox(s, (Icon)null, false);
    }

    protected JCheckBox makeJCheckBox(String s, boolean b) {
        return makeJCheckBox(s, (Icon)null, b);
    }

    protected JCheckBox makeJCheckBox(String s, Icon i) {
        return makeJCheckBox(s, i, false);
    }

    protected JCheckBox makeJCheckBox(
        String panelname, String keyword, boolean defval,
		ResourceSet resource) {
		return UIFactory.makeJCheckBox( this, panelname, keyword,
										defval, resource );
    }

    protected JCheckBox makeJCheckBox(
        String panelname, String keyword, boolean defval) {
		return makeJCheckBox( panelname, keyword, defval, (ResourceSet)null );
	}

    protected JCheckBox makeJCheckBox(
        String panelname, String keyword
    ) {
        return makeJCheckBox(panelname, keyword, false);
    }

    protected JComboBox makeJComboBox(ComboBoxModel cbm) {
		return UIFactory.makeJComboBox( this, cbm );
    }

    protected JComboBox makeJComboBox() {return makeJComboBox(null);}

    protected JComboBox makeJComboBox(
        String panelname, String keyword, String defval,
		ResourceSet resource) {
		return UIFactory.makeJComboBox( this, panelname, keyword,
										defval, resource );
    }

    protected JComboBox makeJComboBox(
        String panelname, String keyword, String defval
    ) {
		return makeJComboBox( panelname, keyword, defval, (ResourceSet)null );
	}

    protected JList makeJList(Vector listData) {
		return UIFactory.makeJList( this, listData );
    }

    protected JList makeJList(
        String panelname, String keyword, String defval,
		ResourceSet resource) {
		return UIFactory.makeJList( this, panelname, keyword,
									defval, resource );
    }

    protected JList makeJList(
        String panelname, String keyword, String defval
    ) {
		return makeJList( panelname, keyword, defval, (ResourceSet)null );
	}

    protected JRadioButton makeJRadioButton(String s, Icon i, boolean b) {
		return UIFactory.makeJRadioButton( this, s, i, b );
    }

    protected JRadioButton makeJRadioButton() {
        return makeJRadioButton(null, (Icon)null, false);
    }

    protected JRadioButton makeJRadioButton(Icon i) {
        return makeJRadioButton(null, i, false);
    }

    protected JRadioButton makeJRadioButton(Icon i, boolean b) {
        return makeJRadioButton(null, i, b);
    }

    protected JRadioButton makeJRadioButton(String s) {
        return makeJRadioButton(s, (Icon)null, false);
    }

    protected JRadioButton makeJRadioButton(String s, boolean b) {
        return makeJRadioButton(s, (Icon)null, b);
    }

    protected JRadioButton makeJRadioButton(
        String panelname, String keyword, boolean defvalue,
		ResourceSet resource) {
		return UIFactory.makeJRadioButton( this, panelname, keyword,
										   defvalue, resource );
    }

    protected JRadioButton makeJRadioButton(
        String panelname, String keyword
    ) {
        return makeJRadioButton(panelname, keyword, false);
    }

    protected JRadioButton makeJRadioButton(
        String panelname, String keyword, boolean defvalue
    ) {
		return makeJRadioButton( panelname, keyword, defvalue,
								 (ResourceSet)null );
	}

    protected JRadioButton makeJRadioButton(String s, Icon i) {
        return makeJRadioButton(s, i, false);
    }

    protected JToolTip makeJToolTip(String s) {
		return UIFactory.makeJToolTip( s );
    }

    /**
     * Convenience for setting tool tip text, which may be null
     *
     * @param panelname from properties file
     * @param keyword from properties file
     * @param w widget to set tool tip for
     */
    protected void setToolTip(String panelname, String keyword, JComponent w,
							  ResourceSet resource) {
		UIFactory.setToolTip( panelname, keyword,
									 w, resource );
    }

    protected void setToolTip(String panelname, String keyword, JComponent w) {
		setToolTip( panelname, keyword, w, (ResourceSet)null );
	}

    protected JToolTip makeJToolTip() {return makeJToolTip(null);}


    protected Insets getStdInsets() {return UIFactory.getStdInsets();}

    protected Insets getBorderInsets() {return UIFactory.getBorderInsets();}

    protected Insets getComponentInsets() {
		return UIFactory.getComponentInsets();
	}

    protected Insets getTextInsets() {return UIFactory.getTextInsets();}


	/**
	 * Set/unset the busy cursor.
	 * Invoke SimpleDialog.setWaitCursor (if inside a DSDialog).
	 * Invoke IDSModel.setWaitCursor (if attached to an IDSModel);
	 */
	protected void setWaitCursor(boolean wait) {
		int count = 0;
		if (_dsModel != null) {
			Debug.println("BlankPanel.setWaitCursor: changing on attached model");
			_dsModel.setWaitCursor(wait);
			count++;
		}
		if (count == 0) {
			Debug.println(0, "BlankPanel.setWaitCursor: unable to change the cursor");
		}
	}
	
    /*==========================================================
     * variables
     *==========================================================*/   
									   
    protected GridBagConstraints _gbc = null;
	private AbstractDialog _abstractDlg = null;
	private ContainerPanel _parent = null;
	protected String _helpToken = "framework-menubar-index";

	protected boolean _refreshWhenSelect = true;

    private IDSModel _dsModel;
	String _title = null;      // Title of pane
    protected JPanel _myPanel; // this panel will contain the subclass panels
                               // content
                               
    private DSEntrySet _dsEntrySet; // The set of attributes in the DS
                                    // which we are supplying views of

    private boolean _dirtyFlag = false; // set to true if the panel contents
                                        // have been edited
    private boolean _validFlag = true; // set if the content of panel is valid

    // set to true when the panel is selected.
    protected boolean _isInitialized = false;

	static final boolean STRETCH_FIELDS = false;

    // This prevents controls & DSAttributes from
	// being created multiple times
    // set to true after the first ChangeEvent.
	// The first change event occurs during panel construction;
	// we need to ignore this event
    private boolean isValidChangeEvent = false; 

	// Change states
	// Component color or font style varies according their change state.
	public static final int CHANGE_STATE_UNMODIFIED = 1;
	public static final int CHANGE_STATE_MODIFIED   = 2;
	public static final int CHANGE_STATE_ERROR      = 3;
	
	// Changes can be indicated using colors or fonts.
	public static final int CHANGE_INDICATOR_COLOR = 1;
	public static final int CHANGE_INDICATOR_FONT  = 2;
	public static final int CHANGE_INDICATOR_NONE  = 3;
	private static int _changeIndicator = CHANGE_INDICATOR_COLOR;
	
    private Hashtable _componentMapDSEntryTable = new Hashtable();
    protected boolean _iconExist = false;
	protected static final boolean _verbose = (Debug.getTraceLevel() > 5);
    public static final Color ORIG_COLOR = Color.black;
    public static final Color LABEL_MODIFIED = new Color(102, 102, 153);
    protected static final String _validsyntax = "abcdefghijklmnopqrstuvwxyz0123456789-_";
}

