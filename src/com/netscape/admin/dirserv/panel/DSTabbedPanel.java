/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IChangeClient;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.Debug;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class DSTabbedPanel extends ContainerPanel {
	public DSTabbedPanel(IDSModel model, boolean doButtons) {
		super(model, doButtons);

		_tabbedPane = new JTabbedPane();
       
		add( _tabbedPane, "Center" );
		_tabbedPane.addChangeListener( this );
        setMinimumSize(new Dimension(1,1));
    }
    
	public DSTabbedPanel(IDSModel model) {
		this( model, true );
	}

    protected void addTab(BlankPanel p) {
		_tabbedPane.addTab(p.getTitle(), p);
		_tabbedPane.addChangeListener(p);
		p.setParent( this );
	}

    protected BlankPanel getSelectedPanel() {
        return (BlankPanel)_tabbedPane.getSelectedComponent();
	}

    /*
     * Support for IDSResourceListenerModel
     *
     */

    /**
      * Called when the TabbedPane is selected.  We need to call the first
      * pane's select so the pane's controls are created and populated.  (No
      * ChangeEvent is created when TabbedPane gets focus.)
      */
    public void select(IResourceObject parent, IPage viewInstance) {
        Debug.println( "DSTabbedPanel.select: " + this );
		super.select( parent, viewInstance );
        _tabbedPane.invalidate();
        _tabbedPane.validate();
    }
    
    /**
      * Called when the object is unselected.
      */
    public void unselect(IResourceObject parent, IPage viewInstance) {
/**** We will not be prompting to save changes any more, as of 02/28/98,
  except if the whole console is dismissed. For now, we can't even do
  that, since there is no callback for this event. */
		/*
		if ( !promptToSaveChanges( parent, viewInstance ) ) {
		    // force this panel to be selected again
			Debug.println("DSTabbedPanel: unselect " +
			              viewInstance.toString());
		    TreePath thisPath =
			    new TreePath(((ResourceObject)parent).getPath());
            TreePath selectedPath =
        	        ((DSResourcePage)viewInstance).
        	            getTree().getSelectionPath();
        	    
       	    if (!thisPath.equals(selectedPath)) {
       	        ((DSResourcePage)viewInstance).getTree().
       	            setSelectionPath(thisPath);
   	    }
		*/
    }

    public void okCallback() {
        int nTabs = _tabbedPane.getTabCount();
        for (int ii = 0; ii < nTabs; ++ii) {
            BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);
			if ( p._isInitialized ) {
				p.okCallback();
			}
        }
        
        if ( !isDirty() ) { // all panels successfully updated
            clearDirtyFlag ();
        } else {
            Debug.println("DSTabbedPanel.okCallback(): " +
						  "panels are still dirty after ok");
        }
    }

    public void resetCallback() {
        int nTabs = _tabbedPane.getTabCount();
        for (int ii = 0; ii < nTabs; ++ii) {
            BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);
			if ( p._isInitialized ) {
				p.resetCallback();
			}
        }
        
        if ( !isDirty() ) { // all panels successfully reset
            clearDirtyFlag ();
        } else {
            Debug.println("DSTabbedPanel.resetCallback(): " +
						  "panels are still dirty after reset");
        }
    }

	/**
	 * Report if any managed panels have unsaved data.
	 * @return true if any panels have unsaved data.
	 */
    public boolean isDirty() {
        int nTabs = _tabbedPane.getTabCount();
        for (int ii = 0; ii < nTabs; ++ii) {
            BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);
            if (p.isDirty()) {
                return true;
            }
        }
        return false;
    }

	/**
	 * Put up a dialog with a list of panels containing unsaved changes.
	 * Based on user selection, either save the changes, discard the
	 * changes, or return false.
	 */
/* Not currently used, after a decision by Netscape UE
    private boolean promptToSaveChanges( IResourceObject parent,
										 IPage viewInstance ) {
		if ( isDirty() ) {

            // popup a dialog asking the user to save/discard
            // disable Apply/Reset buttons
			String dirtyList = getDirtyPanels();
            String args[] = {dirtyList};
            int ans = DSUtil.showConfirmationDialog(
				null, "108", args, "general");
		    switch (ans) {
		    case JOptionPane.YES_OPTION:
		        okCallback();
		        break;
		    case JOptionPane.NO_OPTION:
		        resetCallback();
		        break;
		    default: // cancel
		        break;
		    }
		    
		    // see if any panels are still dirty, which may happen in 1 of
		    // 3 cases: the save was unsuccessful, the reset was unsuccessful,
		    // or the user hit Cancel
            if ( isDirty() ) {
				return false;
		    } else { // disable the Apply/Reset buttons
				clearDirtyFlag ();
		    }
        }
		if ( !isDirty() ) {
			if ( _selectedPanel != null )
				_selectedPanel.unselect(parent, viewInstance);
		}
		return true;
    }
*/

    public void setDirtyFlag(JPanel p) {
        if (((BlankPanel)p).getIconExist() == false) {
			if ( _markImage == null ) {
				String imageName = DSUtil._resource.getString( "general",
															   _tabImageName );
				_markImage = DSUtil.getPackageImage( imageName );
			}
            ((BlankPanel)p).setIconExist(true);
            int i = _tabbedPane.indexOfComponent(p);
            _tabbedPane.setIconAt(i, (Icon)_markImage);  
            _tabbedPane.invalidate();
            _tabbedPane.validate();
            _tabbedPane.repaint();
        }
        setDirtyFlag();
    }
 
    public void clearDirtyFlag(JPanel p) {
        if (((BlankPanel)p).getIconExist() == true) {
            ((BlankPanel)p).setIconExist(false);
            int i = _tabbedPane.indexOfComponent(p);
            _tabbedPane.setIconAt(i, null);  
            _tabbedPane.invalidate();
            _tabbedPane.validate();
            _tabbedPane.repaint();
        }

        /* check if all panels are unedited */
        /* if so, disable Reset button      */
        
        if (!isModified ())
            clearDirtyFlag();
    }

    /**
     * called if one of the panel's have valid data
     * ready to be saved                          
     * @param - pane that have the valid data       
     */
    public void setValidFlag (JPanel p){
        /* need to check that there is panels with */       
        /* invalid data                            */

        if (isModified() && tabbedPanelsAreValid ())
            setValidFlag ();
    }

    /** 
     * called if one of the panels is no longer valid 
     * need to disable OK button                      
     * @param - pane that contains invalid data       
     */

    public void clearValidFlag (JPanel p) {

        /* call Container panel function to disable */
        /* OK button                                */
        clearValidFlag ();
    }

/* Not currently used, after a decision by Netscape UE
    private String getDirtyPanels() {
        StringBuffer dirtyPanel = new StringBuffer();
        boolean firsttime = true;
        int nTabs = _tabbedPane.getTabCount();
        for (int ii = 0; ii < nTabs; ++ii) {
            BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);
            if (p.isDirty()) {
                if (!firsttime)
                    dirtyPanel.append(",");
                dirtyPanel.append(p.getTitle());
                firsttime = false;
            }
        }
		return dirtyPanel.toString();
	}
*/
    /**
     * checks if any of the panels are modified
     * @return true if they are
     */

    public boolean isModified (){
        int nTabs = _tabbedPane.getTabCount();
        for (int ii = 0; ii < nTabs; ++ii) {
            BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);
            if (p.isDirty()) {
                return true;
            }
        }

        return false;
    }

    /**
     * checks if all panels are valid
     * @return true if they are
     */

    public boolean tabbedPanelsAreValid (){
        if (_tabbedPane == null) {
            return true; // valid if not initialized
        }
        int nTabs = _tabbedPane.getTabCount();
        for (int ii = 0; ii < nTabs; ++ii) {
            BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);
            if (!p.panelIsValid()) {
                return false;
            }
        }
    
        return true;
    }

	/**
     *  handle incoming event
     *
	 * Overwrites the ContainerPanel (this one refreshes ALL the panels,
	 * not only the selected one)
	 *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {
			int nTabs = _tabbedPane.getTabCount();
			for (int ii = 0; ii < nTabs; ++ii) {
				BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);			
				if (( p != null ) &&
					p.isInitialized()) {					
					p.refresh();
				}
			}			
		} else {
			super.actionPerformed(e);
		}
	}

	protected JTabbedPane _tabbedPane = null;
	private static final String _tabImageName = "tab-gif";
    private static RemoteImage _markImage = null;
}
