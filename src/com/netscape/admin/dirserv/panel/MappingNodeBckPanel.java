/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;
/*
import java.io.File;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.Enumeration;
import java.util.Hashtable;
*/
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.admin.dirserv.panel.BckListPanel;
import com.netscape.management.nmclf.SuiConstants;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @see     com.netscape.admin.dirserv
 */

public class MappingNodeBckPanel extends BlankPanel  
    implements SuiConstants {

    public MappingNodeBckPanel( IDSModel model,
				LDAPEntry entry ) {
	super( model, _section );
	_helpToken = "configuration-mapping-database-help";
	_model = model;
	_entry = entry;
	_refreshWhenSelect = false;
    }


    public void init() {
		if (_isInitialized) {
			return;
		}
	_myPanel.setLayout( new GridBagLayout() );
	GridBagConstraints gbc = getGBC();

	DSEntrySet entries = getDSEntrySet();	

	// Mapping Tree info
	createDataArea( _myPanel, entries );
	createValueArea( _myPanel, entries );
	addBottomGlue ();
	_isInitialized = true;
    }


    public void reloadDBList( LDAPEntry entry){
	_entry = entry;
	_savebckList = getBackendListInEntry();
	if ( _bckData != null ){
	    // Reload db list 
	    _bckData.removeAllElements();
	    for(int i=0;( _savebckList != null) && 
		    (i < _savebckList.length); i++ ){
		_bckData.addElement( _savebckList[i]);
	    }
	    // Control distrib 
	    checkBckNumber();
	}
    }	

    public int nbBck(){
		if (_isInitialized) {
			return ( _bckList==null?0:_bckList.getModel().getSize() );			
		} else {
			/* The case where the MappingNodeSettingPanel calls this method 
			   and the panel has not been initialized */			
			String[] backends = getBackendListInEntry();
			if (backends != null) {
				return backends.length;
			}
			return 0;
		}
    }
	
    protected void createDataArea( JPanel grid, DSEntrySet entries ) {
		
	GridBagConstraints gbc = getGBC();
	JPanel Mapanel = new GroupPanel( DSUtil._resource.getString( _section, "general-title" ),
					 true);
        gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
        grid.add( Mapanel, gbc );

	GridBagLayout Mabag = new GridBagLayout();
	GridBagConstraints Magbc = new GridBagConstraints() ;
        Magbc.gridx      = 0;
        Magbc.gridy      = 0;
	//        Magbc.gridwidth  = Magbc.REMAINDER;
        Magbc.gridwidth	 = 1;
        Magbc.gridheight = 1;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
        Magbc.fill       = Magbc.BOTH;
        Magbc.anchor     = Magbc.CENTER;
        Magbc.insets     = new Insets(0, 0, 0, 0);
        Magbc.ipadx = 0;
        Magbc.ipady = 0;
	   	
	Mapanel.setLayout(Mabag);


	/* Backends list label */
		
	_bckListLabel = makeJLabel ( _section, "bck-list" );
	Magbc.gridy++;
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.NONE;
	Magbc.anchor = Magbc.WEST;		
	Mapanel.add( _bckListLabel,Magbc );



	/* Backend List */
	_bckData = new DefaultListModel();
	_bckList = new JList( _bckData );
	_bckListLabel.setLabelFor(_bckList);
	_bckList.setVisibleRowCount( ROWS );
	_bckList.setCellRenderer( new BackendCellRenderer() );
	_savebckList = getBackendListInEntry();
	for(int i=0;(_savebckList != null) && i < _savebckList.length; i++ ){
	    _bckData.addElement( _savebckList[i]);
	}

	//		_bckList.setSelectionMode( _bckList.MULTIPLE_INTERVAL_SELECTION );
	JScrollPane scrollBck = new JScrollPane();
	scrollBck.getViewport().setView(_bckList);
	Magbc.gridy++;
	Magbc.gridx = 0;
	Magbc.fill= Magbc.BOTH;		
        Magbc.weightx    = 1;
        Magbc.weighty    = 1;
	Mapanel.add( scrollBck, Magbc );

	_bAddBck = UIFactory.makeJButton(this, _section, "badd", (ResourceSet)null);
	_bAddBck.setActionCommand(ADD_BCK);		
	_bAddBck.setEnabled( true );
	Magbc.gridwidth  = 1;
	Magbc.gridx = 1;
	Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.insets     = new Insets(0, 
				      DIFFERENT_COMPONENT_SPACE, 
				      COMPONENT_SPACE,
				      0);
	Magbc.fill= Magbc.HORIZONTAL;	
	Magbc.anchor = Magbc.NORTHEAST;
	
	//		Magbc.anchor = Magbc.EAST;
	Mapanel.add( _bAddBck,Magbc);


	_bDeleteBck = UIFactory.makeJButton(this, _section, "bdelete", (ResourceSet)null);
	_bDeleteBck.setActionCommand(DELETE_BCK);
	_bDeleteBck.setEnabled(false);
	Magbc.gridwidth  = 1;
	Magbc.gridx = 1;
	//Magbc.gridy++;
	Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.HORIZONTAL;		
	Magbc.insets = new Insets(0, DIFFERENT_COMPONENT_SPACE, 0, 0);
	//Magbc.anchor = Magbc.NORTHEAST;
	Magbc.anchor = Magbc.EAST;
	Mapanel.add( _bDeleteBck,Magbc);
    }

    protected void createValueArea( JPanel grid, DSEntrySet entries ) {
	GridBagConstraints gbc = getGBC();
	JPanel Mapanel = new GroupPanel( DSUtil._resource.getString( _section, "distrib-title" ),
					 true);
        gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
        grid.add( Mapanel, gbc );

	GridBagLayout Mabag = new GridBagLayout();
	GridBagConstraints Magbc = new GridBagConstraints() ;
        Magbc.gridx      = 0;
        Magbc.gridy      = 0;
	//        Magbc.gridwidth  = Magbc.REMAINDER;
        Magbc.gridwidth	 = 1;
        Magbc.gridheight = 1;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
        Magbc.fill       = Magbc.BOTH;
        Magbc.anchor     = Magbc.CENTER;
        Magbc.insets     = new Insets(0, 0, 0, 0);
        Magbc.ipadx = 0;
        Magbc.ipady = 0;
	   	
	Mapanel.setLayout(Mabag);


	_LibLabel =  makeJLabel( _section, "distrib" );
	Magbc.gridy++;
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.NONE;
	Magbc.anchor = Magbc.WEST;		
	Magbc.insets     = new Insets(0,
				      0,
				      COMPONENT_SPACE,
				      0);
	Mapanel.add( _LibLabel,Magbc );

		
	_LibText = makeJTextField( _section, "distrib" );
	_LibLabel.setLabelFor(_LibText);
	Magbc.gridx++;
	Magbc.gridwidth  = 1;
        Magbc.weightx    = 1;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.BOTH;
	Magbc.insets     = new Insets(0, 0, COMPONENT_SPACE, 0);
	Magbc.anchor = Magbc.CENTER;
	Magbc.insets     = new Insets(0,
				      0,
				      COMPONENT_SPACE,
				      0);
	if( _entry.getAttribute("nsslapd-distribution-plugin") == null ) {
	    _saveLib = "";
	} else {
	    _saveLib = _entry.getAttribute("nsslapd-distribution-plugin").getStringValueArray()[0];
	} 
		
	_LibText.setText( _saveLib );
	Mapanel.add( _LibText,Magbc);

	_bLibBrowse =  UIFactory.makeJButton(this, _section, "distrib-button", (ResourceSet)null);
	_bLibBrowse.setActionCommand( BROWSE );		
		
	Magbc.gridwidth  = 1;
	Magbc.gridx++;
	Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.insets     = new Insets(0,
				      DIFFERENT_COMPONENT_SPACE,
				      COMPONENT_SPACE,
				      0);
	Magbc.fill= Magbc.NONE;		
	Magbc.anchor = Magbc.EAST;		
	Mapanel.add( _bLibBrowse,Magbc);

	_functDistribLabel =  makeJLabel( _section, "distrib-function" );
	Magbc.gridy++;
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.NONE;
	Magbc.anchor = Magbc.WEST;
	Magbc.insets     = new Insets(0,
				      0,
				      COMPONENT_SPACE,
				      0);
	Mapanel.add( _functDistribLabel,Magbc );

		
	_functDistribText = makeJTextField( _section, "distrib-function" );
	_functDistribLabel.setLabelFor(_functDistribText);
	Magbc.gridx++;
	Magbc.gridwidth  = 1;
        Magbc.weightx    = 1;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.BOTH;
	Magbc.insets     = new Insets(0, 0, COMPONENT_SPACE, 0);
	Magbc.anchor = Magbc.CENTER;
	Magbc.insets     = new Insets(0,
				      0,
				      COMPONENT_SPACE,
				      0);
	if( _entry.getAttribute("nsslapd-distribution-funct") == null ) {
	    _saveFunct = "";
	} else {
	    _saveFunct = 
			_entry.getAttribute("nsslapd-distribution-funct").getStringValueArray()[0];
	} 
		
	_functDistribText.setText( _saveFunct );

	Mapanel.add( _functDistribText,Magbc);
	checkBckNumber();		
	addBottomGlue();

    }

    private String[] getBackendListInEntry() {
	Vector v = new Vector(1);
  
	LDAPAttribute attr = _entry.getAttribute( "nsslapd-backend" );
	if(attr != null) {
	    return attr.getStringValueArray();
	} else {
	    return null;
	}	
    }

    private String[] getReferralListInEntry() {
	Vector v = new Vector(1);

	LDAPAttribute attr = _entry.getAttribute( "nsslapd-Referral" );
	if(attr != null) {
	    return attr.getStringValueArray();
	} else {
	    return null;
	}
  
    }


    private boolean isListEmpty(JList list){
	boolean testlist = (list.getModel().getSize() == 0);
	return( testlist );
    }

    private void clearBackendsChanges() {
	clearListChanges( _bckListLabel, _bckData, _savebckList );
	_isBckDirty = false;
    }

    private void clearDistribution() {
	_LibText.setText( _saveLib );
	setChangeState( _LibLabel, CHANGE_STATE_UNMODIFIED );
	_functDistribText.setText( _saveFunct );
	setChangeState( _functDistribLabel, CHANGE_STATE_UNMODIFIED );		
    }

    private void clearListChanges( JComponent view, DefaultListModel _listMod, String[] _saveList ) {		
	for (int i = _listMod.getSize() - 1; i>= 0;i--) {					
	    _listMod.removeElementAt(i);
	}
	for(int i=0;( _saveList != null ) && ( i < _saveList.length ); i++) {
	    _listMod.addElement( _saveList[i]);
	}
	setChangeState( view, CHANGE_STATE_UNMODIFIED );
    }

										 
    private String[] getAllBackendsInList() {
	Vector v = new Vector();
		
	for (int i = 0;
	     i < _bckList.getModel().getSize();
	     i++) {
	    v.addElement( _bckList.getModel().getElementAt( i ));
	}
	if( v.size()  == 0 ) {
	    return ( null );
	} else {
	    String backendsList[] = new String[ v.size() ];
	    v.toArray( backendsList );
	    return ( backendsList );
	}
    }
	
    private void checkBckDirty() {
		
	String[] bck = getAllBackendsInList();
	Debug.println("MappingNodeBckPanel.checkBckDirty():");
	String[] diffMore = MappingUtils.whatsMoreInThisList( _savebckList, bck);
	String[] diffLess = MappingUtils.whatsMoreInThisList( bck , _savebckList );
	_isBckDirty = ((( diffMore != null) && ( diffMore.length > 0 )) || 
		       (( diffLess != null) &&( diffLess.length > 0 )));
	if ( _isLibDirty || _isFctDirty || _isBckDirty ) {
	    setDirtyFlag();
	    setChangeState( _bckListLabel, CHANGE_STATE_MODIFIED);
	} else {
	    clearDirtyFlag();
	    setChangeState( _bckListLabel, CHANGE_STATE_UNMODIFIED );
	}
    }

    public void checkDistribDirty() {
		
	if (( _LibText == null ) || ( _functDistribText == null )) return;
	String Libpath = _LibText.getText();
	String FctName = _functDistribText.getText();
	boolean isLibpathEmpty = (Libpath == null) || (Libpath.trim().length() == 0);
	boolean isFctNameEmpty = (FctName == null) || (FctName.trim().length() == 0);

	boolean isOriLibpathEmpty = (_saveLib == null) || (_saveLib.trim().length() == 0);
	boolean isOriFctNameEmpty = (_saveFunct == null) || (_saveFunct.trim().length() == 0);

	// 		Debug.println("MappingNodeBckPanel.checkDistribDirty()");
	// 		Debug.println(" *** isLibpathEmpty :" + isLibpathEmpty);
	// 		Debug.println(" *** isOriLibpathEmpty:" + isOriLibpathEmpty);
	// 		Debug.println(" *** isFctNameEmpty:" + isFctNameEmpty);
	// 		Debug.println(" *** isOriFctNameEmpty:" + isOriFctNameEmpty);
	// 		Debug.println(" *** Libpath =" + Libpath);
	// 		Debug.println(" *** FctName =" + FctName);
	// 		Debug.println(" *** _saveLib =" + _saveLib);
	// 		Debug.println(" *** _saveFunct =" + _saveFunct);


	if ( isLibpathEmpty ) {
	    if ( isOriLibpathEmpty ) {
		_isLibDirty =  false ;			   
	    } else {
		_isLibDirty =  true;
				
	    }
	} else {
	    if ( isOriLibpathEmpty ) {
		_isLibDirty =  true ;
	    } else {
		_isLibDirty = ( Libpath.trim().compareTo( _saveLib.trim()) != 0 );
		Debug.println(" (compare) _isLibDirty:" + _isLibDirty);
	    }
	}
	/* update label colors */
	if( _isLibDirty ) {
	    setChangeState( _LibLabel, CHANGE_STATE_MODIFIED);
	} else {
	    setChangeState( _LibLabel, CHANGE_STATE_UNMODIFIED );
	}

	if ( isFctNameEmpty ) {
	    if ( isOriFctNameEmpty ) {
		_isFctDirty =  false ;
	    } else {
		_isFctDirty =  true;
	    }
	} else {
	    if ( isOriFctNameEmpty ) {
		_isFctDirty =  true ;
	    } else {
		_isFctDirty = ( FctName.trim().compareTo( _saveFunct.trim()) != 0);
		Debug.println(" (compare) _isFctDirty:" + _isLibDirty );
	    }
	}

	// Manage Labels colors
	if( _isFctDirty ) {
	    setChangeState( _functDistribLabel, CHANGE_STATE_MODIFIED);
	} else {
	    setChangeState( _functDistribLabel, CHANGE_STATE_UNMODIFIED );
	}
	// Manage Dirty flag
	if ( _isLibDirty || _isFctDirty || _isBckDirty ) {
	    Debug.println("MappingNodeBckPanel.checkDistribDirty Dirty");
	    setDirtyFlag();
	} else {
	    clearDirtyFlag();
	}
    }

    private void checkBckNumber() {
	boolean bnb =  _bckList.getModel().getSize() > 1;
	Debug.println("MappingNodeBckPanel.checkBckNumber() nb =" +  _bckList.getModel().getSize() );
	_functDistribText.setEnabled( bnb );
	_functDistribLabel.setEnabled( bnb );
	_LibText.setEnabled( bnb );
	_LibLabel.setEnabled( bnb );
	_bLibBrowse.setEnabled( bnb );
    }


    /**
     * Enable/disable OK button
     *
     * @param ok true to enable the OK button
     */
    private void setOkay( boolean ok ) {
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( ok );
	} else {
	}
    }

    private void checkOkay() {
	if(( _LibText != null ) && (_functDistribText != null )) {
	    String Libpath = _LibText.getText();
	    String FctName = _functDistribText.getText();
			
	    boolean isLibpathEmpty = ((Libpath == null) || (Libpath.trim().length() == 0));
	    boolean isFctNameEmpty = ((FctName == null) || (FctName.trim().length() == 0));

	    boolean ok = ( !isLibpathEmpty && !isFctNameEmpty ); 
	    setOkay( ok  );
	}
	modelUpdate();
    }


    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
	if ( e.getActionCommand().equals( ADD_BCK )) {
	    String[] nbck = addBackend();
	    if ( nbck != null ) {
		for (int i = nbck.length - 1; i>= 0;i--) {					
		    _bckData.addElement(nbck[i]);
		}
		checkBckDirty();
		checkBckNumber();

	    }
	} else if ( e.getActionCommand().equals( DELETE_BCK )){
	    int[] indices = _bckList.getSelectedIndices();
	    for (int i = indices.length - 1; i>= 0;i--) {					
		_bckData.removeElementAt(indices[i]);
	    }
	    checkBckDirty();				
	    checkBckNumber();

	} else if ( e.getSource().equals( _bLibBrowse )){
            if (isLocal()) {
                String file = "";
                String[] extensions = { "so","dll","sl","dl", "a" };
                String[] descriptions = { DSUtil._resource.getString( "filefilter", "dynamic-lib-so" ), 
				      DSUtil._resource.getString( "filefilter", "dynamic-lib-dll"),
				      DSUtil._resource.getString( "filefilter", "dynamic-lib-sl"),
				      DSUtil._resource.getString( "filefilter", "dynamic-lib-dl"),
				      DSUtil._resource.getString( "filefilter", "dynamic-lib-a")};
                file = DSFileDialog.getFileName(file, false, extensions,
					    descriptions, this);
                if ( file != null) {
                    _LibText.setText( file );
                }
            } else {
                // Must be local to browse
                DSUtil.showErrorDialog( getModel().getFrame(), "error-not-local", "", _section);
            }
	} else {
	    Debug.println(" MappingNodeSettingPanel.actionPerformed() : wrong action performed");
	}
	checkOkay();
    }


    public void resetCallback() {
	/* No state to preserve */
		reloadEntry();
		clearBackendsChanges();
		clearDistribution();
		checkBckNumber();
		clearDirtyFlag();
    }

  /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
    public boolean refresh () {		
		resetCallback();
				
		return true;
	}
		

	public void reloadEntry() {
		if (_entry == null) {
			return;
		}
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		try {
			_entry = ldc.read(_entry.getDN());
			
			/* We update the values that contain the saved values */
			if( _entry.getAttribute("nsslapd-distribution-plugin") == null ) {
				_saveLib = "";
			} else {
				_saveLib = 
					_entry.getAttribute("nsslapd-distribution-plugin").getStringValueArray()[0];
			}
			if( _entry.getAttribute("nsslapd-distribution-funct") == null ) {
				_saveFunct = "";
			} else {
				_saveFunct = 
					_entry.getAttribute("nsslapd-distribution-funct").getStringValueArray()[0];
			} 
		} catch (LDAPException e) {
			Debug.println("MappingNodeBckPanel.reloadEntry(): "+e);
		}
	}

    public void okCallback() {
	/* No state to preserve */

	/* retreive field to save */
	LDAPModificationSet attrs = new LDAPModificationSet();
	if( _isBckDirty ){
	    prepSaveBck( attrs );
	}
	if( _LibText.isEnabled() ) {
	    String sLib = _LibText.getText();
	    String sFunction = _functDistribText.getText();

	    if(( _isLibDirty ) && 
	       (( sFunction == null ) || 
		(sFunction.trim().length() == 0)) ) {
		DSUtil.showErrorDialog( getModel().getFrame(), "error-no-funct", "", _section);
		return;
	    }
	    if(( _isFctDirty ) && 
	       (( sLib == null) || ( sLib.trim().length() == 0 ))) {
		DSUtil.showErrorDialog( getModel().getFrame(), "error-no-lib", "", _section);
		return;
	    }
	    prepSaveDistrib( attrs );
	}
	if (( _isLibDirty) || ( _isFctDirty ) || ( _isBckDirty)) {
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try {
		Debug.println("******** :" +  _entry.getDN());
		ldc.modify( _entry.getDN(), attrs );
		_saveLib = _LibText.getText();
		_saveFunct = _functDistribText.getText();
		_savebckList = getAllBackendsInList();
		checkDistribDirty();
		_isBckDirty = false;
		clearDirtyFlag();
	    } catch ( LDAPException e) {
		Debug.println("******** :" + e.toString() );
		String[] args_m = { _entry.getDN(), e.toString() };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"update-error",
					args_m,
					_section );
	    }
	}
		
    }

    public boolean doDel() {
	boolean isOK = true;
	/* retreive field to save */
	LDAPModificationSet attrs = new LDAPModificationSet();
	if( _isBckDirty && prepDelBck( attrs )){
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try {
		Debug.println("******** doDel: modify " +  _entry.getDN());
		ldc.modify( _entry.getDN(), attrs );
		_savebckList = getAllBackendsInList();
		_isBckDirty = false;
		clearDirtyFlag();
	    } catch ( LDAPException e) {
		String[] args_m = { _entry.getDN(), e.toString() };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"update-error",
					args_m,
					_section );
		isOK = false;
	    }
	}	      
	return( isOK );
    }




    public boolean doAdd() {
	boolean isOk = true;
	boolean isbcktoSave = false;
	/* retreive field to save */
	LDAPModificationSet attrs = new LDAPModificationSet();
	if( _isBckDirty ){
	    isbcktoSave = prepAddBck( attrs );
	}
	if( _LibText.isEnabled() ) {
	    String sLib = _LibText.getText();
	    String sFunction = _functDistribText.getText();

	    if(( _isLibDirty ) && 
	       (( sFunction == null ) || 
		(sFunction.trim().length() == 0)) ) {
		DSUtil.showErrorDialog( getModel().getFrame(), "error-no-funct", "", _section);
		return( false ) ;
	    }
	    if(( _isFctDirty ) && 
	       (( sLib == null) || ( sLib.trim().length() == 0 ))) {
		DSUtil.showErrorDialog( getModel().getFrame(), "error-no-lib", "", _section);
		return( false );
	    }
	    prepSaveDistrib( attrs );
	}
	if (( _isLibDirty) || ( _isFctDirty ) || (( _isBckDirty) && isbcktoSave)) {
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try {
		ldc.modify( _entry.getDN(), attrs );
		_saveLib = _LibText.getText();
		_saveFunct = _functDistribText.getText();
		_savebckList = getAllBackendsInList();
		checkDistribDirty();
		_isBckDirty = false;
		clearDirtyFlag();
	    } catch ( LDAPException e) {
		Debug.println("******** :" + e.toString() );
		String[] args_m = { _entry.getDN(), e.toString() };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"update-error",
					args_m,
					_section );
		isOk = false;
	    }
	}
	return( isOk );
    }



    private boolean prepAddBck( LDAPModificationSet attrs ) {
		
	String[] bIl = getAllBackendsInList();
	Debug.println("MappingNodeSettingPanel.prepSaveBck");
	if(( _savebckList == null) || (_savebckList.length == 0)) {
	    // It's an add
	    if( bIl != null ) {
		attrs.add(OPADD,
			  new LDAPAttribute( "nsslapd-backend", bIl ));
		return( true );
	    } else {
		return( false );
	    }
	} else {
	    // It's a modify
	    String[] newBck = MappingUtils.whatsMoreInThisList( _savebckList,  bIl );
	    if(( newBck != null) && ( newBck.length != 0 )) {
		attrs.add( OPADD,
			   new LDAPAttribute( "nsslapd-backend", newBck));
		return( true );
	    } else {
		return( false );
	    }
	}
    }


    private boolean prepDelBck( LDAPModificationSet attrs ) {
		
	String[] bIl = getAllBackendsInList();
	Debug.println("MappingNodeSettingPanel.prepSaveBck");
	if(( _savebckList == null) || (_savebckList.length == 0)) {
	    // It's an add, that can be a del .....
	    return( false );
	} else {
	    // It's a modify
	    String[] removedBck  = MappingUtils.whatsMoreInThisList( bIl, _savebckList);
	    if((removedBck != null) && (removedBck.length != 0)) {
		attrs.add( OPDEL,
			   new LDAPAttribute( "nsslapd-backend", removedBck));
		return( true );
	    } else {
		return( false );
	    }
	}
    }


    public void changedUpdate(DocumentEvent e) {
	super.changedUpdate( e );
	checkOkay();
    }

    public void removeUpdate(DocumentEvent e) {
	checkDistribDirty();
	super.removeUpdate( e );
	checkOkay();
    }

    private void modelUpdate( ) {
	if ( _isBckDirty || _isLibDirty || _isFctDirty ){
	    setDirtyFlag();
	    setValidFlag();
	}
    }

    private void prepSaveBck( LDAPModificationSet attrs ) {
		
	String[] bIl = getAllBackendsInList();
	Debug.println("MappingNodeSettingPanel.prepSaveBck");
	if(( _savebckList == null) || (_savebckList.length == 0)) {
	    // It's an add
	    if( bIl != null ) {
		Debug.println("*** new Add:nsslapd-backend" + bIl);
		attrs.add(OPADD,
			  new LDAPAttribute( "nsslapd-backend", bIl ));
	    }
	} else {
	    // It's a modify
	    String[] newBck = MappingUtils.whatsMoreInThisList( _savebckList,  bIl );
	    String[] removedBck  = MappingUtils.whatsMoreInThisList( bIl, _savebckList);
	    if(( newBck != null) && ( newBck.length != 0 )) {
		Debug.println("*** upd Add:nsslapd-backend" + newBck);
		attrs.add( OPADD,
			   new LDAPAttribute( "nsslapd-backend", newBck));
	    }
	    if((removedBck != null) && (removedBck.length != 0)) {
		Debug.println("*** upd Del:nsslapd-backend" + removedBck);
		attrs.add( OPDEL,
			   new LDAPAttribute( "nsslapd-backend", removedBck));
	    }
				
	}
    }


    private void prepSaveDistrib( LDAPModificationSet attrs ) {

	String newLib = _LibText.getText();
	String newFct = _functDistribText.getText();
	Debug.println("MappingNodeSettingPanel.prepSaveDistrib");
	if( _isLibDirty ) {
	    if ((newLib != null) &&
		( newLib.trim().length() >0 )) { // changes in distrib plugin
		int op;
		if (( _saveLib == null ) && ( _saveLib.trim().length() == 0)) {
		    op = OPADD;
		    Debug.println("***  add:");
		} else {
		    op = OPREPLACE;
		    Debug.println("***  upd:");
		}
		Debug.println("***  nsslapd-distribution-plugin" + newLib);
		attrs.add( op,
			   new LDAPAttribute( "nsslapd-distribution-plugin",
					      newLib));
	    } else { // user remove the distribution config
		Debug.println("***  del : nsslapd-distribution-plugin" + newLib);
		attrs.add( OPDEL,
			   new LDAPAttribute( "nsslapd-distribution-plugin",
					      newLib));
	    }
					
	}
	if( _isFctDirty ) {
	    if(( newFct != null ) && ( newFct.trim().length() > 0)) {
		int op;
		if (( _saveFunct == null ) && (_saveFunct.trim().length() == 0)){
		    op = OPADD;
		    Debug.println("***  add:");
		} else {
		    op = OPREPLACE;
		    Debug.println("***  upd:");
		}
		Debug.println("*** nsslapd-distribution-funct" + newFct);
		attrs.add( op,
			   new LDAPAttribute( "nsslapd-distribution-funct",
					      newFct));
				
	    } else {
		Debug.println("***  del : nsslapd-distribution-funct" + newFct);
		attrs.add( OPDEL,
			   new LDAPAttribute( "nsslapd-distribution-funct",
					      newFct));
				
	    }
	}
    }		

    public void insertUpdate(DocumentEvent e) {
		
	checkDistribDirty();
	super.insertUpdate( e );
	checkOkay();
    }

    protected String[] addBackend() {

	String mapBckList[] = 
	    MappingUtils.getBackendList( _model.getServerInfo().getLDAPConnection() );
	String[] obck = getAllBackendsInList();
	BckListPanel child = new BckListPanel( _model,
					       mapBckList,
					       obck,
					       "configuration-mapping-add-backend-dbox-help");
	SimpleDialog dlg =
	    new SimpleDialog( getModel().getFrame(),
			      child.getTitle(),
			      SimpleDialog.OK |
			      SimpleDialog.CANCEL |
			      SimpleDialog.HELP,
			      child );
	
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	dlg.setDefaultButton( SimpleDialog.OK );
	dlg.getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString(_section,
																				   "add-backend-description"));
	dlg.packAndShow();

	String[] bck = (String[])child.getSelectedItem();
	if ( bck != null ) {
	    return ( bck );
	} else {
	    return ( null );
	}
    }

    class BackendCellRenderer extends DefaultListCellRenderer {  
	// This is the only method defined by ListCellRenderer.  We just
	// reconfigure the Jlabel each time we're called.
		
	public Component getListCellRendererComponent(
						      JList list,
						      Object value,            // value to display
						      int index,               // cell index
						      boolean isSelected,      // is the cell selected
						      boolean cellHasFocus)    // the list and the cell have the focus
	{
	    if (list.isSelectionEmpty()) {
		_bDeleteBck.setEnabled(false);
	    } else {
		_bDeleteBck.setEnabled(true);
	    }
	    checkOkay();
	    return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
    }

    private IDSModel			_model = null;
    private JLabel				_bckListLabel;

    private JList				_bckList;
    private String[]			_savebckList;
    private JButton				_bAddBck;
    private JButton				_bDeleteBck;
    private DefaultListModel	_bckData;
    private boolean				_isBckDirty = false;

    private JLabel				_LibLabel;
    private JTextField			_LibText;
    private String				_saveLib = null;
    private JButton				_bLibBrowse;
    private JLabel				_functDistribLabel;
    private JTextField			_functDistribText;
    private String				_saveFunct = null;
    private boolean				_isLibDirty =  false ;
    private boolean				_isFctDirty = false;


    private final static String _section = "mappingtree-backend";
    static final String CONFIG_BASEDN = "cn=plugins, cn=config" ;
    static final String CONFIG_MAPPING = "cn=mapping tree, cn=config" ;
    static final String ROOT_MAPPING_NODE = "is root suffix";
    static final String ADD_BCK = "addBackend";
    static final String DELETE_BCK = "deleteBackend";
    static final String BROWSE = "browseDistribLib";
    static final int	OPADD = LDAPModification.ADD;
    static final int	OPDEL = LDAPModification.DELETE;
    static final int	OPREPLACE = LDAPModification.REPLACE;

    private final int ROWS = 4; 

    private LDAPEntry _entry = null;
 
}
