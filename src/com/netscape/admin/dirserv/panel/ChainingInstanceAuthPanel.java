/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.File;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import com.netscape.management.nmclf.SuiConstants;


/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ChainingInstanceAuthPanel extends BlankPanel 
    implements  SuiConstants{
     
    public ChainingInstanceAuthPanel(IDSModel model,
				     LDAPEntry InstEntry ) {
	//		super( model, _section, true );
	super( model, _section);
	_helpToken = "configuration-chaining-authentication-help";
	_dnEntry = InstEntry.getDN();
	_chEntry = InstEntry;
	_model = model;
	_refreshWhenSelect = false;
	}

	public void init() {
		if (_isInitialized) {
			return;
		}
		_myPanel.setLayout( new GridBagLayout() );
		GridBagConstraints gbc = getGBC();
		gbc.gridx      = 0;
		gbc.gridy      = 0;
		linkData();		
		createAuthArea( _myPanel);
		createComment( _myPanel );
		checkComment();
		addBottomGlue();		
		showAll();
		_isInitialized = true;
	}


	private void createComment( JPanel  grid ) {

		GridBagConstraints ugbc = getGBC(); 
		JPanel panel = 
			new GroupPanel(DSUtil._resource.getString( _section,
													   "proxy-help-title" ),
						   true);
		
		ugbc.gridy= 1;ugbc.gridx = 0;
        ugbc.gridwidth = ugbc.REMAINDER;
		ugbc.fill = ugbc.BOTH;		
		grid.add( panel, ugbc );		

		GridBagConstraints pgbc = new GridBagConstraints();
        pgbc.gridx      = 0;
        pgbc.gridy      = 0;
        pgbc.gridwidth	 = 1;
        pgbc.gridheight = 1;
        pgbc.weightx    = 1;
        pgbc.weighty    = 1;
        pgbc.fill       = pgbc.BOTH;
        pgbc.anchor     = pgbc.CENTER;
        pgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
									0,DEFAULT_PADDING);
        pgbc.ipadx = 0;
        pgbc.ipady = 0;

		_lcomment = makeJLabel( _section,
								"proxy-help-comment" );
		_lcomment.setLabelFor(panel);
		pgbc.gridx  = 0;
		pgbc.anchor = pgbc.WEST; 
		pgbc.fill = pgbc.HORIZONTAL;
		pgbc.gridwidth  = pgbc.REMAINDER;
		panel.add( _lcomment, pgbc);

		_lcommentUsrLabel= makeJLabel( _section,
									   "proxy-help-user" );
		_lcommentUsr = new JLabel();
		_lcommentUsrLabel.setLabelFor(_lcommentUsr);
		pgbc.gridy++;
		pgbc.gridx      = 0;
		pgbc.fill = pgbc.NONE;
		pgbc.gridwidth  = 1;
		pgbc.weightx    = 0;
		pgbc.anchor = pgbc.EAST;
		panel.add( _lcommentUsrLabel, pgbc);

		pgbc.gridx  = 1;
		pgbc.anchor = pgbc.WEST; 
		pgbc.fill = pgbc.HORIZONTAL;
		pgbc.weightx    = 1;
		pgbc.gridwidth  = pgbc.REMAINDER;
		panel.add( _lcommentUsr, pgbc);


		_lcommentNcLabel = makeJLabel( _section,
									   "proxy-help-naming-context" );
		_lcommentNc = new JLabel();
		_lcommentNcLabel.setLabelFor(_lcommentNc);

		pgbc.gridx      = 0;
		pgbc.gridy++;
		pgbc.fill = pgbc.NONE;
		pgbc.gridwidth  = 1;
		pgbc.weightx    = 0;
		pgbc.anchor = pgbc.EAST;
		panel.add( _lcommentNcLabel, pgbc);

		pgbc.gridx  = 1;
		pgbc.anchor = pgbc.WEST; 
		pgbc.fill = pgbc.HORIZONTAL;
		pgbc.weightx    = 1;
		pgbc.gridwidth  = pgbc.REMAINDER;
		panel.add( _lcommentNc, pgbc);

		_lcommentAciLabel = makeJLabel( _section,
										"proxy-help-aci" );
		_tfaCommentAci = new JTextArea( 3, 10);
		_lcommentAciLabel.setLabelFor(_tfaCommentAci);
		_tfaCommentAci.setWrapStyleWord( true );
		_tfaCommentAci.setEditable( false );
		_tfaCommentAci.setBackground(Color.lightGray);

		pgbc.gridy++;
		pgbc.gridx      = 0;
		pgbc.weightx    = 0;
		pgbc.fill = pgbc.NONE;
		pgbc.gridwidth  = 1;
		pgbc.anchor = pgbc.NORTHEAST;
		panel.add( _lcommentAciLabel, pgbc);
		
		


		JScrollPane scrollBck = new JScrollPane();
		scrollBck.getViewport().setView(_tfaCommentAci); 
		pgbc.gridx  = 1;
		pgbc.anchor = pgbc.WEST; 
		pgbc.weightx    = 0;
		pgbc.fill = pgbc.HORIZONTAL;
		pgbc.gridwidth  = pgbc.REMAINDER;
		panel.add( scrollBck, pgbc);
		//
		// panel.add( _tfaCommentAci, pgbc);
		//
		
	}

	private void createAuthArea( JPanel  grid ) {

		GridBagConstraints ugbc = getGBC(); 
		JPanel panel = new JPanel( new GridBagLayout() );
		ugbc.gridy= 0;ugbc.gridx = 0;
        ugbc.gridwidth = ugbc.REMAINDER;
		ugbc.fill = ugbc.HORIZONTAL;		
		grid.add( panel, ugbc );		

		GridBagConstraints pgbc = new GridBagConstraints();
        pgbc.gridx      = 0;
        pgbc.gridy      = 0;
        pgbc.gridwidth	 = 1;
        pgbc.gridheight = 1;
        pgbc.weightx    = 1;
        pgbc.weighty    = 1;
        pgbc.fill       = pgbc.BOTH;
        pgbc.anchor     = pgbc.CENTER;
        pgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
									0,DEFAULT_PADDING);
        pgbc.ipadx = 0;
        pgbc.ipady = 0;	

		pgbc.gridx      = 0;
		pgbc.fill = pgbc.NONE;
		pgbc.weightx    = 0;
		pgbc.anchor = pgbc.EAST;
		panel.add( _lSuffixLabel, pgbc);

		pgbc.gridx  = 1;
		pgbc.anchor = pgbc.WEST;
		pgbc.weightx    = 1;
		panel.add( _lSuffixText, pgbc);

        pgbc.gridy++;
        pgbc.gridx = 0;
        pgbc.fill = pgbc.NONE;
        pgbc.weightx    = 0;
        pgbc.anchor = pgbc.EAST;
        panel.add( _lConnType, pgbc);

        pgbc.gridx  = 1;
        pgbc.anchor = pgbc.WEST;
        pgbc.weightx    = 1;
        panel.add( _noEncrypt, pgbc);

        pgbc.gridy++;
        pgbc.gridx  = 1;
        pgbc.anchor = pgbc.WEST;
        pgbc.weightx    = 1;
        panel.add( _sslEncrypt, pgbc);

        pgbc.gridy++;
        pgbc.gridx  = 1;
        pgbc.anchor = pgbc.WEST;
        pgbc.weightx    = 1;
        panel.add( _tlsEncrypt, pgbc);

		pgbc.gridy++;
        pgbc.gridx = 0;
		addEntryField( panel,
					   pgbc,
					   _lnsfarmserverurl,
					   _tfnsfarmserverurl);

        pgbc.gridy++;
        pgbc.gridx = 0;
        pgbc.fill = pgbc.NONE;
        pgbc.weightx    = 0;
        pgbc.anchor = pgbc.EAST;
        panel.add( _authMechLabel, pgbc);

        pgbc.gridx  = 1;
        pgbc.anchor = pgbc.WEST;
        pgbc.weightx    = 1;
        panel.add( _sslAuth, pgbc);

        pgbc.gridy++;
        pgbc.gridx  = 1;
        pgbc.anchor = pgbc.WEST;
        pgbc.weightx    = 1;
        panel.add( _gssapiAuth, pgbc);

        pgbc.gridy++;
        pgbc.gridx  = 1;
        pgbc.anchor = pgbc.WEST;
        pgbc.weightx    = 1;
        panel.add( _digestAuth, pgbc);

        pgbc.gridy++;
        pgbc.gridx  = 1;
        pgbc.anchor = pgbc.WEST;
        pgbc.weightx    = 1;
        panel.add( _simpleAuth, pgbc);

        pgbc.gridy++;
        pgbc.gridx = 0;
		addEntryField( panel,
					   pgbc,
					   _lnsmultiplexorbinddn,
		 			   _tfnsmultiplexorbinddn );

		pgbc.gridy++;
        pgbc.gridx = 0;
		addEntryField( panel,
					   pgbc,
					   _lnsmultiplexorcredentials,
					   _pfnsmultiplexorcredentials );
		
		pgbc.gridy++;
        pgbc.gridx = 0;
		pgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
									 DEFAULT_PADDING,DEFAULT_PADDING);
		addEntryField( panel,
					   pgbc,
					   _lConfirmPassword,
					   _pfConfirmPassword );
		

	}



    protected void addEntryField( JPanel panel, 
								  GridBagConstraints gbc,
								  JComponent label,
								  JComponent field, 
								  JLabel label2 ) {
		Component endGlue = STRETCH_FIELDS ? null : Box.createGlue();
		Component lastItem =
			STRETCH_FIELDS ? ((label2 != null) ? label2 : field) : endGlue;
		gbc.fill = gbc.NONE;
		gbc.weightx = 0.0;
		int gridwidth = gbc.gridwidth;
        gbc.gridwidth = 1;
		//		gbc.gridx = 0;
		gbc.anchor = gbc.EAST;
		int space = UIFactory.getComponentSpace();
		Insets insets = gbc.insets;
		gbc.insets = new Insets( space, space, 0, space/2 );
		panel.add( label, gbc );

		gbc.gridx++;
		gbc.anchor = gbc.WEST;
		gbc.insets = new Insets( space, 0, 0, 0 );
		if ( STRETCH_FIELDS ) {
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
		}
		gbc.gridwidth = (lastItem == field) ? gbc.REMAINDER : 1;
		panel.add( field, gbc );

		if ( label2 != null ) {
			gbc.gridx++;
			gbc.fill = gbc.NONE;
			gbc.weightx = 0.0;
			gbc.insets = new Insets( space, space/2, 0, 0 );
			gbc.gridwidth = (lastItem == label2) ? gbc.REMAINDER : 1;
			panel.add( label2, gbc );
		}

		if ( !STRETCH_FIELDS ) {
			gbc.gridx++;
			gbc.anchor = gbc.EAST;
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
			gbc.gridwidth = gbc.REMAINDER;
			panel.add( endGlue, gbc );
		}
		// restore previous values
		gbc.insets = insets;
		gbc.gridwidth = gridwidth;
	}

    /**
	 * Add a label and a textfield to a panel, assumed to be using
	 * GridBagLayout.
	 */
    protected void addEntryField( JPanel panel,
								  GridBagConstraints gbc,
								  JComponent label,
								  JComponent field ) {
		addEntryField( panel, gbc, label, field, null );
	}

	private void linkData() {


		_lSuffixLabel =  makeJLabel( _section, "suffix" );
		
		_lSuffixText =  new JLabel();
		if ( _chEntry.getAttribute( SUFFIX_ATTR_NAM ) != null ) {
			_lSuffixText.setText(
					_chEntry.getAttribute( SUFFIX_ATTR_NAM ).getStringValueArray()[0]);
		} else {
			_lSuffixText.setText( SUFFIX_ATTR_NULL );
		}

		entries = new DSEntrySet( false );
		setDSEntrySet( entries );
		
		_lConnType = makeJLabel( _comsection, "conntype" );

		ButtonGroup connGroup = new ButtonGroup();
		_noEncrypt = makeJRadioButton( _comsection, "url-ldap" );
		_noEncrypt.setSelected(true); // default is on
        connGroup.add(_noEncrypt);

		_sslEncrypt = makeJRadioButton( _comsection, "url-ldaps" );
		connGroup.add(_sslEncrypt);

	    _tlsEncrypt = makeJRadioButton( _comsection, "url-starttls" );
	    connGroup.add(_tlsEncrypt);

	    /* add DSEntry to correctly update field
        coloring and buttons enabling/disabling */
	    _ldapDSEntry = new DSEntryBoolean ("on", _noEncrypt);
	    setComponentTable(_noEncrypt, _ldapDSEntry);
	    _sslDSEntry = new DSEntryBoolean ("off", _sslEncrypt);
	    setComponentTable(_sslEncrypt, _sslDSEntry);
	    _tlsDSEntry = new DSEntryBoolean ("off", _tlsEncrypt);
	    setComponentTable(_tlsEncrypt, _tlsDSEntry);

        entryurl = getAttrVal( NSFARMSERVERURL_ATTR );
        boolean ssl = (entryurl != null) &&
            entryurl.toLowerCase().startsWith("ldaps");
        _sslEncrypt.setSelected(ssl);
        _sslDSEntry.fakeInitModel(ssl ? "on" : "off");
        // set starttls initial value
        useStartTLS = getAttrVal( NSUSESTARTTLS_ATTR );
        // cannot use starttls with ldaps
        boolean tls = !ssl && (useStartTLS != null) &&
                       useStartTLS.equalsIgnoreCase("on");
        _tlsEncrypt.setSelected(tls);
        _tlsDSEntry.fakeInitModel(tls ? "on" : "off");
        _noEncrypt.setSelected(!ssl && !tls);
        _ldapDSEntry.fakeInitModel((!ssl && !tls) ? "on" : "off");

        _authMechLabel = makeJLabel( _comsection, "authmech" );

	    //ssl auth radio button
	    ButtonGroup authGroup = new ButtonGroup();
	    _sslAuth = makeJRadioButton( _comsection, "authmech-sslcert" );
	    _sslAuth.setEnabled(false); // because default is plain ldap
	    authGroup.add(_sslAuth);

	    _gssapiAuth = makeJRadioButton( _comsection, "authmech-gssapi" );
	    authGroup.add(_gssapiAuth);

	    _digestAuth = makeJRadioButton( _comsection, "authmech-digest" );
	    authGroup.add(_digestAuth);

	    _simpleAuth = makeJRadioButton( _comsection, "authmech-simple" );
	    _simpleAuth.setSelected(true); // default auth mech
	    authGroup.add(_simpleAuth);

        /* add DSEntry to correctly update field 
        coloring and buttons enabling/disabling */
	    _sslAuthDSEntry = new DSEntryBoolean ("off", _sslAuth);
	    setComponentTable(_sslAuth, _sslAuthDSEntry);        
        _gssapiAuthDSEntry = new DSEntryBoolean ("off", _gssapiAuth);
        setComponentTable(_gssapiAuth, _gssapiAuthDSEntry);        
        _digestAuthDSEntry = new DSEntryBoolean ("off", _digestAuth);
        setComponentTable(_digestAuth, _digestAuthDSEntry);        
        _simpAuthDSEntry = new DSEntryBoolean ("on", _simpleAuth);
        setComponentTable(_simpleAuth, _simpAuthDSEntry);  

        bindMech = getAttrVal( NSBINDMECHANISM_ATTR );
        boolean simpauth = (bindMech == null) || bindMech.equalsIgnoreCase("SIMPLE");
        boolean sslauth = (bindMech != null) && bindMech.equalsIgnoreCase("EXTERNAL");
        boolean gssapi = (bindMech != null) && bindMech.equalsIgnoreCase("GSSAPI");
        boolean digest = (bindMech != null) && bindMech.equalsIgnoreCase("DIGEST-MD5");
        if (!ssl && !tls) {
            sslauth = false;
            _sslAuth.setEnabled(false);
        }
        if (ssl || tls) {
            gssapi = false;
            _gssapiAuth.setEnabled(false);
        }
        _sslAuth.setSelected(sslauth);
        _sslAuthDSEntry.fakeInitModel(sslauth ? "on" : "off");
        _gssapiAuth.setSelected(gssapi);
        _gssapiAuthDSEntry.fakeInitModel(gssapi ? "on" : "off");
        _digestAuth.setSelected(digest);
        _digestAuthDSEntry.fakeInitModel(digest ? "on" : "off");
        // if one of the other auth was selected, but it would be
        // invalid to use them, just default to simple auth
        simpauth = simpauth || (!sslauth && !gssapi && !digest);
        _simpleAuth.setSelected(simpauth);
        _simpAuthDSEntry.fakeInitModel(simpauth ? "on" : "off");

        /* nsmultiplexorbinddn */
		_tfnsmultiplexorbinddn = makeJTextField( _section,
												 "nsmultiplexorbinddn" );
		_lnsmultiplexorbinddn = makeJLabel( _section,
											"nsmultiplexorbinddn" );
		_lnsmultiplexorbinddn.setLabelFor(_tfnsmultiplexorbinddn);

		AbstractButton ary[] = {_sslAuth, _gssapiAuth};
		bindDSEntry _nsmultiplexorbinddn = 
			new bindDSEntry("",
						  _tfnsmultiplexorbinddn,
						  _lnsmultiplexorbinddn,
						  NSMULTIPLEXORBINDDN_ATTR,
						  _model, ary );
		entries.add( _dnEntry,
					 NSMULTIPLEXORBINDDN_ATTR,
					 _nsmultiplexorbinddn );
		setComponentTable( _tfnsmultiplexorbinddn,
						   _nsmultiplexorbinddn );

		/* nsmultiplexorcredentials */
		_pfnsmultiplexorcredentials = makeJPasswordField(20 );
		_lnsmultiplexorcredentials =
			makeJLabel( _section,
						"nsmultiplexorcredentials" );
		_lnsmultiplexorcredentials.setLabelFor(_pfnsmultiplexorcredentials);
		/* nsmultiplexorcredentials confirmation */
		_pfConfirmPassword = makeJPasswordField(20 );
		_lConfirmPassword =
			makeJLabel( _section,
						"confirmcredentials" );
		_lConfirmPassword.setLabelFor(_pfConfirmPassword);
		confirmPwdDSEntry dsEntryConfirm =
			new confirmPwdDSEntry("",
									   _pfConfirmPassword,
									   _lConfirmPassword,
									   _pfnsmultiplexorcredentials,
									   1, ary);
		setComponentTable(_pfConfirmPassword, dsEntryConfirm);
		
		pwdDSEntry dsEntryPwd =
			new pwdDSEntry("",
								_pfnsmultiplexorcredentials,
								_lnsmultiplexorcredentials,
								dsEntryConfirm,
								1, ary);
		setComponentTable(_pfConfirmPassword, dsEntryPwd);
		entries.add(_dnEntry,
					NSMULTIPLEXORCREDENTIALS_ATTR,
					dsEntryPwd);		

		/* nsfarmserverurl */
		_tfnsfarmserverurl = makeJTextField( _section,
												 "nsfarmserverurl" );
		_lnsfarmserverurl = makeJLabel( _section,
											"nsfarmserverurl" );
		_lnsfarmserverurl.setLabelFor(_tfnsfarmserverurl);
		urlDSEntry _nsfarmserverurl = 
			new urlDSEntry("",
							_tfnsfarmserverurl,
							_lnsfarmserverurl );
		entries.add( _dnEntry,
					 NSFARMSERVERURL_ATTR,
					 _nsfarmserverurl );
		setComponentTable( _tfnsfarmserverurl,
						   _nsfarmserverurl );

		entrybind = getAttrVal( NSMULTIPLEXORBINDDN_ATTR );
		entrypwd  = getAttrVal( NSMULTIPLEXORCREDENTIALS_ATTR );

	}

    private void enableSimpleAuth (boolean enable){
        _lnsmultiplexorbinddn.setEnabled(enable);
        _tfnsmultiplexorbinddn.setEnabled(enable);
        _lnsmultiplexorcredentials.setEnabled(enable);
        _pfnsmultiplexorcredentials.setEnabled(enable);
        _pfnsmultiplexorcredentials.setBackground(_tfnsmultiplexorbinddn.getBackground());
        _lConfirmPassword.setEnabled(enable);
        _pfConfirmPassword.setEnabled(enable);
        _pfConfirmPassword.setBackground(_tfnsmultiplexorbinddn.getBackground());
    }

    /* We need to intercept the conn type and auth type buttons in order
	 * to enable/disable various combinations of them
     * @see com.netscape.admin.dirserv.panel.BlankPanel#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(_sslAuth) && _sslAuth.isSelected()) {
            //disable
            enableSimpleAuth (false);
        }
        if (e.getSource().equals(_gssapiAuth) && _gssapiAuth.isSelected()) {
            // enable
            enableSimpleAuth (true);
        }
        if (e.getSource().equals(_simpleAuth) && _simpleAuth.isSelected()) {
            //enable
            enableSimpleAuth (true);
        }
        if (e.getSource().equals(_digestAuth) && _digestAuth.isSelected()) {
            //enable
            enableSimpleAuth (true);
        }
        if (e.getSource().equals(_noEncrypt) && _noEncrypt.isSelected()) {
            //disable
            _sslAuth.setEnabled(false);
            if (_sslAuth.isSelected()) {
                // have to select something else
                _simpleAuth.setSelected(true);
            }
            enableSimpleAuth(true);
            _gssapiAuth.setEnabled(true);
            _digestAuth.setEnabled(true);
            String url = _tfnsfarmserverurl.getText();
            if (url.startsWith("ldaps://")) {
                url = url.replaceFirst("ldaps://", "ldap://");
                _tfnsfarmserverurl.setText(url);
            }
        }
        boolean ssl_selected = false;
        if (e.getSource().equals(_sslEncrypt) && _sslEncrypt.isSelected()) {
            ssl_selected = true;
            String url = _tfnsfarmserverurl.getText();
            if (url.startsWith("ldap://")) {
                url = url.replaceFirst("ldap://", "ldaps://");
                _tfnsfarmserverurl.setText(url);
            }
        }
        if (e.getSource().equals(_tlsEncrypt) && _tlsEncrypt.isSelected()) {
            ssl_selected = true;
            String url = _tfnsfarmserverurl.getText();
            if (url.startsWith("ldaps://")) {
                url = url.replaceFirst("ldaps://", "ldap://");
                _tfnsfarmserverurl.setText(url);
            }
        }
        if (ssl_selected) {
            _sslAuth.setEnabled(true);
            _gssapiAuth.setEnabled(false);
            if (_gssapiAuth.isSelected()) {
                // have to select something else
                _simpleAuth.setSelected(true);
                enableSimpleAuth(true);
            }
        }

        super.actionPerformed(e);
    }

    private void checkComment() {
		String usr = _tfnsmultiplexorbinddn.getText();
		String suf = _chEntry.getAttribute( SUFFIX_ATTR_NAM ).getStringValueArray()[0];
		String aci = "(targetattr = \"*\")" + 
			"(version 3.0; acl \"proxy acl\"; " +
			"allow(proxy) userdn=\"ldap:///" +
			usr + "\";)";


		_lcommentNc.setText( suf );
		_lcommentUsr.setText( usr);
		_tfaCommentAci.setText( aci );

	}
    
    private void checkURL(DocumentEvent e) {
        if (!e.getDocument().equals(_tfnsfarmserverurl.getDocument())) {
            return;
        }
        String url = _tfnsfarmserverurl.getText();
        if (url != null) {
            // user typed in ldap url
            if (url.startsWith("ldap://") && _sslEncrypt.isSelected()) {
                if (_sslAuth.isSelected()) {
                    _tlsEncrypt.setSelected(true); // preserve security setting
                } else {
                    _noEncrypt.setSelected(true);
                    _sslAuth.setEnabled(false);
                    _gssapiAuth.setEnabled(true);
                    enableSimpleAuth(true);
                }
            }
            // user typed in ldaps url
            if (url.startsWith("ldaps://") && !_sslEncrypt.isSelected()) {
                _sslEncrypt.setSelected(true);
                if (!_sslAuth.isSelected()) {
                    _sslAuth.setEnabled(true);
                    _gssapiAuth.setEnabled(false);
                    enableSimpleAuth(true);
                }
            }
        }
    }

    public void changedUpdate(DocumentEvent e) {
        checkURL(e);
		super.changedUpdate( e );
		modelUpdate();		
	}

    public void insertUpdate(DocumentEvent e) {
        checkURL(e);
		super.insertUpdate( e );
		modelUpdate();
	}
    public void removeUpdate(DocumentEvent e) {
        checkURL(e);
		super.removeUpdate( e );
		modelUpdate();
	}

	private void modelUpdate(){
		checkComment();
	}

	private String getAttrVal( String attrName ) {
		if ( _chEntry != null ) {
			LDAPAttribute attr = _chEntry.getAttribute( attrName );
			if ( attr != null ) {
				Enumeration en = attr.getStringValues();
				if ( (en != null) && en.hasMoreElements() )
					return (String)en.nextElement();
			}
		}
		return null;
    }
	
	public void resetCallback() {
	    revertAll();
        if (_noEncrypt.isSelected()) {
            //disable
            _sslAuth.setEnabled(false);
            if (_sslAuth.isSelected()) {
                // have to select something else
                _simpleAuth.setSelected(true);
            }
            enableSimpleAuth(true);
            _gssapiAuth.setEnabled(true);
            _digestAuth.setEnabled(true);
            String url = _tfnsfarmserverurl.getText();
            if (url.startsWith("ldaps://")) {
                url = url.replaceFirst("ldaps://", "ldap://");
                _tfnsfarmserverurl.setText(url);
            }
        }
        boolean ssl_selected = false;
        if (_sslEncrypt.isSelected()) {
            ssl_selected = true;
            String url = _tfnsfarmserverurl.getText();
            if (url.startsWith("ldap://")) {
                url = url.replaceFirst("ldap://", "ldaps://");
                _tfnsfarmserverurl.setText(url);
            }
        }
        if (_tlsEncrypt.isSelected()) {
            ssl_selected = true;
            String url = _tfnsfarmserverurl.getText();
            if (url.startsWith("ldaps://")) {
                url = url.replaceFirst("ldaps://", "ldap://");
                _tfnsfarmserverurl.setText(url);
            }
        }
        if (ssl_selected) {
            _sslAuth.setEnabled(true);
            _gssapiAuth.setEnabled(false);
            if (_gssapiAuth.isSelected()) {
                // have to select something else
                _simpleAuth.setSelected(true);
                enableSimpleAuth(true);
            }
        }
	    super.resetCallback();
	}

	public void okCallback() {
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		int errors = 0;

		// need to update nsUseStartTLS?
		if (_tlsDSEntry.getDirty()) {
		    useStartTLS = _tlsEncrypt.isSelected() ? "on" : "off";
		    LDAPModificationSet attrs = new LDAPModificationSet();
		    attrs.add(LDAPModification.REPLACE,
		            new LDAPAttribute( NSUSESTARTTLS_ATTR,
		                    useStartTLS ));
		    try {
		        ldc.modify( _chEntry.getDN(), attrs );
		        _ldapDSEntry.reset();
                _sslDSEntry.reset();
                _tlsDSEntry.reset();
		    } catch (LDAPException e) {
                errors++;
		        String[] args_m = { _chEntry.getDN(), e.toString() };
		        DSUtil.showErrorDialog( getModel().getFrame(),
		                "update-starttls-error",
		                args_m,
		                _section );                             
		    }
		}

		if (_simpAuthDSEntry.getDirty() || _sslAuthDSEntry.getDirty() ||
		    _gssapiAuthDSEntry.getDirty() || _digestAuthDSEntry.getDirty()) {
		    if (_sslAuth.isSelected()) {
		        bindMech = "EXTERNAL";
		    }
		    if (_gssapiAuth.isSelected()) {
		        bindMech = "GSSAPI";
		    }
		    if (_digestAuth.isSelected()) {
		        bindMech = "DIGEST-MD5";
		    }
		    if (_simpleAuth.isSelected()) {
		        bindMech = "SIMPLE";
		    }
		    LDAPModificationSet attrs = new LDAPModificationSet();
		    attrs.add( LDAPModification.REPLACE,
		            new LDAPAttribute ( NSBINDMECHANISM_ATTR,
		                    bindMech ));
		    try {
		        ldc.modify( _chEntry.getDN(), attrs );
		        _simpAuthDSEntry.reset();
                _sslAuthDSEntry.reset();
                _gssapiAuthDSEntry.reset();
                _digestAuthDSEntry.reset();
		    } catch (LDAPException e) {
                errors++;
		        String[] args_m = { _chEntry.getDN(), e.toString() };
		        DSUtil.showErrorDialog( getModel().getFrame(),
		                "update-bindmech-error",
		                args_m,
		                _section );                             
		    }
		}
		super.okCallback();
		// clear dirty flag if all changes saved successfully
		if (errors == 0) {
		    clearDirtyFlag();
		}
	}

    class urlDSEntry extends DSEntryTextStrict
    {
        urlDSEntry(String model, JComponent view1, JComponent view2) {
            super (model, view1, view2);
        }

        public int validate (){
            JTextField tf = (JTextField) getView (0);
            String     url = tf.getText ();

            boolean ok = (url != null) && (url.length() > 0);
            ok = ok && ( url.startsWith("ldap://") || url.startsWith("ldaps://") );
            ok = ok && ( url.endsWith("/"));
            return ok ? 0 : 1;
        }
    }

    class bindDSEntry extends DSEntryDN
    {
        private AbstractButton ary[];

        bindDSEntry( String model, JComponent view1, JComponent view2,
                String attrName, IDSModel dsModel, AbstractButton ary[] ) {
            super (model, view1, view2, attrName, dsModel);
            this.ary = ary;
        }

        public int validate (){
            JTextField tf = (JTextField) getView (0);

            /* disabled field is always valid */
            if (!tf.isEnabled ())
                return 0;

            // skip check if any of these buttons are
            // selected
            for (int ii = 0; ii < ary.length; ++ii) {
                if (ary[ii].isSelected()) {
                    return 0;
                }
            }

            return super.validate();
        }
    }

    class pwdDSEntry extends DSEntryPassword
    {
        private AbstractButton ary[];

        pwdDSEntry(String model, JPasswordField pfPwd, 
                JLabel lPwd,
                DSEntryConfirmPassword dsEntryConfirm,
                int minLength, AbstractButton ary[]) {
            super (model, pfPwd, lPwd, dsEntryConfirm, minLength);
            this.ary = ary;
        }

        public int validate (){
            JTextField tf = (JTextField) getView (0);

            /* disabled field is always valid */
            if (!tf.isEnabled ())
                return 0;
            
            // skip check if any of these buttons are
            // selected
            for (int ii = 0; ii < ary.length; ++ii) {
                if (ary[ii].isSelected()) {
                    return 0;
                }
            }

            return super.validate();
        }
    }

    class confirmPwdDSEntry extends DSEntryConfirmPassword {
        private AbstractButton ary[];

        confirmPwdDSEntry(String model, JPasswordField pfConfirm,
                JLabel confirm, JPasswordField pfPwd, int minLength, AbstractButton ary[]) {
            super(model, pfConfirm, confirm, pfPwd, minLength);
            this.ary = ary;
        }

        public int validate (){
            JTextField tf = (JTextField) getView (0);

            /* disabled field is always valid */
            if (!tf.isEnabled ())
                return 0;
            
            // skip check if any of these buttons are
            // selected
            for (int ii = 0; ii < ary.length; ++ii) {
                if (ary[ii].isSelected()) {
                    return 0;
                }
            }

            return super.validate();
        }
    }

    private IDSModel			_model = null;
	private LDAPEntry			_chEntry = null;
	private String				_dnEntry;

	private JLabel				_lSuffixText;
	private JLabel				_lSuffixLabel;
	private JTextField			_tfnsmultiplexorbinddn;
	private JLabel				_lnsmultiplexorbinddn;
	private JPasswordField		_pfConfirmPassword;
	private JPasswordField		_pfnsmultiplexorcredentials;
	private JLabel				_lnsmultiplexorcredentials;
	private JLabel				_lConfirmPassword;					  
	private JTextField			_tfnsfarmserverurl;
	private JLabel				_lnsfarmserverurl;
	private JLabel				_lcomment;
	private JLabel				_lcommentNcLabel;
	private JLabel				_lcommentNc;
	private JLabel				_lcommentUsrLabel;
	private JLabel				_lcommentUsr;
	private JLabel				_lcommentAciLabel;
	private JTextArea			_tfaCommentAci;
	private JLabel              _lConnType;
	private JRadioButton        _noEncrypt, _sslEncrypt, _tlsEncrypt;
	private JLabel              _authMechLabel;
    private JRadioButton        _simpleAuth, _sslAuth, _gssapiAuth, _digestAuth;

	private DSEntrySet			entries;
    private DSEntryBoolean      _ldapDSEntry;
    private DSEntryBoolean      _sslDSEntry;
    private DSEntryBoolean      _tlsDSEntry;
    private DSEntryBoolean      _sslAuthDSEntry = null;
    private DSEntryBoolean      _simpAuthDSEntry = null;
    private DSEntryBoolean      _gssapiAuthDSEntry = null;
    private DSEntryBoolean      _digestAuthDSEntry = null;
	
	private String				entrybind;
	private String				entrypwd;
	private String				entryurl;
	private String              bindMech;
	private String              useStartTLS;

								  
	private static final String _section = "chaining-instance-auth";
    private static final String _comsection = "newchaining";
	private static final String NSMULTIPLEXORBINDDN_ATTR = 
										"nsmultiplexorbinddn";
	private static final String NSMULTIPLEXORCREDENTIALS_ATTR =
										"nsmultiplexorcredentials" ;
	private static final String NSFARMSERVERURL_ATTR = 
										"nsfarmserverurl";
    private static final String NSUSESTARTTLS_ATTR = 
        "nsusestarttls";
    private static final String NSBINDMECHANISM_ATTR = 
        "nsbindmechanism";
	private static final String SUFFIX_ATTR_NAM= "nsslapd-suffix";
	private static final String SUFFIX_ATTR_NULL="";

									  
	  
    //copy from BlankPanel
    final static int DEFAULT_PADDING = 6;
    final static Insets DEFAULT_EMPTY_INSETS = new Insets(0,0,0,0);
    final static Insets BOTTOM_INSETS = new Insets(6,6,6,6);
}

