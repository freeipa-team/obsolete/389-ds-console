/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import java.util.Enumeration;
import com.netscape.management.client.util.Debug;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server
 */
public class DSEntryBooleanFake extends DSEntryBoolean {
    /**
     * Extends the functionality of DSEntryBoolean, specialized to
     * handle boolean choice widgets whose value is represented in String
     * form by either a "0" for false or a "1" for true.  It's "fake" in
	 * the sense that this does not represent a boolean attribute in the
	 * DS, it can represent any attribute and value.  The given value
	 * is the "real" value to store to the directory instead of the
	 * usual "on" or "off".
     *
     * @param model the initial value, if any, or just null
     * @param view the AbstractButton representing the value
	 * @param value the actual "real" value to store to the directory
     */
	public DSEntryBooleanFake(String model, AbstractButton view, String value) {
		super(model, view);
		_value = value;
	}

	/**
	 * This method converts the String representation of the attribute
	 * stored in the Directory Server to the local model representation
	 *
	 * @param String remoteValue This is the value as stored in the DS
	 */
	public void remoteToLocal(String remoteValue) {
		// If there is a value, set the model to the true value
		Debug.println(9, "DSEntryBooleanFake.remoteToLocal: " + remoteValue);

		if (remoteValue != null && remoteValue.length() > 0)
			super.remoteToLocal(_trueValue);
		else
			super.remoteToLocal(_falseValue);
	}
		
	/**
	 * This method is used to convert multivalued attributes
	 * stored in the Directory Server to the local model representation
	 *
	 * @param remoteValue An enumeration of Strings
	 * @see com.netscape.admin.dirserv.panel.DSEntrySet#show
	 */
	public void remoteToLocal(Enumeration remoteValues) {
		if (remoteValues != null && remoteValues.hasMoreElements()) {
			remoteToLocal((String)remoteValues.nextElement());
		}
	}

	/**
	 * This method is used to get the local values in the form used to
	 * store the values in the Directory Server
	 *
	 * @return An array of String values
	 * @see com.netscape.admin.dirserv.panel.DSEntrySet#store
	 */
	public String[] localToRemote() {
		String retval[] = null;
		String val = getModel(0);
		if (val != null && val.equals(_trueValue)) {
			retval = new String[1];
			retval[0] = _value;
		}
		Debug.println("DSEntryBooleanFake.localToRemote: model = " + val +
					  " retval = " + (retval == null ? "null" : retval[0]));

		return retval;
	}

	protected String _value = null; // the "real" value to store to the directory
}
