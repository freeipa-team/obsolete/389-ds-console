/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.logging.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.logging.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class AccessLogContentPanel extends LogContentPanel {

    public AccessLogContentPanel(IDSModel model) {
	super(model, "accesslog-content");
	_helpToken = "status-logs-access-help";
	_refreshWhenSelect = false;
    }

    protected DSLogViewer getViewer() {
	return new DSLogViewer( getModel(),
				new AccessLogViewerModel(getModel().getServerInfo(), cgiName ),
				CONFIG_DN, ACCESS_LOG_ATTR_NAME,
				ACCESS_LOGLIST_ATTR_NAME );
    }

    int[] getWidths() {
	return _widths;
    }

    private int[] _widths = { 75, 65, 40, 40, 500 };
    private static final String CONFIG_DN = "cn=config";
    private static final String ACCESS_LOG_ATTR_NAME = "nsslapd-accesslog";
    private static final String ACCESS_LOGLIST_ATTR_NAME =
	"nsslapd-accesslog-list";
}
