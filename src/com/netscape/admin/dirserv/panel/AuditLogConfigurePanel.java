/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.management.client.*;
import com.netscape.admin.dirserv.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class AuditLogConfigurePanel extends LogPanel {

	public AuditLogConfigurePanel(IDSModel model) {
		super(model, "auditlog-configure");
		_helpToken = "configuration-logs-audit-help";
	}
	
	public void init() {
		LOG_ATTR_NAME = "nsslapd-auditlog";
		LOG_MODE_ATTR_NAME = "nsslapd-auditlog-mode";
		LOG_PER_DIR_ATTR_NAME = "nsslapd-auditlog-maxlogsperdir";
		LOG_SIZE_ATTR_NAME = "nsslapd-auditlog-maxlogsize";
		ROTATION_SYNC_ENABLED_ATTR_NAME = "nsslapd-auditlog-logrotationsync-enabled";
		ROTATION_SYNCHOUR_ATTR_NAME = "nsslapd-auditlog-logrotationsynchour";
		ROTATION_SYNCMIN_ATTR_NAME = "nsslapd-auditlog-logrotationsyncmin";
		ROTATION_TIME_ATTR_NAME = "nsslapd-auditlog-logrotationtime";
		ROTATION_UNITS_ATTR_NAME = "nsslapd-auditlog-logrotationtimeunit";
		MAX_DISK_SPACE_ATTR_NAME = "nsslapd-auditlog-logmaxdiskspace";
		MIN_FREE_SPACE_ATTR_NAME = "nsslapd-auditlog-logminfreediskspace";
		MAX_DAYS_OLD_ATTR_NAME = "nsslapd-auditlog-logexpirationtime";
		EXPIRATION_UNITS_ATTR_NAME = "nsslapd-auditlog-logexpirationtimeunit";
		ENABLED_ATTR_NAME = "nsslapd-auditlog-logging-enabled";
		LOG_BASE_NAME = "audit";
		super.init();
		createEnableArea();
		createConfigArea();
        addBottomGlue ();
		enableFields( _cbEnabled.isSelected() );
		super.postInit();
	}

	LogContentPanel getViewerPanel() {
		return new AuditLogContentPanel( getModel() );
	}
}

