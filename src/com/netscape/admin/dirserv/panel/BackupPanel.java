/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Date;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.admin.dirserv.*;
import java.text.SimpleDateFormat;
import javax.swing.border.EmptyBorder;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	03/08/98
 * @see     com.netscape.admin.dirserv
 */
public class BackupPanel extends BlankPanel {
	public BackupPanel(IDSModel model) {
		super( model, _section );
		_helpToken = "tasks-backup-help";
	}

	public void init() {

                _baseDir = DSUtil.getDefaultBackupPath(getModel().getServerInfo());

                _myPanel.setLayout( new GridBagLayout() );
		createFileArea( _myPanel );	
		setDefaultDirectory();
		
		getSimpleDialog().setFocusComponent(_tfExport);
	}

    protected void createFileArea( JPanel grid ) {
        GridBagConstraints gbc = getGBC();
        gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = (Insets)gbc.insets.clone();

		
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;
		grid.add(panel, gbc);
		/* Now substitute grid by panel*/
		
		

		_filenameLabel = makeJLabel( _section, "filename" );
		gbc.gridwidth = 3;
		gbc.fill = gbc.NONE;
  		gbc.weightx = 0.0;
        panel.add( _filenameLabel, gbc );

        _tfExport = makeJTextField( _section, "filename" );
		gbc.fill = gbc.HORIZONTAL;
  		gbc.weightx = 1.0;
        panel.add(_tfExport,gbc);
		_filenameLabel.setLabelFor(_tfExport);

		// Only show the browse button if we're running locally
		if ( isLocal() ) {
			_bExport = makeJButton( _section, "browse-file" );
			gbc.fill = gbc.NONE;
			
			gbc.weightx = 0;			
			gbc.anchor = gbc.EAST;
			
			gbc.gridwidth = gbc.REMAINDER;
			panel.add(_bExport,gbc);
			gbc.insets.top = UIFactory.getDifferentSpace();
			_bExport.setEnabled( true );
			gbc.gridwidth = gbc.RELATIVE;
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
			panel.add(Box.createGlue(), gbc);
			
		}
		
	    _bAuto = makeJButton( _section, "autoName");
        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 0;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
        panel.add( _bAuto, gbc );
                gbc.weighty= 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.VERTICAL;
		grid.add(Box.createGlue(), gbc);	   
	}


    protected void checkOkay() {
		String path = getFilename();
		boolean ok = ( (path != null) &&
					   (path.length() > 0) );
		SimpleDialog dlg = getSimpleDialog();
		if ( dlg != null ) {
			dlg.setOKButtonEnabled( ok );
		} else {
		}
	}

    protected void setDefaultDirectory() {
		Date date = new Date();
		SimpleDateFormat fmt =
			new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		String path = _baseDir + '/' + fmt.format(date);
		_tfExport.setText( path );
    }


   protected SimpleDialog getSimpleDialog() {		
	   for (Container p = getParent(); p != null; p = p.getParent()) {
		   if (p instanceof SimpleDialog) {
			   return (SimpleDialog)p;
		   }
	   }
	   return null;
   }
    
    public void changedUpdate(DocumentEvent e) {
		super.changedUpdate( e );
		checkOkay();
	}

    public void removeUpdate(DocumentEvent e) {
		super.removeUpdate( e );
		checkOkay();
	}

    public void insertUpdate(DocumentEvent e) {
		super.insertUpdate( e );
		checkOkay();
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if ( e.getSource().equals(_bExport) ) {
    	    String file = getFilename().trim();
			if ((file != null) && (file.length() > 1)) {
				/* See if it is an absolute path */
				File theFile = new File(file);
				if (!theFile.isAbsolute()) {			
					/* If no path was provided, use the default path */
					file = _baseDir + java.io.File.separator + file;	
				}
			} else {
				file = _baseDir;
			}
    	    file = DSFileDialog.getDirectoryName(file, true, this);
    	    if (file != null)
    	        _tfExport.setText(file.trim());
        } else if ( e.getSource().equals( _bAuto ) ) {
            setDefaultDirectory();
			checkOkay();
        } else {
			super.actionPerformed(e);
		}
    }

    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

    public void okCallback() {						
		/* If no path was provided, use the default path.  We do it only in the case we are local (if we are
		 remote the user is obliged to provide the full path: we can't determine wether it is an absolute or relative
		 path because we don't know the OS of the server) */
		if (isLocal()) {
			String name = getFilename();
			if (name != null) {
				File file = new File(name);
				if (!file.isAbsolute() ) {
					name = _baseDir + java.io.File.separator + name;
				}
				_tfExport.setText(name);
			}
		}		

		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

    public String getFilename() {
		return _tfExport.getText().trim();
	}


	protected JLabel _filenameLabel;
	protected JButton _bAuto;
	protected JTextField _tfExport;
	protected JButton _bExport;
	protected JButton _browseButton;
	private String _baseDir = "";

	private final static String _section = "backup";
}
