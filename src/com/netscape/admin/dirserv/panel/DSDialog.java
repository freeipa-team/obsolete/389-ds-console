/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.admin.dirserv.DSUtil;

/**
 * DSDialog
 *
 * A convenient Dialog shell for a BlankPanel descendant, with OK, Cancel,
 * and Help buttons that are forwarded to callbacks of the BlankPanel.
 * Note: this means the BlankPanel is responsible for dismissing the dialog.
 *
 * @version 1.0
 * @author rweltman
 **/

public class DSDialog  extends JDialog
                       implements ActionListener {
    public DSDialog( Frame parent ) {
		super(parent, true);
		JPanel p = new JPanel();
//		p.setBackground(SystemColor.control);
		p.setBorder( new EmptyBorder(UIFactory.getBorderInsets()) );
		setContentPane(p);
	}

    public DSDialog( Frame parent, JPanel panel, boolean doCancel ) {
		this( parent );
		setPanel( panel, doCancel );
    }

    public DSDialog( Frame parent, JPanel panel ) {
		this( parent, panel, true );
	}

	public JPanel getPanel() {
		return _panel;
	}

    protected void setPanel( JPanel panel, boolean doCancel ) {
		_panel = panel;
		getContentPane().setLayout(new BorderLayout());
		if ( _panel instanceof BlankPanel ) {
			((BlankPanel)_panel).init();
			setTitle( ((BlankPanel)_panel).getTitle() );
		}

//        setLocation(new Point(100,100));
//       setModal(true);
		getContentPane().add( "Center", _panel );

		_bOK = UIFactory.makeJButton(this, _section, "OK");
		_bHelp = UIFactory.makeJButton(this, _section, "Help");

		JPanel buttonPanel;
		if ( doCancel ) {
			_bCancel = UIFactory.makeJButton(this, _section, "Cancel");
			JButton[] buttons = { _bOK, _bCancel, _bHelp };
			buttonPanel = UIFactory.makeJButtonPanel( buttons, true );
		} else {
			JButton[] buttons = { _bOK, _bHelp };
			buttonPanel = UIFactory.makeJButtonPanel( buttons, true );
		}

		getContentPane().add( "South", buttonPanel );

		/* Handle window closing events explicitly */
		setDefaultCloseOperation( DO_NOTHING_ON_CLOSE );
        addWindowListener(new DSWindowAdapter());
    }

    class Fronter implements Runnable {
		Fronter() {
		}
	    public void run() {
			try {
				Thread.sleep( 100 );
			} catch ( Exception e ) {
			}
			DSDialog.this.toFront();
		}
	}

    /**
	 * Override for JDialog
	 */
    public void show() {
		if ( System.getProperty( "PackDialogs" ) != null ) {
			pack();
		}
        // This prevents the dialog from immediately going behind other windows
        // on the screen on NT, but it causes a blank dialog on Solaris.
		if ( _isWindows ) {
			try {
				SwingUtilities.invokeLater(new Fronter());
			} catch ( Exception e ) {
			}
		}
		super.show();
	}

	public void packAndShow() {
		pack();
		show();
	}


    public void setPanel( JPanel panel ) {
		setPanel( panel, true );
	}

    /**
	 * Enable or disable the OK button
	 *
	 * @param state true to enable the OK button.
	 */
    public void setOkayEnabled( boolean state ) {
		if ( _bOK != null )
			_bOK.setEnabled( state );
	}

    /**
	 * Set text of the OK button
	 *
	 * @param label Label for OK button
	 */
    public void setOkayLabel( String label ) {
		if ( _bOK != null )
			_bOK.setText( label );
		/* May need to adjust widths */
		Vector v = new Vector();
		if ( _bOK != null ) {
			v.addElement( _bOK );
		}
		if ( _bCancel != null ) {
			v.addElement( _bCancel );
		}
		if ( _bHelp != null ) {
			v.addElement( _bHelp );
		}
		JButton[] buttons = new JButton[v.size()];
		v.copyInto( buttons );
		v.removeAllElements();
		UIFactory.resizeButtons( buttons );
	}

    /**
	 * Set the tooltip of the OK button
	 *
	 * @param tip Tooltip for OK button
	 */
    public void setOkayToolTip( String tip ) {
		if ( _bOK != null )
			_bOK.setToolTipText( tip );
	}

	/**
	 * Dispatch ok, cancel, help to the panel. The panel is responsible
	 * for disposing of the dialog.
	 *
	 * @param e event
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(_bCancel)) {
			cancel();
			return;
		}
		if ( (_panel != null) && (_panel instanceof BlankPanel) ) {
			BlankPanel panel = (BlankPanel)_panel;
			if (e.getSource().equals(_bOK)) {
				/* Prevent multi-clicks */
				_bOK.setEnabled( false );
				panel.okCallback();
				_bOK.setEnabled( true );	
			} else if (e.getSource().equals(_bHelp)) {
				_bHelp.setEnabled( false );
				panel.helpCallback();
				_bHelp.setEnabled( true );
			}
		}
	}

    private void cancel() {
		_fCancel = true;
		if ( (_panel != null) && (_panel instanceof BlankPanel) ) {
			((BlankPanel)_panel).resetCallback();
		}
	}

	/**
	 *	return the exit status of the dialog
	 *
	 * @return if the user hits cancel button, it will return true.
	 * Otherwise, it will be false.
	 */
	public boolean isCancel() {
		return _fCancel;
	}

	class DSWindowAdapter extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			/* Check first if Cancel is a valid option */
			if ( (_bCancel != null) && _bCancel.isVisible() ) {
				cancel();
			}
		}
	};	
	
    private JPanel _panel = null;      // The panel where all controls are
	protected JButton	_bOK = null;	    // ok button
	protected JButton  _bCancel = null;		// cancel button
	protected JButton  _bHelp = null;		// Help button
	protected boolean	_fCancel;			// exit state of the dialog
    private static final boolean _isWindows =
	    ( java.io.File.separator.equals("\\") );

	private static ResourceSet resource = DSUtil._resource;
	private static final String _section = "general";
}
