/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSModel;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.Color;
import javax.swing.*;
import javax.swing.JTextField;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server.
 * This class implements that functionality for attributes which have
 * a DN value.
 *
 * @version 1.0
 * @author richm
 */
public class DSEntryDN extends DSEntryText {
	public DSEntryDN( String model, JComponent view1, JComponent view2,
			   String attrName, IDSModel dsModel ) {
		super(model, view1, view2);
		_attrName = attrName;
		_tf = (JTextField)view1;
		if (view2 instanceof JLabel) {
			_label = ((JLabel)view2).getText();
		}
		_dsModel = dsModel;
	}

	public void store() {
		/* Besides saving the change in the server itself,
	   update the change in the topology server */
		super.store();
		String oldDN = getModel(0);
		String dn = _dsModel.getConsoleInfo().getCurrentDN();
		String val = _tf.getText().trim();
		LDAPConnection ldc =
			_dsModel.getConsoleInfo().getLDAPConnection();
		try {
			LDAPModification mod = new LDAPModification( 
				LDAPModification.REPLACE,
				new LDAPAttribute(_attrName, val) );
			ldc.modify(dn, mod);
			Debug.println( "ServerManagerPanel." +
						   "DSEntryDN.store(): modified " +
						   mod + " for " + dn );
		} catch (LDAPException lde) {
			Debug.println( "ServerManagerPanel." +
						   "DSEntryDN.store(): " +
						   "to " + dn + " " + lde );
		}
	}

	public int validate () {
		String value = _tf.getText().trim();
				
		if (!_tf.isEnabled ()) {
			return 0;
		} else if (value.equals ("")) {
			return ERROR_EMPTY_FIELD;
		} else if (!DN.isDN(value)) {
			return ERROR_NOT_A_DN;
		}

		return 0;
	}

	public boolean dsValidate() {
		String val = _tf.getText();
		String err = null; // error number
		String args[] = null; // additional arguments for error messages

		switch (validate()) {
		case ERROR_EMPTY_FIELD:
			err = "101";
			break;
		case ERROR_NOT_A_DN:
			err = "122";
			args = new String[1];
			args[0] = _label;
			break;
		default:
			DSUtil.checkForLDAPv2Quoting(val, _dsModel.getFrame(), _label);
			return true;
		}
		reportError(err, args, _tf);
		return false;
	}

	private String _attrName;
	private JTextField _tf;
	private IDSModel _dsModel;
	private String _label;

	static final private int ERROR_EMPTY_FIELD = 1;
	static final private int ERROR_NOT_A_DN = 2;
}
