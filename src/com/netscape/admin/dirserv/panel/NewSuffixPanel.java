/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.management.nmclf.*;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.management.nmclf.SuiConstants;


/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @see     com.netscape.admin.dirserv
 */
public class NewSuffixPanel extends BlankPanel implements SuiConstants {

    public NewSuffixPanel(IDSModel model, LDAPEntry node) {
	this( model, node, false);
    }

    public NewSuffixPanel(IDSModel model, LDAPEntry node, boolean isRoot) {
	super( model, _section );
	if( isRoot ) {
	    _helpToken = "configuration-new-mapping-node-dbox-help";
	    setTitle( DSUtil._resource.getString( _section, "new-root-title" ));
	} else {
	    _helpToken = "configuration-new-mapping-sub-suffix-dbox-help";
	    setTitle( DSUtil._resource.getString( _section, "new-title" ));
	}
	_model = model;
	_entry = node;
	_isRoot = isRoot;
    }

    public void init() {
		if (!_initialized) {
			_myPanel.setLayout( new GridBagLayout() );
			GridBagConstraints gbc = getGBC();
			
			// Mapping Tree info
			createTypeArea( _myPanel );
			AbstractDialog dlg = getAbstractDialog();
			if ( dlg != null ) {
				dlg.setOKButtonEnabled( false );
			}
			_initialized = true;
		}

    }


    protected void createTypeArea( JPanel grid ) {

	JPanel Mapanel = _myPanel;
	GridBagConstraints Magbc = new GridBagConstraints() ;


        Magbc.gridx      = 0;
        Magbc.gridy      = 0;
        Magbc.gridwidth  = Magbc.REMAINDER;
        Magbc.gridheight = 1;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
        Magbc.fill       = Magbc.BOTH;
        Magbc.anchor     = Magbc.CENTER;
        Magbc.insets     = new Insets(0, DIFFERENT_COMPONENT_SPACE,
				      COMPONENT_SPACE,
				      DIFFERENT_COMPONENT_SPACE);
        Magbc.ipadx = 0;
        Magbc.ipady = 0;
		

	_newSuffixLabel = makeJLabel( _section, "new-node-suffix" );
	_newSuffixLabel.resetKeyboardActions();
	Magbc.gridx = 0;
	Magbc.gridwidth = 1;
	Magbc.fill = Magbc.NONE;
	Magbc.anchor = Magbc.EAST;
	Magbc.weightx = 0.0;
	Mapanel.add( _newSuffixLabel, Magbc );

	_newSuffixText = makeJTextField( _section, "new-node-suffix" );	
	_newSuffixLabel.setLabelFor(_newSuffixText);
	Magbc.weightx = 1.0;
	Magbc.gridx = 1;
	Magbc.anchor     = Magbc.WEST;
	Magbc.gridwidth = Magbc.REMAINDER;
	Magbc.fill = Magbc.HORIZONTAL;
	Mapanel.add( _newSuffixText, Magbc );		

	if(! _isRoot ) {
	    String dady = MappingUtils.getMappingNodeSuffix(_entry);

	    _subordinationLabel = makeJLabel( _section,
					      "new-subordination" );
		_subordinationLabel.resetKeyboardActions();
	    Magbc.gridy++;
	    Magbc.gridx = 0;
	    Magbc.gridwidth = 1;
	    Magbc.anchor     = Magbc.EAST;
	    Magbc.fill = Magbc.NONE;
	    Magbc.weightx = 0.0;
	    Mapanel.add( _subordinationLabel, Magbc);

	    _subordinationText = new JLabel();
		_subordinationLabel.setLabelFor(_subordinationText);
	    Magbc.gridx = 1;
	    Magbc.weightx = 1.0;
	    Magbc.anchor     = Magbc.WEST;
	    Magbc.gridwidth = Magbc.REMAINDER;
	    Magbc.fill = Magbc.HORIZONTAL;
	    _subordinationText.setText( dady ); 
	    Mapanel.add( _subordinationText, Magbc );			

	    _resSuffixLabel = makeJLabel( _section, "new-result-suffix" );
		_resSuffixLabel.resetKeyboardActions();
	    Magbc.gridx = 0;
	    Magbc.gridy++;
	    Magbc.gridwidth = 1;
	    Magbc.fill = Magbc.NONE;
	    Magbc.anchor     = Magbc.EAST;
	    Magbc.weightx = 0.0;
	    Mapanel.add( _resSuffixLabel, Magbc );
	    
	    _resSuffixText = new JLabel();
		_resSuffixLabel.setLabelFor(_resSuffixText);
	    Magbc.weightx = 1.0;
	    Magbc.gridx = 1;
	    Magbc.anchor     = Magbc.WEST;
	    Magbc.gridwidth = Magbc.REMAINDER;
	    Magbc.fill = Magbc.HORIZONTAL;
	    StringBuffer tmpBuf = new StringBuffer("        "); 
	    tmpBuf.append( "," + dady );	
	    _resSuffixText.setText(tmpBuf.toString());
	    Mapanel.add( _resSuffixText, Magbc );		
	}
		
	_cbCreateLDBM = makeJCheckBox(_section , "new-cb-create-db");
	Magbc.gridy++;
	Magbc.gridx = 0;
	Magbc.anchor     = Magbc.EAST;
	Magbc.gridwidth = Magbc.REMAINDER;
	Magbc.fill = Magbc.HORIZONTAL;
	Magbc.weightx = 0.0;
	_cbCreateLDBM.setSelected( true );
	Mapanel.add( _cbCreateLDBM, Magbc);

	_newDBLabel = makeJLabel( _section, "new-db" );
	_newDBLabel.resetKeyboardActions();
	Magbc.gridy++;
	Magbc.gridx = 0;
	Magbc.gridwidth = 1;
	Magbc.fill = Magbc.NONE;
	Magbc.anchor     = Magbc.EAST;
	Magbc.weightx = 0.0;
	Mapanel.add( _newDBLabel, Magbc );

	_newDBText = makeJTextField( _section, "new-db" );
	_newDBLabel.setLabelFor(_newDBText);
	Magbc.weightx = 1.0;
	Magbc.gridx = 1;
	Magbc.anchor     = Magbc.WEST;
	Magbc.gridwidth = Magbc.REMAINDER;
	Magbc.fill = Magbc.HORIZONTAL;	
        Mapanel.add( _newDBText, Magbc );
		
		Magbc.gridy++;
		Magbc.gridx = 0;
		Magbc.weighty    = 1.0;
		Magbc.fill = Magbc.VERTICAL;
		Mapanel.add(Box.createVerticalGlue(), Magbc);

    }	

	/**
	  * Returns the component where we put the focus
	  *
	  */
	public JComponent getFocusComponent() {
		return _newSuffixText;
	}

    /**
     * Enable/disable OK button
     *
     * @param ok true to enable the OK button
     */
    private void setOkay( boolean ok ) {
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( ok );
	} else {
	}
    }

    private void checkOkay() {	   
	String a = _newSuffixText.getText().trim(); 	    
	boolean ok = ((a != null) && (a.length() > 0)) ;
	ok = ok && _isNewMappingNodeValid; 
	if ( _cbCreateLDBM.isSelected()) {
	    ok = ok && (( _newDBText.getText() != null) && 
			( _newDBText.getText().trim().length() > 0) &&
			( DSUtil.isValidBckName( _newDBText.getText())));
	}
	setOkay( ok );
    }

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
	if (e.getSource().equals( _cbCreateLDBM )) {
	    super.actionPerformed(e);
	    boolean enable = _cbCreateLDBM.isSelected();
	    _newDBText.setEnabled( enable );
	}
	checkOkay();
    }


    public void resetCallback() {
	/* No state to preserve */
	clearDirtyFlag();
	hideDialog();
    }


    public void okCallback() {

		String rdn = new DN(_newSuffixText.getText()).explodeDN(true)[0];
		if (rdn.indexOf('"') >=0 || rdn.indexOf("\\\\") >=0) {
			DSUtil.showErrorDialog(getModel().getFrame(),
                                   "new-suffix-invalid-chars","" );
            return;            
		}

        AbstractDialog dlg = getAbstractDialog();
		if (dlg != null) {
			dlg.setBusyCursor(true);
		}

	/* No state to preserve */
	String dady;
	String[] suffixes = MappingUtils.getSuffixList(_model.getServerInfo().getLDAPConnection());
	if( _isRoot ) {
	    dady = MappingUtils.ROOT_MAPPING_NODE;
		if (suffixes != null) {	
			/* Check if a mapping node already exists for this
			 * suffix OR if there is another root suffix that
			 * is parent of this one OR if there is a suffix
			 * that is a child of this one.
			 */
			DN suffixDN = new DN(_newSuffixText.getText());
			for (int i=0; i<suffixes.length; i++) {
				DN currentSuffixDN = new DN(suffixes[i]);

				if (suffixDN.equals(currentSuffixDN)) {
					DSUtil.showErrorDialog(
							getModel().getFrame(),
							"new-suffix-exists",
							suffixDN.toString() );
					if (dlg != null) {
						dlg.setBusyCursor(false);
					}
					return;
				}

				if (suffixDN.isDescendantOf(currentSuffixDN)) {
					String[] args = {suffixes[i]};
					int result = DSUtil.showConfirmationDialog( this,
															   "new-root-confirm-parent-exists",
															   args,
															   _section);
					if (result != JOptionPane.YES_OPTION) {
						if (dlg != null) {
							dlg.setBusyCursor(false);
						}
						return;
					}
					break;
				}
				if (currentSuffixDN.isDescendantOf(suffixDN)) {
					String[] args = {suffixes[i]};
					int result = DSUtil.showConfirmationDialog( this,
																"new-root-confirm-child-exists",
																args,
																_section);
					if (result != JOptionPane.YES_OPTION) {
						if (dlg != null) {
							dlg.setBusyCursor(false);
						}
						return;
					}
					break;
				}
			}	
		}		
	} else {
	    dady = _subordinationText.getText();
	} 
	
	boolean addLDBMB = false;    
	if( _cbCreateLDBM.isSelected()) {
	    String db = _newDBText.getText();	    
	    String[] dbList = { db };
	    String dbLoc = MappingUtils.getDefaultDBLoc( _model, _section) + db;
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();

	    if( ! MappingUtils.checkUnique( _model, db )) {
			if (dlg != null) {
				dlg.setBusyCursor(false);
			}
			return;
		}

	    if( _isRoot ) {
		addLDBMB = DSUtil.addLDBMBackend( _model,
					    db,
					    MappingUtils.LDBM_CONFIG_BASEDN,
					    dbLoc,
					    _newSuffixText.getText(),
					    _section );
	    } else {
		addLDBMB = DSUtil.addLDBMBackend( _model,
					    db,
					    MappingUtils.LDBM_CONFIG_BASEDN,
					    dbLoc,
					    _resSuffixText.getText(),
					    _section );
	    }
		    
		
	    if( addLDBMB &&
		( MappingUtils.addMappingNode( ldc,
					       _section,
					       _newSuffixText.getText(),
					       dady,
					       MappingUtils.BACKEND,
					       dbList,
					       null))) {
		hideDialog();
		if (dlg != null) {
			dlg.setBusyCursor(false);
		}
		return;
	    }
	    else if (addLDBMB) {
            // Cleanup required
            final String cleanupDN = "cn=" + _newDBText.getText() + "," +  MappingUtils.LDBM_CONFIG_BASEDN;
            final LDAPConnection ldc_f = ldc; // = _model.getServerInfo().getLDAPConnection();
            Debug.println("NewSufixPanel: failed to add mapping node, cleanup " + cleanupDN);
            new Thread ( new Runnable () {
                public void run() {
                    DSUtil.deleteTree(cleanupDN, ldc_f);
                }
            }).start();
	    }
        
	} else {
	    if( MappingUtils.addMappingNode(
					    _model.getServerInfo().getLDAPConnection(),
					    _section,
					    _newSuffixText.getText(),
					    dady,
					    MappingUtils.DISABLE,
					    null,
					    null)) {
			hideDialog();
			if (dlg != null) {
				dlg.setBusyCursor(false);
			}
			return;
			
	    }
	}
	DSUtil.showErrorDialog( getModel().getFrame(),
				"new-suffix-failed","" );

    if (dlg != null) {
		dlg.setBusyCursor(false);
	}
	return;	
    }

    public void changedUpdate(DocumentEvent e) {
	suffixUpdate();
	super.changedUpdate( e );
	checkOkay();
    }

    public void removeUpdate(DocumentEvent e) {
	suffixUpdate();
	super.removeUpdate( e );
	checkOkay();
    }

    public void insertUpdate(DocumentEvent e) {
	suffixUpdate();
	super.insertUpdate( e );
	checkOkay();
    }

    private void suffixUpdate() {
	if( ! _isRoot ) {
	    _resSuffixText.setText(  _newSuffixText.getText() +
				     "," +
				     _subordinationText.getText() );
	}
	if ( DN.isDN ( _newSuffixText.getText() )) {
	    setChangeState( _newSuffixLabel, CHANGE_STATE_UNMODIFIED );
	    if( ! _isRoot ) {
		setChangeState( _resSuffixLabel, CHANGE_STATE_UNMODIFIED ); 
	    }
	    _isNewMappingNodeValid = true;
	} else {
	    setChangeState( _newSuffixLabel, CHANGE_STATE_ERROR );
	    if( ! _isRoot ) {
		setChangeState( _resSuffixLabel, CHANGE_STATE_ERROR ); 
	    }
	    _isNewMappingNodeValid = false;
	}
	if ( DSUtil.isValidBckName( _newDBText.getText()) ) {
	    setChangeState( _newDBLabel, CHANGE_STATE_UNMODIFIED );
	} else {
	    setChangeState( _newDBLabel, CHANGE_STATE_ERROR);
	}
    }


    private JTextField			_newSuffixText;
    private JLabel			_newSuffixLabel;
    private JLabel		       	_subordinationText;
    private JLabel		       	_subordinationLabel;
    private JLabel			_resSuffixLabel;
    private JLabel			_resSuffixText;
    private JCheckBox			_cbCreateLDBM;
    private JTextField			_newDBText;
    private JLabel			_newDBLabel;
    

    private IDSModel			_model = null;

    private boolean				_isRoot;

    private boolean				_isNewMappingNodeValid = false;

	private boolean _initialized = false;

    private final static String _section = "mappingtree";
    private final int ROWS = 4; 
    private LDAPEntry _entry = null;

}
