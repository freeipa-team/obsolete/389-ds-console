/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;
/*
import java.io.File;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.Enumeration;
import java.util.Hashtable;
*/
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.management.nmclf.SuiConstants;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @see     com.netscape.admin.dirserv
 */

public class MappingNodeSettingPanel extends BlankPanel  
    implements SuiConstants {

    public MappingNodeSettingPanel( IDSModel model,
				    LDAPEntry entry ) {
	super( model, _section );
	_helpToken = "configuration-mapping-setting-help";
	_model = model;
	_entry = entry;
	_refreshWhenSelect = false;
    }


    public void init() {
		if (_isInitialized) {
			return;
		}

	GridBagLayout Mabag = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
	//        Magbc.gridwidth  = Magbc.REMAINDER;
        gbc.gridwidth	 = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 0;
        gbc.weighty    = 0;
        gbc.fill       = gbc.BOTH;
        gbc.anchor     = gbc.CENTER;
        gbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
				    0,DEFAULT_PADDING);
        gbc.ipadx = 0;
        gbc.ipady = 0;
	   	
	_myPanel.setLayout(Mabag);

	_NodeLabel = makeJLabel( _section, "suffix" );		
	gbc.fill = gbc.NONE;
	gbc.anchor = gbc.EAST;
        _myPanel.add( _NodeLabel, gbc );				

	_NodeText = new JLabel();
	String suffixval = MappingUtils.getMappingNodeName(_entry);
	_NodeText.setText(suffixval);
	gbc.gridx      = 1;
	gbc.anchor = gbc.WEST;
        _myPanel.add( _NodeText, gbc );
	_NodeLabel.setLabelFor(_NodeText);	
      
	_DadyNodeText = new JLabel();
	_DadyNodeText.setLabelFor(this);
	gbc.gridy++;
	if ( _entry.getAttribute("nsslapd-parent-suffix") == null ) {
	    _DadyNodeText.setText( DSUtil._resource.getString( _section,
							       "no-subordinate-label"));
			
	} else {
	    _DadyNodeText.setText( _entry.getAttribute("nsslapd-parent-suffix").getStringValueArray()[0] );
	    _DadyNodeLabel = makeJLabel( _section, "subordination" );
	    gbc.anchor = gbc.EAST;
	    gbc.gridx      = 0;
	    _myPanel.add( _DadyNodeLabel, gbc );
	}
	gbc.gridx      = 1;
	gbc.anchor = gbc.WEST;
	_myPanel.add( _DadyNodeText, gbc );
		
	_saveStatus = 
	    _entry.getAttribute("nsslapd-state").getStringValueArray()[0];

	if( _saveStatus.compareToIgnoreCase(DISABLE) == 0 ) {
	    _isEnabledNode = false;
	    _isBackendNode = false;
	    _isReferral = false;
	    _isRefUpdate = false;
	} else if( _saveStatus.compareToIgnoreCase(BACKEND) == 0 ) {
	    _isEnabledNode = true;
	    _isBackendNode = true;
	    _isReferral = false;
	    _isRefUpdate = false;
	} else if( _saveStatus.compareToIgnoreCase( REFERRAL ) == 0 ) {
	    _isEnabledNode = true;
	    _isBackendNode = false;
	    _isReferral = true;
	    _isRefUpdate = false;
	} else if( _saveStatus.compareToIgnoreCase( REFERRAL_UPDATE ) == 0 ) {
	    _isEnabledNode = true;
	    _isBackendNode = false;
	    _isReferral = false;
	    _isRefUpdate = true;
	}

	_cbEnableNode = new JCheckBox(DSUtil._resource.getString( _section, 
								  "state-enable-label"));
	_cbEnableNode.setSelected(_isEnabledNode);
	_cbEnableNode.addItemListener(new CheckBoxListener());
	gbc.gridx = 0;
	gbc.gridy++;
	_myPanel.add( _cbEnableNode, gbc );

	// Mapping Tree info
	createStateArea( _myPanel );
	StateEnableFields();
	addBottomGlue ();	
	_isInitialized = true;
    }

    public void reloadStatus( LDAPEntry entry){
	_entry = entry;

	_saveStatus = 
	    _entry.getAttribute("nsslapd-state").getStringValueArray()[0];
	if( _saveStatus.compareToIgnoreCase(DISABLE) == 0 ) {
	    _isEnabledNode = false;
	    _isBackendNode = false;
	    _isReferral = false;
	    _isRefUpdate = false;
	} else if( _saveStatus.compareToIgnoreCase(BACKEND) == 0 ) {
	    _isEnabledNode = true;
	    _isBackendNode = true;
	    _isReferral = false;
	    _isRefUpdate = false;
	} else if( _saveStatus.compareToIgnoreCase( REFERRAL ) == 0 ) {
	    _isEnabledNode = true;
	    _isBackendNode = false;
	    _isReferral = true;
	    _isRefUpdate = false;
	} else if( _saveStatus.compareToIgnoreCase( REFERRAL_UPDATE ) == 0 ) {
	    _isEnabledNode = true;
	    _isBackendNode = false;
	    _isReferral = false;
	    _isRefUpdate = true;
	}
	_cbEnableNode.setSelected( _isEnabledNode );
	_rbRefUpdate.setSelected( _isRefUpdate );
	_rbReferral.setSelected( _isReferral );
	_rbBackend.setSelected( _isBackendNode || (!_isEnabledNode));	
	setChangeState( _rbRefUpdate, CHANGE_STATE_UNMODIFIED);
	setChangeState( _rbBackend, CHANGE_STATE_UNMODIFIED );
	setChangeState( _rbReferral, CHANGE_STATE_UNMODIFIED );
	_isStateDirty = false;
	clearDirtyFlag();
    }


    protected void createStateArea( JPanel grid) {

        GridBagConstraints gbc = getGBC();
	JPanel Mapanel = new GroupPanel(
					DSUtil._resource.getString( _section, "mapping-title" ));
        gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
	gbc.gridx = 0;
        grid.add( Mapanel, gbc );

	ButtonGroup nodeStateGroup = new ButtonGroup();

	_rbBackend = new JRadioButton(DSUtil._resource.getString(_section, 
								 "state-backend-label"));
	_rbBackend.setSelected( _isBackendNode );
	_rbBackend.addActionListener(new RadioListener());
	nodeStateGroup.add( _rbBackend );
	gbc.fill = gbc.NONE;
	Mapanel.add( _rbBackend, gbc );

	_rbReferral =  new JRadioButton(DSUtil._resource.getString(_section,
								   "state-referral-label"));
	_rbReferral.setSelected( _isReferral );
	_rbReferral.addActionListener(new RadioListener());
	nodeStateGroup.add( _rbReferral );
	Mapanel.add( _rbReferral, gbc );

	_rbRefUpdate = new JRadioButton(DSUtil._resource.getString(_section,
								   "state-refupdate-label"));
	_rbRefUpdate.setSelected( _isRefUpdate );
	_rbRefUpdate.addActionListener(new RadioListener());
	nodeStateGroup.add( _rbRefUpdate );
	Mapanel.add( _rbRefUpdate, gbc );
		
	addBottomGlue();		
    }	
		

    private String[] getReferralListInEntry() {
	Vector v = new Vector(1);

	LDAPAttribute attr = _entry.getAttribute( "nsslapd-Referral" );
	if(attr != null) {
	    return attr.getStringValueArray();
	} else {
	    return null;
	}
  
    }

    private boolean isListEmpty(JList list){
	boolean testlist = (list.getModel().getSize() == 0);
	return( testlist );
    }
    /*
      private void clearListChanges( JComponent view, 
      DefaultListModel _listMod, 
      String[] _saveList ) {
      
      for (int i = _listMod.getSize() - 1; i>= 0;i--) {					
      _listMod.removeElementAt(i);
      }
      for(int i=0;( _saveList != null ) && ( i < _saveList.length ); i++) {
      _listMod.addElement( _saveList[i]);
      }
      setChangeState( view, CHANGE_STATE_UNMODIFIED );
      }
    */

    /**
     * Enable/disable OK button
     *
     * @param ok true to enable the OK button
     */
    private void setOkay( boolean ok ) {
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( ok );
	} else {
	}
    }
									
    private void checkOkay() {	
	setOkay( _isStateDirty && 
		 ( !_isEnabledNode ||
		   _isBackendNode ||
		   _isReferral ||
		   _isRefUpdate ));
	modelUpdate();
    }


    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
	if (e.getSource() instanceof AbstractButton) {
	    StateEnableFields();
	    super.actionPerformed(e);
	}
	checkOkay();
    }

    private void StateEnableFields() {
	if( _cbEnableNode == null ) {
	    return;
	}		
	_rbRefUpdate.setEnabled( _cbEnableNode.isSelected() );
	_rbReferral.setEnabled( _cbEnableNode.isSelected() );
	_rbBackend.setEnabled( _cbEnableNode.isSelected() );
    }


    public void resetCallback() {
	/* No state to preserve */
		reloadEntry();
		setChangeState( _rbRefUpdate, CHANGE_STATE_UNMODIFIED);
		setChangeState( _rbBackend, CHANGE_STATE_UNMODIFIED );
		setChangeState( _rbReferral, CHANGE_STATE_UNMODIFIED );
		_cbEnableNode.setSelected( _isEnabledNode );
		_rbRefUpdate.setSelected( _isRefUpdate );
		_rbReferral.setSelected( _isReferral );
		_rbBackend.setSelected( _isBackendNode );
		_isStateDirty = false;
		clearDirtyFlag();
    }

  /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
    public boolean refresh () {	
		resetCallback();
				
		return true;
	}


	public void reloadEntry() {
		if (_entry == null) {
			return;
		}
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		try {
			_entry = ldc.read(_entry.getDN());
			
			/* We update the values that contain the saved values */
			_saveStatus = 
				_entry.getAttribute("nsslapd-state").getStringValueArray()[0];
			
			if( _saveStatus.compareToIgnoreCase(DISABLE) == 0 ) {
				_isEnabledNode = false;
				_isBackendNode = false;
				_isReferral = false;
				_isRefUpdate = false;
			} else if( _saveStatus.compareToIgnoreCase(BACKEND) == 0 ) {
				_isEnabledNode = true;
				_isBackendNode = true;
				_isReferral = false;
				_isRefUpdate = false;
			} else if( _saveStatus.compareToIgnoreCase( REFERRAL ) == 0 ) {
				_isEnabledNode = true;
				_isBackendNode = false;
				_isReferral = true;
				_isRefUpdate = false;
			} else if( _saveStatus.compareToIgnoreCase( REFERRAL_UPDATE ) == 0 ) {
				_isEnabledNode = true;
				_isBackendNode = false;
				_isReferral = false;
				_isRefUpdate = true;
			}
		} catch (LDAPException e) {
			Debug.println("MappingNodeSettingPanel.reloadEntry(): "+e);
		}
	}

    public boolean write2server(MappingNodeBckPanel _bckTab,  MappingNodeRefPanel _refTab) {
	   
	String selection = null;
	boolean isnewEnabledNode = false;
	boolean isnewRefUpdate = false;
	boolean isnewReferral = false;
	boolean isnewBackendNode = false;
	if( _isStateDirty ) {			
	    LDAPModificationSet attrs = new LDAPModificationSet();			
	    if ( _cbEnableNode.isSelected() ) {
		if( _rbBackend.isSelected() ) {
		    if( _bckTab.nbBck() == 0) {
			DSUtil.showErrorDialog(
				_model.getFrame(),
				"nobck-title",
				"nobck-msg",
				((String[]) null),
				_section );
			return false;
		    }					
		    selection = BACKEND;
		    isnewEnabledNode = true;
		    isnewRefUpdate = false;
		    isnewReferral = false;
		    isnewBackendNode = true;
			    
		} else if ( _rbReferral.isSelected() ) {
		    if( _refTab.nbRef() == 0) {
			DSUtil.showErrorDialog(
				_model.getFrame(),
				"noref-title",
				"noref-msg",
				((String[]) null),
				_section);
			return false;
		    }
		    selection =  REFERRAL;
		    isnewEnabledNode = true;
		    isnewRefUpdate = false;
		    isnewReferral = true;
		    isnewBackendNode = false;
		} else if ( _rbRefUpdate.isSelected() ) {
		    if( _refTab.nbRef() == 0) {
			DSUtil.showErrorDialog(
				_model.getFrame(),
				"noref-title",
				"noref-msg",
				((String[]) null),
				_section );
			return false;
		    }
		    if( _bckTab.nbBck() == 0) {
			DSUtil.showErrorDialog(
				_model.getFrame(),
				"nobck-title",
				"nobck-msg",
				((String[]) null),
				_section );
			return false;
		    }
		    selection =  REFERRAL_UPDATE;
		    isnewEnabledNode = true;
		    isnewRefUpdate = true;
		    isnewReferral = false;
		    isnewBackendNode = false;
		} 				
	    } else {
		selection =   DISABLE;
		isnewEnabledNode = false;
		isnewRefUpdate = false;
		isnewReferral = false;
		isnewBackendNode = false;
				
	    }
	    attrs.add(LDAPModification.REPLACE,
		      new LDAPAttribute( "nsslapd-state", selection));
	    Debug.println("------ " + selection);

	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try {
		ldc.modify( _entry.getDN(), attrs );
		_saveStatus = selection;
		_isEnabledNode = isnewEnabledNode;
		_isRefUpdate = isnewRefUpdate;
		_isReferral = isnewReferral;
		_isBackendNode = isnewBackendNode;
		resetCallback();
	    } catch ( LDAPException e) {
		String[] args_m = { _entry.getDN(), e.toString() };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"update-error",
					args_m,
					_section );
		return false;
	    }
			
	}
	//		DSUtil.showInformationDialog(getModel().getFrame(),"refresh-console","",_section);
	// hideDialog();
	clearDirtyFlag();
	return true;
    }

    public void changedUpdate(DocumentEvent e) {
	super.changedUpdate( e );
	checkOkay();
    }

    public void removeUpdate(DocumentEvent e) {
	super.removeUpdate( e );
	checkOkay();
    }

    private void modelUpdate( ) {
	setDirtyFlag();
	setValidFlag();
    }

    private void addBackend() {
	//		BackendPanel child = 
    }

    public void insertUpdate(DocumentEvent e) {
	super.insertUpdate( e );
	checkOkay();
    }


    class CheckBoxListener implements ItemListener {
	public void itemStateChanged(ItemEvent e) {
	    Object source = e.getItemSelectable();
	    Debug.println("MappingNodeSettingPanel.CheckBoxListener");
	    if ( source ==  _cbEnableNode ) {
		StateEnableFields();				
		if (( _isEnabledNode != _cbEnableNode.isSelected() ) ||
		    _isStateDirty ){
		    setDirtyFlag();
		    _isStateDirty = true;
		} else {
		    clearDirtyFlag();
		    _isStateDirty = false;
		}
	    } 
	    checkOkay();
	    return;
	}
    }

										 
    class RadioListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {			
	    if( e.getSource().equals(_rbBackend)) { 
		if ( !( _isBackendNode ) || !( _isEnabledNode )) {
		    setDirtyFlag();
		    setChangeState( _rbBackend, CHANGE_STATE_MODIFIED);
		    setChangeState( _rbReferral, CHANGE_STATE_UNMODIFIED );
		    setChangeState( _rbRefUpdate, CHANGE_STATE_UNMODIFIED );
		    _isStateDirty = true;
		} else {
		    clearDirtyFlag();
		    _isStateDirty = false;
		    setChangeState( _rbRefUpdate, CHANGE_STATE_UNMODIFIED);
		    setChangeState( _rbBackend, CHANGE_STATE_UNMODIFIED );
		    setChangeState( _rbReferral, CHANGE_STATE_UNMODIFIED );					
		}

	    } else if (e.getSource().equals(_rbReferral)) {
		if (!( _isReferral ) || !( _isEnabledNode)) {
		    setDirtyFlag();
		    setChangeState( _rbReferral, CHANGE_STATE_MODIFIED);
		    setChangeState( _rbBackend, CHANGE_STATE_UNMODIFIED );
		    setChangeState( _rbRefUpdate, CHANGE_STATE_UNMODIFIED );
		    _isStateDirty = true;
		} else {
		    clearDirtyFlag();
		    setChangeState( _rbRefUpdate, CHANGE_STATE_UNMODIFIED);
		    setChangeState( _rbBackend, CHANGE_STATE_UNMODIFIED );
		    setChangeState( _rbReferral, CHANGE_STATE_UNMODIFIED );
		    _isStateDirty = false;
		}

	    } else if ( e.getSource().equals(_rbRefUpdate)) { 
					   
		if ( !( _isRefUpdate ) || !( _isEnabledNode)) {
		    setDirtyFlag();
		    setChangeState( _rbRefUpdate, CHANGE_STATE_MODIFIED);
		    setChangeState( _rbBackend, CHANGE_STATE_UNMODIFIED );
		    setChangeState( _rbReferral, CHANGE_STATE_UNMODIFIED );
		    _isStateDirty = true;
		} else {
		    clearDirtyFlag();
		    setChangeState( _rbRefUpdate, CHANGE_STATE_UNMODIFIED);
		    setChangeState( _rbBackend, CHANGE_STATE_UNMODIFIED );
		    setChangeState( _rbReferral, CHANGE_STATE_UNMODIFIED );
		    _isStateDirty = false;
					
		}
	    }
	    checkOkay();
	}
    }

    private JTextField			_mappingNameText;
    private JLabel				_mappingNameLabel;
    private JLabel				_NodeText;		 
    private JLabel				_NodeLabel;
    private JLabel	   			_DadyNodeText;
    private JLabel				_DadyNodeLabel;

    private IDSModel			_model = null;
										 
    private JRadioButton		_rbBackend;
    private JRadioButton		_rbReferral;
    private JRadioButton		_rbRefUpdate;
    private boolean				_isEnabledNode = false;
    private boolean				_isBackendNode = false;
    private boolean				_isReferral = false;
    private boolean				_isRefUpdate = false;
    private boolean				_isStateDirty = false;
    private String				_saveStatus = null;



    private JCheckBox			_cbEnableNode = null;


    private final static String _section = "mappingtree-setting";
    static final String CONFIG_BASEDN = DSUtil.PLUGIN_CONFIG_BASE_DN ;
    static final String CONFIG_MAPPING = DSUtil.MAPPING_TREE_BASE_DN ;
    static final String DISABLE = MappingUtils.DISABLE;
    static final String BACKEND = MappingUtils.BACKEND;
    static final String REFERRAL = MappingUtils.REFERRAL;
    static final String REFERRAL_UPDATE = MappingUtils.REFERRAL_UPDATE;
    static final String[] STATUS_ENTRIES = { DISABLE,
					     BACKEND,
					     REFERRAL,
					     REFERRAL_UPDATE };
    private final int ROWS = 4; 

    //copy from BlankPanel
    final static int DEFAULT_PADDING = 6;
    final static Insets DEFAULT_EMPTY_INSETS = new Insets(0,0,0,0);
    final static Insets BOTTOM_INSETS = new Insets(6,6,6,6);


    private LDAPEntry _entry = null;
 
}
