/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.admin.dirserv.panel.LDAPUrlPanel;
import com.netscape.management.nmclf.SuiConstants;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @see     com.netscape.admin.dirserv
 */

public class MappingNodeRefPanel extends BlankPanel  
    implements SuiConstants {

    public MappingNodeRefPanel( IDSModel model,
				LDAPEntry entry ) {
	super( model, _section );
	_helpToken = "configuration-mapping-referral-help";
	_model = model;
	_entry = entry;
	_refreshWhenSelect = false;
    }


    public void init() {
		if (_isInitialized) {
			return;
		}
	_myPanel.setLayout( new GridBagLayout() );
	GridBagConstraints gbc = getGBC();

	DSEntrySet entries = getDSEntrySet();			
	createDataArea( _myPanel, entries );
	addBottomGlue ();
	_isInitialized = true;
    }

	
    protected void 	createDataArea( JPanel grid, DSEntrySet entries ) {
		
	GridBagConstraints gbc = getGBC();
	JPanel Mapanel = new JPanel();
        gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
        grid.add( Mapanel, gbc );

	GridBagLayout Mabag = new GridBagLayout();
	GridBagConstraints Magbc = new GridBagConstraints() ;
        Magbc.gridx      = 0;
        Magbc.gridy      = 0;
	//        Magbc.gridwidth  = Magbc.REMAINDER;
        Magbc.gridwidth	 = 1;
        Magbc.gridheight = 1;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
        Magbc.fill       = Magbc.BOTH;
        Magbc.anchor     = Magbc.CENTER;
        Magbc.insets     = new Insets(0,0,0,0);
        Magbc.ipadx = 0;
        Magbc.ipady = 0;

	Mapanel.setLayout(Mabag);

	/* Referrals Label */
	_refListLabel = makeJLabel ( _section, "list" );
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.insets = new Insets(0, 0, 0, 0);
	Magbc.anchor = Magbc.SOUTHWEST;		
	Magbc.gridy++;
	Mapanel.add( _refListLabel,Magbc);


	/* Referrals List */
	_refData = new DefaultListModel();
	_refList = new JList( _refData );
	_refListLabel.setLabelFor(_refList);
	_refList.setVisibleRowCount(ROWS);
	_refList.setCellRenderer( new ReferralCellRenderer() );
	_saverefList = getReferralListInEntry();
	for(int i=0;(_saverefList != null) && i < _saverefList.length; i++ ){
	    _refData.addElement( _saverefList[i]);
	}
	JScrollPane scrollRef = new JScrollPane();
	scrollRef.getViewport().setView(_refList);
	Magbc.anchor     = Magbc.CENTER;
	Magbc.gridx = 0;
	Magbc.gridy++;
	Magbc.insets = new Insets(0, 0, 0, 0);
        Magbc.weightx    = 1;
	Mapanel.add( scrollRef, Magbc);	
			
	_bDeleteRef = UIFactory.makeJButton(this, _section, "bdelete", (ResourceSet)null);
	_bDeleteRef.setActionCommand(DELETE_REF);
	_bDeleteRef.setEnabled(false);
	Magbc.gridx = 1;
	Magbc.weightx    = 0;
	Magbc.anchor = Magbc.NORTH;
	Magbc.fill     = Magbc.HORIZONTAL;
	Magbc.insets = new Insets(0, DIFFERENT_COMPONENT_SPACE,
				  0, 0);
	Mapanel.add( _bDeleteRef,Magbc);

	/* just to make it nicer */
	Magbc.gridy++;
	Magbc.insets = new Insets(DIFFERENT_COMPONENT_SPACE, 0, 0, 0);
	_refNewLabel = makeJLabel ( _section, "new" );
	Magbc.gridx = 0;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.anchor = Magbc.SOUTHWEST;
	Mapanel.add( _refNewLabel, Magbc);

	/* New Referrals Line */		
        _refNewRefText = makeJTextField(_section, "new");
	Magbc.gridy++;
	Magbc.gridx = 0;
        Magbc.weightx    = 1;
        Magbc.weighty    = 0;		
	Magbc.insets     = new Insets(0, 0, COMPONENT_SPACE, 0);
	//Magbc.anchor = Magbc.CENTER;
	Mapanel.add( _refNewRefText,Magbc);
	_refNewLabel.setLabelFor(_refNewRefText);

	_bAddRef = UIFactory.makeJButton(this, _section, "badd", (ResourceSet)null);
	_bAddRef.setActionCommand(ADD_REF);		
	_bAddRef.setEnabled( false );
	Magbc.gridx = 1;
	Magbc.weightx    = 0;
	Magbc.insets     = new Insets(0,
				      DIFFERENT_COMPONENT_SPACE,
				      COMPONENT_SPACE,
				      0);
	Mapanel.add( _bAddRef,Magbc);		


	_bHelpRef = UIFactory.makeJButton(this, _section, "bhelp", (ResourceSet)null);
	_bHelpRef.setActionCommand(HELP_REF);		
	_bHelpRef.setEnabled( true );
	Magbc.gridy++;
	Magbc.gridx = 1;
	Magbc.weightx    = 0;
	Magbc.insets     = new Insets(0,
				      DIFFERENT_COMPONENT_SPACE,
				      COMPONENT_SPACE,
				      0);
	// Magbc.anchor = Magbc.SOUTHEAST;
	Mapanel.add( _bHelpRef,Magbc);	   
    }

    private String[] getBackendListInEntry() {
	Vector v = new Vector(1);
  
	LDAPAttribute attr = _entry.getAttribute( "nsslapd-backend" );
	if(attr != null) {
	    return attr.getStringValueArray();
	} else {
	    return null;
	}	
    }

    private String[] getReferralListInEntry() {
	Vector v = new Vector(1);

	LDAPAttribute attr = _entry.getAttribute( "nsslapd-Referral" );
	if(attr != null) {
	    return attr.getStringValueArray();
	} else {
	    return null;
	}
  
    }

    private boolean isListEmpty(JList list){
	boolean testlist = (list.getModel().getSize() == 0);
	return( testlist );
    }

    private void clearReferralsChanges() {
	clearListChanges( _refListLabel, _refData, _saverefList );
	_refDirty = false;
    }

    private void clearListChanges( JComponent view, DefaultListModel _listMod, String[] _saveList ) {		
	for (int i = _listMod.getSize() - 1; i>= 0;i--) {					
	    _listMod.removeElementAt(i);
	}
	for(int i=0;( _saveList != null ) && ( i < _saveList.length ); i++) {
	    _listMod.addElement( _saveList[i]);
	}
	setChangeState( view, CHANGE_STATE_UNMODIFIED );
    }
	
    private String[] getReferralsInList() {
	/*
	  Vector v = new Vector(1);
	  Component CVals[] = _refList.getComponents();

	  if (( CVals == null ) || (CVals.length == 0)) {
	  return( null );
	  }

	  String[] refList = new String[CVals.length];
				
	  for(int i =0; i <  CVals.length; i++) {
	  Debug.printl("-----" + CVals[i].toString());
	  v.addElement( CVals[i].toString() );
	  }
	  v.toArray(refList);
	  return refList;
	*/
	Vector v = new Vector();
		
	for (int i = 0;
	     i < _refList.getModel().getSize();
	     i++) {
	    v.addElement( _refList.getModel().getElementAt( i ));
	}
	if( v.size()  == 0 ) {
	    return ( null );
	} else {
	    String referralsList[] = new String[ v.size() ];
	    v.toArray( referralsList );
	    return ( referralsList );
	}
    }


    /**
     * Enable/disable OK button
     *
     * @param ok true to enable the OK button
     */
    private void setOkay( boolean ok ) {
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( ok );
	} else {
	}
    }

    private void checkOkay() {	   
	setOkay( true );
	modelUpdate();
    }


    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {

	if(e.getActionCommand().equals( ADD_REF )) {
		String ldapUrl = _refNewRefText.getText().trim();
		if (ldapUrl != null) {
			if (!DSUtil.isValidLDAPUrl(ldapUrl)) {
				int wantToContinue = DSUtil.showConfirmationDialog( getModel().getFrame(),
																	"LDAPUrl-no-good", 
																	(String)null,
																	_section);
				if (wantToContinue != JOptionPane.YES_OPTION) {
					return;								
				}
			}
			_refData.addElement( ldapUrl );
			_refNewRefText.setText("");
			_bAddRef.setEnabled( false );
			checkRefDirty();
		}
	} else if ( e.getActionCommand().equals( DELETE_REF )) {
	    int[] indices = _refList.getSelectedIndices();
	    for (int i = indices.length - 1; i>= 0;i--) {					
		_refData.removeElementAt(indices[i]);
	    }						
	    _bDeleteRef.setEnabled( false );
	    checkRefDirty();
	}  else if ( e.getActionCommand().equals( HELP_REF )){
	    String url = constructLDAPUrl();
	    if ( url != null ) {
		_refNewRefText.setText( url );
		_bAddRef.setEnabled( true );
	    } 
	} else {
	    Debug.println(" MappingNodeSettingPanel.actionPerformed() : wrong action performed");
	}
	checkOkay();
    }


    private void checkRefDirty() {
	String[] ref =  getReferralsInList();
	String[] diffMore = MappingUtils.whatsMoreInThisList( _saverefList, 
							      ref );
	String[] diffLess = MappingUtils.whatsMoreInThisList( ref ,
							      _saverefList );
	_refDirty = ((( diffMore != null) && ( diffMore.length > 0 )) || 
		     (( diffLess != null) &&( diffLess.length > 0 )));
	if ( _refDirty ) {
	    setDirtyFlag();
	    setChangeState( _refListLabel, CHANGE_STATE_MODIFIED);
	} else {
	    clearDirtyFlag();
	    setChangeState( _refListLabel, CHANGE_STATE_UNMODIFIED );
	}
    }

    public void resetCallback() {
	/* No state to preserve */
		reloadEntry();
		clearReferralsChanges();
		clearDirtyFlag();
    }

	/**
	 * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
    public boolean refresh () {	
		resetCallback();
				
		return true;
	}
		

	public void reloadEntry() {
		if (_entry == null) {
			return;
		}
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		try {
			_entry = ldc.read(_entry.getDN());
			
			/* We update the values that contain the saved values */
			_saverefList = getReferralListInEntry();
		} catch (LDAPException e) {
			Debug.println("MappingNodeRefPanel.reloadEntry(): "+e);
		}
	}

    public void okCallback() {
	/* No state to preserve */
	if ( _refDirty ) {
	    LDAPModificationSet attrs = new LDAPModificationSet();
	    saveRef( attrs );
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try {
		ldc.modify( _entry.getDN(), attrs );
		_saverefList = getReferralsInList();
		checkRefDirty();
	    } catch ( LDAPException e) {
		String[] args_m = { _entry.getDN(), e.toString() };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"update-error",
					args_m,
					_section );
	    }
	}
	clearDirtyFlag();
    }
										 
    private void saveRef( LDAPModificationSet attrs ) {
	String[] rIL = getReferralsInList();
	if (( _saverefList == null ) ||
	    ( _saverefList.length == 0)) {
	    if ( rIL != null ) {
		attrs.add(LDAPModification.ADD,
			  new LDAPAttribute( "nsslapd-Referral", rIL ));
	    }
	} else {
	    String[] newRef = MappingUtils.whatsMoreInThisList( _saverefList,
								rIL );
	    String[] removedRef  = MappingUtils.whatsMoreInThisList( rIL, 
								     _saverefList );
	    if(( newRef != null) && ( newRef.length != 0 )) {
		attrs.add( LDAPModification.ADD,
			   new LDAPAttribute( "nsslapd-Referral", newRef));
	    }
	    if((removedRef != null) && (removedRef.length != 0)) {
		attrs.add( LDAPModification.DELETE,
			   new LDAPAttribute( "nsslapd-Referral", removedRef));
	    }
	}
    }


    public boolean doDel() {
	/* No state to preserve */
	boolean isOK = true;
	LDAPModificationSet attrs = new LDAPModificationSet();
	if ( _refDirty && delRef( attrs )) {
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try {
		ldc.modify( _entry.getDN(), attrs );
		_saverefList = getReferralsInList();
		checkRefDirty();
	    } catch ( LDAPException e) {
		String[] args_m = { _entry.getDN(), e.toString() };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"update-error",
					args_m,
					_section );
		isOK = false;
	    }
	}
	return( isOK );
	//	clearDirtyFlag();
    }
										 
    private boolean delRef( LDAPModificationSet attrs ) {
	String[] rIL = getReferralsInList();
	if (( _saverefList == null ) ||
	    ( _saverefList.length == 0)) {
	    // not suppose to delete when previous list is empty !!
	    return( false );
	} else {
	    String[] removedRef  = MappingUtils.whatsMoreInThisList( rIL, 
								     _saverefList );
	    if((removedRef != null) && (removedRef.length != 0)) {
		attrs.add( LDAPModification.DELETE,
			   new LDAPAttribute( "nsslapd-Referral", removedRef));
		return( true );
	    } else {
		// nothing to remove
		return( false );
	    }
	}
    }

    public boolean doAdd() {
	/* No state to preserve */
	boolean isOK = true;
	LDAPModificationSet attrs = new LDAPModificationSet();
	if ( _refDirty && addRef( attrs )) {
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try {
		ldc.modify( _entry.getDN(), attrs );
		_saverefList = getReferralsInList();
		checkRefDirty();
	    } catch ( LDAPException e) {
		String[] args_m = { _entry.getDN(), e.toString() };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"update-error",
					args_m,
					_section );
		isOK = false;
	    }
	}
	return( isOK );
	//	clearDirtyFlag();
    }
										 
    private boolean addRef( LDAPModificationSet attrs ) {
	String[] rIL = getReferralsInList();
	if (( _saverefList == null ) ||
	    ( _saverefList.length == 0)) {
	    if ( rIL != null ) {
		attrs.add(LDAPModification.ADD,
			  new LDAPAttribute( "nsslapd-Referral", rIL ));
		return( true );
	    } else {
		return( false );
	    }
	} else {
	    String[] newRef = MappingUtils.whatsMoreInThisList( _saverefList,
								rIL );
	    if(( newRef != null) && ( newRef.length != 0 )) {
		attrs.add( LDAPModification.ADD,
			   new LDAPAttribute( "nsslapd-Referral", newRef));
		return( true );
	    } else {
		// nothing to add....
		return( false );
	    }
	}
    }

    public int nbRef(){
		if (_isInitialized) {
			return ( _refList == null?0: _refList.getModel().getSize() );
		} else {
			/* The case where the MappingNodeSettingPanel calls this method 
			   and the panel has not been initialized */			
			String[] referrals = getReferralListInEntry();
			if (referrals != null) {
				return referrals.length;
			}
			return 0;
		}	
    }    

    public void changedUpdate(DocumentEvent e) {
	super.changedUpdate( e );
	checkOkay();
    }

    public void removeUpdate(DocumentEvent e) {
	super.removeUpdate( e );
	checkOkay();
    }

    private void modelUpdate( ) {
	if ( _refDirty ){
	    setDirtyFlag();
	    setValidFlag();
	}
    }

    private void addBackend() {
	//		BackendPanel child = 
    }

    public void insertUpdate(DocumentEvent e) {
	boolean bAddRef_status = (( _refNewRefText != null ) && 
				  ( _refNewRefText.getText().trim().length() > 0 ));
	if (_bAddRef != null ) {
	    _bAddRef.setEnabled( bAddRef_status );
	}

	super.insertUpdate( e );
	checkOkay();
    }

    protected String  constructLDAPUrl() {	   		

	LDAPUrlDialog dlg =
	    new LDAPUrlDialog( getModel().getFrame());
	dlg.packAndShow();
	LDAPUrl url = dlg.getLDAPUrl();
	if ( url != null ) {
	    return ( url.getUrl());
	} else {
	    return ( null );
	}
    } 


    class ReferralCellRenderer extends DefaultListCellRenderer {  
	// This is the only method defined by ListCellRenderer.  We just
	// reconfigure the Jlabel each time we're called.
		
	public Component getListCellRendererComponent(
						      JList list,
						      Object value,            // value to display
						      int index,               // cell index
						      boolean isSelected,      // is the cell selected
						      boolean cellHasFocus)    // the list and the cell have the focus
	{
	    if (list.isSelectionEmpty()) {
		_bDeleteRef.setEnabled(false);
	    } else {
		_bDeleteRef.setEnabled(true);
	    }
	    checkOkay();
	    return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
    }

    private IDSModel			_model = null;

    private JLabel				_refListLabel;
    private JTextField			_refNewRefText;
    private JLabel				_refNewRefLabel;
    private JList				_refList;
    private String[]		   	_saverefList;										 
    private JButton				_bAddRef;
    private JButton				_bDeleteRef;
    private JButton				_bHelpRef;
    private DefaultListModel	_refData;
    private boolean				_refDirty = false;

    private JLabel				_refNewHostLabel;
    private JTextField			_refNewHostText;
    private JLabel				_refNewPortLabel;
    private JTextField			_refNewPortText;

    private JLabel				_refNewLabel;

    private JLabel				_refTargetDNLabel;
    private JTextField			_refTargetDNText;


    private final static String _section = "mappingtree-referral";
    static final String CONFIG_BASEDN = DSUtil.PLUGIN_CONFIG_BASE_DN ;
    static final String CONFIG_MAPPING = DSUtil.MAPPING_TREE_BASE_DN ;
    // static final String ROOT_MAPPING_NODE = "is root suffix";
    static final String ADD_REF = "addReferral";
    static final String DELETE_REF = "deleteReferral";
    static final String HELP_REF = "helpReferral";
    private final int ROWS = 4; 

    //copy from BlankPanel
    final static int DEFAULT_PADDING = 6;
    final static Insets DEFAULT_EMPTY_INSETS = new Insets(0,0,0,0);
    final static Insets BOTTOM_INSETS = new Insets(6,6,6,6);

    private LDAPEntry _entry = null;
 
}
