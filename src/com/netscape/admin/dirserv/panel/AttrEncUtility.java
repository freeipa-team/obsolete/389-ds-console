/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import netscape.ldap.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * Utility for Encrypted Attributes dialog and panel
 *
 * @author  chrisho, modified by cheston williams
 * @version %I%, %G%
 * @date                12/31/97
 * @see     com.netscape.admin.dirserv
 */

public class AttrEncUtility {
    public static boolean modifyAttributes(IDSModel model, 
        String DN, LDAPModificationSet mods) {

        int code = isModifySuccess(model, DN, mods);
        if (code == SUCCESS)
            return true;
        else if (code == FAILURE)
            return false;
        else if (code == MODIFYAGAIN) {
            try {
                LDAPConnection ld = model.getServerInfo().getLDAPConnection();
                ld.modify(DN, mods);
            } catch (LDAPException e) {
				if (SwingUtilities.isEventDispatchThread()) {
					JOptionPane.showMessageDialog(model.getFrame(), e.toString(),
													  "Error Message", JOptionPane.ERROR_MESSAGE);
				} else {
					final IDSModel fModel = model;
					final LDAPException fEx = e;
					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {
								JOptionPane.showMessageDialog(fModel.getFrame(), fEx.toString(),
															  "Error Message", JOptionPane.ERROR_MESSAGE);
							}
						});
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
                return false;
            }
        }
        return true;
    }

    public static boolean addAttribute(IDSModel model, LDAPEntry entry) {
        int code = isAddSuccess(model, entry);
        
        if (code == SUCCESS)
            return true;
        else if (code == FAILURE)
            return false;
        else if (code == ADDAGAIN) {
            try {
                LDAPConnection ld = model.getServerInfo().getLDAPConnection();
                ld.add(entry);
            } catch (LDAPException e) {				
				if (SwingUtilities.isEventDispatchThread()) {
					DSUtil.showLDAPErrorDialog( model.getFrame(),
												e,
												"updating-directory-title" );
				} else {
					final IDSModel fModel = model;
					final LDAPException fEx = e;
					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {
								DSUtil.showLDAPErrorDialog( fModel.getFrame(),
															fEx,
															"updating-directory-title" );
							}
						});
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				return false;
            }
        }
        return true;
    }

    public static boolean deleteAttribute(IDSModel model,
					  String DN) {
        int code = isDeleteSuccess(model, DN);
	if (code == SUCCESS)
	    return true;
	else if (code == FAILURE)
	    return false;
	else if (code == DELETEAGAIN) {
	    try {
	        LDAPConnection ld = model.getServerInfo().getLDAPConnection();
		ld.delete(DN);
	    } catch (LDAPException e) {
			if (SwingUtilities.isEventDispatchThread()) {
				JOptionPane.showMessageDialog(model.getFrame(), e.toString(),
											  "Error Message", 
											  JOptionPane.ERROR_MESSAGE);
			} else {
				final IDSModel fModel = model;
				final LDAPException fEx = e;
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							JOptionPane.showMessageDialog(fModel.getFrame(), fEx.toString(),
														  "Error Message", 
														  JOptionPane.ERROR_MESSAGE);
						}
					});
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
	        
			return false;
	    }
	}
	return true;
    }

    private static int isModifySuccess(IDSModel model, String DN, 
				       LDAPModificationSet mods) {
      
        LDAPConnection ld = model.getServerInfo().getLDAPConnection();
	try {
	    ld.modify(DN, mods);
            return SUCCESS;
        } catch (LDAPException e) {
            switch (e.getLDAPResultCode()) {
                case LDAPException.INVALID_CREDENTIALS:
                    if (!model.getNewAuthentication(false))
                        return FAILURE;
                    break;
                default:
					if (SwingUtilities.isEventDispatchThread()) {
						JOptionPane.showMessageDialog(model.getFrame(),
													  e.toString(), "Error Message",
													  JOptionPane.ERROR_MESSAGE);
					} else {
						final IDSModel fModel = model;
						final LDAPException fEx = e;
						try {
							SwingUtilities.invokeAndWait(new Runnable() {
								public void run() {
									JOptionPane.showMessageDialog(fModel.getFrame(),
													  fEx.toString(), "Error Message",
													  JOptionPane.ERROR_MESSAGE);
								}
							});
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					return FAILURE;
            }
        } finally {
			model.notifyAuthChangeListeners();
		}
        return MODIFYAGAIN;
    }

    private static int isAddSuccess(IDSModel model, LDAPEntry entry) {
        LDAPConnection ld = model.getServerInfo().getLDAPConnection();
        try {
            ld.add(entry);
            return SUCCESS;
        } catch (LDAPException e) {
            switch (e.getLDAPResultCode()) {
                case LDAPException.INVALID_CREDENTIALS:
                    if (!model.getNewAuthentication(false))
                        return FAILURE;
                    break;
                default:
					if (SwingUtilities.isEventDispatchThread()) {
						DSUtil.showLDAPErrorDialog( model.getFrame(),
													e,
													"updating-directory-title" );
					} else {
						final IDSModel fModel = model;
						final LDAPException fEx = e;
						try {
							SwingUtilities.invokeAndWait(new Runnable() {
								public void run() {
									DSUtil.showLDAPErrorDialog( fModel.getFrame(),
																fEx,
																"updating-directory-title" );
								}					
							});
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					return FAILURE;
			}
		} finally {
			model.notifyAuthChangeListeners();
        }
        return ADDAGAIN;
    }

    private static int isDeleteSuccess(IDSModel model, String DN) {
        LDAPConnection ld = model.getServerInfo().getLDAPConnection();
	try {
	    ld.delete(DN);
	    return SUCCESS;
	} catch (LDAPException e) {
	  switch (e.getLDAPResultCode()) {
	      case LDAPException.INVALID_CREDENTIALS:
		  if (!model.getNewAuthentication(false))
		      return FAILURE;
		  break;
	      default:
			  if (SwingUtilities.isEventDispatchThread()) {
				  JOptionPane.showMessageDialog(model.getFrame(),
												e.toString(), "Error Message",
												JOptionPane.ERROR_MESSAGE);
			  } else {
				  final IDSModel fModel = model;
					final LDAPException fEx = e;
					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {
								JOptionPane.showMessageDialog(fModel.getFrame(),
															  fEx.toString(), "Error Message",
															  JOptionPane.ERROR_MESSAGE);
							}
						});
					} catch (Exception ex) {
						ex.printStackTrace();
					}
			  }
			  return FAILURE;
	  }
	} finally {
		model.notifyAuthChangeListeners();
	}
	return DELETEAGAIN;
    }

    static final public int SUCCESS = 0;
    static final public int FAILURE = 1;
    static final public int MODIFYAGAIN = 2;
    static final public int DELETEAGAIN = 3;
    static final public int ADDAGAIN = 4;
}






