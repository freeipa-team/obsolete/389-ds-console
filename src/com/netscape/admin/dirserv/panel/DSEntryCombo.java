/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.Vector;
import java.util.Enumeration;
import javax.swing.JComponent;
import javax.swing.JComboBox;
import javax.swing.AbstractButton;
import javax.swing.JLabel;
import javax.swing.UIManager;
import java.awt.Color;

import com.netscape.admin.dirserv.*;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server
 *
 * @version
 * @author
 */
public class DSEntryCombo extends DSEntry {
    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle combobox entries that are localized.  The first parameter is an array of
     * strings that are used to update the directory server and corresponds to
     * the localized combobox entries.
     *
     * @param comboEntries a string array of valid LDAP entries that have been localized in the view
     * @param view the JComboBox which stores localized entries that need to be replaced with standard LDAP syntax
     */
    public DSEntryCombo(
        String[] comboEntries,
        JComboBox view, 
        boolean allowBlank
    ) {
        super((String)null, view);
		setEntries (comboEntries);	
        _allowBlank = allowBlank;
    }

    public DSEntryCombo(
        String[] comboEntries,
        JComponent view1, JComponent view2, 
        boolean allowBlank 
    ) {
        super((String)null, view1, view2);
        setEntries (comboEntries);	
        _allowBlank = allowBlank;
    }

    public void setEntries (String[] comboEntries) {
       	_comboEntries = null; // wipes out existing values, if any
		if (comboEntries != null && comboEntries[0] != null) {
			_comboEntries = new Vector(comboEntries.length);
			for (int ii = 0; ii < comboEntries.length; ++ii) {
				_comboEntries.addElement(comboEntries[ii]);
			}
		}
    }

    public void addEntry (String entry) {
        if (_comboEntries == null)
            _comboEntries = new Vector(1);
        
        _comboEntries.addElement (entry);    
    }

    public void removeEntry (String entry){
        if (_comboEntries != null)
            _comboEntries.removeElement (entry);
    }

    /**
     * First, get the model as an integer.  If the model is not an integer,
     * just set the value to 0.  Next, convert the value using the given
     * scale factor.  Last, update the view.
     */
    public void show() {
        // update the view
		JComboBox cb = (JComboBox)getView(0);
		String entry = getModel(0);
		int index = _comboEntries.indexOf(entry);
		if (index != -1)
    		cb.setSelectedIndex(index);
    	else
		{
    		cb.setSelectedIndex(0);
		}

        viewInitialized ();
	}

	protected void setInitModel () {
		JComboBox cb = (JComboBox)getView(0);
		String entry = getModel(0);

		if (_comboEntries != null)
		{
			int index = _comboEntries.indexOf(entry);
			if (index == -1)
    			index = 0;
    	
			setModelAt ((String)_comboEntries.elementAt(index), 0);
		}
	}

	/**
	 * since data is update dynamically every time selection is changed
     * through a call to updateModel, nothing needs to be done here
     */
	public void store() {

	}

	/**
	 * First, call validate to see if the value is valid.  If not valid, don't
	 * do anything.  validate() will usually be called before store() by
	 * DSEntrySet.  If the field is valid, we can assume it is a valid
	 * file/directory.
	 * Get the value as an integer and apply the scale factor, then update
	 * the model.
	 */
	protected void updateModel() {
	    JComboBox cb = (JComboBox)getView(0);
	    int index = cb.getSelectedIndex();

		if (_comboEntries != null && index >= 0)
			setModelAt((String)_comboEntries.elementAt(index), 0);
    }

    public int validate() {
        if (_allowBlank)
            return 0;

        // must have a selection
        JComboBox box   = (JComboBox)getView (0);
        int       index = box.getSelectedIndex();

        if (index >= 0)
            return 0;
        else
            return 1;
    }

    protected Vector _comboEntries = null; // a Vector of Strings
    private boolean  _allowBlank = true;
}
