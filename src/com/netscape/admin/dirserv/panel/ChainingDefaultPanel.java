/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.util.*;
import java.io.File;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import com.netscape.management.nmclf.SuiConstants;
/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ChainingDefaultPanel extends BlankPanel 
								  implements  SuiConstants{
     								
    public ChainingDefaultPanel(IDSModel model, LDAPEntry InstEntry) {
		super( model, _section, true );
		_helpToken = "configuration-chaining-default-help";
		_dnEntry =  DEFAULT_PREFIX + InstEntry.getDN();
		_configEntry = InstEntry;
		_model = model;
		_refreshWhenSelect = false;			
	}

	public void init() {
		if (_isInitialized) {
			return;
		}
		linkData();
		_myPanel.setLayout(new GridBagLayout());
		createTwinArea((Container) _myPanel);
		addBottomGlue();
		_isInitialized = true;
		
	}



	private void createTwinArea( Container myContainer ) {
		
		GridBagConstraints pgbc = new GridBagConstraints();
        pgbc.gridx      = 0;
        pgbc.gridy      = 0;
        pgbc.gridwidth	 = 1;
        pgbc.gridheight = 1;
        pgbc.weightx    = 1;
        pgbc.weighty    = 0;
        pgbc.fill       = pgbc.BOTH;
        pgbc.anchor     = pgbc.NORTHWEST;
        pgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
									0,DEFAULT_PADDING);
        pgbc.ipadx = 0;
        pgbc.ipady = 0;	
        JPanel leftPanel = new GroupPanel( 
								DSUtil._resource.getString( _section, 
															"client-title" ),
								true);
		JPanel rightPanel = new GroupPanel( 
								DSUtil._resource.getString( _section, 
															"cascade-title" ),
								true);
		JPanel bottomPanel = new GroupPanel(
								DSUtil._resource.getString( _section,
															"connection-mgt-title" ),
								true);									
		
		pgbc.gridy = 0;
		pgbc.gridwidth = 1;
		pgbc.fill = pgbc.BOTH;   
		myContainer.add( leftPanel, pgbc );
		
		pgbc.gridx++;
		myContainer.add( rightPanel, pgbc );

		pgbc.gridx = 0;
		pgbc.gridy++;
		pgbc.gridwidth = pgbc.REMAINDER;
		myContainer.add( bottomPanel, pgbc );

		createClientArea( leftPanel );
		createCascadeArea( rightPanel );
		createConnMgt( bottomPanel );
	}

	private void createClientArea( JPanel panel ) {

		GridBagLayout chbag = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth	 = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(0,DEFAULT_PADDING,
									0,DEFAULT_PADDING);
        gbc.ipadx = 0;
        gbc.ipady = 0;
	   	
		panel.setLayout(chbag);
		
		/**
		 * Hidden because to dangerous, it transfort every damn anonymous
		 * into proxy user on remote server
		 *
		 * panel.add( _cbnsproxiedauthorization, gbc );
		 * gbc.gridy++;
		 */
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.NONE;
		panel.add(_cbnsreferralonscopedsearch, gbc );
		gbc.fill = gbc.HORIZONTAL;

		gbc.insets.top = DEFAULT_PADDING;
		gbc.gridy++;
        gbc.gridx = 0;
		gbc.gridwidth	 = 1;

		addEntryField( panel,
					   gbc,
					   _lfnsslapd_sizelimit,
					   _tfnsslapd_sizelimit,
					   lns_sizelimit_unit );
		gbc.gridy++;
        gbc.gridx = 0;
		addEntryField( panel,
					   gbc,
					   _lfnsslapd_timelimit,
		 			   _tfnsslapd_timelimit,
		 			   lns_timelimit_unit );

		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.gridy++;
		gbc.fill =gbc.VERTICAL;
		gbc.weighty = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		panel.add(Box.createVerticalGlue(), gbc);
	}

	private void createCascadeArea( JPanel panel ) {

		GridBagLayout chbag = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(0,DEFAULT_PADDING,
									0,DEFAULT_PADDING);
        gbc.ipadx = 0;
        gbc.ipady = 0;
	   	
		panel.setLayout(chbag);
		
		/**
		 * Hidden because to dangerous, it transfort every damn anonymous
		 * into proxy user on remote server
		 *
		 * panel.add( _cbnsproxiedauthorization, gbc );
		 * gbc.gridy++;
		 */

		gbc.gridwidth  = gbc.REMAINDER;
		gbc.fill = gbc.NONE;
		panel.add(_cbnschecklocalaci, gbc );
		gbc.fill = gbc.HORIZONTAL;
		gbc.gridy++;
        gbc.gridx = 0;
		gbc.insets.top = DEFAULT_PADDING;

		gbc.gridwidth  = 1;
		addEntryField( panel,
					   gbc,
					   _lfnshoplimit,
					   _tfnshoplimit);

		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.gridy++;
		gbc.fill =gbc.VERTICAL;
		gbc.weighty = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		panel.add(Box.createVerticalGlue(), gbc);		
	}


	private void createConnMgt( JPanel panel ) {
	
		GridBagLayout chbag = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth	 = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(0,DEFAULT_PADDING,
									0,DEFAULT_PADDING);
        gbc.ipadx = 0;
        gbc.ipady = 0;
	   	
		panel.setLayout(chbag);
		
		gbc.gridy++;
        gbc.gridx = 0;
		//		JLabel lmax = makeJLabel( _section, "shared-label-max-conn");
		//		panel.add( lmax, gbc );
		//		gbc.gridx = 1;
		addEntryField( panel,
					   gbc,
					   _lfnsbindconnectionslimit,
					   _tfnsbindconnectionslimit );
		addEntryField( panel,
					   gbc,
					   _lfnsoperationconnectionslimit,
					   _tfnsoperationconnectionslimit );

		gbc.insets.top = DEFAULT_PADDING;
		gbc.gridy++;
        gbc.gridx = 0;
		//		JLabel lbind = makeJLabel( _section, "shared-label-bind");
		//		panel.add( lbind, gbc );
		//		gbc.gridx = 1;
		addEntryField( panel,
					   gbc,
					   _lfnsbindtimeout,
					   _tfnsbindtimeout );
		addEntryField( panel,
					   gbc,
					   _lfnsbindretrylimit,
					   _tfnsbindretrylimit );		

		gbc.gridy++;
        gbc.gridx = 0;
		addEntryField( panel,
					   gbc,
					   _lfnsconcurrentbindlimit,
					   _tfnsconcurrentbindlimit );

		addEntryField( panel,
					   gbc,
					   _lfnsconcurrentoperationslimit,
					   _tfnsconcurrentoperationslimit );
		gbc.gridy++;
        gbc.gridx = 0;

		addEntryField( panel,
					   gbc,
					   _lfnsabandonedsearchcheckinterval,
					   _tfnsabandonedsearchcheckinterval);
		addEntryField( panel,
					   gbc,
					   _lfnsconnectionlife,
					   _tfnsconnectionlife );
		
		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.gridy++;
		gbc.fill =gbc.VERTICAL;
		gbc.weighty = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		panel.add(Box.createVerticalGlue(), gbc);

	}

    protected void addEntryField( JPanel panel, 
								  GridBagConstraints gbc,
								  JComponent label,
								  JComponent field, 
								  JLabel label2 ) {
		Component endGlue = STRETCH_FIELDS ? null : Box.createGlue();
		Component lastItem =
			STRETCH_FIELDS ? ((label2 != null) ? label2 : field) : endGlue;
		gbc.fill = gbc.NONE;
		gbc.weightx = 0.0;
        gbc.gridwidth = 1;
		//		gbc.gridx = 0;
		gbc.anchor = gbc.EAST;
		int space = UIFactory.getComponentSpace();
		gbc.insets = new Insets( space, space, 0, space/2 );
		panel.add( label, gbc );

		gbc.gridx++;
		gbc.anchor = gbc.WEST;
		gbc.insets = new Insets( space, 0, 0, 0 );
		if ( STRETCH_FIELDS ) {
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
		}
		gbc.gridwidth = (lastItem == field) ? gbc.REMAINDER : 1;
		panel.add( field, gbc );

		if ( label2 != null ) {
			gbc.gridx++;
			gbc.fill = gbc.NONE;
			gbc.weightx = 0.0;
			gbc.insets = new Insets( space, space/2, 0, 0 );
			gbc.gridwidth = (lastItem == label2) ? gbc.REMAINDER : 1;
			panel.add( label2, gbc );
		}

		if ( !STRETCH_FIELDS ) {
			gbc.gridx++;
			gbc.anchor = gbc.EAST;
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
			gbc.gridwidth = gbc.REMAINDER;
			panel.add( endGlue, gbc );
		}
	}

    /**
	 * Add a label and a textfield to a panel, assumed to be using
	 * GridBagLayout.
	 */
    protected void addEntryField( JPanel panel,
								  GridBagConstraints gbc,
								  JComponent label,
								  JComponent field ) {
		addEntryField( panel, gbc, label, field, null );
	}

	private void linkData() {
		//		String defDN = DEFAULT_PREFIX + _dnEntry;
		DSEntrySet entries = getDSEntrySet();

		/* nsslapd-timelimit */
		_tfnsslapd_timelimit = makeNumericalJTextField( _section,
														"nsslapd-timelimit");
		_lfnsslapd_timelimit = makeJLabel( _section,
										   "nsslapd-timelimit");
		_lfnsslapd_timelimit.setLabelFor(_tfnsslapd_timelimit);
		lns_timelimit_unit = makeJLabel( _section,
										 "nsslapd-timelimit-unit");
		lns_timelimit_unit.setLabelFor(_tfnsslapd_timelimit);

		DSEntryInteger nsTimelimitDSEntry = new DSEntryInteger( null,
												_tfnsslapd_timelimit,
												_lfnsslapd_timelimit,
												TIMELIMIT_MIN_VAL,
												TIMELIMIT_MAX_VAL,
												1 );
		entries.add( _dnEntry,
					 TIMELIMIT_ATTR,
					 nsTimelimitDSEntry );

		setComponentTable( _tfnsslapd_timelimit,
						   nsTimelimitDSEntry );

						  
		/* nsslapd-sizelimit */
		_tfnsslapd_sizelimit = makeNumericalJTextField(_section,
													   "nsslapd-sizelimit");
		_lfnsslapd_sizelimit = makeJLabel(_section,
										  "nsslapd-sizelimit");
		_lfnsslapd_sizelimit.setLabelFor(_tfnsslapd_sizelimit);
		lns_sizelimit_unit = makeJLabel(_section,
											   "nsslapd-sizelimit-unit");
		lns_sizelimit_unit.setLabelFor(_tfnsslapd_sizelimit);

		DSEntryInteger nsSizelimitDSEntry = new DSEntryInteger( null,
												_tfnsslapd_sizelimit,
												_lfnsslapd_sizelimit,
												SIZELIMIT_MIN_VAL,
												SIZELIMIT_MAX_VAL,
												1 );
		entries.add( _dnEntry,
					 SIZELIMIT_ATTR,
					 nsSizelimitDSEntry );

		setComponentTable( _tfnsslapd_sizelimit,
						   nsSizelimitDSEntry );

		
		/* nsbindconnectionslimit */
		_tfnsbindconnectionslimit = makeNumericalJTextField( _section,
									   			"nsbindconnectionslimit");
		_lfnsbindconnectionslimit = makeJLabel( _section,
												"nsbindconnectionslimit");
		_lfnsbindconnectionslimit.setLabelFor(_tfnsbindconnectionslimit);
		lnsbindconnectionslimit_unit = makeJLabel( _section,
												"nsbindconnectionslimit-unit");
		
		DSEntryInteger nsBindConnDSEntry = new DSEntryInteger( null,
												_tfnsbindconnectionslimit,
												_lfnsbindconnectionslimit,
												1,
												50,
												1 );
		entries.add( _dnEntry,
					 BINDCONNDSENTRY_ATTR,
					 nsBindConnDSEntry );

		setComponentTable( _tfnsbindconnectionslimit,
						   nsBindConnDSEntry );

		/* nsoperationconnectionslimit */
		_tfnsoperationconnectionslimit = makeNumericalJTextField( _section,
									   			"nsoperationconnectionslimit");
		_lfnsoperationconnectionslimit = makeJLabel( _section,
												"nsoperationconnectionslimit");
		_lfnsoperationconnectionslimit.setLabelFor(_tfnsoperationconnectionslimit);
		lnsoperationconnectionslimit_unit = makeJLabel( _section,
												"nsoperationconnectionslimit-unit");
		
		DSEntryInteger nsoperationconnectionslimitDSEntry = 
										new DSEntryInteger( null,
												_tfnsoperationconnectionslimit,
												_lfnsoperationconnectionslimit,
												1,
												50,
												1 );
		entries.add( _dnEntry,
					 OPCONLIMIT_ATTR,
					 nsoperationconnectionslimitDSEntry );

		setComponentTable( _tfnsoperationconnectionslimit,
						   nsoperationconnectionslimitDSEntry );		

		/* nsabandonedsearchcheckinterval */
		_tfnsabandonedsearchcheckinterval = makeNumericalJTextField( _section,
									   			"nsabandonedsearchcheckinterval");
		_lfnsabandonedsearchcheckinterval = makeJLabel( _section,
												"nsabandonedsearchcheckinterval");
		_lfnsabandonedsearchcheckinterval.setLabelFor(_tfnsabandonedsearchcheckinterval);
		lnsabandonedsearchcheckinterval_unit = makeJLabel( _section,
												"nsabandonedsearchcheckinterval-unit");
		
		DSEntryInteger nsabandonedsearchcheckintervalDSEntry = 
						   	new DSEntryInteger( null,
												_tfnsabandonedsearchcheckinterval,
												_lfnsabandonedsearchcheckinterval,
												0,
												MAX_INT_VAL,
												1 );
		entries.add( _dnEntry,
					 ABANDON_ATTR,
					 nsabandonedsearchcheckintervalDSEntry );

		setComponentTable( _tfnsabandonedsearchcheckinterval,
						   nsabandonedsearchcheckintervalDSEntry );	
	
		/* nsconcurrentbindlimit */
		_tfnsconcurrentbindlimit = makeNumericalJTextField( _section,
									   			"nsconcurrentbindlimit");
		_lfnsconcurrentbindlimit = makeJLabel( _section,
												"nsconcurrentbindlimit");
		_lfnsconcurrentbindlimit.setLabelFor(_tfnsconcurrentbindlimit);
		lnsconcurrentbindlimit_unit = makeJLabel( _section,
												"nsconcurrentbindlimit-unit");
		
		DSEntryInteger nsconcurrentbindlimitDSEntry = new DSEntryInteger( null,
												_tfnsconcurrentbindlimit,
												_lfnsconcurrentbindlimit,
												1,
												25,
												1 );
		entries.add( _dnEntry,
					 CONBINDLIMIT_ATTR,
					 nsconcurrentbindlimitDSEntry );

		setComponentTable( _tfnsconcurrentbindlimit,
						   nsconcurrentbindlimitDSEntry );		

		/* nsconcurrentoperationslimit */
		_tfnsconcurrentoperationslimit = makeNumericalJTextField( _section,
									   			"nsconcurrentoperationslimit");
		_lfnsconcurrentoperationslimit = makeJLabel( _section,
												"nsconcurrentoperationslimit");
		_lfnsconcurrentoperationslimit.setLabelFor(_tfnsconcurrentoperationslimit);
		lnsconcurrentoperationslimit_unit = makeJLabel( _section,
												"nsconcurrentoperationslimit-unit");
		
		DSEntryInteger nsconcurrentoperationslimitDSEntry = new DSEntryInteger( null,
												_tfnsconcurrentoperationslimit,
												_lfnsconcurrentoperationslimit,
												1,
												50,
												1 );
		entries.add( _dnEntry,
					 CONOPLIMIT_ATTR,
					 nsconcurrentoperationslimitDSEntry );

		setComponentTable( _tfnsconcurrentoperationslimit,
						   nsconcurrentoperationslimitDSEntry );		
		/* nsproxiedauthorization */
		_cbnsproxiedauthorization = makeJCheckBox( _section,
												   "nsproxiedauthorization",
												   true);
		
		DSEntryBoolean  nsproxiedauthorizationDSEntry = new DSEntryBoolean( "1",
												_cbnsproxiedauthorization );
		entries.add( _dnEntry,
					 PROXAUTH_ATTR,
					 nsproxiedauthorizationDSEntry );

		setComponentTable( _cbnsproxiedauthorization,
						   nsproxiedauthorizationDSEntry );

		/* nsconnectionlife */
		_tfnsconnectionlife = makeNumericalJTextField( _section,
									   			"nsconnectionlife");
		_lfnsconnectionlife = makeJLabel( _section,
												"nsconnectionlife");
		_lfnsconnectionlife.setLabelFor(_tfnsconnectionlife);
		lnsconnectionlife_unit = makeJLabel( _section,
												"nsconnectionlife-unit");
		
		DSEntryInteger nsconnectionlifeDSEntry = new DSEntryInteger( null,
												_tfnsconnectionlife,
												_lfnsconnectionlife,
												0,
												MAX_INT_VAL,
												1 );
		entries.add( _dnEntry,
					 CONLIFE_ATTR,
					 nsconnectionlifeDSEntry );

		setComponentTable( _tfnsconnectionlife,
						   nsconnectionlifeDSEntry );
	
		/* nsbindtimeout */
		_tfnsbindtimeout = makeNumericalJTextField( _section,
									   			"nsbindtimeout");
		_lfnsbindtimeout = makeJLabel( _section,
												"nsbindtimeout");
		_lfnsbindtimeout.setLabelFor(_tfnsbindtimeout);
		lnsbindtimeout_unit = makeJLabel( _section,
												"nsbindtimeout-unit");
		
		DSEntryInteger nsbindtimeoutDSEntry = new DSEntryInteger( null,
												_tfnsbindtimeout,
												_lfnsbindtimeout,
												0,
												BINDTIMEOUT_MAX_VAL,
												1 );
		entries.add( _dnEntry,
					 BINDTIMEOUT_ATTR,
					 nsbindtimeoutDSEntry );

		setComponentTable( _tfnsbindtimeout,
						   nsbindtimeoutDSEntry );

		/* nsreferralonscopedsearch */
		_cbnsreferralonscopedsearch = makeJCheckBox( _section,
									   			"nsreferralonscopedsearch",
												 true );
		
		DSEntryBoolean nsreferralonscopedsearchDSEntry = new DSEntryBoolean( "0",
												_cbnsreferralonscopedsearch );
		entries.add( _dnEntry,
					 REFONSEARCH_ATTR,
					 nsreferralonscopedsearchDSEntry );

		setComponentTable( _cbnsreferralonscopedsearch,
						   nsreferralonscopedsearchDSEntry );
		
		/* nschecklocalaci */
		_cbnschecklocalaci = makeJCheckBox( _section,
											"nschecklocalaci");
		
		DSEntryBoolean nschecklocalaciDSEntry = new DSEntryBoolean( "0",
												_cbnschecklocalaci );
		entries.add( _dnEntry,
					 CHECKLOCALACI_ATTR,
					 nschecklocalaciDSEntry );

		setComponentTable( _cbnschecklocalaci,
						   nschecklocalaciDSEntry );		

		/* nsbindretrylimit */
		_tfnsbindretrylimit = makeNumericalJTextField( _section,
									   			"nsbindretrylimit");
		_lfnsbindretrylimit = makeJLabel( _section,
												"nsbindretrylimit");
		_lfnsbindretrylimit.setLabelFor(_tfnsbindretrylimit);
		
		DSEntryInteger nsbindretrylimitDSEntry = new DSEntryInteger( null,
												_tfnsbindretrylimit,
												_lfnsbindretrylimit,
												0,
												BINDRETRY_MAX_VAL,
												1 );
		entries.add( _dnEntry,
					 BINDRETRY_ATTR,
					 nsbindretrylimitDSEntry );

		setComponentTable( _tfnsbindretrylimit,
						   nsbindretrylimitDSEntry );
		
		/* nshoplimit */
		_tfnshoplimit = makeNumericalJTextField( _section,
									   			"nshoplimit");
		_lfnshoplimit = makeJLabel( _section,
												"nshoplimit");
		_lfnshoplimit.setLabelFor(_tfnshoplimit);
		
		DSEntryInteger nshoplimitDSEntry = new DSEntryInteger( null,
												_tfnshoplimit,
												_lfnshoplimit,
												0,
												20,
												1 );
		entries.add( _dnEntry,
					 HOPLIMIT_ATTR,
					 nshoplimitDSEntry );

		setComponentTable( _tfnshoplimit,
						   nshoplimitDSEntry );		


	}


	private IDSModel			_model = null;
	private LDAPEntry			_configEntry = null;
	private String				_dnEntry;
	private JTextField			_tfnsslapd_timelimit;
	private JLabel				_lfnsslapd_timelimit;
	private JTextField			_tfnsslapd_sizelimit;
	private JLabel				_lfnsslapd_sizelimit;
	private JLabel				_lfnsbindconnectionslimit;
	private JTextField			_tfnsbindconnectionslimit;
	private JTextField			_tfnsoperationconnectionslimit;
	private JLabel				_lfnsoperationconnectionslimit;
	private JTextField			_tfnsabandonedsearchcheckinterval;
	private JLabel				_lfnsabandonedsearchcheckinterval;
	private JTextField			_tfnsconcurrentbindlimit;
	private JLabel				_lfnsconcurrentbindlimit;
	private JTextField			_tfnsconcurrentoperationslimit;
	private JLabel				_lfnsconcurrentoperationslimit;
	private JCheckBox			_cbnsproxiedauthorization;
	private JTextField			_tfnsconnectionlife;
	private JLabel				_lfnsconnectionlife;
	private JTextField			_tfnsbindtimeout;
	private JLabel				_lfnsbindtimeout;
	private JCheckBox			_cbnsreferralonscopedsearch;
	private JCheckBox			_cbnschecklocalaci;
	private JTextField			_tfnsbindretrylimit;
	private JLabel				_lfnsbindretrylimit;
	private JTextField			_tfnshoplimit;
	private JLabel				_lfnshoplimit;
	private JLabel				lns_sizelimit_unit;
	private JLabel				lns_timelimit_unit;
	private JLabel				lnsbindtimeout_unit;
	private JLabel				lnsconnectionlife_unit;
	private JLabel				lnsconcurrentoperationslimit_unit;
	private JLabel				lnsconcurrentbindlimit_unit;
	private JLabel				lnsabandonedsearchcheckinterval_unit;
	private JLabel				lnsbindconnectionslimit_unit;
	private JLabel				lnsoperationconnectionslimit_unit;

	private static final String TIMELIMIT_ATTR = "nsslapd-timelimit";
	private static final String SIZELIMIT_ATTR = "nsslapd-sizelimit";
	private static final String BINDCONNDSENTRY_ATTR = "nsbindconnectionslimit";
	private static final String OPCONLIMIT_ATTR = "nsoperationconnectionslimit";
	private static final String ABANDON_ATTR = "nsabandonedsearchcheckinterval";
	private static final String CONBINDLIMIT_ATTR = "nsconcurrentbindlimit";
	private static final String CONOPLIMIT_ATTR = "nsconcurrentoperationslimit";
	private static final String PROXAUTH_ATTR = "nsproxiedauthorization";
	private static final String CONLIFE_ATTR = "nsconnectionlife";
	private static final String BINDTIMEOUT_ATTR = "nsbindtimeout";
	private static final String REFONSEARCH_ATTR = "nsreferralonscopedsearch";
	private static final String CHECKLOCALACI_ATTR = "nschecklocalaci";
	private static final String BINDRETRY_ATTR = "nsbindretrylimit";
	private static final String HOPLIMIT_ATTR = "nshoplimit";



									  
	private static final String DEFAULT_PREFIX = "cn=default instance config,";
	private static final String _section = "chaining-default";
	private static final int MAX_INT_VAL = 2147483647;
	private static final int TIMELIMIT_MAX_VAL = MAX_INT_VAL;
	private static final int TIMELIMIT_MIN_VAL = -1;
	private static final int SIZELIMIT_MAX_VAL = MAX_INT_VAL;
	private static final int SIZELIMIT_MIN_VAL = -1;								
	private static final int BINDTIMEOUT_MAX_VAL = 3600;
	private static final int BINDRETRY_MAX_VAL = 10;
	  
    //copy from BlankPanel
    final static int DEFAULT_PADDING = 6;
    final static Insets DEFAULT_EMPTY_INSETS = new Insets(0,0,0,0);
    final static Insets BOTTOM_INSETS = new Insets(6,6,6,6);
}

