/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.LDAPExport;
import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import netscape.ldap.LDAPAttribute;
import netscape.ldap.util.DN;
/**
 * Panel for Directory Server resource page
 *
 * @author  jvergara
 * @version %I%, %G%
 * @date	 	08/03/2000
 * @see     com.netscape.admin.dirserv
 */
public class DatabaseExportPanel extends FilePanel {
    public DatabaseExportPanel(IDSModel model,
							   ISubtreeSelectionDialog dialog) {
		super(model, "export");
		_sDialog = dialog;
		_helpToken = "configuration-database-export-dbox-help";
	}

    public DatabaseExportPanel(IDSModel model,
						   String backend) {		
		this(model, backend, false);
		_helpToken = "configuration-database-export-single-dbox-help";
	}

    public DatabaseExportPanel(IDSModel model,
							   String backend,
							   boolean isReplicaExport) {
		super(model, "export", false, true);
		_globalExport = false;
		_backend = backend;
		_isReplicaExport = isReplicaExport;
		if (_isReplicaExport) {
			_helpToken = "configuration-replication-export-help";
		}
	}

	public DatabaseExportPanel(IDSModel model,
							   ISubtreeSelectionDialog dialog,
							   boolean globalExport) {
		super(model, "export");
		_sDialog = dialog;
		_helpToken = "configuration-database-export-dbox-help";
		_globalExport = globalExport;
	}

	public void init() {
		GridBagConstraints gbc = getGBC();
		_myPanel.setLayout( new GridBagLayout() );
		
		JPanel grid = new JPanel();
		grid.setLayout( new GridBagLayout() );
		
		createFileArea(grid);
		
		if (_globalExport) {
			/* We add a separator to make it clear that the radio buttons with message 'on server machine' and 'on console machine'
			   refer to the LDIF file */
			if ( !isLocal() && 
				 !_onlyRemote &&
				 !_onlyLocal) {
				createSeparator(grid);
			}
			createOptionsArea(grid);
		}
		
		gbc.fill = gbc.BOTH;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;
		gbc.weighty = 0;
		gbc.insets = new Insets(0,0,0,0);
		_myPanel.add(grid, gbc);	
		
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weighty = 1;
		_myPanel.add(Box.createGlue(), gbc);
						
		_rbRemote.setSelected(!_globalExport);
		setLocalState(_globalExport);
		getAbstractDialog().setFocusComponent(_tfExport);
		getAbstractDialog().getAccessibleContext().setAccessibleDescription(_resource.getString("export",
																								"description"));
	}

    private void createOptionsArea( JPanel grid ) {
        GridBagConstraints gbc = getGBC();
		
		gbc.insets = getComponentInsets();
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.NORTHWEST;
		gbc.weightx = 1.0;
		grid.add(optionsPanel(), gbc);	  
	}


	private JPanel optionsPanel() {
        GridBagConstraints gbc = getGBC();

		/* Subtree group */
        JPanel panel = new JPanel(new GridBagLayout());        
    	ButtonGroup group = new ButtonGroup();
        _rbWholeTree = makeJRadioButton( _section, "whole-tree", true );
    	group.add(_rbWholeTree);

		gbc.anchor = gbc.NORTHWEST;
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 0.0;
		gbc.gridwidth = 4;
        panel.add( _rbWholeTree, gbc );
        _rbSubtree = makeJRadioButton( _section, "sub-tree", false );

		gbc.gridwidth--;
    	group.add(_rbSubtree);
        panel.add( _rbSubtree, gbc );

		//subtree text field
        _subtreeText = makeJTextField( _section, "subtree" );
  		gbc.weightx = 1.0;
		gbc.gridwidth--;
		gbc.fill = gbc.HORIZONTAL;
        panel.add( _subtreeText, gbc );
		

        //browse button
        _browseButton = makeJButton( _section, "browse-subtree" );
        gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 0.0;
        panel.add( _browseButton, gbc );		

        setSubtreeState( false );

		return panel;
	}


    protected void setLocalState( boolean state ) {
		if ( isLocal() ) {
			return;
		}
		super.setLocalState( state );   
    }
    
    private void setSubtreeState( boolean state ) {
        _subtreeText.setEnabled( state );
        _browseButton.setEnabled( state );
		repaint();
    }
    
    protected void checkOkay() {
	    String path = _tfExport.getText();
		AbstractDialog dlg = getAbstractDialog();
		if ( dlg != null ) {
			boolean state = ( (path != null) &&
							  (path.length() > 0) );
			dlg.setOKButtonEnabled( state );
		} else {
		}
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if ( e.getSource().equals(_bExport) ) {
    	    String file = getFilename();
			String[] extensions = { "ldif" };
			String[] descriptions = { _resource.getString( "filefilter", "ldif-label" ) };
			String defaultPath = getDefaultPath(getModel());
			if ( (file == null) || (file.trim().length() < 1) ) {
				file = DSFileDialog.getFileName(false, extensions,
												descriptions, (Component)this, "*.ldif", defaultPath);
			} else {
				File theFile = new File(file);
				if (theFile.isAbsolute()) {
					file = DSFileDialog.getFileName(file, false, extensions,
													descriptions, (Component)this);
				} else {
					file = DSFileDialog.getFileName(false, extensions,
													descriptions, (Component)this, file, defaultPath);
				}
			}
			if (file != null)
				setFilename(file);
        } else if ( e.getSource().equals(_browseButton) ) {
			ConsoleInfo serverInfo = getModel().getServerInfo();			
			_sDialog.packAndShow();			
            if (!_sDialog.isOk()) {
                return;
            }
            _subtreeText.setText(_sDialog.getDN());
            _subtreeText.repaint(1);
        } else if ( e.getSource().equals( _rbWholeTree ) ) {
            setSubtreeState( false );
        } else if ( e.getSource().equals( _rbSubtree ) ) {
            setSubtreeState( true );
        } else if ( e.getSource().equals( _rbLocal ) ) {
            setLocalState( true );
			validate();
			repaint();	
        } else if ( e.getSource().equals( _rbRemote ) ) {
            setLocalState( false );
			validate();
			repaint();
        } else {
			super.actionPerformed(e);
		}
    }

    public void okCallback() {
    	boolean status = false;


		boolean fileInConsole = _rbLocal.isSelected() || isLocal();	

	    String exportPath = _tfExport.getText().trim();
		/*We only test if the file name is not empty*/
		if (exportPath.trim().equals("")) {
			if (!validateFilename())
				return;
		}


		File file = new File( exportPath );
 		if (fileInConsole) {
			/* If no path was provided, we try first with the path of the DSFileDialog
			   and then with the default path of the console.  The user has to give the full path
			   in the case the file is not in the console's machine */
			if ( !file.isAbsolute() ) {
				if (DSFileDialog.getPath() != null) {
					exportPath = DSFileDialog.getPath() + exportPath;
				} else {
					exportPath = getDefaultPath(getModel()) + exportPath;
				}
			}
			File auxFile = new File(exportPath);			
			exportPath = auxFile.getAbsolutePath();
		}	  

		if ( isLocal() || _rbRemote.isSelected()) {
			if (fileInConsole) {
				/* Check if the file exists and show a warning message */
				file = new File(exportPath);
				if (DSUtil.fileExists(file)) {
					int response = DSUtil.showConfirmationDialog(getModel().getFrame(),
																 "confirm-delete-export-file",
																 DSUtil.inverseAbreviateString(exportPath, 30),
																 _section );
					if ( response != JOptionPane.YES_OPTION ) {
						return;
					}
				}
			}
			Hashtable attributes = new Hashtable();
			
			attributes.put(LDAPExport.FILENAME, new LDAPAttribute(LDAPExport.FILENAME, exportPath));
			attributes.put(LDAPExport.USE_ONE_FILE, new LDAPAttribute(LDAPExport.USE_ONE_FILE, "TRUE"));
			if (_isReplicaExport) {
				attributes.put(LDAPExport.EXPORT_REPLICA, new LDAPAttribute(LDAPExport.EXPORT_REPLICA, "TRUE"));
			}

			String subtree = null;
			if (_subtreeText != null) {
				subtree = _subtreeText.getText();
				if ( (subtree != null) && (subtree.length() > 0) ) {
					attributes.put(LDAPExport.INCLUDE_SUFFIX, new LDAPAttribute(LDAPExport.INCLUDE_SUFFIX, subtree));
				}						
			}
			if ((_backend != null) && (_backend.length()>0)) {
				attributes.put(LDAPExport.INSTANCE, new LDAPAttribute(LDAPExport.INSTANCE, _backend));
				/* We are going to call to the export of the server, we don't know if it has the rights to access to the file.
				   That's why we show a warning */
				DSUtil.showInformationDialog(getModel().getFrame(),
											 "LDAPMode-FilePermisionWarning",
											 "",
											 _section );		
			} else {
				String[] backends = MappingUtils.getBackendList(getModel().getServerInfo().getLDAPConnection(), MappingUtils.LDBM);
				/* We don't need to use all the backends if we only have to export a subtree */
				if ( (subtree != null) && (subtree.length() > 0) ) {
					Vector vBackends = new Vector();
					DN subtreeDN = new DN(subtree);
					/* We find the parent suffix */
					String parentSuffix = MappingUtils.getSuffixForEntry(getModel().getServerInfo().getLDAPConnection(),
																		 subtree);
					if (parentSuffix != null) {
						String[] suffixBackends = MappingUtils.getBackendsForSuffix(getModel().getServerInfo().getLDAPConnection(),
																					parentSuffix);
						if (suffixBackends != null) {
							for (int j=0; j<suffixBackends.length; j++) {
								/* Check that the backend is an LDBM backend */
								for (int k=0; k<backends.length; k++) {
									if(suffixBackends[j].equalsIgnoreCase(backends[k])) {
										if (vBackends.indexOf(suffixBackends[j].toLowerCase()) < 0) {
											vBackends.addElement(suffixBackends[j].toLowerCase());
										}
									}
								}							
							}
						}
					}
					/* We find the suffixes that are under the selected subtree */
					String[] suffixes = MappingUtils.getSuffixList(getModel().getServerInfo().getLDAPConnection(), 
																   MappingUtils.LDBM);
					if (suffixes != null) {
						for (int i=0; i< suffixes.length; i++) {
							DN suffixDN = new DN(suffixes[i]);
							if (suffixDN.isDescendantOf(subtreeDN) || subtreeDN.equals(suffixDN)) {
								String[] suffixBackends = MappingUtils.getBackendsForSuffix(getModel().getServerInfo().getLDAPConnection(),
																							suffixes[i]);
								if (suffixBackends != null) {
									for (int j=0; j<suffixBackends.length; j++) {
										/* Check that the backend is an LDBM backend */
										for (int k=0; k<backends.length; k++) {
											if(suffixBackends[j].equalsIgnoreCase(backends[k])) {
												if (vBackends.indexOf(suffixBackends[j].toLowerCase()) < 0) {
													vBackends.addElement(suffixBackends[j].toLowerCase());
												}
											}
										}							
									}
								}
							}
						}
					}
					if (vBackends.size() > 0) {
						backends = new String[vBackends.size()];
						vBackends.copyInto(backends);
					}
				}		
				if ((backends!=null) && (backends.length > 0)) {
					/* We are going to call to the export of the server, we don't know if it has the rights to access to the file.
					   That's why we show a warning */
					DSUtil.showInformationDialog(getModel().getFrame(),
												 "LDAPMode-FilePermisionWarningAndConfigNotExported",
												 "",
												 _section );
					attributes.put(LDAPExport.INSTANCE, new LDAPAttribute(LDAPExport.INSTANCE, backends));
					
				} else {
					Debug.println("DatabaseExportPanel.okCallBack(): Could not find backends");
					DSUtil.showErrorDialog( getModel().getFrame(),
											"failed", "", _section );
					return;
				}					
			}			

			/* No state to preserve */
			clearDirtyFlag();
			hideDialog();
			
			LDAPExport task = new LDAPExport(getModel(), attributes);
			return;
		}

		/* See if it has the rights (only directory manager can perform this task: is the only one that can read ALL the entries) */
		LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
		if (!DSUtil.isLocalDirectoryManager(ldc)) {
			int response = DSUtil.showConfirmationDialog(getModel().getFrame(),
										  "ldapsearchExport-needtobedirectorymanager", 
										  (String[])null,
										  _section);
			if ( response != JOptionPane.YES_OPTION ) {
				return;
			}
		}

		/* Check if the file exists and show a warning message */
		file = new File(exportPath);
		if (DSUtil.fileExists(file)) {
			int response = DSUtil.showConfirmationDialog(getModel().getFrame(),
														 "confirm-delete-export-file",
														 DSUtil.inverseAbreviateString(exportPath, 30),
														 _section );
			if ( response != JOptionPane.YES_OPTION ) {
				return;
			}
		}

		/* If it's a local file and it's not in the server, check here if it can be created.  At this point we are going to use LDAP for
		   exporting the entries one by one.  That's why we have to check if WE have the rights to write on the provided file*/

		String path = exportPath;
		if ( java.io.File.separator.equals("\\") ) {
			path = path.replace( '/', '\\' );
		}
		file = new File( path );
		Debug.println( "DatabaseExportPanel.okCallback: testing " +
					   "writability of " + path );
		/* File.canWrite() lies */
		try {
			FileOutputStream out = new FileOutputStream( file );
			byte[] dummy = new byte[1];
			dummy[0] = 0;
			out.write( dummy );
			try {
				out.close();
				file.delete();
			} catch ( IOException e ) {
			}
		} catch ( IOException e ) {
			DSUtil.showErrorDialog( getModel().getFrame(),
									"unwritable", path, _section );
			return;
		}		        		

		if ( !DSUtil.reconnect( ldc ) ) {
			DSUtil.showErrorDialog( getModel().getFrame(),
									"fetching-server-unavailable", "" );
			clearDirtyFlag();
			return;
		}

		/* No state to preserve */
		clearDirtyFlag();

		hideDialog();

		String base = null;
	    String subtree = _subtreeText.getText();
		if ( (subtree != null) && (subtree.length() > 0) )
			base = subtree;	
		String title = _resource.getString("export", "title");
		GenericProgressDialog dlg = new GenericProgressDialog(getModel().getFrame(),
															  true,
															  GenericProgressDialog.ONLY_CANCEL_BUTTON_OPTION,
															  title);
		
		DSSearchExportHelper ldapSearchExport = new DSSearchExportHelper(exportPath,
																		 getModel().getServerInfo().getLDAPConnection(),
																		 dlg,
																		 base);
		try {
			Thread th = new Thread((Runnable)ldapSearchExport);
			th.start();
			dlg.packAndShow();
		} catch ( Exception e ) {
			Debug.println("DatabaseExportPanel.okCallBack: " +
						  e );
			e.printStackTrace();	
		}	
    }

    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

	public String getTitle() {
		String title;
		if (_backend!=null) {
			String[] args = {_backend};
			title = _resource.getString("export-partition", "title", args);			
		} else {
			title = _resource.getString("export", "title");
		}
		return title;
	}   

	private boolean _globalExport = true ;
	/*This is the boolean we define to know if what we are exporting is a replica or not */
	private boolean _isReplicaExport = false;
	private JTextField _subtreeText;
    private JRadioButton _rbWholeTree;
    private JRadioButton _rbSubtree;
	private ISubtreeSelectionDialog _sDialog;
	private String _backend;
}
