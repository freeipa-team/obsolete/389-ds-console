/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.logging.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.logging.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
abstract public class LogContentPanel extends RefreshablePanel {

	public LogContentPanel( IDSModel model, String title ) {
		super( model, title, false );
	}

    /**
      * Called when the object is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
		super.select( parent, viewInstance );
 		ContainerPanel p = getParentPanel();
 		if ( p != null ) {
 			p.getOKButton().setVisible( false );
 			p.getCancelButton().setVisible( false );
 			p.updateButtons();
 		}
	}

    public void init() {
        _myPanel.setLayout(new GridBagLayout());

		GridBagConstraints gbc = getGBC();

		/* Refresh button and checkbox */
        JPanel panel = createRefreshArea();
		gbc.gridx = 0;
		int different = UIFactory.getDifferentSpace();
		gbc.insets = new Insets(0,0,0,0);
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.anchor = gbc.WEST;
		_myPanel.add( panel, gbc );

		/* Get a log viewer panel */
		_viewer = getViewer();
		/* Set our desired column widths */
		JTable table = _viewer.getTable();
		table.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		table.setRequestFocusEnabled(false);
		int[] widths = getWidths();
		Enumeration en = table.getColumnModel().getColumns();
		int i = 0;
		int width = 0;
		while( en.hasMoreElements() ) {
			TableColumn col = (TableColumn)en.nextElement();
			col.setPreferredWidth( widths[i] );
			col.setResizable( true );
			width += widths[i];
			i++;
		}
		adjustTableSize( width );

		gbc = getGBC();
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.insets = new Insets(0,0,0,0);
		_myPanel.add( _viewer, gbc );
	}

	abstract protected DSLogViewer getViewer();
    abstract int[] getWidths();

    private void adjustTableSize( int width ) {
		JTable table = _viewer.getTable();
		DSLogViewerModel model = (DSLogViewerModel)table.getModel();
		/* Make the minimum and the current size of the table fit all
		   columns */
		int rows = model.getRowCount();
		/* Rows returns 0 here, so we can't really use this height */
		rows = Math.max( rows, _rows );
		int height = table.getRowHeight() * rows + 6;
		Dimension d = new Dimension( width, height );
		/* Get the maximum displayable area */
		Insets ins = _viewer.getInsets();
		width = _viewer.getWidth() - ins.left - ins.right;
		height = _viewer.getHeight() - ins.top - ins.bottom;
		d.width = Math.max( d.width, width );
		/* In reality, we will be setting the table height equal to the
		   max available horizontal space */
		d.height = Math.max( d.height, height );
		table.setMinimumSize( d );
		/* Make sure the viewport just fits */
		d.width = width;
        table.setPreferredScrollableViewportSize(d);
	}

    private void adjustTableSize() {
		JTable table = _viewer.getTable();
		Enumeration en = table.getColumnModel().getColumns();
		int width = 0;
		while( en.hasMoreElements() ) {
			TableColumn col = (TableColumn)en.nextElement();
			width += col.getWidth();
		}
		adjustTableSize( width );
	}

    public boolean refresh() {
		getModel().setWaitCursor( true );
		boolean status = _viewer.updateContents();
		adjustTableSize();
        /* scroll to the buttom of the table */
        _viewer.scrollToEnd ();
		getModel().setWaitCursor( false );
		return status;
	}

	/**
	 * Callbacks if running in a dialog
	 */
    public void okCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
	}

    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

	private int _rows = 15;
	private DSLogViewer _viewer;
	protected static final String cgiName = "Tasks/Operation/ViewLog";
}
