/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;
import javax.swing.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	 	1/8/2000
 * @see     com.netscape.admin.dirserv
 */
public class ChainingInstancePanel extends DSTabbedPanel {

	public ChainingInstancePanel(IDSModel model, LDAPEntry dbEntry) {
		super( model );
		_authTab = new ChainingInstanceAuthPanel( model, dbEntry );
		addTab( _authTab );
		_settingsTab = new ChainingInstanceConnPanel( model, dbEntry );
		addTab( _settingsTab );

		_tabbedPane.setSelectedIndex( 0 );
	}

	private BlankPanel _settingsTab;
	private BlankPanel _authTab;
}
