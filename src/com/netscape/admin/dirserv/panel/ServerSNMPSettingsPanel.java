/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.admin.dirserv.task.SnmpCtrl;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ServerSNMPSettingsPanel extends BlankPanel {

	public ServerSNMPSettingsPanel(IDSModel model) {
		super(model, _section);
		_helpToken = "configuration-system-snmp-help";
		_refreshWhenSelect = false;
	}

	public void init() {
		if (_isInitialized) {
			return;
		}
		_isNT = DSUtil.isNT(getModel().getServerInfo());

		if (!ALWAYS_ENABLED) {
			_cbSNMPEnabled = makeJCheckBox(_section,"snmpenabled");
		}

        _tfName = makeJTextField(_section, "name");
        _lfName = makeJLabel(_section, "name");
        _lfName.setLabelFor(_tfName);

        _tfDescription = makeJTextField(_section, "description");
        _lfDescription = makeJLabel(_section, "description");
        _lfDescription.setLabelFor(_tfDescription);

        _tfOrganization = makeJTextField(_section, "organization");
        _lfOrganization = makeJLabel(_section, "organization");
        _lfOrganization.setLabelFor(_tfOrganization);

        _tfLocation = makeJTextField(_section, "location");
        _lfLocation = makeJLabel(_section, "location");
        _lfLocation.setLabelFor(_tfLocation);

        _tfContact = makeJTextField(_section, "contact");
        _lfContact = makeJLabel(_section, "contact");
        _lfContact.setLabelFor(_tfContact);
		
		// No validation of attributes, because they are not necessarily
		// there
        DSEntrySet entries = new DSEntrySet( false );
        setDSEntrySet( entries );

		if (!ALWAYS_ENABLED) {
			_enabledDSEntry = new DSEntryBoolean("0", _cbSNMPEnabled);
			entries.add(SNMPSETTINGS_DN, SNMP_ENABLED_ATTR_NAM,
						_enabledDSEntry);
			setComponentTable(_cbSNMPEnabled, _enabledDSEntry);
		}

        DSEntryText nameDSEntry = new DSEntryText("", _tfName, _lfName);
        entries.add(SNMPSETTINGS_DN, SNMP_NAME_ATTR_NAM, nameDSEntry);
        setComponentTable(_tfName, nameDSEntry);

        DSEntryText descDSEntry = new DSEntryText("", _tfDescription,
            _lfDescription);
        entries.add(SNMPSETTINGS_DN, SNMP_DESCRIPTION_ATTR_NAM, descDSEntry);
        setComponentTable(_tfDescription, descDSEntry);

        DSEntryText oDSEntry = new DSEntryText("", _tfOrganization,
											   _lfOrganization);
        entries.add(SNMPSETTINGS_DN, SNMP_ORGANIZATION_ATTR_NAM, oDSEntry);
        setComponentTable(_tfOrganization, oDSEntry);

        DSEntryText lDSEntry = new DSEntryText("", _tfLocation, _lfLocation);
        entries.add(SNMPSETTINGS_DN, SNMP_LOCATION_ATTR_NAM, lDSEntry);
        setComponentTable(_tfLocation, lDSEntry);

        DSEntryText contactDSEntry = new DSEntryText("", _tfContact,
													 _lfContact);
        entries.add(SNMPSETTINGS_DN, SNMP_CONTACT_ATTR_NAM, contactDSEntry);
        setComponentTable(_tfContact, contactDSEntry);

        JPanel grid = _myPanel;
		grid.setLayout( new GridBagLayout() );

		if (!ALWAYS_ENABLED) {
			addEntryField( grid, new JLabel(" "), _cbSNMPEnabled );
		}
		JPanel panel = new GroupPanel(_resource.getString(
														  _section, "othersettings-title" ));
        panel.setLayout( new GridBagLayout() );
        addEntryField( panel, _lfName, _tfName );
        addEntryField( panel, _lfDescription, _tfDescription );
        addEntryField( panel, _lfOrganization, _tfOrganization );
        addEntryField( panel, _lfLocation, _tfLocation );
        addEntryField( panel, _lfContact, _tfContact );
        resetGBC();
        _gbc.weightx = 1.0;
        _gbc.gridwidth = _gbc.REMAINDER;
        _gbc.fill = _gbc.HORIZONTAL;
        _gbc.insets.bottom = UIFactory.getComponentSpace();
        grid.add( panel, _gbc );

        addBottomGlue();	
        _isInitialized = true;
    }

    /**
     * Update on-screen data from Directory.
     *
     **/
    public boolean refresh () {
		boolean status = super.refresh();
		if (!ALWAYS_ENABLED) {
			_enabled = _cbSNMPEnabled.isSelected();
		} else {
			_enabled = true;
		}
		enableFields( _enabled );
		return status;
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        // normally we would call super.actionPerformed
        // first, but since we don't want to set the dirty
        // flag for these special buttons, we have to
        // skip that and call just highlightButtons for them
		getModel().setWaitCursor(true);
		if (!ALWAYS_ENABLED) {
			if (e.getSource().equals(_cbSNMPEnabled)) {
				super.actionPerformed(e);  
				boolean enable = _cbSNMPEnabled.isSelected();
				enableFields( enable );
			}
		}
        getModel().setWaitCursor(false);
    }

	/**
	 * Called when Apply is selected. After the regular processing, enable the
	 * Start and Restart buttons if parameters have been set appropriately.
	 */
    public void okCallback() {
		super.okCallback();
		if (!ALWAYS_ENABLED) {
			_enabled = _cbSNMPEnabled.isSelected();
		} else {
			_enabled = true;
		}

		if (_enabled) {
			DSUtil.showInformationDialog(getModel().getFrame(),
										 "requires-restart", (String)null );
		}
	}

    private void enableFields( boolean enable ) {
		_tfName.setEnabled( enable );
		_lfName.setEnabled( enable );
		_lfName.repaint();
		_tfDescription.setEnabled( enable );
		_lfDescription.setEnabled( enable );
		_lfDescription.repaint();
		_tfOrganization.setEnabled( enable );
		_lfOrganization.setEnabled( enable );
		_lfOrganization.repaint();
		_tfLocation.setEnabled( enable );
		_lfLocation.setEnabled( enable );
		_lfLocation.repaint();
		_tfContact.setEnabled( enable );
		_lfContact.setEnabled( enable );
		_lfContact.repaint();;
    }

	private JCheckBox	_cbSNMPEnabled;

	private JTextField _tfName;
	private JLabel  _lfName;
	private JTextField  _tfDescription;
	private JLabel  _lfDescription;
	private JTextField  _tfOrganization;
	private JLabel  _lfOrganization;

	private JTextField  _tfLocation;
	private JLabel  _lfLocation;
	private JTextField  _tfContact;
	private JLabel  _lfContact;
		
	private DSEntryBoolean _enabledDSEntry;

	private boolean _enabled = true;
	private boolean _isNT = false;
	// the enabled button doesn't actually do anything - in
	// reality, SNMP collection is always enabled
	private static final boolean ALWAYS_ENABLED = true;

	private static final String SNMPSETTINGS_DN = "cn=SNMP,cn=config";
	private static final String SNMP_ENABLED_ATTR_NAM = "nssnmpenabled";
	private static final String SNMP_NAME_ATTR_NAM = "nssnmpname";
	private static final String SNMP_DESCRIPTION_ATTR_NAM =
                                               "nssnmpdescription";
	private static final String SNMP_ORGANIZATION_ATTR_NAM =
                                               "nssnmporganization";
	private static final String SNMP_LOCATION_ATTR_NAM =
                                               "nssnmplocation";
	private static final String SNMP_CONTACT_ATTR_NAM = "nssnmpcontact";

	private static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "snmpsettings";
}
