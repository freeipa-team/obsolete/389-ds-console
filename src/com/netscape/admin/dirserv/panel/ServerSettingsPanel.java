/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.StringTokenizer;
import javax.swing.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import netscape.ldap.*;

/**
 *	Panel containing network settings for the Directory Server.
 *
 * @author  rweltman
 * @version 1.2, 03/30/00
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ServerSettingsPanel extends BlankPanel {

	public ServerSettingsPanel(IDSModel model) {
		super(model, _section);
		_helpToken = "configuration-system-settings-help";
		_refreshWhenSelect = false;
	}

	public void init() {
		if (_isInitialized) {
			return;
		}
        
        _isConfigurationDir = isConfigurationDir();
        _isSSLEnabled = isSSLEnabled();
        
        _tfPort = makeNumericalJTextField(_section, "port");
        JLabel _lPort = makeJLabel(_section, "port");
		_lPort.setLabelFor(_tfPort);

        _tfEncryptedPort = makeNumericalJTextField(_section, "encryptedport");
        
        JLabel _lEncryptedPort = makeJLabel(_section, "encryptedport");
		_lEncryptedPort.setLabelFor(_tfEncryptedPort);

        _tfReferral = makeJTextField(_section, "referral");
        _lReferral = makeJLabel(_section, "referral");
		_lReferral.setLabelFor(_tfReferral);

	    _cbTrackModifies = makeJCheckBox(_section,"trackModifies");
	    
	    _cbSchemaChecking = makeJCheckBox(_section,"schemaChecking");
	    
	    _cbIsReadOnly = makeJCheckBox(_section,"isReadOnly");
		DSEntrySet entries = getDSEntrySet();

        _portDSEntry =
          new DSEntryPort( null, _tfPort, _lPort, PORT_NUM_MIN_VAL,
						   PORT_NUM_MAX_VAL, 1, "nsserverport" ); 
        entries.add(SERVERSETTINGS_DN, SERVERSETTINGS_PORT_ATTR_NAM,
					_portDSEntry);
        setComponentTable(_tfPort, _portDSEntry);

        _encryptedPortDSEntry =
          new DSEntryPort( null, _tfEncryptedPort, _lEncryptedPort,
						   PORT_NUM_MIN_VAL,
						   PORT_NUM_MAX_VAL, 1, "nssecureserverport" );
        entries.add(SERVERSETTINGS_DN, SERVERSETTINGS_ENCRYPTED_PORT_ATTR_NAM,
					_encryptedPortDSEntry);
        setComponentTable(_tfEncryptedPort, _encryptedPortDSEntry);
         
        DSEntryText referralDSEntry =
			new ReferralText( _tfReferral, _lReferral );
        entries.add(SERVERSETTINGS_DN, REFERRAL_ATTR_NAM,
					referralDSEntry);
        setComponentTable(_tfReferral, referralDSEntry);

		DSEntryBoolean trackModsDSEntry = new DSEntryBoolean(null, _cbTrackModifies);
		entries.add(SERVERSETTINGS_DN, TRACKMODIFIES_ATTR_NAM,
		            trackModsDSEntry);
		setComponentTable(_cbTrackModifies, trackModsDSEntry);

		DSEntryBoolean schemaCheckDSEntry = new DSEntryBoolean(null, _cbSchemaChecking);
		entries.add(SERVERSETTINGS_DN, SCHEMACHECKING_ATTR_NAM,
		            schemaCheckDSEntry);
		setComponentTable(_cbSchemaChecking, schemaCheckDSEntry);

		DSEntryBoolean readOnlyDSEntry = new DSEntryBoolean(null, _cbIsReadOnly);
		entries.add(SERVERSETTINGS_DN, READ_ONLY_ATTR_NAM,
		            readOnlyDSEntry);
		setComponentTable(_cbIsReadOnly, readOnlyDSEntry);

        GridBagConstraints gbc = new GridBagConstraints();
        JPanel grid = _myPanel;
        grid.setLayout( new GridBagLayout() );

		// Network Settings group
        JPanel panel = new GroupPanel(_resource.getString(
			_section, "network-group-title" ));
		gbc.anchor = gbc.WEST;
  		gbc.insets = (Insets)getComponentInsets().clone();
		int space = UIFactory.getComponentSpace();
//		gbc.insets.left = space;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        grid.add(panel, gbc);        

        panel.setLayout( new GridBagLayout() );

		addEntryField( panel, _lPort, _tfPort );
		addEntryField( panel, _lEncryptedPort, _tfEncryptedPort );
		addEntryField( panel, _lReferral, _tfReferral );
        gbc.fill = GridBagConstraints.NONE;
  		gbc.insets = new Insets( 0, 0, 0, 0 );
		panel.add( Box.createVerticalStrut( space ), gbc );

		gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
		gbc.insets = new Insets( 10, 14, 6, 0 );
        grid.add(_cbIsReadOnly, gbc);

		gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.insets.top = 0;
        grid.add(_cbTrackModifies, gbc);
        grid.add(_cbSchemaChecking, gbc);

        addBottomGlue ();		
		_isInitialized = true;
   }

    boolean isConfigurationDir() {
        ConsoleInfo myInfo = getModel().getServerInfo();
        ConsoleInfo cdInfo = getModel().getConsoleInfo();
        int    myPort = myInfo.getPort(), configDirPort = cdInfo.getPort();
        String myHost = myInfo.getHost(), configDirHost = cdInfo.getHost();
        
        return  ((myPort == configDirPort) && myHost.equals(configDirHost));
    }

    boolean isSSLEnabled() {
        JFrame frame = UtilConsoleGlobals.getActivatedFrame(); 
        if (frame == null || ! (frame instanceof DSFramework)) {
            return false;
        }
        DSFramework fmk = (DSFramework) frame; 
        DSAdmin serverObj = fmk.getServerObject();
        try {
            return serverObj != null && 
                   serverObj.getSecurityState() == serverObj.SECURITY_ENABLE;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    boolean isConfigDirPortChanged() {
        if (!_isConfigurationDir) {
            return false;
        }

        if (_isSSLEnabled && _encryptedPortDSEntry.isChanged()) {
            return true;
        }
        
        return _portDSEntry.isChanged();
    }
    
    boolean confirmPortChange() {
        IDSModel model = ServerSettingsPanel.this.getModel();
        int option = DSUtil.showConfirmationDialog(
            model.getFrame(), "confirmPortChange", (String)null, "general");
        return option == JOptionPane.YES_OPTION;
    }
    
    public void okCallback() {

        if (isConfigDirPortChanged() && !confirmPortChange()) {
            return;
        }
        super.okCallback();                    
    }            
            
    class DSEntryPort extends DSEntryInteger {
		DSEntryPort( String model, JComponent view1, JComponent view2,
					 int minValue, int maxValue, int scaleFactor,
					 String attrName ) {
			super(model, view1, view2, minValue, maxValue, scaleFactor);
			_attrName = attrName;
			_tf = (JTextField)view1;
		}

        public void show () {
            _origPort = getModelValue ();

            super.show ();
        }

        public boolean isChanged() {
            int newPort = getModelValue ();
            return (newPort != _origPort);
        }
				
        
		public void store() {
			IDSModel model = ServerSettingsPanel.this.getModel();
			ConsoleInfo info = model.getServerInfo();
			int oldPort = info.getPort();
            int    newPort = getModelValue ();
            String val     = getModel (0);

			/* Besides saving the change in the server itself,
			   update the change in the topology server */				
			super.store();

            if (newPort == _origPort)
                return;
				
			String dn = model.getConsoleInfo().getCurrentDN();
			try {
				LDAPConnection ldc =
					model.getConsoleInfo().getLDAPConnection();
				LDAPModification mod = new LDAPModification( 
						LDAPModification.REPLACE,
						new LDAPAttribute(_attrName, val) );
				ldc.modify(dn, mod);
			} catch (LDAPException lde) {
				Debug.println( "ServerSettingsPanel." +
							   "encryptedPortDSEntry.store(): " +
							   "to " + dn + " " + lde );
				setDirty( true );
			}

            _origPort = newPort;
		}
		private String _attrName;
		private JTextField _tf;
        private int _origPort;
	}

        
        
	class ReferralText extends DSEntryText {
		ReferralText ( JTextField tf, JLabel label ) {
			super (null, tf, label);
		}
		public void show() {
			String text = "";
			if ( getModelSize() < 2 ) {
				text = getModel( 0 ).trim();
			} else {
				text = "\"";
				for( int i = 0; i < getModelSize(); i++ ) {
					text = text + getModel( i ).trim() + "\"";
					if ( i < (getModelSize() - 1) ) {
						text = text + " \"";
					}
				}
			}
			Debug.println( "ServerSettingsPanel.ReferralText.show: <" +
						   text + ">" );
			_tfReferral.setText( text );

			viewInitialized ();
		}
		protected void updateModel (){
			clearModel();
			String text = _tfReferral.getText().trim();
			if ( text.length() > 0 ) {
				Vector v = parseString( text );
				for( int i = 0; i < v.size(); i++ ) {
					setModelAt((String)v.elementAt(i), i);
				}
			} else {
//				setModelAt( text, 0 );
			}
		}

		/* Check for balanced quotes and for a reasonable LDAP URL */
		public int validate () {
			JTextField tf = (JTextField)getView(0);
			String text = tf.getText().trim();
			if ( !hasBalancedQuotes( text ) ) {
				return 1;
			}
			StringBuffer buf;
			Vector v = parseString( text.toLowerCase() );
			for( int i = 0; i < v.size(); i++ ) {
				text = (String)v.elementAt(i);
				try {
					netscape.ldap.LDAPUrl url =
						new netscape.ldap.LDAPUrl( text );
					if ( url.getPort() > PORT_NUM_MAX_VAL ) {
						return 1;
					}
				} catch ( Exception e ) {
					return 1;
				}
			}
			return 0;
		}
	}

	/* Check for balanced quotes; no escaped quotes are respected */
	private static boolean hasBalancedQuotes( String text ) {
		StringBuffer buf = new StringBuffer( text );
		int nQuotes = 0;
		for( int i = 0; i < buf.length(); i++ ) {
			if ( buf.charAt( i ) == '"' ) {
				nQuotes++;
			}
		}
		return ( (nQuotes % 2) == 0 );
	}

	private static Vector parseString( String s ) {
        Vector v = new Vector();
		if ( !hasBalancedQuotes( s ) ) {
			v.addElement( "" );
            return v;
        }
		StringTokenizer st = new StringTokenizer( s.replace( '"', ' ' ) );
		while( st.hasMoreTokens() ) {
			v.addElement( st.nextToken() );
		}
        return v;
	}

	private JTextField  _tfPort;
	
	private JLabel  _lEncryptedPort;
	private JTextField  _tfEncryptedPort;
	
	private JLabel  _lReferral;
	private JTextField  _tfReferral;

	private JCheckBox	_cbTrackModifies;

	private JCheckBox	_cbSchemaChecking;
	
	private JCheckBox	_cbIsReadOnly;
 
    private boolean _isConfigurationDir;
    private boolean _isSSLEnabled;
    private DSEntryPort _portDSEntry, _encryptedPortDSEntry;

	private static final String SERVERSETTINGS_DN = "cn=config";
	private static final String SERVERSETTINGS_ENCRYPTED_PORT_ATTR_NAM =
	                            "nsslapd-secureport";
	private static final String SERVERSETTINGS_PORT_ATTR_NAM = "nsslapd-port";
	private static final String SERVERSETTINGS_ENABLED_ATTR_NAM =
	                            "nsslapd-ntsynch";
	private static final String SERVERSETTINGS_SYNCH_SSL_ATTR_NAM =
	                            "nsslapd-NTSynch-SSL";
	private static final String SERVERSETTINGS_SYNCHPORT_ATTR_NAM =
	                            "nsslapd-ntsynch-port";
	private static final String REFERRAL_ATTR_NAM = "nsslapd-referral";
	private static final String TRACKMODIFIES_ATTR_NAM = "nsslapd-lastmod";
	private static final String SCHEMACHECKING_ATTR_NAM =
	                            "nsslapd-schemacheck";
	private static final String READ_ONLY_ATTR_NAM = "nsslapd-readonly";
	private static final int LIMIT_MIN_VAL = 0;
	private static final int LIMIT_MAX_VAL = 65535;
	private static final int PORT_NUM_MIN_VAL = 0;
	private static final int PORT_NUM_MAX_VAL = 65535;

	private ResourceSet _resource = DSUtil._resource;
    private static final String _section = "serversettings";
}
