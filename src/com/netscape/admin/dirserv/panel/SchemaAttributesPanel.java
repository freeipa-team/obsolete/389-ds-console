/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.UITools;
import com.netscape.management.nmclf.SuiTableCellRenderer;
import com.netscape.management.nmclf.SuiTableHeaderRenderer;
import netscape.ldap.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class SchemaAttributesPanel extends BlankPanel
    implements ListSelectionListener {
	public SchemaAttributesPanel( IDSModel model, String section,
								  boolean needsScrollbars ) {
		super( model, section, needsScrollbars );
		_helpToken = "configuration-schema-attr-help";
		_htSyntaxStrings = getSyntaxStrings(getModel().getSchema());
		_refreshWhenSelect = false;
	}

	public SchemaAttributesPanel(IDSModel model) {
		this( model, _section, false );
	}

    static Hashtable getSyntaxStrings(LDAPSchema sch) {
		Hashtable htSyntaxStrings = new Hashtable();				
		Enumeration syntaxes = sch.getSyntaxes();
		while (syntaxes.hasMoreElements()) {
			LDAPSyntaxSchema syntax = (LDAPSyntaxSchema)syntaxes.nextElement();
			if (syntax != null) {				
				htSyntaxStrings.put(syntax.getID(), syntax.getDescription());				
			}
		}
		return htSyntaxStrings;		
	}

	static String getSyntaxString(String id) {		
		if (_htSyntaxStrings != null  ) {
			String normalizedID = getNormalizedID(id);
			String syntaxString = (String)_htSyntaxStrings.get(normalizedID);
			if (syntaxString != null) {
				return syntaxString;
			}
		}
		return "";
	}

	// Create a hashtable of matching rule names to the AVA syntax OIDs
	static Hashtable getMatchingRuleStrings(LDAPSchema sch) {
		Hashtable htMatchingRuleStrings = new Hashtable();
		Enumeration matchingrules = sch.getMatchingRules();
		while (matchingrules.hasMoreElements()) {
			LDAPMatchingRuleSchema mr = (LDAPMatchingRuleSchema)matchingrules.nextElement();
			if (mr != null) {
				htMatchingRuleStrings.put(mr.getName(), mr.getSyntaxString());
			}
		}
		return htMatchingRuleStrings;
	}

	/**
	  * Takes an ID in format 1.2.3.4{128} and creates an ID  without the length definition.
	  * In this case the resulting ID is 1.2.3.4 
	  */
	private static String getNormalizedID(String id) {
		String normalizedId;
		int index = id.indexOf('{');
		if (index < 0) {
			normalizedId = new String(id);
		} else {
			normalizedId = id.substring(0, index);
		}
		return normalizedId;			
	}

	public void init() {
		getModel().setWaitCursor( true );

		int different = UIFactory.getDifferentSpace();
        EmptyBorder border = new EmptyBorder(getBorderInsets());
        _myPanel.setBorder(border);
        _myPanel.setLayout(new GridBagLayout());
		
		JLabel introLabel = makeJLabel(_section, "intro");
		introLabel.setLabelFor(this);
        resetGBC();
        _gbc.gridwidth = _gbc.REMAINDER;
		_gbc.insets = new Insets( 0, 0, different, 0 );
		_myPanel.add( introLabel, _gbc );

		JComponent splitPane = createAttributeListArea((Container)_myPanel);
        _gbc.weightx = 1.0;
        _gbc.weighty = 1.0;
        _gbc.fill = _gbc.BOTH;
		_myPanel.add( splitPane, _gbc ); 

		JComponent p = createButtonsPanel();
		_gbc.weightx = 1.0;
        _gbc.weighty = 0.0;
		_gbc.fill = _gbc.HORIZONTAL;
		_gbc.insets.top = different;
        _myPanel.add(p, _gbc);

//		populateTables();

		getModel().setWaitCursor( false );
	}

    protected void setColumnWidths( JTable table, int[] widths ) {
		if (table == null)
			return;

		Enumeration en = table.getColumnModel().getColumns();
		int i = 0;
		while( en.hasMoreElements() ) {
			TableColumn col = (TableColumn)en.nextElement();
			col.setWidth( widths[i] );
			col.setResizable( true );
			if (i == MULTI_INDEX) {
				TableModel tm = table.getModel();
				if (tm.getColumnClass(i) == Boolean.TYPE) {
					col.setMinWidth(widths[i]);
					/* Center-align the checkbox header */
					col.setHeaderRenderer( new CenterAlignedHeaderRenderer() );
					col.setCellRenderer( new CheckBoxTableCellRenderer() );
				}
			}
			i++;
		}
	}

    protected int getTotalColumnWidth( JTable table ) {
		if (table == null)
			return 0;

		Enumeration en = table.getColumnModel().getColumns();
		int width = 0;
		while( en.hasMoreElements() ) {
			TableColumn col = (TableColumn)en.nextElement();
			width += col.getWidth();
		}
		return width;
	}

    private Dimension getOptimalDimension( JTable table,
										   Container container ) {
		if (table == null)
			return new Dimension();

		/* What is the initial total width of all columns? */
		int width = getTotalColumnWidth( table );
		/* Make the minimum and the current size of the table fit all
		   columns */
        Dimension d =
			new Dimension( width,
						   table.getRowHeight() * table.getRowCount() + 8 );
		Insets ins = container.getInsets();
		width = ((JComponent)container).getWidth() - ins.left - ins.right;
//		Debug.println( "Table: " + d + ", container: " + width );
		d.width = Math.max( d.width, width );
		return d;
	}

    private void setupTable( JTable table,
							 Container container,
							 Dimension dim ) {
		table.setBackground( Color.white );
		table.setRowHeight( 16 );
		Dimension d = dim;
		if( d == null )
			d = getOptimalDimension( table, container );
		table.setMinimumSize( d );
		table.setSize( d );
        table.setPreferredScrollableViewportSize(d);
		table.getTableHeader().setReorderingAllowed(false);
		table.setRowSelectionAllowed(false);
		table.setColumnSelectionAllowed(false);
		table.setAutoResizeMode( table.AUTO_RESIZE_NEXT_COLUMN );
		table.setRequestFocusEnabled(false);
//		table.setAutoResizeMode( table.AUTO_RESIZE_LAST_COLUMN );
//		table.setAutoResizeMode( table.AUTO_RESIZE_OFF );
	}

    private void setupTable( JTable table,
							 Container container ) {
		setupTable( table, container, null );
	}

	protected JScrollPane addTableInScrollPane( JTable table,
										 Container container,
										 Dimension dim ) {
		JScrollPane scrollpane = new JScrollPane( table );
		scrollpane.setBackground( Color.white );
		scrollpane.setBorder( UITools.createLoweredBorder() );
        resetGBC();
		_gbc.anchor = GridBagConstraints.WEST;
        _gbc.gridwidth = _gbc.REMAINDER;
        _gbc.weighty = 1.0;
        _gbc.fill = _gbc.BOTH;
		_gbc.insets = new Insets( 0, 0, 0, 0 );
        container.add( scrollpane, _gbc );
		setupTable( table, container, dim );

        return scrollpane;
	}

	protected JScrollPane addTableInScrollPane( JTable table,
												Container container ) {
		return (addTableInScrollPane( table, container, null ));
	}

	protected JComponent createAttributeListArea(Container myContainer) {
		String dummy = new String(); // for getClass()
        Object[][] colHeader = {
			{DSUtil._resource.getString(_section, "namecolumn-label"), dummy.getClass()},
			{DSUtil._resource.getString(_section, "oidcolumn-label"), dummy.getClass()},
			{DSUtil._resource.getString(_section, "syntaxcolumn-label"), dummy.getClass()},
			{DSUtil._resource.getString(_section, "multicolumn-label"), Boolean.TYPE},
			{DSUtil._resource.getString(_section, "mrequality-label"), dummy.getClass()},
			{DSUtil._resource.getString(_section, "mrordering-label"), dummy.getClass()},
			{DSUtil._resource.getString(_section, "mrsubstring-label"), dummy.getClass()}
		};

        /* create standard attributes panel */
        JLabel label = makeJLabel( _section, "standard" );
        JPanel stdPanel = new JPanel (new GridBagLayout ());
		stdPanel.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );
        resetGBC();
        _gbc.gridwidth = _gbc.REMAINDER;
		_gbc.insets = new Insets( 0, 0, 0, 0 );
		stdPanel.add( label, _gbc );
        _stdTableModel = new AttrTableModel( colHeader );
        _userDefTableModel = new AttrTableModel( colHeader );
		populateTables();

	    _stdTable = new AttrTable( _stdTableModel );
		label.setLabelFor(_stdTable);
		JScrollPane stdScrollPane =
			addTableInScrollPane( _stdTable, stdPanel );

        /* create user defined attribute table */
		label = makeJLabel( _section, "userdefined" );
        JPanel userPanel = new JPanel (new GridBagLayout ());
		userPanel.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );
        resetGBC();
        _gbc.gridwidth = _gbc.REMAINDER;
		_gbc.insets = new Insets( 0, 0, 0, 0 );
		userPanel.add( label, _gbc );

	    _userDefTable = new AttrTable( _userDefTableModel );
		label.setLabelFor(_userDefTable);
		JScrollPane userScrollPane =
			addTableInScrollPane( _userDefTable, userPanel );
		_userDefTable.setRowSelectionAllowed( true );
		_userDefTable.getSelectionModel().addListSelectionListener(this);
        /* create splitter pane and add tables to it */
		/* make it final so we can use the invokeLater thing */
        final JSplitPane splitPane = new JSplitPane (JSplitPane.VERTICAL_SPLIT,
                                               stdPanel, userPanel);
		splitPane.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );

        // Set the initial location and size of the divider
		splitPane.setDividerLocation(0.5);
		splitPane.setDividerSize(10);

		// set divider location doesn't work until after the pane is displayed		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				splitPane.setDividerLocation(0.5);
			}
		});	   
		
		return splitPane;
	}

    /**
      * Called when the object is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
        Debug.println( "SchemaAttributesPanel.select" );
        if (!_isInitialized) { // first time called
            init();
            _isInitialized = true;
			clearDirtyFlag();
        }
    }

    public boolean refresh () {
        Debug.println( "SchemaAttributesPanel.refresh" );
		getModel().setWaitCursor( true );
		try {
			LDAPSchema schema = new LDAPSchema();
			LDAPConnection ld = getModel().getServerInfo().getLDAPConnection();
			if ( ld != null ) {
				schema.fetchSchema( ld );
				getModel().setSchema( schema );
			}
		} catch ( LDAPException e ) {
			Debug.println( "SchemaAttributesPanel.refresh: " + e );
			return false;
		}
		populateTables();
		getModel().setWaitCursor( false );
		return true;
	}

    protected void populateTables() {
		_userDefTableModel.removeAllRows();
		_stdTableModel.removeAllRows();
		LDAPSchema sch = getModel().getSchema();
		if (sch == null)
		    return;
		synchronized (sch) {
            for (Enumeration e = sch.getAttributes(); e.hasMoreElements();) {
                LDAPAttributeSchema las = (LDAPAttributeSchema)e.nextElement();
				addAttributeRow( las );
            }
        }
		setColumnWidths( _stdTable, _widths );
		setColumnWidths( _userDefTable, _widths );
		if (_stdTable != null)
			_stdTable.repaint();
		if (_userDefTable != null)
			_userDefTable.repaint();
	}

    private void addAttributeRow( LDAPAttributeSchema las ) {
		if (!DSUtil.isStandardSchema(las)) {
			_userDefTableModel.addRow(las);
		} else {
			_stdTableModel.addRow(las);
		}
	}

	public void valueChanged(ListSelectionEvent e) {
		//Debug.println("SchemaAttributesPanel.valueChanged(): e = " + e);
        // something was selected; enable the buttons
		updateButtons();
	}

	private void updateButtons() {
		if ( _editButton == null )
			return;
		boolean enable = ( _userDefTable.getSelectedRowCount() > 0 );
        _editButton.setEnabled( _userDefTable.getSelectedRowCount() == 1 );
        _deleteButton.setEnabled( enable );
    }

	protected JComponent createButtonsPanel() {
        _createButton = makeJButton(_section, "create");
        _editButton = makeJButton(_section, "edit");
        _deleteButton = makeJButton(_section, "delete");
        _helpButton = makeJButton("general", "Help");

		JButton[] buttons = { _createButton, _editButton,
							  _deleteButton, _helpButton };
		JPanel p =  UIFactory.makeJButtonPanel( buttons, true );

        // no items selected by default
        _editButton.setEnabled(false);
        _deleteButton.setEnabled(false);

        return p;
    }

    private boolean removeRowByName( String name ) {
		return _userDefTable.removeRowByName(name);
	}

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        AttributeDialog dialog = null;
		String name = "";
		LDAPSchema sch = getModel().getSchema();
		LDAPAttributeSchema las = null;

        if (source.equals(_createButton)) {
            dialog = new AttributeDialog( getModel() );
			dialog.setTitle( DSUtil._resource.getString(
				_section, "new-title") );
        } else if (source.equals(_editButton)) {
			int row = _userDefTable.getSelectedRow();
			if ( row < 0 ) {
				/* Should never happen, since the Edit button would be disabled
				   otherwise, but this is safe */
				return;
			}
			name = (String)_userDefTable.getValueAt( row, 0 );
			las = sch.getAttribute( name );
			if ( las == null ) {
				Debug.println( "SchemaAttributePanel.actionPerformed: " +
							   "No schema def for " + name );
				return;
			}
			dialog = new AttributeDialog( getModel(), las );
			dialog.setTitle( DSUtil._resource.getString(
				_section, "edit-title") );
        } else if (source.equals(_helpButton)) {
			helpCallback();
        } else if (source.equals(_deleteButton)) {
			int row = _userDefTable.getSelectedRow();
			if ( row < 0 ) {
				/* Should never happen, since the Delete button would be disabled
				   otherwise, but this is safe */
				return;
			}
			/* Have to track all selected attributes by name */
			int[] rows = _userDefTable.getSelectedRows();
			String[] rowNames = new String[rows.length];
			LDAPAttributeSchema[] lases = new LDAPAttributeSchema[rows.length];
			for( int i = 0; i < rows.length; i++ ) {
				rowNames[i] = (String)_userDefTable.getValueAt( rows[i], 0 );
				lases[i] = sch.getAttribute( rowNames[i] );
				if ( lases[i] == null ) {
					Debug.println( "SchemaAttributePanel.actionPerformed: " +
								   "No schema def for " + rowNames[i] );
					return;
				}
			}
			boolean confirm = requiresConfirmation(
				GlobalConstants.PREFERENCES_CONFIRM_DELETE_ATTRIBUTE );
			if ( confirm ) {
				String str = rowNames[0];
				for( int i = 1; i < rowNames.length; i++ ) {
					str += " " + rowNames[i];
				}
				str = "'"+DSUtil.abreviateString(str, 20)+"'";
				int response = DSUtil.showConfirmationDialog(
					getModel().getFrame(),
					"confirm-delete",
					str,
					_section );
				if ( response != JOptionPane.YES_OPTION ) {
					updateButtons();
					return;
				}
			}
			LDAPConnection ldc =
				getModel().getServerInfo().getLDAPConnection();
			if ( ldc == null ) {
				Debug.println( "SchemaAttributePanel.actionPerformed: " +
							   "No LDAPConnection" );				
				return;
			}
			boolean done = false;
			boolean status = false;
			while (!done) {
				try {
					for( int i = 0; i < rowNames.length; i++ ) {
						las = lases[i];
						if ( las != null ) {
							Debug.println( "SchemaAttributesPanel." +
										   "actionPerformed: delete " +
										   las.getValue() );
							las.remove( ldc );
							lases[i] = null;
						}
					}
					done = true;
					status = true;
				} catch (LDAPException lde) {
					Debug.println(
						"SchemaAttributesPanel.actionPerformed: " +
						lde + " attr = " + las.getValue());
					if (lde.getLDAPResultCode() ==
						LDAPException.INSUFFICIENT_ACCESS_RIGHTS) {
						// display a message
						DSUtil.showPermissionDialog( getModel().getFrame(),
													 ldc );
						if (!getModel().getNewAuthentication(false)) {
							done = true; // error, just punt
						}
					} else { // some other LDAP error, bad network
						     //connection, etc.
						done = true;
						DSUtil.showErrorDialog( getModel().getFrame(),
												"failed-delete",
												lde.toString(),
												_section );
					}
				}
			}

			getModel().notifyAuthChangeListeners();

			if ( status ) {
				String names = rowNames[0];
				removeRowByName( rowNames[0] );
				for( int i = 1; i < rows.length; i++ ) {
					names += "," + rowNames[i];
					removeRowByName( rowNames[i] );
				}
				names  = "'"+DSUtil.abreviateString(names, 20)+"'";
				
				DSUtil.showInformationDialog( getModel().getFrame(),
											  "successful-delete",
											  names,
											  _section );
				// Force refetch of schema on next access
				getModel().setSchema( null );
			}
        }

        if ( dialog != null ) {
            try {	
				dialog.packAndShow();							
				
				las = dialog.getAttribute();
				if ( las != null ) {
					if ( source.equals(_editButton) )
						removeRowByName( name );
					addAttributeRow( las );					
				}
            } catch (java.lang.NullPointerException ne) {
                // do nothing
            }
        }
		updateButtons();
    }

	class AttrTable extends JTable implements MouseListener {
		AttrTable(TableModel tm) {
			super(tm);
//						  " column 0 = " + getColumnModel().getColumn(0));
			addMouseListener(this);
			createDefaultColumnsFromModel();			
			tm.addTableModelListener(this);			
		}		

		public void add(LDAPAttributeSchema las) {
			AttrTableModel tm = (AttrTableModel)getModel();
			tm.addRow(las);
		}

		boolean removeRowByName(String name) {
			AttrTableModel tm = (AttrTableModel)getModel();
			for (int row = 0; row < tm.getRowCount(); row++) {
				String cell = (String)getValueAt(row, 0);
				if (name.equalsIgnoreCase(cell)) {
					tm.removeRow(row);
					return true;
				}
			}
			return false;
		}

		/**
		 * Process mouse events, in order to pop up context menu
		 */
		void singleClicked( MouseEvent e ) {		
//			Debug.println( "AttrTable.singleClicked " + e );
//			int index = _entryList.locationToIndex(e.getPoint());
//			if ( index >= 0 ) {
//				int mods = e.getModifiers();
//				if ( (mods & e.BUTTON3_MASK) != 0 ) {
//					popup( e );
//				}
//			}
		}

		void doubleClicked( MouseEvent e ) {			
			int row = rowAtPoint(e.getPoint());
			Debug.println("AttrTable.doubleClicked on row " + row);
			if ( row >= 0 ) {
				String attrName = (String)getValueAt(row, 0);
				IDSModel model = SchemaAttributesPanel.this.getModel();
				LDAPSchema sch = model.getSchema();
				LDAPAttributeSchema las = sch.getAttribute(attrName);
				if (DSUtil.isStandardSchema(las)) 				
					return; // disallow edit of standard schema				
				
				AttributeDialog dialog = new AttributeDialog(model, las);
				dialog.setTitle(DSUtil._resource.getString(
														   _section, "edit-title"));
                dialog.show();
				synchronized (this) {
					LDAPAttributeSchema newLas = dialog.getAttribute();
					if (newLas != null) {
						AttrTableModel atm = (AttrTableModel)getModel();
						removeRowByName(las.getName());
						add(newLas);
					}
				}
			}
		}

		public void tableChanged(TableModelEvent e) {
			Debug.println(8, "AttrTable.tableChanged " + e );
			repaint();
			super.tableChanged(e);
		}

		private void popup( MouseEvent e ) {
			Debug.println( "AttrTable.popup " + e );
//			if ( _contextMenu != null ) {
//				_contextMenu.show( _entryList, e.getX(), e.getY() );
//				e.consume();
//			}
		}

		public void processMouseEvent(MouseEvent e)	{
			super.processMouseEvent( e );
		}

		/**
		 * Process mouse clicks, in order to distinguish between single-clicks
		 * and double-clicks; dispatch accordingly.
		 */
		public void mouseClicked(MouseEvent e) {
			/* Don't make a left-click plus a right-click be a double */
			int mods = e.getModifiers();
			if ( (e.getClickCount() == 2) && (mods & e.BUTTON3_MASK) == 0 ) {
				doubleClicked( e );
			} else {
				singleClicked( e );
			}
		}

		public void mousePressed(MouseEvent e) {
		}

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
			//		_entryList.setToolTipText( null );
		}

		synchronized public String getToolTipText(MouseEvent e) {
			String ttip = "";
			if (e == null)
				return ttip;

			Point pt = e.getPoint();
			if (pt == null)
				return ttip;

			int row = rowAtPoint(pt);
			if (row >= 0 && row < getRowCount()) {
				String attrName = (String)getValueAt(row, 0);
				Debug.println(8, "AttrTable.getToolTipText: row = " + row +
							  " attrName = " + attrName);
				IDSModel model = SchemaAttributesPanel.this.getModel();
				Debug.println(8, "AttrTable.getToolTipText: model = " + model);
				LDAPSchema sch = model.getSchema();
				Debug.println(8, "AttrTable.getToolTipText: schema = " +
							  (sch == null));
				LDAPAttributeSchema las = sch.getAttribute(attrName);
				Debug.println(8, "AttrTable.getToolTipText: las = " + las);
				if (las != null) {
					ttip = attrName;
					String aliases = DSSchemaHelper.arrayToString(las.getAliases());
					if (aliases != null && aliases.length() > 0) {
						ttip += ", " + aliases;
					}
					ttip += ": " + las.getDescription();
				}
			}
			return ttip;
		}
	}

    public static void main( String[] args ) {
		Debug.setTrace( true );
		try { 
			UIManager.setLookAndFeel(
				"com.netscape.management.nmclf.SuiLookAndFeel" ); 
		} catch (Exception e) { 
			System.err.println("Cannot load nmc look and feel."); 
		} 
		DefaultResourceModel model = new DefaultResourceModel();
		try {
			int port = Integer.parseInt( args[1] );
			LDAPConnection ldc = new LDAPConnection();
			ldc.connect( 3, args[0], port, args[2], args[3] );
			LDAPSchema schema = new LDAPSchema();
			schema.fetchSchema( ldc );
			model.setSchema( schema );
		} catch ( Exception e ) {
			System.err.println( e );
			System.err.println( "Usage: SchemaAttributesPanel HOST PORT " +
								"AUTHDN AUTHPASSWORD" );
			System.exit( 1 );
		}
		
		BlankPanel child = new SchemaAttributesPanel( model ) {
			public void okCallback() {
				hideDialog();
				System.exit( 0 );
			}
		};
		child.init();
		ContainerPanel container = new ContainerPanel( model, child, false );
		SimpleDialog dlg = createDialog( container, child );
//		dlg.setSize(350,300);
		dlg.show();
    }

    private JButton _createButton = null;
    private JButton _deleteButton = null;
    private JButton _editButton = null;
    private JButton _helpButton = null;
    private AttrTable _userDefTable = null;
    private AttrTable _stdTable = null;
    private AttrTableModel _userDefTableModel = null;
    private AttrTableModel _stdTableModel = null;

	private static int _widths[] = { 130, 160, 115, 40, 40, 40, 40 };
	/* Index of multi/single checkbox column */
	private final static int MULTI_INDEX = 3;
	private static Hashtable _htSyntaxStrings = null;
    // name of panel in resource file
	static final private String _section = "schemaattributes"; 
}

/* AttrTableModel is not an internal class just because some backrev
   compilers run out of memory when there are a few of them around */
class AttrTableModel extends AbstractTableModel {
	AttrTableModel(Object[][] headers) {
		_headers = headers;
		for( int i = getColumnCount(); i > 0; i-- ) {
			_tableColumns.addElement( new Vector() );
		}
	}
    public int getColumnCount() {
		return _headers.length;
	}
	public int getRowCount() {
		Vector v = (Vector)_tableColumns.elementAt( 0 );
		return v.size();
	}
	public Object getValueAt(int row, int col) {
		Vector v = (Vector)_tableColumns.elementAt( col );
		Object obj = v.elementAt( row );
		return obj;
	}
	public void setValueAt(Object obj, int row, int col) {
		Vector v = (Vector)_tableColumns.elementAt( col );
		if (row < 0) {
			v.addElement(obj);
			fireTableRowsInserted(v.size(), v.size());
		} else {
			v.setElementAt(obj, row);
		}
	}
	void addRow(LDAPAttributeSchema las) {
		Object row[] = new Object[7];
		String name = las.getName();
		String [] mr_eq_vals = las.getQualifier("EQUALITY");
		String [] mr_ord_vals = las.getQualifier("ORDERING");
		String [] mr_sub_vals = las.getQualifier("SUBSTR");

//		can't have alias list as first column in table because the code uses
//		that value to look up the attribute schema
//		String[] aliases = las.getAliases();
//		if (aliases != null) {
//			name += ", ";
//			name += DSSchemaHelper.arrayToString(aliases);
//		}
		row[0] = name;
		row[1] = las.getID();
		row[2] = SchemaAttributesPanel.getSyntaxString(las.getSyntaxString());
		row[3] = new Boolean( !las.isSingleValued() );

		// Fill in any matching rules that are set
		if ((mr_eq_vals != null) && (mr_eq_vals.length > 0)) {
			row[4] = mr_eq_vals[0];
		}
		if ((mr_ord_vals != null) && (mr_ord_vals.length > 0)) {
			row[5] = mr_ord_vals[0];
		}
		if ((mr_sub_vals != null) && (mr_sub_vals.length > 0)) {
			row[6] = mr_sub_vals[0];
		}

		addRow(row);
	}
	void addRow( Object[] values ) {
		int row = 0;
		/* Insert alphabetically */
		Vector v = (Vector)_tableColumns.elementAt( 0 );
		String s = (String)values[0];
		while( (row < v.size()) &&
			   (s.compareTo( (String)v.elementAt(row) ) > 0)  )
			row++;

		for( int i = 0; i < getColumnCount(); i++ ) {
			v = (Vector)_tableColumns.elementAt( i );
			v.insertElementAt( values[i], row );
		}
		fireTableRowsInserted( row, row );
	}
	void removeRow( int row ) {
		for( int col = 0; col < _tableColumns.size(); col++ ) {
			Vector v = (Vector)_tableColumns.elementAt( col );
			v.removeElementAt( row );
		}
		fireTableRowsDeleted( row, row );
	}
	void removeAllRows() {
		int last = getRowCount() - 1;
		if ( last >= 0 ) {
			for( int col = 0; col < _tableColumns.size(); col++ ) {
				Vector v = (Vector)_tableColumns.elementAt( col );
				v.removeAllElements();
			}
			fireTableRowsDeleted( 0, last );
		}
	}
	public String getColumnName(int column) {
		return (String)_headers[column][0];
	}

	public Class getColumnClass(int column) {
		Object o = null;
		if (_tableColumns != null && _tableColumns.size() > 0 &&
			_tableColumns.elementAt(column) != null) {
			Vector v = (Vector)_tableColumns.elementAt(column);
			if (v.size() > 0)
				o = getValueAt(0, column);
		}
		Class c = null;
		if (o == null)
			c = (Class)_headers[column][1];
		else
			c = o.getClass();
		if (column == 0 || column == 3) {
			Debug.println(8, "AttrTableModel.getColumnClass: this = " + this +
						  " column " + column + " has class " +
						  c + " object " + o + " numcolumns = " +
						  getColumnCount());
		}
		return c;
	}

	// there is an array for each column.  Each sub array contains two elements:
	// the first is the string label for the column header.  The second is the type
	// of data represented by the column and returned by getColumnClass()
	protected Object[][] _headers;
	private Vector _tableColumns  = new Vector();
}
