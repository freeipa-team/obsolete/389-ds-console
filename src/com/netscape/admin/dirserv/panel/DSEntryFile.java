/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.io.File;
import java.awt.Color;
import java.util.Vector;
import java.util.Enumeration;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.AbstractButton;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.util.*;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server
 *
 * @version
 * @author
 */
class DSEntryFile extends DSEntryText {
    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle text entry fields which accept file and directory names.  One
     * model value, one widget.  This class verifies that the path and/or
     * file name is valid.
     *
     * @param model the initial value, if any, or just null
     * @param view the JTextField used as the text entry field
     * @param isFile sets whether the path should be a file or directory (used when validating)
     */
    DSEntryFile(
        String model,
        JTextField view,
        boolean isFile
    ) {
        super(model, view);
        this.isFile = isFile;
    }

    DSEntryFile(
        String model,
        JTextField view
    ) {
        super(model, view);
        isFile = true;
    }

    DSEntryFile(
        String model,
        JComponent view1, JComponent view2) {
        super(model, view1, view2);
        this.isFile = true;
    }
  
    DSEntryFile(
        String model,
        JComponent view1, JComponent view2, boolean isFile) {
        super(model, view1, view2);
        this.isFile = isFile;
    }

    DSEntryFile(
        String model,
        JComponent view1, JComponent view2, boolean isFile, boolean local) {
        super(model, view1, view2);
        this.isFile = isFile;
        this.isLocal = local;
    }

	DSEntryFile(String model,
				JTextField view1, 
				boolean isFile, 
				boolean local) {
		super(model, view1);
        this.isFile = isFile;
        this.isLocal = local;
	}

	/**
	  * Change a DOS-style file name to use backslash instead of forward
	  * slash
	  */
	private String uncanonicalize( String filename ) {
		if ( (filename != null) && (filename.length() >= 2) &&
			 (filename.charAt(1) == ':') ) {
			filename = filename.replace( '/', '\\' );
		}
		return filename;
	}

    /**
     * several types of validation are done; first, the text field is checked
     * to see if the user has typed in anything.  Next, the file name is checked
     * to see if it is a directory and returns
     * true if isFile = false.  Then it varifies that  the parent of the file
     * is a directory.  If any of these conditions fail,
     * false is returned, otherwise, true is returned.
     *
     * @return false if the text field does not contain a valid integer
     */
	public boolean dsValidate() {
		
		JTextField tf = (JTextField)getView(0);

		String name = uncanonicalize( tf.getText() );
        File file = new File(name);
        String err = null; // error number
        String args[] = null; // additional arguments for error message
        
        if (!isLocal)
            return true;

        if (file.isDirectory() && isFile) {
            // We are only looking for non-directories
            err = "directoryButNeedFile";
            args = new String[1];
            args[0] = name;
            Debug.println("You need to enter a file name as well as " +
							   "a dir: " + file.getPath()); 
        } else if (file.isFile() && !isFile) {
            // Want a directory but got a file 
            err = "fileButNeedDirectory";
            args = new String[1];
            args[0] = name;
            Debug.println("\nYou need to enter a directory name: " +
							   file.getPath()); 
        } else {
    	    String parentName = file.getParent();
			File parent = null;
			if ( parentName != null ) {
				parent = new File( parentName );
			} else {
				Debug.println( "DSEntryFile.dsValidate: null parent to " +
									 name );
			}
	        if ( (parent == null) || !parent.isDirectory() ) {
	            err = "notValidDirectory";
	            args = new String[1];
	            args[0] = name;
	            Debug.println("Not a valid directory: " + name);
	        }
	    }
	    
        if (err != null) { // popup an error dialog
            reportError(err, args, tf);
        }
        
		return (err == null); // if err was set, there must have been an error
	}
    
	public int validate() {
		JTextField tf = (JTextField)getView(0);

		String name = uncanonicalize( tf.getText() );
        File file = new File(name);
        
        if (isLocal) {
            if ((file.isDirectory() && isFile) || 
                (file.isFile() && !isFile)) {
                 return 1;
            } else {
    	        String parentName = file.getParent();
		        File parent = null;
		        if ( parentName != null )
		        	parent = new File( parentName );
	            if ( (parent == null) || !parent.isDirectory() ) {
                     return 1;
	            }
	        }
        }

	    return 0;    
	}

    // is a directory when false
    private boolean isFile = true;
    private boolean isLocal = true;
}
