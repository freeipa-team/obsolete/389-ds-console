/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
abstract public class RefreshablePanel extends BlankPanel {

	public RefreshablePanel( IDSModel model, String title,
							 boolean scroll ) {
		super( model, title, scroll );
	}

	/**
	 * Create the refresh panel.
	 *
	 * @param myContainer The parent component.
	 */
    protected JPanel createRefreshArea() {
		JPanel panel = new JPanel();
		panel.setLayout( new FlowLayout(FlowLayout.LEFT) );
        _refreshButton = makeJButton( _section, "refresh" );
	    _refreshCheckbox = makeJCheckBox( _section, "continuous",
										  false );
		panel.add( _refreshButton );
		panel.add( Box.createHorizontalStrut(UIFactory.getComponentSpace()) );
		panel.add( _refreshCheckbox );
		panel.add( Box.createGlue() );

		return panel;
	}


    protected void startUpdater() {
		_doRefresh = true;
		if ( _updater == null ) {
			_updater = new Thread() {
				public void run() {
//					Debug.println( "Monitor thread starting" );
					while(_doRefresh ) {
						if (isShowing()) {
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									refresh();
								}
							});
						}
						try {
							sleep( 5000 );
						} catch ( InterruptedException ex ) {
						}
					}
//					Debug.println( "Monitor thread exiting" );
					_updater = null;
				}
			};
			_updater.start();
		}
	}

    protected void stopUpdater() {
		_doRefresh = false;
	}

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
		if ( source == _refreshButton ) {
			refresh();
        }   
		else if ( source == _refreshCheckbox ) {
			if ( _refreshCheckbox.isSelected() ) {
				startUpdater();
			} else {
				stopUpdater();
			}
        }
    }

    // name of panel in resource file
    static final private String _section = "monitorserver";
	static final ResourceSet _resource = DSUtil._resource;

	protected JButton _refreshButton;
	protected JCheckBox _refreshCheckbox;
	protected boolean _doRefresh = false;
	protected Thread _updater = null;
}
