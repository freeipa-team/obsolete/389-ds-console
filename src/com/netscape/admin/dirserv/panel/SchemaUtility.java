/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import javax.swing.*;

/**
 * Tools for schema
 * @author Christine Ho
 * @version %I%, %G%
 * @date                12/10/97
 */
public class SchemaUtility {

    /**
     * Sort an array of strings in ascending order
     */
    static void SortStrings(String array[], int low, int high) {
        if (low >= high)
            return;
        String pivot = array[low];
        int slow = low-1, shigh = high+1;
        while (true) {
            do
                shigh--;
            while (greater(array[shigh], pivot));
            do
                slow++;
            while (greater(pivot, array[slow]));

            if (slow >= shigh)
                break;

            String temp = array[slow];
            array[slow] = array[shigh];
            array[shigh] = temp;
       }

       SortStrings(array, low, shigh);
       SortStrings(array, shigh+1, high);
   }

   static void SortStrings(ListModel model, int low, int high) {
       int size = model.getSize();
       DefaultListModel listModel = (DefaultListModel)model;
       String[] strArray = new String[listModel.size()];
       listModel.copyInto(strArray);
       SortStrings(strArray, low, high);
       listModel.removeAllElements();
       for (int i=0; i<strArray.length; i++) {
           listModel.addElement(strArray[i]);
       }
   }

   /**
    * Sort an array of characters in ascending order
    */
   static void SortStrings(String a[]) {
       SortStrings(a, 0, a.length-1);
   }

   //string comparison
   private static boolean greater(String a, String b) {
       if (a.compareTo(b) <= 0)
           return false;
       return true;
   }

   /** Insert an element to the list model
    * @param list The list the object being added to
    * @param object The object being inserted
    */
   public static void InsertElement(Object list, Object object) {
       if (list instanceof DefaultListModel) {
           DefaultListModel listModel = (DefaultListModel)list;
           String[] strArray = new String[listModel.size()];
           listModel.copyInto(strArray); 
           int i=0;
           for (; i<strArray.length; i++) {
               if (greater(strArray[i], (String)object)) {
                   listModel.insertElementAt(object, i);
                       return;
               }
           }
           if (i >= strArray.length) {
               listModel.addElement(object);
           }
       } else if (list instanceof JComboBox) {
           JComboBox comboBox = (JComboBox)list;
           int size = comboBox.getItemCount();
           int i=0;
           for (; i<size; i++) {
               if (greater((String)comboBox.getItemAt(i), (String)object)) {
                   comboBox.insertItemAt(object, i);
                   return;
               }
           }
           if (i >= size) {
               comboBox.addItem(object);
           }
       }
   }
}

