/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.Vector;
import java.util.Enumeration;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.AbstractButton;

import com.netscape.admin.dirserv.*;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server
 *
 * @version
 * @author
 */
class DSEntryList extends DSEntry {
    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle list(box) entries that are localized.  The first parameter is
	 * an array of strings that are used to update the directory server and
	 * corresponds to the localized list entries.
     *
     * @param listEntries a string array of valid LDAP entries that have
	 * been localized in the view
     * @param view the JComboBox which stores localized entries that need
	 * to be replaced with standard LDAP syntax
     */
    DSEntryList(
        String[] listEntries,
        JList view
    ) {
        super((String)null, view);
		_listEntries = null; // wipes out existing values, if any
		if (listEntries != null && listEntries[0] != null) {
			_listEntries = new Vector(listEntries.length);
			for (int ii = 0; ii < listEntries.length; ++ii) {
				_listEntries.addElement(listEntries[ii]);
			}
		}
    }

    /**
     * First, get the model as an integer.  If the model is not an integer,
     * just set the value to 0.  Next, convert the value using the given
     * scale factor.  Last, update the view.
     */
    public void show() {
        // update the view
		JList lb = (JList)getView(0);
		
		int i = 0, size = getModelSize();
		String entry;
		int indices[] = new int[size];
		while ((entry = getModel(i)) != null) {
		    indices[i] = _listEntries.indexOf(entry);
		    i++;
		}
		
		
   		lb.setSelectedIndices(indices);

        viewInitialized ();
	}

	/**
	 * Since data is dynamically updated when it is modified
     * thought a call to updateModified, nothing needs ro be
     * done here
     */
	public void store (){
	}

	/**
	 * Remove all the old entries before updating the model with the
	 * currently selected items in the listbox.
	 */
	protected void updateModel() {
	    JList lb = (JList)getView(0);
	    clearModel();
	    int selected[] = lb.getSelectedIndices();
	    int ii = 0;
	    while (ii < selected.length) {
            setModelAt((String)_listEntries.elementAt(selected[ii]), ii);
            ii++;
        }
    }

    public int validate() {
        return 0;
    }

    private Vector _listEntries = null; // a Vector of Strings
}
