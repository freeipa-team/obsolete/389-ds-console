/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.Debug;
/**
 * Table renderer for checkboxes
 * Note: it attempts to set the right highlight colors, but they don't
 * ever change.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */

public class CheckBoxTableCellRenderer implements TableCellRenderer {	
	public Component getTableCellRendererComponent(
		JTable table, Object value,
		boolean isSelected, boolean hasFocus, int row, int column) {		
		if ( comp == null ) {
			comp = new JCheckBox();
			comp.setHorizontalAlignment(SwingConstants.CENTER);			
		}						
					
		if (( value != null ) && (value instanceof Boolean)) {	   
			comp.setSelected( ((Boolean)value).booleanValue() );			
		} else {
			Debug.println("CheckBoxTableCellRenderer.getTableCellRendererComponent: NOT A BOOLEAN");
		}			   

		if (isSelected && !hasFocus) {
			comp.setForeground(table.getSelectionForeground());
			comp.setBackground(table.getSelectionBackground());
		} else if (isSelected && hasFocus) {
			comp.setForeground(table.getForeground());
			comp.setBackground(table.getBackground());
		} else {
			comp.setForeground(table.getForeground());
			comp.setBackground(table.getBackground());
		}

		if( isSelected ) {
			comp.setOpaque( true );
			if (!hasFocus) {				
				comp.setBorderPainted(false);				
			} else {
				comp.setBorderPainted(true);
			}
		} else {
			comp.setOpaque( false );			
			comp.setBorderPainted(false);
		}
		return comp;
	}	
	JCheckBox comp = null;	
}

