/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.Font;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.border.CompoundBorder;
import com.netscape.management.nmclf.SuiConstants;
/**
 * GroupPanel
 *
 * A JPanel with a titled border, and a GridBagLayout.
 *
 * @version 1.0
 * @author rweltman
 **/
public class GroupPanel extends JPanel implements SuiConstants {
	
	public GroupPanel( String title ) {
		this(title, false);
	}

	public GroupPanel(String title, boolean margin) {
		super (new GridBagLayout());
		_titledBorder =  new TitledBorder( new EtchedBorder(), title );
		if (margin) {			
			CompoundBorder border = new CompoundBorder(_titledBorder, 
													   new EmptyBorder(COMPONENT_SPACE, 
																	   DIFFERENT_COMPONENT_SPACE,
																	   COMPONENT_SPACE,
																	   DIFFERENT_COMPONENT_SPACE));
			setBorder(border);
		} else {
			setBorder( _titledBorder );	
		}
		_titledBorder.setTitleColor(BlankPanel.ORIG_COLOR);
	}
	
	public Color getTitleColor() {
		return _titledBorder.getTitleColor();
	}
	
	public void setTitleColor(Color c) {
		_titledBorder.setTitleColor(c);
	}
	
	public Font getTitleFont() {
		return _titledBorder.getTitleFont();
	}
	
	public void setTitleFont(Font f) {
		_titledBorder.setTitleFont(f);
	}
	
	TitledBorder _titledBorder;
}
