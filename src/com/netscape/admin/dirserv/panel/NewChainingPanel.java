/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.admin.dirserv.panel.replication.ReplicationTool;

import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.management.nmclf.SuiConstants;

/**
 * Panel for Create a new chaining database for Dir Server
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	06/29/00
 * @see     com.netscape.admin.dirserv.panel
 */
public class NewChainingPanel extends BlankPanel  
    implements SuiConstants {

    public NewChainingPanel(IDSModel model, LDAPEntry SuffixEntry) {
	super( model, _section );
	_helpToken = "configuration-new-chaining-instance-dbox-help";
	_model = model;
	_entrySuffix = SuffixEntry;
    }

    public void init() {

	_myPanel.setLayout( new GridBagLayout() );
	GridBagConstraints gbc = getGBC();

	// Physical DB info
	createChainArea( _myPanel );

	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( false );
	}

	_isInitialized = true;

    }

    protected void createChainArea( JPanel grid ) {

	GridBagConstraints ugbc = getGBC(); 
	JPanel HostPanel = new GroupPanel(
					  DSUtil._resource.getString( _section, "new-chaining-title" ), true);

        ugbc.gridwidth = ugbc.REMAINDER;
	ugbc.fill = ugbc.HORIZONTAL;		
	grid.add( HostPanel, ugbc );

	GridBagLayout HLbag = new GridBagLayout();
	GridBagConstraints HLgbc = new GridBagConstraints();

        HLgbc.gridx      = 0;
        HLgbc.gridy      = 0;
	// HLgbc.gridwidth  = HLgbc.REMAINDER;
        HLgbc.gridwidth	 = 1;
        // HLgbc.gridheight = 1;
        HLgbc.weightx    = 0;
        HLgbc.weighty    = 0;
        HLgbc.fill       = HLgbc.BOTH;
        HLgbc.anchor     = HLgbc.CENTER;
        HLgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
				      0,DEFAULT_PADDING);
        HLgbc.ipadx = 0;
        HLgbc.ipady = 0;

	HostPanel.setLayout(HLbag);

	_suffix = MappingUtils.getMappingNodeSuffix(_entrySuffix);
	_suffixLabel =  makeJLabel( _section, "suffix-name" );
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.weightx	= 0;
	HLgbc.gridx		= 0;
	HLgbc.anchor	= HLgbc.EAST;
	HostPanel.add( _suffixLabel, HLgbc );

	_suffixValue = new JLabel( _suffix );
	_suffixLabel.setLabelFor(_suffixValue);

	HLgbc.weightx	= 1;
	HLgbc.gridx		= 1;
	HLgbc.gridwidth = 1;
	HLgbc.anchor	= HLgbc.CENTER;
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HostPanel.add( _suffixValue, HLgbc );

	_instanceNameLabel = makeJLabel( _section, "instance-name" );
	HLgbc.gridy++;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.weightx	= 0;
	HLgbc.gridx		= 0;
	HLgbc.anchor	= HLgbc.EAST;
	HostPanel.add( _instanceNameLabel, HLgbc );
		  
	_instanceNameText = makeJTextField( _section, "instance-name" );
	_instanceNameLabel.setLabelFor(_instanceNameText);
	HLgbc.weightx	= 1;
	HLgbc.gridx		= 1;
	HLgbc.gridwidth = 1;
	HLgbc.anchor	= HLgbc.CENTER;
	HLgbc.fill		= HLgbc.HORIZONTAL;
	_instanceNameText.setColumns( 10 );
	HostPanel.add( _instanceNameText, HLgbc );

	_authMechLabel = makeJLabel( _section, "authmech" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth = 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.weightx	= 0;
	HLgbc.anchor	= HLgbc.EAST;
	HostPanel.add( _authMechLabel, HLgbc );

    //ssl auth radio button
    ButtonGroup authGroup = new ButtonGroup();
    _sslAuth = makeJRadioButton( _section, "authmech-sslcert" );
    _sslAuth.addActionListener(this);
    _sslAuth.setEnabled(false); // because default is plain ldap
    authGroup.add(_sslAuth);
	HLgbc.gridx		= 1;
	HLgbc.fill = HLgbc.HORIZONTAL;
	HLgbc.gridwidth = HLgbc.REMAINDER;
	HLgbc.weightx = 1;
    HostPanel.add( _sslAuth, HLgbc );

    //gssapi auth radio button
    _gssapiAuth = makeJRadioButton( _section, "authmech-gssapi" );
    _gssapiAuth.addActionListener(this);
    authGroup.add(_gssapiAuth);
	HLgbc.gridy++;
	HLgbc.gridx		= 1;
	HLgbc.fill = HLgbc.HORIZONTAL;
	HLgbc.gridwidth = HLgbc.REMAINDER;
	HLgbc.weightx = 1;
    HostPanel.add( _gssapiAuth, HLgbc );

    //digest auth radio button
    _digestAuth = makeJRadioButton( _section, "authmech-digest" );
    _digestAuth.addActionListener(this);
    authGroup.add(_digestAuth);
	HLgbc.gridy++;
	HLgbc.gridx		= 1;
	HLgbc.fill = HLgbc.HORIZONTAL;
	HLgbc.gridwidth = HLgbc.REMAINDER;
	HLgbc.weightx = 1;
    HostPanel.add( _digestAuth, HLgbc );

    //simple auth radio button
    _simpleAuth = makeJRadioButton( _section, "authmech-simple" );
    _simpleAuth.setSelected(true); // default auth mech
    _simpleAuth.addActionListener(this);
    authGroup.add(_simpleAuth);
	HLgbc.gridy++;
	HLgbc.gridx		= 1;
	HLgbc.fill = HLgbc.HORIZONTAL;
	HLgbc.gridwidth = HLgbc.REMAINDER;
	HLgbc.weightx = 1;
    HostPanel.add( _simpleAuth, HLgbc );

    _bindDNLabel =  makeJLabel( _section, "bind-DN" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth = 1;
	HLgbc.fill = HLgbc.NONE;
	HLgbc.weightx = 0;
	HLgbc.anchor	= HLgbc.EAST;
	HostPanel.add( _bindDNLabel, HLgbc );
		
	_bindDNText = makeJTextField( _section, "bind-DN" );
	_bindDNLabel.setLabelFor(_bindDNText);
	HLgbc.gridx		= 1;
	HLgbc.fill = HLgbc.HORIZONTAL;
	HLgbc.gridwidth = HLgbc.REMAINDER;
	HLgbc.weightx = 1;
	HostPanel.add( _bindDNText, HLgbc );

	_bindPasswdLabel =  makeJLabel( _section, "bind-Passwd" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth = 1;
	HLgbc.fill = HLgbc.NONE;
	HLgbc.weightx = 0;
	HLgbc.anchor	= HLgbc.EAST;
	HostPanel.add( _bindPasswdLabel, HLgbc );
		
	_bindPasswdText = makeJPasswordField(20 );
	_bindPasswdLabel.setLabelFor(_bindPasswdText);
	HLgbc.gridx		= 1;
	HLgbc.fill = HLgbc.HORIZONTAL;
	HLgbc.gridwidth = 1;
	HLgbc.weightx = 1;
	HostPanel.add( _bindPasswdText, HLgbc );

	createRemoteServers( HostPanel, HLgbc );

	createURLArea( HostPanel, HLgbc );
	_bHostModified = false;
	_bAlterModified = false;


    }

    private void createURLArea( JPanel grid, GridBagConstraints ugbc ) {

	JPanel UrlPanel = new GroupPanel(
					 DSUtil._resource.getString( _section,
								     "new-url-title" ), 
					 true);

	ugbc.gridx		= 0;
	ugbc.gridy++;
        ugbc.gridwidth = ugbc.REMAINDER;
	ugbc.fill = ugbc.HORIZONTAL;		
	grid.add( UrlPanel, ugbc );

	GridBagLayout ubag = new GridBagLayout();
	GridBagConstraints urlgbc = new GridBagConstraints();

        urlgbc.gridx      = 0;
        urlgbc.gridy      = 0;
	urlgbc.gridwidth  = ugbc.REMAINDER;
        // urlgbc.gridwidth	 = 1;
        // urlgbc.gridheight = 1;
        urlgbc.weightx    = 0;
        urlgbc.weighty    = 0;
        urlgbc.fill       = urlgbc.BOTH;
        urlgbc.anchor     = urlgbc.CENTER;
        urlgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
				       0,DEFAULT_PADDING);
        urlgbc.ipadx = 0;
        urlgbc.ipady = 0;

	UrlPanel.setLayout(ubag);


	_urlValueLabel = makeJLabel( _section, "ldap-url-value" );
	urlgbc.gridx		= 0;
	urlgbc.gridwidth	= 1;
	urlgbc.fill		= urlgbc.NONE;
	urlgbc.anchor	= urlgbc.EAST;
	urlgbc.insets     = new Insets(0 ,0,
				       0, DEFAULT_PADDING);
	urlgbc.weightx	= 0;
	UrlPanel.add( _urlValueLabel, urlgbc );

		
	urlgbc.gridx		= 1;
	urlgbc.gridwidth	= urlgbc.REMAINDER;
	urlgbc.fill			= urlgbc.HORIZONTAL;
	urlgbc.anchor		= urlgbc.WEST;
	urlgbc.weightx		= 1;
	urlgbc.insets	    = new Insets(0, DEFAULT_PADDING, 0, 0);
	_urlValue = new JLabel();
	_urlValueLabel.setLabelFor(_urlValue);
	_urlValue.setLabelFor(UrlPanel);
	_sURL = new StringBuffer( "ldap://" );
	_urlValue.setText( _sURL.toString() );
	UrlPanel.add( _urlValue, urlgbc );

		

    }


    private void createRemoteServers( JPanel grid, GridBagConstraints ugbc ) {

	JPanel AlternatePanel = new GroupPanel(
					       DSUtil._resource.getString( _section,
									   "new-alternate-host-title" ), 
					       true);

	ugbc.gridx		= 0;
	ugbc.gridy++;
        ugbc.gridwidth = ugbc.REMAINDER;
	ugbc.fill = ugbc.HORIZONTAL;		
	grid.add( AlternatePanel, ugbc );

	GridBagLayout HLbag = new GridBagLayout();
	GridBagConstraints HLgbc = new GridBagConstraints();

        HLgbc.gridx      = 0;
        HLgbc.gridy      = 0;
	// HLgbc.gridwidth  = HLgbc.REMAINDER;
        HLgbc.gridwidth	 = 1;
        // HLgbc.gridheight = 1;
        HLgbc.weightx    = 0;
        HLgbc.weighty    = 0;
        HLgbc.fill       = HLgbc.BOTH;
        HLgbc.anchor     = HLgbc.CENTER;
	/*
	  HLgbc.insets     = new Insets(DEFAULT_PADDING,DEFAULT_PADDING,
	  0,DEFAULT_PADDING);
	*/
	HLgbc.insets	= new Insets(0,
				     DIFFERENT_COMPONENT_SPACE,
				     COMPONENT_SPACE,
				     0);
        HLgbc.ipadx = 0;
        HLgbc.ipady = 0;

	AlternatePanel.setLayout(HLbag);

    ButtonGroup connGroup = new ButtonGroup();
    _noEncrypt = makeJRadioButton( _section, "url-ldap" );
    _noEncrypt.setSelected(true); // default is on
    _noEncrypt.addActionListener(this);
    connGroup.add(_noEncrypt);
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.anchor	= HLgbc.WEST;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.gridwidth	= HLgbc.REMAINDER;
	HLgbc.weightx	= 0;
	AlternatePanel.add( _noEncrypt, HLgbc );		

    _sslEncrypt = makeJRadioButton( _section, "url-ldaps" );
    _sslEncrypt.addActionListener(this);
    connGroup.add(_sslEncrypt);
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.anchor	= HLgbc.WEST;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.gridwidth	= HLgbc.REMAINDER;
	HLgbc.weightx	= 0;
	AlternatePanel.add( _sslEncrypt, HLgbc );		

    _tlsEncrypt = makeJRadioButton( _section, "url-starttls" );
    _tlsEncrypt.addActionListener(this);
    connGroup.add(_tlsEncrypt);
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.anchor	= HLgbc.WEST;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.gridwidth	= HLgbc.REMAINDER;
	HLgbc.weightx	= 0;
	AlternatePanel.add( _tlsEncrypt, HLgbc );		

	_urlLdapHostLabel = makeJLabel( _section, "ldap-url-host" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth	= 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.anchor	= HLgbc.EAST;
	HLgbc.insets	= new Insets(0, 0, COMPONENT_SPACE, 0);
	HLgbc.weightx	= 0;
	AlternatePanel.add( _urlLdapHostLabel, HLgbc );
		
	_urlLdapHostText = makeJTextField( _section, "ldap-url-host" );
	_urlLdapHostLabel.setLabelFor(_urlLdapHostText);
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 1;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 1;
	HLgbc.insets	= new Insets(0, DEFAULT_PADDING, 
				     COMPONENT_SPACE,
				     DIFFERENT_COMPONENT_SPACE);
	_urlLdapHostText.setColumns( 12 );
	AlternatePanel.add( _urlLdapHostText, HLgbc );		

	_urlLdapPortLabel = makeJLabel( _section, "ldap-url-port" );
	HLgbc.gridx		= 2;
	HLgbc.gridwidth	= 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.anchor	= HLgbc.EAST;
	HLgbc.weightx	= 0;
	HLgbc.insets	= new Insets(0, 0, 
				     COMPONENT_SPACE, 
				     0);
	AlternatePanel.add( _urlLdapPortLabel, HLgbc );
		
	_urlLdapPortText = makeJTextField( _section, "ldap-url-port" );
	_urlLdapPortLabel.setLabelFor(_urlLdapPortText);
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 3;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 0; // doesn't need to extend when screen is resized
	_urlLdapPortText.setColumns( 5 );
	_urlLdapPortText.setText( _sDefaultPort );
	HLgbc.insets	= new Insets(0, DEFAULT_PADDING,
				     COMPONENT_SPACE, 0);
	AlternatePanel.add( _urlLdapPortText, HLgbc );
	//

	_urlAltHostLabel = makeJLabel( _section, "alt-url-host" );
	HLgbc.gridy++;
	HLgbc.gridx		= 0;
	HLgbc.gridwidth	= 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.anchor	= HLgbc.EAST;
	HLgbc.insets	= new Insets(0, 0, COMPONENT_SPACE, 0);
	HLgbc.weightx	= 0;
	AlternatePanel.add( _urlAltHostLabel, HLgbc );
		
	_urlAltHostText = makeJTextField( _section, "alt-url-host" );
	_urlAltHostLabel.setLabelFor(_urlAltHostText);
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 1;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 1;
	HLgbc.insets	= new Insets(0, DEFAULT_PADDING, 
				     COMPONENT_SPACE,
				     DIFFERENT_COMPONENT_SPACE);
	_urlAltHostText.setColumns( 12 );
	AlternatePanel.add( _urlAltHostText, HLgbc );		

	_urlAltPortLabel = makeJLabel( _section, "alt-url-port" );
	HLgbc.gridx		= 2;
	HLgbc.gridwidth	= 1;
	HLgbc.fill		= HLgbc.NONE;
	HLgbc.anchor	= HLgbc.EAST;
	HLgbc.weightx	= 0;
	HLgbc.insets	= new Insets(0, 0, 
				     COMPONENT_SPACE, 
				     0);
	AlternatePanel.add( _urlAltPortLabel, HLgbc );
		
	_urlAltPortText = makeJTextField( _section, "alt-url-port" );
	_urlAltPortLabel.setLabelFor(_urlAltPortText);
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 3;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 0; // doesn't need to extend when screen is resized
	HLgbc.insets	= new Insets(0, DEFAULT_PADDING,
				     COMPONENT_SPACE, 0);
	_urlAltPortText.setColumns( 5 );
	if ( ! _bAlterModified ) {
	    _urlAltPortText.setText( _sDefaultPort );
	}
	AlternatePanel.add( _urlAltPortText, HLgbc );

	_bAddAlternateServer = makeJButton( _section, "ldap-add-alternate" );
	_bAddAlternateServer.setActionCommand(ADD_ALT);
	_bAddAlternateServer.setEnabled(false);
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 4;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 0;
	HLgbc.insets	= new Insets(0,
				     DIFFERENT_COMPONENT_SPACE,
				     COMPONENT_SPACE,
				     0);
	AlternatePanel.add( _bAddAlternateServer, HLgbc );


	_urlAltData = new DefaultListModel();
	_urlAltList = new JList( _urlAltData );
	_urlAltPortLabel.setLabelFor(_urlAltList);
	_urlAltList.setVisibleRowCount( ROWS );
	_urlAltList.setCellRenderer( new AlternateCellRenderer() );
	JScrollPane scrollAlt = new JScrollPane();
	scrollAlt.getViewport().setView( _urlAltList );
	HLgbc.gridwidth  = 4;
	HLgbc.gridx = 0;
	HLgbc.gridy++;
	HLgbc.insets = new Insets(0, 0, 0, 0);
	HLgbc.fill= HLgbc.BOTH;		
        HLgbc.weightx    = 1;
        HLgbc.weighty    = 1;
	AlternatePanel.add( scrollAlt, HLgbc);	

	_bDelAlternateServer = makeJButton( _section, "ldap-del-alternate" );
	_bDelAlternateServer.setActionCommand(DELETE_ALT);
	_bDelAlternateServer.setEnabled(false);
	HLgbc.fill		= HLgbc.HORIZONTAL;
	HLgbc.gridx		= 4;
	HLgbc.gridwidth	= 1;
	HLgbc.weightx	= 0;
	HLgbc.insets	= new Insets(0,
				     DIFFERENT_COMPONENT_SPACE,
				     COMPONENT_SPACE,
				     0);
	AlternatePanel.add( _bDelAlternateServer, HLgbc );

    }

		
    /**
     * Retreive db default folder from Directory server
     */
    private void getChainingLoc() {

	// Search default databse directory 
	try {
	    _model.setWaitCursor( true );
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    LDAPSearchResults res =
		ldc.search( CONFIG_BASEDN,
			    ldc.SCOPE_ONE,
			    "nsslapd-pluginid=chaining database",
			    null,
			    false );
	    if(res.hasMoreElements() ) {
		LDAPEntry bentry = (LDAPEntry)res.nextElement();
		_PluginLoc = bentry.getDN();
		Debug.println( "NewChainingPanel.getChainingLoc() {");
		Debug.println( "*** plugin db: " + _PluginLoc );	
	    }
	} catch ( LDAPException e ) {
	    Debug.println( "NewChainingPanel.getChainingLoc() " + e );
	} finally {
	    _model.setWaitCursor( false );
	}
		
	return;
    } // getChainingLoc

    private boolean checkConnAndAuth() {
	boolean ret = true;
	if (_simpleAuth.isSelected() || _digestAuth.isSelected()) {
	    // dn & pwd required
	    ret = ( _bindDNText.getText() != null ) && 
	       	  ( _bindDNText.getText().trim().length() > 0 ) &&
	       	  DN.isDN(_bindDNText.getText()) &&
	          ( _bindPasswdText.getText() != null ) && 
		  ( _bindPasswdText.getText().trim().length() > 0 );
		
	}
	
	return ret;
    }

    /**
     * Enable/disable OK button
     *
     * @param ok true to enable the OK button
     */
    private void setOkay( boolean ok ) {
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( ok );
	} else {
	}
    }

    private void checkOkay() {
	String chname = getInstancename();
	String chserver = _urlLdapHostText.getText();
		
	boolean ok = ( (chname != null) && 
		       (chname.length() > 0) && 
		       (DSUtil.isValidBckName (chname)) &&
		       checkConnAndAuth() &&
		       (chserver != null) &&
		       (chserver.length() > 0));
	setOkay( ok );
    }

    private void enableSimpleAuth (boolean enable){
        _bindDNLabel.setEnabled(enable);
        _bindDNText.setEnabled(enable);
        _bindPasswdLabel.setEnabled(enable);
        _bindPasswdText.setEnabled(enable);
	_bindPasswdText.setBackground(_bindDNText.getBackground());
    }

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
	if (e.getSource().equals(_sslAuth) && _sslAuth.isSelected()) {
	    //disable
	    enableSimpleAuth (false);
	}
	if (e.getSource().equals(_gssapiAuth) && _gssapiAuth.isSelected()) {
	    // enable
	    enableSimpleAuth (true);
	}
	if (e.getSource().equals(_simpleAuth) && _simpleAuth.isSelected()) {
	    //enable
	    enableSimpleAuth (true);
	}
	if (e.getSource().equals(_digestAuth) && _digestAuth.isSelected()) {
	    //enable
	    enableSimpleAuth (true);
	}
	if (e.getSource().equals(_noEncrypt) && _noEncrypt.isSelected()) {
	    /* set to use non-SSL port LDAP */
	    _sDefaultPort = DEFAULT_LDAP_PORT;
	    //disable
	    _sslAuth.setEnabled(false);
	    if (_sslAuth.isSelected()) {
		// have to select something else
		_simpleAuth.setSelected(true);
	    }
	    enableSimpleAuth(true);
	    _gssapiAuth.setEnabled(true);
	    _digestAuth.setEnabled(true);
	}
	boolean ssl_selected = false;
	if (e.getSource().equals(_sslEncrypt) && _sslEncrypt.isSelected()) {
	    /* set to use SSL port */
	    _sDefaultPort = DEFAULT_LDAPS_PORT;
	    ssl_selected = true;
	}
	if (e.getSource().equals(_tlsEncrypt) && _tlsEncrypt.isSelected()) {
	    /* set to use non-SSL port for startTLS */
	    _sDefaultPort = DEFAULT_LDAP_PORT;
	    ssl_selected = true;
	}
	if (ssl_selected) {
	    _sslAuth.setEnabled(true);
	    _gssapiAuth.setEnabled(false);
	    if (_gssapiAuth.isSelected()) {
		// have to select something else
		_simpleAuth.setSelected(true);
		enableSimpleAuth(true);
	    }
	}
	if (e.getSource().equals( _bAddAlternateServer )) {
	    StringBuffer nalt = new StringBuffer();
	    nalt.append( _urlAltHostText.getText() );

	    if (( _urlAltPortText.getText() != null ) &&
		    ( _urlAltPortText.getText().trim().length() > 0)) {
		nalt.append( ":" );
		nalt.append( _urlAltPortText.getText().trim() );
	    }

	    _urlAltData.addElement( nalt.toString() );
	    _urlAltHostText.setText("");
	    _bAddAlternateServer.setEnabled( false );

	} else if (e.getSource().equals( _bDelAlternateServer )) {
	    int[] indices = _urlAltList.getSelectedIndices();
	    for (int i = indices.length - 1; i>= 0;i--) {					
		_urlAltData.removeElementAt(indices[i]);
	    }						
	    _bDelAlternateServer.setEnabled( false );
	} 
	super.actionPerformed(e);
	checkUrl();
	checkOkay();
    }


    private String getUrlVal() {
	StringBuffer nurl = new StringBuffer();

	if ( _sslEncrypt.isSelected() ) {
	    nurl.append( "ldaps://");
	} else {
	    nurl.append( "ldap://");
	}

	// Don't need to test if it's null the OK is not activated otherwise
	if(( _urlLdapHostText.getText() != null ) && 
	   ( _urlLdapHostText.getText().trim().length() > 0 )){
	    nurl.append( _urlLdapHostText.getText().trim());
	    if(( _urlLdapPortText.getText() != null ) && 
	       ( _urlLdapPortText.getText().trim().length() > 0 )){
		nurl.append(":");
		nurl.append( _urlLdapPortText.getText().trim());
	    }
	}				

	// Loop on alternate servers		
	for (int i = 0;
	     i < _urlAltList.getModel().getSize();
	     i++) {
	    nurl.append(" ");
	    nurl.append( _urlAltList.getModel().getElementAt( i ));
	}
				
	// Must finish by a '/'
	nurl.append("/");
	return( nurl.toString());
    }

    private void checkUrl() {

	String nurl = getUrlVal();
	_urlValue.setText( nurl );

	try {
	    setChangeState( _urlValue, CHANGE_STATE_UNMODIFIED );
	    LDAPUrl nu = new LDAPUrl( nurl );
	} catch ( java.net.MalformedURLException e ) {
	    setChangeState( _urlValue, CHANGE_STATE_ERROR );
	}
    }									 

    public int addChainingBackend(String suffix ) {

	String _instName = _instanceNameText.getText().trim();
	getChainingLoc();

	LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	// Add Instance Entry
	String dn_dbInst = "cn=" + _instName + "," + _PluginLoc;
	Debug.println("****** dn = " + dn_dbInst);

	LDAPAttributeSet attrs = new LDAPAttributeSet();

	String objectclass_dbInst[] = { "top", "extensibleObject", "nsBackendInstance" };
	LDAPAttribute attr = new LDAPAttribute( "objectclass", objectclass_dbInst );
	attrs.add( attr );

	String cn_dbInst[] = { _instName };
	attrs.add( new LDAPAttribute( "cn", cn_dbInst ) );

				   
	String suffix_dbInstConfig[] = { suffix };
	attrs.add( new LDAPAttribute ( "nsslapd-suffix", suffix_dbInstConfig ));

	String nsBindDN = _bindDNText.getText();

	if(( nsBindDN != null ) &&
	   ( nsBindDN.trim().length() > 0)) {
			
	    String nsMultiplexorBindDn[] = { nsBindDN.trim() };
	    attrs.add( new LDAPAttribute ( "nsMultiplexorBindDn",
					   nsMultiplexorBindDn ));

	    String nsPasswd = _bindPasswdText.getText();

	    if(( nsPasswd != null ) &&
	       ( nsPasswd.trim().length() > 0)) {
		Debug.println("****** pass= " + nsPasswd);
		String nsMultiplexorCredentials[] = { nsPasswd.trim() };
		attrs.add( new LDAPAttribute ( "nsMultiplexorCredentials",
					       nsMultiplexorCredentials ));
	    }

	}

	String mech = null;
	if (_sslAuth.isSelected()) {
	    mech = "EXTERNAL";
	}
	if (_gssapiAuth.isSelected()) {
	    mech = "GSSAPI";
	}
	if (_digestAuth.isSelected()) {
	    mech = "DIGEST-MD5";
	}
	if (mech != null) {
	    attrs.add( new LDAPAttribute ( "nsBindMechanism",
		    mech ));
	}
	if (_tlsEncrypt.isSelected()) {
	    attrs.add( new LDAPAttribute ( "nsUseStartTLS",
	    	    "on" ));	    
	}

	String ldapurl = getUrlVal();
	if(( ldapurl != null ) &&
	   ( ldapurl.trim().length() > 0)) {
			
	    String nsFarmServerURL[] = { ldapurl.trim() };
	    attrs.add( new LDAPAttribute ( "nsFarmServerURL",
					   nsFarmServerURL ));
	}

	LDAPEntry dbInst = new LDAPEntry( dn_dbInst, attrs );
	_model.setWaitCursor( true );
	try {
	    ldc.add( dbInst );
	    Debug.println("****** add:" + dn_dbInst);
	} catch (LDAPException e) {
	    if( e.getLDAPResultCode() == 82 ) {
		String[] args_m = { nsBindDN };
		DSUtil.showErrorDialog( getModel().getFrame(),
					"error-bad-user",
					new String[] { dn_dbInst, nsBindDN },
					_section);
		return (0);
	    } else {
		String[] args_m = { dn_dbInst, e.toString()} ;
		DSUtil.showErrorDialog( getModel().getFrame(),
					"error-add-mapping",
					args_m,
					_section);
		return (0);
	    }
	} finally {
	    _model.setWaitCursor( false );
	}
	return (1);
		
    }

    public boolean checkUnique() {
	_model.setWaitCursor( true );
	try {
	    String InstanceName = _instanceNameText.getText();
	    Debug.println("NewChainingPanel.checkUnique(). check if " + InstanceName + " is unique");
	    _model.setWaitCursor( true );
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    if ( InstanceName != null) {
		LDAPSearchResults res =
		    ldc.search( CONFIG_BASEDN,
				ldc.SCOPE_SUB ,
				"(&(cn=" + InstanceName + ")(objectclass=nsBackendInstance))",
				null,
				false );
		_model.setWaitCursor( false );
		if (res.hasMoreElements()) {
		    DSUtil.showErrorDialog( getModel().getFrame(),
					    "backendname-exist",
					    InstanceName );
		    _instanceNameText.selectAll();
		    return (false);
		} //if (res.hasMoreElements())
	    } // if( InstanceName != null)
	} catch (LDAPException e) { 
	    _model.setWaitCursor( false );
	    DSUtil.showErrorDialog( getModel().getFrame(),
				    "backendname-can-create",
				    e.toString() );
	    return( false );
	} finally {
	    _model.setWaitCursor( false );
	} 
	return( true );
    }

    public void resetCallback() {
	/* No state to preserve */
	clearDirtyFlag();
	hideDialog();
    }

    /**
     * Enable the button and attached field.
     */
    private void setMappingState() {
	setMappingState( NEW_NODE );
    }

    private void setMappingState( int isSelected ) {
	switch ( isSelected ) {
	case EXISTING_NODE : {
	    _NewNodeText.setEnabled( false );
	    _NewNodeLabel.setEnabled( false );
	    _comboNewNodeMapping.setEnabled( false );
	    _comboNewNodeLabel.setEnabled( false );
	    _comboMapping.setEnabled( true );
	    _comboLabel.setEnabled( true );
	    _isNewMappingNodeValid = true;
	    break;
	}
	case NEW_NODE : {
	    _comboMapping.setEnabled( false );
	    _comboLabel.setEnabled( false );
	    _comboNewNodeMapping.setEnabled( true );
	    _comboNewNodeLabel.setEnabled( true );
	    _NewNodeText.setEnabled( true );
	    _NewNodeLabel.setEnabled( true );
	    _isNewMappingNodeValid = false;
	    break;
	}
	case NO_NODE : {
	    _NewNodeText.setEnabled( false );
	    _NewNodeLabel.setEnabled( false );
	    _comboMapping.setEnabled( false );
	    _comboLabel.setEnabled( false );
	    _comboNewNodeMapping.setEnabled( false );
	    _comboNewNodeLabel.setEnabled( false );
	    _isNewMappingNodeValid = true;
	    break;
	}
	}
	repaint();

    }

    public void okCallback() {
	/* No state to preserve */
	if(checkUnique()) {
	    /* Add backend in cn=chaining database,cn=plugins,cn=config */
	    if ( addChainingBackend( _suffix ) > 0 ) {
		// reload the entry anyway
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		try {
		    _entrySuffix = ldc.read(_entrySuffix.getDN());
		} catch (LDAPException e){
		    String[] args = { _entrySuffix.getDN(), e.toString() };
		    DSUtil.showErrorDialog( getModel().getFrame(),
					    "error-reload-suffix",
					    args,
					    _section );
		    return;
		}
		if(MappingUtils.addBackendInSuffix( _model,
				       _entrySuffix,
				       _instanceNameText.getText(),
				       _section)){
		    /* WARNING ON Anonymous user */
		    if (( _bindDNText.getText() == null ) ||
			( _bindPasswdText.getText() == null ) ||
			( _bindDNText.getText().trim().length() == 0 ) ||
			( _bindPasswdText.getText().trim().length() == 0 )) {
			
			String[] args_m = { _urlLdapHostText.getText() };
			DSUtil.showInformationDialog(getModel().getFrame(),
						     "refresh-console-anon",
						     args_m,
						     _section);
		    } else {
			String[] args_m = { _bindDNText.getText(), _urlLdapHostText.getText() };
			DSUtil.showInformationDialog(getModel().getFrame(),
						     "refresh-console-user",
						     args_m,
						     _section);
		    }
		    hideDialog();
		}
	    }
	}
    }

	
    private boolean isPortValid() {
	String nport = _urlLdapPortText.getText();
	boolean res = true;

	if(( nport != null) &&
	   ( nport.trim().length() > 0)) {
	    try {
		int p = Integer.parseInt( nport );
	    } catch (NumberFormatException nfe) {
		res = false ;
	    }
	}
	return ( res );
    }

    private void checkLabels( DocumentEvent e ) {
	if( e.getDocument() == _bindDNText.getDocument() ) {
	    if(( _bindDNText.getText() != null) && 
	       ( _bindDNText.getText().trim().length() > 0 )){
		// depends on auth type
		boolean required = _simpleAuth.isSelected() || _digestAuth.isSelected();
		if ( !required || DN.isDN ( _bindDNText.getText() )) {
		    setChangeState( _bindDNLabel, CHANGE_STATE_UNMODIFIED );
		    _isBindDNValid = true;
		} else {
		    setChangeState( _bindDNLabel, CHANGE_STATE_ERROR );
		    _isBindDNValid = false;
		}
	    } else {
		setChangeState( _bindDNLabel, CHANGE_STATE_UNMODIFIED );
		_isBindDNValid = true;
	    }
	} else if( e.getDocument() == _urlAltHostText.getDocument() ) {
	    boolean addok = false;
	    if(( _urlAltHostText.getText() != null) && 
	       ( _urlAltHostText.getText().trim().length() > 0)) {
		addok = true;
	    }
	    _bAddAlternateServer.setEnabled( addok && isPortValid());

	} else if( e.getDocument() == _urlAltPortText.getDocument() ) {
	    if( isPortValid() ) {
		setChangeState( _urlLdapPortLabel, CHANGE_STATE_UNMODIFIED );
	    } else {
		setChangeState( _urlLdapPortLabel,
			  CHANGE_STATE_ERROR);
		_bAddAlternateServer.setEnabled( false );
	    }
	} else if( e.getDocument() == _instanceNameText.getDocument() ) {
	    if ( DSUtil.isValidBckName( _instanceNameText.getText()) ) {
		setChangeState( _instanceNameLabel, CHANGE_STATE_UNMODIFIED );
	    }else {
		setChangeState( _instanceNameLabel, CHANGE_STATE_ERROR );
	    }
	}
    }


    public void changedUpdate(DocumentEvent e) {
	if( ! _isInitialized ) return;	
	checkLabels( e );
	checkUrl();
	super.changedUpdate( e );
	checkOkay();
    }


    public void removeUpdate(DocumentEvent e) {
	if( ! _isInitialized ) return;
	checkLabels( e );
	checkUrl();
	super.removeUpdate( e );
	checkOkay();
    }

    public void insertUpdate(DocumentEvent e) {
	if( ! _isInitialized ) return;
	checkLabels( e );
	checkUrl();
	super.insertUpdate( e );
	checkOkay();
    }

    public String getInstancename() {
	return _instanceNameText.getText().trim();
    }

    class AlternateCellRenderer extends DefaultListCellRenderer {  
	// This is the only method defined by ListCellRenderer.  We just
	// reconfigure the Jlabel each time we're called.
		
	public Component getListCellRendererComponent(
						      JList list,
						      Object value,            // value to display
						      int index,               // cell index
						      boolean isSelected,      // is the cell selected
						      boolean cellHasFocus)    // the list and the cell have the focus
	{
	    if ( list == _urlAltList ){
		if (list.isSelectionEmpty()) {
		    _bDelAlternateServer.setEnabled(false);
		} else {
		    _bDelAlternateServer.setEnabled(true);
		}
	    }
	    checkOkay();
	    return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
    }

    private JLabel		_suffixLabel;
    private JLabel		_suffixValue;
    private JTextField	_instanceNameText;
    private JLabel		_instanceNameLabel;
    private JTextField	_mappingNameText;
    private JLabel		_mappingNameLabel;
    private JTextField	_NewNodeText;
    private JLabel		_NewNodeLabel;

    private IDSModel	_model = null;


    private JComboBox _comboMapping;
    private JLabel _comboLabel;
    private JButton _browseButton;
    private String _PluginLoc = "";


    private JTextField	_urlLdapHostText;
    private JLabel		_urlLdapHostLabel;
    private JTextField	_urlLdapPortText;
    private JLabel		_urlLdapPortLabel;
	
    private JTextField	_urlAltHostText;
    private JLabel		_urlAltHostLabel;
    private JTextField	_urlAltPortText;
    private JLabel		_urlAltPortLabel;

    private JLabel      _authMechLabel;
    private JRadioButton _sslAuth, _gssapiAuth, _digestAuth, _simpleAuth;
    private JLabel		_bindDNLabel;
    private JTextField	_bindDNText;
    private JLabel		_bindPasswdLabel;
    private JTextField	_bindPasswdText;
    private JRadioButton _noEncrypt, _sslEncrypt, _tlsEncrypt;

    private JComboBox	_comboNewNodeMapping;
    private JLabel		_comboNewNodeLabel;

    private JButton		_bAddAlternateServer;
    private JButton		_bDelAlternateServer;
    private JButton		_bTestUrl;

    private JLabel		_urlValue;
    private JLabel		_urlValueLabel;
    private JList		_urlAltList;
    private DefaultListModel	_urlAltData;

    private boolean		_isBindDNValid = true;
    private boolean		_isNewMappingNodeValid = false;
    private boolean		_bHostModified = false;
    private boolean		_bAlterModified = false;
    private final int ROWS = 4;

    private StringBuffer _sURL;
    private String _sDefaultPort = DEFAULT_LDAP_PORT;
    private LDAPEntry	_entrySuffix;
    private String		_suffix = "";


    private final static String _section = "newchaining";
    static final String CONFIG_BASEDN = "cn=plugins, cn=config" ;
    static final String CONFIG_MAPPING = "cn=mapping tree, cn=config" ;
    static final String ROOT_MAPPING_NODE = "is root suffix";
    static final String HELP_REF = "helpReferral";
    static final int EXISTING_NODE = 0 ;
    static final int NEW_NODE = 1;
    static final int NO_NODE = 2;
    static final String ADD_ALT = "addAlternate";
    static final String DELETE_ALT = "deleteAlternate";
    static final String DEFAULT_LDAP_PORT = "389";
    static final String DEFAULT_LDAPS_PORT = "636";

 
    //copy from BlankPanel
    final static int DEFAULT_PADDING = 6;
    final static Insets DEFAULT_EMPTY_INSETS = new Insets(0,0,0,0);
    final static Insets BOTTOM_INSETS = new Insets(6,6,6,6);
}
