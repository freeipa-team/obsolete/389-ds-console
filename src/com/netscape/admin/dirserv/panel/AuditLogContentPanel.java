/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.logging.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class AuditLogContentPanel extends LogContentPanel {

	public AuditLogContentPanel(IDSModel model) {
		super(model, "auditlog-content");
		_helpToken = "status-logs-audit-help";
		_refreshWhenSelect = false;
	}

    protected DSLogViewer getViewer() {
		return new DSLogViewer( getModel(),
								new AuditLogViewerModel(
									getModel().getServerInfo(), cgiName ),
								CONFIG_DN, AUDIT_LOG_ATTR_NAME, AUDIT_LOGLIST_ATTR_NAME );
	}

    int[] getWidths() {
		return _widths;
	}

	private int[] _widths = { 500 };
	private static final String CONFIG_DN = "cn=config";
	private static final String AUDIT_LOG_ATTR_NAME = "nsslapd-auditlog";
	private static final String AUDIT_LOGLIST_ATTR_NAME = "nsslapd-auditlog-list";
}
