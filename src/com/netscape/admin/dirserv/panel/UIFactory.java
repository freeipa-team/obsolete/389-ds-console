/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/


package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.text.*;
import com.netscape.management.nmclf.SuiConstants;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.JButtonFactory;
import com.netscape.management.client.util.SingleByteTextField;
import com.netscape.management.client.util.MultilineLabel;
import com.netscape.admin.dirserv.DSUtil;

/**
 * UIFactory
 * Provides methods for producing standardized JFC UI components
 * 
 * @version
 * @author
 **/
public class UIFactory {
    public static boolean initialize() {
		setConstants();
		return true;
	}

    private static void setConstants() {
		if ( _tfEnabledColor != null ) {
			return;
		}

        _stdInsets = new Insets(
			SuiConstants.HORIZ_WINDOW_INSET,
            SuiConstants.VERT_WINDOW_INSET,
			SuiConstants.HORIZ_WINDOW_INSET,
            SuiConstants.VERT_WINDOW_INSET );
        
        _borderInsets = new Insets(
			SuiConstants.HORIZ_WINDOW_INSET,
            SuiConstants.VERT_WINDOW_INSET,
			SuiConstants.HORIZ_WINDOW_INSET,
            SuiConstants.VERT_WINDOW_INSET );
        
       _componentInsets = new Insets(
		   SuiConstants.COMPONENT_SPACE,
		   SuiConstants.COMPONENT_SPACE,
		   SuiConstants.COMPONENT_SPACE,
		   SuiConstants.COMPONENT_SPACE );

	   _componentSpace = SuiConstants.COMPONENT_SPACE;
	   _differentSpace = SuiConstants.DIFFERENT_COMPONENT_SPACE;
	   _separatedSpace = SuiConstants.SEPARATED_COMPONENT_SPACE;

	   /* Get active and disabled text field colors */
	   JTextField tf = new JTextField();
	   _tfEnabledColor = tf.getBackground();
	   JPanel panel = new JPanel();
	   _tfDisabledColor = panel.getBackground();
	}

    /*==========================================================
	 * Factory methods to produce JComponents
     *==========================================================*/

    /**
     * Label with optional icon, optional text, and optional alignment
     *
     * @param i Icon, may be null
     * @param s Text for label, may be null
     * @param a Alignment, -1 for default
	 *
	 * @return a JLabel
     */
    public static JLabel makeJLabel( Icon i,
									 String s,
									 int a ) {
        JLabel label = new JLabel();
        if (i != null)
            label.setIcon(i);
        if (s != null)
            label.setText(s);
        if (a != -1)
            label.setHorizontalAlignment(a);
        return label;
      }

    /**
     * Label with no icon or text, and default alignment
	 *
	 * @return a JLabel
     */
    public static JLabel makeJLabel() {
		return makeJLabel((Icon)null, null, -1);
	}

    /**
     * Label with optional icon, no text, and default alignment
     *
     * @param i Icon, may be null
	 *
	 * @return a JLabel
     */
    public static JLabel makeJLabel( Icon i ) {
        return makeJLabel(i, null, -1);
    }

    /**
     * Label with optional icon, no text, and optional alignment
     *
     * @param i Icon, may be null
     * @param a Alignment, -1 for default
	 *
	 * @return a JLabel
     */
    public static JLabel makeJLabel( Icon i,
									 int a ) {
        return makeJLabel(i, null, a);
    }

    /**
     * Label no icon, optional text, and default alignment
     *
     * @param s Text for label, may be null
	 *
	 * @return a JLabel
     */
    public static JLabel makeJLabel( String s ) {
        return makeJLabel((Icon)null, s, -1);
    }

    /**
     * Label with no icon, optional text, and optional alignment
     *
     * @param i Icon, may be null
     * @param s Text for label, may be null
     * @param a Alignment, -1 for default
	 *
	 * @return a JLabel
     */
    public static JLabel makeJLabel( String s,
									 int a ) {
        return makeJLabel((Icon)null, s, a);
    }

    /**
     * Label with no icon, with text from a resource file, and with
	 * optional alignment
     *
     * @param section from properties file
     * @param keyword from properties file
     * @param a Alignment, -1 for default
     * @param resource ResourceSet to get value from, may be null
     */
    public static JLabel makeJLabel( String section,
									 String keyword,
									 int a,
									 ResourceSet resource ) {
		ResourceSet res = (resource != null) ? resource : _resource;
        String label = res.getString(section, keyword + "-label");
        if (label == null)
            label = "label";
        JLabel jl = makeJLabel((Icon)null, label, a);
        setToolTip(section, keyword, jl);		

        return jl;
    }

    /**
     * Label with no icon, with text from a resource file, and with
	 * default alignment
     *
     * @param section from properties file
     * @param keyword from properties file
     * @param resource ResourceSet to get value from, may be null
     */
    public static JLabel makeJLabel( String section,
									 String keyword,
									 ResourceSet resource ) {
        return makeJLabel(section, keyword, -1, resource);
    }

    /**
     * Label with no icon, with text from a default resource file, and with
	 * default alignment
     *
     * @param section from properties file
     * @param keyword from properties file
     * @param resource ResourceSet to get value from, may be null
     */
    public static JLabel makeJLabel( String section,
									 String keyword ) {
        return makeJLabel(section, keyword, -1, (ResourceSet)null);
    }

    /**
     * Word-wrappable multi-line label
	 *
	 * @param rows Number of rows
	 * @param cols Number of columns
	 * @return a JTextArea
     */
    public static JTextArea makeMultiLineLabel( int rows, int cols ) {
		return new MultilineLabel( rows, cols );
	}

    /**
     * Word-wrappable multi-line label
	 * @param rows Number of rows
	 * @param cols Number of columns
	 * @param text Initial text
	 *
	 * @return a JTextArea
     */
    public static JTextArea makeMultiLineLabel( int rows, int cols,
												String text ) {
		JTextArea area = makeMultiLineLabel( rows, cols );
		area.setText( text );
		return area;
	}

    /**
     * Text field with optional action listener and document listener,
	 * optional Document, optional text, and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param d Optional Document, may be null
     * @param s Optional text for the field
	 * @param len Length in columns of the field
	 * @param numerical True if the field is to only accept numerical values
	 *
	 * @return A JTextField
     */
    private static JTextField makeTextField( Object l,
											 Document d,
											 String s,
											 int len,
											 boolean numerical ) {
		if ( _tfEnabledColor == null ) {
			initialize();
		}
        JTextField tf;
		if ( numerical ) {
			tf = new SingleByteTextField() {
				public void setEnabled( boolean enabled ) {
					super.setEnabled( enabled );
					setBackground( enabled ? _tfEnabledColor : _tfDisabledColor );
				}
			};
		} else {
			tf = new JTextField() {
				public void setEnabled( boolean enabled ) {
					super.setEnabled( enabled );
					setBackground( enabled ? _tfEnabledColor : _tfDisabledColor );
				}
			};
		}
		tf.setEnabled( true );
		/* All text fields are supposed to have a 3 pixel horizontal inset */
		tf.setMargin( getTextInsets() );
        if (d != null)
            tf.setDocument(d);
        if (s != null)
            tf.setText(s);
        if (len != -1)
            tf.setColumns(len);

		if ( l != null ) {
			if ( l instanceof ActionListener )
				tf.addActionListener( (ActionListener)l );
			if ( l instanceof DocumentListener )
				tf.getDocument().addDocumentListener( (DocumentListener)l );
			//detect text changes
		}
        return tf;
    }

    /**
     * Text field with optional action listener and document listener
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
	 * @param numerical True if the field is to only accept numerical values
	 *
	 * @return A JTextField
     */
    private static JTextField makeTextField( Object l, boolean numerical ) {
        return makeTextField( l, null, null, -1, numerical );
    }

    /**
     * Text field with optional action listener and document listener,
	 * and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
	 * @param len Length in columns of the field
	 * @param numerical True if the field is to only accept numerical values
	 *
	 * @return A JTextField
     */
    private static JTextField makeTextField( Object l,
											 int len,
											 boolean numerical ) {
        return makeTextField( l, null, null, len, numerical );
    }

    /**
     * Text field with optional action listener and document listener,
	 * and optional text
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param s Optional text for the field
	 * @param numerical True if the field is to only accept numerical values
	 *
	 * @return A JTextField
     */
    private static JTextField makeTextField( Object l,
											 String s,
											 boolean numerical ) {
        return makeTextField( l, null, s, -1, numerical );
    }

    /**
     * Text field with optional action listener and document listener,
	 * optional text, and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param s Optional text for the field
	 * @param len Length in columns of the field
	 * @param numerical True if the field is to only accept numerical values
	 *
	 * @return A JTextField
     */
    private static JTextField makeTextField( Object l,
											 String s,
											 int len,
											 boolean numerical ) {
        return makeTextField( l, null, s, len, numerical );
    }

    /**
     * Text field with optional action listener and document listener,
	 * to be defined in a resource file, with optional default text and
	 * length if the values are not in the resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param defval Optional text for the field
	 * @param defcols Length in columns of the field
     * @param resource ResourceSet to get value from, may be null
	 * @param numerical True if the field is to only accept numerical values
	 *
	 * @return A JTextField
     */
    private static JTextField makeTextField( Object l, 
											 String section,
											 String keyword,
											 String defval,
											 int defcols,
											 ResourceSet resource,
											 boolean numerical ) {
		ResourceSet res = (resource != null) ? resource : _resource;
        String text = res.getString(section, keyword + "-default");
        if (text == null)
            text = defval;
        int columns = 0;
        try {
            columns = Integer.parseInt(
                res.getString(section, keyword + "-columns")
            );
        } catch (Exception e) {
            // if invalid integer, just use default
        }
        if (columns < 1)
            columns = defcols;
        JTextField jtf = makeTextField( l, null, text, columns, numerical );
        setToolTip(section, keyword, jtf, res);

        return jtf;
    }

    /**
     * Text field with optional action listener and document listener,
	 * to be defined in a resource file, with optional default text and
	 * length if the values are not in the resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param defval Optional text for the field
	 * @param defcols Length in columns of the field
	 * @param numerical True if the field is to only accept numerical values
	 *
	 * @return A JTextField
     */
    private static JTextField makeTextField( Object l, 
											 String section,
											 String keyword,
											 String defval,
											 int defcols,
											 boolean numerical ) {
		return makeTextField( l, section, keyword, defval, defcols, null,
							  numerical );
	}

    /**
     * Text field with optional action listener and document listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param resource ResourceSet to get value from, may be null
	 * @param numerical True if the field is to only accept numerical values
	 *
	 * @return A JTextField
     */
    private static JTextField makeTextField( Object l,
											 String section,
											 String keyword,
											 ResourceSet resource,
											 boolean numerical ) {
		return makeTextField( l, section, keyword, null, -1, resource, numerical );
	}

    /**
     * Text field with optional action listener and document listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 * @param numerical True if the field is to only accept numerical values
	 *
	 * @return A JTextField
     */
    private static JTextField makeTextField( Object l,
											 String section,
											 String keyword,
											 boolean numerical ) {
		return makeTextField( l, section, keyword, (ResourceSet)null, numerical );
	}

    /**
     * Text field with optional action listener and document listener,
	 * optional Document, optional text, and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param d Optional Document, may be null
     * @param s Optional text for the field
	 * @param len Length in columns of the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeJTextField( Object l,
											 Document d,
											 String s,
											 int len) {
		return makeTextField( l, d, s, len, false );
    }

    /**
     * Text field with optional action listener and document listener
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
	 *
	 * @return A JTextField
     */
    public static JTextField makeJTextField( Object l ) {
        return makeJTextField( l, null, null, -1 );
    }

    /**
     * Text field with optional action listener and document listener,
	 * and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
	 * @param len Length in columns of the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeJTextField( Object l,
											 int len ) {
        return makeJTextField( l, null, null, len );
    }

    /**
     * Text field with optional action listener and document listener,
	 * and optional text
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param s Optional text for the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeJTextField( Object l,
											 String s ) {
        return makeJTextField( l, null, s, -1 );
    }

    /**
     * Text field with optional action listener and document listener,
	 * optional text, and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param s Optional text for the field
	 * @param len Length in columns of the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeJTextField( Object l,
											 String s,
											 int len ) {
        return makeJTextField( l, null, s, len );
    }

    /**
     * Text field with optional action listener and document listener,
	 * to be defined in a resource file, with optional default text and
	 * length if the values are not in the resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param defval Optional text for the field
	 * @param defcols Length in columns of the field
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JTextField
     */
    public static JTextField makeJTextField( Object l, 
											 String section,
											 String keyword,
											 String defval,
											 int defcols,
											 ResourceSet resource ) {
		return makeTextField( l, section, keyword, defval, defcols, resource,
							  false );
    }

    /**
     * Text field with optional action listener and document listener,
	 * to be defined in a resource file, with optional default text and
	 * length if the values are not in the resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param defval Optional text for the field
	 * @param defcols Length in columns of the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeJTextField( Object l, 
											 String section,
											 String keyword,
											 String defval,
											 int defcols ) {
		return makeJTextField( l, section, keyword, defval, defcols, null );
	}

    /**
     * Text field with optional action listener and document listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JTextField
     */
    public static JTextField makeJTextField( Object l,
											 String section,
											 String keyword,
											 ResourceSet resource ) {
		return makeJTextField( l, section, keyword, null, -1, resource );
	}

    /**
     * Text field with optional action listener and document listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 *
	 * @return A JTextField
     */
    public static JTextField makeJTextField( Object l,
											 String section,
											 String keyword) {
		return makeJTextField( l, section, keyword, (ResourceSet)null );
	}

    /**
     * Numerical Text field with optional action listener and document listener,
	 * optional Document, optional text, and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param d Optional Document, may be null
     * @param s Optional text for the field
	 * @param len Length in columns of the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeNumericalJTextField( Object l,
													  Document d,
													  String s,
													  int len) {
		return makeTextField( l, d, s, len, true );
    }

    /**
     * Numerical Text field with optional action listener and document listener
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
	 *
	 * @return A JTextField
     */
    public static JTextField makeNumericalJTextField( Object l ) {
        return makeNumericalJTextField( l, null, null, -1 );
    }

    /**
     * Numerical Text field with optional action listener and document listener,
	 * and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
	 * @param len Length in columns of the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeNumericalJTextField( Object l,
													  int len ) {
        return makeNumericalJTextField( l, null, null, len );
    }

    /**
     * Numerical Text field with optional action listener and document listener,
	 * and optional text
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param s Optional text for the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeNumericalJTextField( Object l,
													  String s ) {
        return makeNumericalJTextField( l, null, s, -1 );
    }

    /**
     * Numerical Text field with optional action listener and document listener,
	 * optional text, and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param s Optional text for the field
	 * @param len Length in columns of the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeNumericalJTextField( Object l,
													  String s,
													  int len ) {
        return makeNumericalJTextField( l, null, s, len );
    }

    /**
     * Numerical Text field with optional action listener and document listener,
	 * to be defined in a resource file, with optional default text and
	 * length if the values are not in the resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param defval Optional text for the field
	 * @param defcols Length in columns of the field
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JTextField
     */
    public static JTextField makeNumericalJTextField( Object l, 
													  String section,
													  String keyword,
													  String defval,
													  int defcols,
													  ResourceSet resource ) {
		return makeTextField( l, section, keyword, defval, defcols, resource,
							  true );
    }

    /**
     * Numerical Text field with optional action listener and document listener,
	 * to be defined in a resource file, with optional default text and
	 * length if the values are not in the resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param defval Optional text for the field
	 * @param defcols Length in columns of the field
	 *
	 * @return A JTextField
     */
    public static JTextField makeNumericalJTextField( Object l, 
													  String section,
													  String keyword,
													  String defval,
													  int defcols ) {
		return makeNumericalJTextField( l, section, keyword, defval, defcols,
										null );
	}

    /**
     * Numerical Text field with optional action listener and document listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JTextField
     */
    public static JTextField makeNumericalJTextField( Object l,
													  String section,
													  String keyword,
													  ResourceSet resource ) {
		return makeNumericalJTextField( l, section, keyword, null, -1,
										resource );
	}

    /**
     * Numerical Text field with optional action listener and document listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 *
	 * @return A JTextField
     */
    public static JTextField makeNumericalJTextField( Object l,
													  String section,
													  String keyword) {
		return makeNumericalJTextField( l, section, keyword,
										(ResourceSet)null );
	}

    /**
     * Password field with optional action listener and document listener
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
	 *
	 * @return A JPasswordField
     */
    public static JPasswordField makeJPasswordField( Object l ) {
        return makeJPasswordField( l, null, null, -1 );
    }

    /**
     * Password field with optional action listener and document listener,
	 * optional Document, optional text, and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param d Optional Document, may be null
     * @param s Optional text for the field
	 * @param len Length in columns of the field
	 *
	 * @return A JPasswordField
     */
    public static JPasswordField makeJPasswordField( Object l,
													 Document d,
													 String s,
													 int len) {
        JPasswordField pf = new JPasswordField();
		/* All text fields are supposed to have a 6 pixel inset */
		pf.setMargin( getTextInsets() );
        if (d != null)
            pf.setDocument(d);
        if (s != null)
            pf.setText(s);
        if (len != -1)
            pf.setColumns(len);

		if ( l != null ) {
			if ( l instanceof ActionListener )
				pf.addActionListener( (ActionListener)l );
			//detect text changes
			if ( l instanceof DocumentListener )
				pf.getDocument().addDocumentListener( (DocumentListener)l );
		}
        return pf;
    }

    /**
     * Password field with optional action listener and document listener,
	 * and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
	 * @param len Length in columns of the field
	 *
	 * @return A JPasswordField
     */
    public static JPasswordField makeJPasswordField( Object l,
													 int len ) {
        return makeJPasswordField( l, null, null, len );
    }

    /**
     * Password field with optional action listener and document listener,
	 * and optional text
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param s Optional text for the field
	 *
	 * @return A JPasswordField
     */
    public static JPasswordField makeJPasswordField( Object l,
													 String s ) {
        return makeJPasswordField( l, null, s, -1 );
    }

    /**
     * Password field with optional action listener and document listener,
	 * optional text, and optional length
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param s Optional text for the field
	 * @param len Length in columns of the field
	 *
	 * @return A JPasswordField
     */
    public static JPasswordField makeJPasswordField( Object l,
													 String s,
													 int len ) {
        return makeJPasswordField( l, null, s, len );
    }

    /**
     * Password field with optional action listener and document listener,
	 * to be defined in a resource file, with optional default text and
	 * length if the values are not in the resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param defval Optional text for the field
	 * @param defcols Length in columns of the field
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JPasswordField
     */
    public static JPasswordField makeJPasswordField( Object l, 
													 String section,
													 String keyword,
													 String defval,
													 int defcols,
													 ResourceSet resource ) {
		ResourceSet res = (resource != null) ? resource : _resource;
        String text = res.getString(section, keyword + "-default");
        if (text == null)
            text = defval;
        int columns = 0;
        try {
            columns = Integer.parseInt(
                res.getString(section, keyword + "-columns")
            );
        } catch (Exception e) {
            // if invalid integer, just use default
        }
        if (columns < 1)
            columns = defcols;
        JPasswordField jpf = makeJPasswordField( l, null, text, columns );
        setToolTip(section, keyword, jpf);

        return jpf;
    }

    /**
     * Password field with optional action listener and document listener,
	 * to be defined in a resource file, with optional default text and
	 * length if the values are not in the resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param defval Optional text for the field
	 * @param defcols Length in columns of the field
	 *
	 * @return A JPasswordField
     */
    public static JPasswordField makeJPasswordField( Object l, 
													 String section,
													 String keyword,
													 String defval,
													 int defcols ) {
        return makeJPasswordField( l, section, keyword, defval,
								   defcols, (ResourceSet)null );
	}

    /**
     * Button with optional ActionListener, optional icon and optional text
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
     * @param s Text for label, may be null
	 *
	 * @return a JButton
     */
    public static JButton makeJButton( Object l,
									   String s,
									   Icon i ) {
        JButton button = new JButton();
		/* ??? Need to make this smarter for resizing buttons in groups,
		 to a multiple of 18 pixels in width */
		Dimension d = new Dimension( 90, 23 );
		button.setPreferredSize( d );
        if (s != null) {
            button.setText(s);
			button.getAccessibleContext().setAccessibleDescription(s);
		}
        if (i != null)
            button.setIcon(i);

		if ( (l != null) && (l instanceof ActionListener) ) {
			button.addActionListener( (ActionListener)l );
		}
		JButtonFactory.resize( button );
        return button;
    }

    /**
     * Button with optional ActionListener
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
	 *
	 * @return a JButton
     */
    public static JButton makeJButton(  Object l ) {
		return makeJButton( l, null, (Icon)null );
	}

    /**
     * Button with optional ActionListener and optional text
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param s Text for label, may be null
	 *
	 * @return a JButton
     */
    public static JButton makeJButton( Object l,
									   String s ) {
        return makeJButton( l, s, (Icon)null );
    }

    /**
     * Button with optional ActionListener and optional icon
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
	 *
	 * @return a JButton
     */
    public static JButton makeJButton( Object l,
									   Icon i ) {
        return makeJButton( l, null, i );
    }

    /**
     * Button with optional action listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JButton
     */
    public static JButton makeJButton( Object l,
									   String section,
									   String keyword,
									   ResourceSet resource ) {
		ResourceSet res = (resource != null) ? resource : _resource;
        String label = res.getString(section, keyword + "-label");
        if (label == null)
            label = "label";
        JButton jb = makeJButton( l, label, (Icon)null );
        setToolTip(section, keyword, jb, resource);
		setMnemonic(section, keyword, jb, resource);

        return jb;
    }

    /**
     * Button with optional action listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 *
	 * @return A JButton
     */
    public static JButton makeJButton( Object l,
									   String section,
									   String keyword ) {
		return makeJButton( l, section, keyword, (ResourceSet)null );
	}

	/**
	 * Create a panel with horizontally arranged, equally sized buttons
	 * The buttons are aligned to the right in the panel (if it is
	 * stretched beyond the length of the buttons)
	 *
	 * @param buttons An array of buttons for the panel
	 * @param hasHelp true if the right-most button is Help, and so needs
	 * more space between it and the others
	 *
	 * @return A panel containing the buttons
	 */
    public static JPanel makeJButtonPanel( JButton[] buttons,
										   boolean hasHelp ) {
		JButtonFactory.resize( buttons );
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque( false );
		GridBagConstraints gbc = new GridBagConstraints();
		buttonPanel.setLayout(new GridBagLayout());
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		gbc.gridwidth = 1;
		buttonPanel.add( Box.createGlue(), gbc );
		gbc.fill = gbc.NONE;
		gbc.weightx = 0;
		int generalSpace = _componentSpace;
		int lastSpace = hasHelp ? _separatedSpace : _componentSpace;
		for( int i = 0; i < buttons.length; i++ ) {
			if ( i == buttons.length-1 )
				gbc.gridwidth = gbc.REMAINDER;
			buttonPanel.add( buttons[i], gbc );
			if ( i < buttons.length-2 )
				buttonPanel.add( Box.createHorizontalStrut(generalSpace) );
			else if ( i < buttons.length-1 )
				buttonPanel.add( Box.createHorizontalStrut(lastSpace) );
		}
		return buttonPanel;
	}

	/**
	 * Create a panel with horizontally arranged, equally sized buttons
	 * The buttons are aligned to the right in the panel (if it is
	 * stretched beyond the length of the buttons)
	 *
	 * @param buttons An array of buttons for the panel
	 *
	 * @return A panel containing the buttons
	 */
    public static JPanel makeJButtonPanel( JButton[] buttons ) {
		return makeJButtonPanel( buttons, false );
	}

	/**
	 * Resize buttons properly, using the longest one as the guide
	 *
	 * @param buttons An array of buttons for the panel
	 */
    public static void resizeButtons( JButton[] buttons ) {
		JButtonFactory.resize( buttons );
	}

    /**
     * Checkbox with optional ActionListener, optional icon, optional text,
	 * and initial state
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
     * @param s Text for label, may be null
	 * @param b true if the checkbox is to be selected
	 *
	 * @return a JCheckBox
     */
	public static JCheckBox makeJCheckBox( Object l,
										   String s,
										   Icon i,
										   boolean b ) {
        JCheckBox cb = new JCheckBox();
        if (s != null) {
            cb.setText(s);
			cb.getAccessibleContext().setAccessibleDescription(s);
		}
        if (i != null)
            cb.setIcon(i);
        cb.setSelected(b);
		if ( (l != null) && (l instanceof ActionListener) ) {
			cb.addActionListener( (ActionListener)l );
		}

        return cb;
    }

    /**
     * Checkbox with optional ActionListener, no text, no icon, deselected
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
	 *
	 * @return a JCheckBox
     */
    public static JCheckBox makeJCheckBox( Object l ) {
        return makeJCheckBox( l, null, (Icon)null, false );
    }

    /**
     * Checkbox with optional ActionListener, optional icon, no text,
	 * and deselected
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
	 *
	 * @return a JCheckBox
     */
    public static JCheckBox makeJCheckBox( Object l,
										   Icon i) {
        return makeJCheckBox( l, null, i, false );
    }

    /**
     * Checkbox with optional ActionListener, optional icon, no text,
	 * and initial state
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
	 * @param b true if the checkbox is to be selected
	 *
	 * @return a JCheckBox
     */
    public static JCheckBox makeJCheckBox( Object l,
										   Icon i,
										   boolean b ) {
        return makeJCheckBox( l, null, i, b );
    }

    /**
     * Checkbox with optional ActionListener, no icon, optional text,
	 * and deselected
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param s Text for label, may be null
	 *
	 * @return a JCheckBox
     */
    public static JCheckBox makeJCheckBox( Object l,
										   String s ) {
        return makeJCheckBox( l, s, (Icon)null, false );
    }

    /**
     * Checkbox with optional ActionListener, no icon, optional text,
	 * and initial state
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param s Text for label, may be null
	 * @param b true if the checkbox is to be selected
	 *
	 * @return a JCheckBox
     */
    public static JCheckBox makeJCheckBox( Object l,
										   String s,
										   boolean b ) {
        return makeJCheckBox( l, s, (Icon)null, b );
    }

    /**
     * Checkbox with optional ActionListener, optional icon, optional text,
	 * and deselected
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
     * @param s Text for label, may be null
	 *
	 * @return a JCheckBox
     */
    public static JCheckBox makeJCheckBox( Object l,
										   String s,
										   Icon i ) {
        return makeJCheckBox( l, s, i, false );
    }

    /**
     * Checkbox with optional action listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 * @param defval text for checkbox if not found in resource file
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JCheckBox
     */
    public static JCheckBox makeJCheckBox( Object l,
										   String section,
										   String keyword,
										   boolean defval,
										   ResourceSet resource ) {
		ResourceSet res = (resource != null) ? resource : _resource;
		String label = res.getString(section, keyword + "-label");
        if (label == null)
            label = "label";
        boolean value = false;
        try {
            value = Boolean.valueOf(
                res.getString(section, keyword + "-default")
            ).booleanValue();
        } catch (Exception e) {
            // if invalid boolean, just use default
            value = defval;
        }
        JCheckBox jcb = makeJCheckBox( l, label, (Icon)null, value );
        setToolTip(section, keyword, jcb, res);

        return jcb;
    }

    /**
     * Checkbox with optional action listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 * @param defval text for checkbox if not found in resource file
	 *
	 * @return A JCheckBox
     */
    public static JCheckBox makeJCheckBox( Object l,
										   String section,
										   String keyword,
										   boolean defval ) {
		return makeJCheckBox( l, section, keyword, defval,
							  (ResourceSet)null );
	}

    /**
     * Checkbox with optional action listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 *
	 * @return A JCheckBox
     */
    public static JCheckBox makeJCheckBox( Object l,
										   String section,
										   String keyword ) {
        return makeJCheckBox( l, section, keyword, false );
    }

    /**
     * Combobox with optional ItemListener
     *
     * @param l Object which may be an ActionListener, and/or an ItemListener,
	 * and may be null
     * @param cbm Model, may be null
	 *
	 * @return a JComboBox
     */
    public static JComboBox makeJComboBox( Object l,
										   ComboBoxModel cbm ) {
        JComboBox cb = new JComboBox();
        if (cbm != null)
            cb.setModel(cbm);
		if ( l != null ) {
			if ( l instanceof ItemListener ) {
				cb.addItemListener( (ItemListener)l );
			}
			if ( l instanceof ActionListener ) {
				cb.addActionListener( (ActionListener)l );
			}
		}

        return cb;
    }

    /**
     * Combobox with optional ItemListener
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
	 *
	 * @return a JComboBox
     */
    public static JComboBox makeJComboBox( ItemListener l ) {
		return makeJComboBox( l, null );
	}

    /**
     * Combobox with optional Item listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 * @param defval text for checkbox if not found in resource file
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JComboBox
     */
    public static JComboBox makeJComboBox( Object l,
										   String section,
										   String keyword,
										   String defval,
										   ResourceSet resource ) {
		ResourceSet res = (resource != null) ? resource : _resource;
        String value = res.getString(section, keyword + "-default");
        if (value == null)
            value = defval;
		JComboBox jcb = makeJComboBox( l, null );
        String val = null;
        int ii = 1;
        do {
            val = res.getString(section, keyword + "-" + ii);
            if (val != null) {
                jcb.addItem(val);
            }
            ++ii;
        } while (val != null);

        jcb.setSelectedItem(value);
        setToolTip(section, keyword, jcb, res);
		/* All text fields are supposed to have a 6 pixel inset */
		ComboBoxEditor ed = jcb.getEditor();
//		Debug.println( "BlankPanel.makeJComboBox: editor = " + ed );
		if ( ed != null ) {
			Component c = ed.getEditorComponent();
//			Debug.println( "BlankPanel.makeJComboBox: component = " + c );
			if ( c instanceof JTextField )
				((JTextField)c).setMargin( getTextInsets() );
		}
        return jcb;
    }

    /**
     * Combobox with optional action listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 * @param defval text for checkbox if not found in resource file
	 *
	 * @return A JComboBox
     */
    public static JComboBox makeJComboBox( Object l,
										   String section,
										   String keyword,
										   String defval ) {
		return makeJComboBox( l, section, keyword, defval,
							  (ResourceSet)null );
	}

    /**
     * List with optional selection listener
     *
     * @param l Object which may be a ListSelectionListener,
	 * and may be null
     * @param listData Model, may be null
	 *
	 * @return A JList
     */
    public static JList makeJList( Object l,
								   Vector listData) {
        JList li = new JList(listData);
		if ( l != null ) {
			if ( l instanceof ListSelectionListener ) {
				li.addListSelectionListener( (ListSelectionListener)l );
			}
		}
        return li;
    }

    /**
     * List with optional selection listener
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param listData Model, may be null
	 *
	 * @return A JList
     */
    public static JList makeJList( Object l,
								   ListModel listData) {
        JList li = new JList(listData);
		if ( (l != null) && (l instanceof ListSelectionListener) ) {
			li.addListSelectionListener( (ListSelectionListener)l );
		}
        return li;
    }

    /**
     * List with optional selection listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 * @param defval text for list if not found in resource file
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JList
     */
    public static JList makeJList( Object l,
								   String section,
								   String keyword,
								   String defval,
								   ResourceSet resource ) {
		ResourceSet res = (resource != null) ? resource : _resource;
        String value = res.getString(section, keyword + "-default");
        if (value == null)
            value = defval;
        String val = null;
        Vector listData = new Vector();
        int ii = 1;
        do {
            val = res.getString( section, keyword + "-" + ii );
            if (val != null) {
                listData.addElement(val);
            }
            ++ii;
        } while (val != null);

        JList jl = makeJList( l, listData );
        setToolTip(section, keyword, jl, resource);
        return jl;
    }

    /**
     * List with optional selection listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener and/or a
	 * DocumentListener, and may be null
     * @param section from properties file
     * @param keyword from properties file
	 * @param defval text for list if not found in resource file
	 *
	 * @return A JList
     */
    public static JList makeJList( Object l,
								   String section,
								   String keyword,
								   String defval ) {
		return makeJList( l, section, keyword, defval, (ResourceSet)null );
	}

    /**
     * Radio button with optional ActionListener, optional icon, optional text,
	 * and initial state
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
     * @param s Text for label, may be null
	 * @param b true if the button is to be selected
	 *
	 * @return a JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l,
												 String s,
												 Icon i,
												 boolean b ) {
        JRadioButton rb = new JRadioButton();
        if (s != null)
            rb.setText(s);
        if (i != null)
            rb.setIcon(i);
        rb.setSelected(b);
		if ( l != null ) {
			if ( l instanceof ActionListener ) {
				rb.addActionListener( (ActionListener)l );
				rb.getModel().addActionListener( (ActionListener)l );
			}
			if ( l instanceof ChangeListener ) {
				rb.addChangeListener( (ChangeListener)l );
			}
			if ( l instanceof ItemListener ) {
				rb.addItemListener( (ItemListener)l );
			}
		}

        return rb;
    }

    /**
     * Radio button with optional ActionListener, no icon, no text,
	 * and deselected
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
	 *
	 * @return a JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l ) {
        return makeJRadioButton( l, null, (Icon)null, false );
    }

    /**
     * Radio button with optional ActionListener, optional icon, no text,
	 * and deselected
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
	 *
	 * @return a JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l,
												 Icon i ) {
        return makeJRadioButton( l, null, i, false );
    }

    /**
     * Radio button with optional ActionListener, optional icon, no text,
	 * and initial state
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
	 * @param b true if the button is to be selected
	 *
	 * @return a JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l,
												 Icon i,
												 boolean b ) {
        return makeJRadioButton( l, null, i, b );
    }

    /**
     * Radio button with optional ActionListener, no icon, optional text,
	 * and deselected
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param s Text for label, may be null
	 *
	 * @return a JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l,
												 String s) {
        return makeJRadioButton( l, s, (Icon)null, false );
    }

    /**
     * Radio button with optional ActionListener, no icon, optional text,
	 * and optional selection state
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param s Text for label, may be null
	 * @param b true if the button is to be selected
	 *
	 * @return a JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l,
												 String s,
												 boolean b ) {
        return makeJRadioButton( l, s, (Icon)null, b );
    }

    /**
     * Radio button with optional ActionListener, optional icon, optional text,
	 * and deselected
     *
     * @param l Object which may be an ActionListener,
	 * and may be null
     * @param i Icon, may be null
     * @param s Text for label, may be null
	 *
	 * @return a JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l,
												 String s,
												 Icon i ) {
        return makeJRadioButton( l, s, i, false );
    }

    /**
     * Radiobutton with optional action listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener
     * @param section from properties file
     * @param keyword from properties file
	 * @param defval state for radiobutton if not found in resource file
     * @param resource ResourceSet to get value from, may be null
	 *
	 * @return A JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l,
												 String section,
												 String keyword,
												 boolean defvalue,
												 ResourceSet resource ) {
		ResourceSet res = (resource != null) ? resource : _resource;
		String label = res.getString(section, keyword + "-label");
        if (label == null)
            label = "label";
        boolean value = defvalue;
        try {
            value = Boolean.valueOf(
                res.getString(section, keyword + "-default")
            ).booleanValue();
        } catch (Exception e) {
            // if invalid boolean, just use default
        }
        JRadioButton jrb = makeJRadioButton( l, label, (Icon)null, value );
        setToolTip(section, keyword, jrb, resource);

        return jrb;
    }

    /**
     * Radiobutton with optional action listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener
     * @param section from properties file
     * @param keyword from properties file
	 *
	 * @return A JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l,
												 String section,
												 String keyword ) {
        return makeJRadioButton( l, section, keyword, false );
    }

    /**
     * Radiobutton with optional action listener,
	 * to be defined in a resource file
     *
     * @param l Object which may be an ActionListener
     * @param section from properties file
     * @param keyword from properties file
	 * @param defval state for radiobutton if not found in resource file
	 *
	 * @return A JRadioButton
     */
    public static JRadioButton makeJRadioButton( Object l,
												 String section,
												 String keyword,
												 boolean defvalue ) {
		return makeJRadioButton( l, section, keyword, defvalue,
								 (ResourceSet)null );
	}

    /**
     * Convenience for setting tool tip text from resource file
     *
     * @param section from properties file
     * @param keyword from properties file
     * @param w widget to set tool tip for
	 * @param resource ResourceSet to get value from
	 *
	 * @return JToolTip
     */
    public static void setToolTip( String section,
								   String keyword,
								   JComponent w,
								   ResourceSet resource ) {
		ResourceSet res = (resource != null) ? resource : _resource;
        String ttip = res.getString(section, keyword + "-ttip");
        if (ttip != null) {
            w.setToolTipText(ttip);
        }

        return;
    }

    /**
     * Convenience for setting tool tip text from resource file
     *
     * @param section from properties file
     * @param keyword from properties file
     * @param w widget to set tool tip for
	 *
	 * @return JToolTip
     */
    public static void setToolTip( String section,
								   String keyword,
								   JComponent w ) {
		setToolTip( section, keyword, w, (ResourceSet)null );
	}

 

    /**
     * Convenience for setting tool tip text, which may be null
     *
     * @param s Optional text
	 *
	 * @return JToolTip
     */
    public static JToolTip makeJToolTip( String s ) {
        JToolTip tt = new JToolTip();
        if (s != null)
            tt.setTipText(s);

        return tt;
    }
    /**
     * Convenience for setting empty tool tip text
     *
	 * @return JToolTip
     */
    public static JToolTip makeJToolTip() {
		return makeJToolTip(null);
	}

    /**
     * Convenience for setting mnemonics from resource file
     *
     * @param section from properties file
     * @param keyword from properties file
     * @param w widget to set tool tip for
	 * @param resource ResourceSet to get value from
	 *
	 * @return 
     */
    public static void setMnemonic( String section,
								   String keyword,
								   JButton button,
								   ResourceSet resource ) {
		ResourceSet res = (resource != null) ? resource : _resource;
        String mnemonic = res.getString(section, keyword+"-mnemonic");
		if ((mnemonic != null) &&
			(mnemonic.length() > 0)) {
			button.setMnemonic(mnemonic.charAt(0));
		}
    }

	/**
	 * Make a JTextField reject double-byte characters
	 *
	 * @param state true to reject double-byte characters
	 */
	public static void setSingleByte( JTextField field, boolean state ) { 
		if ( state ) {
			field.setDocument( new SingleByteDocument( field ) );
		} else {
			field.setDocument( new JTextField().getDocument() );
		}
	}

	/**
	 * Set the default ResourceSet for lookups
	 *
	 * @param resource An open/active ResourceSet
	 */
    public static void setResource( ResourceSet resource ) {
		_resource = resource;
	}

	private static boolean initialized = initialize();
    public static Insets getStdInsets() {return _stdInsets;}

    public static Insets getBorderInsets() {return _borderInsets;}

    public static Insets getComponentInsets() {return _componentInsets;}

    public static Insets getTextInsets() {return _textInsets;}

    public static int getComponentSpace() {return _componentSpace;}

    public static int getDifferentSpace() {return _differentSpace;}

    public static int getSeparatedSpace() {return _separatedSpace;}

    /*==========================================================
     * variables
     *==========================================================*/   
    private static Insets _stdInsets;  // Border insets for typical panel
                                    // with 10 or less controls
    private static Insets _borderInsets;  // Border insets for panel that needs
                                    // maximum area i.e. a grid panel
                            // insets for spaces between components
    private static Insets _componentInsets;
                            // insets insided a text field
    private static Insets _textInsets = new Insets( 0, 3, 0, 3 );
	private static int _componentSpace;
	private static int _differentSpace;
	private static int _separatedSpace;
	private static Color _tfEnabledColor = null;
	private static Color _tfDisabledColor = null;

	private static ResourceSet _resource = DSUtil._resource;
}
