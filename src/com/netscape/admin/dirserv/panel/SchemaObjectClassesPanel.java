/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.Framework;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.UITools;
import netscape.ldap.*;
import java.util.Hashtable;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class SchemaObjectClassesPanel extends BlankPanel
                                      implements ListSelectionListener {

    public SchemaObjectClassesPanel(IDSModel model) {
		super(model, _section, false);
		_helpToken = "configuration-schema-objclass-help";
		_refreshWhenSelect = false;
	}
 
    public void init() {
		((Framework)getModel().getFrame()).setBusyCursor( true );
		if ( OID_UNKNOWN_STR == null )
			OID_UNKNOWN_STR =
				DSUtil._resource.getString(_section,"unknown-label");
        if ( PARENT_UNKNOWN_STR == null )
			PARENT_UNKNOWN_STR =
				DSUtil._resource.getString(_section,"noParent");

		_ocModel = new DefaultListModel();
		_reqAttrModel = new DefaultListModel();
		_allowAttrModel = new DefaultListModel();

        _ld = getModel().getServerInfo().getLDAPConnection();
		updateTables();

		createLayout();
		((Framework)getModel().getFrame()).setBusyCursor(false);
	}

	private void createLayout() {
		int space = UIFactory.getComponentSpace();
		int different = UIFactory.getDifferentSpace();
		_myPanel.setBorder( new EmptyBorder(getBorderInsets()) );
		_myPanel.setLayout( new GridBagLayout() );

		JLabel introLabel = makeJLabel(_section, "intro");
		introLabel.setLabelFor(this);

		Border emptyBorder = new EmptyBorder( 0, 0, 0, 0 );
		JPanel panel1 = new JPanel();
		panel1.setBorder( emptyBorder );
		panel1.setLayout(new BoxLayout( panel1, BoxLayout.Y_AXIS ));
		JLabel lObjectclass = UIFactory.makeJLabel( _section,
										  "objectclasses" );
		panel1.add( lObjectclass );
		JPanel panel2 = new JPanel();
		panel2.setLayout(new BoxLayout( panel2, BoxLayout.Y_AXIS ));
		JLabel lRequiredAttributes = UIFactory.makeJLabel( _section,
										  "reqAttributesList" );
		panel2.add( lRequiredAttributes );
		panel2.setBorder( emptyBorder );
		JPanel panel3 = new JPanel();
		panel3.setLayout(new BoxLayout( panel3, BoxLayout.Y_AXIS ));
		JLabel lAllowAttributes = UIFactory.makeJLabel( _section,
										  "allowAttributesList" );
		panel3.add( lAllowAttributes );
		panel3.setBorder( emptyBorder );

		_ocListBox = createListBox(_ocModel, 2);
		lObjectclass.setLabelFor(_ocListBox);
		_ocListBox.addMouseListener(
			new MouseAdapter() {
			    public void mouseClicked(MouseEvent me) {
					if ( DSUtil.isStandardSchema(_currOCEntry) ) {
						return;
					}
					if (me.getClickCount() == 2) {
						LDAPSchema schema = getModel().getSchema();
						ObjectClassDialog dialog =
							new ObjectClassDialog( getModel(), true, 
												   _currOCEntry, schema );
						if (dialog != null)			
						    dialog.packAndShow();
					}
				}
		    }
		);

		_ocListBox.clearSelection();
		_reqListBox = createListBox(_reqAttrModel, 1);
		lRequiredAttributes.setLabelFor(_reqListBox);
		_allowListBox = createListBox(_allowAttrModel, 2);
		lAllowAttributes.setLabelFor(_allowListBox);

		_ocListBox.addListSelectionListener(this);

		panel1.add(createScrollPane(_ocListBox));
		panel2.add(createScrollPane(_reqListBox));
		panel3.add(createScrollPane(_allowListBox));

		JPanel buttonsPanel = createButtonsPanel();
		JPanel textPanel = createTextArea();
		JPanel attrPanel = createAttrPanel(panel2, panel3);

		resetGBC();
		_gbc.gridwidth = _gbc.REMAINDER;
		_gbc.weightx = 1.0;
		_gbc.fill = _gbc.HORIZONTAL;
		_gbc.insets = new Insets( 0, 0, different, 0 );
		_myPanel.add(introLabel, _gbc);
		_myPanel.add(textPanel, _gbc);

		_gbc.gridwidth = _gbc.RELATIVE;
		_gbc.weighty = 1.0;
		_gbc.fill = _gbc.BOTH;
		_gbc.insets = new Insets( 0, 0, 0, 0 );
		_myPanel.add(panel1, _gbc);

		_gbc.gridwidth = _gbc.REMAINDER;
		_gbc.insets.left = different;
		_myPanel.add(attrPanel, _gbc);

		resetGBC();
		_gbc.weightx = 1.0;
		_gbc.insets = new Insets(different, 0, 0, 0);
		_gbc.fill = _gbc.HORIZONTAL;
		_gbc.gridwidth = _gbc.REMAINDER;
		_myPanel.add(buttonsPanel, _gbc);

		_ocListBox.setSelectedIndex( 0 );
	}

    /**
      * Called when the object is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
        Debug.println( "SchemaObjectClassesPanel.select " );
        if (!_isInitialized) { // first time called
            init();
            _isInitialized = true;
			clearDirtyFlag();
        } else if (isDirty()) { // not first time called, don't show()
            return;
        }
    }

    /**
     * Update on-screen data from Directory.
     *
     **/
    public boolean refresh () {
		Debug.println( "SchemaObjectClasses.refresh" );
		((Framework)getModel().getFrame()).setBusyCursor(true);
		/* Force refetch */
		getModel().setSchema( null );
		updateTables();
		((Framework)getModel().getFrame()).setBusyCursor(false);
		return true;
	}

    private void updateTables() {
		LDAPSchema schema = getModel().getSchema();
		if ( schema == null ) {
			Debug.println( "SchemaObjectClassesPanel.updateTables: " +
						   "no schema available" );
			return;
		}
		_ocModel.removeAllElements();
		Enumeration enumObjclasses = schema.getObjectClassNames();
		while (enumObjclasses.hasMoreElements()) {
			SchemaUtility.InsertElement(_ocModel, enumObjclasses.nextElement());
		}
	}

    public void actionPerformed(ActionEvent e) {		
		Object source = e.getSource();
		String str = (String)(_ocListBox.getSelectedValue());
		ObjectClassDialog dialog = null;
		((Framework)getModel().getFrame()).setBusyCursor(true);		
		if (source.equals(_createButton)) {			
			LDAPSchema schema = getModel().getSchema();					
			dialog = new ObjectClassDialog( getModel(), false, schema );			
		} else if ((source.equals(_editButton)) && (str != null)) {			
			LDAPSchema schema = getModel().getSchema();			
			dialog = new ObjectClassDialog( getModel(), true, _currOCEntry,
											schema );			
		} else if ((source.equals(_deleteButton)) && (str != null)) {
			int response = JOptionPane.YES_OPTION;
			boolean confirm = requiresConfirmation(
				GlobalConstants.PREFERENCES_CONFIRM_DELETE_OBJECTCLASS );
			if ( confirm ) {
				response = DSUtil.showConfirmationDialog(
					getModel().getFrame(),
					"confirm-delete",
					str,
					_section );
			}
			
			while( response == JOptionPane.YES_OPTION ) {
				try {							
					confirmUpdate(str);
					refresh();						
					// JList wont fire the fireSelection event if the index doesnt get
					// changed. First highlight something else and then highlight the 
					// correct one.
					_ocListBox.setSelectedIndex(0);					
					return;
				} catch (LDAPException ex) {
					((Framework)getModel().getFrame()).setBusyCursor(false);
					if ( ex.getLDAPResultCode() ==
						 LDAPException.INSUFFICIENT_ACCESS_RIGHTS) {
						// display a message
						DSUtil.showPermissionDialog( getModel().getFrame(),
													 _ld );
						if (!getModel().getNewAuthentication(false)) {								
							return; // could not modify entry
						}
					} else {
						/* Some other error. Just show a dialog. */
						DSUtil.showLDAPErrorDialog( getModel().getFrame(),
													ex, "111-title" );							
						return;
					}
				}				
			} 
			getModel().notifyAuthChangeListeners();			
        } else if (source.equals(_helpButton)) {
			helpCallback();
		}		
		if (dialog != null) {			
			dialog.packAndShow();			
			str = dialog.getObject();
			if ( str == null ) {
				((Framework)getModel().getFrame()).setBusyCursor(false);
				return;
			}
			refresh();

			// JList wont fire the fireSelection event if the index doesnt get
			// changed. First highlight something else and then highlight the 
			// correct one.
			_ocListBox.setSelectedValue("top", false);
			_ocListBox.setSelectedValue(str, true);
		}		
		((Framework)getModel().getFrame()).setBusyCursor(false);
	}

    private void confirmUpdate(String str) throws LDAPException {
		Debug.println( "SchemaObjectClasses.confirmUpdate: deleting " +
					   _currOCEntry.getValue() );
        _currOCEntry.remove(_ld);
		DSUtil.showInformationDialog( getModel().getFrame(),
									  "deleted", str, _section );
        _ocModel.removeElement(str);
    }
 
    public void removeUpdate(DocumentEvent e) {
    }

    public void insertUpdate(DocumentEvent e) {
    }

    public void valueChanged(ListSelectionEvent e) {
		Object source = e.getSource();

		if (source.equals(_ocListBox)) {
			if ( _ocModel.size() < 1 ) {
				return;
			}
			String ocName = (String)(_ocListBox.getSelectedValue());
    
			if (ocName == null) {
				_reqAttrModel.removeAllElements();
				_allowAttrModel.removeAllElements();
				_oidText.setText("");
				_parentText.setText("");
				return;
			}

			LDAPSchema schema = getModel().getSchema();
			_currOCEntry = schema.getObjectClass(ocName);
			enableButtons( !DSUtil.isStandardSchema(_currOCEntry) );
			String oid = _currOCEntry.getID();
			String[] superiors = DSSchemaHelper.getSuperiors(_currOCEntry);

			if (oid.length() == 0)
				oid = OID_UNKNOWN_STR;

			if (superiors==null) {
				superiors = new String[1];
				superiors[0] = PARENT_UNKNOWN_STR;
			} else if (superiors.length ==0) {
				superiors = new String[1];
				superiors[0] = PARENT_UNKNOWN_STR;
			} else if (superiors[0] == null) {
				superiors = new String[1];
				superiors[0] = PARENT_UNKNOWN_STR;
			} else if (superiors[0].length() == 0) {
				superiors = new String[1];
				superiors[0] = PARENT_UNKNOWN_STR;
			}

			/* We construct an string with the list of superiors for this objectclass*/
			String textForSuperiors = superiors[0];
			for (int i=1; i<superiors.length; i++) {
				textForSuperiors = ", " + superiors[i];
			}
			
			/* Depending on the number of superiors (one or many) we display one label or other */
			if (superiors.length > 1) {
				_parentLabel.setText(DSUtil._resource.getString(_section, "parents-label"));
			} else {
				_parentLabel.setText(DSUtil._resource.getString(_section, "parent-label"));
			}

			_oidText.setText(oid);
			_parentText.setText(textForSuperiors);

			Hashtable ht = new Hashtable();
			DSSchemaHelper.allOptionalAttributesWithCase(_currOCEntry, schema, ht);
			Enumeration enumAllow = ht.elements();
			ht = new Hashtable();
			DSSchemaHelper.allRequiredAttributesWithCase(_currOCEntry, schema, ht);
			Enumeration enumReq = ht.elements();			
		
			_reqAttrModel.removeAllElements();
			_allowAttrModel.removeAllElements();

			while (enumReq.hasMoreElements()) {
				SchemaUtility.InsertElement(_reqAttrModel, 
											enumReq.nextElement());
			}
     
			while (enumAllow.hasMoreElements()) {
				Object o = enumAllow.nextElement();
				// users may redefine inherited allowed attributes as
				// required attributes
				if (!_reqAttrModel.contains(o)) {
					SchemaUtility.InsertElement(_allowAttrModel, o);
				}												
			} 
		}
	}

    private void enableButtons( boolean isUserDefined ) {
		_editButton.setEnabled( isUserDefined );
		_deleteButton.setEnabled( isUserDefined );
	}

    private JPanel createButtonsPanel() {
        _createButton = makeJButton(_section, "create");
        _editButton = makeJButton(_section, "edit");
        _deleteButton = makeJButton(_section, "delete");
        _helpButton = makeJButton("general", "Help");

		JButton[] buttons = { _createButton, _editButton,
							  _deleteButton };
		UIFactory.resizeButtons( buttons );
		JPanel buttonPanel = new JPanel();
		buttonPanel.setOpaque( false );
		GridBagConstraints gbc = new GridBagConstraints();
		buttonPanel.setLayout(new GridBagLayout());
		gbc.gridwidth = 1;
		int generalSpace = UIFactory.getComponentSpace();
		for( int i = 0; i < buttons.length; i++ ) {
			buttonPanel.add( buttons[i], gbc );
			buttonPanel.add( Box.createHorizontalStrut(generalSpace) );
		}
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;
		buttonPanel.add( Box.createGlue(), gbc );
		gbc.fill = gbc.NONE;
		gbc.weightx = 0;
		buttonPanel.add( _helpButton, gbc );
		return buttonPanel;
	}

    private JPanel createAttrPanel(JPanel j1, JPanel j2) {
		JPanel p = new JPanel();
		p.setLayout( new GridBagLayout() );

		resetGBC();
		_gbc.insets.top = 0;
		_gbc.anchor = _gbc.NORTH;
		_gbc.fill = _gbc.BOTH;
		_gbc.weightx = 1.0;
		_gbc.weighty = 1.0;
		p.add(j1, _gbc);

		_gbc.insets.top = UIFactory.getDifferentSpace();
		_gbc.gridx = 0;
		p.add(j2, _gbc);
		return p;
	}

    private JPanel createTextArea() {
		int space = UIFactory.getComponentSpace();
		int different = UIFactory.getDifferentSpace();
		JPanel p = new JPanel();
		p.setBorder(new EmptyBorder(0, 0, 0, 0));
		p.setLayout( new GridBagLayout() );

		JLabel oidLabel = makeJLabel(_section, "oid");
		_parentLabel = makeJLabel(_section, "parent");
		_oidText = makeJTextField();
		oidLabel.setLabelFor(_oidText);
		_oidText.setEditable(false); 
		_parentText = makeJTextField();
		_parentLabel.setLabelFor(_parentText);
		_parentText.setEditable(false);

		resetGBC();
		_gbc.gridx = 0;
		_gbc.gridwidth = _gbc.RELATIVE;
		_gbc.weightx = 0.0;
		_gbc.fill = _gbc.NONE;
		_gbc.insets = new Insets(0, 0, 0, space);
		p.add(_parentLabel, _gbc);

		_gbc.gridx = 1;
		_gbc.gridwidth = _gbc.REMAINDER;
		_gbc.insets.right = 0;
		_gbc.weightx = 1.0;
		_gbc.fill = _gbc.HORIZONTAL;
		p.add(_parentText, _gbc);

		_gbc.gridx = 0;
		_gbc.gridwidth = _gbc.RELATIVE;
		_gbc.weightx = 0.0;
		_gbc.fill = _gbc.NONE;
		_gbc.insets.right = space;
		_gbc.insets.top = different;
		p.add(oidLabel, _gbc);

		_gbc.gridx = 1;
		_gbc.gridwidth = _gbc.REMAINDER;
		_gbc.insets.right = 0;
		_gbc.weightx = 1.0;
		_gbc.fill = _gbc.HORIZONTAL;
		p.add(_oidText, _gbc);

		return p;
	}

    private JList createListBox(DefaultListModel listModel, int visibleCount) {
		JList listbox = new JList(listModel);
		listbox.setCellRenderer(new OClassesCellRenderer());
		listbox.setPrototypeCellValue("1234567890 1234567890");
		listbox.setVisibleRowCount(visibleCount);

		return listbox;
	}

    private JScrollPane createScrollPane(JList listbox) {
		JScrollPane scrollPane =
			new JScrollPane(listbox,
							JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
							JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		scrollPane.setAlignmentX(LEFT_ALIGNMENT);
		scrollPane.setAlignmentY(TOP_ALIGNMENT);
		scrollPane.setBorder(UITools.createLoweredBorder());
		
		return scrollPane;
	}
										  
    private JLabel _parentLabel;
    private JList _ocListBox;
    private JList _reqListBox;
    private JList _allowListBox;
    private JTextField _oidText;
    private JTextField _parentText;
    private JButton _createButton;
    private JButton _deleteButton;
    private JButton _editButton;
    private JButton _helpButton;
    private DefaultListModel _ocModel;
    private DefaultListModel _reqAttrModel;
    private DefaultListModel _allowAttrModel;
    private LDAPConnection _ld;
    private LDAPObjectClassSchema _currOCEntry;
    private static String OID_UNKNOWN_STR = null;
    private static String PARENT_UNKNOWN_STR = null;
    private static final String _section = "objectclasses";
}

class OClassesCellRenderer extends DefaultListCellRenderer {
    OClassesCellRenderer() {
        setBorder(noFocusBorder = new EmptyBorder(0,3,0,3));
    }
}
