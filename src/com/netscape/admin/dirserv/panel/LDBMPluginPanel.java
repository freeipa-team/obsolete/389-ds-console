/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import com.netscape.admin.dirserv.IDSModel;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class LDBMPluginPanel extends DSTabbedPanel {

	public LDBMPluginPanel(IDSModel model, LDAPEntry dbpEntry) {
		super( model );

		addTab( _indexTab = 
				new DefaultIndexManagementPanel( model, 
										  DSUtil.DEFAULT_LDBM_INDEX_PREFIX +
										  ", " + 
										  dbpEntry.getDN()));
		addTab( _performanceTab = 
				new LDBMPluginSettingPanel( model,
											"cn=config," +
											dbpEntry.getDN()));


		_tabbedPane.setSelectedIndex( 0 );
	}

	private BlankPanel _indexTab;
	private BlankPanel _passwordTab;
	private BlankPanel _performanceTab;
	private BlankPanel _settingsTab;
}
