/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.Vector;
import java.util.Enumeration;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.AbstractButton;

import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.*;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server
 *
 * @version
 * @author
 */
class DSEntryBitList extends DSEntry {
    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle list(box) entries that are localized.  The first parameter is an array of
     * strings that are used to update the directory server and corresponds to
     * the localized list entries.
     *
     * @param listEntries a string array of valid LDAP entries that have been localized in the view
     * @param view the JComboBox which stores localized entries that need to be replaced with standard LDAP syntax
     */
    DSEntryBitList(
        JList view ) {
        super((String)null, view);
		for( int i = 0; i < 32; i++ ) {
			_redirect.addElement( new Integer(1 << i) );
		}
    }

    DSEntryBitList(
        JList view, int[] masks ) {
        super((String)null, view);
		for( int i = 0; i < masks.length; i++ ) {
			_redirect.addElement( new Integer(masks[i]) );
		}
    }

    /**
     * First, get the model as an integer.  If the model is not an integer,
     * just set the value to 0.  Next, convert the value using the given
     * scale factor.  Last, update the view.
     */
    public void show() {
        // update the view
		JList lb = (JList)getView(0);
		if ( getModelSize() > 0 ) {
			_bitMask = Integer.parseInt( getModel( 0 ) );
		}
		Vector v = new Vector();
		int nBits = lb.getModel().getSize();
		int mask = 1;
		for( int i = 0; i < 32; i++ ) {
			if( (mask & _bitMask) != 0 ) {
				int ind = _redirect.indexOf( new Integer( mask ) );
				if ( ind >= 0 ) {
					v.addElement( new Integer( ind ) );
				} else {
					Debug.println( "DSEntryBitList.show: no known mask " +
								   Integer.toHexString( mask ) );
				}
			}
			mask <<= 1;
		}

		int size = v.size();
		int indices[] = new int[size];
		for( int i = 0; i < size; i++ ) {
		    indices[i] = ((Integer)v.elementAt( i )).intValue();
		}
		
   		lb.setSelectedIndices(indices);

        viewInitialized ();
	}

	/**
	 * Since model is update dynamically every time selection
     * is changed, store does not have to do anything
     */

	public void store() {
	    
    }

	/**
     * Updates the model with currently selected items in the listbox
     * Remove all the old entries before updating the model
     */
	protected void updateModel () {
		JList lb = (JList)getView(0);
	    clearModel();
	    int selected[] = lb.getSelectedIndices();
	    int ii = 0;
		_bitMask = 0;
	    while (ii < selected.length) {
			Integer ind = (Integer)_redirect.elementAt(selected[ii]);
			_bitMask |= ind.intValue();
            ii++;
        }
		setModelAt( Integer.toString( _bitMask ), 0 );
	}

    /**
     *
     * @return false if the text field does not contain a valid integer
     */
	public int validate() {
	    return 0;
	}

    private Vector _listEntries = null; // a Vector of Strings
	private int _bitMask = 0;
	private Vector _redirect = new Vector();
}
