/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Vector;
import java.util.Enumeration;
import javax.swing.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;

/**
 *	Panel for image display
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	8/24/98
 * @see     com.netscape.admin.dirserv
 */
public class GalleryPanel extends BlankPanel {

	public GalleryPanel(IDSModel model ) {
		super(model, _section, false);
		setOpaque( false );
	}

	public void init() {
		remove( _myPanel );
        Vector v = new Vector();
        try {
			getModel().setWaitCursor( true );
			final String file = "com/netscape/admin/dirserv/also/files.lst";
            InputStream is =
				getClass().getClassLoader().getResourceAsStream( file );
			if ( is == null ) {
				Debug.println( "GalleryPanel.init: could not open " + file );
			} else {
				String s;
				BufferedReader reader =
					new BufferedReader( new InputStreamReader( is ) );
				while( (s = reader.readLine()) != null ) {
					v.addElement( s );
				}
			}
		} catch ( Exception e ) {
		    Debug.println( "GalleryPanel.init: " + e );
		}
		_images = new ImageIcon[v.size()];
		Enumeration en = v.elements();
		String path = "";
		int i = 0;
		try {
			while( en.hasMoreElements() ) {
				path = _sImageDir + "/" + (String)en.nextElement();
				InputStream is =
					getClass().getClassLoader().getResourceAsStream( path );
				byte[] buf = new byte[is.available()];
				is.read(buf);
				Image img = Toolkit.getDefaultToolkit().createImage(buf);
				_images[i++] = new ImageIcon( img );
			}
		} catch ( Exception e ) {
			System.err.println( "GalleryPanel: reading " + path +
								", " + e );
			_images = new ImageIcon[0];
		} finally {
			getModel().setWaitCursor( false );
		}
		initCoords();
		_animator = new Timer(50, this);
		addMouseListener( new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				int mods = e.getModifiers();
				int check = e.SHIFT_MASK | e.CTRL_MASK;
				boolean animate = ( ((mods & check) != 0) &&
					(e.getX() < 8) && (e.getY() < 8) );
// 				Debug.println( "GalleryPanel.mousePressed: " + e.getX() +
// 							   ", " + e.getY() + ", " +
// 							   ((mods & check) != 0) );
				setAnimation( animate );
			}
		});
		_isInitialized = true;
	}

	private void setAnimation( boolean animate ) {
		if ( animate && !_animate ) {
			_animator.start();
		} else if ( !animate && _animate ) {
			_animator.stop();
			repaint();
		}
		_animate = animate;
	}

	private void initCoords() {
		int margin = getWidth() / 4;
		_x = ((int)(Math.random() * getWidth() / 2) & 0xfffffffe) + margin;
		margin = getHeight() / 4;
		_y = ((int)(Math.random() * getHeight() / 2) & 0xfffffffe) + margin;
// 		ImageIcon image = _images[_index];
// 		int dim = Math.max( image.getIconWidth(),
// 							image.getIconHeight() );
// 		_scale = 20.0 / Math.max( 20, dim );
		_scale = 0.25;
	}

    /**
      * Called when the object is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
		Debug.println( "GalleryPanel.select" );
		_selected = true;
        if (!_isInitialized) { // first time called
			init();
		}
	}

    /**
      * Called when the object is unselected.
      */
    public void unselect(IResourceObject parent, IPage viewInstance) {
		_selected = false;
		setAnimation( false );
	}

	public void paint(Graphics g) {
		if ( _animate ) {
			g.setColor( Color.black );
		} else {
			g.setColor( getParent().getBackground() );
		}
	    g.fillRect(0, 0, getWidth(), getHeight());
		if ( !_animate || (_images.length < 1) ) {
			return;
		}
		ImageIcon image = _images[_index];
		int w = (int)(image.getIconWidth()*_scale);
		int h = (int)(image.getIconHeight()*_scale);
		int x = _x - w / 2;
		int y = _y - h / 2;
		g.drawImage( image.getImage(), x, y, w, h, this );
		if (
// 			 ((x + w) >= getWidth()) || (x < 0) ||
// 			 ((y + h) >= getHeight()) || (y < 0) ||
			 (w > getWidth()) || (h > getHeight()) ||
			 (_scale >= 4.0) ) {
			_index++;
			if ( _index >= _images.length ) {
				_index = 0;
			}
			initCoords();
		} else {
			_scale += 0.05;
		}
	}

	public void actionPerformed(ActionEvent e) {
	    if( _animate ) {
			repaint();
	    }
	}

	private boolean _animate = false;
    private Timer _animator = null;
    private boolean _selected = false;
	private ImageIcon[] _images;
	// Center coordinates
	private int _x;
	private int _y;
	private double _scale;
	private int _index = 0;
	private static ResourceSet _resource = DSUtil._resource;
    private static final String _section = "imagegallery";
	private static final String  _sImageDir	=
    	"com/netscape/admin/dirserv/also"; // Images live here
}

