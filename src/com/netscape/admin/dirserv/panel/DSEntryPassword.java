/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import javax.swing.JPasswordField;
import javax.swing.JLabel;
import java.util.Enumeration;

/* This class implements password part of the password/confirm pair
   of fields. It is responsible for updating data of the confirm field
   as well as validation/modification of its own field. It should be used
   in combination with DSEntryConfirmPassword class which implements the
   confirm part.

   Since confirm field does not perform data exchange with the
   server, this class invokes data exchange function for the confirm class
   to insure that the confirm class contains correct state for label coloring
   and button enabling.
 */

public class DSEntryPassword extends DSEntryConfirmPassword{

    /**
     * This method constructs the DSEntryPassword object
     *
     * @param model - initial entry model
     * @param pfPwd - control with which data exchange is performed
     * @param lPwd  - label for the above control
     * @param dsEntryConfirm dsEntry corresponding to the confirm
     *        password field.
     * @param minLength - minimum password length
     */

    public DSEntryPassword (String model, JPasswordField pfPwd, 
                            JLabel lPwd,
                            DSEntryConfirmPassword dsEntryConfirm,
                            int minLength){
        super (model, pfPwd, lPwd, 
               (JPasswordField)dsEntryConfirm.getView(0), minLength);

        _dsEntryConfirm = dsEntryConfirm;
        _minLength = minLength;
    }

    /**
     * This method converts server data into DSEntry format. It overwrites
     * the base class function to set data for the corresponding confirm
     * password entry.
     *
     * @param remoteValue - data in the server format
     */

    public void remoteToLocal(String remoteValue) {
        super.remoteToLocal (remoteValue);
        _dsEntryConfirm.remoteToLocal (remoteValue);
    }

    /**
     * This method converts server data into DSEntry format. It overwrites
     * the base class function to set data for the corresponding confirm
     * password entry.
     *
     * @param remoteValue - data in the server format
     */

    public void remoteToLocal(Enumeration remoteValues) {
        super.remoteToLocal (remoteValues);

        _dsEntryConfirm.remoteToLocal (getModel (0));
    }

    /**
     * This method dispalys entry data in the UI control.
     * It overwrites the base class function to display data in the
     * corresponding confirm control.
     */

    public void show (){
        super.show ();
        _dsEntryConfirm.show ();
    }

    /* entry corresponding to the confirm field */
    private DSEntryConfirmPassword _dsEntryConfirm; 
    private int _minLength; /* minimum password length */
}
