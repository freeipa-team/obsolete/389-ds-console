/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import netscape.ldap.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSSchemaHelper;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Help;
import com.netscape.management.client.util.JButtonFactory;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.util.UITools;
import com.netscape.management.client.Framework;
//import com.netscape.management.client.util.ArrowIcon;




/**
 * Panel for Directory Server Create Object Class page
 *
 * @author  Christine Ho  
 * @version %I%, %G%
 * @date                9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ObjectClassDialog extends AbstractDialog
                               implements ActionListener,
                                          DocumentListener,
                                          ListSelectionListener,
 	                                      ItemListener {

    public ObjectClassDialog( IDSModel model,
							  boolean update,
							  LDAPSchema schema ) {

		super(model.getFrame(), true);		
		initialize( model, update, null, schema );

		setTitle(_resource.getString("objectclasses","create-title"));
		getAccessibleContext().setAccessibleDescription(_resource.getString("objectclasses",
																			"create-description"));

	}

    public ObjectClassDialog( IDSModel model,
							  boolean update,
							  LDAPObjectClassSchema entry,
							  LDAPSchema schema ) {

		super(model.getFrame(), true);		
		initialize( model, update, entry, schema );

		setTitle(_resource.getString("objectclasses","edit-title"));
		getAccessibleContext().setAccessibleDescription(_resource.getString("objectclasses",
																			"edit-description"));
	}

    /**
	 * Get the selected object class
	 */
    public String getObject() {
		if ( _result ) {
			return _nameText.getText();
		} else {
			return null;
		}
	}

    private void initialize( IDSModel model,
							 boolean update,
							 LDAPObjectClassSchema entry,
							 LDAPSchema schema ) {	
		_model = model;		
		_update = update;
		_schema = schema;
		_noParent = "top";		

		setSize(500, 500);
		setResizable(true);
		
		_origEntry = (entry == null) ? _schema.getObjectClass( "top" ) :
			_schema.getObjectClass( entry.getName());		
				
		updateAvailAttrList();		
		init( entry );		
		setContentPane(createDialogContent());
		setLocationRelativeTo(model.getFrame());
		addWindowListener(
			new WindowAdapter() {
			    public void windowClosing(WindowEvent we) {
					dispose();
					DSUtil.dialogCleanup();
				}
		    }
		);
        checkOkay();		
	}

    private void updateAvailAttrList() {		
		if (_availModel.size() != 0)
			_availModel.removeAllElements();
		Enumeration enumAttrs = _schema.getAttributeNames();
		while (enumAttrs.hasMoreElements()) {			
			String attr = (String)enumAttrs.nextElement();
			if (!attr.equals("dncomp") && !attr.equals("entrydn")&& 
			    !attr.equals("entryid") && !attr.equals("parentid")) {
				SchemaUtility.InsertElement(_availModel, attr);
			}
		}				
	}

    public void actionPerformed(ActionEvent e) {
		if ( !(e.getSource() instanceof JButton) && 
             !e.getActionCommand().equals("close")) {
			return;
		}
		setBusyCursor( true );
		JButton button = (JButton)(e.getSource());
		if (button.getActionCommand().equals(REQ_ADD_STR)) {
			Object[] input = _availAttrListBox.getSelectedValues();
			if (input != null) {
				for (int ii = 0; ii < input.length; ++ii) {
					_availModel.removeElement(input[ii]);
					SchemaUtility.InsertElement(_reqAttrModel, input[ii]);					
					_reqAttrListBox.setSelectedValue(input[ii], true);
					_extendedReqAttrVector.addElement(input[ii]);
				}
				_availAttrListBox.clearSelection();
				redrawListBox(_reqAttrListBox);
				redrawListBox(_availAttrListBox);
				_reqAttrListBox.clearSelection();
			}
			checkOkay();
		} else if (button.getActionCommand().equals(REQ_REMOVE_STR)) {
			Object[] input = _reqAttrListBox.getSelectedValues();
			if (input != null) {
				for (int ii = 0; ii < input.length; ++ii) {					
					_reqAttrModel.removeElement(input[ii]);
					SchemaUtility.InsertElement(_availModel, input[ii]);
					_availAttrListBox.setSelectedValue(input[ii], true);
					_extendedReqAttrVector.remove(input[ii]);
				}
				_reqAttrListBox.clearSelection();
				redrawListBox(_reqAttrListBox);
				updateReqRemoveButton();
				redrawListBox(_availAttrListBox);
			}
			checkOkay();
		} else if (button.getActionCommand().equals(ALLOW_ADD_STR)) {
			Object[] input = _availAttrListBox.getSelectedValues();
			if (input != null) {
				for (int ii = 0; ii < input.length; ++ii) {
					_availModel.removeElement(input[ii]);
					SchemaUtility.InsertElement(_allowAttrModel, input[ii]);
					_allowAttrListBox.setSelectedValue(input[ii], true);
					_extendedAllowAttrVector.addElement(input[ii]);
				}
				_availAttrListBox.clearSelection();
				redrawListBox(_allowAttrListBox);
				redrawListBox(_availAttrListBox);
                _allowAttrListBox.clearSelection();
			}
			checkOkay();
		} else if (button.getActionCommand().equals(ALLOW_REMOVE_STR)) {
			Object[] input = _allowAttrListBox.getSelectedValues();
			if (input != null) {
				for (int ii = 0; ii < input.length; ++ii) {
					_allowAttrModel.removeElement(input[ii]);
					SchemaUtility.InsertElement(_availModel, input[ii]);
					_availAttrListBox.setSelectedValue(input[ii], true);
					_extendedAllowAttrVector.remove(input[ii]);
				}
				_allowAttrListBox.clearSelection();
				redrawListBox(_allowAttrListBox);
				updateAllowRemoveButton();
				redrawListBox(_availAttrListBox);
			}
			checkOkay();
		} else if (button.equals(_OKButton)) {
			if (updateOC()) {
				_result = true;
				setVisible(false); 
				dispose();
				DSUtil.dialogCleanup();
			}
		} else if (button.equals(_cancelButton) || e.getActionCommand().equals("close")) {
			_result = false;
			setVisible(false); 
			dispose();
			DSUtil.dialogCleanup();
		} else if (button.equals(_helpButton)) {
			DSUtil.help( _helpToken );
		}
		setBusyCursor( false );
	}

	public void valueChanged(ListSelectionEvent e) {
		LDAPObjectClassSchema entry = _schema.getObjectClass(
			(String)(_ocComboBox.getSelectedItem()));
		if (e.getSource().equals(_reqAttrListBox)) {
			removeReqButton.setEnabled(false);
			Object sels[] = _reqAttrListBox.getSelectedValues();
			if ((sels != null) && (sels.length > 0)) {		
				Hashtable ht = new Hashtable();
				DSSchemaHelper.allRequiredAttributes(entry, _schema, ht);
				Enumeration reqEnum = ht.elements();			
				while (reqEnum.hasMoreElements()) {
					String str = (String)reqEnum.nextElement();
					for (int i=0; i < sels.length; i++) {
						if (str.equals((String)sels[i]))
							return;
					}
				}
 				updateReqRemoveButton();
			}
		} else if (e.getSource().equals(_allowAttrListBox)) {
			removeAllowButton.setEnabled(false);
			Object sels[] = _allowAttrListBox.getSelectedValues();
			if ((sels != null) && (sels.length > 0)) {					
				Hashtable ht = new Hashtable();
				DSSchemaHelper.allOptionalAttributes(entry, _schema, ht);
				Enumeration allowEnum = ht.elements();
	
				while (allowEnum.hasMoreElements()) {
					String str = (String)allowEnum.nextElement();
					for (int i=0; i < sels.length; i++) {
						if (str.equals((String)sels[i]))
							return;
					}
				}
				updateAllowRemoveButton();
			}
		}
	}

	private void updateReqRemoveButton() {
		boolean enable = false;
		if ( _reqAttrModel.getSize() > 0 ) {
			Object sels[] = _reqAttrListBox.getSelectedValues();
			enable = ((sels != null) && (sels.length > 0));
		}
		removeReqButton.setEnabled(enable);
	}

	private void updateAllowRemoveButton() {
		boolean enable = false;
		if ( _allowAttrModel.getSize() > 0 ) {
			Object sels[] = _allowAttrListBox.getSelectedValues();
			enable = ((sels != null) && (sels.length > 0));
		}
		removeAllowButton.setEnabled(enable);
	}

    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()==ItemEvent.SELECTED) {
            if (e.getSource().equals(_ocComboBox)) {
                updateAllListBoxes((String)(_ocComboBox.getSelectedItem()));
            }
            if ( _availAttrListBox == null )
				return;
            _availAttrListBox.clearSelection();
            _allowAttrListBox.clearSelection();
            _reqAttrListBox.clearSelection();
        }
    }

    private boolean updateOC() {
        String name = _nameText.getText();
		if ( name.length() < 1 ) {
			return false;
		}
        String oid = _oidText.getText();
		if ( oid.length() < 1 ) {
			oid = name + "-oid";
		}

		/* Prompt for overwriting an existing object class */
		boolean isBuiltIn = false;
		LDAPObjectClassSchema oc;
		if ( (oc = _schema.getObjectClass( name )) != null ) {
			isBuiltIn = DSUtil.isStandardSchema(oc);
		}
		if ( isBuiltIn ) {
			int response = DSUtil.showConfirmationDialog(
				getModel().getFrame(),
				"confirm-overwrite",
				name,
				_section );
			if ( response != JOptionPane.YES_OPTION ) {
				return false;
			}
		}

        String parent = (String)(_ocComboBox.getSelectedItem());

		/* Have to make sure objectclass is a required attribute */
        if ( !_reqAttrModel.contains( "objectclass" ) ) {
			_reqAttrModel.addElement( "objectclass" );
		}
        String[] required = new String[_reqAttrModel.size()];
        String[] optional = new String[_allowAttrModel.size()];
        _reqAttrModel.copyInto(required);
        _allowAttrModel.copyInto(optional);

        LDAPObjectClassSchema entry = new LDAPObjectClassSchema(name, oid, 
            parent, "", required, optional);
		Debug.println( "ObjectClassDialog.updateOC: modifying schema with " +
					   entry );
  
        LDAPConnection ld = _model.getServerInfo().getLDAPConnection();

		/* Loop to allow reauthenticating if necessary */
		boolean done = false;
		try {
			while ( true ) {
				try {
					confirmUpdate(entry, ld, isBuiltIn);
					return true;
				} catch (LDAPException lde) {
					if (lde.getLDAPResultCode() ==
						LDAPException.INSUFFICIENT_ACCESS_RIGHTS) {
						// display a message
						DSUtil.showPermissionDialog( getModel().getFrame(),
													 ld );
						if (!getModel().getNewAuthentication(false)) {
							return false;
						}
					}
					else try {
					  // the update failed, and we deleted the original objectclass
					  // We should restore the schema to its original state by
					  // adding the original objectclass back
					  if ( _origEntry != null &&
						   _schema.getObjectClass(_origEntry.getName()) != null ) {
						_origEntry.add(ld);
					  }
					  DSUtil.showLDAPErrorDialog(getModel().getFrame(),
												 lde,
												 "updating-directory-title");
					  return false;
					} catch( LDAPException e ) {
					  DSUtil.showLDAPErrorDialog(getModel().getFrame(),
												 lde,
												 "updating-directory-title");
					  return false;
					}
				}
			}
		}
	 finally {
	  getModel().notifyAuthChangeListeners();
	 }
	}										
    private void confirmUpdate(LDAPObjectClassSchema entry, 
							   LDAPConnection ld,
							   boolean isBuiltIn) throws LDAPException {
	    String str = _nameText.getText();
        int index = 0;
        if (_update) {
			//556763 Must modify the objectclass with a single add (no prior delete).
			entry.add(ld);
			DSUtil.showInformationDialog( getModel().getFrame(),
										  "successful-update",
										  str,
										  _section );
		} else {
            entry.add(ld);
			/* Standard object classes are "added" rather than "replaced"
			   in DS 3.x and 4.0 */
			String msg = isBuiltIn ? "successful-update" : "successful-add";
			DSUtil.showInformationDialog( getModel().getFrame(),
										  msg,
										  str,
										  _section );
        }
    }

    private void redrawListBox(JList list) {
		list.invalidate();
		list.validate();
		list.repaint(1);
	}

    private void init(LDAPObjectClassSchema entry) {
		_nameText = UIFactory.makeJTextField( this, 20 );
		_oidText = UIFactory.makeJTextField( this, 20 );
		if ( entry != null ) {
			String oid = entry.getID();
			if (oid.length() == 0)
				oid = OID_UNKNOWN_STR;

			_nameText.setText( entry.getName() );
			_oidText.setText( oid );

			_ocComboBox = createComboBox(entry.getSuperior());
			updateAllListBoxes(entry);
		} else {
			_ocComboBox = createComboBox( "top" );
			updateAllListBoxes( _schema.getObjectClass("top") );
		}
	}

    private void updateAllListBoxes(LDAPObjectClassSchema entry) {
        updateAvailAttrList();
		Hashtable ht = new Hashtable();
		DSSchemaHelper.allRequiredAttributes(entry, _schema, ht);
		Enumeration reqEnum = ht.elements();
		
        if (_reqAttrModel.size() != 0) {
            _reqAttrModel.removeAllElements();
		}

        while (reqEnum.hasMoreElements()) {
            String str = (String)reqEnum.nextElement();
            SchemaUtility.InsertElement(_reqAttrModel, str);  
            _availModel.removeElement(str);
        }

		ht = new Hashtable();
		DSSchemaHelper.allOptionalAttributes(entry, _schema, ht);
		Enumeration allowEnum = ht.elements();
   
        if (_allowAttrModel.size() != 0)
            _allowAttrModel.removeAllElements();
        while (allowEnum.hasMoreElements()) {
            String str = (String)allowEnum.nextElement();
			// users may redefine inherited allowed attributes as required
			// attributes
			if (!_reqAttrModel.contains(str)) {
				SchemaUtility.InsertElement(_allowAttrModel, str);  
				_availModel.removeElement(str);
			}
        }		

		/* 
		   We manage the extended attributes of the user before the method was called (this is useful when 
		   the user changes the parent, in order not to delete all the extensions she/he defined 
		   */
		int i=0;
		String str;
		for (i=_extendedReqAttrVector.size() - 1; i>= 0; i--) {
			str = (String)_extendedReqAttrVector.elementAt(i);
			if ((!_reqAttrModel.contains(str)) && (!_allowAttrModel.contains(str))) {	
				SchemaUtility.InsertElement(_reqAttrModel, str);
				_availModel.removeElement(str);
			} else {
				_extendedReqAttrVector.remove(str);
			}
		}
		for (i=_extendedAllowAttrVector.size() - 1; i>= 0; i--) {
			str = (String)_extendedAllowAttrVector.elementAt(i);
			if ((!_reqAttrModel.contains(str)) && (!_allowAttrModel.contains(str))) {
				SchemaUtility.InsertElement(_allowAttrModel, str);
				_availModel.removeElement(str);
			} else {
				_extendedAllowAttrVector.remove(str);				
			}
		}		
    }

    private void updateAllListBoxes(String name) {
        updateAvailAttrList();
        LDAPObjectClassSchema entry = _schema.getObjectClass(name);
        updateAllListBoxes(entry);
    }

    protected JPanel createDialogContent() {		
		JPanel container = new JPanel();
		container.setBorder( new EmptyBorder( UIFactory.getBorderInsets() ) );
		container.setLayout( new GridBagLayout() );

		JPanel j1 = new JPanel();
		j1.setBorder(new EmptyBorder( UIFactory.getComponentInsets() ));

		JLabel labelName = UIFactory.makeJLabel( _section, "name" );
		labelName.setLabelFor(_nameText);
		JLabel labelParent = UIFactory.makeJLabel( _section, "parent" );
		labelParent.setLabelFor(_ocComboBox);
		JLabel labelOID = UIFactory.makeJLabel( _section, "optionalOID" );
		labelOID.setLabelFor(_oidText);
		j1.setLayout( new GridBagLayout() );
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		int space = UIFactory.getComponentSpace();
		int different = UIFactory.getDifferentSpace();
		gbc.insets = new Insets(0, 0, 0, space);

//		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.gridx = 0;
		j1.add(labelName, gbc);		
		
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.gridx = 1;
		gbc.weightx = 0.5;
		j1.add(labelParent, gbc);		

		gbc.gridx = 0;
//		gbc.gridwidth = 2;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		j1.add(_nameText, gbc);

		gbc.gridx = 1;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.weightx = 0.5;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		j1.add(_ocComboBox, gbc);

		j1.add( Box.createVerticalStrut( space ), gbc );

		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.gridx = 0;
		gbc.insets = new Insets(0, 0, 0, space);
		j1.add(new JLabel(), gbc);

		gbc.gridx = 1;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets.right = 0;
		j1.add(labelOID, gbc);

		gbc.gridwidth = GridBagConstraints.RELATIVE;
		gbc.gridx = 0;
		gbc.insets = new Insets(0, 0, 0, space);
		j1.add(new JLabel(), gbc);

		gbc.gridx = 1;
		gbc.gridwidth = gbc.REMAINDER;
		j1.add(_oidText, gbc);

		int w = 108;
		int h = 23;
		Dimension d = new Dimension( w, h );
		Icon rightIcon = new ArrowIcon( SwingConstants.EAST );
		JButton addReqButton = createButton( "addreq", d, REQ_ADD_STR,
											 rightIcon );		
		addReqButton.setHorizontalTextPosition(addReqButton.LEFT);		
		Icon leftIcon = new ArrowIcon( SwingConstants.WEST );
		removeReqButton = createButton( "removereq", d, REQ_REMOVE_STR,
										leftIcon);
        removeReqButton.setEnabled(false);

		JButton addAllowButton = createButton( "addallow", d, ALLOW_ADD_STR,
											   rightIcon );
		addAllowButton.setHorizontalTextPosition(addAllowButton.LEFT);

		removeAllowButton = createButton( "removeallow", d,
										  ALLOW_REMOVE_STR, leftIcon );
        removeAllowButton.setEnabled(false);
		JButton[] buttons = { addReqButton, removeReqButton,
							  addAllowButton, removeAllowButton };
		JButtonFactory.resize( buttons );

		_ocComboBox.addItemListener(this);

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout( panel, BoxLayout.Y_AXIS ));
		JLabel lAvailAttr = UIFactory.makeJLabel( _section, "availAttributesList" );
		panel.add( lAvailAttr );

		_availAttrListBox = createListBox(_availModel, 15, false);
		lAvailAttr.setLabelFor(_availAttrListBox);
		JScrollPane availScrollPane = createScrollPane(_availAttrListBox);
		panel.add(availScrollPane);
   
		_reqAttrListBox = createListBox(_reqAttrModel, 8, true);
		JScrollPane reqScrollPane = createScrollPane(_reqAttrListBox);
		String title = "reqAttributesList";
		JPanel attrPanel1 =
			createAttrPanel(reqScrollPane, title,
							addReqButton, removeReqButton);
        _reqAttrListBox.addListSelectionListener(this);

		_allowAttrListBox = createListBox(_allowAttrModel, 8, true);
		JScrollPane allowScrollPane = createScrollPane(_allowAttrListBox);
		title = "allowAttributesList";
		JPanel attrPanel2 =
			createAttrPanel(allowScrollPane, title,
							addAllowButton, removeAllowButton);
        _allowAttrListBox.addListSelectionListener(this);

		JPanel attrPanel = new JPanel();
		attrPanel.setLayout( new GridBagLayout() );
    
		gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		attrPanel.add(attrPanel1, gbc);
    
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.insets.top = different;
		attrPanel.add(attrPanel2, gbc);

		w = 80;
		d = new Dimension( w, h );
		_OKButton = createButton( "OK", d, null );
		_cancelButton = createButton( "Cancel", d, null );
		_helpButton = createButton( "Help", d, null );
		JButton[] buttons2 = { _OKButton, _cancelButton,_helpButton };
		JPanel buttonPanel = UIFactory.makeJButtonPanel( buttons2, true );

		gbc = new GridBagConstraints();
		gbc.insets = new Insets( 0, 0, 0, 0 );
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		container.add(j1, gbc);

		gbc.gridheight = GridBagConstraints.RELATIVE;
		gbc.gridwidth = 1;
		gbc.insets = new Insets( different, space, 0, 0 );
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.BOTH;
		container.add(panel, gbc);

		gbc.anchor = GridBagConstraints.NORTH;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.insets = new Insets( space, space, 0, space );
		container.add(attrPanel, gbc);

		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		gbc.anchor = gbc.WEST;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets( different, 0, 0, 0 );
		container.add(buttonPanel, gbc);
 
		return container;
	}

  protected JComboBox createComboBox() {
      JComboBox comboBox = UIFactory.makeJComboBox( this );
      Enumeration schemaEnum = _schema.getObjectClassNames();
      while (schemaEnum.hasMoreElements()) {
          SchemaUtility.InsertElement(comboBox, schemaEnum.nextElement());
      }
      comboBox.insertItemAt( _noParent, 0);
      comboBox.setSelectedIndex(0);

      return comboBox;
  }

    protected JComboBox createComboBox(String parent) {
        JComboBox comboBox = createComboBox();
        int size = comboBox.getItemCount();
        int i=0;
        for (; i<size; i++) {
            String str = (String)comboBox.getItemAt(i);
            if (str.equalsIgnoreCase(parent)) {
                comboBox.setSelectedIndex(i);
                return comboBox;
            }
        }

        if (i >= size)
            comboBox.setSelectedIndex(0);

        return comboBox;
    }

    protected JPanel createAttrPanel(JScrollPane pane,
									 String title,
									 JButton addButton,
									 JButton removeButton) {
		int space = UIFactory.getComponentSpace();
		JLabel label = UIFactory.makeJLabel( _section, title );
		label.setLabelFor(pane);
		JPanel listPanel = new JPanel();
		listPanel.setLayout(new BoxLayout( listPanel, BoxLayout.Y_AXIS ));
		listPanel.add(label);
		listPanel.add(pane);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout( new GridBagLayout() );

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridheight = 1;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.gridx = 0;
		gbc.insets = new Insets(0, 0, 0, 0);

		gbc.gridy = 0;
		gbc.weighty = 1.0;
		gbc.fill = gbc.BOTH;
		buttonPanel.add( Box.createGlue(), gbc );

		gbc.gridy++;
		gbc.anchor = GridBagConstraints.SOUTH;
		gbc.fill = gbc.NONE;
		buttonPanel.add(addButton, gbc);

		gbc.gridy++;
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.insets = new Insets(space, 0, 0, 0);
		buttonPanel.add(removeButton, gbc);

		gbc.gridy++;
		gbc.fill = gbc.BOTH;
		gbc.insets = new Insets(0, 0, 0, 0);
		buttonPanel.add( Box.createGlue(), gbc );

		JPanel allPanel = new JPanel();
 		allPanel.setLayout( new GridBagLayout() );
 		gbc.gridx = 0;
 		gbc.gridy = 0;
 		gbc.weightx = 0.0;
 		gbc.weighty = 1.0;
 		gbc.insets = new Insets(0, 0, 0, 0);
 		gbc.fill = GridBagConstraints.VERTICAL;
 		gbc.gridwidth = gbc.RELATIVE;
 		allPanel.add( buttonPanel, gbc );

 		gbc.gridx = 1;
 		gbc.insets = new Insets(0, space, 0, 0);
 		gbc.weightx = 1.0;
 		gbc.weighty = 1.0;
 		gbc.fill = GridBagConstraints.BOTH;
 		gbc.gridwidth = gbc.REMAINDER;
 		allPanel.add( listPanel, gbc );

		return allPanel;
	}

    JList createListBox(DefaultListModel listModel, int visibleCount,
						boolean specialCellRenderer) {
		JList listbox = new JList(listModel);
		if (specialCellRenderer) {
			listbox.setCellRenderer(new OCCellRenderer(this));
		} else {
			listbox.setCellRenderer(new OCCellRenderer());
		}
		listbox.setPrototypeCellValue("1234567890 1234567890");
		listbox.setVisibleRowCount(visibleCount);

		return listbox;
	}

	JScrollPane createScrollPane(JList listbox) {
		JScrollPane scrollPane =
			new JScrollPane(listbox,
							JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
							JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		scrollPane.setAlignmentX(LEFT_ALIGNMENT);
		scrollPane.setAlignmentY(TOP_ALIGNMENT);
		scrollPane.setBorder(UITools.createLoweredBorder());

		return scrollPane;
	}

    private JButton createButton( String label, Dimension d, String cmd,
								  Icon image ) {
		JButton button = UIFactory.makeJButton( this, _section, label );
        if ( cmd != null )
            button.setActionCommand( cmd );
		if ( image != null ) {
			button.setIcon( image );
		}
		return button;
	}

    private JButton createButton( String label, Dimension d, String cmd ) {
		return createButton( label, d, cmd, null );
	}

    private IDSModel getModel() {
		return _model;
	}

    protected boolean isValidSchemaSyntax(String tok) {
        if ((tok == null) || (tok.length() == 0))
            return false;
        StringCharacterIterator iter = new StringCharacterIterator(tok, 0);
        for (char c = iter.first(); c != CharacterIterator.DONE; c = iter.next()) {
            if (_validsyntax.indexOf(Character.toLowerCase(c)) == -1)
                return false;
        }
        return true;
    }

    private void checkOkay() {
        if (_OKButton != null) {
    		String s = _nameText.getText();
			// also need to make sure that all inherited attributes are in either
			// the required list or the allowed list
			String parent = (String)(_ocComboBox.getSelectedItem());
			LDAPObjectClassSchema locs = _schema.getObjectClass(parent);
			boolean hasAllSuperAttrs = true;	
			Hashtable ht = new Hashtable();
			DSSchemaHelper.allRequiredAttributes(locs, _schema, ht);
			for (Enumeration e = ht.elements();
				 hasAllSuperAttrs && e.hasMoreElements(); ) {
				hasAllSuperAttrs = !_availModel.contains(e.nextElement());
			}
			ht = new Hashtable();
			DSSchemaHelper.allOptionalAttributes(locs, _schema, ht);
			for (Enumeration e = ht.elements();			
				 hasAllSuperAttrs && e.hasMoreElements(); ) {
				hasAllSuperAttrs = !_availModel.contains(e.nextElement());
			}
				
    		_OKButton.setEnabled( hasAllSuperAttrs && (s != null) &&
								  (s.length() > 0) && isValidSchemaSyntax(s));
        }
	}
    public void changedUpdate(DocumentEvent e) {
		checkOkay();
    }
    public void removeUpdate(DocumentEvent e) {
		checkOkay();
    }
    public void insertUpdate(DocumentEvent e) {
		checkOkay();
    }

	/**
	 * Inherited attributes cannot be removed from an objectclass
	 * definition.
	 */
	boolean isRemovable(String name) {
		String parent = (String)(_ocComboBox.getSelectedItem());
		LDAPObjectClassSchema locs = _schema.getObjectClass(parent);
		boolean retVal = true;	
		Hashtable ht = new Hashtable();
		DSSchemaHelper.allRequiredAttributes(locs, _schema, ht);
		for (Enumeration e = ht.elements();	
			 retVal && e.hasMoreElements(); ) {
			retVal = !name.equals(e.nextElement().toString());
		}
		ht = new Hashtable();
		DSSchemaHelper.allOptionalAttributes(locs, _schema, ht);
		for (Enumeration e = ht.elements();
			 retVal && e.hasMoreElements(); ) {
			retVal = !name.equals(e.nextElement().toString());
		}

		return retVal;
	}

	public void packAndShow() {
		super.pack();
		super.show();
	}

											  
											  

    private int _status;
    private boolean _result = false;
    private JList _availAttrListBox;
    private JList _reqAttrListBox;
    private JList _allowAttrListBox;
    private JButton _OKButton;
    private JButton _cancelButton;
    private JButton _helpButton;
    private JButton removeReqButton;
    private JButton removeAllowButton;
    private JTextField _nameText;
    private JTextField _oidText;
    private IDSModel _model;
    private boolean _update;
    private LDAPSchema _schema;
    private JComboBox _ocComboBox;
    private DefaultListModel _availModel = new DefaultListModel();
    private DefaultListModel _reqAttrModel = new DefaultListModel();
    private DefaultListModel _allowAttrModel = new DefaultListModel();
	/* These two vectors contain the extended attributes (the required/allowed attributes
	that are not present in the parent's definition */
	private Vector _extendedReqAttrVector = new Vector();
	private Vector _extendedAllowAttrVector = new Vector();
    private String _noParent = null;
	private LDAPObjectClassSchema _origEntry;

    private static final String REQ_ADD_STR = "ReqAdd";
    private static final String REQ_REMOVE_STR = "ReqRemove";
    private static final String ALLOW_ADD_STR = "AllowAdd";
    private static final String ALLOW_REMOVE_STR = "AllowRemove";
    private static final String OID_UNKNOWN_STR = "OID Unknown";
	private static final String _helpToken =
	    "configuration-schema-objclass-create-dbox-help";
    private static final String _section = "objectclasses";
    ResourceSet _resource = DSUtil._resource;
    protected static final String _validsyntax = "abcdefghijklmnopqrstuvwxyz0123456789-_";
}

class OCCellRenderer extends DefaultListCellRenderer {

    OCCellRenderer() {
        setBorder(noFocusBorder = new EmptyBorder(0, 3, 0, 3));
    }

    OCCellRenderer(ObjectClassDialog ocd) {
        this();
        _ocd = ocd;
    }

    public Component getListCellRendererComponent(JList list, Object value, 
        int index, boolean isSelected, boolean cellHasFocus) {

        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        // if the item is not removable, disable it
        setEnabled((_ocd == null) || _ocd.isRemovable(value.toString()));

        return this;
    }

    private ObjectClassDialog _ocd = null;
}
