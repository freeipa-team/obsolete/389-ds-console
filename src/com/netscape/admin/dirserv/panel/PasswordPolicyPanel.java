/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.DocumentEvent;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.nmclf.*;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Panel for Directory Server password policy settings page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date        9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class PasswordPolicyPanel extends BlankPanel {
    public PasswordPolicyPanel(IDSModel model) {
        super(model, _section);
		_policyspecdn = CONFIG_DN;
		_helpToken = "configuration-database-passwords-help";
		_refreshWhenSelect = false;
		_mode = SERVER;
    }

	public PasswordPolicyPanel(IDSModel model, String dn, int mode) {
		super(model, _section);
		_helpToken = "configuration-browser-passwords-help";
		_refreshWhenSelect = false;
		
		_dn = dn;
		_mode = mode;
		_parentdn = new DN(dn).getParent().toString();

		_containerdn = ( _mode == FINEGRAINED_USER ?
							("cn=" + CONTAINER_CN + "," + _parentdn) :
							("cn=" + CONTAINER_CN + "," + _dn) );

		_policyspecdn = "cn=\"cn=" + USER_SPEC_CN + "," +
						_dn + "\"," + _containerdn;

		_costemplatedn = "cn=\"cn=" + COS_TEMPL_CN + "," +
						_dn + "\"," + _containerdn;

		_cosspecdn = "cn=" + COS_SPEC_CN + "," + _dn;

		// Use a DSEntrySet that doesn't complain if entry/attr are non-existant
		setDSEntrySet(new DSEntrySet(false, true));
	}

    public void init() {
		if (_isInitialized) {
			return;
		}
		_spec_exists = fineGrainedPolicyPresent();
		_myPanel.setLayout(new GridBagLayout());

		if (_mode != SERVER) {
			createFineGrainedToggleArea((Container)_myPanel);
		} else {
			createFineGrainedPolicyArea((Container)_myPanel);
		}

       	createChangeArea((Container)_myPanel);
       	createExpirationArea((Container)_myPanel);
       	createSyntaxArea((Container)_myPanel);
        createStorageArea((Container)_myPanel);

        addBottomGlue ();
		_isInitialized = true;
    }

    public boolean refresh () {
        boolean result = super.refresh ();

        if (result){
			enableChange();
            enableExpire ();
            enableHistory();
            enableSyntax();
			if (_mode != SERVER){
				enableStorage();
			}
        }

        return result;
    }

	public int getMode() {
		return _mode;
	}

    protected void createFineGrainedPolicyArea( Container myContainer ) {
        _cbFineGrainedPolicy = makeJCheckBox(_section, "finegrained", false);
        DSEntrySet entries = getDSEntrySet();
		DSEntryBoolean fineGrainedDSEntry = new DSEntryBoolean( "off", _cbFineGrainedPolicy );
        entries.add( "cn=config", FINE_GRAINED_POLICY_ATTR_NAME, fineGrainedDSEntry );
		setComponentTable(_cbFineGrainedPolicy, fineGrainedDSEntry);		

		GridBagConstraints gbc = getGBC();
		gbc.gridwidth = gbc.REMAINDER;
		gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
        myContainer.add(_cbFineGrainedPolicy, gbc);
    }

	protected void createFineGrainedToggleArea( Container myContainer ) {

		String subsection = ( _mode == FINEGRAINED_USER ? "finegraineduser" :
													 	  "finegrainedsubtree" );
		// If spec exists we use a different label for the top control checkbox
		if( !_spec_exists ) {
			subsection = subsection + "create";
		}
		_cbFineGrainedEnabled = makeJCheckBox(_section, subsection, false);
		_cbFineGrainedEnabled.setSelected( _spec_exists );

		// Layout
		GridBagConstraints gbc = getGBC();
		gbc.gridwidth = gbc.REMAINDER;
		gbc.anchor = gbc.WEST;
		gbc.fill = gbc.NONE;
		gbc.weightx = 1.0;
		myContainer.add(_cbFineGrainedEnabled, gbc);
	}

    protected void createChangeArea( Container myContainer ) {
        // create widgets

        _cbReset = makeJCheckBox (_section, "changereset", false);
        _cbMayChange = makeJCheckBox(_section, "changemay", true); 

        DSEntrySet entries = getDSEntrySet();

		DSEntryBoolean mustDSEntry = new DSEntryBoolean("0", _cbReset);
        entries.add(_policyspecdn, MUST_CHANGE_ATTR_NAME,
                    mustDSEntry);
		setComponentTable(_cbReset, mustDSEntry);

		DSEntryBoolean mayDSEntry = new DSEntryBoolean("1", _cbMayChange);
		entries.add(_policyspecdn, CHANGE_ATTR_NAME,
                    mayDSEntry);
		setComponentTable(_cbMayChange, mayDSEntry);
            
        // create containers and layouts
        JPanel changePanel =
			new GroupPanel(_resource.getString( _section,
													   "change-title"));
		GridBagConstraints gbc = getGBC();
		gbc.gridwidth = gbc.REMAINDER;
		gbc.anchor = gbc.WEST;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        myContainer.add(changePanel, gbc);
        changePanel.setLayout(new GridBagLayout());
        gbc.fill = gbc.NONE;
        _cbReset.setHorizontalAlignment(SwingConstants.LEFT);
        changePanel.add(_cbReset, gbc);
        _cbMayChange.setHorizontalAlignment(SwingConstants.LEFT);
        changePanel.add(_cbMayChange, gbc);

		/* Allow changes in [  ] days  (min age) */
		_tfMinAgeValue = makeNumericalJTextField(_section, "minage1");

		_minAgeLabel1 = makeJLabel(_section, "minage1");
		_minAgeLabel1.setLabelFor(_tfMinAgeValue);
		_minAgeLabel2 = makeJLabel(_section, "minage2");
		_minAgeLabel2.setLabelFor(_tfMinAgeValue);
		
		DSEntryInteger minDSEntry = 
		  new DSEntryInteger(null, 
							 _tfMinAgeValue,
							 _minAgeLabel1, 
							 MIN_AGE_MIN_VAL, 
							 DSUtil.epochConstraint(MIN_AGE_MAX_VAL,1), 
							 DAY_TO_SEC_FACTOR);
        entries.add(_policyspecdn, MIN_AGE_ATTR_NAME, minDSEntry);
        setComponentTable(_tfMinAgeValue, minDSEntry);
		JPanel minAgeInPanel =
            new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		changePanel.add(minAgeInPanel, gbc);
        _minAgeLabel1.setHorizontalAlignment(SwingConstants.LEFT);
        minAgeInPanel.add(_minAgeLabel1);
		int space = UIFactory.getComponentSpace();
        minAgeInPanel.add(Box.createHorizontalStrut(space));
        minAgeInPanel.add(_tfMinAgeValue);
        minAgeInPanel.add(Box.createHorizontalStrut(space));
        _minAgeLabel2.setHorizontalAlignment(SwingConstants.LEFT);
        minAgeInPanel.add(_minAgeLabel2);

		/* Keep Password History, remember [ ] passwords */
		_cbKeepHistory = makeJCheckBox(_section, "keephistory", false);
		_rememberLabel1 = makeJLabel(_section, "remember");
        _tfRememberValue = makeNumericalJTextField(_section, "remember");
		_rememberLabel1.setLabelFor(_tfRememberValue);
        _rememberLabel2 = makeJLabel(_section, "remember2");
		_rememberLabel2.setLabelFor(_tfRememberValue);

		DSEntryBoolean keepHistDSEntry = new DSEntryBoolean("0", _cbKeepHistory);
        entries.add(_policyspecdn, HISTORY_ATTR_NAME,
                    keepHistDSEntry);
		
		setComponentTable(_cbKeepHistory, keepHistDSEntry);
		
		DSEntryInteger historyDSEntry = 
		  new DSEntryInteger(null, 
							 _tfRememberValue, _rememberLabel1,
							 HISTORY_NUM_MIN_VAL, HISTORY_NUM_MAX_VAL, 1);
        entries.add(_policyspecdn, HISTORY_NUM_ATTR_NAME, historyDSEntry);
        setComponentTable(_tfRememberValue, historyDSEntry);
		_cbKeepHistory.setHorizontalAlignment(SwingConstants.LEFT);
		
        JPanel rememberPanel =
            new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        changePanel.add(rememberPanel, gbc);
		int rememberspace = UIFactory.getComponentSpace();
		int separated = UIFactory.getSeparatedSpace();
        rememberPanel.add(_cbKeepHistory);
        rememberPanel.add(Box.createHorizontalStrut(separated));
        rememberPanel.add(_rememberLabel1);
        rememberPanel.add(Box.createHorizontalStrut(rememberspace));
        rememberPanel.add(_tfRememberValue);
        rememberPanel.add(Box.createHorizontalStrut(space));
        rememberPanel.add(_rememberLabel2);
    }

    protected void createSyntaxArea( Container myContainer ) {
        _cbCheckSyntax = makeJCheckBox(_section, "check", false);

        _minLengthLabel = makeJLabel(_section, "minlength");

        _tfMinLengthValue = makeNumericalJTextField(_section, "minlength");
                _minLengthLabel.setLabelFor(_tfMinLengthValue);

        _minDigitsLabel = makeJLabel(_section, "mindigits");

        _tfMinDigitsValue = makeNumericalJTextField(_section, "mindigits");
                _minDigitsLabel.setLabelFor(_tfMinDigitsValue);

        _minAlphasLabel = makeJLabel(_section, "minalphas");

        _tfMinAlphasValue = makeNumericalJTextField(_section, "minalphas");
                _minAlphasLabel.setLabelFor(_tfMinAlphasValue);

        _minUppersLabel = makeJLabel(_section, "minuppers");

        _tfMinUppersValue = makeNumericalJTextField(_section, "minuppers");
                _minUppersLabel.setLabelFor(_tfMinUppersValue);

        _minLowersLabel = makeJLabel(_section, "minlowers");

        _tfMinLowersValue = makeNumericalJTextField(_section, "minlowers");
                _minLowersLabel.setLabelFor(_tfMinLowersValue);

        _minSpecialsLabel = makeJLabel(_section, "minspecials");

        _tfMinSpecialsValue = makeNumericalJTextField(_section, "minspecials");
                _minSpecialsLabel.setLabelFor(_tfMinSpecialsValue);

        _min8bitLabel = makeJLabel(_section, "min8bit");

        _tfMin8bitValue = makeNumericalJTextField(_section, "min8bit");
                _min8bitLabel.setLabelFor(_tfMin8bitValue);

        _maxRepeatsLabel = makeJLabel(_section, "maxrepeats");

        _tfMaxRepeatsValue =  makeNumericalJTextField(_section, "maxrepeats");
                _maxRepeatsLabel.setLabelFor(_tfMaxRepeatsValue);

        _minCategoriesLabel = makeJLabel(_section, "mincategories");

        _tfMinCategoriesValue = makeNumericalJTextField(_section, "mincategories");
                _minCategoriesLabel.setLabelFor(_tfMinCategoriesValue);

        _minTokenLengthLabel = makeJLabel(_section, "mintokenlength");

        _tfMinTokenLengthValue = makeNumericalJTextField(_section, "mintokenlength");
                _minTokenLengthLabel.setLabelFor(_tfMinTokenLengthValue);

        // tie components to config attributes
        DSEntrySet entries = getDSEntrySet();

        DSEntryBoolean se = new DSEntryBoolean( "off", _cbCheckSyntax );
        entries.add( _policyspecdn, SYNTAX_ATTR_NAME, se );
                setComponentTable(_cbCheckSyntax, se);

        DSEntryInteger minLengthDSEntry = new DSEntryInteger(MINLENGTH_DEFAULT_VAL,
            _tfMinLengthValue, _minLengthLabel, MINLENGTH_MIN_VAL, MINLENGTH_MAX_VAL, 1);
        entries.add(_policyspecdn, MINLENGTH_ATTR_NAME, minLengthDSEntry);
        setComponentTable(_tfMinLengthValue, minLengthDSEntry);

        DSEntryInteger minDigitsDSEntry = new DSEntryInteger(MINDIGITS_DEFAULT_VAL,
            _tfMinDigitsValue, _minDigitsLabel, MINDIGITS_MIN_VAL, MINDIGITS_MAX_VAL, 1);
        entries.add(_policyspecdn, MINDIGITS_ATTR_NAME, minDigitsDSEntry);
        setComponentTable(_tfMinDigitsValue, minDigitsDSEntry);

        DSEntryInteger minAlphasDSEntry = new DSEntryInteger(MINALPHAS_DEFAULT_VAL,
            _tfMinAlphasValue, _minAlphasLabel, MINALPHAS_MIN_VAL, MINALPHAS_MAX_VAL, 1);
        entries.add(_policyspecdn, MINALPHAS_ATTR_NAME, minAlphasDSEntry);
        setComponentTable(_tfMinAlphasValue, minAlphasDSEntry);

        DSEntryInteger minUppersDSEntry = new DSEntryInteger(MINUPPERS_DEFAULT_VAL,
            _tfMinUppersValue, _minUppersLabel, MINUPPERS_MIN_VAL, MINUPPERS_MAX_VAL, 1);
        entries.add(_policyspecdn, MINUPPERS_ATTR_NAME, minUppersDSEntry);
        setComponentTable(_tfMinUppersValue, minUppersDSEntry);

        DSEntryInteger minLowersDSEntry = new DSEntryInteger(MINLOWERS_DEFAULT_VAL,
            _tfMinLowersValue, _minLowersLabel, MINLOWERS_MIN_VAL, MINLOWERS_MAX_VAL, 1);
        entries.add(_policyspecdn, MINLOWERS_ATTR_NAME, minLowersDSEntry);
        setComponentTable(_tfMinLowersValue, minLowersDSEntry);

        DSEntryInteger minSpecialsDSEntry = new DSEntryInteger(MINSPECIALS_DEFAULT_VAL,
            _tfMinSpecialsValue, _minSpecialsLabel, MINSPECIALS_MIN_VAL, MINSPECIALS_MAX_VAL, 1);
        entries.add(_policyspecdn, MINSPECIALS_ATTR_NAME, minSpecialsDSEntry);
        setComponentTable(_tfMinSpecialsValue, minSpecialsDSEntry);

        DSEntryInteger min8bitDSEntry = new DSEntryInteger(MIN8BIT_DEFAULT_VAL,
            _tfMin8bitValue, _min8bitLabel,MIN8BIT_MIN_VAL, MIN8BIT_MAX_VAL, 1);
        entries.add(_policyspecdn, MIN8BIT_ATTR_NAME, min8bitDSEntry);
        setComponentTable(_tfMin8bitValue, min8bitDSEntry);

        DSEntryInteger maxRepeatsDSEntry = new DSEntryInteger(MAXREPEATS_DEFAULT_VAL,
            _tfMaxRepeatsValue, _maxRepeatsLabel,MAXREPEATS_MIN_VAL, MAXREPEATS_MAX_VAL, 1);
        entries.add(_policyspecdn, MAXREPEATS_ATTR_NAME, maxRepeatsDSEntry);
        setComponentTable(_tfMaxRepeatsValue, maxRepeatsDSEntry);

        DSEntryInteger minCategoriesDSEntry = new DSEntryInteger(MINCATEGORIES_DEFAULT_VAL,
            _tfMinCategoriesValue, _minCategoriesLabel,MINCATEGORIES_MIN_VAL, MINCATEGORIES_MAX_VAL, 1);
        entries.add(_policyspecdn, MINCATEGORIES_ATTR_NAME, minCategoriesDSEntry);
        setComponentTable(_tfMinCategoriesValue, minCategoriesDSEntry);

        DSEntryInteger minTokenLengthDSEntry = new DSEntryInteger(MINTOKENLENGTH_DEFAULT_VAL,
            _tfMinTokenLengthValue, _minTokenLengthLabel,MINTOKENLENGTH_MIN_VAL, MINTOKENLENGTH_MAX_VAL, 1);
        entries.add(_policyspecdn, MINTOKENLENGTH_ATTR_NAME, minTokenLengthDSEntry);
        setComponentTable(_tfMinTokenLengthValue, minTokenLengthDSEntry);

        // create containers and layouts
        JPanel syntaxPanel = new GroupPanel(  _resource.getString(_section, "syntax-title"));
        syntaxPanel.setLayout(new GridBagLayout());
                GridBagConstraints gbc = getGBC();
                gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        myContainer.add(syntaxPanel, gbc);

        GridBagUtil.constrain(syntaxPanel, _cbCheckSyntax, 0, 0,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, SuiConstants.COMPONENT_SPACE, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _minLengthLabel, 0, 1,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMinLengthValue, 1, 1, 
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _minDigitsLabel, 0, 2,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMinDigitsValue, 1, 2,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _minAlphasLabel, 0, 3,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMinAlphasValue, 1, 3,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _minUppersLabel, 0, 4,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMinUppersValue, 1, 4,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _minLowersLabel, 0, 5,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMinLowersValue, 1, 5,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _minSpecialsLabel, 0, 6,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMinSpecialsValue, 1, 6,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _min8bitLabel, 0, 7,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMin8bitValue, 1, 7,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _maxRepeatsLabel, 0, 8,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMaxRepeatsValue, 1, 8,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _minCategoriesLabel, 0, 9,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMinCategoriesValue, 1, 9,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);

        GridBagUtil.constrain(syntaxPanel, _minTokenLengthLabel, 0, 10,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.EAST, GridBagConstraints.NONE,
                              0, 0, 0, SuiConstants.COMPONENT_SPACE);

        GridBagUtil.constrain(syntaxPanel, _tfMinTokenLengthValue, 1, 10,
                              1, 1, 1.0, 0.0,
                              GridBagConstraints.WEST, GridBagConstraints.NONE,
                              0, 0, SuiConstants.COMPONENT_SPACE, 0);
    }


    protected void createExpirationArea( Container myContainer) {
        ButtonGroup expirationButtonGroup = new ButtonGroup();

        _rbNever = makeJRadioButton(_section, "neverexpire", true);
        expirationButtonGroup.add(_rbNever);

        _rbExpiresIn = makeJRadioButton(_section, "expires");
        expirationButtonGroup.add(_rbExpiresIn);

        _tfExpiresInValue = makeNumericalJTextField(_section, "expiresin");

        _expiresInLabel = makeJLabel(_section, "expiresin");		
		_expiresInLabel.setLabelFor(_tfExpiresInValue);

        _sendWarningLabel1 = makeJLabel(_section, "sendwarning");

        _tfSendWarningValue = makeNumericalJTextField(_section, "sendwarning");
		_sendWarningLabel1.setLabelFor(_tfSendWarningValue);

        _sendWarningLabel2 =
            makeJLabel(_section, "sendwarning2");
		_sendWarningLabel2.setLabelFor(_tfSendWarningValue);

        _graceLoginLabel1 = makeJLabel(_section, "gracelogin");

        _tfGraceLoginValue = makeNumericalJTextField(_section, "gracelogin");
		_graceLoginLabel1.setLabelFor(_tfGraceLoginValue);

        _graceLoginLabel2 =
            makeJLabel(_section, "gracelogin2");
		_graceLoginLabel2.setLabelFor(_tfGraceLoginValue);
                
        DSEntrySet entries = getDSEntrySet();

        DSEntryExpire expiresInDSEntry = new DSEntryExpire("0", _rbExpiresIn);
        entries.add(_policyspecdn, EXPIRES_ATTR_NAME,
                    expiresInDSEntry);
		setComponentTable(_rbExpiresIn, expiresInDSEntry);        

        DSEntryInteger expireAgeDSEntry =
			new DSEntryInteger(null, _tfExpiresInValue,
								_expiresInLabel,EXPIRES_AGE_MIN_VAL,
								DSUtil.epochConstraint(EXPIRES_AGE_MAX_VAL, 1),
								DAY_TO_SEC_FACTOR);
        entries.add(_policyspecdn, EXPIRES_AGE_ATTR_NAME, expireAgeDSEntry);
        setComponentTable(_tfExpiresInValue, expireAgeDSEntry);

        DSEntryWarning warningDSEntry = new DSEntryWarning(null,
			_tfSendWarningValue, _sendWarningLabel1, 
			EXPIRES_WARNING_MIN_VAL,
			DSUtil.epochConstraint(EXPIRES_WARNING_MAX_VAL, 1), 
			DAY_TO_SEC_FACTOR, _tfExpiresInValue);
        entries.add(_policyspecdn, EXPIRES_WARNING_ATTR_NAME,
            warningDSEntry);
        setComponentTable(_tfSendWarningValue, warningDSEntry);

        DSEntryInteger graceLoginDSEntry =
			new DSEntryInteger(null, _tfGraceLoginValue,
							   _graceLoginLabel1,GRACE_LOGIN_MIN_VAL,
							   GRACE_LOGIN_MAX_VAL, 1);
        entries.add(_policyspecdn, GRACE_LOGIN_ATTR_NAME, graceLoginDSEntry);
        setComponentTable(_tfGraceLoginValue, graceLoginDSEntry);

        // create containers and layouts
        JPanel expirationPanel =
			new GroupPanel( _resource.getString(_section,
													   "expiration-title"));
        expirationPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = getGBC();
		gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        myContainer.add(expirationPanel, gbc);
        gbc.fill = gbc.NONE;
        _rbNever.setHorizontalAlignment(SwingConstants.LEFT);
        expirationPanel.add(_rbNever, gbc);
        JPanel expiresInPanel =
            new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        expirationPanel.add(expiresInPanel, gbc);
        _rbExpiresIn.setHorizontalAlignment(SwingConstants.LEFT);
        expiresInPanel.add(_rbExpiresIn);
        expiresInPanel.add(_tfExpiresInValue);
		int space = UIFactory.getComponentSpace();
        expiresInPanel.add(Box.createHorizontalStrut(space));
        expiresInPanel.add(_expiresInLabel);

        JPanel sendWarningPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        expirationPanel.add(sendWarningPanel, gbc);
        _sendWarningLabel1.setHorizontalAlignment(SwingConstants.LEFT);
        sendWarningPanel.add(_sendWarningLabel1);
        sendWarningPanel.add(_tfSendWarningValue);
        _sendWarningLabel2.add(Box.createHorizontalStrut(space));
        _sendWarningLabel2.setHorizontalAlignment(SwingConstants.LEFT);
        sendWarningPanel.add(_sendWarningLabel2);

        JPanel graceLoginPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        expirationPanel.add(graceLoginPanel, gbc);
        _graceLoginLabel1.setHorizontalAlignment(SwingConstants.LEFT);
        graceLoginPanel.add(_graceLoginLabel1);
        graceLoginPanel.add(_tfGraceLoginValue);
        _graceLoginLabel2.add(Box.createHorizontalStrut(space));
        _graceLoginLabel2.setHorizontalAlignment(SwingConstants.LEFT);
        graceLoginPanel.add(_graceLoginLabel2);
        
    }

    class DSEntryExpire extends DSEntryBoolean{
        public DSEntryExpire(String model, AbstractButton view) {
            super(model, view);
        }

        public void show (){
            String val = getModel(0);
		    if (val.equals (_trueValue)){
                _rbNever.setSelected (false);
                _rbExpiresIn.setSelected (true);
            } else{
                _rbNever.setSelected (true);
                _rbExpiresIn.setSelected (false);
            }

            viewInitialized ();
        }
    }

    protected void createStorageArea( Container myContainer ) {
		_cbStorageScheme = makeJComboBox();
	
		_lStorageScheme = makeJLabel(_section, "storageScheme");
        _lStorageScheme.setLabelFor(_cbStorageScheme);

        DSEntrySet entries = getDSEntrySet();
		
		findPasswordPlugins();

		if ((COMBO_ENTRIES == null) || (COMBO_ENTRIES.length == 0)) {
			Debug.println("PasswordPolicyPanel.init(): we are not creating the right thing");
			COMBO_ENTRIES = COMBO_ENTRIES_HARDCOPY;
			COMBO_DESCRIPTION = COMBO_DESCRIPTION_HARDCOPY;
		}

		for (int i=0; i< COMBO_ENTRIES.length; i++) {						
			String val = COMBO_DESCRIPTION[i];
			if (val.equals("")) {
				val = COMBO_ENTRIES[i];
			}			
			_cbStorageScheme.addItem(val);
		}

        DSEntryCombo storageEntry = new DSEntryCombo(COMBO_ENTRIES,
						     _cbStorageScheme,
					             _lStorageScheme,
                                                     true);
        entries.add(_policyspecdn, STORAGE_SCHEME_ATTR_NAME, storageEntry);
        setComponentTable(_cbStorageScheme, storageEntry);

		// create containers and layouts
		GridBagConstraints gbc = getGBC();
		gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        JPanel panel =
            new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        myContainer.add(panel, gbc);

		panel.add(_lStorageScheme);
		int space = UIFactory.getComponentSpace();
        panel.add(Box.createHorizontalStrut(space));
		panel.add(_cbStorageScheme);
    }

	public void okCallback() {
		if ( _mode != SERVER ) {
			if ( _spec_exists ) {
				if ( ! _cbFineGrainedEnabled.isSelected() ) {
				}
			} else {
				addStructure();
			}
		}
		updateCreateLabel();
		super.okCallback();
	}

	public void resetCallback() {
		if ( _mode != SERVER ) {
			_cbFineGrainedEnabled.setSelected( _spec_exists );
		}
		super.resetCallback();
	}

	public boolean addEntry(LDAPConnection ldc, LDAPEntry entry) 
	throws LDAPException {
		try {
			ldc.add(entry);
		} catch(LDAPException e) {
			if (e.getLDAPResultCode() == e.ENTRY_ALREADY_EXISTS) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	private boolean deleteStructure() {
		boolean status = false;
		String msgargs[] = new String[1];
		String msg = (_mode == FINEGRAINED_USER ? "confirmDelete-user" :
												  "confirmDelete-subtree");
		msgargs[0] = _dn;
		int option = DSUtil.showConfirmationDialog(
			getModel().getFrame(), msg, msgargs, "passwordpolicy");
		
		if (option == JOptionPane.NO_OPTION) {
			return status;
		}
		try {
			String curdn;
			LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
			if (ldc == null) {
				return status;
			}
// Remove the common spec entry
			ldc.delete( _policyspecdn );
// If we successfully remove the spec entry, we're happy
			status = true;
			if (_mode == FINEGRAINED_USER) {
// Remove pwpolicysubentry attribute from entry that we're removing PWP from
				LDAPModificationSet attrs = new LDAPModificationSet();
				attrs.add(LDAPModification.DELETE, 
								new LDAPAttribute("pwdpolicysubentry") );
				ldc.modify( _dn, attrs );
			} else {
// Remove the subtree CoS entries
				ldc.delete( _costemplatedn );
				ldc.delete( _cosspecdn );
			}
// Delete container entry
			ldc.delete( _containerdn );
		} catch(LDAPException e) {
			String failure = "PasswordPolicyPanel.deleteStructure: delete of"+
							" container entry "+_containerdn+" failed: ";
			Debug.println(failure + e.getLDAPResultCode());
		}
		_spec_exists = false;
		return status;
	}

	private boolean addStructure() {
		try {
			LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
			if (ldc == null) {
				return false;
			}
			LDAPAttributeSet las = new LDAPAttributeSet();

			Debug.println("PasswordPolicyPanel.addStructure: Adding necessary structure");
			// Add container
			las.add(new LDAPAttribute("objectclass", "nsContainer"));
			LDAPEntry newEntry = new LDAPEntry(_containerdn, las);
			addEntry ( ldc, newEntry );

			// Add password policy specification entry
			las = new LDAPAttributeSet();
			las.add(new LDAPAttribute("objectclass", "ldapsubentry"));
			las.add(new LDAPAttribute("objectclass", "passwordpolicy"));
			newEntry = new LDAPEntry(_policyspecdn, las);
			addEntry ( ldc, newEntry );

			_spec_exists = true;

			// Add CoS stuff if we're working on a subtree
			if ( _mode == FINEGRAINED_SUBTREE ) {
				las = new LDAPAttributeSet();
				las.add(new LDAPAttribute("objectclass", "extensibleObject"));
				las.add(new LDAPAttribute("objectclass", "costemplate"));
				las.add(new LDAPAttribute("objectclass", "ldapsubentry"));
				las.add(new LDAPAttribute("cosPriority", "1"));
				las.add(new LDAPAttribute("pwdpolicysubentry", _policyspecdn));
				newEntry = new LDAPEntry(_costemplatedn, las);
				addEntry ( ldc, newEntry );

				las = new LDAPAttributeSet();
				las.add(new LDAPAttribute("objectclass", "ldapsubentry"));
				las.add(new LDAPAttribute("objectclass", "cosSuperDefinition"));
				las.add(new LDAPAttribute("objectclass", "cosPointerDefinition"));
				las.add(new LDAPAttribute("cosTemplateDn", _costemplatedn));
				las.add(new LDAPAttribute("cosAttribute", "pwdpolicysubentry default operational-default"));
				newEntry = new LDAPEntry(_cosspecdn, las);
				addEntry ( ldc, newEntry );
			}
			// Modify pwdpolicysubentry if we're working on a single entry
			if ( _mode == FINEGRAINED_USER ) {
				LDAPModificationSet attrs = new LDAPModificationSet();
				attrs.add(LDAPModification.REPLACE,
								new LDAPAttribute("pwdpolicysubentry",
								_policyspecdn));
				ldc.modify( _dn, attrs );
			}
			
		} catch(LDAPException e) {
			Debug.println("PasswordPolicyPanel.addStructure LDAPException: " + 
							e.toString() );
		}
		return true;
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(_cbCheckSyntax)) {
			enableSyntax();
        } else if (e.getSource().equals(_cbKeepHistory)) {
			enableHistory();
        } else if (e.getSource().equals(_rbExpiresIn) ||
               	   e.getSource().equals(_rbNever)){
            enableExpire ();
		} else if (e.getSource().equals(_cbFineGrainedEnabled)) {
			enableSyntax();
			enableHistory();
			enableExpire();
			enableChange();
			enableStorage();
			if (_spec_exists && !_cbFineGrainedEnabled.isSelected() ){
				boolean result = deleteStructure();
				if (result == false) {
					_cbFineGrainedEnabled.setSelected( true );
				}
				enableSyntax();
				enableHistory();
				enableExpire();
				enableChange();
				enableStorage();
			}
			updateCreateLabel();
			setDirtyFlag();
			refresh();
        }
		super.actionPerformed(e);  // Always want to pass event to parent
		                           // to enable buttons.		
    }

	private void updateCreateLabel() {
        String subsection = ( _mode == FINEGRAINED_USER ? "finegraineduser" :
                                                          "finegrainedsubtree" );
        // If spec exists we use a different label for the top control checkbox
        if( !_spec_exists ) {
            subsection = subsection + "create";
        }

		if( _cbFineGrainedEnabled != null) {
			_cbFineGrainedEnabled.setText( 
									_resource.getString( _section,
									subsection + "-label") );
			_cbFineGrainedEnabled.setToolTipText(
									_resource.getString( _section,
									subsection + "-ttip") );
			_cbFineGrainedEnabled.repaint();
		}
	}

	private void enableStorage() {
		boolean enable = _cbFineGrainedEnabled.isSelected();
		
		_lStorageScheme.setEnabled(enable);
		_lStorageScheme.repaint();

		_cbStorageScheme.setEnabled(enable);
		_cbStorageScheme.repaint();
	}

    private void enableHistory() {
		boolean enable = _cbKeepHistory.isSelected();
		if (_mode != SERVER) {
			if( ! _cbFineGrainedEnabled.isSelected()) {
				enable = false;
			}
		}

        _rememberLabel1.setEnabled( enable );
        _rememberLabel1.repaint();

        _tfRememberValue.setEnabled( enable );
        _tfRememberValue.repaint();

        _rememberLabel2.setEnabled( enable );
        _rememberLabel2.repaint();
    }

    private void enableSyntax() {
		boolean enable = false;

		if (_mode != SERVER) {
			if( ! _cbFineGrainedEnabled.isSelected() ) {
			    _cbCheckSyntax.setEnabled( false );
			    _cbCheckSyntax.repaint();
			} else {
				_cbCheckSyntax.setEnabled( true );
				enable = _cbCheckSyntax.isSelected();
				_cbCheckSyntax.repaint();
			}
	    } else {
			enable = _cbCheckSyntax.isSelected();
		}

		_minLengthLabel.setEnabled( enable );
		_minLengthLabel.repaint();
		_tfMinLengthValue.setEnabled( enable );
		_tfMinLengthValue.repaint();
                _minDigitsLabel.setEnabled( enable );
                _minDigitsLabel.repaint();
                _tfMinDigitsValue.setEnabled( enable );
                _tfMinDigitsValue.repaint();
                _minAlphasLabel.setEnabled( enable );
                _minAlphasLabel.repaint();
                _tfMinAlphasValue.setEnabled( enable );
                _tfMinAlphasValue.repaint();
                _minUppersLabel.setEnabled( enable );
                _minUppersLabel.repaint();
                _tfMinUppersValue.setEnabled( enable );
                _tfMinUppersValue.repaint();
                _minLowersLabel.setEnabled( enable );
                _minLowersLabel.repaint();
                _tfMinLowersValue.setEnabled( enable );
                _tfMinLowersValue.repaint();
                _minSpecialsLabel.setEnabled( enable );
                _minSpecialsLabel.repaint();
                _tfMinSpecialsValue.setEnabled( enable );
                _tfMinSpecialsValue.repaint();
                _min8bitLabel.setEnabled( enable );
                _min8bitLabel.repaint();
                _tfMin8bitValue.setEnabled( enable );
                _tfMin8bitValue.repaint();
                _maxRepeatsLabel.setEnabled( enable );
                _maxRepeatsLabel.repaint();
                _tfMaxRepeatsValue.setEnabled( enable );
                _tfMaxRepeatsValue.repaint();
                _minCategoriesLabel.setEnabled( enable );
                _minCategoriesLabel.repaint();
                _tfMinCategoriesValue.setEnabled( enable );
                _tfMinCategoriesValue.repaint();
                _minTokenLengthLabel.setEnabled( enable );
                _minTokenLengthLabel.repaint();
                _tfMinTokenLengthValue.setEnabled( enable );
                _tfMinTokenLengthValue.repaint();
    }

	private void enableChange (){
		if (_mode == SERVER) {
			return;
		}
		boolean enable = _cbFineGrainedEnabled.isSelected();
		
		_cbReset.setEnabled (enable);
		_cbReset.repaint();

		_cbMayChange.setEnabled (enable);
		_cbMayChange.repaint();

		_minAgeLabel1.setEnabled (enable);
		_minAgeLabel1.repaint();

		_minAgeLabel2.setEnabled (enable);
		_minAgeLabel2.repaint();

		_tfMinAgeValue.setEnabled (enable);
		_tfMinAgeValue.repaint();

		_cbKeepHistory.setEnabled (enable);
		_cbKeepHistory.repaint();
	}
		


    private void enableExpire (){
        boolean enable = _rbExpiresIn.isSelected();

		if (_mode != SERVER) {
			if( ! _cbFineGrainedEnabled.isSelected() ) {
				enable = false;
				_rbExpiresIn.setEnabled (false);
				_rbExpiresIn.repaint();
				_rbNever.setEnabled (false);
				_rbNever.repaint();
			} else {
				_rbExpiresIn.setEnabled (true);
				_rbExpiresIn.repaint();
				_rbNever.setEnabled (true);
				_rbNever.repaint();
			}
		}

		_tfExpiresInValue.setEnabled (enable);
       	_tfExpiresInValue.repaint ();

       	_expiresInLabel.setEnabled (enable);
       	_expiresInLabel.repaint ();

        _sendWarningLabel1.setEnabled (enable);
		_sendWarningLabel1.repaint ();

		_tfSendWarningValue.setEnabled (enable);
       	_tfSendWarningValue.repaint ();

       	_sendWarningLabel2.setEnabled (enable);
       	_sendWarningLabel2.repaint ();        
       
       	_graceLoginLabel1.setEnabled (enable);
       	_graceLoginLabel1.repaint ();

       	_tfGraceLoginValue.setEnabled (enable);
       	_tfGraceLoginValue.repaint ();

       	_graceLoginLabel2.setEnabled (enable);
       	_graceLoginLabel2.repaint ();        
    }

	private boolean fineGrainedPolicyPresent() {
		try {
			LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();

			if (ldc == null) {
				return false;
			}
			LDAPSearchConstraints cons =
				(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
			cons.setMaxResults( 0 );

			LDAPEntry entry = ldc.read(_policyspecdn, cons);
			
			if (entry != null) {
				return true;
			} else {
				return false;
			}
		 } catch (LDAPException lde) {
		 	Debug.println("PasswordPolicyPanel.fineGrainedPolicyPresent LDAPException: " +lde.toString());
		}
		return false;
	}

	protected boolean getFGCheckboxStatus() {
		return _cbFineGrainedEnabled.isSelected();
	}

	/**
	  * Returns the cn of the password plugins in a String[]
	  */
	private void findPasswordPlugins() {
		try {											
			String filter = PLUGIN_TYPE_ATTR_NAME+"="+PWDSTORAGE;
			Debug.println("PasswordPolicyPanel.findPasswordPlugins: the filter is "+filter);
			String[] attrs = { CN_ATTR_NAME, DESCRIPTION_ATTR_NAME };

			LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();

			if (ldc == null) {
				return;
			}

			LDAPSearchConstraints cons =
				(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
			cons.setMaxResults( 0 );
			
			LDAPSearchResults entries = ldc.search(DSUtil.PLUGIN_CONFIG_BASE_DN,
												 ldc.SCOPE_SUB,
												 filter, attrs, false, cons);
			Vector vector = new Vector();
			Vector vectorDesc = new Vector();
			LDAPEntry findEntry;
			LDAPAttribute name;
			while(entries.hasMoreElements()) {
				findEntry = (LDAPEntry)entries.nextElement();
				name = findEntry.getAttribute(CN_ATTR_NAME);				
				if ( name != null ) {					
					Enumeration e = name.getStringValues();
					String  nameOfPlugin= (String)e.nextElement();					
					
					/* The NS-MTA-MD5 password scheme is for comparision only and for backward
					   compatibility with an Old Messaging Server that was setting passwords in the
					   directory already encrypted. The scheme cannot and don't encrypt password if
					   they are in clear.
					   We don't take it */
					if (nameOfPlugin.equalsIgnoreCase("ns-mta-md5")) {
						continue;
					}
					vector.addElement(nameOfPlugin.toLowerCase());

					String descriptionOfPlugin = "";
					name = findEntry.getAttribute(DESCRIPTION_ATTR_NAME);
					if (name != null) {
					    e = name.getStringValues();
						descriptionOfPlugin= (String)e.nextElement();
					}
					vectorDesc.addElement(descriptionOfPlugin);					
				}
				
			}

			/* Searching for the current used password policy */						
			String[] attr = { STORAGE_SCHEME_ATTR_NAME };
		
			findEntry = DSUtil.readEntry(ldc, MANAGER_DN, attr, cons);
			if (findEntry != null) {
				name = findEntry.getAttribute(STORAGE_SCHEME_ATTR_NAME);				
				if ( name != null ) {					
					Enumeration e = name.getStringValues();
					String  nameOfPlugin= (String)e.nextElement();					
					
					nameOfPlugin = nameOfPlugin.toLowerCase();
					
					int index = vector.indexOf(nameOfPlugin);
					/* This is done because the first element in the combo is the one that is selected (we have to select the
					   password policy that we have in cn = config) */
					if (index >= 0) {
						Object objectAtPos0 = vector.elementAt(0);					
						vector.set(index, objectAtPos0);
						vector.set(0, nameOfPlugin);
						
						Object descAtPos0 = vectorDesc.elementAt(0);
						Object descAtIndex = vectorDesc.elementAt(index);
						vectorDesc.set(index, descAtPos0);
						vectorDesc.set(0, descAtIndex);
					}
				}
			}
			COMBO_ENTRIES = new String[vector.size()];
			vector.toArray(COMBO_ENTRIES);
			COMBO_DESCRIPTION = new String[vectorDesc.size()];
			vectorDesc.toArray(COMBO_DESCRIPTION);
		} catch (LDAPException ex) {
			Debug.println("PasswordPolicyPanel.findPasswordPlugins: "+ex);
		}	
	
	}

    private JCheckBox       _cbFineGrainedPolicy;
	private JCheckBox		_cbFineGrainedEnabled;

    private JCheckBox       _cbReset;
    private JCheckBox       _cbMayChange;
    
    private JCheckBox       _cbCheckSyntax;

    private JLabel          _minLengthLabel;
    private JTextField      _tfMinLengthValue;
    private JLabel          _minDigitsLabel;
    private JTextField      _tfMinDigitsValue;
    private JLabel          _minAlphasLabel;
    private JTextField      _tfMinAlphasValue;
    private JLabel          _minUppersLabel;
    private JTextField      _tfMinUppersValue;
    private JLabel          _minLowersLabel;
    private JTextField      _tfMinLowersValue;
    private JLabel          _minSpecialsLabel;
    private JTextField      _tfMinSpecialsValue;
    private JLabel          _min8bitLabel;
    private JTextField      _tfMin8bitValue;
    private JLabel          _maxRepeatsLabel;
    private JTextField      _tfMaxRepeatsValue;
    private JLabel          _minCategoriesLabel;
    private JTextField      _tfMinCategoriesValue;
    private JLabel          _minTokenLengthLabel;
    private JTextField      _tfMinTokenLengthValue;

    private JRadioButton    _rbNever;
    private JRadioButton    _rbExpiresIn;
    private JTextField      _tfExpiresInValue;
    private JTextField      _tfSendWarningValue;
    private JTextField      _tfGraceLoginValue;    
    private JLabel          _expiresInLabel;
    private JLabel          _sendWarningLabel1;
    private JLabel          _sendWarningLabel2;
    private JLabel          _graceLoginLabel1;
    private JLabel          _graceLoginLabel2;
	private JLabel			_minAgeLabel1;
	private JLabel			_minAgeLabel2;

    private JTextField      _tfMinAgeValue;

    private JCheckBox       _cbKeepHistory;
    private JTextField      _tfRememberValue;
    private JLabel          _rememberLabel1;
    private JLabel          _rememberLabel2;

    private JRadioButton    _rbNoLockout;
    private JRadioButton    _rbLockout;
    private JTextField      _tfLockoutValue;
    private JTextField      _tfResetAfterValue;
    private JRadioButton    _rbLockoutForever;
    private JRadioButton    _rbLockoutMinutes;
    private JTextField      _tfLockoutMinutesValue;
    private JLabel _lStorageScheme;
    private JComboBox _cbStorageScheme;

	private JTabbedPane _tabbedPanel;

	// Flag that tells us if the PWP spec entry exists or not
	private boolean _spec_exists = false;

	// Target dn for fine grained password policy
	private String _dn;

	// Target dn's parent
	private String _parentdn;

	// dn of password policy container for target dn
	private String _containerdn;

	// dn of password specification object for target dn
	private String _policyspecdn;

	// dn of CoS template, for subtree password policy
	private String _costemplatedn;

	// dn of CoS specification, for subtree password policy
	private String _cosspecdn;

	// What mode the panel is in: SERVER or FINEGRAINED
	private int _mode;

	public static final int NO_PWP = 0;
	public static final int USER_PWP = 1;
	public static final int SUBTREE_PWP = 2;
	public static final int BOTH_PWP = 3;

	public static final int SERVER = 1;
	public static final int FINEGRAINED_USER = 2;
	public static final int FINEGRAINED_SUBTREE = 3;

	private static final String CONTAINER_CN = "nsPwPolicyContainer";
	private static final String COS_SPEC_CN = "nsPwPolicy_CoS";
	private static final String USER_SPEC_CN = "nsPwPolicyEntry";
	private static final String COS_TEMPL_CN = "nsPwTemplateEntry";
	private static final String PWPSUBENTRY_ATTR_NAME = "pwdpolicysubentry";

   private static final String FINE_GRAINED_POLICY_ATTR_NAME = "nsslapd-pwpolicy-local";

	private static final String CONFIG_DN = "cn=config";

    private static final String CHANGE_DN = "cn=config";
	private static final String MUST_CHANGE_ATTR_NAME = "passwordMustChange";
    private static final String CHANGE_ATTR_NAME = "passwordChange";

    private static final String SYNTAX_DN = "cn=config";
    private static final String SYNTAX_ATTR_NAME =
        "passwordCheckSyntax";

    private static final String MINLENGTH_DN = "cn=config";
    private static final String MINLENGTH_ATTR_NAME = "passwordMinLength";
    private static final int MINLENGTH_MIN_VAL = 2;
    private static final int MINLENGTH_MAX_VAL = 512;
    private static final String MINLENGTH_DEFAULT_VAL = "8";

    private static final String MINDIGITS_DN = "cn=config";
    private static final String MINDIGITS_ATTR_NAME = "passwordMinDigits";
    private static final int MINDIGITS_MIN_VAL = 0;
    private static final int MINDIGITS_MAX_VAL = 64;
    private static final String MINDIGITS_DEFAULT_VAL = "0";

    private static final String MINALPHAS_DN = "cn=config";
    private static final String MINALPHAS_ATTR_NAME = "passwordMinAlphas";
    private static final int MINALPHAS_MIN_VAL = 0;
    private static final int MINALPHAS_MAX_VAL = 64;
    private static final String MINALPHAS_DEFAULT_VAL = "0";

    private static final String MINUPPERS_DN = "cn=config";
    private static final String MINUPPERS_ATTR_NAME = "passwordMinUppers";
    private static final int MINUPPERS_MIN_VAL = 0;
    private static final int MINUPPERS_MAX_VAL = 64;
    private static final String MINUPPERS_DEFAULT_VAL = "0";

    private static final String MINLOWERS_DN = "cn=config";
    private static final String MINLOWERS_ATTR_NAME = "passwordMinLowers";
    private static final int MINLOWERS_MIN_VAL = 0;
    private static final int MINLOWERS_MAX_VAL = 64;
    private static final String MINLOWERS_DEFAULT_VAL = "0";

    private static final String MINSPECIALS_DN = "cn=config";
    private static final String MINSPECIALS_ATTR_NAME = "passwordMinSpecials";
    private static final int MINSPECIALS_MIN_VAL = 0;
    private static final int MINSPECIALS_MAX_VAL = 64;
    private static final String MINSPECIALS_DEFAULT_VAL = "0";

    private static final String MIN8BIT_DN = "cn=config";
    private static final String MIN8BIT_ATTR_NAME = "passwordMin8bit";
    private static final int MIN8BIT_MIN_VAL = 0;
    private static final int MIN8BIT_MAX_VAL = 64;
    private static final String MIN8BIT_DEFAULT_VAL = "0";

    private static final String MAXREPEATS_DN = "cn=config";
    private static final String MAXREPEATS_ATTR_NAME = "passwordMaxRepeats";
    private static final int MAXREPEATS_MIN_VAL = 0;
    private static final int MAXREPEATS_MAX_VAL = 64;
    private static final String MAXREPEATS_DEFAULT_VAL = "0";

    private static final String MINCATEGORIES_DN = "cn=config";
    private static final String MINCATEGORIES_ATTR_NAME = "passwordMinCategories";
    private static final int MINCATEGORIES_MIN_VAL = 1;
    private static final int MINCATEGORIES_MAX_VAL = 5;
    private static final String  MINCATEGORIES_DEFAULT_VAL = "3";

    private static final String MINTOKENLENGTH_DN = "cn=config";
    private static final String MINTOKENLENGTH_ATTR_NAME = "passwordMinTokenLength";
    private static final int MINTOKENLENGTH_MIN_VAL = 1;
    private static final int MINTOKENLENGTH_MAX_VAL = 64;
    private static final String MINTOKENLENGTH_DEFAULT_VAL = "3";

    private static final String EXPIRES_DN = "cn=config";
    private static final String EXPIRES_ATTR_NAME = "passwordExp";
    private static final String EXPIRES_AGE_DN = "cn=config";
    private static final String EXPIRES_AGE_ATTR_NAME = "passwordMaxAge";
    private static final int EXPIRES_AGE_MIN_VAL = 1;
    // max days that fit an int when converted to seconds
    private static final int EXPIRES_AGE_MAX_VAL = 24855;
    private static final String EXPIRES_WARNING_DN = "cn=config";
    private static final String EXPIRES_WARNING_ATTR_NAME = 
	"passwordWarning";
    private static final int EXPIRES_WARNING_MIN_VAL = 0;
    private static final int EXPIRES_WARNING_MAX_VAL = 24855; 

    private static final String GRACE_LOGIN_DN = "cn=config";
    private static final String GRACE_LOGIN_ATTR_NAME = 
	"passwordGraceLimit";
    private static final int GRACE_LOGIN_MIN_VAL = 0;
    private static final int GRACE_LOGIN_MAX_VAL = 2147483647;     
    
    private static final String MIN_AGE_DN = "cn=config";
    private static final String MIN_AGE_ATTR_NAME = "passwordMinAge";
	private static final int MIN_AGE_MIN_VAL = 0;
    private static final int MIN_AGE_MAX_VAL = 24855;

    private static final String HISTORY_DN = "cn=config";
    private static final String HISTORY_ATTR_NAME =
        "passwordHistory";
    private static final String HISTORY_NUM_DN = "cn=config";
    private static final String HISTORY_NUM_ATTR_NAME =
        "passwordInHistory";
    private static final int HISTORY_NUM_MIN_VAL = 2;
    private static final int HISTORY_NUM_MAX_VAL = 24;

    private static final int DAY_TO_SEC_FACTOR = 86400;

	private static final String STORAGE_DN = "cn=config";
	private static final String STORAGE_SCHEME_ATTR_NAME =
	                                              "passwordStorageScheme";

	private static final String PLUGIN_TYPE_ATTR_NAME = "nsslapd-plugintype";
	private static final String DESCRIPTION_ATTR_NAME = "nsslapd-plugindescription";
	private static final String PWDSTORAGE = "pwdstoragescheme";
	private static final String CN_ATTR_NAME = "cn";
	private static final String MANAGER_DN = "cn=config";
    /* NOTE: The following are the attribute values that get sent to
       the Directory, not what the user sees. So they should NOT be
       localized or changed, unless the Directory Server is changed.
       To change what the user sees, edit the properties file. */
	private String[] COMBO_ENTRIES;
	private String[] COMBO_DESCRIPTION;
	private final String[] COMBO_ENTRIES_HARDCOPY = { "sha",
													"clear",
													"crypt"
													};

	private ResourceSet _resource = DSUtil._resource;
	private final String[] COMBO_DESCRIPTION_HARDCOPY = { _resource.getString(_section+"-storageScheme","1"),
													_resource.getString(_section+"-storageScheme","2"),
													_resource.getString(_section+"-storageScheme","3")
													};
	// section in resource file
    private static final String _section = "passwordpolicy";
}

/**
 * This class is the DSEntry class for the warning text field.
  * It does the validations of the DSEntryInteger class AND compares
  * the value of the 'send warning' field to that of the 'expires in' field.
  * if the warning value is bigger than the value of the the expires value, the
  * value is considered non valid.
  */

class DSEntryWarning extends DSEntryInteger {
 	JTextField _tfExpiresIn;
	public DSEntryWarning( String model,
						   JTextField view1, 
						   JComponent view2,
						   int minValue,
						   int maxValue,
						   int scaleFactor,
						   JTextField tfExpiresIn) {
		super(model,
			  view1,
			  view2,
			  minValue,
			  maxValue,
			  scaleFactor);
 		_tfExpiresIn = tfExpiresIn;
	}
	
	/**
	  * Overwrites the DSEntryInteger.doValidate() to compare the two values
	  * of the fields
	  */
	public int doValidate() {
		int result = super.doValidate();
		boolean isValid = (result == DSE_VALID_NOMOD) || (result == DSE_VALID_MOD);
		if (isValid && _tfExpiresIn.isEnabled()) {
			try {
				int expiresIn = Integer.parseInt(_tfExpiresIn.getText());
				JTextField tf = (JTextField)getView(0);
				int sendWarning = Integer.parseInt(tf.getText());
				if (sendWarning > expiresIn) {
					result = (result == DSE_VALID_NOMOD)?DSE_INVALID_NOMOD:DSE_INVALID_MOD;
				}
			} catch ( NumberFormatException e) {
			}
		}
		return result;
	}            
}
