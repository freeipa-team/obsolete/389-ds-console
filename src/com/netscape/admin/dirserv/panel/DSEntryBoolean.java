/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import javax.swing.AbstractButton;
import javax.swing.JComponent;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server
 *
 * @version
 * @author
 */
public class DSEntryBoolean extends DSEntry {
    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle boolean choice widgets whose value is represented in String
     * form by either a "0" for false or a "1" for true.  The view should
     * be some subclass of AbstractButton, like JRadioButton or
     * JCheckBox.
     *
     * @param model the initial value, if any, or just null
     * @param view the AbstractButton representing the value
     */
    public DSEntryBoolean(String model, AbstractButton view) {
        super(model, view);
    }
    
    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle boolean choice widgets whose value is represented in String
     * form by either a "0" for false or a "1" for true.  The view should
     * be some subclass of AbstractButton, like JRadioButton or
     * JCheckBox.
     *
     * @param model the initial value, if any, or just null
     * @param view the AbstractButton representing the value
     */
    public DSEntryBoolean(String model, AbstractButton view1,
						  JComponent view2) {
        super(model, view1, view2);
    }
    

    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle boolean choice widgets whose value is represented in String
     * form by either a "0" for false or a "1" for true.  The view should
     * be some subclass of AbstractButton, like JRadioButton or
     * JCheckBox.
     *
     * @param model the initial value, if any, or just null
     * @param view the AbstractButton representing the value
	 * @param trueValue The String that represents on/true
	 * @param falseValue The String that represents off/false
     */
    public DSEntryBoolean(String model, AbstractButton view,
				   String trueValue, String falseValue ) {
        super(model, view);
		_trueValue = trueValue;
		_falseValue = falseValue;
    }
    
    /**
     * The model should be either "0" or "1".  Set the button to selected
     * if "1" or unselected if "0".
     */
    public void show() {
        String val = getModel(0);
		AbstractButton ab = (AbstractButton)getView(0);
		ab.setSelected(val.equals(_trueValue));

        viewInitialized ();
	}

	/**
     * Since data is update dynamically when it changes
     * through updateModel method, nothing needs to be done here.
     */

	public void store () {
	}
	
	/**
	 * Set the model to "1" if the button is selected or "0" otherwise
	 */
	protected void updateModel() {
		AbstractButton ab = (AbstractButton)getView(0);
		if (ab.isSelected())
			setModelAt(_trueValue, 0);
		else
			setModelAt(_falseValue, 0);
	}

	/**
	 * Just return 0; the button's state is always valid . . .
	 */
	public int validate() {return 0;}

	protected void setInitModel () {
		String value = getModel(0);
	
		if (value == null || value.equals("") || value.equals("0"))
			setModelAt(_falseValue, 0);

		if (value.equals("1"))
			setModelAt(_trueValue, 0);
	}

	protected String _trueValue = "on";
	protected String _falseValue = "off";
}
