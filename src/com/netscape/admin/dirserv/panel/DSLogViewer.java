/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;
import javax.swing.table.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.components.Table;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.logging.*;
import netscape.ldap.*;


public class DSLogViewer extends BlankPanel
    implements TableModelListener {

    public DSLogViewer( IDSModel dsmodel, DSLogViewerModel model,
			String dn, String attrName, String listAttrName ) {
	super( dsmodel, false );

	_configDN = dn;
	_configAttr = attrName;
        _configListAttr = listAttrName;

	JPanel panel = new JPanel();
	panel.setLayout( new GridBagLayout() );

	/* Add a combo box for log selection? */
	if ( (dn != null) && (attrName != null) ) {
	    createFileSelectionArea( panel );
	}

	/* Area to select number of lines, and a filter */
	createLineCountArea( panel );

	/* Pane to display results */
	_table = createTable( model );
	/* Log contents are to be displayed in "courier" */
	_table.setFont( new Font( "Monospaced", Font.PLAIN, 10 ) );
	JScrollPane jscrollpane = new JScrollPane( _table );
	jscrollpane.setMinimumSize(new Dimension(300,200));

	_myPanel.setLayout( new GridBagLayout() );
	GridBagConstraints gbc = getGBC();
	gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
	gbc.weightx = 1;
	gbc.weighty = 0;
	gbc.anchor = gbc.WEST;
	_myPanel.add( panel, gbc );
	gbc.weighty = 1;
	gbc.fill = gbc.BOTH;
	//		int different = UIFactory.getDifferentSpace();
	gbc.insets = new Insets(0,0,0,0);
	_myPanel.add( jscrollpane, gbc );
	refreshFileSelector();
    }

    public DSLogViewer( IDSModel dsmodel, DSLogViewerModel model ) {
	this( dsmodel, model, null, null, null );
    }

    protected boolean refreshFileSelector() {
        _cbFilename.removeItemListener(this);
        _cbFilename.removeAllItems();
        LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
	String attrs[] = { _configAttr, _configListAttr };
        String logfile;
        int i;
	try {
	    LDAPEntry entry = ldc.read( _configDN, attrs );
	    if ( entry != null ) {
		LDAPAttribute attr = entry.getAttribute( attrs[0] );
		if ( attr != null ) {
		    Enumeration en = attr.getStringValues();
		    while ( en.hasMoreElements() ) {
                        // Chop off the path.  We only want the filename.
                        logfile = (String)en.nextElement();
                        if ((i = logfile.lastIndexOf('/')) >= 0 ) {
                            // Make sure we're not at the end of the string
                            if ( logfile.length() > (i + 1) ) {
                                logfile = logfile.substring(i + 1);
                            } else {
                                logfile = "";
                            }
                        }

                        // Check if string is empty before adding
                        if (logfile.length() > 0) {
                            _cbFilename.addItem(logfile);
                        }
		    }
		}
		attr = entry.getAttribute( attrs[1] );
		if ( attr != null ) {
		    Enumeration en = attr.getStringValues();
		    while ( en.hasMoreElements() ) {
                        // Chop off the path.  We only want the filename.
                        logfile = (String)en.nextElement();
                        if ((i = logfile.lastIndexOf('/')) >= 0 ) {
                            // Make sure we're not at the end of the string
                            if ( logfile.length() > (i + 1) ) {
                                logfile = logfile.substring(i + 1);
                            } else {
                                logfile = "";
                            }
                        }

                        // Check if string is empty before adding
                        if (logfile.length() > 0) {
                            _cbFilename.addItem(logfile);
                        }
		    }
		}
	    } else {
		return false;
	    }
	} catch ( LDAPException e ) {
	    Debug.println(
			  "AccessLogContentPanel.refreshFileSelector: " + e );
	    return false;
	}
	_cbFilename.setSelectedIndex( 0 );
        if (_table != null)
            ((DSLogViewerModel)_table.getModel()).setLogFileName(
								 (String)_cbFilename.getSelectedItem() );
        _cbFilename.addItemListener(this);
	return true;
    }

    private void createFileSelectionArea( JPanel parent ) {
    	_cbFilename = makeJComboBox();
	JPanel panel = new JPanel();
	panel.setLayout(new GridBagLayout());
	JLabel label = makeJLabel( _section, "selectlog" );
	label.setLabelFor(_cbFilename);
	_myPanel.setLayout( new GridBagLayout() );

	GridBagConstraints gbc = getGBC();
	gbc.gridwidth = 1;
	gbc.fill = gbc.NONE;
	gbc.weightx = 0;
	gbc.weighty = 1;
	gbc.insets = new Insets(0,0,0,0);
	gbc.anchor = gbc.WEST;
	panel.add(label, gbc);
	panel.add( Box.createHorizontalStrut(UIFactory.getComponentSpace()), gbc );
	gbc.fill = gbc.HORIZONTAL;
	gbc.weightx = 1;
	gbc.weighty = 1;
	gbc.anchor = gbc.WEST;
	panel.add(_cbFilename, gbc);

	gbc = getGBC();
	gbc.gridheight = 1;
	gbc.gridx = 0;
	gbc.gridy = _y;
	gbc.insets = new Insets(0,0,0,0);
	gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
	gbc.weightx = 1;
	//		gbc.fill = gbc.NONE;
	//		gbc.weightx = 0;
	gbc.anchor = gbc.WEST;
	parent.add(panel, gbc);
	_y++;
    }
		
    private void createLineCountArea( JPanel parent ) {
	/* Fields for setting the number of lines to fetch */
	JPanel panel = new JPanel();
	panel.setLayout( new GridBagLayout() );
	int space = UIFactory.getComponentSpace();

	GridBagConstraints gbc = getGBC();
	gbc.gridwidth = 1;
	gbc.fill = gbc.NONE;
	gbc.weightx = 0;
	gbc.weighty = 1;
	gbc.insets = new Insets(6,0,6,0);
	gbc.anchor = gbc.WEST;

	JLabel label = makeJLabel( _section, "lines" );
	panel.add(label, gbc);
	panel.add( Box.createHorizontalStrut(space), gbc );

	_tfLines = makeJTextField( _section, "lines" );
	gbc.fill = gbc.HORIZONTAL;
	gbc.weightx = 0;
	label.setLabelFor(_tfLines);

	panel.add(_tfLines, gbc);
	panel.add( Box.createHorizontalStrut(space), gbc );
	panel.add( Box.createHorizontalStrut(space), gbc );

	label = makeJLabel( _section, "filter" );
	gbc.fill = gbc.NONE;
	gbc.weightx = 0;
	panel.add(label, gbc);
	panel.add( Box.createHorizontalStrut(space), gbc );

	_tfFilter = makeJTextField( _section, "filter" );
	gbc.fill = gbc.HORIZONTAL;
	gbc.weightx = 1;
	label.setLabelFor(_tfFilter);
	panel.add(_tfFilter, gbc);
	gbc.fill = gbc.NONE;
	gbc.weightx = 0;
	//		panel.add( Box.createHorizontalStrut(space), gbc );

	gbc = getGBC();
	gbc.gridheight = _y + 1;
	gbc.gridx = 0;
	gbc.gridy = _y;
	//		int different = UIFactory.getDifferentSpace();
	//		gbc.insets = new Insets(0,different,0,different);
	gbc.insets = new Insets(0,0,0,0);
	gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
	gbc.weightx = 1;
	//		gbc.fill = gbc.NONE;
	//		gbc.weightx = 0;
	gbc.anchor = gbc.WEST;
	parent.add(panel, gbc);
	_y++;
    }

    private JTable createTable( DSLogViewerModel model ) {
	Table table = new Table(model);
	table.setAutoCreateColumnsFromModel(true);
	table.setShowGrid(true);
	table.getTableHeader().setReorderingAllowed(false);
	table.setRowSelectionAllowed(false);
	table.getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "table-description"));
	// Defaults :
	// table.setColumnSelectionAllowed(false);
	// table.setAutoResizeMode(Table.AUTO_RESIZE_DATA);

	table.getModel().addTableModelListener( this );
	
	return table;
    }

    protected int getTotalColumnWidth( JTable table ) {
	Enumeration en = table.getColumnModel().getColumns();
	int width = 0;
	while( en.hasMoreElements() ) {
	    TableColumn col = (TableColumn)en.nextElement();
	    width += col.getWidth();
	}
	return width;
    }

    /**
     * Called when the table structure has changed.
     *
     * @param e Event describing the changes.
     */
    public void tableChanged(TableModelEvent e) {
    }

    public JTable getTable() {
	return _table;
    }

    public boolean refresh() {
        refreshFileSelector();
	return updateContents();
    }

    public void scrollToEnd (){
        Rectangle rectLastRow = _table.getCellRect (_table.getRowCount()-1, 0, false);
        _table.scrollRectToVisible (rectLastRow);
    }
    boolean updateContents() {
	DSLogViewerModel model = (DSLogViewerModel)getTable().getModel();
	Debug.println( "DSLogViewer.updateContents: " +
		       model.getLogFileName() );
	String filter = _tfFilter.getText();
	if ( filter.length() < 1 )
	    filter = null;
	model.setFilter( filter );
	String countString = _tfLines.getText();
	if ( countString.length() > 0 ) {
	    int newLineCount;
	    try {
	    	newLineCount = Integer.parseInt( countString );
	    }
	    catch(NumberFormatException x) {
	    	// _tfLines contains an invalid number
		// -1 will force the reset of _tfLines.
		newLineCount = -1;
	    }
	    // Update the model or reset _tfLines
	    if (newLineCount >= 1)
	    	model.setLineCount(newLineCount);
	    else
	        _tfLines.setText( Integer.toString( model.getLineCount() ));
	}
	model.updateNow();
	return true;
    }

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void itemStateChanged(ItemEvent e) {
        if (e.getSource().equals(_cbFilename)) {
	    int index = _cbFilename.getSelectedIndex();
	    //Debug.println( "DSLogViewer.itemStateChanged: selected " +
	    //index );
	    if ( index >= 0 ) {
		String name = (String)_cbFilename.getItemAt( index );
		if ( (name != null) && (_table != null) ) {
		    DSLogViewerModel model =
			(DSLogViewerModel)_table.getModel();
		    model.setLogFileName( name );
		    updateContents();
		}
	    }
	} else {
	    super.itemStateChanged(e);
	}
    }
	
    private JTable _table = null;
    private boolean _initialized = false;
    private int _y = 0;
    private JTextField _tfLines;
    private JTextField _tfFilter;
    private JComboBox _cbFilename;
    private String _configDN = null;
    private String _configAttr = null;
    private String _configListAttr = null;
    private ResourceSet _resource = DSUtil._resource;
    static private final String _section = "logcontent";
}
