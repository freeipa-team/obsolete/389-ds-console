/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import netscape.ldap.*;
import javax.swing.JOptionPane;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.util.Debug;

/**
 * This is really just a collection of DSEntry objects.  The DSEntrySet object
 * manages the DSEntry objects on behalf of an application.  Each entry is
 * represented by its DN relative to the application suffix.  This root
 * suffix must be supplied by the application when the DSEntrySet show() and
 * store() methods are called.  An entry may also have an absolute DN instead
 * of a relative DN.  Some caveats:  Attribute names must be unique, even if
 * they have different DNs.
 *
 * @version %I%, %G%
 * @author Richard Megginson
 * @see com.netscape.admin.dirserv.panel.BlankPanel
 * @see com.netscape.admin.dirserv.panel.DSEntry
 **/
public class DSEntrySet {
    /**
     * Constructs a new DSEntrySet, with validation that attributes can
	 * be read from the Directory
     */
    public DSEntrySet() {
		this( true, false );
    }

    /**
     * Constructs a new DSEntrySet
	 *
	 * @param validate If true, specifies that the user should be prompted
	 * to reauthenticate if attributes cannot be read
     */
    public DSEntrySet( boolean validate ) {
		this( validate, false );
	}

    /**
     * Constructs a new DSEntrySet
	 *
	 * @param validate If true, specifies that the user should be prompted
	 * to reauthenticate if attributes cannot be read
	 * @param allowNoEntry If true, just ignore the fact that there may be
	 * no actual entry yet in the configuration DIT for this configuration
	 * item.  So, when showing, there will be no data.  However, this must
	 * cause an error if attempting to save.  The user will need to create
	 * the entry before saving the changes back to the directory.
     */
    public DSEntrySet( boolean validate, boolean allowNoEntry ) {
		_validate = validate;
		_allowNoEntry = allowNoEntry;
        _attributeNameToDNMap = new Hashtable();
        _dnToAttributeNameArrayMap = new Hashtable();
        _attrNametoDSAVectorMap = new Hashtable();
        _DSAToAttributeMap = new Hashtable();
	}

    /**
     * Constructs a new DSEntrySet
	 *
	 * @param validate If true, specifies that the user should be prompted
	 * to reauthenticate if attributes cannot be read
	 * @param allowNoEntry If true, just ignore the fact that there may be
	 * no actual entry yet in the configuration DIT for this configuration
	 * item.  So, when showing, there will be no data.  However, this must
	 * cause an error if attempting to save.  The user will need to create
	 * the entry before saving the changes back to the directory.
	 * @param newEntry The entry to store to the directory may not yet
	 * exist.  If newEntry is non-null, it is entry to add, complete with
	 * the DN and any default attributes and values (e.g. objectclasses)
	 * to add with the entry.
     */
    public DSEntrySet( boolean validate, boolean allowNoEntry,
					   LDAPEntry newEntry ) {
		_validate = validate;
		_allowNoEntry = allowNoEntry;
        _attributeNameToDNMap = new Hashtable();
        _dnToAttributeNameArrayMap = new Hashtable();
        _attrNametoDSAVectorMap = new Hashtable();
        _DSAToAttributeMap = new Hashtable();
		_newEntry = newEntry;
	}

    /**
     * This method is called to add a new DSEntry to the DSEntrySet.  No
     * checking is done on the parameters, but if there are invalid values
     * given here, expect a lot of errors to come out of show() and store().
     *
     * @param entryDN the name of the entry in the DS e.g. "cn=foo, o=bar".
     * If this is a relative DN, the suffix to use must be passed in as part
     * of the show() and store() methods.
     * @param attributeName the name of the attribute e.g. "passwordchange".
     * The attribute name must be unique
     * @param dse the DSEntry representing this attribute
     */
    public void add(String entryDN, String attributeName, IDSEntry dse) {
        _dnToAttributeNameArrayMap.put(entryDN, new Boolean(false));
        _attributeNameToDNMap.put(attributeName, entryDN);
		String key = entryDN + ':' + attributeName;
        _DSAToAttributeMap.put(dse, key.toLowerCase());
        if (!_attrNametoDSAVectorMap.containsKey(attributeName)) {
            _attrNametoDSAVectorMap.put(attributeName, new Vector());
        }
        Vector v = (Vector)_attrNametoDSAVectorMap.get(attributeName);
        v.addElement(dse);
    }

	/**
	 * Remove all entries
	 */
    public void removeAll() {
        _dnToAttributeNameArrayMap.clear();
        _attributeNameToDNMap.clear();
        _DSAToAttributeMap.clear();
        _attrNametoDSAVectorMap.clear();
	}

	/**
	 * Try to get all attribute values. Return false if any of them
	 * cannot be read.
	 */
    private boolean getAttributes( IDSModel model ) throws LDAPException {
        LDAPConnection ldc = model.getServerInfo().getLDAPConnection();

        for (Enumeration e = _dnToAttributeNameArrayMap.keys();
             e.hasMoreElements(); ) {
            String dn = (String)e.nextElement();
            LDAPEntry entry = null;
            // collect all attribute names for this entry into an array
            String[] attrNames = getAttributeNames(dn);
            Debug.println(9, "DSEntrySet.getAttributes(): attempting to read entry " +
                           dn + " from the server attrNames = " + attrNames);
            try {
                entry = ldc.read(dn, attrNames);
            } catch (LDAPException lde) {
                if ( lde.getLDAPResultCode() == lde.NO_SUCH_OBJECT && _allowNoEntry ) {
                    entry = null;
                } else {
                    throw lde;
                }
            }

            if ( entry == null && !_allowNoEntry ) {
                Debug.println( "DSEntrySet.getAttribytes: failed to read " + dn );
                return false;
            } else {
                // Debug.println( "DSEntrySet.getAttributes(): entry = " + entry );
            }

            Debug.println("DSEntrySet.getAttributes(): read entry from DS:" + entry);
            Debug.println("DSEntrySet.getAttributes(): attributes for this entry:" + attrNames);

            // now we have the entry; get the LDAPAttributes
            boolean haveAllAttrs = true; // flag if any attribues were missing
            for (int ii = 0; ii < attrNames.length; ++ii) {
                String attrName = attrNames[ii];
                LDAPAttribute lda = null;
                if (entry != null) {
                    lda = entry.getAttribute(attrName);
                }

                if (lda == null && entry != null) {
                    Debug.println( "DSEntrySet.getAttributes(): failed to get " +
                                   "attribute " + attrName + " in " + entry.getDN());
                    // missing at least one attr value
                    // haveAllAttrs = false;
                    // Instead of returning an auth error, create an empty attr
                    Debug.println( "DSEntrySet.getAttributes(): created " +
                                   "attribute " + attrName + " in " + entry.getDN());
                    lda = new LDAPAttribute(attrName, ""); 
                }

                // loop through all of the DSEntrys which
                // correspond to this attribute name and do the
                // remoteToLocal for each one that was found
                Vector v = (Vector)_attrNametoDSAVectorMap.get(attrName);
                for (int jj = 0; v != null && jj < v.size(); ++jj) {
                    IDSEntry dse = (IDSEntry)v.elementAt(jj);
                    if (lda != null) {
                        Debug.println("DSEntrySet.getAttributes(): " + attrName  + 
                                      " was found, setting value from entry");
                        dse.remoteToLocal(lda.getStringValues());
                    }

                    // Debug.println("DSEntrySet.show(): set DSE" +
                    //               " for attribute" + attrName +
                    //               " to value=" + lda);
                    // Debug.println("DSEntrySet.show(): new val=" + dse);

                    dse.show();
                }
            }

            if (!haveAllAttrs) {
                Debug.println("DSEntrySet.show(): some of the attributes of " +
                               entry.getDN() + " could not be read.  Either they " +
                              "are not present in the entry or there is an ACI " +
                              "which prevents that attribute from being read. " +
                              "Try authenticating as a user with more access");
                if (_validate)
                    return false;
                }
            }

            return true;
    }

    /**
     * This method first reads the contents of each directory entry in its
     * list.  It only reads the attributes it is concerned about.  Next, it
     * updates the internal model in each DSEntry.  Finally, it calls the
     * show() method in each DSEntry to update the view (GUI) of each DSEntry.
     *
     * @param model The application's resource model.  This should contain the
     * suffix used by the application and an LDAPConnection.  The
     * LDAPConnection may change if the current connection does not have
     * permission to view the entries and DSEntrySet needs to reauthenticate
     * to the server.
     * @exception netscape.ldap.LDAPException thrown by the LDAPConnection
     * object if an LDAP error occurs
     * @see com.netscape.admin.dirserv.panel.DSEntry#show
     * @see com.netscape.admin.dirserv.panel.BlankPanel#select
     */
    public void show( IDSModel model ) throws LDAPException {
		Debug.println(9, "DSEntrySet.show: model=" + model);
		LDAPConnection ldc = model.getServerInfo().getLDAPConnection();
		Debug.println(9, "DSEntrySet.show: ldc=" + DSUtil.format(ldc));

		/* Loop to allow reauthenticating if necessary */
		boolean done = false;
		while( !done ) {
			try {
				done = getAttributes( model );
			} catch (LDAPException lde) {
				Debug.println("DSEntrySet.show(): LDAP error code=" +
							  lde.getLDAPResultCode() + " error=" +
							  lde);
				if ( lde.getLDAPResultCode() == lde.NO_SUCH_OBJECT &&
					 _allowNoEntry ) {
					done = true;
				} else if ( lde.getLDAPResultCode() !=
					 lde.INSUFFICIENT_ACCESS_RIGHTS ) {
					model.notifyAuthChangeListeners();
					throw lde; // rethrow the error
                }
			}
			if ( !done ) {
				/* Check if this already is the root DN */
				String rootdn = (String)model.getServerInfo().get( "rootdn" );
				if (rootdn != null)
					done = rootdn.equalsIgnoreCase( ldc.getAuthenticationDN() );
			}
			if ( !done ) {
				// display a message
				DSUtil.showPermissionDialog( model.getFrame(), ldc );
				if (!model.getNewAuthentication(false)) {
					done = true; // error, just punt
				}
            }
        }
		model.notifyAuthChangeListeners();
    }

    /**
     * This method is called to grab all of the GUI representations of the
     * values and store them to the LDAP server.  It in essence implements
     * the Apply functionality.  store is a four phase process.  During phase
     * 1, all DSEntry views are validated.  The implementor i.e. the specific
     * panel is responsible for informing the user which field is invalid,
     * setting the focus on that field, etc.  If store() encounters an invalid
     * entry, store() will abort and return.  During phase 2, the model of the
     * entry is updated from its view i.e. the widget value is stored
     * internally.  During phase 3, the entry's internal value is converted to
     * a form usable by the LDAP server API and returned here.  During phase 4,
     * all updates to a single LDAP server entry are batched together and sent
     * to avoid sending a value for every single attribute.  An alternate would
     * be to have the localToRemote method return null if the attribute has not
     * changed and only write the changed values to the server.
     * If the store was not successful, store() will either throw an exception
     * or return false.  Usually an exception will be thrown for a hard error,
     * such as a network problem which interferes with LDAP.  The value false
     * will usually be returned as a result of a soft error, such as an
     * invalid field.  If no exception is thrown, and the value true is
     * returned, the store was successful.
     *
     * @param model the model contains the LDAPConnection object to use and
     * the application's base suffix; the LDAPConnection may be change if the
     * user does not have permission to modify the entry and needs to
     * reauthenticate to the server
     * @return true - the DS was successfully updated; false - usually means
     * that one of the views did not validate() properly
     * @exception netscape.ldap.LDAPException thrown by the LDAPConnection
     * object if an LDAP error occurs
     * @see com.netscape.admin.dirserv.panel.DSEntry#store
     * @see com.netscape.admin.dirserv.panel.BlankPanel#okCallback
     */
    public boolean store( IDSModel model ) throws LDAPException {
//        if (!reconnect(model))
//            return false;
            
        LDAPConnection ldc = model.getServerInfo().getLDAPConnection();
        // loop through our list of unique DNs
        boolean retval = false;
		boolean requiresRestart = false;
		Vector needReset = new Vector();
		for (Enumeration e = _dnToAttributeNameArrayMap.keys();
			 e.hasMoreElements(); ) {
            String dn = (String)e.nextElement();
            // loop through our list of attributes for this dn
            String[] attrList = getAttributeNames(dn);
            if (attrList == null)
                continue; // no attributes!?!?!??!

            LDAPModificationSet ldapmodset = new LDAPModificationSet();
            for (int ii = 0; ii < attrList.length; ++ii) {
                boolean dirty = false;
                // loop through all of the DSEntrys which
                // correspond to this attribute name
                String attrName = attrList[ii];
                Vector v = (Vector)_attrNametoDSAVectorMap.get(attrName);
                // first, see if the DSE's view is valid
                boolean viewsAreValid = true;
                for (int jj = 0; viewsAreValid && jj < v.size(); ++jj) {
                    IDSEntry dse = (IDSEntry)v.elementAt(jj);
                        viewsAreValid = dse.dsValidate();
                }

                // if we encountered an invalid view, just punt
                if (!viewsAreValid)
                    return retval;

                // if we got here, it means that all of the dse views are
                // valid, and we can store them . . .
                Vector values = new Vector();
                // collect all of the values from all of the DSEntrys
                // which correspond to this attribute name
                for (int jj = 0; v != null && jj < v.size(); ++jj) {
                    IDSEntry dse = (IDSEntry)v.elementAt(jj);
                    //dse.clearModel();
                    dse.store();
                    String[] sa = dse.localToRemote();
                    if (((DSEntry)dse).getDirty()) {
        				if ( !requiresRestart ) {
        					requiresRestart = DSUtil.requiresRestart( dn, attrName );
                        }
                        dirty = true; 
                        // possibly reset if save successful
                        needReset.add(dse);
                    }
                    for (int kk = 0; sa != null && kk < sa.length; ++kk) {
                        values.addElement(sa[kk]);
                    }
                }

                if (!dirty)
                    continue;

                // convert the values Vector to a String[]
                int newsize = values.size();

                String[] valArray = new String[newsize];
                values.copyInto(valArray);
                // now, use our valArray to construct the LDAPAttributes
				ldapmodset.add(LDAPModification.REPLACE,
							   new LDAPAttribute(attrName, valArray));
            }

            if (ldapmodset.size() == 0)
                return true;

            // we now have our LDAPModificationSet ready to go; this call
            // may throw an exception, so the caller is responsible for
            // catching it
            boolean done = false;
            retval = false;
			// useAdd will be true if 
			boolean useAdd = false;
			boolean haveEntry = (_newEntry != null);
            while (!done) {
                try {
					Debug.println( "DSEntrySet.store: Saving " + ldapmodset +
								   " to " + dn + " haveEntry=" + haveEntry);
					if (useAdd && haveEntry) {
						// copy the modifications in the ldapmodset to the
						// ldapentry to be added
						LDAPEntry addEntry = addMods(_newEntry, ldapmodset);
						ldc.add(addEntry);
						haveEntry = false; // already built the entry to add
						useAdd = false;
						ldapmodset = null;
					} else {
						ldc.modify(dn, ldapmodset);
						ldapmodset = null;
						// reset the fields that were dirty and were successfully
						// stored to the server - this means that if ldc.modify
						// threw an exception, we do not get to this code, and
						// therefore all of the fields will be left marked as
						// dirty and un-reset
						Enumeration nmr = needReset.elements();
						while (nmr.hasMoreElements()) {
						    DSEntry dse = (DSEntry)nmr.nextElement();
						    dse.reset(); // save was successful - reset
						}
						needReset = null;
					}
                    done = true;
                    retval = true;
                } catch (LDAPException lde) {
                    Debug.println("DSEntrySet.store(): LDAP error code=" +
								  lde.getLDAPResultCode() + " dn=" +
								  dn + " error=" + lde);
                    // if the modify failed due to permissions, notify the user
                    // via a dialog and ask the user to authenticate using a
                    // different user id/password.  If the user selects OK,
                    // popup the password dialog asking for another user id and
                    // password.  If the user hits OK, update the userid and
                    // password in the _ldc object and retry the modify.  Keep
                    // looping until: 1. The modify succeeds 2. The user hits
                    // Cancel in either the confirm dialog or the password
                    // dialog 3. we get some other type of LDAP error
					if (lde.getLDAPResultCode() ==
						LDAPException.INSUFFICIENT_ACCESS_RIGHTS) {
						// display a message
						DSUtil.showPermissionDialog( model.getFrame(), ldc );
						if (!model.getNewAuthentication(false)) {
							done = true; // error, just punt
							retval = false; // could not modify entry
						}
					} else if (lde.getLDAPResultCode() ==
							   lde.NO_SUCH_OBJECT &&
							   haveEntry) {
						useAdd = true;
					} else {
						model.notifyAuthChangeListeners();
						throw lde; // rethrow the error
					}
                }
            }
		}
		model.notifyAuthChangeListeners();
		if ( requiresRestart ) {
			DSUtil.showInformationDialog( model.getFrame(), "requires-restart",
										  (String)null ) ;
		}
        
        return retval;
    }

	String getAttributeForEntry( IDSEntry dse ) {
		return (String)_DSAToAttributeMap.get(dse);
	}

    /**
     * This function gets a list of attribute names in a form useful to
     * the LDAP classes; the String[] is stored in the _dnToAttrNameArrayMap;
     * if the array is null, it is constructed from the _attributeNameToDNMap
     * and stored in the _dnToAttrNameArrayMap; thus, we only create the
     * array once; if the attributes change, we will probably have to change
     * the code which uses this class anyway . . .
     *
     * @param dn the DN of the entry housing the attributes
     * @return an array of String attribute names for the given dn
     */
    private String[] getAttributeNames(String dn) {
        if (dn == null)
            return null;

        String[] retval = null;
        if (_dnToAttributeNameArrayMap.get(dn) instanceof Boolean) {
            // first, put all matching attributes into a Vector
            Vector v = new Vector();
            for (Enumeration e = _attributeNameToDNMap.keys();
                 e.hasMoreElements(); )
            {
                String attrName = (String)e.nextElement();
                String myDN = (String)_attributeNameToDNMap.get(attrName);
                if (myDN.equals(dn))
                    v.addElement(attrName);
            }
            int newsize = v.size();
            // convert the vector to an array of Strings
            if (newsize > 0) { // we found some matching ones . . .
                retval = new String[newsize];
                v.copyInto(retval);
                _dnToAttributeNameArrayMap.put(dn, retval);
            }
        } else {
            retval = (String[])_dnToAttributeNameArrayMap.get(dn);
        }
        return retval;
    }
    
    private boolean reconnect(IDSModel model) {
        String sieEntry = model.getConsoleInfo().getCurrentDN();
        String[] attrs = {"port", "serverhost"};
        LDAPConnection ldc = model.getConsoleInfo().getLDAPConnection();
        LDAPEntry entry = null;
        boolean status = false;
        try {
            entry = ldc.read(sieEntry, attrs);
        } catch (LDAPException lde) {
            Debug.println("DSEntrySet.reconnect(): caught exception " +
                          lde + " reading entry " + sieEntry);
        }
        if (entry == null)
            return status;
        
        String remoteHost = getAttribute(entry, "serverhost");
        String remotePort = getAttribute(entry, "port");
        int port = 0;
        try {
            port = Integer.parseInt(remotePort);
        } catch (Exception e) {
            // do nothing
        }
        
        ldc = model.getServerInfo().getLDAPConnection();
        if (((remoteHost == null) || ldc.getHost().equals(remoteHost)) &&
            ((port == 0) || ldc.getPort() == port)) {
            // already connected to correct host
            return true;
        }
        
        boolean done = false;
        while (!done) {
            try {
                ldc.connect(remoteHost, port, ldc.getAuthenticationDN(),
                            ldc.getAuthenticationPassword());
                done = true;
                status = true;
            } catch (LDAPException lde) {
                Debug.println("DSEntrySet.reconnect(): LDAP error code=" +
                                   lde.getLDAPResultCode() + " error=" +
                                   lde);
                // if the modify failed due to permissions, notify the user
                // via a dialog and ask the user to authenticate using a
                // different user id/password.  If the user selects OK,
                // popup the password dialog asking for another user id and
                // password.  If the user hits OK, update the userid and
                // password in the _ldc object and retry the modify.  Keep
                // looping until: 1. The modify succeeds 2. The user hits
                // Cancel in either the confirm dialog or the password
                // dialog 3. we get some other type of LDAP error
				if (lde.getLDAPResultCode() ==
					LDAPException.INSUFFICIENT_ACCESS_RIGHTS) {
					// display a message
					DSUtil.showPermissionDialog( model.getFrame(), ldc );
					if (!model.getNewAuthentication(false)) {
						done = true; // error, just punt
						status = false; // could not modify entry
					}
				} else { // some other ldap error
				    done = true;
				    status = false;
				}
			}
        }

		model.notifyAuthChangeListeners();
        
        return status;
    }

    private String getAttribute(LDAPEntry lde, String attrName) {
        LDAPAttribute lda = lde.getAttribute(attrName);
        if (lda != null) {
            for (Enumeration e = lda.getStringValues(); e.hasMoreElements();) {
                return (String)e.nextElement();
            }
        }
        
        return null;
    }

	// construct an AttributeSet from the modification set, merge it with the
	// attribute set from the oldEntry, add the attributes to the new entry,
	// and return the new entry
	private LDAPEntry addMods(LDAPEntry oldEntry, LDAPModificationSet ldapmodset) {
		LDAPAttributeSet las = oldEntry.getAttributeSet();
		for (int ii = 0; ii < ldapmodset.size(); ++ii) {
			LDAPModification ldm = (LDAPModification)ldapmodset.elementAt(ii);
			if (ldm.getOp() == ldm.DELETE || ldm.getOp() == ldm.REPLACE) {
				las.remove(ldm.getAttribute().getName());
			}
			if (ldm.getOp() == ldm.ADD || ldm.getOp() == ldm.REPLACE) {
				las.add(ldm.getAttribute());
			}
		}
		return new LDAPEntry(oldEntry.getDN(), las);
	}
    
    private String printArray(String[] array) {
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        if (array == null) {
            sb.append("null");
        } else {
            for (int ii = 0; ii < array.length; ++ii) {
                sb.append("{" + array[ii] + "}");
            }
        }
        sb.append("}");
        return sb.toString();
    }
    
    /**
     * Right now, just returns a constant string, but function interface
     * gives us the ability to make dynamic if needed
     */
    String getConfigDN() {return CONFIG_DN;}
    
    // This hash table stores a mapping of unique attribute names to
    // their corresponding DN; the key is the String attribute name, and
    // the value is the String dn
    private Hashtable _attributeNameToDNMap = null;

    // This hash table stores a mapping of unique dn's to an array of
    // attribute names; the key is the String dn, and the value is a
    // String[] of attribute names; the array is null until first use,
    // then it is created from the _attributeNameToDNMap.  When first
    // created, a value of new Boolean(false) is stored to the value,
    // which is later replaced with the array once all of the values have
    // been added
    private Hashtable _dnToAttributeNameArrayMap = null;

    // This hash table stores a mapping of attribute names to a vector
    // of DSEntrys, since an attribute name may be represented in
    // the client by more than one visual component; the key is the String
    // attribute name, and the value is a Vector of DSEntrys
    private Hashtable _attrNametoDSAVectorMap = null;

    // This hash table stores a mapping of DSEntrys to attribute names
    private Hashtable _DSAToAttributeMap = null;

	// If true, pop up a dialog for reauthentication if attributes cannot
	// be read from the directory
    private boolean _validate = true;

	// If true, this means that the entry holding the configuration data may not
	// yet exist, and the caller knows that, so don't complain if we try to read
	// the default values from the directory and the entry does not exist
    private boolean _allowNoEntry = true;

	// If non null, this is the entry to add if we are changing the configuration
	// for an entry that does not yet exist.  The entry should contain default
	// attributes and values (e.g. objectclasses)
	private LDAPEntry _newEntry = null;

    static final private String CONFIG_DN = "cn=config";
}
