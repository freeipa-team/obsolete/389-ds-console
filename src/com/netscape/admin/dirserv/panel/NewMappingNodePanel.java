/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.management.nmclf.*;
import netscape.ldap.*;
import netscape.ldap.util.*;


/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @see     com.netscape.admin.dirserv
 */
public class NewMappingNodePanel extends BlankPanel {

    public NewMappingNodePanel(IDSModel model, LDAPEntry node) {
	super( model, _section );
	_helpToken = "configuration-new-mapping-node-dbox-help";
	setTitle( DSUtil._resource.getString( _section, "new-title" ));
	_model = model;
	_entry = node;
    }

    public void init() {

	_myPanel.setLayout( new GridBagLayout() );
	GridBagConstraints gbc = getGBC();

	// Mapping Tree info
	createTypeArea( _myPanel );
	createValueArea( _myPanel );
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( false );
	}

    }

    protected void 	createValueArea( JPanel grid ) {
		
	GridBagConstraints gbc = getGBC();
	JPanel Mapanel = new GroupPanel( DSUtil._resource.getString( _section, "bck-title" ),
					 true);
        gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
        grid.add( Mapanel, gbc );

	GridBagLayout Mabag = new GridBagLayout();
	GridBagConstraints Magbc = new GridBagConstraints() ;
        Magbc.gridx      = 0;
        Magbc.gridy      = 0;
        Magbc.gridwidth  = Magbc.REMAINDER;
        Magbc.gridheight = 1;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
        Magbc.fill       = Magbc.BOTH;
        Magbc.anchor     = Magbc.CENTER;
        Magbc.insets     = new Insets(0, 0, 0, 0);
        Magbc.ipadx = 0;
        Magbc.ipady = 0;
	   	
	Mapanel.setLayout(Mabag);

	/* Backends list label */
	_bckListLabel = makeJLabel ( _section, "bck-list" );
	Magbc.fill= Magbc.NONE;
	Magbc.anchor = Magbc.WEST;		
	Mapanel.add( _bckListLabel,Magbc );

	/* Backend List */
	String BList[] = getBackendList();
	_bckList = new JList( BList );
	_bckList.setVisibleRowCount( ROWS );
	_bckList.setCellRenderer( new ReferralCellRenderer() );
	//		_bckList.setSelectionMode( _bckList.MULTIPLE_INTERVAL_SELECTION );
	JScrollPane scrollBck = new JScrollPane();
	scrollBck.getViewport().setView(_bckList);
	Magbc.gridy++;
	Magbc.fill= Magbc.BOTH;		
        Magbc.weightx    = 1;
        Magbc.weighty    = 1;
	Mapanel.add( scrollBck, Magbc );

	/* Referrals Label */
	_refListLabel = makeJLabel ( _section, "ref-list" );
	Magbc.gridy++;
        Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.NONE;
	Magbc.anchor = Magbc.WEST;		
	Magbc.gridy++;
	Mapanel.add( _refListLabel,Magbc);


	/* New Referrals Line */		
        _refNewRefText = makeJTextField(_section, "ref-new");
	Magbc.gridy++;
	Magbc.gridwidth  = 1;
        Magbc.weightx    = 1;
        Magbc.weighty    = 0;		
	Magbc.fill= Magbc.BOTH;
	Magbc.insets     = new Insets(0,
				      0, 
				      SuiLookAndFeel.COMPONENT_SPACE,
				      0);
	Magbc.anchor = Magbc.CENTER;
	Mapanel.add( _refNewRefText,Magbc);

	_bAdd = UIFactory.makeJButton(this, _section, "badd", (ResourceSet)null);
	_bAdd.setActionCommand(ADD_REF);		
	_bAdd.setEnabled(false);
	Magbc.gridwidth  = 1;
	Magbc.gridx++;
	Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.insets     = new Insets(0,
				      SuiLookAndFeel.DIFFERENT_COMPONENT_SPACE,
				      SuiLookAndFeel.COMPONENT_SPACE,
				      0);
	Magbc.fill= Magbc.NONE;		
	Magbc.anchor = Magbc.EAST;
	Mapanel.add( _bAdd,Magbc);
		
	/* just to make it nicer */
	Magbc.gridy++;

	/* Referrals List */
	_refData = new DefaultListModel();
	_refList = new JList( _refData );
	_refList.setVisibleRowCount(ROWS);
	_refList.setCellRenderer( new ReferralCellRenderer() );
	JScrollPane scrollRef = new JScrollPane();
	scrollRef.getViewport().setView(_refList);
        Magbc.gridwidth  = 1;
	Magbc.gridx = 0;
	Magbc.gridy++;
	Magbc.insets = new Insets(0, 0, 0, 0);
	Magbc.fill= Magbc.BOTH;		
        Magbc.weightx    = 1;
        Magbc.weighty    = 1;
	Mapanel.add( scrollRef, Magbc);	
			
	_bDelete = UIFactory.makeJButton(this, _section, "bdelete", (ResourceSet)null);
	_bDelete.setActionCommand(DELETE_REF);
	_bDelete.setEnabled(false);
	Magbc.gridwidth  = 1;
	Magbc.gridx++;
	Magbc.weightx    = 0;
        Magbc.weighty    = 0;
	Magbc.fill= Magbc.NONE;		
	Magbc.insets = new Insets(0, SuiLookAndFeel.DIFFERENT_COMPONENT_SPACE, 0, 0);
	Magbc.anchor = Magbc.NORTHEAST;
	Mapanel.add( _bDelete,Magbc);
		

    }

    protected void createTypeArea( JPanel grid ) {
        GridBagConstraints gbc = getGBC();
	gbc.insets.bottom = _gbc.insets.top;
        gbc.gridwidth = gbc.REMAINDER;
		
	String[] mapList = MappingUtils.getMappingNode( _model.getServerInfo().getLDAPConnection() );

	JPanel Mapanel = new GroupPanel(
					DSUtil._resource.getString( _section, "new-mapping-title" ));
        gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
        grid.add( Mapanel, gbc );


	_NewNodeLabel = makeJLabel( _section, "new-node-suffix" );
	gbc.gridwidth = 3;
	gbc.fill = gbc.NONE;
	gbc.weightx = 0.0;
        Mapanel.add( _NewNodeLabel, gbc );

        _NewNodeText = makeJTextField( _section, "new-node-suffix" );
	gbc.weightx = 1.0;
	gbc.gridwidth = gbc.REMAINDER;
	gbc.fill = gbc.HORIZONTAL;
        Mapanel.add( _NewNodeText, gbc );		

	_comboNewNodeMapping = new JComboBox();
        _comboNewNodeMapping.addItemListener(this);
        _comboNewNodeMapping.addActionListener(this);

	if ( mapList != null ) {
	    int j =0;
	    _comboNewNodeMapping.addItem( ROOT_MAPPING_NODE );
	    String suffix = null;
	    if (_entry != null) {
		suffix = MappingUtils.getMappingNodeSuffix(_entry);
	    }
	    for( int i = 0; i < mapList.length; i++ ) {
                _comboNewNodeMapping.addItem( mapList[i] );
		if((suffix != null) &&
		   DSUtil.equalDNs(mapList[i], suffix)) {
		    j = i + 1;
		}
				
	    }
	    if ( mapList.length > 0 )
		_comboNewNodeMapping.setSelectedIndex( j );
	}

	_comboNewNodeLabel = makeJLabel( _section, "new-node-subordination" );
	gbc.gridwidth = 3; // 2
	gbc.fill = gbc.NONE;
	gbc.weightx = 0.0;
        Mapanel.add( _comboNewNodeLabel, gbc );
        gbc.gridwidth = gbc.REMAINDER;
	gbc.weightx    = 1.0;
	gbc.fill = gbc.HORIZONTAL;
        Mapanel.add( _comboNewNodeMapping, gbc );


	_comboStatus = new JComboBox();
        _comboStatus.addItemListener(this);
        _comboStatus.addActionListener(this);

	_comboStatus.addItem( BACKEND );
	_comboStatus.addItem( REFERRAL );
	_comboStatus.addItem( REFERRAL_UPDATE );
	_comboStatus.addItem( DISABLE );
		
	_comboStatusLabel =  makeJLabel( _section, "new-node-status" );
	gbc.gridwidth = 3;
	gbc.fill = gbc.NONE;
	gbc.weightx = 0.0;
        Mapanel.add( _comboStatusLabel, gbc );
        gbc.gridwidth = gbc.REMAINDER;
	gbc.weightx    = 1.0;
	gbc.fill = gbc.HORIZONTAL;
        Mapanel.add( _comboStatus, gbc );
    }	
		

    private String[] getBackendList() {
	Vector v = new Vector(1);
  
	try {			
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    _model.setWaitCursor( true );
	    LDAPSearchResults res =
		ldc.search( CONFIG_BASEDN,
			    ldc.SCOPE_SUB,
			    "objectclass=nsBackendInstance",
			    null,
			    false );
	    while( res.hasMoreElements() ) {
		LDAPEntry bentry = (LDAPEntry)res.nextElement();
		String name = bentry.getDN();
		Debug.println( "NewMappingNodePanel.getBackendList() {");
		Debug.println( "*** backend : " + name );
		v.addElement(LDAPDN.explodeDN(bentry.getDN(),true)[0]);
	    }
	} catch ( LDAPException e ) {
	    Debug.println( "NewMappingNodePanel.getBackendList() " + e );
	} finally {
	    _model.setWaitCursor( false );
	}		
	Debug.println( "*** Nb : " + v.size() + " Dir :" + v.toString());
	String[] bckList = new String[v.size()];
	v.toArray(bckList);
	return bckList;
    }

    private boolean isListEmpty(JList list){
	boolean testlist = (list.getModel().getSize() == 0);
	return( testlist );
    }
	
    private String[] getReferralsInList() {

	Vector v = new Vector();
		
	for (int i = 0;
	     i < _refList.getModel().getSize();
	     i++) {
	    v.addElement( _refList.getModel().getElementAt( i ));
	}
	if( v.size()  == 0 ) {
	    return ( null );
	} else {
	    String referralList[] = new String[ v.size() ];
	    v.toArray( referralList );
	    return ( referralList );
	}
    }

    private String[] getBackendsInList() {
	Vector v = new Vector(1);
	Object OVals[] = _bckList.getSelectedValues();

	if (( OVals == null ) || (OVals.length == 0)) {
	    return ( null );
	}

	String[] bckList = new String[OVals.length];
	int i=0;
		

	for(i =0; i <  OVals.length; i++) {			
	    v.addElement( OVals[i].toString() );
	    Debug.println( "*** backends : " + OVals[i].toString() );
	}
	v.toArray(bckList);
	return bckList;
    }

    /**
     * Enable/disable OK button
     *
     * @param ok true to enable the OK button
     */
    private void setOkay( boolean ok ) {
	AbstractDialog dlg = getAbstractDialog();
	if ( dlg != null ) {
	    dlg.setOKButtonEnabled( ok );
	} else {
	}
    }

    private void checkOkay() {	   
	String a = _NewNodeText.getText().trim(); 	    
	boolean ok = ((a != null) && (a.length() > 0)) ;
	ok = ok && _isNewMappingNodeValid &&
	    ( !( _bckList.isSelectionEmpty() ) ||
	      !( isListEmpty( _refList ) )); 
	setOkay( ok );
    }

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
	if(e.getActionCommand().equals(ADD_REF)) {
	    _refData.addElement( _refNewRefText.getText());
	    _refNewRefText.setText("");
	    _bAdd.setEnabled( false );
	} else if (e.getActionCommand().equals(DELETE_REF)) {
	    int[] indices = _refList.getSelectedIndices();
	    for (int i = indices.length - 1; i>= 0;i--) {					
		_refData.removeElementAt(indices[i]);
	    }						
	    _bDelete.setEnabled( false );
	} 
	checkOkay();
    }


    public void resetCallback() {
	/* No state to preserve */
	clearDirtyFlag();
	hideDialog();
    }


    public void okCallback() {
	/* No state to preserve */
	if(( _NewNodeText.getText() != null ) &&
	   ( _NewNodeText.getText().trim().length() > 0 )) {
	    if( MappingUtils.addMappingNode(
					    _model.getServerInfo().getLDAPConnection(),
					    _section,
					    _NewNodeText.getText(),
					    (String) _comboNewNodeMapping.getSelectedItem(),
					    (String) _comboStatus.getSelectedItem(),
					    getBackendsInList(),
					    getReferralsInList())) return;
			
	} else {
	    DSUtil.showErrorDialog( getModel().getFrame(),
				    "new-mapping-node","" );
	    return;
	}
	hideDialog();
    }

    public void changedUpdate(DocumentEvent e) {
	if( e.getDocument() == _NewNodeText.getDocument() ) {
	    if ( DN.isDN ( _NewNodeText.getText() )) {
		setChangeState( _NewNodeLabel, CHANGE_STATE_UNMODIFIED );
		_isNewMappingNodeValid = true;
	    } else {
		setChangeState( _NewNodeLabel, CHANGE_STATE_ERROR );
		_isNewMappingNodeValid = false;
	    }
	}
	super.changedUpdate( e );
	checkOkay();
    }

    public void removeUpdate(DocumentEvent e) {
	if( e.getDocument() == _NewNodeText.getDocument() ) {
	    if ( DN.isDN ( _NewNodeText.getText() )) {
		setChangeState( _NewNodeLabel, CHANGE_STATE_UNMODIFIED );
		_isNewMappingNodeValid = true;
	    } else {
		setChangeState( _NewNodeLabel, CHANGE_STATE_ERROR );
		_isNewMappingNodeValid = false;
	    }
	}
	super.removeUpdate( e );
	checkOkay();
    }

    public void insertUpdate(DocumentEvent e) {
	if( e.getDocument() == _refNewRefText.getDocument() ) {
	    boolean bAdd_status = ( _refNewRefText.getText().trim().length() > 0 );
	    _bAdd.setEnabled( bAdd_status );
	} else if( e.getDocument() == _NewNodeText.getDocument() ) {
	    if ( DN.isDN ( _NewNodeText.getText() )) {
		setChangeState( _NewNodeLabel, CHANGE_STATE_UNMODIFIED );
		_isNewMappingNodeValid = true;
	    } else {
		setChangeState( _NewNodeLabel, CHANGE_STATE_ERROR );
		_isNewMappingNodeValid = false;
	    }
	}
	super.insertUpdate( e );
	checkOkay();
    }


    class ReferralCellRenderer extends DefaultListCellRenderer {  
	// This is the only method defined by ListCellRenderer.  We just
	// reconfigure the Jlabel each time we're called.
		
	public Component getListCellRendererComponent(
						      JList list,
						      Object value,            // value to display
						      int index,               // cell index
						      boolean isSelected,      // is the cell selected
						      boolean cellHasFocus)    // the list and the cell have the focus
	{
	    if ( list == _refList ){
		if (list.isSelectionEmpty()) {
		    _bDelete.setEnabled(false);
		} else {
		    _bDelete.setEnabled(true);
		}
	    }
	    checkOkay();
	    return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
    }

    private JTextField			_mappingNameText;
    private JLabel				_mappingNameLabel;
    private JTextField			_NewNodeText;
    private JLabel				_NewNodeLabel;

    private JTextField			_subordinationText;
    private JLabel				_subordinationLabel;

    private IDSModel			_model = null;
    private JComboBox			_comboNewNodeMapping;
    private JLabel				_comboNewNodeLabel;
    private JComboBox			_comboStatus;
    private JLabel				_comboStatusLabel;
    private JLabel				_bckListLabel;

    private JList				_bckList;
    private JButton				_bAdd;
    private JButton				_bDelete;
    private JLabel				_refListLabel;
    private JTextField			_refNewRefText;

    private JLabel				_refNewRefLabel;
    private JList				_refList;
    private DefaultListModel	_refData;

    private boolean				_isNewMappingNodeValid = false;

    private final static String _section = "mappingtree";
    static final String CONFIG_BASEDN = "cn=plugins, cn=config" ;
    static final String CONFIG_MAPPING = "cn=mapping tree, cn=config" ;
    static final String ROOT_MAPPING_NODE = "is root suffix";
    static final String DISABLE = MappingUtils.DISABLE;
    static final String BACKEND = MappingUtils.BACKEND;
    static final String REFERRAL = MappingUtils.REFERRAL;
    static final String REFERRAL_UPDATE = MappingUtils.REFERRAL_UPDATE;
    static final String ADD_REF = "addReferral";
    private final String DELETE_REF = "deleteReferral";
    private final int ROWS = 4; 
    private LDAPEntry _entry = null;

}
