/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;



/**
  * Key Listener used in combination with tables that have CheckBoxTableCellRenderer.
  * This class just listens to clicks on the key SPACE and udpates the checkboxes of
  * the table properly.
  */
public class CheckboxTableKeyListener implements KeyListener {
	public void keyPressed(KeyEvent e) {	
		if (e.getKeyCode() == KeyEvent.VK_SPACE ) { 			
			if (e.getSource() instanceof JTable) {
				JTable table = (JTable) e.getSource();
				int selectedRow = table.getSelectedRow();
				int selectedColumn = table.getSelectedColumn(); 				
				if ((selectedRow >= 0) && (selectedColumn >= 0)) { 				
					TableCellRenderer renderer = table.getCellRenderer(selectedRow, selectedColumn);
					if (renderer instanceof CheckBoxTableCellRenderer) {						
						Boolean oldValue = (Boolean)table.getValueAt(selectedRow, selectedColumn);
						boolean newState = !oldValue.booleanValue();						
						
						AbstractTableModel model = (AbstractTableModel )table.getModel();
						DefaultCellEditor cbEditor = (DefaultCellEditor) table.getCellEditor(selectedRow, selectedColumn);						
						model.setValueAt(new Boolean(newState), selectedRow, selectedColumn);
						model.fireTableCellUpdated(selectedRow, selectedColumn);																		
						table.prepareEditor(cbEditor,  selectedRow, selectedColumn);											
					}
				}
			}
		}
	}

	public void keyReleased(KeyEvent e) {	
	}

	public void keyTyped(KeyEvent e) {				
	}
}
