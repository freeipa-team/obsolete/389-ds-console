/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import com.netscape.admin.dirserv.GenericProgressDialog;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.GlobalConstants;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import netscape.ldap.*;
import netscape.ldap.util.DN;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class ServerManagerPanel extends BlankPanel implements Runnable {

	public ServerManagerPanel(IDSModel model) {
		super(model, _section);
		_helpToken = "configuration-system-manager-help";
		_refreshWhenSelect = false;
	}

	public void init() {
		if (_isInitialized) {
			return;
		}
		initServerValues();

        _cbStorageScheme = makeJComboBox();
		_cbStorageScheme.removeActionListener(this);
        _lStorageScheme = makeJLabel( _section, "storageScheme" );
		_lStorageScheme.setLabelFor(_cbStorageScheme);

		_tfRootDN = makeJTextField( _section, "rootdn" );
		_tfRootDN.getDocument().addDocumentListener(this);
  		_lRootDN = makeJLabel(_section, "rootdn");
		_lRootDN.setLabelFor(_tfRootDN);
		_tfRootDN.setText((String)_serverValues.get(ROOT_DN_ATTR_NAME));

		_pfNewPassword = makeJPasswordField(20);
		_pfNewPassword.getDocument().addDocumentListener(this);
  		_lNewPassword = makeJLabel(_section, "newPassword");
		_lNewPassword.setLabelFor(_pfNewPassword);
		_pfNewPassword.setText((String)_serverValues.get(PASSWORD_ATTR_NAME));
		
		_pfConfirmPassword = makeJPasswordField(20);
		_pfConfirmPassword.getDocument().addDocumentListener(this);
		_lConfirmPassword = makeJLabel(_section, "confirmPassword");
		_lConfirmPassword.setLabelFor(_pfConfirmPassword);
		_pfConfirmPassword.setText((String)_serverValues.get(PASSWORD_ATTR_NAME));

		findPasswordPlugins();
		
		for (int i=0; i< COMBO_ENTRIES.length; i++) {
			String	val = COMBO_DESCRIPTION[i];
			if (val.equals("")) {
				val = COMBO_ENTRIES[i];
			}
			_cbStorageScheme.addItem(val);
		}		

        JPanel panel = _myPanel;
        panel.setLayout( new GridBagLayout() );

		addEntryField( panel, _lRootDN, _tfRootDN );
		addEntryField( panel, _lStorageScheme, _cbStorageScheme );

		/* GridBagConstraints gbc = getGBC();
  		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;
        panel.add( new JLabel(""), gbc); */

		addEntryField( panel, _lNewPassword, _pfNewPassword );
		addEntryField( panel, _lConfirmPassword, _pfConfirmPassword );

        addBottomGlue();

		_isInitialized = true;
	}

	
	private void findPasswordPlugins() {
		try {											
			String filter = PLUGIN_TYPE_ATTR_NAME+"="+PWDSTORAGE;
			String[] attrs = { CN_ATTR_NAME, DESCRIPTION_ATTR_NAME };

			LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();

			if (ldc == null) {
				Debug.println("ServerManagerPanel.findPasswordPlugins(): we are not creating the right thing (ldc is null)");
				COMBO_ENTRIES = COMBO_ENTRIES_HARDCOPY;
				COMBO_DESCRIPTION = COMBO_DESCRIPTION_HARDCOPY;
				return;
			}

			LDAPSearchConstraints cons =
				(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
			cons.setMaxResults( 0 );
			
			LDAPSearchResults entries = ldc.search(DSUtil.PLUGIN_CONFIG_BASE_DN,
												 ldc.SCOPE_SUB,
												 filter, attrs, false, cons);
			Vector vector = new Vector();
			Vector vectorDesc = new Vector();
			LDAPEntry findEntry;
			LDAPAttribute name = null;			
			while((entries != null) && entries.hasMoreElements()) {
				findEntry = (LDAPEntry)entries.nextElement();
				if (findEntry != null) {
					name = findEntry.getAttribute(CN_ATTR_NAME);
				}
				if ( name != null ) {					
					Enumeration e = name.getStringValues();
					String  nameOfPlugin= (String)e.nextElement();
					
					/* The NS-MTA-MD5 password scheme is for comparision only and for backward
					   compatibility with an Old Messaging Server that was setting passwords in the
					   directory already encrypted. The scheme cannot and don't encrypt password if
					   they are in clear.
					   We don't take it */
					if (nameOfPlugin.equalsIgnoreCase("ns-mta-md5")) {
						continue;
					}
					vector.addElement(nameOfPlugin.toLowerCase());

					String descriptionOfPlugin = "";
					name = findEntry.getAttribute(DESCRIPTION_ATTR_NAME);
					if (name != null) {
					    e = name.getStringValues();
						descriptionOfPlugin= (String)e.nextElement();
					}
					vectorDesc.addElement(descriptionOfPlugin);					
				}
				
			}

			/* Searching for the current used password policy */			
			String[] attr = { STORAGE_SCHEME_ATTR_NAME };
		
			findEntry = DSUtil.readEntry(ldc, MANAGER_DN, attr, cons);
			if (findEntry != null) {
				name = findEntry.getAttribute(STORAGE_SCHEME_ATTR_NAME);				
				if ( name != null ) {					
				Enumeration e = name.getStringValues();
				String  nameOfPlugin= (String)e.nextElement();					
				
				nameOfPlugin = nameOfPlugin.toLowerCase();
				
				int index = vector.indexOf(nameOfPlugin);
				/* This is done because the first element in the combo is the one that is selected (we have to select the
				   password policy for the manager that we have in cn = config) */
				if (index >= 0) {
					Object objectAtPos0 = vector.elementAt(0);					
					vector.set(index, objectAtPos0);
					vector.set(0, nameOfPlugin);
					
					Object descAtPos0 = vectorDesc.elementAt(0);
					Object descAtIndex = vectorDesc.elementAt(index);
					vectorDesc.set(index, descAtPos0);
					vectorDesc.set(0, descAtIndex);
				}
				}
			}
			COMBO_ENTRIES = new String[vector.size()];
			vector.toArray(COMBO_ENTRIES);
			COMBO_DESCRIPTION = new String[vectorDesc.size()];
			vectorDesc.toArray(COMBO_DESCRIPTION);
			} catch (LDAPException ex) {
				Debug.println("ServerManagerPanel.findPasswordPlugins: "+ex);
			}
		if ((COMBO_ENTRIES == null) || (COMBO_ENTRIES.length == 0)) {
			Debug.println("ServerManagerPanel.findPasswordPlugins(): we are not creating the right thing");
			COMBO_ENTRIES = COMBO_ENTRIES_HARDCOPY;
			COMBO_DESCRIPTION = COMBO_DESCRIPTION_HARDCOPY;
		}	
	}

	private void initServerValues() {
		_serverValues = new Hashtable();
		try {
			LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();

			if (ldc == null) {
				return;
			}
			
			LDAPSearchConstraints cons =
				(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
			cons.setMaxResults( 0 );
			String[] attrs = {
				STORAGE_SCHEME_ATTR_NAME,
				PASSWORD_ATTR_NAME,
				ROOT_DN_ATTR_NAME};
				
			LDAPEntry entry = DSUtil.readEntry(ldc, MANAGER_DN, attrs, cons);
						
			for (int i=0; i<attrs.length; i++) {
				String value = DSUtil.getAttrValue(entry, attrs[i]);
				_serverValues.put(attrs[i], value);									
			}
		} catch (LDAPException e) {
			Debug.println("ServerManagerPanel.initServerValues(): "+e);
		}
	}

    /**
	 * Called on hitting the Apply or Okay button
	 */
    public void okCallback() {
		_dlg = new GenericProgressDialog(getModel().getFrame(), 
										 true, 
										 GenericProgressDialog.NO_BUTTON_OPTION, 
										 _resource.getString(_section,"updating-title"),
										 null,
										 null);
		try { 		
			Thread th = new Thread(this);			
			th.start();
			_dlg.packAndShow();			
		} catch ( Exception e ) {
			Debug.println("ServerManagerPanel.okCallback(): " +
						  e );
			e.printStackTrace();	
		
		}		
		resetCallback();
	}


	public void run() {
		checkOkay();
		
		if (!_isNameValid || !_isPasswordValid) {
			_dlg.closeCallBack();
			return;
		}

		String dn = getModel().getConsoleInfo().getCurrentDN();		
		LDAPConnection ldc =
			getModel().getConsoleInfo().getLDAPConnection();
		LDAPModificationSet mods = new LDAPModificationSet();

		/* CHECK the NAME */
		if (_isNameModified) {			
			String oldDN = (String)_serverValues.get(ROOT_DN_ATTR_NAME);		
			String name = _tfRootDN.getText();
			if (name.trim().equals("")) {	
				mods.add(LDAPModification.REPLACE, 
						 new LDAPAttribute(ROOT_DN_ATTR_NAME, ""));
			} else {	
				mods.add(LDAPModification.REPLACE,
						 new LDAPAttribute(ROOT_DN_ATTR_NAME, name));
			}
			/* Besides saving the change in the server itself,
			   update the change in the topology server */
			LDAPModification mod = new LDAPModification( 
														LDAPModification.REPLACE,
														new LDAPAttribute("nsbinddn", name) );
			try {
				if (_dlg != null) {
					_dlg.setTextInLabel( _resource.getString(_section,
															 "updating-entry",
															 DSUtil.abreviateString(dn, 40)));
				}				
				ldc.modify(dn, mod);
				Debug.println( "ServerManagerPanel.okCallback(): modified " +
							   mod + " for " + dn );
			} catch (LDAPException lde) {
				Debug.println( "ServerManagerPanel.okCallback(): modified " +
							   mod  +" to " + dn + " " + lde );
				try {				   
					SwingUtilities.invokeAndWait(new ExceptionDisplayer(lde, _dlg));
				} catch (Exception exc) {
				}
			}
		}
			


		/* CHECK the PASSWORD */
		String password = null;
		if (_isPasswordModified) {
			char [] pwd = _pfNewPassword.getPassword();			
			if (pwd != null) {
				password = new String(pwd);
			}			 
			if (password == null) {
				password = "";
			}
			if (password.equals("")) {
				mods.add(LDAPModification.REPLACE,
						 new LDAPAttribute(PASSWORD_ATTR_NAME,""));
			} else {
				mods.add(LDAPModification.REPLACE,
						 new LDAPAttribute(PASSWORD_ATTR_NAME, password));
			}
			/* Besides saving the change in the server itself,
			   update the change in the topology server */
			// replace the password in the SIE
			LDAPModification mod = new LDAPModification( 
														LDAPModification.REPLACE,
														new LDAPAttribute("userPassword", password) );
			try {
				if (_dlg != null) {
					_dlg.setTextInLabel( _resource.getString(_section,
															 "updating-entry",
														 DSUtil.abreviateString(dn, 40)));
				}				
				ldc.modify(dn, mod);
				Debug.println( "ServerManagerPanel.okCallback(): modified " +
							   mod + " for " + dn );
				
				// change the password expiration to never expire
				// the magic string below equals MAX_INTEGER seconds
				// after the epoch (1970/01/01)
				password = "20380119031407Z";
				mod = new LDAPModification( 
										   LDAPModification.REPLACE,
										   new LDAPAttribute("passwordexpirationtime",
															 password));
				try {
					ldc.modify(dn, mod);
					Debug.println( "ServerManagerPanel.okCallback(): modified " +
							   mod + " for " + dn );
				}
				catch(LDAPException x) {
					Debug.println( 0, "ServerManagerPanel.okCallback(): failed to " +
							   mod + " for " + dn );
				}

				// Update task authentication password
				updateTaskAuthPWD(password);
				
			} catch (LDAPException lde) {
				Debug.println( "ServerManagerPanel.okCallback(): modified " +
						   mod  +" to " + dn + " " + lde );
				try {
					SwingUtilities.invokeAndWait(new ExceptionDisplayer(lde, _dlg));
				} catch (Exception exc) {
				}
			}		
		}

		/* Check the crypt method */	
		if (_isEncryptionTypeModified) {
			int index = _cbStorageScheme.getSelectedIndex();
			if ((index < COMBO_ENTRIES.length) && (index >= 0)) {
				String encryptionType = COMBO_ENTRIES[index];
				if (encryptionType != null) {
					mods.add(LDAPModification.REPLACE,
							 new LDAPAttribute(STORAGE_SCHEME_ATTR_NAME, encryptionType));
				}
			}
		}

		if (mods.size() > 0) {
			try {
			    LDAPConnection serverLdc = getModel().getServerInfo().getLDAPConnection();
				serverLdc.modify(MANAGER_DN, mods);
				Debug.println("ServerManagerPanel.okCallback(): " + mods +
						   " to " + MANAGER_DN );
			} catch (LDAPException lde) {
				_lde = lde;
				Debug.println("ServerManagerPanel.okCallback(): modified " +
							  mods + " to " + MANAGER_DN +" Exception "+lde);
				try {
					SwingUtilities.invokeAndWait(new ExceptionDisplayer(lde, _dlg));
				} catch (Exception exc) {
				}
			}
		}

		/* Save the new root DN, if any */
		if ( !_rootDN.equalsIgnoreCase( _tfRootDN.getText().trim() ) ) {
			_rootDN = _tfRootDN.getText().trim();
			getModel().rootDNChanged( _rootDN );
		}
		_dlg.closeCallBack();	
	}	


	/**
	 * Update the password for task authentication.
	 * Here we check if the bind DN is also the task DN.
	 * In that case, we keep the task password aligned
	 * on the bind password.
	 */
	void updateTaskAuthPWD(String pwd) {
		ConsoleInfo serverInfo = getModel().getServerInfo();
		DN sieDN = new DN(serverInfo.getCurrentDN());
		DN taskAuthDN = new DN((String)serverInfo.get(GlobalConstants.TASKS_AUTH_DN));
		Debug.println("ServerManagerPanel.updateTaskAuthPWD: bind DN is " + sieDN);
		Debug.println("ServerManagerPanel.updateTaskAuthPWD: task DN is " + taskAuthDN);
		if (sieDN.equals(taskAuthDN)) {
			serverInfo.put(GlobalConstants.TASKS_AUTH_PWD, pwd);
			Debug.println("ServerManagerPanel.updateTaskAuthPWD: password has been updated");
		}
	}
	
    
    /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
    public void refreshFromServer () {
		resetCallback();
	}

    private void checkOkay() {
		if (!_isInitialized) {
			return;
		}
		/* CHECK THE NAME */
		_isNameModified = false;
		_isNameValid = true;
		String name = _tfRootDN.getText();
		if (name == null) {
			name = "";
		}
		/* Is a DN ?? */
		_isNameValid = DSUtil.isValidDN(name);
		DN nameDN = new DN(name);
		DN serverValueDN = new DN((String)_serverValues.get(ROOT_DN_ATTR_NAME));
		if (!nameDN.equals(serverValueDN)) {
			_isNameModified = true;
		}
		
		/* CHECK THE PASSWORD */
		_isPasswordModified = false;
		_isPasswordValid = true;
		char [] pwd = _pfNewPassword.getPassword();
		String password = null;
		if (pwd != null) {
			 password = new String(pwd);
		}			 
		if (password == null) {
			password = "";
		}

		String confirmPassword = null;
		pwd = _pfConfirmPassword.getPassword();
		if (pwd != null) {
			confirmPassword = new String(pwd);
		}
		if (confirmPassword == null) {
			confirmPassword = "";
		}
		
		if (!password.equals((String)_serverValues.get(PASSWORD_ATTR_NAME)) ||
			!confirmPassword.equals((String)_serverValues.get(PASSWORD_ATTR_NAME))) {
			_isPasswordModified = true;
		}

		if (!confirmPassword.equals(password)) {
			_isPasswordValid = false;
		} else if ((password.length() < _pwdMinLength) && (password.length() != 0)) {
			_isPasswordValid = false;		
		} 

		_isEncryptionTypeModified = false;
		int index = _cbStorageScheme.getSelectedIndex();
		if ((index < COMBO_ENTRIES.length) && (index >= 0)){
			String encryptionType = COMBO_ENTRIES[index];
			if (encryptionType == null) {
				encryptionType = "";
			}
			if (!encryptionType.trim().equalsIgnoreCase((String)_serverValues.get(STORAGE_SCHEME_ATTR_NAME))) {
				_isEncryptionTypeModified = true;
			}
		}
		
		
		if (!_isNameValid) {
			setChangeState (_lRootDN, CHANGE_STATE_ERROR);
		} else if (_isNameModified) {
			setChangeState (_lRootDN, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState (_lRootDN, CHANGE_STATE_UNMODIFIED);
		}
		
		if (!_isPasswordValid) {
			setChangeState (_lNewPassword, CHANGE_STATE_ERROR);
			setChangeState (_lConfirmPassword, CHANGE_STATE_ERROR);
		} else if (_isPasswordModified) {
			setChangeState (_lNewPassword, CHANGE_STATE_MODIFIED);
			setChangeState (_lConfirmPassword, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState (_lNewPassword, CHANGE_STATE_UNMODIFIED);
			setChangeState (_lConfirmPassword, CHANGE_STATE_UNMODIFIED);
		}

		if (_isEncryptionTypeModified) {
			setChangeState (_lStorageScheme, CHANGE_STATE_MODIFIED);
		} else {
			setChangeState (_lStorageScheme, CHANGE_STATE_UNMODIFIED);
		}
		

		boolean isValid = _isNameValid &&
			_isPasswordValid;
		boolean isModified = _isNameModified ||
			_isPasswordModified ||
			_isEncryptionTypeModified;
		
		if (!isValid) {
			clearValidFlag();
		} else {
			setValidFlag();
		}
		if (!isModified) {
			clearDirtyFlag();
		} else {
			setDirtyFlag();
		}
	}
			

    /**
	 * Called on hitting the Reset button
	 */
    public void resetCallback() {
        initServerValues();
		/* Re create the combo box with the storage scheme */
		findPasswordPlugins();
		_cbStorageScheme.removeAllItems();
		for (int i=0; i< COMBO_ENTRIES.length; i++) {
			String	val = COMBO_DESCRIPTION[i];
			if (val.equals("")) {
				val = COMBO_ENTRIES[i];
			}
			_cbStorageScheme.addItem(val);
		}
		_tfRootDN.setText((String)_serverValues.get(ROOT_DN_ATTR_NAME));

		_pfNewPassword.setText((String)_serverValues.get(PASSWORD_ATTR_NAME));

		_pfConfirmPassword.setText((String)_serverValues.get(PASSWORD_ATTR_NAME));

		String encryptionType = (String)_serverValues.get(STORAGE_SCHEME_ATTR_NAME);
		for (int i=0; i< COMBO_ENTRIES.length; i++) {
			if (encryptionType.equalsIgnoreCase(COMBO_ENTRIES[i])) {
				_cbStorageScheme.setSelectedIndex(i);
				break;			
			}
		}

		checkOkay();
    }

	/**
	  * Overwrite the methods from Document Listener interface in BlankPanel
	  *
	  **/
	public void changedUpdate(DocumentEvent e) {		
		insertUpdate(e);
    }
    public void removeUpdate(DocumentEvent e) {		
		insertUpdate(e);
    }
    public void insertUpdate(DocumentEvent e) {		
		checkOkay();
    }

	/**
	 * Some list component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void itemStateChanged(ItemEvent e) {
		if (e.getSource().equals(_cbStorageScheme)) {
			checkOkay();
		}
    }
	
    private JLabel _lStorageScheme;
    private JComboBox _cbStorageScheme;
	private JLabel _lRootDN;
    private JTextField _tfRootDN;
	private JLabel _lNewPassword;
    private JPasswordField _pfNewPassword;
	private JLabel _lConfirmPassword;
    private JPasswordField _pfConfirmPassword;
	private String _rootDN = "";
	private Hashtable _serverValues = null;

	private boolean _isNameValid;
	private boolean _isPasswordValid;
	private boolean _isNameModified;
	private boolean _isPasswordModified;
	private boolean _isEncryptionTypeModified;

	private LDAPException _lde;

    private static final int _pwdMinLength = 8;
	private static final String MANAGER_DN = "cn=config";
	private static final String STORAGE_SCHEME_ATTR_NAME =
	                                         "nsslapd-rootpwstoragescheme";
	private static final String ROOT_DN_ATTR_NAME = "nsslapd-rootdn";
	private static final String PASSWORD_ATTR_NAME = "nsslapd-rootpw";
	private static final String PLUGIN_TYPE_ATTR_NAME = "nsslapd-plugintype";
	private static final String DESCRIPTION_ATTR_NAME = "nsslapd-plugindescription";
	private static final String PWDSTORAGE = "pwdstoragescheme";
	private static final String CN_ATTR_NAME = "cn";
    /* NOTE: The following are the attribute values that get sent to
       the Directory, not what the user sees. So they should NOT be
       localized or changed, unless the Directory Server is changed.
       To change what the user sees, edit the properties file. */
	private String[] COMBO_ENTRIES;
	private String[] COMBO_DESCRIPTION;
	private final String[] COMBO_ENTRIES_HARDCOPY = { "sha",
													"clear",
													"crypt"
													};

	private ResourceSet _resource = DSUtil._resource;
	private final String[] COMBO_DESCRIPTION_HARDCOPY = { _resource.getString(_section+"-storageScheme","1"),
													_resource.getString(_section+"-storageScheme","2"),
													_resource.getString(_section+"-storageScheme","3")
													};

	private GenericProgressDialog _dlg;
    private static final String _section = "manager";

}

class ExceptionDisplayer implements Runnable {
	public ExceptionDisplayer(LDAPException lde, Component comp) {
		_lde = lde;
		_comp = comp;
	}
	public void run () {
		DSUtil.showLDAPErrorDialog(_comp,
								   _lde,									   
								   "updating-directory-title");
	}
	LDAPException _lde;
	Component _comp;
}
