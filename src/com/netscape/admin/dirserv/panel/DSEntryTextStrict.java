/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/* this class behaves like DSEntryText, except it does */
/* not allow for blank text fields                     */

package com.netscape.admin.dirserv.panel;

import javax.swing.JComponent;
import javax.swing.JTextField;
import com.netscape.admin.dirserv.panel.*;

public class DSEntryTextStrict extends DSEntryText
{
	public DSEntryTextStrict(String model, JTextField field) {
		super(model, field);
	}
	public DSEntryTextStrict(String model, JComponent field1, JComponent field2) {
		super(model, field1, field2);
	}

	public int validate ()
	{
		JTextField tf = (JTextField)getView(0);
		String value = tf.getText().trim();

        if (!tf.isEnabled ())
            return 0;

		if (value.equals (""))
		{
			return ERROR_EMPTY_FIELD;
		}
		else
		{
			return 0;
		}
	}

	public static final int ERROR_EMPTY_FIELD=1;
}
