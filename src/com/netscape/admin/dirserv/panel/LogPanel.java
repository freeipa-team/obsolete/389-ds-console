/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.DocumentEvent;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import netscape.ldap.LDAPException;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
abstract public class LogPanel extends BlankPanel {

	public LogPanel(IDSModel model) {
		this(model, null, true);
	}
	public LogPanel(IDSModel model, String panelName) {
		this(model, panelName, true);
	}
	public LogPanel(IDSModel model, String panelName,
						 boolean scroll) {
		super(model, panelName, scroll);
	}

    public void init() {
        _myPanel.setLayout(new GridBagLayout());
	}

    protected void postInit() {
		Rectangle rect = _myPanel.getVisibleRect();
		Debug.println( "LogPanel.postInit: visible = " + rect );
	}

	protected void createEnableArea() {

	    _cbEnabled = makeJCheckBox( _section, "loggingEnabled");
		_bView = makeJButton( _section, "viewLog" );
    
		DSEntryBoolean enabledDSEntry = new DSEntryBoolean("off", _cbEnabled) {
			    public void show() {
					super.show();
					enableFields( _cbEnabled.isSelected() );
				}
			};

		DSEntrySet entries = getDSEntrySet();

		entries.add(LOG_DN, ENABLED_ATTR_NAME, enabledDSEntry);
		setComponentTable(_cbEnabled, enabledDSEntry);

        GridBagConstraints gbc = getGBC();
        gbc.gridwidth = 3;
  		gbc.weightx    = 0;
		gbc.fill = gbc.NONE;
		int different = UIFactory.getDifferentSpace();
		int separate = UIFactory.getSeparatedSpace();
		gbc.insets = new Insets(0,separate,0,3);
        _myPanel.add(_cbEnabled,gbc);
  		gbc.weightx    = 1.0;
		gbc.fill = gbc.HORIZONTAL;
        _myPanel.add(Box.createGlue(),gbc);
  		gbc.weightx    = 0;
		gbc.fill = gbc.NONE;
        gbc.gridwidth = gbc.REMAINDER;
        _myPanel.add(_bView,gbc);
	}

	protected void createConfigArea() {
		_tfLog = makeJTextField(_section, "logDirectory");
		_tfLog.getAccessibleContext().setAccessibleDescription(_resource.getString( _section,
																					"log-logDirectory-title" ));
		_bLog = makeJButton(_section, "browse");
        JPanel panel =
			new GroupPanel(_resource.getString( _section,
												"log-logDirectory-title" ));
    		
		_tfLogMode = makeJTextField(_section, "LogMode");
    	JLabel lLogMode = makeJLabel(_section, "LogMode");
		lLogMode.setLabelFor(_tfLogMode);

    	_tfLogsPerDir = makeNumericalJTextField(_section, "LogsPerDir");
    	JLabel lLogsPerDir = makeJLabel(_section, "LogsPerDir");
		lLogsPerDir.setLabelFor(_tfLogsPerDir);
    	_tfLogSize = makeNumericalJTextField(_section, "LogSize");
		_lLogSize = makeJLabel(_section, "LogSize");
		_lLogSize.setLabelFor(_tfLogSize);
    	_tfRotationTime = makeNumericalJTextField(_section, "RotationTime");
    	JLabel lRotationTime = makeJLabel(_section, "RotationTime");
		lRotationTime.setLabelFor(_tfRotationTime);
    	_cbRotationUnits = makeJComboBox(_section, "RotationUnits", null);
    	lRotationTime.setLabelFor(_cbRotationUnits);

        _cbRotationSyncEnabled = makeJCheckBox( _section, "RotationSyncEnabled");

    	_tfRotationSyncHour = makeJTextField(_section, "RotationSyncHour");
    	JLabel lRotationSyncHour = makeJLabel(_section, "RotationSyncHour");
		lRotationSyncHour.setLabelFor(_tfRotationSyncHour);
    	_tfRotationSyncMin = makeJTextField(_section, "RotationSyncMin");
    	JLabel lRotationSyncMin = makeJLabel(_section, "RotationSyncMin");
		lRotationSyncMin.setLabelFor(_tfRotationSyncMin);

    	// Expiration fields
    	_tfMaxDiskSpace = makeNumericalJTextField(_section, "MaxDiskSpace");
    	_lMaxDiskSpace = makeJLabel(_section, "MaxDiskSpace");
		_lMaxDiskSpace.setLabelFor(_tfMaxDiskSpace);
    	_tfMinFreeSpace = makeNumericalJTextField(_section, "MinFreeSpace");
    	JLabel lMinFreeSpace = makeJLabel(_section, "MinFreeSpace");
		lMinFreeSpace.setLabelFor(_tfMinFreeSpace);
    	_tfMaxDaysOld = makeNumericalJTextField(_section, "MaxDaysOld");
		JLabel lMaxDaysOld = makeJLabel(_section, "MaxDaysOld");
		lMaxDaysOld.setLabelFor(_tfMaxDaysOld);
    	_cbExpirationUnits = makeJComboBox(_section, "ExpirationUnits", null);
		lMaxDaysOld.setLabelFor(_cbExpirationUnits);
		// Do not enable if remote
		if (!isLocal())
		    _bLog.setEnabled(false);

		DSEntrySet entries = getDSEntrySet();

		DSEntryFile logDSEntry = 
              new DSEntryFile(null, _tfLog, panel, true, isLocal());
		entries.add(LOG_DN, LOG_ATTR_NAME, logDSEntry);
		setComponentTable(_tfLog, logDSEntry);

		DSEntryFileMode logMode = new DSEntryFileMode( "",
													   _tfLogMode,
													   lLogMode);

		entries.add(LOG_DN, LOG_MODE_ATTR_NAME, logMode);
        setComponentTable(_tfLogMode, logMode);

        DSEntryInteger logsPerDirEntry = new DSEntryInteger(null,
															_tfLogsPerDir,
															lLogsPerDir,
                                                            -1,
															1000,
															1);
        entries.add(LOG_DN, LOG_PER_DIR_ATTR_NAME, logsPerDirEntry);
        setComponentTable(_tfLogsPerDir, logsPerDirEntry);


        DSEntryInteger logsSizeEntry = new DSEntryInteger(null,
														  _tfLogSize,
														  _lLogSize,
                                                          -1,
														  100000,
														  1);
        entries.add(LOG_DN, LOG_SIZE_ATTR_NAME, logsSizeEntry);
        setComponentTable(_tfLogSize, logsSizeEntry);

        DSEntryInteger timeEntry = new DSEntryInteger(null, 
														_tfRotationTime,
														lRotationTime,
														-1,
														1000,
														1);
        entries.add(LOG_DN, ROTATION_TIME_ATTR_NAME, timeEntry);
        setComponentTable(_tfRotationTime, timeEntry);
    
        DSEntryCombo unitEntry = new DSEntryCombo(RCOMBO_ENTRIES,
						  _cbRotationUnits,
						  lRotationTime,
                                                  true);
        entries.add(LOG_DN, ROTATION_UNITS_ATTR_NAME, unitEntry);
        setComponentTable(_cbRotationUnits, unitEntry);

        DSEntryBoolean bRotationSyncEnabled = new DSEntryBoolean("off", _cbRotationSyncEnabled) {
			    public void show() {
					super.show();
					enableRotationSyncFields( _cbRotationSyncEnabled.isSelected() );
				}
			};
        entries.add(LOG_DN, ROTATION_SYNC_ENABLED_ATTR_NAME, bRotationSyncEnabled);
        setComponentTable(_cbRotationSyncEnabled, bRotationSyncEnabled);

        DSEntryInteger hourEntry = new DSEntryInteger(null,
													_tfRotationSyncHour,
													lRotationSyncHour,
													0,
													23,
													1);
        entries.add(LOG_DN, ROTATION_SYNCHOUR_ATTR_NAME, hourEntry);
        setComponentTable(_tfRotationSyncHour, hourEntry);

        DSEntryInteger minEntry = new DSEntryInteger(null,
													_tfRotationSyncMin,
													lRotationSyncMin,
													0,
													59,
													1);
        entries.add(LOG_DN, ROTATION_SYNCMIN_ATTR_NAME, minEntry);
        setComponentTable(_tfRotationSyncMin, minEntry);

        DSEntryInteger maxDiskEntry = new DSEntryInteger(null,
														 _tfMaxDiskSpace,
														 _lMaxDiskSpace,
                                                         -1,
														 100000,
														 1);
        entries.add(LOG_DN, MAX_DISK_SPACE_ATTR_NAME, maxDiskEntry);
        setComponentTable(_tfMaxDiskSpace, maxDiskEntry);

        DSEntryInteger minDiskEntry = new DSEntryInteger(null,
														 _tfMinFreeSpace,
														 lMinFreeSpace,
                                                         -1,
														 100000,
														 1);
        entries.add(LOG_DN, MIN_FREE_SPACE_ATTR_NAME, minDiskEntry);
        setComponentTable(_tfMinFreeSpace, minDiskEntry);

        DSEntryInteger maxDaysEntry = new DSEntryInteger(null,
														 _tfMaxDaysOld, 
														 lMaxDaysOld,
                                                         -1,
														 1000,
														 1);
        entries.add(LOG_DN, MAX_DAYS_OLD_ATTR_NAME, maxDaysEntry);
        setComponentTable(_tfMaxDaysOld, maxDaysEntry);

        DSEntryCombo unitsEntry = new DSEntryCombo(ECOMBO_ENTRIES, 
            _cbExpirationUnits, lMaxDaysOld, true);
        entries.add(LOG_DN, EXPIRATION_UNITS_ATTR_NAME, unitsEntry);
        setComponentTable(_cbExpirationUnits, unitsEntry);

        GridBagConstraints gbc = getGBC();
        JPanel grid = _myPanel;

        gbc = getGBC();
		gbc.gridwidth = 3;
  		gbc.weightx = 0.5;
		gbc.insets.bottom = UIFactory.getDifferentSpace();
		gbc.fill = gbc.HORIZONTAL;
        panel.add(_tfLog, gbc);
        
		gbc.anchor = gbc.WEST;
  		gbc.weightx = 0;
		gbc.fill = gbc.NONE;
        panel.add(_bLog, gbc);

        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 0.5;
		gbc.fill = gbc.HORIZONTAL;
	    panel.add( Box.createGlue(), gbc);

        gbc = getGBC();
        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
        grid.add(panel, gbc);        
	
		/* Rotation group */
        panel =
			new GroupPanel(_resource.getString( _section,
												"rotation-group-title" ));
		gbc.anchor = gbc.WEST;        
  		gbc.weightx = 1.0;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
        grid.add(panel, gbc);        

  		gbc.gridwidth  = 3;
  		gbc.weightx = 0;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
	    panel.add(lLogMode, gbc);

		gbc.anchor = gbc.WEST;
	    panel.add(_tfLogMode, gbc);

        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
	    panel.add( Box.createGlue(), gbc);

  		gbc.gridwidth  = 3;
  		gbc.weightx = 0;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
	    panel.add(lLogsPerDir, gbc);

		gbc.anchor = gbc.WEST;
	    panel.add(_tfLogsPerDir, gbc);

        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
	    panel.add( Box.createGlue(), gbc);

 		gbc.gridwidth = 4;
  		gbc.weightx = 0;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
	    panel.add(_lLogSize,gbc);

		gbc.anchor = gbc.WEST;
	    panel.add(_tfLogSize,gbc);

		JLabel lMb = makeJLabel(_section, "mb");
		lMb.setLabelFor(_tfLogSize);
	    panel.add(lMb, gbc);		

        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
	    panel.add( Box.createGlue(), gbc);

  		gbc.gridwidth  = 4;
  		gbc.weightx = 0;
  		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
		gbc.insets.bottom = UIFactory.getDifferentSpace();
	    panel.add(lRotationTime, gbc);

		gbc.anchor = gbc.WEST;
	    panel.add(_tfRotationTime, gbc);
	    panel.add(_cbRotationUnits, gbc);

	    panel.add(lRotationSyncHour, gbc);
	    panel.add(_cbRotationSyncEnabled, gbc);
	    panel.add(_tfRotationSyncHour, gbc);
	    panel.add(lRotationSyncMin, gbc);
	    panel.add(_tfRotationSyncMin, gbc);

        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
	    panel.add( Box.createGlue(), gbc);
	
		/* Expiration group */
        panel =
			new GroupPanel(_resource.getString( _section,
												"expiration-group-title" ));
        gbc = getGBC();
		gbc.anchor = gbc.WEST;
   		gbc.gridx = gbc.RELATIVE;
      	gbc.gridy = gbc.RELATIVE;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
        grid.add(panel, gbc);        
	
  		gbc.gridwidth = 4;
  		gbc.weightx = 0;
  		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
	    panel.add(_lMaxDiskSpace, gbc);
		gbc.anchor = gbc.WEST;
	    panel.add(_tfMaxDiskSpace, gbc);
		lMb = makeJLabel(_section, "mb");
		lMb.setLabelFor(_tfMaxDiskSpace);
	    panel.add(lMb, gbc);			    
        gbc.gridwidth = gbc.REMAINDER;
	    panel.add( Box.createGlue(), gbc);

		int topOldValue = gbc.insets.top;
		gbc.insets.top = 0;

		gbc.gridwidth = 4;
		gbc.weightx = 0;
		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
		JLabel lLabel = makeJLabel(_section, "limit");
		lLabel.setLabelFor(_tfMaxDiskSpace);
		panel.add(lLabel, gbc);
		gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
	    panel.add( Box.createGlue(), gbc);
		
		gbc.insets.top = 2 * topOldValue;
  		gbc.gridwidth  = 4;
  		gbc.weightx = 0;
  		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
	    panel.add(lMinFreeSpace, gbc);
		gbc.anchor = gbc.WEST;
	    panel.add(_tfMinFreeSpace, gbc);
	    lMb = makeJLabel(_section, "mb");
		lMb.setLabelFor(_tfMinFreeSpace);
	    panel.add(lMb, gbc);
        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
	    panel.add( Box.createGlue(), gbc);

		gbc.insets.top = topOldValue;		
  		gbc.gridwidth  = 4;
  		gbc.weightx = 0;
  		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.EAST;
		gbc.insets.bottom = UIFactory.getDifferentSpace();
	    panel.add(lMaxDaysOld, gbc);
		gbc.anchor = gbc.WEST;
	    panel.add(_tfMaxDaysOld, gbc);
	    panel.add(_cbExpirationUnits, gbc);
        gbc.gridwidth = gbc.REMAINDER;
  		gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
	    panel.add( Box.createGlue(), gbc);
	}
	
	/**
	 * Extend BlankPanel.refresh().
	 */
	public boolean refresh() {
		boolean result = super.refresh();
		
		// If _tfLog has no value, _cbSelected is forced to off.
		// This is to accomodate with the server behavior: for the
		// server, logging is implicitly disabled if there is no
		// log file specified.
		if (_tfLog.getText().trim().length() == 0) {
                        String path = DSUtil.getDefaultLogPath(getModel().getServerInfo()) + File.separator + LOG_BASE_NAME;
			_cbEnabled.setSelected(false);
			_cbRotationSyncEnabled.setSelected(false);
			_tfLog.setText(path);
			enableFields(false);
		}
		
		return result;
	}
	

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(_cbEnabled)) {
            boolean enable = _cbEnabled.isSelected();
			enableFields( enable );
			super.actionPerformed(e);
        } else if (e.getSource().equals(_bLog)) {
    	    String file = _tfLog.getText();
			if ( java.io.File.separator.equals("\\") ) {
				file = file.replace( '/', '\\' );
			}
    	    file = DSFileDialog.getFileName( file, false);
    	    if (file != null)
    	        _tfLog.setText(file);
        } else if (e.getSource().equals(_bView)) {
			LogContentPanel child = getViewerPanel();
			SimpleDialog dlg =
				new SimpleDialog( getModel().getFrame(), SimpleDialog.CLOSE|SimpleDialog.HELP, child );
			dlg.setLocationRelativeTo( this );
			dlg.setDefaultCloseOperation( dlg.DISPOSE_ON_CLOSE );
			// We need to call refresh() between init() and show()
			// So we disable the auto init of SimpleDialog.
			dlg.setAutoInit(false);
			dlg.getAccessibleContext().setAccessibleDescription(_resource.getString(_section,
																					"view-dialog-description"));
			child.init();
			child.refresh();
			dlg.packAndShow();
        } else if (e.getSource().equals(_cbRotationUnits)) {
			boolean enable = true;
			String selectedUnit = (String)_cbRotationUnits.getSelectedItem();
			if (selectedUnit.equals("Minutes") || selectedUnit.equals("Hours")) {
				enable = false;
			}
			_cbRotationSyncEnabled.setEnabled(enable);
			enableRotationSyncFields(enable & _cbRotationSyncEnabled.isSelected());
        } else if (e.getSource().equals(_cbRotationSyncEnabled)) {
			enableRotationSyncFields(_cbRotationSyncEnabled.isSelected());
			super.actionPerformed(e);
        } else {
			super.actionPerformed(e);
		}
    }
	

    protected void enableRotationSyncFields( boolean enable ) {
		if ( _tfRotationSyncHour != null )
			_tfRotationSyncHour.setEnabled(enable);
		if ( _tfRotationSyncMin != null )
			_tfRotationSyncMin.setEnabled(enable);
	}

    protected void enableFields( boolean enable ) {
		if ( _tfLog != null )
			_tfLog.setEnabled(enable);
        // only enable the button if local
		if ( _bLog != null && isLocal())
   			_bLog.setEnabled(enable);
		if ( _tfLogMode != null )
			_tfLogMode.setEnabled(enable);
		if ( _tfLogsPerDir != null )
			_tfLogsPerDir.setEnabled(enable);
		if ( _tfLogSize != null )
			_tfLogSize.setEnabled(enable);
		if ( _tfRotationTime != null )
			_tfRotationTime.setEnabled(enable);
		if ( _cbRotationUnits != null )
			_cbRotationUnits.setEnabled(enable);
		if ( _cbRotationSyncEnabled != null ) {
			_cbRotationSyncEnabled.setEnabled(enable);
			enableRotationSyncFields(enable & _cbRotationSyncEnabled.isSelected());
		}
		if ( _tfMaxDiskSpace != null )
			_tfMaxDiskSpace.setEnabled(enable);
		if ( _tfMinFreeSpace != null )
			_tfMinFreeSpace.setEnabled(enable);
		if ( _tfMaxDaysOld != null )
			_tfMaxDaysOld.setEnabled(enable);
		if ( _cbExpirationUnits != null )
			_cbExpirationUnits.setEnabled(enable);
		repaint();
    }

    /**
      * Called when the object is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
		super.select( parent, viewInstance );
		ContainerPanel p = getParentPanel();
		if ( p != null ) {
			p.getOKButton().setVisible( true );
			p.getCancelButton().setVisible( true );
			p.updateButtons();
		}
	}

    /**
	 * Called on errors saving entry changes to the directory
	 *
	 * @param lde LDAPException on saving
	 */
    protected boolean processSaveErrors( LDAPException lde ) {
		JFrame frame = getModel().getFrame();
		if ( lde.getLDAPResultCode() == lde.UNWILLING_TO_PERFORM ) {			
			DSUtil.showErrorDialog( frame,
									"invalid-filename",
									_tfLog.getText().trim(),
									_section );
			return false;
		} else {			
			String ldapError = lde.errorCodeToString(); 
			String ldapMessage = lde.getLDAPErrorMessage(); 
			
			if ((ldapMessage != null) && 
				(ldapMessage.length() > 0)) { 
				ldapError = ldapError + ".\n"+ldapMessage; 
			} 
			String[] args = {ldapError}; 
			DSUtil.showErrorDialog(frame, 
								   "save-error", 
								   args, 
								   _section, 
								   _resource);		
			return false;
		}
	}

    /**
	 * Validate text component, and set appropriate visual feedback.  Overwrites BlankPanel's method.
	 *
	 * @param e Event indicating what changed
	 */
    protected void validate(DocumentEvent e) {
		super.validate(e);
		if ((_tfLogSize != null) && (_tfMaxDiskSpace != null)) {
			String logSize = _tfLogSize.getText();
			String maxDiskSpace = _tfMaxDiskSpace.getText();
			if ((logSize != null) && (maxDiskSpace != null)) {
				try {
					int logSizeValue = Integer.parseInt(logSize);
					int maxDiskSpaceValue = Integer.parseInt(maxDiskSpace);
					// If max disk space is unlimited (-1), there's no need to compare
					// it with the individual log size.
					if ((maxDiskSpaceValue >= 0) && (logSizeValue > maxDiskSpaceValue)) {
						setChangeState(_lLogSize, CHANGE_STATE_ERROR );
						setChangeState(_lMaxDiskSpace, CHANGE_STATE_ERROR );	
						clearValidFlag();
					}
				} catch (NumberFormatException ex) {
					/* The case of the bad String representing a value has been already treated by super.validate(e) */ 
					Debug.println("LogPanel.validate() "+ex);
				}
			}
		}		
    }

	abstract LogContentPanel getViewerPanel();


	protected static final String LOG_DN = "cn=config";
	protected String LOG_ATTR_NAME;
	protected String LOG_MODE_ATTR_NAME;
	protected String LOG_PER_DIR_ATTR_NAME;
	protected String LOG_SIZE_ATTR_NAME;
	protected String ROTATION_SYNC_ENABLED_ATTR_NAME;
	protected String ROTATION_SYNCHOUR_ATTR_NAME;
	protected String ROTATION_SYNCMIN_ATTR_NAME;
	protected String ROTATION_TIME_ATTR_NAME;
	protected String ROTATION_UNITS_ATTR_NAME;
	protected String MAX_DISK_SPACE_ATTR_NAME;
	protected String MIN_FREE_SPACE_ATTR_NAME;
	protected String MAX_DAYS_OLD_ATTR_NAME;
	protected String EXPIRATION_UNITS_ATTR_NAME;
	protected String ENABLED_ATTR_NAME;
	protected String LOG_BASE_NAME;


	/* NOTE: The following are the attribute values that get sent to
	   the Directory, not what the user sees. So they should NOT be
	   localized or changed, unless the Directory Server is changed.
	   To change what the user sees, edit the properties file. */
	protected static final String[] RCOMBO_ENTRIES = {
													"minute",
													"hour",
													"day",
													"week",
													"month"
												};
	protected static final String[]ECOMBO_ENTRIES = {
													"day",
													"week",
													"month"
												};
	
	// Enabled
	protected JCheckBox _cbEnabled;
	protected JButton _bView = null;
	// File name
	protected JTextField _tfLog = null;
	protected JButton _bLog = null;
	// Mode
	protected JTextField _tfLogMode;
	// Rotation
	protected JTextField _tfLogsPerDir;
	protected JTextField _tfLogSize;
	protected JCheckBox _cbRotationSyncEnabled;
	protected JTextField _tfRotationSyncHour;
	protected JTextField _tfRotationSyncMin;
	protected JTextField _tfRotationTime;
	protected JComboBox  _cbRotationUnits;
	// Expiration fields
	protected JTextField _tfMaxDiskSpace;
	protected JTextField _tfMinFreeSpace;
	protected JTextField _tfMaxDaysOld;
	// The labels of the fields where we perform the control in validate()
	protected JLabel _lMaxDiskSpace;
	protected JLabel _lLogSize;
	protected JComboBox  _cbExpirationUnits;
	protected ResourceSet _resource = DSUtil._resource;
    protected static final String _section = "log";
}

/**
 * This class is the DSEntry class for file mode entry.
  * It enforces Unix-style file permissions (0-7)(0-7)(0-7).
  */

class DSEntryFileMode extends DSEntryInteger {
	public DSEntryFileMode( String model,
						   JTextField view1, 
						   JComponent view2) {
		super(model,
			  view1,
			  view2,
			  0,
			  777,
			  1);
	}
	
	/**
	  * Override DSEntryInteger.doValidate()
	  */
	public int doValidate() {
		int result = super.doValidate();
		boolean isValid = (result == DSE_VALID_NOMOD) || (result == DSE_VALID_MOD);
		if (isValid) {
			try {
				boolean tripletgood = false;
				JTextField tf = (JTextField)getView(0);
				int triplet = Integer.parseInt(tf.getText());
				int mode = triplet / 100;
				if ( (mode >= 0 && mode <= 7) && (tf.getText().length() == 3) ) {
					triplet -= mode * 100;
					mode = triplet / 10;
					if (mode >= 0 && mode <= 7) {
						triplet -= mode * 10;
						mode = triplet;
						if (mode >= 0 && mode <= 7) {
							tripletgood = true;
						}
					}
				}
				result = ( (tripletgood == true) ?
										DSE_VALID_MOD : DSE_INVALID_MOD );
			} catch ( NumberFormatException e) {
			}
		}
		return result;
	}            
}
