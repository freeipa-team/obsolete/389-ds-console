/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/* ConfigEntry.java - class that implements a standalone 
                      configuration entry
 */

package com.netscape.admin.dirserv.panel;

import netscape.ldap.*;
import java.util.*;

public class ConfigEntry {
    public ConfigEntry (String dn, LDAPConnection ldc){
        _configDN = dn;
        _ldc = ldc;
    }

    public void addAttribute (String attrName, DSEntry dsEntry){
        _attrTable.put (attrName, dsEntry);    
    }

    public void setAddAttributes (/*LDAPAttributeSet*/LDAPAttribute[] attrSet){
        _addAttrs = attrSet;
    }

    public void read () throws LDAPException{
        int i;
        int size;
        String [] attrs;
        Enumeration keys;
        String name;
        DSEntry dsEntry;
        LDAPAttribute lda;
        LDAPEntry entry = null;

        /* get the list of attributes we are interested in */
        size = _attrTable.size ();
        if (size == 0)
        {
            /* ONREPL print warning */
            return;
        }

        attrs = new String [size];

        keys = _attrTable.keys ();
        i = 0;
        while (keys.hasMoreElements()) {
            attrs [i] = (String)keys.nextElement();
            i ++;
        }
        
        /* read the entry */
        try {
            entry = _ldc.read (_configDN, attrs);
        } catch (LDAPException lde){
            if (lde.getLDAPResultCode() != lde.NO_SUCH_OBJECT)
                throw lde;
        }

        _configEntry = entry;
        
        /* update correspondent form fields */
        keys = _attrTable.keys ();
        while (keys.hasMoreElements()) {
            name = (String)keys.nextElement ();
            dsEntry = (DSEntry)_attrTable.get (name);
            if (entry != null){
                lda = entry.getAttribute (name);    
                dsEntry.remoteToLocal(lda.getStringValues());
            }
            else{
                dsEntry.remoteToLocal("");                
            }
            dsEntry.show ();        
        }
    }

    public void write () throws LDAPException{
        /* we are adding new entry */
        if (_configEntry == null)
            _configEntry = addEntry ();
        else
            modifyEntry ();
    }

    public void delete () throws LDAPException{
        /* delete entry */
        _ldc.delete (_configDN);
    }

    private LDAPEntry addEntry () throws LDAPException{
        LDAPEntry entry;
        LDAPAttribute attr;
        LDAPAttributeSet attrs;
        Enumeration entries;
        Enumeration keys;
        DSEntry dsEntry;
        String name;
        String[] values;

        /* populate entry */
        if (_addAttrs != null)
            attrs = new LDAPAttributeSet (_addAttrs);
        else
            attrs = new LDAPAttributeSet ();

        keys = _attrTable.keys ();
        while (keys.hasMoreElements ()){
            name = (String)(keys.nextElement ());
            dsEntry = (DSEntry)_attrTable.get (name);
            dsEntry.store ();
            values = dsEntry.localToRemote ();
            attr = new LDAPAttribute (name, values);
            attrs.add (attr);
        }        

        entry = new LDAPEntry (_configDN, attrs);

        try{        
            _ldc.add (entry);
        }catch (LDAPException lde){
            entry = null; /* help garbage collection */
            throw (lde);
        }
        
        /* reset DSEntries */
        entries = _attrTable.elements ();
        while (entries.hasMoreElements ()){
            dsEntry = (DSEntry)entries.nextElement ();
            dsEntry.reset ();
        }

        return entry;
        
    }
   
    private void modifyEntry () throws LDAPException{
        LDAPModificationSet mods;
        LDAPAttribute       attr;
        Enumeration         keys;
        Enumeration         entries;
        String              name;
        DSEntry             dsEntry;
        String[]            values;

        mods = new LDAPModificationSet ();

        keys = _attrTable.keys ();
        while (keys.hasMoreElements ()){
            name = (String)(keys.nextElement ());
            dsEntry = (DSEntry)_attrTable.get (name);
            dsEntry.store ();
            if (dsEntry.getDirty()){
                values = dsEntry.localToRemote ();
                attr = new LDAPAttribute (name, values);
                mods.add(LDAPModification.REPLACE, attr);    
            }
        }              
        
        try{
            _ldc.modify (_configDN, mods);
        }catch (LDAPException lde){
            throw lde;
        }

        /* reset DSEntries */
        entries = _attrTable.elements ();
        while (entries.hasMoreElements ()){
            dsEntry = (DSEntry)entries.nextElement ();
            dsEntry.reset ();
        }
    }

    /* private data */
    private LDAPEntry _configEntry = null;
    private String _configDN;    
    private LDAPConnection _ldc;
    /* attributes that should be part of the entry when it added as
       opposed to modified, like object class
     */
    private /*LDAPAttributeSet*/ LDAPAttribute[] _addAttrs = null;     
    private Hashtable _attrTable = new Hashtable();
}
