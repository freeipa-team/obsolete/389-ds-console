/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.io.File;
import java.util.Vector;
import java.util.Enumeration;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import javax.swing.AbstractButton;
import javax.swing.UIManager;
import java.awt.Color;

import com.netscape.admin.dirserv.*;

/**
 * A Directory Server attribute must implement a subclass in order
 * to be able to update itself with its representation stored in the
 * Directory Server, display that value to the user, allow the user to
 * edit that value, and store the value back to the Directory Server
 *
 * @version
 * @author
 */
public class DSEntryText extends DSEntry {
    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle text entry fields which accept file and directory names.  One
     * model value, one widget.  This class verifies that the path and/or
     * file name is valid.
     *
     * @param model the initial value, if any, or just null
     * @param view the JTextComponent used as the text entry field
     */
    public DSEntryText(
        String model,
        JTextComponent view
    ) {
        super(model, view);
    }

    /**
     * Extends the functionality of the base DSEntry, specialized to
     * handle text entry fields which accept file and directory names.  One
     * model value, one widget.  This class verifies that the path and/or
     * file name is valid. The corresponding label also gets passed to this
     * constructor for visual feedback.
     *
     * @param model the initial value, if any, or just null
     * @param view1 the JComponent used as the text entry field
     * @param view2 the label for the corresponding view.
     */
    public DSEntryText(String model, JComponent view1, JComponent view2) {
        super(model, view1, view2);
    }

    public void show() {
        // update the view
		JTextComponent tf = (JTextComponent)getView(0);
		tf.setText(getModel(0));

        viewInitialized ();
	}

	/**
	 * Since data is dynamically updated when it is modified
     * thought a call to updateModified, nothing needs ro be
     * done here
     */
	public void store() {
	    
    }

	protected void updateModel (){
		JTextComponent tf = (JTextComponent)getView(0);
	    String path = tf.getText();
	    setModelAt(path, 0);
	}

    public int validate() {
        return 0;
    }
}

