/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.tree.TreeNode;
import javax.swing.text.JTextComponent;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.node.DataRootResourceObject;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.nmclf.SuiConstants;
import netscape.ldap.*;
import netscape.ldap.util.DN;

/**
 * Panel for Directory Server Replica Setting
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	 	10/20/1999
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class ReplicaSettingPanel extends BlankPanel {
    
    /*==========================================================
     * constructors
     *==========================================================*/
     
    /**
     * Public Constructor
     * construction is delayed until selected.
     * @param parent parent panel
     */
    public ReplicaSettingPanel(IDSModel model,
			       IDSContentListener idscl,
			       LDAPEntry instEntry,
			       LDAPEntry mapTreeEntry, LDAPEntry replicaEntry) {
	super(model, "replication");
	setTitle(_resource.getString(_section+"-setting","title"));
	_model = model;
	_helpToken = "configuration-replication-replicasettings-help";
	_instEntry = instEntry;
	_mapTreeEntry = mapTreeEntry;
	if (replicaEntry != null) {
	    _repSettingDN = replicaEntry.getDN();
	}
	_idscListener = idscl;
	_refreshWhenSelect = false;
    }
	
    /*==========================================================
     * public methods
     *==========================================================*/	
	
    //================== ITabPanel functions ===================
    /**
     * Actual Panel construction
     */
    public void init() {
		if (_isInitialized) {
			return;
		}

		_myPanel.setLayout(new GridBagLayout());
		
		createEnableReplicaArea(_myPanel);
		
		createReplicaTypeArea(_myPanel);
		
		createCommonAttributeArea(_myPanel);
		
		createConsumerArea(_myPanel);
	
		addBottomGlue();		  		

		populateData();

        _isInitialized =true;
		checkFields();
		checkEnablingState();
    }

    private void createEnableReplicaArea(Container cc) {
        GridBagConstraints gbc = new GridBagConstraints();
        // Enable Replication check box
        gbc.gridwidth = gbc.RELATIVE;
        gbc.anchor = gbc.NORTHWEST;
        gbc.fill = gbc.NONE;
        gbc.insets = (Insets)ReplicationTool.BOTTOM_INSETS.clone();
        _cbReplicaEnabled = makeJCheckBox(_section, "enable", true, _resource);
	_cbReplicaEnabled.setToolTipText(_resource.getString( "replication-replica-enable","ttip"));		
        cc.add(_cbReplicaEnabled, gbc);
                
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        cc.add(Box.createHorizontalGlue(), gbc);
	}

    private void createReplicaTypeArea(Container cc) {
        _replicaTypePanel = new GroupPanel(_resource.getString(_section,"type-label"));
		
		GridBagConstraints gbc = new GridBagConstraints();		
		gbc.weightx = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.NORTHWEST;
		cc.add(_replicaTypePanel, gbc);

		/* Replica type radiobuttons */
		_rbIsSimpleMaster = makeJRadioButton(_section, "simplemaster", false, _resource);
		_rbIsSimpleMaster.setToolTipText(_resource.getString( "replication-replica-simplemaster",
														 "ttip"));
		_rbIsSlave = makeJRadioButton(_section, "slave", true, _resource);
		_rbIsSlave.setToolTipText(_resource.getString( "replication-replica-slave",
														 "ttip"));
		_rbIsHub = makeJRadioButton(_section, "hub", false, _resource);
		_rbIsHub.setToolTipText(_resource.getString( "replication-replica-hub",
														 "ttip"));
		_rbIsMultiMaster = makeJRadioButton(_section, "multimaster", false, _resource);
		_rbIsMultiMaster.setToolTipText(_resource.getString( "replication-replica-multimaster",
														 "ttip"));

		ButtonGroup replicaType = new ButtonGroup();
		replicaType.add(_rbIsSimpleMaster);
		replicaType.add(_rbIsMultiMaster);
		replicaType.add(_rbIsHub);
		replicaType.add(_rbIsSlave);

		gbc.insets = (Insets)ReplicationTool.DEFAULT_INSETS.clone();
		gbc.fill = gbc.NONE;
		gbc.weightx = 1.0;
		gbc.anchor = gbc.NORTHWEST;
		gbc.gridwidth = gbc.RELATIVE;
		_replicaTypePanel.add(_rbIsSimpleMaster, gbc);
		gbc.gridwidth = gbc.REMAINDER;
		_replicaTypePanel.add(_rbIsMultiMaster, gbc);
		gbc.gridwidth = gbc.RELATIVE;
		_replicaTypePanel.add(_rbIsHub, gbc);
		gbc.gridwidth = gbc.REMAINDER;
		_replicaTypePanel.add(Box.createHorizontalGlue(), gbc);
		gbc.gridwidth = gbc.RELATIVE;
		_replicaTypePanel.add(_rbIsSlave, gbc);
		gbc.gridwidth = gbc.REMAINDER;
		_replicaTypePanel.add(Box.createHorizontalGlue(), gbc);
	
    }

	private void createCommonAttributeArea(Container cc) {	
        _commonAttributePanel = new GroupPanel(_resource.getString(_section, "common-panel-label"));
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 1.0;	
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.NORTHWEST;
		gbc.gridwidth = gbc.REMAINDER;
		cc.add(_commonAttributePanel, gbc);	
		
		JPanel replicaIDPanel = new JPanel(new GridBagLayout());		
		gbc.gridwidth = gbc.REMAINDER;
		_commonAttributePanel.add(replicaIDPanel, gbc);

		JPanel purgeDelayPanel = new JPanel(new GridBagLayout());
		gbc.gridwidth = gbc.REMAINDER;
		_commonAttributePanel.add(purgeDelayPanel, gbc);
		
		JPanel consumerTypePanel = new JPanel(new GridBagLayout());
		gbc.gridwidth = gbc.REMAINDER;
		_commonAttributePanel.add(consumerTypePanel, gbc);

		// Replica Id 
		_lReplicaID = makeJLabel(_section, "replicaid", _resource);
		gbc.fill = gbc.NONE;
		gbc.insets = (Insets)ReplicationTool.DEFAULT_INSETS.clone();
		gbc.gridwidth = 3;
		gbc.weightx = 0.0;
		replicaIDPanel.add(_lReplicaID, gbc);
		
		gbc.gridwidth = gbc.RELATIVE;
		_tfReplicaID = makeNumericalJTextField(_section, "replicaid", _resource);
		_lReplicaID.setLabelFor(_tfReplicaID);
		_tfReplicaID.getDocument().addDocumentListener(this);		
		replicaIDPanel.add(_tfReplicaID, gbc);
			
		_lReplicaIDWarning = makeMultiLineLabel(2, 40, 
												_resource.getString(_section, "replicaid-warning-label"));
		gbc.weightx = 1.0;
		replicaIDPanel.add(_lReplicaIDWarning, gbc);

		// Replica Purge Delay
		_lPurgeDelay = makeJLabel(_section, "purgedelay", _resource );
		_lPurgeDelay.setToolTipText(_resource.getString( "replication-replica-purgedelay",
														 "ttip"));
		gbc.fill = gbc.NONE;
		gbc.gridwidth = 5;
		gbc.weightx = 0.0;		
		purgeDelayPanel.add(_lPurgeDelay, gbc);

		_tfPurgeDelay = makeNumericalJTextField(_section, "purgedelay", _resource );		
		_lPurgeDelay.setLabelFor(_tfPurgeDelay);
		_tfPurgeDelay.getDocument().addDocumentListener(this);
		gbc.gridwidth--;
		purgeDelayPanel.add(_tfPurgeDelay, gbc);		
		
		/* The following items have to be aligned with the int[] CONVERSION_VALUES } */
		_cboPurgeUnit = makeJComboBox();
		_lPurgeDelay.setLabelFor(_cboPurgeUnit);
		_cboPurgeUnit.addItem( _resource.getString("replication-replica-purgedelay",
												  "days"));
		_cboPurgeUnit.addItem( _resource.getString("replication-replica-purgedelay",
												  "hours"));
		_cboPurgeUnit.addItem( _resource.getString("replication-replica-purgedelay",
												  "minutes"));
		_cboPurgeUnit.addItem( _resource.getString("replication-replica-purgedelay",
												  "seconds"));
		
		gbc.gridwidth--;		
		purgeDelayPanel.add( _cboPurgeUnit, gbc );
		
		_cbPurgeDelayNever = makeJCheckBox(_section, "purgedelay-never", true, _resource);
		_cbPurgeDelayNever.setToolTipText(_resource.getString( "replication-replica-purgedelay-never",
														 "ttip"));
		gbc.gridwidth = gbc.RELATIVE;
		purgeDelayPanel.add(_cbPurgeDelayNever, gbc);

		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;
		purgeDelayPanel.add(Box.createHorizontalGlue(), gbc);
   }


	/* NOTE: this method uses _tfReplicaID.  Should be called after createCommonAttributeArea */

   private void createConsumerArea(Container cc) {
        _consumerPanel = new GroupPanel(_resource.getString(_section, "consumer-panel-label"));
		
		GridBagConstraints gbc = new GridBagConstraints();		
		gbc.weightx = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
		gbc.anchor = gbc.NORTHWEST;		
		cc.add(_consumerPanel, gbc);

		gbc.insets = (Insets)ReplicationTool.DEFAULT_INSETS.clone();
		gbc.insets.bottom = 0;
		int defaultRightInsets = gbc.insets.right;
		int defaultLeftInsets = gbc.insets.left;
		int defaultTopInsets = gbc.insets.top;
		/* 
		 * Bind DNs
		 */

		/* Bind DNs list label */		
		_lBindDNList = new JLabel(_resource.getString(
														  _section,"bindDNlistlabel-label") );		
		gbc.weightx = 0.0;				
		_consumerPanel.add(_lBindDNList, gbc);				

		/* List of bind DNs and Delete Bind DNs button */				
		_bindDNListData = new DefaultListModel();
		_bindDNList = new JList( _bindDNListData );
		_lBindDNList.setLabelFor(_bindDNList);
		_bindDNList.setVisibleRowCount(BIND_DN_LIST_ROWS);
		_bindDNList.setCellRenderer( new BindDNCellRenderer() );

		JScrollPane scrollBindDN = new JScrollPane();
		scrollBindDN.getViewport().setView(_bindDNList);

		_bDeleteBindDN = UIFactory.makeJButton(this, 
												   _section,
												   "deleteBindDN",
												   _resource);
		
		gbc.gridwidth = gbc.RELATIVE;
		
		gbc.weightx=1.0;
		gbc.insets.top = 0;
		_consumerPanel.add(scrollBindDN, gbc);
		
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx=0.0;
		gbc.insets.right = defaultLeftInsets;
		_consumerPanel.add(_bDeleteBindDN, gbc);
		gbc.insets.right = defaultRightInsets;
		gbc.insets.top = defaultTopInsets;
		/* New bind DN label */
		JLabel lNewBindDN = makeJLabel(_resource.getString(_section, "newBindDN-label"));		  
		  
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx=1.0;
		gbc.insets.bottom = 0;
		_consumerPanel.add(lNewBindDN, gbc);

		/* New bind DN textfield and add button */
		_tfNewBindDN = makeJTextField();
		lNewBindDN.setLabelFor(_tfNewBindDN);
		_tfNewBindDN.getDocument().addDocumentListener(this);
		_bAddBindDN = UIFactory.makeJButton(this, 
											  _section,
											  "addBindDN",
											  _resource);
		
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx = 1.0;
		gbc.insets.top = 0;
		_consumerPanel.add(_tfNewBindDN, gbc);

		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx=0.0;
		gbc.insets.right = defaultLeftInsets;
		_consumerPanel.add(_bAddBindDN, gbc);		
		gbc.insets.right = defaultRightInsets;

		/* 
		 * Referrals 
		 */
		/* Referral list label */
		_lReferralList = new JLabel(_resource.getString(
                                                        _section,"referrallistlabel-label") );
		gbc.weightx = 0.0;				
		gbc.insets.top = 2 * defaultTopInsets;
		_consumerPanel.add(_lReferralList, gbc);

		/* List of referrals and Delete referral button */				
		_referralListData = new DefaultListModel();
		_referralList = new JList( _referralListData );
		_lReferralList.setLabelFor(_referralList);
		_referralList.setVisibleRowCount(REFERRAL_LIST_ROWS);
		_referralList.setCellRenderer( new ReferralCellRenderer() );

		JScrollPane scrollReferral = new JScrollPane();
		scrollReferral.getViewport().setView(_referralList);

		_bDeleteReferral = UIFactory.makeJButton(this, 
												_section,
												 "deleteReferral",
												 _resource); 		
		
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx=1.0;
		gbc.insets.top = 0;
		_consumerPanel.add(scrollReferral, gbc);		

		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx=0.0;
		gbc.insets.right = defaultLeftInsets;
		_consumerPanel.add(_bDeleteReferral, gbc);
		gbc.insets.top = defaultTopInsets;
		gbc.insets.right = defaultRightInsets;

		/* New referral label */		
		JLabel lNewReferral = makeJLabel(_resource.getString(_section, "newReferral-label"));		  
		
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx=1.0;
		gbc.insets.bottom = 0;
		_consumerPanel.add(lNewReferral, gbc);
		gbc.insets.bottom = defaultTopInsets;
		gbc.insets.top = 0;
		
		/* New referral textfield and add button */
		_tfNewReferral = makeJTextField();		
		lNewReferral.setLabelFor(_tfNewReferral);

		_bAddReferral = UIFactory.makeJButton(this, 
											  _section,
											  "addReferral",
											  _resource);
		
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx=1.0;
		_consumerPanel.add(_tfNewReferral, gbc);

		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx=0.0;
		gbc.insets.right = defaultLeftInsets;
		_consumerPanel.add(_bAddReferral, gbc);
		gbc.insets.right = defaultRightInsets;

		/* Construct button */
		_bConstructReferral = UIFactory.makeJButton(this, 
													_section,
													"constructReferral",
													_resource);         		
		gbc.gridwidth = gbc.RELATIVE;
		gbc.weightx=1.0;		
		_consumerPanel.add(Box.createHorizontalGlue(), gbc);

		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx=0.0;
		gbc.insets.top = defaultTopInsets;
		gbc.insets.right = defaultLeftInsets;
		gbc.insets.bottom = defaultTopInsets;
		_consumerPanel.add(_bConstructReferral, gbc);		
   }
	
	private void populateData() {
		readValuesFromServer();
		populateFieldsWithValuesFromServer();
	}

	private void readValuesFromServer() {
		/* Default values */
        _saveReplicaEnabled = false;
        _saveWindowsSyncEnabled = false;

		_saveDS5ReplicaTypeExist = false;
		_saveReplicaIDExist = false;
		_saveReplicaIDExist = false;
		_saveBindDNExist = false;
		_saveReferralListExist = false;
		_savePurgeDelayExist = false;
		_saveDS5FlagsExist = false;

		_saveReadOnly = false;		
		_savePurgeDelay = ReplicationTool.DEFAULT_PURGE_DELAY;
		_saveReplicaID = 0;
		_saveBindDNList = new Vector();
		_saveDS5Flags = true;
		_saveReferralList = new Vector();

		_saveIsSimpleMaster = false;
		_saveIsSlave = false;
		_saveIsMultiMaster = false;
		_saveIsHub = false;
		/* first, get the replica entry.  it should be the cn=replica child of the
		   _mapTreeEntry */
        if (_repSettingDN == null) {
            _repSettingDN = ReplicationTool.REPLICA_RDN + "," +
                _mapTreeEntry.getDN();
        }
        
                LDAPEntry replicaEntry = null;
		LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
		try {
			replicaEntry = ldc.read( _repSettingDN );
			_saveReplicaEnabled = true;
			String valattr;

			// nsDS5ReplicaType		
			valattr = DSUtil.getAttrValue( replicaEntry, ReplicationTool.REPLICA_TYPE_ATTR );			
			_saveReadOnly = (valattr.length() != 0) &&
				valattr.equalsIgnoreCase(REP_TYPE_READONLY);
			if (valattr.length() != 0) {
				_saveDS5ReplicaTypeExist = true;
			}			

			// nsds5ReplicaPurgeDelay		
			valattr = DSUtil.getAttrValue( replicaEntry, ReplicationTool.REPLICA_PURGE_DELAY_ATTR );
			if( valattr.length() != 0 ) {
				_savePurgeDelayExist = true;
				_savePurgeDelay = valattr;
			}

			// nsDS5ReplicaID		
			valattr = DSUtil.getAttrValue( replicaEntry, ReplicationTool.REPLICA_ID_ATTR );
			if( valattr.length() != 0 ) {
				_saveReplicaIDExist = true;
				Integer tmpID;
				try {
					tmpID = Integer.valueOf( valattr );
					_saveReplicaID = tmpID.intValue();
				} catch ( NumberFormatException e ) {
					_saveReplicaID = 0;
				}
			}

			// nsDS5ReplicaBindDN
			String[] bindDNList = DSUtil.getAttrValues( replicaEntry, ReplicationTool.REPLICA_BINDDN_ATTR );			
			if (( bindDNList != null ) &&
				(bindDNList.length > 0)) {		
				for (int i=0; i< bindDNList.length; i++) {
					_saveBindDNList.addElement(bindDNList[i]);
				}
				_saveBindDNExist = true;
			}			

			// ndsDS5flags	
			valattr = DSUtil.getAttrValue( replicaEntry, ReplicationTool.REPLICA_LOG_CHANGES_ATTR );
			_saveDS5Flags = (valattr.length() != 0) &&
				valattr.equals(REP_FLAG_LOG_CHANGES);
			if (valattr.length() != 0) {
				_saveDS5FlagsExist = true;
			}

			// nsDS5ReplicaReferral
			String[] referralList = DSUtil.getAttrValues( replicaEntry, ReplicationTool.REPLICA_REFERRAL_ATTR );			
			if (( referralList != null ) &&
				(referralList.length > 0)) {		
				for (int i=0; i< referralList.length; i++) {
					_saveReferralList.addElement(referralList[i]);
				}
				_saveReferralListExist = true;
			}

			/* We make the mapping from what we read from the server and the replica type radiobuttons */
			_saveIsMultiMaster = _saveDS5Flags && (_saveBindDNList.size() > 0) && !_saveReadOnly;
			_saveIsHub = _saveDS5Flags && (_saveBindDNList.size() > 0) && _saveReadOnly;
			if (!_saveIsMultiMaster && !_saveIsHub) {
				_saveIsSlave = !_saveDS5Flags && _saveReadOnly;
				_saveIsSimpleMaster = _saveDS5Flags && !_saveReadOnly;
			}
		} catch (LDAPException e){
			/* Error reading entry, if the entry does not exist we continue.  If there is another error
			   we display an error message */
			if (e.getLDAPResultCode() != LDAPException.NO_SUCH_OBJECT) {			
				DSUtil.showLDAPErrorDialog(_model.getFrame(),
										   e,
										   "updating-server-unavailable-title");										   
			}
		}
                
        _syncSettingDN = null;
        if (_syncSettingDN == null) {
            _syncSettingDN = ReplicationTool.WINDOWS_REPLICA_RDN + "," + //XXX
            _mapTreeEntry.getDN();
        }
		LDAPEntry syncEntry = null;
		ldc = getModel().getServerInfo().getLDAPConnection();
        try {
            syncEntry = ldc.read( _syncSettingDN );
            _saveWindowsSyncEnabled = true;
            String valattr;
            
        } catch (LDAPException e){
      /* Error reading entry, if the entry does not exist we continue.  If there is another error
         we display an error message */
            if (e.getLDAPResultCode() != LDAPException.NO_SUCH_OBJECT) {
                DSUtil.showLDAPErrorDialog(_model.getFrame(),
                e,
                "updating-server-unavailable-title");
            }
        }
/*		Debug.println("ReplicaSettingPanel.readValuesFromServer():\n"+
					  "replicaEntry = "+replicaEntry+"\n"+
					  "_saveReplicaEnabled = "+_saveReplicaEnabled+"\n"+
					  "_saveDS5ReplicaTypeExist = "+_saveDS5ReplicaTypeExist+"\n"+
					  "_saveBindDNExist = "+_saveBindDNExist+"\n"+
					  "_saveReferralListExist = "+_saveReferralListExist+"\n"+
					  "_savePurgeDelayExist = "+_savePurgeDelayExist+"\n"+
					  "_saveDS5FlagsExist = "+_saveDS5FlagsExist+"\n"+
					  "_saveReadOnly = "+_saveReadOnly+"\n"+
					  "_savePurgeDelay = "+_savePurgeDelay+"\n"+
					  "_saveReplicaID = "+_saveReplicaID+"\n"+
					  "_saveBindDNList = "+_saveBindDNList+"\n"+
					  "_saveDS5Flags = "+_saveDS5Flags+"\n"+
					  "_saveReferralList = "+_saveReferralList+"\n"+
					  "_saveIsSimpleMaster = "+_saveIsSimpleMaster+"\n"+
					  "_saveIsSlave = "+_saveIsSlave+"\n"+
					  "_saveIsHub = "+_saveIsHub);
*/		
    }

	private void populateFieldsWithValuesFromServer() {
		_cbReplicaEnabled.setSelected(_saveReplicaEnabled);
		
		/* If the replica is not configured by default we propose a dedicated consumer */
		if (!_saveReplicaEnabled) {		
			_rbIsSlave.setSelected(true);
		} else {
			if (_saveIsMultiMaster) {			
				_rbIsMultiMaster.setSelected(true);
			} else if (_saveIsSlave) {				
				_rbIsSlave.setSelected(true);
			} else if (_saveIsSimpleMaster) {				
				_rbIsSimpleMaster.setSelected(true);
			} else {
				_rbIsHub.setSelected(true);
			}
		}			

		/* Common Fields */
		if (_saveReplicaID > 0) {
			_tfReplicaID.setText(String.valueOf(_saveReplicaID));
		} else {
			_tfReplicaID.setText("");
		}
		populatePurgeDelay(_savePurgeDelay);

		/* Consumer Fields */
		_tfNewBindDN.setText("");

		_bindDNListData.clear();
		for (int i=0; i<_saveBindDNList.size(); i++) {
			_bindDNListData.addElement(_saveBindDNList.elementAt(i));
		}
		
		
		_tfNewReferral.setText("");

		_referralListData.clear();
		for (int i=0; i<_saveReferralList.size(); i++) {
			_referralListData.addElement(_saveReferralList.elementAt(i));
		}
	}

	private void populatePurgeDelay(String time) {
		try {
			long value = Integer.valueOf(time).longValue();
			/* The zero value corresponds to the instantaneous purge */
			if (value == 0) {
				_cbPurgeDelayNever.setSelected(true);
				return;
			}

			_cbPurgeDelayNever.setSelected(false);
			for (int i=0; i<CONVERSION_VALUES.length; i++) {
				Debug.println("ReplicaSettingPanel.populatePurgeDelay() "+ value+ " % "+CONVERSION_VALUES[i]+" = "+(value % CONVERSION_VALUES[i]));
				if ((value % CONVERSION_VALUES[i]) == 0) {
					_cboPurgeUnit.setSelectedIndex(i);
					String sValueInUnit = String.valueOf(value / CONVERSION_VALUES[i]);
					_tfPurgeDelay.setText(sValueInUnit);
					break;
				}
			}
		} catch (Exception e) {
			Debug.println("ReplicaSettingPanel.populatePurgeDelay() "+e);
		}
		
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if (!_isInitialized) {
			return;
		}		
		Object source = e.getSource();

		/* If the user tries to define a master or a hub, we need to log the changes.
		   That's why we see if the change log entry exists or not.
		   If the change log entry does not exist we display an error message telling 
		   the user that he/she has to define the Change Log first.  In this dialog we 
		   describe the steps to follow */
		if ((source.equals(_rbIsSimpleMaster) &&
			 _rbIsSimpleMaster.isSelected()) ||
			(source.equals(_rbIsMultiMaster) &&
			 _rbIsMultiMaster.isSelected()) ||
			(source.equals(_rbIsHub) &&
			 _rbIsHub.isSelected())) {
			boolean logChangesExists = false;
			try {
				LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
				String[] attrs = {"dn"};
				LDAPEntry entry = ldc.read(SupplierSettingPanel.CHANGELOG_DN, attrs);
				if (entry != null) {
					logChangesExists = true;
				}
			} catch (LDAPException lde) {
			}
			if (!logChangesExists) {
				_rbIsSlave.setSelected(true);
				DSUtil.showErrorDialog( _model.getFrame(),
										"enable-changelog-error",
										(String[])null,
										_section,
										_resource);
			} else if (!_rbIsHub.isSelected()) {
				/* In the case the user wants to configure a master replica (no hub selected) 
				   we check if the database is on read-only mode or not.  If it is read only we display
				   an error message */				
				if (isDatabaseReadOnly()) {
					_rbIsSlave.setSelected(true);
					DSUtil.showErrorDialog( _model.getFrame(),
											"enable-master-error",
											(String[])null,
											_section,
											_resource);
				}
				
			}
		} else if (source.equals(_bAddBindDN)) {
			actionAddBindDN();
		} else if (source.equals(_bDeleteBindDN)) {
			actionDeleteBindDN();
		} else if (source.equals(_bConstructReferral)) {
			actionConstructReferral();
		} else if (source.equals(_bAddReferral)) {
			actionAddReferral();
		} else if (source.equals(_bDeleteReferral)) {
			actionDeleteReferral();
		} 
		checkFields();
		checkEnablingState();
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void changedUpdate(DocumentEvent e) {
		insertUpdate(e);
    }
	
	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void removeUpdate(DocumentEvent e) {
		insertUpdate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void insertUpdate(DocumentEvent e) {
		if (!_isInitialized) {
			return;
		}		
		checkFields();
		checkAddReferralButton();
		checkAddBindDNButtonAndLabel();
    }

	/**
	 * Some list component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void itemStateChanged(ItemEvent e) {		
    }

    /**
     * we have to override the BlankPanel okCallback here.  If the replica
     * was disabled, we just want to remove the entry, we don't want to do
     * anything else
     */
    public void okCallback() { 
		/* In some particular cases (multi master with 4.x compatibility and change to slave replica
		   with replica agreements defined ) we ask for confirmation to the user */
		if (saveConfirmationDialog()) {
			/* We read the contents of the server before making modifications */
			readValuesFromServer();
			
			/* We make the modifications */
			store();
			
			/* We re-read from the server and update the contents of the replica entry AFTER our modifications */
			resetCallback();
		}
    }

	public void resetCallback() {
		populateData();
		checkFields();
		checkEnablingState();
	}

   /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
    public boolean refresh () {
		Debug.println("ReplicaSettingPanel.refresh()");
		
		populateData();
		checkFields();
		checkEnablingState();
				
		return true;
	}

   /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
    public void refreshFromServer () {
		Debug.println("ReplicaSettingPanel.refreshFromServer()");
		resetCallback();
	}

	/**
	  * Method called when the user clicks on 'Construct...' button.
	  * It displays the LDAP URL editor, if the user creates an URL (does click on 'OK' in the editor),
	  * we update the text field that contains the candidate LDAP URL referral (we don't update directly the referral
	  * list).
	  */
	private void actionConstructReferral() {		
		String suffix = DSUtil.getAttrValue(_instEntry,
											ReplicationTool.PUBLIC_SUFFIX_ATTR);		
		LDAPUrl initUrl = new LDAPUrl(null, LDAPv2.DEFAULT_PORT, suffix);
		
		int editableOptions = LDAPUrlDialog.EDITABLE_HOST | LDAPUrlDialog.EDITABLE_PORT;
		LDAPUrlDialog dlg = new LDAPUrlDialog( getModel().getFrame(), initUrl.getUrl(), editableOptions);	 		
		dlg.packAndShow();		
		LDAPUrl url = dlg.getLDAPUrl();		
	    if ( url != null ) {
			_tfNewReferral.setText( url.getUrl() );
			_bAddReferral.setEnabled( true );
		}
	}

	/**
	  * Method called when the user clicks on 'Delete' button.
	  * It removes the selected referrals from the list of referrals
	  */
	private void actionDeleteReferral() {
		Object[] referralToDelete = _referralList.getSelectedValues();
		if (referralToDelete != null) {
			for (int i=0; i < referralToDelete.length; i++) {
				_referralListData.removeElement(referralToDelete[i]);
          }
		}
	}

	/**
	 * Method called when the user clicks on 'Add' button of the bind DN.
	 * It adds the DN in the list of bind DNs.
	 */
	private void actionAddBindDN() {
		String newBindDN = _tfNewBindDN.getText().trim();
		
		_bindDNListData.addElement(newBindDN);
		_tfNewBindDN.setText("");
		_bAddBindDN.setEnabled(false);		
	}

	/**
	  * Method called when the user clicks on 'Delete' button.
	  * It removes the selected bind dns from the list
	  */
	private void actionDeleteBindDN() {
		Object[] bindDNToDelete = _bindDNList.getSelectedValues();
		if (bindDNToDelete != null) {
			for (int i=0; i < bindDNToDelete.length; i++) {
				_bindDNListData.removeElement(bindDNToDelete[i]);
			}
		}
	}

	/**
	  * Method called when the user clicks on 'Add' button.
	  * It adds the LDAP URL in the list of URLs.
	  */
	private void actionAddReferral() {
		String newReferral = _tfNewReferral.getText().trim();
		boolean addReferral = true;
		/* IF the URL provided has not a recognised syntax, we display a warning dialog */
		if (!DSUtil.isValidLDAPUrl(newReferral)) {
			int wantToContinue = DSUtil.showConfirmationDialog( getModel().getFrame(),
																"LDAPUrl-no-good", 
																(String)null,
																"mappingtree-referral");
			if (wantToContinue != JOptionPane.YES_OPTION) {
				addReferral = false;								
			}
		}
		if (addReferral) {
			_referralListData.addElement(newReferral);
			_tfNewReferral.setText("");
			_bAddReferral.setEnabled(false);
		}
	}

	/**
	  * This methos tells wether the database associated witht this replica is in read only
	  * mode or not.
	  *
	  * @return true if the database of this replica is on read-only mode.
	  */
	private boolean isDatabaseReadOnly() {
		boolean isDatabaseReadOnly = false;
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		String[] attrs = {"nsslapd-readonly"};
		LDAPEntry entry = null;
		try {
			entry = ldc.read(_instEntry.getDN(), attrs);
		} catch (LDAPException e) {
			Debug.println("ReplicaSettingPanel.isDatabaseReadOnly(): "+e);
		}
		if (entry != null) {			
			String readOnlyMode = DSUtil.getAttrValue( entry, "nsslapd-readonly" );
			if (readOnlyMode != null) {
				if (readOnlyMode.trim().equalsIgnoreCase("on")) {
					isDatabaseReadOnly = true;
				}
			}
		}
		return isDatabaseReadOnly;
	}

	/**
	  * This method is used to ask to the user if for some particular configurations
	  * he / she wants to continue
	  */
	private boolean saveConfirmationDialog() {
		boolean confirmation = true;
		if (_rbIsSlave.isSelected() &&
				   !_saveIsSlave &&
				   _saveReplicaEnabled &&
				   hasReplicationAgreements()) {
			/* The case where we had a defined replica and we change it to slave,
			   we display an information dialog telling the user that the defined
			   replication agreements are no longer valid */
			int option = DSUtil.showConfirmationDialog(
													   _model.getFrame(),
													   "slave-defined-agreements-no-valid",
													   "",
													   _section,
													   _resource);		
			if (option != JOptionPane.YES_OPTION) {
				confirmation = false;
			} else {
			/* User clicked yes, so we'll be deleting replication agreements now.
			Leaving replication agreements on a consumer may lead to unexpected 
			behavior. #557328 */
				deleteReplicationAgreements();
			}
		}
		return confirmation;
	}

	private void store() {
            /* Replica entry needs to be deleted */
            if (!_cbReplicaEnabled.isSelected()) {
                if (_saveReplicaEnabled) {
                    deleteReplicaEntry();
                }
            } else {
                if (!_saveReplicaEnabled) {
                    addReplicaEntry();
                } else {
                    modifyReplicaEntry();
                }
            }
        }
       

	private void deleteReplicaEntry() {
                /* Ask for confirmation since this will delete all replication
                   agreements and erase all replica information */
            int option = DSUtil.showConfirmationDialog(
            _model.getFrame(),
            "confirmrepdelete",
            "",
            _section,
            _resource);
            if (option != JOptionPane.YES_OPTION) {
                return;
            }
            
            try {
                getModel().setWaitCursor( true );
                LDAPConnection ldc =
                getModel().getServerInfo().getLDAPConnection();
                
                if (DSUtil.deleteTree(_repSettingDN, ldc)) {
                    _idscListener.contentChanged();
                } else {
                    Debug.println("ReplicaSettingPanel.deleteReplicaEntry(): error " +
                    "deleting replica " + _repSettingDN);
                }
                
                        /* In the case we deleted  a consumer (hub, multimaster or dedicated consumer) that had
                           defined some referrals we show a message informing that this referrals still work and
                           that the user might want to delete the referrals */
                if (!_saveIsSimpleMaster &&
                hasReferrals()){
                    String suffix = DSUtil.getAttrValue(_instEntry,
                    ReplicationTool.PUBLIC_SUFFIX_ATTR);
                    String[] args = {suffix};
                    DSUtil.showInformationDialog( _model.getFrame(),
                    "delete-referrals-information",
                    args,
                    _section,
                    _resource);
                }
                
                if (DSUtil.deleteTree(_syncSettingDN, ldc)) {
                    _idscListener.contentChanged();
                } 
            } finally {
                _model.setWaitCursor( false );
            }
        }
        
      
	/**
	  * Checks if the mapping tree entry corresponding to this replica contains referrals or not.
	  * @returns true if the mapping tree contains referrals.  false otherwise.
	  */
	private boolean hasReferrals() {
		String[] referrals = null;
		String[] attrs = {"nsslapd-referral"};
		LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
		try {
			LDAPEntry entry = ldc.read(_mapTreeEntry.getDN(),
									   attrs);
			if (entry != null) {
				referrals = DSUtil.getAttrValues(entry,
												 "nsslapd-referral");
			}
		} catch (LDAPException e) {
		}
		return ((referrals != null) &&
				(referrals.length > 0));
	}

	/**
	  * Checks if there are replica agreements defined for this replica or not.
	  * @returns true if there are replication agreements.  false otherwise.
	  */
	private boolean hasReplicationAgreements() {
		boolean hasReplicationAgreements = false;
		
		LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
		LDAPSearchConstraints cons =
			(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
		cons.setMaxResults(1);		

		String[] attrs = {"dn"};
		String AGREEMENT_FILTER = "(objectclass=nsds5replicationagreement)";
		try {			
			LDAPSearchResults search_results = ldc.search(_repSettingDN,
														  ldc.SCOPE_ONE,
														  AGREEMENT_FILTER,
														  attrs,
														  false,
														  cons);
			while (search_results.hasMoreElements() &&
				   (search_results.nextElement() != null)) {				
				hasReplicationAgreements = true;
			}			
		} catch (LDAPException lde) {			
		}
		return hasReplicationAgreements;
	}

	/**
	  * Deletes replication agreements from this replica.
	  */
	private void deleteReplicationAgreements() {
		
		LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
		LDAPSearchConstraints cons =
			(LDAPSearchConstraints)ldc.getSearchConstraints().clone();

		String[] attrs = {"dn"};
		String AGREEMENT_FILTER = "(objectclass=nsds5replicationagreement)";
		try {			
			LDAPSearchResults search_results = ldc.search(_repSettingDN,
														  ldc.SCOPE_ONE,
														  AGREEMENT_FILTER,
														  attrs,
														  false,
														  cons);
			while (search_results.hasMoreElements()) {
				ldc.delete(((LDAPEntry)search_results.nextElement()).getDN());
			}			
		} catch (LDAPException lde) {			
		}
	}

	private void addReplicaEntry() {
		LDAPAttributeSet attrSet = new LDAPAttributeSet();
		
		// objectClass
		LDAPAttribute attr = new LDAPAttribute("objectClass",
											   "nsDS5Replica");
		attrSet.add(attr);

		// nsDS5ReplicaRoot
		/* We read the suffix corresponding to this replica from the mapping tree
		   entry */
		String suffix = DSUtil.getAttrValue(_instEntry,
											ReplicationTool.PUBLIC_SUFFIX_ATTR);
		if (suffix.equals("")) {
			Debug.println("ReplicaSettingPanel.addReplicaEntry: error, could not get suffix of replica");
			return;
		}
		attr = new LDAPAttribute(ReplicationTool.REPLICA_ROOT_ATTR,
		   									   suffix);
		attrSet.add(attr);

		// nsDS5ReplicaType
		/* The hub and the slave are read only */
		String value;
		if (_rbIsSlave.isSelected() ||
			_rbIsHub.isSelected()  ) {
			value = REP_TYPE_READONLY;
		} else {
			value = REP_TYPE_UPDATEABLE;
		}
		attr = new LDAPAttribute(ReplicationTool.REPLICA_TYPE_ATTR,
								 value);
		attrSet.add(attr);

		// ndsDS5flags
		/* If we are a slave we DON'T log changes */
		if (!_rbIsSlave.isSelected()) {
			value = REP_FLAG_LOG_CHANGES;
		} else {
			value = REP_FLAG_NONE;
		}
		attr = new LDAPAttribute(ReplicationTool.REPLICA_LOG_CHANGES_ATTR,
								 value);
		attrSet.add(attr);

		// nsDS5ReplicaID
		/* We add the given value if the user wants to create a master if not 
		 we use the fixed 65535 value */
		if (_rbIsSimpleMaster.isSelected() ||
			_rbIsMultiMaster.isSelected()) {
			value = _tfReplicaID.getText().trim();
		} else {
			value = "65535";
		}
		attr = new LDAPAttribute(ReplicationTool.REPLICA_ID_ATTR,
								 value);
		attrSet.add(attr);		

		// nsds5ReplicaPurgeDelay		
		try {
			if (!_cbPurgeDelayNever.isSelected()) {
				value = _tfPurgeDelay.getText().trim();
				long lValue = Integer.valueOf(value).longValue();
				int index = _cboPurgeUnit.getSelectedIndex();
				lValue = lValue * CONVERSION_VALUES[index];			
				value = String.valueOf(lValue);
			} else {
				value = "0";
			}
			attr = new LDAPAttribute(ReplicationTool.REPLICA_PURGE_DELAY_ATTR,
									 value);
			attrSet.add(attr);
		} catch (Exception e) {
			Debug.println("ReplicaSettingPanel.addReplicaEntry(): "+e);
		}


		// nsDS5ReplicaBindDN
		/* We only add this attribute if the replica is not a simple master */
		if (!_rbIsSimpleMaster.isSelected()) {			
			attr = new LDAPAttribute(ReplicationTool.REPLICA_BINDDN_ATTR);
			Enumeration e = _bindDNListData.elements();
			while( e.hasMoreElements() ) {			   
				attr.addValue((String)e.nextElement());					
			}
			attrSet.add(attr);
		}
		
		// nsDS5ReplicaReferral
		/* We only add this attribute if the replica is not a simple master */
		if (!_rbIsSimpleMaster.isSelected()) {
			attr = new LDAPAttribute(ReplicationTool.REPLICA_REFERRAL_ATTR);
			boolean hasReferralValue = _referralListData.size() > 0;
			if (hasReferralValue) {
				Enumeration e = _referralListData.elements();
				while( e.hasMoreElements() ) {			   
					attr.addValue((String)e.nextElement());					
				}
				attrSet.add(attr);
			}
		}

		/* We add the entry using the created attribute set */
		LDAPConnection ldc =
			getModel().getServerInfo().getLDAPConnection();
		LDAPEntry replicaEntry = new LDAPEntry(_repSettingDN, attrSet);
		try {
			Debug.println("ReplicaSettingPanel.addReplicaEntry(): dn: " +
							  _repSettingDN+ "\n" + replicaEntry);
			ldc.add(replicaEntry);
			_idscListener.contentChanged();
			/* The addition of the replica has been successful.  If we added a read only replica we have to 
			   update the Multiple Database UI (the database for this backend became read only). 
			   The hub and the slave are read only */			
			if (_rbIsSlave.isSelected() ||
				_rbIsHub.isSelected()  ) {
				updateDataNode();
			}
		} catch (LDAPException e) {
			if (e.getLDAPResultCode() != LDAPException.ENTRY_ALREADY_EXISTS) {
				DSUtil.showLDAPErrorDialog(_model.getFrame(),
										   e,
										   "111-title");				   
			} else {
				readValuesFromServer();
				modifyReplicaEntry();
			}
		}
	}
	
	private void modifyReplicaEntry() {
		LDAPModificationSet modSet = new LDAPModificationSet();
		String value;
		int modType;

		// nsDS5ReplicaType
		/* The hub and the slave are read only */
		modType = _saveDS5ReplicaTypeExist?LDAPModification.REPLACE:LDAPModification.ADD;
		boolean readOnly = _rbIsSlave.isSelected() || _rbIsHub.isSelected();
		if ( readOnly ) {
			value = REP_TYPE_READONLY;
		} else {
			value = REP_TYPE_UPDATEABLE;
		}
		if (!_saveDS5ReplicaTypeExist ||
			(readOnly != _saveReadOnly)) {
			modSet.add(modType,
					   new LDAPAttribute(ReplicationTool.REPLICA_TYPE_ATTR,
										 value));
		}

		// ndsDS5flags
		/* If we are a slave we DON'T log changes */
		modType = _saveDS5FlagsExist?LDAPModification.REPLACE:LDAPModification.ADD;
		if (!_rbIsSlave.isSelected()) {
			value = REP_FLAG_LOG_CHANGES;
		} else {
			value = REP_FLAG_NONE;
		}
		if (!_saveDS5FlagsExist ||
			_rbIsSlave.isSelected() == _saveDS5Flags) {
			modSet.add(modType,
					   new LDAPAttribute(ReplicationTool.REPLICA_LOG_CHANGES_ATTR,
										 value));
		}

		// nsDS5ReplicaID
		modType = _saveReplicaIDExist?LDAPModification.REPLACE:LDAPModification.ADD;
		/* We add the given value if the user wants to create a master if not 
		 we use the fixed 65535 value */
		if (_rbIsSimpleMaster.isSelected() ||
			_rbIsMultiMaster.isSelected()) {
			value = _tfReplicaID.getText().trim();
		} else {
			value = "65535";
		}
		if (!_saveReplicaIDExist ||
			!value.equals(String.valueOf(_saveReplicaID))) {
			modSet.add(modType,
					   new LDAPAttribute(ReplicationTool.REPLICA_ID_ATTR,
										 value));
		}

		// nsds5ReplicaPurgeDelay
		modType = _savePurgeDelayExist?LDAPModification.REPLACE:LDAPModification.ADD;		
		try {
			if (!_cbPurgeDelayNever.isSelected()) {
				value = _tfPurgeDelay.getText().trim();
				long lValue = Integer.valueOf(value).longValue();
				int index = _cboPurgeUnit.getSelectedIndex();
				lValue = lValue * CONVERSION_VALUES[index];			
				value = String.valueOf(lValue);
			} else {
				value = "0";
			}
			if (!_savePurgeDelayExist ||
				!value.equals(_savePurgeDelay)) {
				modSet.add(modType,
						   new LDAPAttribute(ReplicationTool.REPLICA_PURGE_DELAY_ATTR,
											 value));
			}
		} catch (Exception e) {
			Debug.println("ReplicaSettingPanel.modifyReplicaEntry(): "+e);
		}
		
		// nsDS5ReplicaBindDN
		/* We only add this attribute if the replica is not a simple master */
		if (!_rbIsSimpleMaster.isSelected()) {
			modType = _saveBindDNExist?LDAPModification.REPLACE:LDAPModification.ADD;			

			LDAPAttribute attr = new LDAPAttribute(ReplicationTool.REPLICA_BINDDN_ATTR);
			Enumeration e = _bindDNListData.elements();
			while( e.hasMoreElements() ) {			   
				attr.addValue((String)e.nextElement());					
			}

			if (!_saveBindDNExist ||
				isBindDNListDirty()) {			
				modSet.add(modType, attr);				
			}
			/* It is a simple master: we delete the attribute */
		} else if (_saveBindDNExist) {
			modSet.add(LDAPModification.DELETE,
					   new LDAPAttribute(ReplicationTool.REPLICA_BINDDN_ATTR));	
		}
		
		// nsDS5ReplicaReferral		
		/* We only add this attribute if the replica is not a simple master */
		if (!_rbIsSimpleMaster.isSelected()) {
			modType = _saveReferralListExist?LDAPModification.REPLACE:LDAPModification.ADD;			
			LDAPAttribute attr = new LDAPAttribute(ReplicationTool.REPLICA_REFERRAL_ATTR);
			boolean hasReferralValue = _referralListData.size() > 0;
			if (hasReferralValue) {
				Enumeration e = _referralListData.elements();
				while( e.hasMoreElements() ) {			   
					attr.addValue((String)e.nextElement());					
				}
			
				if (!_saveReferralListExist ||
					isReferralListDirty()) {
					modSet.add(modType, attr);
				}
				/* If the field is void, we delete the attribute */
			} else if (_saveReferralListExist) {
				modSet.add(LDAPModification.DELETE,
					   new LDAPAttribute(ReplicationTool.REPLICA_REFERRAL_ATTR));
			}
			/* If it is a simple master, we delete the attribute */
		} else if (_saveReferralListExist) {
			modSet.add(LDAPModification.DELETE,
					   new LDAPAttribute(ReplicationTool.REPLICA_REFERRAL_ATTR));
		}

		LDAPConnection ldc =
			getModel().getServerInfo().getLDAPConnection();
		try {
			if (modSet.size() >0) {
				Debug.println("ReplicaSettingPanel.modifyReplicaEntry(): dn: " +
							  _repSettingDN+ "\n" + modSet);
				ldc.modify(_repSettingDN, modSet);
				_idscListener.contentChanged();
			}
		} catch (LDAPException e) {
			if (e.getLDAPResultCode() != LDAPException.NO_SUCH_OBJECT) {
				DSUtil.showLDAPErrorDialog(_model.getFrame(),
										   e,
										   "111-title");								   
			} else {
				addReplicaEntry();
			}
		}
	}
	
	/**
	  * This method performs a refresh on the Root Node of the multiple database UI.
	  * This is called when the Multiple Database UI has to be refreshed (for example when we create
	  * a read only replica */
	protected void updateDataNode() {
		TreeNode rootNode = (TreeNode)_model.getRoot();
		int childNumber = rootNode.getChildCount();
		for (int i=0; i<childNumber; i++) {
			TreeNode child = rootNode.getChildAt(i);
			/* We check if it is the node we are looking for looking at its class */
			if (child instanceof DataRootResourceObject) {				
				/* We perform a refresh the same way we do in the DSResourceModel...*/				
				DataRootResourceObject dataRoot = (DataRootResourceObject)child;
				dataRoot.actionPerformed(new ActionEvent( this,
														  ActionEvent.ACTION_PERFORMED,
														  DSResourceModel.REFRESH ) );
				Component panel = dataRoot.getCustomPanel();
				if ( (panel != null) && (panel instanceof ActionListener) )
					((ActionListener)panel).actionPerformed(new ActionEvent( this,
																			 ActionEvent.ACTION_PERFORMED,
																			 DSResourceModel.REFRESH ) );				
				break;
			}
		}
	}

	protected void checkEnablingState() {
		if (!_isInitialized) {
			return;
		}
		if (_cbReplicaEnabled.isSelected()) {
			setEnabledPanel(_replicaTypePanel, true);
			setEnabledPanel(_commonAttributePanel, true);			
			/* Check the consumer panel */			
			setEnabledPanel(_consumerPanel, !_rbIsSimpleMaster.isSelected());
			_bindDNList.setBackground(_tfNewBindDN.getBackground());
			_referralList.setBackground(_tfNewReferral.getBackground());			

			/* Check the purge delay panel */
			_tfPurgeDelay.setEnabled(!_cbPurgeDelayNever.isSelected());
			_cboPurgeUnit.setEnabled(!_cbPurgeDelayNever.isSelected());


			/* We don't ask for the replica ID if we are NOT defining a master.
			 
			 The only case where we change the replica ID is when  we are
			 changing the replica from a slave to a master */
			boolean enableID = !_rbIsHub.isSelected() &&
				!_rbIsSlave.isSelected() &&
				(!_saveReplicaEnabled || _saveIsHub || _saveIsSlave);
			_tfReplicaID.setEnabled(enableID);
			_lReplicaID.setEnabled(enableID);
			_lReplicaIDWarning.setEnabled(enableID);
		} else {
			setEnabledPanel(_replicaTypePanel, false);
			setEnabledPanel(_commonAttributePanel, false);
			setEnabledPanel(_consumerPanel, false);
			_bindDNList.setBackground(_tfNewBindDN.getBackground());
			_referralList.setBackground(_tfNewReferral.getBackground());
		}
		checkAddReferralButton();
		checkAddBindDNButtonAndLabel();
		checkDeleteBindDNButton();
		checkDeleteReferralButton();
	}

	/* Disable/Enable the elements of a container.  It is used to disable the elements of a panel 
	 * 
	 * @param  panel. The panel we want to disable/enable
	 * @param state boolean indicating wether we want to enable or disable the elements of the panel
	 */

	protected void setEnabledPanel(JPanel panel, boolean state) {
		Component[] comps = panel.getComponents();
		if (comps != null) {
			for (int i=0; i<comps.length; i++) {
				if (comps[i] instanceof JPanel) {
					setEnabledPanel((JPanel)comps[i], state);
				} else {
					comps[i].setEnabled(state);
				}
			}
		}
	}

	/**
	  * Method used to enable/disable the delete bind dn button.
	  * The button is enabled if there is something selected in the list of bind DN
	  */
	protected void checkDeleteBindDNButton() {
		_bDeleteBindDN.setEnabled(!_bindDNList.isSelectionEmpty());
	}

	/**
	  * Method used to enable/disable the delete bind dn button.
	  * The button is enabled if there is something selected in the list of bind DN
	  */
	protected void checkDeleteReferralButton() {
		_bDeleteReferral.setEnabled(!_referralList.isSelectionEmpty());
	}

	/**
	  * Method used to enable/disable the add bind dn button.
	  * The button is enabled if there is something to add AND we can edit the referral 
	  * (the replica is enabled, and we are not editing a simple master replica) AND
	  * is a valid DN.  If it is not a valid DN, we use the colour code
	  */
	protected void checkAddBindDNButtonAndLabel() {
		String dn = _tfNewBindDN.getText().trim();
		boolean isValidDN = DSUtil.isValidDN(dn);
			
		boolean enable = isValidDN &&
			!dn.equals("") &&
			!_rbIsSimpleMaster.isSelected() &&
			_cbReplicaEnabled.isSelected();
				
		/* Update the state of the add button */
		_bAddBindDN.setEnabled(enable);
	}

	/**
	  * Method used to enable/disable the add referral button.
	  * The button is enabled if there is something to add AND we can edit the referral 
	  * (the replica is enabled, and we are not editing a simple master replica).
	  */
	protected void checkAddReferralButton() {
		_bAddReferral.setEnabled(!_tfNewReferral.getText().trim().equals("") &&
								 !_rbIsSimpleMaster.isSelected() &&
								 _cbReplicaEnabled.isSelected());
	}

	protected void checkFields() {
		if (!_isInitialized) {
			return;
		}
		/* Check the enable/disable of the replica */
		boolean isReplicaEnabledDirty = _cbReplicaEnabled.isSelected() != _saveReplicaEnabled;
		
		/* If the replica is disabled we stop the check */
		if (!_cbReplicaEnabled.isSelected()) {		
			if (isReplicaEnabledDirty) {
				setChangeState( _cbReplicaEnabled, CHANGE_STATE_MODIFIED );
				setDirtyFlag();
			} else {
				setChangeState( _cbReplicaEnabled, CHANGE_STATE_UNMODIFIED);
				clearDirtyFlag();
			}
			setValidFlag();
			return;
		}
                
                
		/* Check the elements of the replica type panel */
		boolean isReplicaTypeDirty = false;
		if (_rbIsSlave.isSelected() != _saveIsSlave) {
			isReplicaTypeDirty = true;
			setChangeState( _rbIsSlave, CHANGE_STATE_MODIFIED );
		} else {
			setChangeState( _rbIsSlave, CHANGE_STATE_UNMODIFIED);
		}
		if (_rbIsSimpleMaster.isSelected() != _saveIsSimpleMaster) {
			isReplicaTypeDirty = true;
			setChangeState( _rbIsSimpleMaster, CHANGE_STATE_MODIFIED );
		} else {
			setChangeState( _rbIsSimpleMaster, CHANGE_STATE_UNMODIFIED);
		}
		if (_rbIsMultiMaster.isSelected() != _saveIsMultiMaster) {
			isReplicaTypeDirty = true;
			setChangeState( _rbIsMultiMaster, CHANGE_STATE_MODIFIED );
		} else {
			setChangeState( _rbIsMultiMaster, CHANGE_STATE_UNMODIFIED);
		}
		if (_rbIsHub.isSelected() != _saveIsHub) {
			isReplicaTypeDirty = true;
			setChangeState( _rbIsHub, CHANGE_STATE_MODIFIED );
		} else {
			setChangeState( _rbIsHub, CHANGE_STATE_UNMODIFIED);
		}
		

		/* Check the replica ID field, only if we are defining a master */
		boolean isReplicaIDDirty = false;
		boolean isReplicaIDValid = true;

		if (_rbIsSimpleMaster.isSelected() ||
			_rbIsMultiMaster.isSelected()) {
			String sValue = _tfReplicaID.getText();
			try {
				int value = Integer.parseInt(sValue);
				if ((value <= 0) ||
					(value >= 65535)) {
					isReplicaIDValid = false;
				} else {
					if (value != _saveReplicaID) {
						isReplicaIDDirty = true;
					}
				}
			} catch (Exception e) {
				isReplicaIDValid = false;
			}				
		}

		/* Check the purge delay */
		boolean isPurgeDelayDirty = false;
		boolean isPurgeDelayValid = true;
							
		if (!_cbPurgeDelayNever.isSelected()) {
			try {
				String sValue = _tfPurgeDelay.getText();
				long value = Integer.valueOf(sValue).longValue();
				int index = _cboPurgeUnit.getSelectedIndex();
				value *= CONVERSION_VALUES[index];
				if (value <= 0) {
					isPurgeDelayValid = false;
				} else {
					sValue = String.valueOf(value);
					if (!sValue.trim().equals(_savePurgeDelay)) {
						isPurgeDelayDirty = true;
					}
				}
			} catch (Exception e) {
				isPurgeDelayValid = false;
			}
			/* The value corresponding to check the checkbox of instantaneous
			   purging is '0' */
		} else {
			if (!_savePurgeDelay.equals("0")) {
				isPurgeDelayDirty = true;
			}
		}
		/* Check the supplier DN field */
		boolean isBindDNListDirty = false;
		boolean isBindDNListValid = true;
		/* If the it is an simple master we don't continue the check (is not pertinent to check
		   the bind dn as it is not used with this option).
		   We just check that the user added DNs (we assume that we can only add VALID dns) */		
		if (!_rbIsSimpleMaster.isSelected()) {
			isBindDNListValid =  _bindDNListData.getSize() > 0;				
			isBindDNListDirty = isBindDNListDirty();
		}

		/* Check the referral list.  We don't check the validity of the LDAP URLs in the list. 
		 We only check the validity of the URL when the user tries to add using the 'Add' button*/
		boolean isReferralListDirty = false;		
		/* If the it is an simple master we don't continue the check (is not pertinent to check
		   the referrals as they are not used with this option) */	
		if (!_rbIsSimpleMaster.isSelected()) {						
			isReferralListDirty = isReferralListDirty();
		}

		if (isReplicaEnabledDirty) {
			setChangeState( _cbReplicaEnabled, CHANGE_STATE_MODIFIED );			
		} else {
			setChangeState( _cbReplicaEnabled, CHANGE_STATE_UNMODIFIED);	
		}

		if (!isReplicaIDValid) {
			setChangeState( _lReplicaID, CHANGE_STATE_ERROR );
		} else if (isReplicaIDDirty) {
			setChangeState( _lReplicaID, CHANGE_STATE_MODIFIED );
		} else {
			setChangeState(_lReplicaID, CHANGE_STATE_UNMODIFIED);
		}

		if (!isBindDNListValid) {
			setChangeState( _lBindDNList, CHANGE_STATE_ERROR );
		} else if (isBindDNListDirty) {
			setChangeState( _lBindDNList, CHANGE_STATE_MODIFIED );			
		} else {
			setChangeState(_lBindDNList, CHANGE_STATE_UNMODIFIED);
		}
		if (!isPurgeDelayValid) {
			setChangeState(_lPurgeDelay, CHANGE_STATE_ERROR );
			setChangeState(_cbPurgeDelayNever, CHANGE_STATE_ERROR );
		} else if (isPurgeDelayDirty) {			
			if (_savePurgeDelay.equals("0") ||
				_cbPurgeDelayNever.isSelected()) {
				/* In this case we have modified the state of the _cbPurgeDelayNever checkbox */
				setChangeState(_cbPurgeDelayNever, CHANGE_STATE_MODIFIED );
			} else {
				setChangeState(_cbPurgeDelayNever, CHANGE_STATE_UNMODIFIED);
			}
			setChangeState(_lPurgeDelay, CHANGE_STATE_MODIFIED );
			
		} else {
			setChangeState(_lPurgeDelay, CHANGE_STATE_UNMODIFIED);
			setChangeState(_cbPurgeDelayNever, CHANGE_STATE_UNMODIFIED);
		}

		if (isReferralListDirty) {
			setChangeState(_lReferralList, CHANGE_STATE_MODIFIED );
		} else {
			setChangeState(_lReferralList, CHANGE_STATE_UNMODIFIED);
		}

		if (isReplicaIDDirty ||			
			isBindDNListDirty ||
			isPurgeDelayDirty ||
			isReferralListDirty ||
			isReplicaTypeDirty ||
			isReplicaEnabledDirty) {
			setDirtyFlag();
		} else {
			clearDirtyFlag();
		}
		
		if (isReplicaIDValid && 
			isBindDNListValid &&
			isPurgeDelayValid) {
			setValidFlag();
		} else {
			clearValidFlag();
		}	
	}

	/**
	  * This method tells wether the user has modified or not the list of bind DNs
	  * for this replica.  It compares the size of what is in the server and what we have in the
	  * list, and after that, it compares the content.
	  */
	private boolean isBindDNListDirty() {		
		boolean isDirty = false;
		if (_bindDNList != null) {			
			isDirty = _bindDNListData.getSize() != _saveBindDNList.size();
			if (!isDirty) {
				Enumeration e = _bindDNListData.elements();
				while (e.hasMoreElements() &&
					   !isDirty) {
					isDirty = !_saveBindDNList.contains(e.nextElement());
				}		
			}
		}
		return isDirty;
	}

	/**
	  * This method tells wether the user has modified or not the list of additional referrals
	  * for this replica.  It compares the size of what is in the server and what we have in the
	  * list, and after that, it compares the content.
	  */
	private boolean isReferralListDirty() {		
		boolean isDirty = false;
		if (_referralList != null) {			
			isDirty = _referralListData.getSize() != _saveReferralList.size();
			if (!isDirty) {
				Enumeration e = _referralListData.elements();
				while (e.hasMoreElements() &&
					   !isDirty) {
					isDirty = !_saveReferralList.contains(e.nextElement());
				}		
			}
		}
		return isDirty;
	}


    class BindDNCellRenderer extends DefaultListCellRenderer {  
		// This is the only method defined by ListCellRenderer.  We just
		// reconfigure the Jlabel each time we're called.
		
		public Component getListCellRendererComponent(
													  JList list,
													  Object value,            // value to display
													  int index,               // cell index
													  boolean isSelected,      // is the cell selected
													  boolean cellHasFocus)    // the list and the cell have the focus
			{
				checkDeleteBindDNButton();
				return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			}
    }
	
    class ReferralCellRenderer extends DefaultListCellRenderer {  
		// This is the only method defined by ListCellRenderer.  We just
		// reconfigure the Jlabel each time we're called.
		
		public Component getListCellRendererComponent(
													  JList list,
													  Object value,            // value to display
													  int index,               // cell index
													  boolean isSelected,      // is the cell selected
													  boolean cellHasFocus)    // the list and the cell have the focus
			{
				checkDeleteReferralButton();				
				return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			}
    }



    /*==========================================================
     * variables
     *==========================================================*/	
	private JButton _bAddBindDN = null;
	private JButton _bDeleteBindDN = null;

	private JButton _bConstructReferral = null;
	private JButton _bAddReferral = null;
	private JButton _bDeleteReferral = null;

	private JCheckBox _cbReplicaEnabled = null;
	private JCheckBox _cbPurgeDelayNever = null;

	private JRadioButton _rbIsSimpleMaster = null;
	private JRadioButton _rbIsSlave = null;
	private JRadioButton _rbIsHub = null;
	private JRadioButton _rbIsMultiMaster = null;

	private GroupPanel _replicaTypePanel = null;
	private GroupPanel _commonAttributePanel = null;
	private GroupPanel _consumerPanel = null;

    private JTextField _tfReplicaID = null;
	private JTextField _tfPurgeDelay = null;
	private JTextField _tfNewBindDN = null;
	private JTextField _tfNewReferral = null;

	private JComboBox _cboPurgeUnit = null;
    
	private JList _bindDNList = null;
    private JList _referralList = null;	
	
	private DefaultListModel _bindDNListData = null;
	private DefaultListModel _referralListData = null;	
	
	/* This booleans members, tell if the attributes exist on the server or not */
	private boolean _saveDS5ReplicaTypeExist = false;
	private boolean _saveReplicaIDExist = false;
    private boolean _saveBindDNExist = false;
    private boolean _saveReferralListExist = false;
    private boolean _savePurgeDelayExist = false;
	private boolean _saveDS5FlagsExist = false;

    private JLabel _lBindDNList = null;
	private JLabel _lReferralList = null; 
	private JLabel _lPurgeDelay = null;
	private JLabel _lReplicaID = null;

	private JTextArea _lReplicaIDWarning = null;
	/* These booleans members, tell which are the values in the server (or the default values
	   if there are no values on the server) */
	private boolean _saveIsSimpleMaster = false;
	private boolean _saveIsSlave = false;
	private boolean _saveIsMultiMaster = false;
	private boolean _saveIsHub = false;
	private boolean _saveReadOnly = false;
	private boolean _saveReplicaEnabled = false;
	private boolean _saveWindowsSyncEnabled = false;
	private boolean _saveDS5Flags = false;

	/* These members, tell which are the values in the server (or the default values
	   if there are no values on the server) */
	private int _saveReplicaID = 0;	
	private String _savePurgeDelay = "";
	private Vector _saveBindDNList = new Vector();
	private Vector _saveReferralList = new Vector();  /* This vector contains the list of the additional
														 referrals defined on the replica */

    private IDSModel _model;

	private IDSContentListener _idscListener;
	private LDAPEntry _instEntry;
	private LDAPEntry _mapTreeEntry;
	private String _repSettingDN;
        private String _syncSettingDN;

    private static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
    private static final String _section = "replication-replica";
    private static final String REP_TYPE_READONLY = "2";
    private static final String REP_TYPE_UPDATEABLE = "3";
    private static final String REP_FLAG_NONE = "0";
    private static final String REP_FLAG_LOG_CHANGES = "1";
    final static int DEFAULT_PADDING = 6;

	final static int REFERRAL_LIST_ROWS = 3;
	final static int BIND_DN_LIST_ROWS = 3;

	int[] CONVERSION_VALUES = {86400, 3600, 60, 1};
}
