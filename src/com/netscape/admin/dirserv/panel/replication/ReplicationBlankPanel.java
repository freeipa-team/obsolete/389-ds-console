/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import javax.swing.event.*;
import javax.swing.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.util.*;
import java.util.*;
import java.awt.*;

/**
 * This blank panel extends the BlankPanel which provides extra feature,
 * highlighting the label for the modified textfield.
 */
class ReplicationBlankPanel extends BlankPanel {

    public ReplicationBlankPanel(IAgreementPanel parent, int index) {
        super(parent.getModel());
        _parent      = parent;
        _tabbedPanel = parent.getTabbedPane();
        _agreement   = (ReplicationAgreement)parent.getAgreement();
        _model       = parent.getModel();
        _index       = index;
    }

    //======== BlankPanel callback functions =======
	public void okCallback() {
	    Debug.println("okCallback()");
	   	
        _initialized = false;
		resetAll ();
        _initialized = true;
	}

	public void resetCallback() {

        Debug.println("resetCallback()");

        _initialized = false;
		
	    populateData();
        showAll ();
	    
	    super.resetCallback ();

        _initialized = true;
	}

    /* should be overwritten in the derived class that
       to populate panel's data                        */

    protected void populateData () {
    }

    protected void setDirtyFlag() {
        super.setDirtyFlag();
        if ((_tabbedPanel != null) &&
			(_tabbedPanel.getIconAt(_index )== null) ) {
            _tabbedPanel.setIconAt(_index,
                        ReplicationTool.getImage(_dirtyIcon));
            _tabbedPanel.repaint();
        }
    }

    protected void clearDirtyFlag() {
        super.clearDirtyFlag();

       if ((_tabbedPanel != null) &&
			(_tabbedPanel.getIconAt(_index )!= null) ){
            _tabbedPanel.setIconAt(_index, null);
            _tabbedPanel.repaint();
        } 
    }

    protected void initialize () {
        populateData ();
        showAll ();
        _initialized = true;
    }

    boolean initialized () {
        return _initialized;
    }

    protected ReplicationAgreement _agreement   = null;
    protected Color                _activeColor = null;
    protected IDSModel             _model       = null;
    protected IAgreementPanel      _parent      = null;

    private JTabbedPane          _tabbedPanel = null;
    private int                  _index       = -1;
    private boolean              _initialized = false;

    private static final String _dirtyIcon = "red-ball-small.gif";                             

    //get resource bundle
    public static ResourceSet _resource =
       new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");

 }

