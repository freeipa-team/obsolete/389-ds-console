/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;

/**
 * Schedule Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class AgreementSchedulePanel extends ReplicationBlankPanel
    implements ActionListener, ITabPanel {
    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    public AgreementSchedulePanel(IAgreementPanel parent, int index) {
        super(parent, index);
        setTitle(_resource.getString("replication-schedule-tab","label"));
        if(_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD)
        {
        	_helpToken = "configuration-sync-schedule-help";
        } else {
        	_helpToken = "configuration-replication-schedule-help";
        }
  		_refreshWhenSelect = false;
    }
	/*==========================================================
	 * public methods
     *==========================================================*/

    /**
	 * Actual panel construction
	 */
    public void init() {
		if (_isInitialized) {
			return;
		}
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
		_myPanel.setLayout(new GridBagLayout());
        _myPanel.setBackground(getBackground());

        //in sync button
        ButtonGroup timeGroup = new ButtonGroup();
        _inSync = makeJRadioButton(
			_resource.getString("replication-schedule-inSyncButton","label"));
        timeGroup.add(_inSync);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx=1.0;
        gbc.insets = new Insets(0,10,5,10);        
        _myPanel.add(_inSync, gbc);

        //sync on date        
		String title = _resource.getString(
										   "replication-schedule-days","label");
        syncPanel = new GroupPanel(title);        
        
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.weighty=1.0;        
        _myPanel.add(syncPanel, gbc);

        //every radio
        _syncOn = makeJRadioButton(
			_resource.getString("replication-schedule-syncOnButton","label"));
        timeGroup.add(_syncOn);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx=1.0;        
        syncPanel.add(_syncOn, gbc);

		gbc.insets = new Insets(0, 0, 0, 0);

		JPanel dayButtonsPanel = new JPanel(new GridBagLayout());		
        syncPanel.add(dayButtonsPanel, gbc);

		JPanel timePanel = new JPanel(new GridBagLayout());		
        syncPanel.add(timePanel, gbc);				

        //day buttons		

        makeDayButton(dayButtonsPanel, 1,
					  _resource.getString("replication-schedule-dateMon",
										  "label"));
        makeDayButton(dayButtonsPanel, 2,
					  _resource.getString("replication-schedule-dateTue",
										  "label"));
        makeDayButton(dayButtonsPanel, 3,
					  _resource.getString("replication-schedule-dateWed",
										  "label"));
        makeDayButton(dayButtonsPanel, 4,
					  _resource.getString("replication-schedule-dateThu",
										  "label"));
        makeDayButton(dayButtonsPanel, 5,
					  _resource.getString("replication-schedule-dateFri",
										  "label"));
        makeDayButton(dayButtonsPanel, 6,
					  _resource.getString("replication-schedule-dateSat",
										  "label"));
        makeDayButton(dayButtonsPanel, 0,
					  _resource.getString("replication-schedule-dateSun",
										  "label"));

		gbc.weightx = 1.0;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.fill = gbc.HORIZONTAL;		
		dayButtonsPanel.add(Box.createHorizontalGlue(), gbc);

        _allButton = UIFactory.makeJButton(this, "replication-schedule", "allButton", _resource);		
        ReplicationTool.resetGBC(gbc);
		gbc.insets.right = gbc.insets.left;
		gbc.fill = gbc.NONE;
        gbc.anchor = gbc.EAST;
        gbc.gridwidth = gbc.REMAINDER;
                
        dayButtonsPanel.add(_allButton, gbc);

        //between
        _lBetween = makeJLabel(
			_resource.getString("replication-schedule-between","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = 3;
        gbc.fill = gbc.NONE;        
        timePanel.add(_lBetween, gbc);

        //add time selector
        // changed the size of time fields from 2 to 3 since
        // you can't see the whole thing with size 2
        _startHr = makeJTextField("00",3);
		_lBetween.setLabelFor(_startHr);
        _activeColor = _startHr.getBackground();
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;        
        timePanel.add(_startHr, gbc);

        _startMin = makeJTextField("00",3);
		_lBetween.setLabelFor(_startMin);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;        
        timePanel.add(_startMin, gbc);

        _lAnd = makeJLabel(
			_resource.getString("replication-schedule-and","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;        
        timePanel.add(_lAnd, gbc);

        _endHr = makeJTextField("00",3);
		_lAnd.setLabelFor(_endHr);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;        
        timePanel.add(_endHr, gbc);

        _endMin = makeJTextField("00",3);
		_lAnd.setLabelFor(_endMin);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;        
        timePanel.add(_endMin, gbc);

		gbc.weightx = 1.0;
		gbc.gridwidth = gbc.RELATIVE;
		gbc.fill = gbc.HORIZONTAL;			
		timePanel.add(Box.createHorizontalGlue(), gbc);
        
		_isInitialized = true;
    }


    //========= ACTIONLISTENER =================
    public void actionPerformed(ActionEvent e) {
        Debug.println("AgreementSchedulePanel: actionPerformed()"+
					  e.toString());
 
        if (e.getSource().equals(_allButton)) {
            checkDate(true);
        }
        if (e.getSource().equals(_inSync)) {
            disableTimeSelection();
        }
        if (e.getSource().equals(_syncOn)) {
            enableTimeSelection();
        }
	
		validateButtons();
    }

	//========= DOCUMENTLISTENER =================
	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void changedUpdate(DocumentEvent e) {
		insertUpdate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void removeUpdate(DocumentEvent e) {		
		insertUpdate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void insertUpdate(DocumentEvent e) {
		validateButtons();
    }


    //===== ITabPanel interface functions =========
    //specify entry verification rules here
    public boolean validateEntries() {
        /* We don't need to do anyhting 
           here                                        */
        return true;
    }

    public void getUpdateInfo( Object inf ) {
		AgreementWizardInfo info = (AgreementWizardInfo)inf;
        Vector out = new Vector();
        out.addElement(createDateString());
        info.setDate(out);
    }

    /*==========================================================
	 * private methods
	 *==========================================================*/

	private void makeDayButton(JPanel panel, int index, String label) {
        _dayButton[index] = makeJCheckBox(label);
        _dayButton[index].setVerticalTextPosition(JCheckBox.TOP);
        GridBagConstraints gbc = new GridBagConstraints();
        ReplicationTool.resetGBC(gbc);        
        panel.add(_dayButton[index], gbc);
	}

    private void disableTimeSelection() {
        for (int i=0; i<7; i++) {
            _dayButton[i].setEnabled(false);
        }
        _allButton.setEnabled(false);
        _startHr.setEnabled(false);
        _startHr.setEditable(false);
        _startHr.setBackground(getBackground());
        _startMin.setEnabled(false);
        _startMin.setEditable(false);
        _startMin.setBackground(getBackground());
        _endHr.setEnabled(false);
        _endHr.setEditable(false);
        _endHr.setBackground(getBackground());
        _endMin.setEnabled(false);
        _endMin.setEditable(false);
        _endMin.setBackground(getBackground());
		_lBetween.setEnabled(false);
		_lAnd.setEnabled(false);
        syncPanel.invalidate();
        syncPanel.validate();
        syncPanel.repaint(1);
    }

    private void enableTimeSelection() {
        for (int i=0; i<7; i++) {
            _dayButton[i].setEnabled(true);
        }
        _allButton.setEnabled(true);
        _startHr.setEnabled(true);
        _startHr.setEditable(true);
        _startHr.setBackground(_activeColor);
        _startMin.setEnabled(true);
        _startMin.setEditable(true);
        _startMin.setBackground(_activeColor);
        _endHr.setEnabled(true);
        _endHr.setEditable(true);
        _endHr.setBackground(_activeColor);
        _endMin.setEnabled(true);
        _endMin.setEditable(true);
        _endMin.setBackground(_activeColor);
		_lBetween.setEnabled(true);
		_lAnd.setEnabled(true);
    }

    private void checkDate(boolean check) {
        for (int i=0; i<7; i++) {
            _dayButton[i].setSelected(check);
        }
    }

   /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
    public boolean refresh() {		
        resetCallback();

	    return true;
    }

	public void resetCallback() {
		populateData();		
	}

		/* This is called AFTER the modifications in the server are done (in AgreementPanel).
		   Just read the content from the server */
	public void okCallback() {
		resetCallback();
	}

	/**
	 * Populate the UI with data
	 */
	protected void populateData() {
		_agreement.updateAgreementFromServer();
        Vector timeVec = _agreement.getUpdateSchedule();
        String time = "";
        if ((timeVec != null) && (timeVec.size()>0) ) {
            time = (String)timeVec.firstElement();
        }		
        setSchedule(time);
		validateButtons();
    }

	private void setSchedule(String s) {
		if (!_isInitialized) {
			return;
		}
	    Debug.println("AgreementSchedulePanel.setSchedule(): Schedule: "+s);
		StringBuffer startHr = new StringBuffer();
		StringBuffer startMin = new StringBuffer();
		StringBuffer endHr = new StringBuffer();
		StringBuffer endMin = new StringBuffer();
		StringBuffer dow = new StringBuffer();
	    if (!ReplicationTool.parseReplicaSchedule(s, startHr, startMin,
												  endHr, endMin, dow)) {
			_inSync.setSelected(true);

            disableTimeSelection();
			_saveInSync = true;			
        } else {			
            enableTimeSelection();
			_syncOn.setSelected(true);
			_saveInSync = false;

			/* Look for the hours and mins of the sync */
			_startHr.setText(startHr.toString());
			_saveStartHr = startHr.toString();

			_startMin.setText(startMin.toString());
			_saveStartMin = startMin.toString();

			_endHr.setText(endHr.toString());
			_saveEndHr = endHr.toString();

			_endMin.setText(endMin.toString());
			_saveEndMin = endMin.toString();

			/* Look for the days where we do sync */
			for (int i=0; i<_saveDay.length; i++) {
				_saveDay[i] = false;
				_dayButton[i].setSelected(false);
			}
			for (int i=0; i<dow.length(); i++) {
				String sDayOfWeek = dow.substring(i, i+1);
				try {
					int dayOfWeek = Integer.parseInt(sDayOfWeek);
					if ((dayOfWeek >= 0) &&
						(dayOfWeek < 7)) {
						_saveDay[dayOfWeek] = true;
						_dayButton[dayOfWeek].setSelected(true);
					}
				} catch (Exception e) {
					Debug.println("AgreementSchedulePanel.setSchedule error parsing "+sDayOfWeek);
				}				
			}			
        }		
    }

		/**
		 * This methods checks if there have been modifications and makes the coloring of the labels.
		 */
	protected void validateButtons() {
		if (!_isInitialized) {
			return;
		}
		boolean isInSyncDirty = false;
		if (_inSync.isSelected()) {
			/* Don't need to check further in this case */
			if (!_saveInSync) {
				setChangeState(_inSync, CHANGE_STATE_MODIFIED );
				setChangeState(_syncOn, CHANGE_STATE_MODIFIED );
				setDirtyFlag();
			} else {
				setChangeState(_inSync, CHANGE_STATE_UNMODIFIED);
				setChangeState(_syncOn, CHANGE_STATE_UNMODIFIED);
				clearDirtyFlag();
			}			
			setValidFlag();
			return;
		} else if (_saveInSync) {
			isInSyncDirty = true;
		}

		if (isInSyncDirty) {
			setChangeState(_inSync, CHANGE_STATE_MODIFIED );
			setChangeState(_syncOn, CHANGE_STATE_MODIFIED );
		} else {
			setChangeState(_inSync, CHANGE_STATE_UNMODIFIED);
			setChangeState(_syncOn, CHANGE_STATE_UNMODIFIED);
		}
		
		boolean isDaySelectionDirty = false;
		boolean isDaySelectionValid = false;

		for (int i=0; i<_dayButton.length; i++) {			
			boolean isDayDirty = (_dayButton[i].isSelected() != _saveDay[i]);
			if (isDayDirty) {
				isDaySelectionDirty = true;
				setChangeState(_dayButton[i], CHANGE_STATE_MODIFIED );
			} else {
				setChangeState(_dayButton[i], CHANGE_STATE_UNMODIFIED);
			}
			if (_dayButton[i].isSelected()) {
				isDaySelectionValid = true;
			}			
		}
		
		if (!isDaySelectionValid) {
			for (int i=0; i<_dayButton.length; i++) {
				setChangeState(_dayButton[i], CHANGE_STATE_ERROR);
			}
		}
		
		boolean isStartEndTimeValid = true;
		boolean isStartEndTimeDirty = true;

		int saveStartHour = -1;
		int startHour = -1;
		int saveStartMin = -1;
		int startMin = -1;
		int saveEndHour = -1;
		int endHour = -1;
		int saveEndMin = -1;
		int endMin = -1;


		try {
			saveStartHour = Integer.parseInt(_saveStartHr);
		} catch (Exception e) {
		}
		try {
			startHour = Integer.parseInt(_startHr.getText());
		} catch (Exception e) {
		}
		try {
			saveStartMin = Integer.parseInt(_saveStartMin);
		} catch (Exception e) {
		}
		try {
			startMin = Integer.parseInt(_startMin.getText());
		} catch (Exception e) {
		}
		try {
			saveEndHour = Integer.parseInt(_saveEndHr);
		} catch (Exception e) {
		}
		try {
			endHour = Integer.parseInt(_endHr.getText());
		} catch (Exception e) {
		}
		try {
			saveEndMin = Integer.parseInt(_saveEndMin);
		} catch (Exception e) {
		}
		try {
			endMin = Integer.parseInt(_endMin.getText());
		} catch (Exception e) {
		}

		if ((startMin < 0) || (startMin >= 60) ||
			(endMin < 0) || (endMin >= 60) ||
			(startHour < 0) || (startHour >= 24) ||
			(endHour < 0) || (endHour >= 24) ||
			(startHour > endHour) ||
			((startHour == endHour) && (startMin >= endMin))) {
			isStartEndTimeValid = false;
		}

		if ((startMin == saveStartMin) &&
			(startHour == saveStartHour) &&
			(endMin == saveEndMin) &&
			(endHour == saveEndHour)) {
			isStartEndTimeDirty = false;
		}

		if (!isStartEndTimeValid) {
			setChangeState(_lAnd, CHANGE_STATE_ERROR);
			setChangeState(_lBetween, CHANGE_STATE_ERROR);			
		} else if (isStartEndTimeDirty) {
			setChangeState(_lAnd, CHANGE_STATE_MODIFIED);
			setChangeState(_lBetween, CHANGE_STATE_MODIFIED);			
		} else {
			setChangeState(_lAnd, CHANGE_STATE_UNMODIFIED);
			setChangeState(_lBetween, CHANGE_STATE_UNMODIFIED);			
		}

		if (isInSyncDirty || isDaySelectionDirty || isStartEndTimeDirty) {
			setDirtyFlag();
		} else {
			clearDirtyFlag();
		}

		if (isDaySelectionValid && isStartEndTimeValid) {
			setValidFlag();
		} else {
			clearValidFlag();
		}

	}

    private String createDateString() {
        if (_inSync.isSelected()) {
            return "";
        } else {
			return ReplicationTool.createDateString(_startHr, _startMin,
													_endHr, _endMin,
													_dayButton);
		}
	}

    /*==========================================================
     * variables
     *==========================================================*/

		/* Variables used to check if there have been modifications or not */
		
		/* NOTE: this array has to be the same lenght as _dayButton array */
	private boolean[] _saveDay = new boolean[7];

	private boolean _saveInSync = false;
	private String _saveStartHr;
	private String _saveStartMin;
	private String _saveEndHr;
	private String _saveEndMin;		    

	private JRadioButton _inSync, _syncOn;
	private JButton _allButton;
	private JCheckBox _dayButton[] = new JCheckBox[7];
    private JTextField _startHr, _startMin, _endHr, _endMin;
    private JPanel syncPanel;

	protected JLabel _lBetween;
	protected JLabel _lAnd;

	private static final int LIMIT_MAX_VAL = Integer.MAX_VALUE;
	private static final int MAX_HOUR = 23;
	private static final int MAX_MIN  = 59;
	private static final int MIN_TIME = 0;

}
