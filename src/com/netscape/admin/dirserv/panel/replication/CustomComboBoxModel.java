/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

/**
 * Custom Combo Box Model
 * Let you specify an icon and title to be displayed.
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/09/97
 * @see     com.netscape.admin.dirserv.panel.replication
 * @see     CustomComboBox
 */
class CustomComboBoxModel extends AbstractListModel implements ComboBoxModel {
    
    /*==========================================================
     * constructors
     *==========================================================*/    
    public CustomComboBoxModel() {
        _cache = new Vector();
        _index = new Vector();
        _icon = ReplicationTool.getImage("directory.gif");
    }

	/*==========================================================
	 * public methods
     *==========================================================*/
    /**
     * set selected item
     * DO NOT USE!!!
     * use JComboBox.setSelectedIndex()
     */
    public void setSelectedItem(Object anObject) {
        _currentValue = anObject;
        fireContentsChanged(this,-1,-1);
    }

    /**
     * Get selected Item.
     * DO NOT USE !!!
     * use JComboBox.getItemAt(JComboBox.getSelectedIndex())
     */
    public Object getSelectedItem() {
        return _currentValue;
    }

    /**
     * Return size
     * @return size
     */
    public int getSize() {
        return _cache.size();
    }

    /**
     * Retrieve element at index position
     * @param index location
     * @Object Hashtable obejct with "icon" and "title" field
     */
    public Object getElementAt(int index) {
        try {
            return _cache.elementAt(index);
        } catch(ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }
    
    /**
     * Replace default ds icon
     * @param icon new icon to be used
     */
    public void setIcon(ImageIcon icon) {
        _icon = icon;
    }

    /**
     * Add new ds list entry into the model
     * @param ds directory server instance object
     */
    public void addItem(ServerInstance ds) {
        Hashtable newItem = new Hashtable();
        newItem.put(SELECTION_ICON,_icon);
        newItem.put(SELECTION_TITLE, ds.toString());
        newItem.put(SELECTION_DATA,ds);
        _cache.addElement(newItem);
        _index.addElement(ds.toString().toUpperCase());
    }
    
    /**
     * Add new list entry into model
     * @param icon new icon associated
     * @param title text associated
     */
    public void addItem(ImageIcon icon, String title, Object data) {
        Hashtable newItem = new Hashtable();
        newItem.put(SELECTION_ICON,icon);
        newItem.put(SELECTION_TITLE, title);
        newItem.put(SELECTION_DATA, data);
        _cache.addElement(newItem);
        _index.addElement(title.toUpperCase());
    }    
    
    /**
     * Add new list entry into model
     * @param icon new icon associated
     * @param title text associated
     */
    public void addItem(ImageIcon icon, String title) {
        Hashtable newItem = new Hashtable();
        newItem.put(SELECTION_ICON,icon);
        newItem.put(SELECTION_TITLE, title);
        _cache.addElement(newItem);
        _index.addElement(title.toUpperCase());
    }
    
    /**
     * Add new list entry into model.
     * Default icon used
     * @param title text associated
     */
    public void addItem(String title) {
        Hashtable newItem = new Hashtable();
        newItem.put(SELECTION_TITLE, title);
        _cache.addElement(newItem);
        _index.addElement(title.toUpperCase());
    }
    
    /**
     * Remove all entries from the model
     */
    public void removeAll() {
        _cache.removeAllElements();
    }
    
    /**
     * Remove specific entry from the model
     * @param key key string associated with the entry
     */
    public void removeEntry(String key) {
        int x = _index.indexOf(key.toUpperCase());
        if ((x != -1) && (x < _cache.size()) ) {
            _cache.removeElementAt(x);
            _index.removeElementAt(x);
        }
    }

    public boolean isPresent (String key){
        int x = _index.indexOf(key.toUpperCase());

        return ((x != -1) && (x < _cache.size ()));
    }
    
    /*==========================================================
     * variables
     *==========================================================*/
     
    public static final String SELECTION_TITLE = "title";
    public static final String SELECTION_ICON = "icon";
    public static final String SELECTION_DATA = "data";
    
    private Object _currentValue;
    private Vector _cache;
    private Vector _index;
    private ImageIcon _icon;
}
