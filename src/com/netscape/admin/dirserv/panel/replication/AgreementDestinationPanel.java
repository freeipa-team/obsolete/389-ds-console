/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.RDN;

/**
 * Destination Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class AgreementDestinationPanel extends ReplicationBlankPanel
    implements ActionListener, ItemListener, ITabPanel, DocumentListener {

    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    public AgreementDestinationPanel(IAgreementPanel parent, int index) {
        super(parent, index);
        setTitle(_resource.getString("replication-destination-tab","label"));
        _monitor = (AgreementInfoPanel)parent.getInfoTab();
        _consoleInfo = _model.getConsoleInfo();
        _serverInfo = _model.getServerInfo();
        _frame = _model.getFrame();
        _dsInstances = new Hashtable();
        _icon = ReplicationTool.getImage("directory.gif");
        if(_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD)
        {
        	_helpToken = "configuration-sync-connection-help";
        } else {
        	_helpToken = "configuration-replication-content-help";
        }
		_refreshWhenSelect = false;
    }

    /*==========================================================
     * public methods
     *==========================================================*/

    /**
     * Actual panel construction
     */
    public void init() {		
		GridBagConstraints gbc = new GridBagConstraints();		
		_myPanel.setLayout(new GridBagLayout());
        _myPanel.setBackground(getBackground());    

        //connection radio buttons
        ButtonGroup connGroup = new ButtonGroup();
        //plain old ldap button
        _noEncrypt = makeJRadioButton(_resource.getString(
			"replication-destination-noEncrypt","label"));
        _noEncrypt.setToolTipText(_resource.getString(
			"replication-destination-noEncrypt","ttip"));
        connGroup.add(_noEncrypt);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
        _myPanel.add(_noEncrypt, gbc);

        //ssl check box
        _sslEncrypt = makeJRadioButton(_resource.getString(
							"replication-destination-sslEncrypt","label"));
        _sslEncrypt.setToolTipText(_resource.getString(
    			"replication-destination-sslEncrypt","ttip"));
        connGroup.add(_sslEncrypt);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx=1.0;        
        _myPanel.add(_sslEncrypt, gbc);

        //tls check box
        _tlsEncrypt = makeJRadioButton(_resource.getString(
							"replication-destination-startTLS","label"));
        _tlsEncrypt.setToolTipText(_resource.getString(
    			"replication-destination-startTLS","ttip"));
        connGroup.add(_tlsEncrypt);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;        
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx=1.0;        
        _myPanel.add(_tlsEncrypt, gbc);

        /* add DSEntry to correctly update field 
           coloring and buttons enabling/disabling */
        _ldapDSEntry = new DSEntryBoolean ("on", _noEncrypt);
        setComponentTable(_noEncrypt, _ldapDSEntry);        
        _sslDSEntry = new DSEntryBoolean ("off", _sslEncrypt);
        setComponentTable(_sslEncrypt, _sslDSEntry);        
        _tlsDSEntry = new DSEntryBoolean ("off", _tlsEncrypt);
        setComponentTable(_tlsEncrypt, _tlsDSEntry);        

        //auth using label
        JLabel auth = makeJLabel(_resource.getString(
						     "replication-destination-authUsing","label"));		
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;        
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx=1.0;        
        _myPanel.add(auth, gbc);
		
		// Auth mode panel
		JPanel authModePanel = new JPanel(new GridBagLayout());        
		auth.setLabelFor(authModePanel);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.insets = new Insets(0,0,0,0);        
        _myPanel.add(authModePanel, gbc);

        if(_agreement.getAgreementType() != ReplicationAgreement.AGREEMENT_TYPE_AD) {
        	//ssl auth radio button
        	ButtonGroup authGroup = new ButtonGroup();
        	_sslAuth = makeJRadioButton(_resource.getString(
        			"replication-destination-sslClientAuth","label"));
            _sslAuth.setToolTipText(_resource.getString(
        			"replication-destination-sslClientAuth","ttip"));
        	authGroup.add(_sslAuth);
        	ReplicationTool.resetGBC(gbc);
        	gbc.anchor = gbc.WEST;        
        	gbc.gridwidth = gbc.REMAINDER;
        	gbc.fill = gbc.NONE;
        	gbc.weightx=1.0;        
        	authModePanel.add(_sslAuth, gbc);

        	/* add DSEntry to correctly update field 
               coloring and buttons enabling/disabling */
        	_sslAuthDSEntry = new DSEntryBoolean ("off", _sslAuth);
        	setComponentTable(_sslAuth, _sslAuthDSEntry);        

            //gssapi auth radio button
            _gssapiAuth = makeJRadioButton(_resource.getString(
    			"replication-destination-gssapiAuth","label"));
            _gssapiAuth.setToolTipText(_resource.getString(
        			"replication-destination-gssapiAuth","ttip"));
            authGroup.add(_gssapiAuth);
            ReplicationTool.resetGBC(gbc);
            gbc.anchor = gbc.WEST;
            gbc.gridwidth = gbc.REMAINDER;
            gbc.fill = gbc.NONE;
            gbc.weightx = 1.0;
            authModePanel.add(_gssapiAuth, gbc);

            /* add DSEntry to correctly update field 
               coloring and buttons enabling/disabling */
            _gssapiAuthDSEntry = new DSEntryBoolean ("off", _gssapiAuth);
            setComponentTable(_gssapiAuth, _gssapiAuthDSEntry);        

            //digest auth radio button
            _digestAuth = makeJRadioButton(_resource.getString(
    			"replication-destination-digestAuth","label"));
            _digestAuth.setToolTipText(_resource.getString(
        			"replication-destination-digestAuth","ttip"));
            authGroup.add(_digestAuth);
            ReplicationTool.resetGBC(gbc);
            gbc.anchor = gbc.WEST;
            gbc.gridwidth = gbc.REMAINDER;
            gbc.fill = gbc.NONE;
            gbc.weightx = 1.0;
            authModePanel.add(_digestAuth, gbc);

            /* add DSEntry to correctly update field 
               coloring and buttons enabling/disabling */
            _digestAuthDSEntry = new DSEntryBoolean ("off", _digestAuth);
            setComponentTable(_digestAuth, _digestAuthDSEntry);        

            //simple auth radio button
            _simpAuth = makeJRadioButton(_resource.getString(
            		"replication-destination-simpleAuth","label"));
            authGroup.add(_simpAuth);
            ReplicationTool.resetGBC(gbc);
            gbc.insets.top = 0;
            gbc.anchor = gbc.WEST;        
            gbc.gridwidth = gbc.REMAINDER;
            gbc.fill = gbc.NONE;
            gbc.weightx=1.0;        
            authModePanel.add(_simpAuth, gbc);

            /* add DSEntry to correctly update field 
               coloring and buttons enabling/disabling */
            _simpAuthDSEntry = new DSEntryBoolean ("on", _simpAuth);
            setComponentTable(_simpAuth, _simpAuthDSEntry);  
        }
 
        //simp panel
        JPanel simpPanel = new JPanel(new GridBagLayout());        
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.insets = new Insets(0,0,0,0);        
        authModePanel.add(simpPanel, gbc);

        //bind as
        _bind = makeJLabel(_resource.getString(
					       "replication-destination-bindAs","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTHEAST;        
        gbc.fill = gbc.NONE;
		gbc.insets.left +=20;		
        simpPanel.add(_bind, gbc);

        _bindText = makeJTextField(10);
		_bind.setLabelFor(_bindText);
        ReplicationTool.resetGBC(gbc);
		gbc.insets.right = gbc.insets.left;
        gbc.anchor = gbc.NORTHWEST;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;  
	// tmp solution until server suport it
        simpPanel.add(_bindText, gbc);
	
        /* add DSEntry to correctly update field
           coloring and buttons enabling/disabling */
		_bindDSEntry = new bindDSEntry ("", _bindText, _bind);
		setComponentTable(_bindText, _bindDSEntry);
		
        //password
        _pwd = makeJLabel(_resource.getString(
											  "replication-destination-bindPwd","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTHEAST;
		gbc.insets.left +=20;
        gbc.fill = gbc.NONE;
        simpPanel.add(_pwd, gbc);

        _pwdText= makeJPasswordField(10);		
		_pwd.setLabelFor(_pwdText);
        ReplicationTool.resetGBC(gbc);
		gbc.insets.right = gbc.insets.left;
        gbc.anchor = gbc.NORTHWEST;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;
        simpPanel.add(_pwdText, gbc);
        
        /* add DSEntry to correctly update field
           coloring and buttons enabling/disabling */
		_pwdDSEntry = new pwdDSEntry ("", _pwdText, _pwd);
		setComponentTable(_pwdText, _pwdDSEntry);
        
        if(_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD){
            //new Windows user Sync
            _newWinUserSync = makeJLabel(_resource.getString(
            "replication-destination-new-user-sync","label"));
            _newWinUserSync.setToolTipText(_resource.getString(
            "replication-destination-new-user-sync","ttip"));
            ReplicationTool.resetGBC(gbc);
            gbc.anchor = gbc.NORTHEAST;
            gbc.insets.left +=20;
            gbc.fill = gbc.NONE;
            simpPanel.add(_newWinUserSync, gbc);
            
            _newWinUserSyncCB= makeJCheckBox();
            _newWinUserSync.setLabelFor(_newWinUserSyncCB);
            ReplicationTool.resetGBC(gbc);
            gbc.insets.right = gbc.insets.left;
            gbc.anchor = gbc.NORTHWEST;
            gbc.gridwidth = gbc.REMAINDER;
            gbc.weightx = 1.0;
            simpPanel.add(_newWinUserSyncCB, gbc);
            
            _newUserDSEntry = new DSEntryBoolean ("off", _newWinUserSyncCB);
		setComponentTable(_newWinUserSyncCB, _newUserDSEntry); 
/*            _newWinUserSyncCB.setEnabled(false); // Do not allow editting of this post-creation.
            _newWinUserSyncCB.repaint(1);
            _newWinUserSync.setEnabled(false);
            _newWinUserSync.repaint(1);*/
            
            /******** new Windows group Sync ***********/
            _newWinGroupSync = makeJLabel(_resource.getString(
            "replication-destination-new-group-sync","label"));
            _newWinGroupSync.setToolTipText(_resource.getString(
            "replication-destination-new-group-sync","ttip"));
            ReplicationTool.resetGBC(gbc);
            gbc.anchor = gbc.NORTHEAST;
            gbc.insets.left +=20;
            gbc.fill = gbc.NONE;
            simpPanel.add(_newWinGroupSync, gbc);
            
            _newWinGroupSyncCB= makeJCheckBox();
            _newWinGroupSync.setLabelFor(_newWinGroupSyncCB);
            ReplicationTool.resetGBC(gbc);
            gbc.insets.right = gbc.insets.left;
            gbc.anchor = gbc.NORTHWEST;
            gbc.gridwidth = gbc.REMAINDER;
            gbc.weightx = 1.0;
            simpPanel.add(_newWinGroupSyncCB, gbc);
            
            _newGroupDSEntry = new DSEntryBoolean ("off", _newWinGroupSyncCB);
            setComponentTable(_newWinGroupSyncCB, _newGroupDSEntry); 
/*            _newWinGroupSyncCB.setEnabled(false); // Do not allow editting of this post-creation.
            _newWinGroupSyncCB.repaint(1);
            _newWinGroupSync.setEnabled(false);
            _newWinGroupSync.repaint(1);*/
                
        }

        /* add DSEntry to correctly update field
           coloring and buttons enabling/disabling */
		  

		addBottomGlue();
        
        initialize ();		
		enableFields();
                
                
    }

    private void enableFields(){
    	boolean ssl = _sslEncrypt.isSelected() || _tlsEncrypt.isSelected();
    	if (_sslAuth != null) {
    		_sslAuth.setEnabled(ssl);
    	}
    	if (_gssapiAuth != null) {
    		_gssapiAuth.setEnabled(!ssl);
    	}
    	// cannot convert to or from LDAPS because there is no way to change
    	// the port number in this panel
    	if (_sslEncrypt.isSelected()) {
    	    _tlsEncrypt.setEnabled(false);
    	    _noEncrypt.setEnabled(false);
    	} else {
    	    _sslEncrypt.setEnabled(false);
    	}
    }


  /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
	public boolean refresh() {
            if(!refreshed){
                refreshed=true;
		resetCallback();
            }
            refreshed=false;
		return true;
	}

    //========= ACTIONLISTENER =================
    public void actionPerformed(ActionEvent e) {
    	_bindDSEntry.setSkipCheck(false);
    	_pwdDSEntry.setSkipCheck(false);
        if (e.getSource().equals(_sslAuth) && _sslAuth.isSelected()) {
    	    setDirtyFlag();
    	    //disable
        	enableSimpleAuth (false);
        }
        if (e.getSource().equals(_gssapiAuth) && _gssapiAuth.isSelected()) {
    	    setDirtyFlag();
        	// enable
        	enableSimpleAuth (true);
        	// requires ldap
        	_noEncrypt.setSelected(true);
        	/* set to use non-SSL port LDAP */
        	_portAttr = ATTR_PORT;
        	_bindDSEntry.setSkipCheck(true);
        	_pwdDSEntry.setSkipCheck(true);
        }
        if (e.getSource().equals(_simpAuth) && _simpAuth.isSelected()) {
    	    setDirtyFlag();
        	//enable
        	enableSimpleAuth (true);
        }
        if (e.getSource().equals(_digestAuth) && _digestAuth.isSelected()) {
    	    setDirtyFlag();
        	//enable
        	enableSimpleAuth (true);
        }

        if (e.getSource().equals(_noEncrypt) && _noEncrypt.isSelected()) {
    	    setDirtyFlag();
        	//disable
    	    if (_sslAuth != null) {
    	    	_sslAuth.setEnabled(false);
    	    }
        	//enable
        	if ((_sslAuth != null) && _sslAuth.isSelected() && (_simpAuth != null)) {
        		// have to select something else
            	_simpAuth.setSelected(true);
        	}
        	enableSimpleAuth(true);
        	if (_gssapiAuth != null) {
        		_gssapiAuth.setEnabled(true);
        	}
        	if (_digestAuth != null) {
        		_digestAuth.setEnabled(true);
        	}

        	/* set to use non-SSL port */
        	_portAttr = ATTR_PORT;
        }
        boolean ssl_selected = false;
        if (e.getSource().equals(_sslEncrypt) && _sslEncrypt.isSelected()) {
            /* set to use SSL port */
            _portAttr = ATTR_SSL_PORT;
            ssl_selected = true;
        }
        if (e.getSource().equals(_tlsEncrypt) && _tlsEncrypt.isSelected()) {
        	/* set to use non-SSL port for startTLS */
        	_portAttr = ATTR_PORT;
            ssl_selected = true;
        }
        if (ssl_selected) {
        	if (_sslAuth != null) {
        		_sslAuth.setEnabled(true);
        	}
        	if (_gssapiAuth != null) {
        		_gssapiAuth.setEnabled(false);
        		if (_gssapiAuth.isSelected()) {
        			// have to select something else
        			_simpAuth.setSelected(true);
        			enableSimpleAuth(true);
        		}
        	}
        }
        super.actionPerformed (e);
    }

    static void showErrorDialog( JFrame frame, String err,
				 String section ) {
	DSUtil.showErrorDialog( frame, err, "",
				section, _resource );
    }

    //===== ITabPanel interface functions =========

    //specify entry verification rules here
    public boolean validateEntries() {

        /* all other validation is done every time
           field is changed using DSEntry mechanism */
	// CopieFrom doesn't exist ...
        return true;
    }

    public void getUpdateInfo( Object inf ) {
		AgreementWizardInfo info = (AgreementWizardInfo)inf;

		info.setStartTLS(_tlsEncrypt.isSelected());
		info.setSSL(_sslEncrypt.isSelected());
	    info.setSSLAuth( (_sslAuth != null) && _sslAuth.isSelected() );
	    info.setGSSAPIAuth( (_gssapiAuth != null) && _gssapiAuth.isSelected() );
	    info.setDigestAuth( (_digestAuth != null) && _digestAuth.isSelected() );

        if (info.getSSLAuth()) {
            info.setBindDN("");
            info.setBindPWD("");
        } else {
            info.setBindDN(_bindText.getText());
            info.setBindPWD(_pwdText.getText());
        }
        if(_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD) {
        	if(_newWinUserSyncCB.isSelected()) {
        		info.setNewWinUserSync("on");
        	}else{
        		info.setNewWinUserSync("off");
        	}
        	if(_newWinGroupSyncCB.isSelected()) {
        		info.setNewWinGroupSync("on");
        	}else{
        		info.setNewWinGroupSync("off");
        	}
        }
    }

    /*==========================================================
     * private methods
     *==========================================================*/

    /**
     * Populate the UI with data
     */
    protected void populateData() {

        /* populate all fileds except to and from */
        if ( (_agreement.getBindCredentials() == null) || (_agreement.getBindDN() == null) ) {
            _agreement.updateAgreementFromServer();
        }
        copyData ();
    }
 
    /**
     * Copy Rest of data from agreement
     */
    private void copyData() {
        if ((_agreement.getBindCredentials()!= null) &&
	    (_agreement.getBindDN()!= null)){
            _pwdDSEntry.fakeInitModel (_agreement.getBindCredentials());
            _bindDSEntry.fakeInitModel (_agreement.getBindDN());
            _origPwd    = _agreement.getBindCredentials();
            _origBindDN = _agreement.getBindDN();
        }
        boolean ssl = _agreement.getUseSSL() || _agreement.getUseStartTLS();
        _ldapDSEntry.fakeInitModel(ssl ? "off" : "on");
        _noEncrypt.setSelected(!ssl);
        _sslEncrypt.setSelected(_agreement.getUseSSL());
        _sslDSEntry.fakeInitModel(_agreement.getUseSSL() ? "on" : "off");
        _tlsEncrypt.setSelected(_agreement.getUseStartTLS());
        _tlsDSEntry.fakeInitModel(_agreement.getUseStartTLS() ? "on" : "off");
        _portAttr = _agreement.getUseSSL() ? ATTR_SSL_PORT : ATTR_PORT;

        if (_sslAuth != null) {
        	_sslAuth.setEnabled(ssl);
        	_sslAuth.setSelected(_agreement.getUseSSLAuth());
        	_sslAuthDSEntry.fakeInitModel (_agreement.getUseSSLAuth() ? "on" : "off");
            enableSimpleAuth (!_agreement.getUseSSLAuth());
        }
        if (_gssapiAuth != null) {
        	_gssapiAuth.setEnabled(!ssl);
        	_gssapiAuth.setSelected(_agreement.getUseGSSAPIAuth());
        	_gssapiAuthDSEntry.fakeInitModel (_agreement.getUseGSSAPIAuth() ? "on" : "off");
        }
        if (_digestAuth != null) {
        	_digestAuth.setSelected(_agreement.getUseDigestAuth());
        	_digestAuthDSEntry.fakeInitModel (_agreement.getUseDigestAuth() ? "on" : "off");
        }
        boolean issimple = !_agreement.getUseSSLAuth() && !_agreement.getUseDigestAuth() && !_agreement.getUseGSSAPIAuth();
        if (_simpAuth != null) {
            _simpAuth.setEnabled(issimple);
            _simpAuth.setSelected(issimple);
            _simpAuthDSEntry.fakeInitModel (issimple ? "on" : "off");
        }

        if(_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD){
            if(((ActiveDirectoryAgreement)_agreement).getNewWinUserSync().compareTo("on") == 0){
                _newWinUserSyncCB.setSelected(true);
                _newUserDSEntry.fakeInitModel("on");
            }else{
                _newWinUserSyncCB.setSelected(false);
                _newUserDSEntry.fakeInitModel("off");
            }
            if(((ActiveDirectoryAgreement)_agreement).getNewWinGroupSync().compareTo("on") == 0){
                _newWinGroupSyncCB.setSelected(true);
                _newGroupDSEntry.fakeInitModel("on");
            }else{
                _newWinGroupSyncCB.setSelected(false);
                _newGroupDSEntry.fakeInitModel("off");
            }
        }
    }

    private void enableSimpleAuth (boolean enable){
        _bind.setEnabled(enable);
        _bind.repaint(1);
        _bindText.setEnabled(enable);
        _bindText.repaint(1);
        _pwd.setEnabled(enable);
        _pwd.repaint(1);    
        _pwdText.setEnabled(enable);
	if (enable)
	    _pwdText.setBackground(_bindText.getBackground());
	else
	    _pwdText.setBackground(getBackground());         
    }

	/**
	  * Method used to know wether the password has been modified or not.
	  *
	  *@return boolean telling if the password has been modified or not
	  */
	public boolean isPwdDirty() {
		return _pwdDSEntry.getDirty();
	}

    class bindDSEntry extends DSEntryTextStrict
    {
    	private boolean skipCheck = false;

        bindDSEntry(String model, JComponent view1, JComponent view2) {
            super (model, view1, view2);
        }

        public int validate (){
            JTextField tf = (JTextField) getView (0);
            String     dn = tf.getText ();

            /* disabled field is always valid */
            if (!tf.isEnabled ())
                return 0;
            
            if (skipCheck) {
            	return 0;
            }

            if (!dn.equals ("") && DSUtil.isValidDN (dn))
                return 0;
            else
                return 1;
        }
        
        public void setSkipCheck(boolean val) {
        	skipCheck = val;
        }
    }

    class pwdDSEntry extends DSEntryTextStrict
    {
    	private boolean skipCheck = false;

    	pwdDSEntry(String model, JComponent view1, JComponent view2) {
            super (model, view1, view2);
        }

        public int validate (){
            JTextField tf = (JTextField) getView (0);
            String     dn = tf.getText ();

            /* disabled field is always valid */
            if (!tf.isEnabled ())
                return 0;
            
            if (skipCheck) {
            	return 0;
            }
            return super.validate();
        }
        
        public void setSkipCheck(boolean val) {
        	skipCheck = val;
        }
    }

   /*==========================================================
     * variables
     *==========================================================*/
    private ConsoleInfo _serverInfo;
    private ConsoleInfo _consoleInfo;
    private AgreementInfoPanel _monitor;
    private Hashtable _dsInstances;
    private JTabbedPane _tabbedPanel;;
    private Hashtable _baseSuffixes = null;
    
    private boolean refreshed = false;

    JTextField _bindText;
    private JRadioButton _noEncrypt, _sslEncrypt, _tlsEncrypt;
    private JCheckBox _newWinUserSyncCB, _newWinGroupSyncCB;
    private JRadioButton _simpAuth = null, _sslAuth = null, _gssapiAuth = null, _digestAuth = null;
    JPasswordField _pwdText;
    private JLabel _bind, _pwd, _newWinUserSync, _newWinGroupSync;
    private JFrame _frame;
    private ImageIcon _icon;

    private bindDSEntry        _bindDSEntry;
    private pwdDSEntry         _pwdDSEntry;
    private DSEntryBoolean     _newUserDSEntry;
    private DSEntryBoolean     _newGroupDSEntry;
    private DSEntryBoolean     _ldapDSEntry;
    private DSEntryBoolean     _sslDSEntry;
    private DSEntryBoolean     _tlsDSEntry;
    private DSEntryBoolean     _sslAuthDSEntry = null;
    private DSEntryBoolean     _simpAuthDSEntry = null;
    private DSEntryBoolean     _gssapiAuthDSEntry = null;
    private DSEntryBoolean     _digestAuthDSEntry = null;

    private String _origBindDN;
    private String _origPwd; 

    private String _portAttr = ATTR_PORT;

    private static final int CONTINUE = 0;
    private static final int PROMPT = 1;
    private static final int SKIP =3;

    //get resource bundle
    public static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
    private static final String _section = "replication-destination-dialog";
    private static final String DSClass = "nsDirectoryServer";
    private static final String NSClass = "netscapeServer";
    private static final String ATTR_PORT = "nsserverport";
    private static final String ATTR_SSL_PORT = "nssecureserverport";
}
