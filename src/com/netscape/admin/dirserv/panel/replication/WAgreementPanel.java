/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import javax.swing.JPanel;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.wizard.*;
import com.netscape.admin.dirserv.DSUtil;

/**
 * Panel for Replication Wizard
 *
 * @author  rweltmanjpanchen
 * @version %I%, %G%
 * @date	 	6/1/98
 * @see     com.netscape.admin.dirserv.panel.replication
 */
abstract public class WAgreementPanel extends JPanel {
	/**
	 * Display Help for this page
	 */
    public void callHelp() {
		DSUtil.help( _helpToken );
	}
	/**
	 * Get title for this page
	 */
    public String getTitle() {
		String title = _resource.getString( "replication-wizard",
											_section + "-title" );
		if ( title == null ) {
			Debug.println( "WAgreementPanel.getTitle: no <" +
						   "replication-wizard-"+_section+"-title>" );
		} else {
			Debug.println( "WAgreementPanel.getTitle: " + title );
		}
						   
		return title;
	}
	protected String _helpToken = "";
	//get resource bundle
    public static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
    protected String _section = "";
}
