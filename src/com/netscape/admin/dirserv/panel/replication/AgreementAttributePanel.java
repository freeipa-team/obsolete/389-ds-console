/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.Enumeration;
import java.util.Vector;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.BlankPanel;
import com.netscape.admin.dirserv.panel.DSTabbedPanel;
import com.netscape.admin.dirserv.panel.GroupPanel;
import com.netscape.admin.dirserv.panel.SchemaUtility;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import netscape.ldap.LDAPSchema;
import netscape.ldap.LDAPAttributeSchema;

/**
 * Content Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class AgreementAttributePanel extends BlankPanel
    implements ActionListener, ITabPanel
{
    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    public AgreementAttributePanel(IAgreementPanel parent, int index) {
        super(parent.getModel());
        setTitle(_resource.getString("replication-attribute-tab","label"));
        _parent = parent;
        _agreement = (ReplicationAgreement)parent.getAgreement();
        _index = index;
        _model = parent.getModel();
        _tabbedPanel = parent.getTabbedPane();
        _helpToken = "replication-wizard-attribute-help";
    }

	/*==========================================================
	 * public methods
     *==========================================================*/

    /**
	 * Actual panel construction
	 */
    public void init() {
		GridBagLayout gb = new GridBagLayout();
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
		_myPanel.setLayout(gb);
        _myPanel.setBackground(getBackground());
        _myPanel.setPreferredSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        _myPanel.setMaximumSize(ReplicationTool.DEFAULT_PANEL_SIZE);

        // For now, we don't do filtered replication
        if (false)
        {
        //===========  Entries Panel ============================
        _entriesPanel =
			new GroupPanel(_resource.getString(
				"replication-attribute-entries",
				"label" ));
        GridBagLayout gb1 = new GridBagLayout();
        _entriesPanel.setLayout(gb1);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gb.setConstraints(_entriesPanel, gbc);
        _myPanel.add(_entriesPanel);

        //entire radio
        ButtonGroup entriesGroup = new ButtonGroup();
        _allEntries = makeJRadioButton(_resource.getString(
			"replication-attribute-entriesAll","label"));
        entriesGroup.add(_allEntries);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(0,20,0,5);
        gb1.setConstraints(_allEntries, gbc);
        _entriesPanel.add(_allEntries);

        //filter radio
        _filterEntries = makeJRadioButton(_resource.getString(
			"replication-attribute-matchFilter","label"));
        entriesGroup.add(_filterEntries);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(0,20,0,5);
        gb1.setConstraints(_filterEntries, gbc);
        _entriesPanel.add(_filterEntries);

        //filter textfield
        _filterText = makeJTextField("");
        _activeColor = _filterText.getBackground();
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,30,0,5);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gb1.setConstraints(_filterText, gbc);
        _entriesPanel.add(_filterText);
        }

        //========= Attribute Panel ============================
        _attrPanel =
			new GroupPanel(_resource.getString(
				"replication-attribute-attribute",
				"label" ));
        GridBagLayout gb2 = new GridBagLayout();
        _attrPanel.setLayout(gb2);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.gridheight = gbc.REMAINDER;
        gbc.fill = gbc.BOTH;
        gbc.weightx=1.0;
        gbc.weighty=1.0;
        gb.setConstraints(_attrPanel, gbc);
        _myPanel.add(_attrPanel);

        //radio buttons panel
        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new BorderLayout());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.insets = new Insets(0,20,0,20);
        gb2.setConstraints(radioPanel, gbc);
        _attrPanel.add(radioPanel);

        //left box panel
        _leftPanel =
			new GroupPanel(_resource.getString(
				"replication-attribute-excluded",
				"label" ));
        _leftPanel.setLayout(new BorderLayout());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.fill = gbc.BOTH;
        gbc.weighty=1.0;
        gbc.weightx=0.5;
        gb2.setConstraints(_leftPanel, gbc);
        _attrPanel.add(_leftPanel);

        //attr list box
        _excludeBox = createListBox( _excludeBoxModel, 10);
        _leftScroll = createScrollPane(_excludeBox);
        _leftPanel.add("Center", _leftScroll);

        //right box Panel
        _rightPanel =
			new GroupPanel(_resource.getString(
				"replication-attribute-included",
				"label" ));
        _rightPanel.setLayout(new BorderLayout());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.BOTH;
        gbc.weightx=0.5;
        gbc.weighty=1.0;
        gb2.setConstraints(_rightPanel, gbc);
        _attrPanel.add(_rightPanel);

        //replicated list box
        _includeBox = createListBox( _includeBoxModel, 10);
        _rightScroll = createScrollPane(_includeBox);
        _rightPanel.add("Center", _rightScroll);

        //==== middle button panel ====
        JPanel midPanel = new JPanel();
        GridBagLayout gb2b = new GridBagLayout();
        midPanel.setLayout(gb2b);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridheight = gbc.REMAINDER;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gb2.setConstraints(midPanel, gbc);
        _attrPanel.add(midPanel);

        //add all button
        _addAllButton = makeJButton(_resource.getString(
			"replication-attribute-attributeAddAllButton","label"));
        _addAllButton.setSize(BUTTON_MIN_SIZE);
        ReplicationTool.resetGBC(gbc);
        gb2b.setConstraints(_addAllButton, gbc);
        midPanel.add(_addAllButton);

        //add button
        _addButton = makeJButton(_resource.getString(
			"replication-attribute-attributeAddButton","label"));
        _addButton.setSize(BUTTON_MIN_SIZE);
        ReplicationTool.resetGBC(gbc);
        gb2b.setConstraints(_addButton, gbc);
        midPanel.add(_addButton);

        //remove button
        _removeButton = makeJButton(_resource.getString(
			"replication-attribute-attributeRemoveButton","label"));
        _removeButton.setSize(BUTTON_MIN_SIZE);
        ReplicationTool.resetGBC(gbc);
        gb2b.setConstraints(_removeButton, gbc);
        midPanel.add(_removeButton);

        //remove all button
        _removeAllButton = makeJButton(_resource.getString(
			"replication-attribute-attributeRemoveAllButton","label"));
        _removeAllButton.setSize(BUTTON_MIN_SIZE);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gb2b.setConstraints(_removeAllButton, gbc);
        midPanel.add(_removeAllButton);

        populateData();
        _initialized = true;
    }

    public boolean refresh() {
        return true;
    }

    //========= ACTIONLISTENER =================
    public void actionPerformed(ActionEvent e) {
        if (_initialized) {
            Debug.println("AgreementAttributePanel: actionPerformed()"+e.toString());
            if (e.getSource().equals(_allEntries)) {
                setDirtyFlag();
                _filterText.setEditable(false);
                _filterText.setBackground(getBackground());
                _filterText.repaint(1);
            }
            if (e.getSource().equals(_filterEntries)) {
                setDirtyFlag();
                _filterText.setEditable(true);
                _filterText.setBackground(_activeColor);
                _filterText.repaint(1);
            }
            if (e.getSource().equals(_addAllButton)) {
                setDirtyFlag();
                populateExcludeList(false);
            }
            if (e.getSource().equals(_addButton)) {
                setDirtyFlag();
                //get selected and add them over
                Object obj[] = _excludeBox.getSelectedValues();
                if (obj.length > 0) {
                    for (int i=0; i< obj.length; i++) {
                        SchemaUtility.InsertElement(_includeBoxModel,obj[i]);
                        _excludeBoxModel.removeElement(obj[i]);
                    }
                }
                _excludeBox.invalidate();
                _excludeBox.validate();
                _excludeBox.repaint(1);
            }
            if (e.getSource().equals(_removeButton)) {
                setDirtyFlag();
                //get selected and move them over
                Object obj[] = _includeBox.getSelectedValues();
                if (obj.length > 0) {
                    for (int i=0; i< obj.length; i++) {
                        SchemaUtility.InsertElement(_excludeBoxModel,obj[i]);
                        _includeBoxModel.removeElement(obj[i]);
                    }
                }
                _includeBox.invalidate();
                _includeBox.validate();
                _includeBox.repaint(1);
            }
            if (e.getSource().equals(_removeAllButton)) {
                setDirtyFlag();
                populateExcludeList(true);
            }
        }
    }

    //======== BlankPanel callback functions =======
	public void okCallback() {
	    Debug.println("okCallback()");
	    clearDirtyFlag();
	}

	public void resetCallback() {
	    Debug.println("resetCallback()");
	    populateData();
	    clearDirtyFlag();
	}

	/*========== DOCUMENTLISTENER ================
    public void changedUpdate(DocumentEvent e) {
        if (_initialized) {
            Debug.println("AgreementAttributePanel: "+e.toString());
            setDirtyFlag();
        }
    }
    public void removeUpdate(DocumentEvent e) {
        if (_initialized) {
            Debug.println("AgreementAttributePanel: "+e.toString());
            setDirtyFlag();
        }
    }
    public void insertUpdate(DocumentEvent e) {
        if (_initialized) {
            Debug.println("AgreementAttributePanel: "+e.toString());
            setDirtyFlag();
        }
    }
    */

    //===== ITabPanel interface functions =========
    //specify entry verification rules here
    public boolean validateEntries() {

        //filter
        if ((_filterEntries != null &&_filterEntries.isSelected()) && (_filterText != null && _filterText.getText().equals(""))) {
			String msg = _resource.getString("replication-attribute-dialog",
											 "filterEmpty");
			DSUtil.showErrorDialog( _parent.getFrame(), "error", msg,
									"replication-attribute-dialog" );
            return false;
        }
        if (_includeBoxModel.size() == 0) {
			String msg = _resource.getString("replication-attribute-dialog",
											 "noAttr");
			DSUtil.showErrorDialog( _parent.getFrame(), "error", msg,
									"replication-attribute-dialog" );
            return false;
        }
        return true;
    }

    public void getUpdateInfo( Object inf) {

		AgreementWizardInfo info = (AgreementWizardInfo)inf;
        //save filter
        if (_filterEntries != null &&_filterEntries.isSelected() && (_filterText != null))
            info.setFilter(_filterText.getText());
        else
            info.setFilter("");

        //save attr
        Vector attr = new Vector();
       /*
        if (_excludeBoxModel.size() == 0) {
            _enableFR.setSelected(false);
            info.setAttrType(info.SELATTR_ALL);
            disableAttrSelection();
        } else {
            if (_includeBoxModel.size() <= _excludeBoxModel.size()) {
                for(int i=0; i<_includeBoxModel.size(); i++) {
                    attr.addElement(_includeBoxModel.getElementAt(i));
                }
                info.setAttrType(info.SELATTR_INCLUDE);
            } else {
        */
                for(int i=0; i<_excludeBoxModel.size(); i++) {
                    attr.addElement(_excludeBoxModel.getElementAt(i));
                }
                info.setAttrType(info.SELATTR_EXCLUDE);
        //    }
       // }
        
        info.setSelectedAttr(attr);
    }

    //=======  CHANGELISTENER ===================
    /*
   	public void stateChanged(ChangeEvent e) {
   	    if (!_initialized)
            init();
    }
    */

    /*==========================================================
	 * private methods
	 *==========================================================*/

	/**
	 * Populate the UI with data
	 */
	private void populateData() {
        // We don't do filtered replication at present
        if (false) 
        {
        //entries
        if ( (_agreement.getEntryFilter()== null) || (_agreement.getEntryFilter().equals("")) ) {
            _allEntries.setSelected(true);
            _filterText.setEditable(false);
            _filterText.setBackground(getBackground());
        } else {
            _filterEntries.setSelected(true);
            _filterText.setEditable(true);
            _filterText.setBackground(_activeColor);
            _filterText.setText(_agreement.getEntryFilter());
        }
        }

        //attributes
        if (_agreement.getSelectedAttributes() == null) {
            populateExcludeList(false);
            disableAttrSelection();
        } else {
            //populate replication list
            populateExcludeList(false);
            for (Enumeration e = _agreement.getSelectedAttributes().elements();
                e.hasMoreElements();) {
                String attr = (String) e.nextElement();
                _includeBoxModel.removeElement(attr);
                SchemaUtility.InsertElement(_excludeBoxModel,attr);
            }
            enableAttrSelection();
        }
	}

    //populate Attribute List
    private void populateExcludeList(boolean leftList) {
            //cleanup attr list
            _excludeBoxModel.removeAllElements();
            _includeBoxModel.removeAllElements();

            //get all attributes and populate them
		    LDAPSchema sch = getModel().getSchema();
		    if (sch == null)
		        return;
		    synchronized (sch) {
                for (Enumeration e = sch.getAttributes(); e.hasMoreElements();) {
                    LDAPAttributeSchema las = (LDAPAttributeSchema)e.nextElement();
                    if (leftList)
                        SchemaUtility.InsertElement(_excludeBoxModel,las.getName());
                    else
                        SchemaUtility.InsertElement(_includeBoxModel,las.getName());
                }
            }
            return;
    }

    //disable the attribute selection area
    private void disableAttrSelection() {
        _leftPanel.setEnabled(false);
        _leftScroll.setEnabled(false);
        _rightPanel.setEnabled(false);
        _rightScroll.setEnabled(false);
        _excludeBox.setEnabled(false);
        _includeBox.setEnabled(false);
        _addAllButton.setEnabled(false);
        _addButton.setEnabled(false);
        _removeButton.setEnabled(false);
        _removeAllButton.setEnabled(false);
    }

    //enable the attribute selection area
    private void enableAttrSelection() {
        _leftPanel.setEnabled(true);
        _leftScroll.setEnabled(true);
        _rightPanel.setEnabled(true);
        _rightScroll.setEnabled(true);
        _excludeBox.setEnabled(true);
        _includeBox.setEnabled(true);
        _addAllButton.setEnabled(true);
        _addButton.setEnabled(true);
        _removeButton.setEnabled(true);
        _removeAllButton.setEnabled(true);
    }


	protected void setDirtyFlag()
    {
        super.setDirtyFlag();
		super.setValidFlag();

        if ((_tabbedPanel != null) && (_tabbedPanel.getIconAt(_index )== null) ) {
            _tabbedPanel.setIconAt(_index,
                        ReplicationTool.getImage("red-ball-small.gif"));
            _tabbedPanel.repaint();
        }
    }

    protected void clearDirtyFlag()
    {
        super.clearDirtyFlag();

        if ((_tabbedPanel != null) && (_tabbedPanel.getIconAt(_index )!= null) ){
            _tabbedPanel.setIconAt(_index, null);
            _tabbedPanel.repaint();
        }
    }


    JList createListBox(DefaultListModel listModel, int visibleCount) {
        JList listbox = new JList(listModel);
        listbox.setCellRenderer(new AttrCellRenderer());
        listbox.setSelectionModel(new DefaultListSelectionModel());
        listbox.setPrototypeCellValue("1234567890 1234567890");
        listbox.setVisibleRowCount(visibleCount);
        return listbox;
    }

    JScrollPane createScrollPane(JList listbox) {
        JScrollPane scrollPane = new JScrollPane(listbox,
            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBackground(getBackground());
        scrollPane.setAlignmentX(LEFT_ALIGNMENT);
        scrollPane.setAlignmentY(TOP_ALIGNMENT);
        scrollPane.setBorder(BorderFactory.createLoweredBevelBorder());
        return scrollPane;
    }

    /*==========================================================
     * variables
     *==========================================================*/
    private IAgreementPanel _parent;
    private ReplicationAgreement _agreement;
    private boolean _initialized = false;
    private IDSModel _model;
    private JTabbedPane _tabbedPanel;
	private int _index;
	private Color _activeColor;

    private JScrollPane _rightScroll, _leftScroll;
    private DefaultListModel _excludeBoxModel = new DefaultListModel();
    private DefaultListModel _includeBoxModel = new DefaultListModel();
    private JList _excludeBox, _includeBox;
    private JPanel _entriesPanel, _attrPanel;
    private JRadioButton _allEntries, _filterEntries;
    private JButton _addButton, _removeButton, _addAllButton, _removeAllButton;
    private JTextField _filterText;
    private JPanel _leftPanel, _rightPanel;
    private static final Dimension BUTTON_MIN_SIZE = new Dimension(100,25);

	//get resource bundle
    public static ResourceSet _resource =
	                new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");


}
