/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.management.client.util.*;


/**
 * Server Info Dialog
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
class HostInfoDialog extends AbstractDialog
                     implements ActionListener, MouseMotionListener {
    
    /*==========================================================
     * constructors
     *==========================================================*/
     
    public HostInfoDialog(JFrame parent, String title) {
        this(parent, title, null, 0);
    }

    public HostInfoDialog (JFrame parent, String title, String fqHost, int port){
        super (parent, true);

        setLocationRelativeTo(parent);

        _fqHostName = fqHost;
        _port       = port;

        init ();
        setTitle( title );	
    }

    private void init (){
        getRootPane().setDoubleBuffered(true);
		getContentPane().setLayout(new BorderLayout());
//		getContentPane().setBackground(SystemColor.control);
		int space = UIFactory.getComponentSpace();
		int different = UIFactory.getDifferentSpace();
		int large = 20;

		JPanel centerPanel = new JPanel();
		getContentPane().add("Center",centerPanel);

		centerPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		//desc
        JLabel desc = UIFactory.makeJLabel(_section,
										   "hostInfo",
										   _resource);
		desc.setLabelFor(this);
		getAccessibleContext().setAccessibleDescription(desc.getText());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(space,large,space,different);
        centerPanel.add(desc, gbc);

        //host name
        JLabel host = UIFactory.makeJLabel(_section,
										   "hostName",
										   _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.insets = new Insets(0,large,space,space);
        gbc.fill = gbc.NONE;
        centerPanel.add(host, gbc);

        if (_fqHostName != null)
            _hostText = UIFactory.makeJTextField(null, _fqHostName);
        else
            _hostText = UIFactory.makeJTextField(null, "");
		host.setLabelFor(_hostText);

        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 0.7;
        centerPanel.add(_hostText, gbc);

        //port number
        JLabel port = UIFactory.makeJLabel(_section,
										   "hostPort",
										   _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.insets = new Insets(0,large,space,space);
        gbc.fill = gbc.NONE;
        centerPanel.add(port, gbc);

		_portText = UIFactory.makeJTextField(null, "", 5);
        if (_port != -1)
			_portText.setText(Integer.toString (_port));
		port.setLabelFor(_portText);

        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 0.0;
        gbc.fill = gbc.NONE;
        centerPanel.add(_portText, gbc);

        //button panel
        _bOk = UIFactory.makeJButton( this, "general", "OK" );
        _bCancel = UIFactory.makeJButton( this, "general", "Cancel" );
        _bHelp = UIFactory.makeJButton( this, "general", "Help" );
        
		JButton[] buttons = { _bOk, _bCancel, _bHelp };
        JPanel buttonPanel = UIFactory.makeJButtonPanel( buttons, true );
		JPanel p = new JPanel();
		p.setLayout( new BorderLayout() );
		p.add( "Center", buttonPanel );
		p.add( "South",
			   Box.createVerticalStrut(UIFactory.getDifferentSpace()) );
		p.add( "North",
			   Box.createVerticalStrut(UIFactory.getDifferentSpace()) );
		p.add( "East",
			   Box.createHorizontalStrut(UIFactory.getDifferentSpace()) );
		p.add( "West",
			   Box.createHorizontalStrut(UIFactory.getDifferentSpace()) );
		getContentPane().add("South",p);
		pack();

        DocumentListener l = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				doValidate();
			}
			public void removeUpdate(DocumentEvent e) {
				doValidate();
			}
			public void insertUpdate(DocumentEvent e) {
				doValidate();
			}
			private void doValidate() {
				boolean host_ok = false;
				boolean port_ok = false;

				if ( _hostText.getText().trim().length() > 0 ) {
					host_ok = true;
					String host = new String(_hostText.getText().trim());
					for (int i = 0; host_ok && (i < host.length()); i++) {
						if ( (Character.UnicodeBlock.of(host.charAt(i)) != Character.UnicodeBlock.BASIC_LATIN) ||
						    ( (!Character.isLetterOrDigit(host.charAt(i))) && (host.charAt(i) != '.') &&
						    (host.charAt(i) != '-') ) ) {
							host_ok = false;
						}
                                        }

					try {
						int value = Integer.parseInt(_portText.getText());
						port_ok = ( (value > 0 ) && (value  <= 65535) );
					} catch ( Exception e ) {
					}
				}
				_bOk.setEnabled( host_ok && port_ok );
			}
		};

        addMouseMotionListener(this);
        centerPanel.addMouseMotionListener(this);
        host.addMouseMotionListener(this);
        port.addMouseMotionListener(this);
        _portText.addMouseMotionListener(this);
        _hostText.addMouseMotionListener(this);
        _portText.getDocument().addDocumentListener(l);
        _hostText.getDocument().addDocumentListener(l);
        _bOk.addMouseMotionListener(this);
        _bCancel.addMouseMotionListener(this);
        _bHelp.addMouseMotionListener(this);
        buttonPanel.addMouseMotionListener(this);
        p.addMouseMotionListener(this);

		/* Figure out the initial state of the "OK" button */
		boolean ok = false;
		if ( _hostText.getText().trim().length() > 0 ) {
			try {
				int value = Integer.parseInt(_portText.getText());
				ok = ( (value > 0 ) && (value  <= 65553) );
			} catch ( Exception e ) {
			}
		}
		_bOk.setEnabled( ok );
    }

	/*==========================================================
	 * public methods
     *==========================================================*/

    //========= ACTIONLISTENER =================
    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource().equals(_bOk)) {
                // resolve host name if we haven't yet
                if (_fqHostName == null) {
                    _waiting = true;
                    _bOk.setEnabled(false);
                    _bCancel.setEnabled(false);
                    _bHelp.setEnabled(false);
                    _fqHostName = ReplicationTool.fullyQualifyHostName(_hostText.getText());
					if (_fqHostName == null)
						_fqHostName = new String("");                    
                }
                
                try {
                    _port = Integer.parseInt(_portText.getText());
                } catch(NumberFormatException ex) {
                    return;
                }
                _isOk = true;
                setVisible(false);
                dispose();
				DSUtil.dialogCleanup();
            }
            if (e.getSource().equals(_bCancel)) {
                setVisible(false);
                dispose();
				DSUtil.dialogCleanup();
            }
            if (e.getSource().equals(_bHelp)) {
				DSUtil.help( _helpToken );
			}
        } catch (NullPointerException ex) {
            Debug.println(ex.toString());
        }
    }

    public void mouseDragged(MouseEvent e) {
    }
    public void mouseMoved(MouseEvent e) {
        if (_waiting)
            if (e.getSource() instanceof Component)
                ((Component)e.getSource()).setCursor(
                    Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }
    /**
     * return if cancel is pressed
     * @return true if ok button is pressed
     */
    public boolean isOk() {
        return _isOk;
    }

    /**
     * Host Name
     * @return host name
     */
    public String getHost() {
//        return ReplicationTool.fullyQualifyHostName(_hostText.getText());
        return _fqHostName;
    }

    /**
     * Port Number
     * @retun port number
     */
    public int getPort() {
        return _port;
    }

    /*==========================================================
     * variables
     *==========================================================*/ 
    private boolean _isOk = false;
    private int _port=-1;
    private JButton _bOk, _bCancel, _bHelp;
    private JTextField _hostText, _portText;
    private String _fqHostName = null;
    private boolean _waiting = false;

	//get resource bundle
    private static ResourceSet _resource =
	    new ResourceSet(
			"com.netscape.admin.dirserv.panel.replication.replication");
	private static final String _section = "replication-destination-dialog";
	private static final String _helpToken =
	    "configuration-replication-host-dbox-help";
}
