/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.awt.event.*;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.admin.dirserv.wizard.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;

public class WAgreementInitPanel extends WAgreementPanel
                                 implements IWizardPanel,
                                            ActionListener,
                                            DocumentListener {

    public WAgreementInitPanel(AgreementWizard wizard) {
		_section = "replication-initialize";
		_helpToken = "replication-wizard-consumerinit-help";
		_wizard = wizard;
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        ReplicationTool.resetGBC(gbc);
		int space = UIFactory.getComponentSpace();
		int different = UIFactory.getDifferentSpace();
		int large = 20;
		int tiny = 2;

        JLabel ask = UIFactory.makeJLabel(
			_section, "select-init", _resource );
		ask.setLabelFor(this);

        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(different,space,different,different);
        add(ask, gbc);

        //radio buttons
        ButtonGroup group = new ButtonGroup();
        _notInitConsumer = UIFactory.makeJRadioButton(
			this, _section, "notInitConsumer", false, _resource);
        group.add(_notInitConsumer);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(0,different,0,different);
        add(_notInitConsumer, gbc);

        _initConsumer = UIFactory.makeJRadioButton(
			this, _section, "initConsumer", false, _resource);
		group.add(_initConsumer);
        add(_initConsumer, gbc);

       _initFile = UIFactory.makeJRadioButton(
			this, _section, "createInitFile", true, _resource );        
        group.add(_initFile);		
        add(_initFile, gbc);

        JPanel filePanel = new JPanel();
        filePanel.setLayout(new GridBagLayout());
        filePanel.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.weighty=1.0;
        add(filePanel, gbc);

        _nameLabel = UIFactory.makeJLabel(
			_section, "ldifName", _resource );
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(different,space,0,space);
        filePanel.add(_nameLabel, gbc);

        _nameTextField= UIFactory.makeJTextField(null, "");
		_nameLabel.setLabelFor(_nameTextField);
		_nameTextField.getDocument().addDocumentListener(this);

        _activeColor = _nameTextField.getBackground();
        ReplicationTool.resetGBC(gbc);
        //gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 1.0;
        gbc.insets = new Insets(different,space,0,different);
        filePanel.add(_nameTextField, gbc);

        _browseButton = UIFactory.makeJButton(
			this, _section, "browseButton", _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.insets = new Insets(different,space,0,space);
        filePanel.add(_browseButton, gbc);

		checkNextButton();
    }

    private void disableInitFile() {
        _nameTextField.setEnabled(false);
        _nameTextField.setEditable(false);
        _nameTextField.setBackground(getBackground()); 
        _browseButton.setEnabled(false);
        _nameTextField.invalidate();
        _nameTextField.validate();
        _nameTextField.repaint(1);

        _wizardInfo.removeLDIFFilename();
    }
 
    private void enableInitFile() {
        _nameTextField.setEnabled(true);
        _nameTextField.setEditable(true);
        _nameTextField.setBackground(_activeColor); 
        _browseButton.setEnabled(true);
        _nameTextField.invalidate();
        _nameTextField.validate();
        _nameTextField.repaint(1);
    }

    /*==========================================================
     * public methods
     *==========================================================*/

    public void actionPerformed(ActionEvent e) {
        Debug.println("AgreementInitPanel: actionPerformed()"+e.toString());
        if (e.getSource().equals(_initFile)) {
            enableInitFile();
        } else if ((e.getSource().equals(_notInitConsumer)) ||
            (e.getSource().equals(_initConsumer))) {
            disableInitFile();
        } else if (e.getSource().equals(_browseButton)) {
			String[] extensions = { "ldif" };
			String[] descriptions =
			    { DSUtil._resource.getString( "filefilter", "ldif-label" ) };			
    	    String file = DSFileDialog.getFileName(false, extensions,
												   descriptions, (Component)this, "*.ldif", getDefaultPath());
            if (file != null)
                _nameTextField.setText(file);
        }
		checkNextButton();
    }

	protected String getDefaultPath() {
		return DSUtil.getDefaultLDIFPath(_wizardInfo.getServerInfo());
	}

    //====== IWizardPanel =============================
    public boolean initializePanel(WizardInfo info) {
        Debug.println("WAgreementInitPanel: initialize()");
        _wizardInfo = (AgreementWizardInfo) info;

        if (!_init) {
			/* Propose a default file */
			String fileName = getDefaultPath() + _wizardInfo.getName() + 
				_resource.getString("replication-initialize","file-extension-label");
			_nameTextField.setText(fileName);

            /* disable LDIF file option for certain agreements */
		    if (_wizardInfo.getAgreementType().equals(_wizardInfo.LEGACYR_AGREEMENT)){
			    enableInitFile(false);
		    }
		    else{
			    enableInitFile (true);
		    }

			/* Check the browsebutton */
			if (!DSUtil.isLocal(_wizardInfo.getServerInfo().getHost())) {
				_nameLabel.setText(_resource.getString(_section, "not-local-ldifName-label"));
				_browseButton.setEnabled(false);
			}

            _init = true;
         }
		checkNextButton();
        return true;
    }

    public boolean validatePanel() {
        Debug.println("AgreementInitPanel: validate()");

        /* if ldif file option is selected, check that
           a valid file path is supplied               */

        if (_initFile.isSelected () && !isValidLDIFFile ()){

            _error = _resource.getString(_section,
										 "invalidLDIFFile-msg",
										 _nameTextField.getText().trim());

            return false;
        }

        return true;
    }

    public boolean concludePanel(WizardInfo info) {
        Debug.println("AgreementInitPanel: conclude()");
        return true;
    }

    public void getUpdateInfo(WizardInfo info) {
        Debug.println("AgreementInitPanel: getUpdateInfo()");
        if (_initConsumer.isSelected())
            _wizardInfo.setInitialize(true);
        else
            _wizardInfo.setInitialize(false);

        if (_initFile.isSelected()) 
            _wizardInfo.setLDIFFilename(_nameTextField.getText());
    }

    public String getErrorMessage() {
        return _error;
    }

	protected void enableInitFile (boolean enable)
	{
		_initFile.setVisible      (enable);
		_nameLabel.setVisible     (enable);
		_nameTextField.setVisible (enable);
		_browseButton.setVisible  (enable);
					
		if (enable)
		{
			_initFile.setSelected (true);
            enableInitFile ();
            _wizardInfo.setInitialize (false);
		}
		else
		{
			_notInitConsumer.setSelected (true);
			_wizardInfo.removeLDIFFilename();
            _wizardInfo.setInitialize (false);
		}
	}

    private boolean isValidLDIFFile (){
        String           fileName = _nameTextField.getText ();
        FileOutputStream out; 

        if (fileName.equals (""))
            return false;

		/* If we are not local we can't say if the file exist or not...*/
		if (!DSUtil.isLocal(_wizardInfo.getServerInfo().getHost())) {
			return true;
		}

        try{
            out = new FileOutputStream(fileName);
            
            /* file was successfully created - need to remove */
            out.close ();
            File file = new File (fileName);
            file.delete ();

        }catch (IOException e){
            /* invalid path or insufficient permisions */
            Debug.println ("WAIP: Error trying to check file path " + e);
            return false;
        } 

        return true;   
    } 

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void changedUpdate(DocumentEvent e) {		
		checkNextButton();
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void removeUpdate(DocumentEvent e) {
		changedUpdate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void insertUpdate(DocumentEvent e) {
		changedUpdate(e);
    }

	/**
	 * Method used to update the next button of the wizard.
	 * If the user has selected the option to create an LDIF file with the content of
	 * the replica, we enable the "Next" button if the text field with the name of the 
	 * file is not empty */
	protected void checkNextButton() {
		if (_initFile.isSelected()) {
			if (_nameTextField.getText().trim().equals("")) {
				_wizard.getbNext_Done().setEnabled(false);
				return;
			}
		}
		_wizard.getbNext_Done().setEnabled(true);
	}

	private AgreementWizard _wizard;
	private JLabel _nameLabel;
    private JTextField _nameTextField;
    private Color _activeColor;
    private JRadioButton _initConsumer, _notInitConsumer, _initFile;
    private JButton _browseButton;
    private AgreementWizardInfo _wizardInfo;
    private String _error = "WAgreementInitPanel: error message";
    private boolean _init = false;
}
