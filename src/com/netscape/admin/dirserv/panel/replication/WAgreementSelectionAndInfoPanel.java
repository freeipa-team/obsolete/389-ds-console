/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.admin.dirserv.wizard.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.RemoteImage;
import netscape.ldap.*;


/**
 * Information Panel for Replication Agreement
 *
 * @author  jvergara
 * @version %I%, %G%
 * @date	 	11/11/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */

public class WAgreementSelectionAndInfoPanel extends WAgreementPanel
    implements DocumentListener, ActionListener, IWizardPanel {

    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
   
    public WAgreementSelectionAndInfoPanel(AgreementWizard wizard) {		
	super();
	_helpToken = "replication-wizard-legacyrmmrname-help";
	_section = "replication-info";

	_wizard = wizard;
    }
    public WAgreementSelectionAndInfoPanel(AgreementWizard wizard, 
					   boolean showSelectionPanel,
					   String type) {       
	this(wizard);
	_type = type;
	_showSelectionPanel = showSelectionPanel;			
    }
   
    /*==========================================================
     * public methods
     *==========================================================*/

    public void init() {
	GridBagLayout gb = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints();
	ReplicationTool.resetGBC(gbc);
	setLayout(gb);

        JLabel ask = UIFactory.makeJLabel("replication",
					  "info-ask",
					  _resource);
		ask.setLabelFor(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.insets = UIFactory.getBorderInsets();
        gbc.insets.left = UIFactory.getComponentSpace();
        gb.setConstraints(ask, gbc);
        add(ask);
	
	//======== Selection Panel ====================
	if (_showSelectionPanel) {
	    Debug.println("WAgreementSelectionAndInfoPanel.init(): Including the SelectionPanel");
	    JPanel selectionPanel = new JPanel();
	    selectionPanel.setLayout(new GridBagLayout());
	    selectionPanel.setBackground(getBackground());

	    ReplicationTool.resetGBC(gbc);
	    int space = UIFactory.getComponentSpace();
	    int different = UIFactory.getDifferentSpace();
	    int large = 20;
	    int tiny = 2;

	    //wizard images
	    //======== selectionTop Panel ===========================
	    JPanel selectionTop = new JPanel();
	    selectionTop.setLayout(new GridBagLayout());
	    selectionTop.setBackground(getBackground());
	    ReplicationTool.resetGBC(gbc);
	    gbc.anchor = gbc.NORTH;
	    gbc.gridwidth = gbc.REMAINDER;
	    gbc.weightx = 1.0;
	    selectionPanel.add(selectionTop, gbc);

	    //add icon
	    RemoteImage selectionImg = ReplicationTool.getImage(
						_resource.getString(_section2,
								    "wizardIcon"));
	    JLabel selectionIconLabel1 = UIFactory.makeJLabel(selectionImg);
	    ReplicationTool.resetGBC(gbc);
	    gbc.fill = gbc.NONE;
	    selectionTop.add(selectionIconLabel1, gbc);

	    //wizard name
	    JLabel selectionName = UIFactory.makeJLabel(_section2,
							"wizard",
							_resource);
	    ReplicationTool.resetGBC(gbc);
	    gbc.weightx=1.0;
	    gbc.gridwidth = gbc.REMAINDER;
	    selectionTop.add(selectionName, gbc);
		selectionName.setLabelFor(this);

	    //==== wizard description =============
	    JTextArea selectionDesc =
		UIFactory.makeMultiLineLabel( 2, 30,
				_resource.getString(_section2,
						    "desc") );
	    selectionDesc.setBackground(getBackground());
	    selectionDesc.setEditable(false);
	    selectionDesc.setCaretColor(getBackground());
	    gbc.anchor = gbc.NORTH;
	    gbc.weightx = 1.0;
	    gbc.weighty = 0.1;
	    gbc.insets = new Insets(different,large,space,different);
	    gbc.gridwidth = gbc.REMAINDER;
	    selectionPanel.add(selectionDesc, gbc);

	    // 			//==== agreement type selection panel =======
	    // 			JPanel selectionBottom = new JPanel();
	    // 			selectionBottom.setLayout(new GridBagLayout());
	    // 			selectionBottom.setBackground(getBackground());
	    // 			ReplicationTool.resetGBC(gbc);
	    // 			gbc.anchor = gbc.NORTH;
	    // 			gbc.gridwidth = gbc.REMAINDER;
	    // 			gbc.insets = new Insets(10,40,5,10);
	    // 			gbc.weightx = 1.0;
	    // 			gbc.weighty = 0.9;
	    // 			selectionPanel.add(selectionBottom, gbc);

	    // 			//label
	    // 			JLabel select = UIFactory.makeJLabel(
	    // 												 _section2, "selectType", _resource );
	    // 			ReplicationTool.resetGBC(gbc);
	    // 			gbc.weightx = 1.0;
	    // 			gbc.anchor = gbc.WEST;
	    // 			gbc.fill = gbc.NONE;
	    // 			gbc.gridwidth = gbc.REMAINDER;
	    // 			selectionBottom.add(select, gbc);
	    // 			selectionDesc.setFont( select.getFont() );

	    //radio buttons
	    // 			ButtonGroup group = new ButtonGroup();
	    // 			_legacyrButton = UIFactory.makeJRadioButton(
	    // 													this, _section2, "LEGACYR", false, _resource );
	    // 			_legacyrButton.addActionListener(this);
	    // 			group.add(_legacyrButton);
	    // 			gbc.anchor = gbc.WEST;
	    // 			gbc.fill = gbc.NONE;
	    // 			gbc.weightx = 1.0;
	    // 			gbc.gridwidth = gbc.REMAINDER;
	    // 			gbc.insets = new Insets(space,large,0,space);
	    // 			selectionBottom.add(_legacyrButton, gbc);
	    // 			JTextArea label =
	    // 				UIFactory.makeMultiLineLabel( 2, 30,
	    // 											  _resource.getString(
	    // 																  _section2, "LEGACYR-desc-label" ) );
	    // 			gbc.fill = gbc.HORIZONTAL;
	    // 			gbc.gridwidth = gbc.REMAINDER;
	    // 			selectionBottom.add(label, gbc);

	    // 			_mmrButton = UIFactory.makeJRadioButton(
	    // 													this, _section2, "MMR", false, _resource );
	    // 			_mmrButton.addActionListener(this);
	    // 			group.add(_mmrButton);
	    // 			gbc.anchor = gbc.WEST;
	    // 			gbc.fill = gbc.NONE;
	    // 			gbc.weightx = 1.0;
	    // 			gbc.gridwidth = gbc.REMAINDER;
	    // 			gbc.insets = new Insets(space,large,space,space);
	    // 			selectionBottom.add(_mmrButton, gbc);
	    // 			label =
	    // 				UIFactory.makeMultiLineLabel( 2, 30,
	    // 											  _resource.getString(
	    // 																  _section2, "MMR-desc-label" ) );
	    // 			gbc.insets = new Insets(0,large,space,space);
	    // 			gbc.fill = gbc.HORIZONTAL;
	    // 			gbc.gridheight = gbc.REMAINDER;
	    // 			selectionBottom.add(label, gbc);
			
	    //  			if (_type.equals(AgreementWizardInfo.LEGACYR_AGREEMENT)){
	    //  				_legacyrButton.setSelected(true);
	    //  				_legacyrButton.requestFocus();
	    //  			}else{ /* MMR */
	    //  				_mmrButton.setSelected(true);
	    //  				_mmrButton.requestFocus();
	    //  			}
			
	    ReplicationTool.resetGBC(gbc);
	    gbc.anchor = gbc.NORTH;
	    gbc.gridwidth = gbc.REMAINDER;
	    gbc.weightx = 1.0;
	    gbc.weighty = 0.1;
	    add(selectionPanel, gbc);
			
	}

        //======== Top Panel ===========================
        JPanel top = new JPanel();
        top.setLayout(new GridBagLayout());
        top.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gbc.weighty = 0.1;
        add(top, gbc);

        //add icon	
        img1 = ReplicationTool.getImage(_resource.getString(
							    _section, "legacyrIcon"));
        desc1 = _resource.getString(_section,"legacyrDesc");
        img2 = ReplicationTool.getImage(_resource.getString(
							    _section, "mmrIcon"));
        desc2 = _resource.getString(_section, "mmrDesc");
		
        iconLabel1 = UIFactory.makeJLabel(img1);
        ReplicationTool.resetGBC(gbc);
        gbc.fill = gbc.NONE;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS;
        top.add(iconLabel1, gbc);


        JPanel titlePanel = new JPanel();
        GridBagLayout gb1a = new GridBagLayout();
        titlePanel.setLayout(gb1a);
        titlePanel.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.weightx = 1.0;
        gbc.gridwidth = gbc.REMAINDER;
        top.add(titlePanel, gbc);

        JLabel _nameText = UIFactory.makeJLabel("");
        ReplicationTool.resetGBC(gbc);
        gbc.weightx=1.0;
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS;
        titlePanel.add(_nameText, gbc);

        descLabel1 = UIFactory.makeJLabel(desc1);
		descLabel1.setLabelFor(this);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.gridheight = gbc.REMAINDER;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS;
        gbc.weightx=1.0;
        titlePanel.add(descLabel1, gbc);

        //name
        JLabel name = UIFactory.makeJLabel("replication",
					   "destination-name",
					   _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = UIFactory.getComponentInsets();
        gbc.insets.bottom = 0;
	gbc.insets.top = UIFactory.getDifferentSpace();
        top.add(name, gbc);
	
        _nameTextField= UIFactory.makeJTextField(null, "");			
		name.setLabelFor(_nameTextField);
		_nameTextField.getDocument().addDocumentListener(this); 
		
        _activeColor = _nameText.getBackground();
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = 1; // gbc.REMAINDER;
        gbc.weightx= 1.0;
        gbc.insets = UIFactory.getBorderInsets();
        gbc.insets.left = UIFactory.getComponentSpace();
        gbc.insets.bottom = 0;
        gbc.insets = new Insets(10,5,0,10);
        top.add(_nameTextField, gbc);

	JLabel filler = new JLabel(""); // here to reduce name size compare to description
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 1.0;
	top.add(filler, gbc);

	// description
        JLabel description = UIFactory.makeJLabel("replication",
						  "destination-description",
						  _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = UIFactory.getComponentInsets();
        gbc.insets.bottom = 0;
	gbc.insets.top = UIFactory.getDifferentSpace();
        top.add(description, gbc);
	
        _descriptionTextField= UIFactory.makeJTextField(null, "");
		description.setLabelFor(_descriptionTextField);
		_descriptionTextField.getDocument().addDocumentListener(this); 
		
        _activeColor = _nameText.getBackground();
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 1.0;
        gbc.insets = UIFactory.getBorderInsets();
        gbc.insets.left = UIFactory.getComponentSpace();
        gbc.insets.bottom = 0;
        gbc.insets = new Insets(10,5,0,10);
        top.add(_descriptionTextField, gbc);

	gbc.weighty=1.0;
	gbc.fill = gbc.VERTICAL;
	add(Box.createVerticalGlue(),gbc);
        populateData();
        _initialized = true;
    }

    //====== IWizardPanel =============================
    public boolean initializePanel(WizardInfo info) {
        Debug.println("WAgreementSelectionAndInfoPanel: initialize()");
					
        _wizardInfo = (AgreementWizardInfo) info;
        if (!_initialized) {		
            init();				
	    if (_wizardInfo.getAgreementType()!= null) {
		if (_wizardInfo.getAgreementType().equals(_wizardInfo.MMR_AGREEMENT) &&
		    _mmrButton != null) {
		    _mmrButton.setSelected(true);
		}
	    }
	}

	if (_legacyrButton != null && _legacyrButton.isSelected()){
            setLEGACYR(true);
        } else {
            setLEGACYR(false);
        }
	if (isTextFieldEmpty()) {
	    _wizard.getbNext_Done().setEnabled(false);
	} else {
	    _wizard.getbNext_Done().setEnabled(true);
	}
				
        return true;
    }

    public boolean validatePanel() {
        Debug.println("WAgreementSelectionAndInfoPanel: conclude()");
        if (_nameTextField.getText().equals("")) {
            _error = _resource.getString(_section, "dialog-nameEmpty");
            return false;
        }
        if (_descriptionTextField.getText().equals("")) {
            _error = _resource.getString(_section, "dialog-descrEmpty");
            return false;
        }
	       
        return true;
    }

    public boolean concludePanel(WizardInfo info) {
        Debug.println("WAgreementSelectionAndInfoPanel: conclude()");
        return true;
    }

    public void getUpdateInfo(WizardInfo info) {
        Debug.println("WAgreementSelectionAndInfoPanel: getUpdateInfo()");
        _wizardInfo.setName(_nameTextField.getText());
	_wizardInfo.setDescription(_descriptionTextField.getText());
	if (_legacyrButton != null && _legacyrButton.isSelected()) {
	    _wizardInfo.setAgreementType(_wizardInfo.LEGACYR_AGREEMENT);
	} else {
	    _wizardInfo.setAgreementType(_wizardInfo.MMR_AGREEMENT);
	}
    }
    
    public String getErrorMessage() {
        return _error;
    }

    /*==========================================================
	 * private methods
	 *==========================================================*/

    /**
     * Populate the UI with data
     */
    private void populateData() {
        if ((_wizardInfo.getName()!= null) &&
	    (!_wizardInfo.getName().equals(""))) {
            _nameTextField.setText(_wizardInfo.getName());
        }
	if((_wizardInfo.getDescription()!=null) &&
	   (!_wizardInfo.getDescription().equals(""))) {
	    _descriptionTextField.setText(_wizardInfo.getDescription());
	}
    }

    private void setLEGACYR(boolean x) {	
	if (x) {
	    descLabel1.setText(desc1);
	    iconLabel1.setIcon(img1);
	} else {
	    descLabel1.setText(desc2);
	    iconLabel1.setIcon(img2);
	}	
    }
    
    public boolean isTextFieldEmpty() {
	return (_nameTextField.getText().trim().equals("") || 
		_descriptionTextField.getText().trim().equals(""));
    }

    public void changedUpdate(DocumentEvent e) {			
	if (isTextFieldEmpty()) {
	    _wizard.getbNext_Done().setEnabled(false);
	} else {
	    _wizard.getbNext_Done().setEnabled(true);
	}	
    }
	
			

    public void removeUpdate(DocumentEvent e) {
	changedUpdate(e);
    }

    public void insertUpdate(DocumentEvent e) {
	changedUpdate(e);			
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource().equals(_legacyrButton)) {
	    setLEGACYR(true);
	} 
	if (e.getSource().equals(_mmrButton)) {
	    setLEGACYR(false);
	} 
    }

    /*==========================================================
   * variables
   *==========================================================*/	

    private AgreementWizard _wizard = null;
    private boolean _showSelectionPanel=false;
    private String _type = "";
    private String _section2 = "replication-agreementWizard";
    private JRadioButton _legacyrButton = null, _mmrButton = null;
    private RemoteImage img1, img2;	
    private String desc1, desc2;

    private Color _activeColor;
    private AgreementWizardInfo _wizardInfo;
    private boolean _initialized = false;
    private String _error = "WAgreementSchedulePanel: error message";

    private JTextField _nameTextField;
    private JTextField _descriptionTextField;

    private JLabel iconLabel1, descLabel1;
}
