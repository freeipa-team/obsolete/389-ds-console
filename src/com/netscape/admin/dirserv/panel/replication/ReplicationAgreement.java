/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.util.*;
import java.text.*;
import java.net.*;
import java.io.*;
import javax.swing.*;
import netscape.ldap.*;
import netscape.ldap.controls.*;
import netscape.ldap.util.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.admin.dirserv.task.ReadOnly;
import com.netscape.admin.dirserv.task.LDAPExport;

/**
 * Replication Agreement
 *
 * @author  ggoods
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/11/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class ReplicationAgreement implements IReplicationAgreement {
    
    /*==========================================================
     * constructors
     *==========================================================*/
    public ReplicationAgreement(int aType) {
        agreementType = aType;
        selattrType = SELATTR_ALL;  /* just a guess */
        agreementIsNew = true;
        updateSchedule = new Vector();
        needsReORC = false;
    }
    
	/*==========================================================
	 * public methods
     *==========================================================*/
     
    /**
     * Set the current server information
     */
    public void setServerInfo(ConsoleInfo info) {
        _serverInfo = info;
    }
    
    /**
     * Get server info
     */
    public ConsoleInfo getServerInfo() {
        return _serverInfo;
    }

    /**
     * Set the nickname for this agreement.
     */
    public void setNickname(String s) {
        nickname = s;
    }

    /**
     * Get the nickname for this agreement.
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Set the original DN of the entry corresponding to this agreement.
     */
    public void setOrigEntryDN(String dn) {
		Debug.println( "ReplicationAgreement.setOrigEntryDN: " + dn );
        origEntryDN = dn;
    }

    /**
     * Get the original DN of the entry corresponding to this agreement.
     */
    public String getOrigEntryDN() {
        return origEntryDN;
    }

    /**
     * Set the DN of the entry corresponding to this agreement.
     */
    protected void setEntryDN(String dn) {
		Debug.println( "ReplicationAgreement.setEntryDN: " + dn );
        entryDN = dn;
    }

    /**
     * Get the DN of the entry corresponding to this agreement.
     */
    public String getEntryDN() {
        return entryDN;
    }
    
    /**
     * Set the root of the replicated subtree.  This routine
     * removes any extra spaces between DN components.
     */
    public void setReplicatedSubtree(String r) {
        if (r == null ||
			r.equals(_resource.getString("replication","misc-rootName"))) {
            replicatedSubtree = "";
        } else {
            if (r == null || r.length() == 0) {
                replicatedSubtree = null;
            } else {
                DN tmpDN = new DN(r);
                replicatedSubtree = tmpDN.toString();
            }
        }
   }

    /**
     * Get the root of the replicated subtree.
     */
    public String getReplicatedSubtree() {
        return replicatedSubtree;
    }

    /**
     * Set the host name of the consumer.
     */
    public void setConsumerHost(String h) {
        consumerHost = h;
    }

    /**
     * Get the host name of the consumer.
     */
    public String getConsumerHost() {
        return consumerHost;
    }

    /**
     * Set the port number for the consumer server.
     */
    public void setConsumerPort(int p) {
        consumerPort = p;
    }

    /**
     * Get the port number for the consumer server.
     */
    public int getConsumerPort() {
        return consumerPort;
    }
    /**
     * Set the host name of the supplier.
     */
    public void setSupplierHost(String h) {
        supplierHost = h;
    }

    /**
     * Get the host name of the supplier.
     */
    public String getSupplierHost() {
        return supplierHost;
    }

    /**
     * Set the port number for the supplier server.
     */
    public void setSupplierPort(int p) {
        supplierPort = p;
    }

    /**
     * Get the port number for the supplier server.
     */
    public int getSupplierPort() {
        return supplierPort;
    }

    /**
     * Set the remote host name of the server
     */
    public void setRemoteHost(String h) {
		consumerHost = h;
    }

    /**
     * Get the remote host name
     */
	public String getRemoteHost() {
		return consumerHost;
    }

    /**
     * Set the remote port of the server
     */
    public void setRemotePort(int p) {
		consumerPort = p;
    }

    /**
     * Get the remote port
     */
    public int getRemotePort() {
		return consumerPort;
    }

    /**
     * Set the DN used when binding to the consumer.
     */
    public void setBindDN(String dn) {
        bindDN = dn;
    }

    /**
     * Get the DN used when binding to the consumer.
     */
    public String getBindDN() {
        return bindDN;
    }

    /**
     * Set the credentials used when binding to the consumer.
     */
    public void setBindCredentials(String cred) {
        bindCredentials = cred;
    }

    /**
     * Get the credentials used when binding to the consumer.
     */
    public String getBindCredentials() {
        return bindCredentials;
    }

    /**
     * Set the schedule used to update the consumer.
     */
    public void setUpdateSchedule(Vector sched) {
        updateSchedule = sched;
    }

    /**
     * Set the schedule used to update the consumer.
     */
    public void setUpdateSchedule(String s) {
        updateSchedule.removeAllElements();
        updateSchedule.addElement(s);
    }

    /**
     * Add to the update schedule
     */
    public void addUpdateSchedule(String schedItem) {
        if (schedItem != null) {
            updateSchedule.addElement(schedItem);
        }
    }


    /**
     * Get the schedule used to update the consumer.
     */
    public Vector getUpdateSchedule() {
        return updateSchedule;
    }

    /**
     * Get the replication schedule, as an array of Strings
     */
    public String[] getUpdateScheduleStrings() {
        String[] ret;

        if (updateSchedule == null || updateSchedule.isEmpty()) {
            return null;
        } else {
            ret = new String[updateSchedule.size()];
        }
        Enumeration schedEnum = updateSchedule.elements();
        int i = 0;
        while (schedEnum.hasMoreElements()) {
            ret[i] = ((String)schedEnum.nextElement());
            i++;
        }
        return ret;
    }

    /**
     * Set the useSSL flag
     */
    public void setUseSSL(String val) {
	Debug.println(8,"ReplicationAgreement.useSSL() val = " + val);
        if (val.equalsIgnoreCase("true")) {
            useSSL = true;
        } else if (val.equals("1")) {
            useSSL = true;
        } else if (val.equals(ReplicationTool.REPLICA_TRANSPORT_SSL)) {
            useSSL = true;
        } else {
            useSSL = false;
        }
	Debug.println(8,"ReplicationAgreement.useSSL() = " + useSSL );
    }

    /**
     * Set the useSSL flag
     */
    public void setUseSSL(boolean val) {
        useSSL = val;
    }

    /**
     * Get the useSSL flag
     */
    public boolean getUseSSL() {
        return useSSL;
    }

    /**
     * Set the useStartTLS flag
     */
    public void setUseStartTLS(String val) {
	Debug.println(8,"ReplicationAgreement.useStartTLS() val = " + val);
        if ((val != null) && val.equals(ReplicationTool.REPLICA_TRANSPORT_TLS)) {
            useStartTLS = true;
            useSSL = false;
        } else {
            useStartTLS = false;
        }
	Debug.println(8,"ReplicationAgreement.useStartTLS() = " + useStartTLS );
    }

    /**
     * Set the useSSL flag
     */
    public void setUseStartTLS(boolean val) {
        useStartTLS = val;
    }

    /**
     * Get the useSSL flag
     */
    public boolean getUseStartTLS() {
        return useStartTLS;
    }

     /**
     * Set the setUseSSLAuth flag
     */
    public void setUseSSLAuth(String val) {
        if (val.equalsIgnoreCase("true")) {
            useSSLAuth = true;
        }  else if (val.equals(ReplicationTool.REPLICA_BINDMETHOD_SSLCLIENTAUTH)) {
            useSSLAuth = true;
        } else {
            useSSLAuth = false;
        }
    }
   
    /**
     * Set the setUseSSLAuth flag
     */
    public void setUseSSLAuth(boolean val) {
        useSSLAuth = val;
    }

    /**
     * Get the useSSLAuth flag
     */
    public boolean getUseSSLAuth() {
        return useSSLAuth;
    }

    /**
     * Set the setUseGSSAPIAuth flag
     */
    public void setUseGSSAPIAuth(String val) {
        if (val.equalsIgnoreCase("true")) {
            useGSSAPIAuth = true;
        } else if (val.equals(ReplicationTool.REPLICA_BINDMETHOD_SASL_GSSAPI)) {
        	useGSSAPIAuth = true;
        } else {
        	useGSSAPIAuth = false;
        }
    }
   
    /**
     * Set the setUseGSSAPIAuth flag
     */
    public void setUseGSSAPIAuth(boolean val) {
    	useGSSAPIAuth = val;
    }

    /**
     * Get the setUseGSSAPIAuth flag
     */
    public boolean getUseGSSAPIAuth() {
        return useGSSAPIAuth;
    }

    /**
     * Set the setUseDigestAuth flag
     */
    public void setUseDigestAuth(String val) {
        if (val.equalsIgnoreCase("true")) {
            useDigestAuth = true;
        } else if (val.equals(ReplicationTool.REPLICA_BINDMETHOD_SASL_DIGEST_MD5)) {
        	useDigestAuth = true;
        } else {
        	useDigestAuth = false;
        }
    }
   
    /**
     * Set the setUseDigestAuth flag
     */
    public void setUseDigestAuth(boolean val) {
    	useDigestAuth = val;
    }

    /**
     * Get the setUseDigestAuth flag
     */
    public boolean getUseDigestAuth() {
        return useDigestAuth;
    }

    /**
     * Set the type of agreement.
     */
    public void setAgreementType(int type) {
        agreementType = type;
    }

    /**
     * Get the agreement type
     */
    public int getAgreementType() {
        return agreementType;
    }

    /**
     * Get the CN (common name) of this entry
     */
    public String getEntryCN() {
        return entryCN;
    }

    /**
     * Explicitly set the CN (common name) of this entry
     */
    public void setEntryCN(String cn) {
        entryCN = cn;
    }

    /**
     * Set the replica entry filter (for filtered replication)
     */
    public void setEntryFilter(String f) {
        entryFilter = f;
    }

    /**
     * Get the replica entry filter (for filtered replication)
     */
    public String getEntryFilter() {
        return entryFilter;
    }

    /**
     * Get the type of attribute selection
     */
    public int getSelattrType() {
        return selattrType;
    }

    /**
     * Set the type of attribute selection
     */
    public void setSelattrType(int t) {
        selattrType = t;
    }

    /**
     * Get the list of selected attributes
     */
    public Vector getSelectedAttributes() {
        return selectedAttrs;
    }

    public String getSelectedAttributesString() {
        String retStr = "";
        if (selectedAttrs == null) {
            return retStr;
        }
        Enumeration en = selectedAttrs.elements();
        while (en.hasMoreElements()) {
            retStr += (String)en.nextElement() + "\n";
        }
        return retStr;
    }

    /**
     * Set the list of selected attributes in Sorted Vector format
     */
    public void setSelectedAttributes(Vector s) {
        selectedAttrs = s;    
    }

    /**
     * Set the list of selected attributes, sorting it on the
     * way in.
     */
    public void setSelectedAttributes(String s) {
        // Parse the list into a set of attributes.  Tokens are
        // whitespace, newlines, tabs, colon, or comma.
        // The sorting algorithm is lame, but who cares?  There
        // aren't more than a few hundred elements, tops.

        int i, j;
        StringTokenizer st = new StringTokenizer(s, " \n\t:,", false);
        Vector tmp = new Vector();
        while (st.hasMoreTokens()) {
            tmp.addElement(st.nextToken());
        }

        // Convert to an array of objects
        String[] arr = new String[tmp.size()];
        tmp.copyInto(arr);

        // Sort "arr" with a cheesy bubble sort
        for (i = arr.length; --i >= 0; ) {
            boolean swapped = false;
            for (j = 0; j < i; j++) {
                if (arr[j].toLowerCase().compareTo(arr[j + 1].toLowerCase()) > 0) {
                    String o = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = o;
                    swapped = true;
                }
                //if (!swapped) {
                   // break;
                //}
            }
        }

        // Copy back into selectedAttrs
        selectedAttrs = new Vector(arr.length);
        for (i = 0; i < arr.length; i++) {
            if (i < arr.length - 1) {
                // Remove any duplicates
                if (arr[i].equalsIgnoreCase(arr[i +1])) {
                    continue;
                }
            }
            selectedAttrs.addElement(arr[i]);
        }
    }

    /**
     * Return a long, descriptive string for this agreement
     */
//     public String getDescription() {
//         String r;
//         String host;
//         int port;

// 		host = consumerHost;
// 		port = consumerPort;

//         r = host;
//         if (r != null && port != 0) {
//             r = r + " : " + port;
//         }
//         if (nickname != null) {
//             r = r + " (" + nickname + ")";
//         }
//         return r;
//     }


    /** 
     * Set agreement description
     */
    public void setDescription(String s){
	_description = s;
    }

    public String getDescription(){
	return _description;	
    }
    /**
     * Set a reference to the other replciation agreements
     */
    public void setAgreementTable(AgreementTable r) {
        repList = r;
    }

    /**
     * Set the ORC value (one of "start", ???, or null)
     */
    public void setORCValue(String s) {
        orcValue = s;
    }


    /**
     * Get the state of ORC at the time the entry was last read
     */
    public String getORCValue() {
        return orcValue;
    }


    /**
     * Return true if the agreement's DN has changed, false otherwise
     */
    public boolean agreementDNHasChanged() {
	computeNewEntryCNandDN();
        boolean changed = !DSUtil.equalDNs(origEntryDN, entryDN);
	Debug.println( "ReplicationAgreement.agreementDNHasChanged: " +
		       origEntryDN + (changed ? " <> " : " = ") +
		       entryDN );
        return changed;
    }

    /**
     * Construct a new entry CN and DNbased on the replica root, host,
         * and port number
     */
    protected void computeNewEntryCNandDN() {
	entryCN = nickname;
        String xdn[] = LDAPDN.explodeDN(origEntryDN, false);
        entryDN = "cn" + "=" + entryCN;

        if (xdn == null || xdn.length == 0) {
            entryDN = null;
        } else {
            for (int i = 1; i < xdn.length; i++) {
                entryDN = entryDN + ", " + xdn[i];
            }
        }
    }

    /**
     * Return true if the agreement is new (that is, it does not yet
     * exist as an entry in the server's database.
     */
    public boolean getAgreementIsNew() {
        return agreementIsNew;
    }
    
    public int writeAgreementToServer() {
        int rc = 0;

        if (checkForReORC()) {
            Debug.println("ReplicationAgreement.writeAgreementToServer: " +
						  "Consumer needs to be reinitialized");
        }
        
        // First check for some common fixups we need to do:
        // 1) If the agreement's subtree, host, or port number has changed,
		//    its DN will change.
        switch (agreementType) {
        case AGREEMENT_TYPE_MMR:
            setConsumerHost(ReplicationTool.fullyQualifyHostName(
				getConsumerHost()));
            try {
                rc = ((MMRAgreement)this).writeToServer();
            } catch (IOException e) {
                Debug.println("ReplicationAgreement.writeAgreementToServer: " +
							  "Cannot save entry <" + entryDN +
							  "> to server: " + e );
            }
            break;
        case AGREEMENT_TYPE_LEGACYR:
            setConsumerHost(ReplicationTool.fullyQualifyHostName(
				getConsumerHost()));
            try {
                rc = ((SIRAgreement)this).writeToServer();
            } catch (IOException e) {
                Debug.println("ReplicationAgreement.writeAgreementToServer: " +
							  "Cannot save entry <" + entryDN +
							  "> to server: " + e );
            }
            break;
            case AGREEMENT_TYPE_AD:
           // case AGREEMENT_TYPE_NT:
                    setConsumerHost(ReplicationTool.fullyQualifyHostName(
				getConsumerHost()));
            try {
                rc = ((ActiveDirectoryAgreement)this).writeToServer();
            } catch (IOException e) {
                Debug.println("ReplicationAgreement.writeAgreementToServer: " +
							  "Cannot save entry <" + entryDN +
							  "> to server: " + e );
            }
            break;
        }
        return rc;
    }

    public void deleteAgreementFromServer() throws LDAPException {
        LDAPConnection ld = _serverInfo.getLDAPConnection();
        ld.delete(origEntryDN);
    }
    
    /**
     * Verify that the agreement is valid, and doesn't conflict with any
     * other agreements we know about.  "conflict" means has the same
     * server, port, and subtree.  Returns a vector of integers describing
     * the errors, which will be empty if there were no errors.
     */
    public Vector checkForErrors() {
        Vector errors = new Vector();
        Enumeration replicasEnum = repList.elements();
        String tmpHost;
        if ((tmpHost =
			 ReplicationTool.fullyQualifyHostName(getRemoteHost())) != null) {
            setRemoteHost(tmpHost);
        }
        while (replicasEnum.hasMoreElements()) {
            ReplicationAgreement ra =
				(ReplicationAgreement) replicasEnum.nextElement();
            if (!ra.getRemoteHost().equalsIgnoreCase(getRemoteHost())) {
                continue;
            }
            if (ra.getRemotePort() != getRemotePort()) {
                continue;
            }
            if (!DSUtil.equalDNs(ra.getReplicatedSubtree(),
								 getReplicatedSubtree())) {
                continue;
            } else if (ra == this) {
                // If it's this agreement, then it's ok.
                continue;
            } else {
            }
            errors.addElement(new Integer(AGREEMENT_EXISTS));
        }
        
        return errors;
    }


    /**
     * Cause the replica to be updated immediately
     */
    public void updateNow() {   
        String attrname;
        Debug.println("ReplicationAgreement.updateNow: begin");
        if (agreementType == AGREEMENT_TYPE_LEGACYR) {
            attrname = "replicaUpdateSchedule";
        } else if (agreementType == AGREEMENT_TYPE_MMR || agreementType == AGREEMENT_TYPE_AD) {
            attrname = ReplicationTool.REPLICA_SCHEDULE_ATTR;
        } else {
            return;
        }
        
		LDAPModificationSet mods;
		LDAPAttribute attr;
        try {
            mods = new LDAPModificationSet();
            attr = new LDAPAttribute(attrname, "*");
            mods.add(LDAPModification.ADD, attr);
            updateEntry(mods);
        } catch (LDAPException me) {
			Debug.println("Unable to add value \"*\" to attribute " + attrname + ": " + me.toString());
		}

        if (agreementType == AGREEMENT_TYPE_LEGACYR) {            
            try {
                mods = new LDAPModificationSet();
                attr = new LDAPAttribute("replicaUpdateReplayed", (String)null);
                mods.add(LDAPModification.REPLACE, attr);
                attr = new LDAPAttribute("replicaUpdateFailedAt", (String)null);
                mods.add(LDAPModification.REPLACE, attr);
                updateEntry(mods);
            } catch (LDAPException me2) {
			Debug.println("Unable to remove \"replicaUpdateReplayed\" and \"replicaUpdateFailedAt\" attributes: " + me2.toString());
	    }
        }
            
        try {
            mods = new LDAPModificationSet();
            attr = new LDAPAttribute(attrname, "*");
            mods.add(LDAPModification.DELETE, attr);
            updateEntry(mods);
        } catch (LDAPException me3) {
			Debug.println("Unable to remove value \"*\" from attribute " + attrname + ": " + me3.toString());
        }
    }
    
    
    /**
     * Cause online replica creation to begin for this agreement
	 * @return Status ot ORCTask
     */
    public int initializeConsumer() {
        String title = "";
        String msg = "";
        
        if (this.agreementType == AGREEMENT_TYPE_AD) {
            title = _resource.getString("sync-node-orc", "title");
            msg   = _resource.getString("sync-node-orc",
										   "description");
        } else {
    		title = _resource.getString("replication-node-orc", "title");
    		msg   = _resource.getString("replication-node-orc",
    										   "description");
        }
		SimpleProgressDialog dlgORC;
		ORCTask taskORC;
        
		taskORC = new ORCTask(this);

		IDSModel model = (IDSModel)_serverInfo.get( "dsresmodel" );
		dlgORC = new SimpleProgressDialog (model.getFrame(), taskORC,
										   title, msg);
		taskORC.setProgressDialog (dlgORC);
        dlgORC.setModal(true);
		dlgORC.setLocationRelativeTo(model.getFrame());
        dlgORC.setVisible(true);
				
		if (taskORC.getStatus() == 0) {
			/* No problem launching initialization => we only display a message if it was not cancelled */
			if (!dlgORC.isCancelled()) {
				/* If we had no problems launching the initialization, we check what happened
				   with the first update */
				if (taskORC.getFirstUpdateStatus() == 0) {
					if (getStatusByKeyword(ReplicationTool.REPLICA_REFRESH_ATTR).equals(ReplicationTool.REPLICA_CONSUMER_INIT_IN_PROGRESS)) {
						/* The initialization has been started, we have the first successful
						   result, but it has not finished ...*/
						DSUtil.showInformationDialog(model.getFrame(),
													 "initconsumer-not-finished",
													 (String[]) null,
													 "replication-agreement",
													 _resource);
					} else {
						/* The initialization has been started, we have the first successful
						   result, and it is finished ...*/
						DSUtil.showInformationDialog(model.getFrame(),
													 "initconsumer-finished",
													 (String[]) null,
													 "replication-agreement",
													 _resource);
					}
				} else {
					/* We get the error message from the first update */
					String[] args = {taskORC.getFirstUpdateMessage()};
					if (getStatusByKeyword(ReplicationTool.REPLICA_REFRESH_ATTR).equals(ReplicationTool.REPLICA_CONSUMER_INIT_IN_PROGRESS)) {
						/* The initialization has been started, the first result is no good 
						   but the initialization has not finished ...*/
						DSUtil.showErrorDialog(model.getFrame(),
											   "initconsumer-error-not-finished",
											   args,
											   "replication-agreement",
											   _resource);
					} else {
						/* The initialization has been started,  the first result is no good 
						   and the initialization is finished ...*/
						DSUtil.showErrorDialog(model.getFrame(),
											   "initconsumer-error-finished",
											   args,
											   "replication-agreement",
											   _resource);
					}
				}
			}
		} else {
			if (!dlgORC.isCancelled()) {
				/* An exception was thrown inside the taskORC. We have the ldap error code in the status of the task
				   and the error sent by the server in the firstupdatemessage */
				String ldapError = LDAPException.errorCodeToString(taskORC.getStatus());
				String ldapMessage = taskORC.getFirstUpdateMessage();
				
				if ((ldapMessage != null) &&
					(ldapMessage.length() > 0)) {
					ldapError = ldapError + ". "+ldapMessage;
				} 
				String[] args = {ldapError};
				DSUtil.showErrorDialog(model.getFrame(),
									   "initconsumer-local-error",
									   args,
									   "replication-agreement",
									   _resource);
			} else {
				/* An exception was thrown inside the taskORC and we tried to cancel the task.
				   We have the ldap error code in the status of the task and the error sent by the server in the firstupdatemessage */
				String ldapError = LDAPException.errorCodeToString(taskORC.getStatus());
				String ldapMessage = taskORC.getFirstUpdateMessage();
				
				if ((ldapMessage != null) &&
					(ldapMessage.length() > 0)) {
					ldapError = ldapError + ". "+ldapMessage;
				} 
				String[] args = {ldapError};
				DSUtil.showErrorDialog(model.getFrame(),
									   "initconsumer-stop-local-error",
									   args,
									   "replication-agreement",
									   _resource);
			}
		}	
		return taskORC.getStatus();
    }

	/**
	 * Create an LDIF file with data to initialize the consumer
	 *
	 * @param fileName Name of file for consumer data
	 * @return true if the file creation succeeded
	 */
	public boolean populateLDIFFile (String fileName) {
		/* Check if the file exists and show a warning message */
		File file = new File(fileName);
		if (DSUtil.fileExists(file)) {
			JFrame frame = UtilConsoleGlobals.getActivatedFrame();
			int response = DSUtil.showConfirmationDialog(frame,
														 "confirm-delete-export-file",
														 DSUtil.inverseAbreviateString(fileName, 30),
														 "export");
			if ( response != JOptionPane.YES_OPTION ) {
				return false;
			}
		}
		String str = getDataVersion();
		LDAPConnection ld = _serverInfo.getLDAPConnection();
		boolean status = false;
		boolean wasReadOnly = false;
		// RM : TBD : We must add the database dn
		String backendName = MappingUtils.getBackendForSuffix(_serverInfo.getLDAPConnection(), replicatedSubtree);
		if (backendName == null) {
			return false;
		}
		String dnDB = "cn="+backendName+"," + DSUtil.LDBM_BASE_DN;	
		/* Get read-only state of database */
		
		try {			
			wasReadOnly = ReadOnly.isReadOnly( _serverInfo, dnDB );
			status = true;
		} catch ( LDAPException e ) {
			Debug.println("ReplicationAgreement.populateLDIFFile() (1) "+e);
		}
		if ( status && !wasReadOnly ) {			
			/* Make server read-only */
			status = ReadOnly.setReadOnly( _serverInfo, true, dnDB );
		}
		if ( status ) {			
			Hashtable attributes = new Hashtable();
			
			attributes.put(LDAPExport.FILENAME, new LDAPAttribute(LDAPExport.FILENAME, fileName));
			attributes.put(LDAPExport.USE_ONE_FILE, new LDAPAttribute(LDAPExport.USE_ONE_FILE, "TRUE"));
			attributes.put(LDAPExport.INSTANCE, new LDAPAttribute(LDAPExport.INSTANCE, backendName));
			attributes.put(LDAPExport.EXPORT_REPLICA, new LDAPAttribute(LDAPExport.EXPORT_REPLICA, "TRUE"));

			IDSModel model = (IDSModel)_serverInfo.get( "dsresmodel" );	

			LDAPExport task = new LDAPExport(model, attributes);
		}
		if ( !wasReadOnly ) {			
			ReadOnly.setReadOnly( _serverInfo, false, dnDB );
		}
		return status;
	}


	/**
	 * Create an LDIF file with data to initialize the consumer
	 *
	 * @param fileName Name of file for consumer data
	 * @return true if the file creation succeeded
	 */
	public boolean populateLDIFFile (IDSModel model) {		
        String str = getDataVersion();
        LDAPConnection ld = model.getServerInfo().getLDAPConnection();
		boolean status = false;
		boolean wasReadOnly = false;
		// RM : TBD : We must add the database dn
		String backendName = MappingUtils.getBackendForSuffix(ld, replicatedSubtree);
		if (backendName == null) {
			return false;
		} 
		String dnDB = "cn="+backendName+"," + DSUtil.LDBM_BASE_DN;		
		/* Get read-only state of database */
		try {			
			wasReadOnly = ReadOnly.isReadOnly( model.getServerInfo(), dnDB );
			status = true;
		} catch ( LDAPException e ) {
			Debug.println("ReplicationAgreement.populateLDIFFile() (1) "+e);
		}
		if ( status && !wasReadOnly ) {			
			/* Make server read-only */
			status = ReadOnly.setReadOnly( model.getServerInfo(), true, dnDB );
		}
		if ( status ) {			
			DatabaseExportPanel child = new DatabaseExportPanel( model, backendName, true);			
			String[] args = {backendName};
			String title = _resource.getString("replication-export", "title", args);
			SimpleDialog dlg = new SimpleDialog( model.getFrame(),
												 title,
												 SimpleDialog.OK |
												 SimpleDialog.CANCEL |
												 SimpleDialog.HELP,
												 child);
			dlg.setComponent( child );
			dlg.setOKButtonEnabled( false );
			dlg.setDefaultButton( SimpleDialog.OK );
			dlg.packAndShow();
		}
		if ( !wasReadOnly ) {			
			ReadOnly.setReadOnly( model.getServerInfo(), false, dnDB );
		}
		return status;
	}

	void waitForORCCompletion (String attrname) throws LDAPException {
		boolean               done=false;
		LDAPSearchResults     results = null;
		String                filter="objectclass=*";
		LDAPConnection        ldc = _serverInfo.getLDAPConnection ();
		LDAPConnection        ldc_clone = (LDAPConnection)ldc.clone ();   
		LDAPSearchConstraints cons=ldc_clone.getSearchConstraints();
		LDAPEntry             entry;
		String                value;
		String[]              attrs;
		LDAPSearchResults     result;
		LDAPPersistSearchControl control;

		attrs = new String [1];
		attrs[0] = attrname;

		/* return intermidiate results */
        cons.setBatchSize(1);

		/* setup persistent search control to be notified when 
           start ORC is deleted                                */
        control = new LDAPPersistSearchControl(
                       LDAPPersistSearchControl.MODIFY, false, false, false);
        cons.setServerControls(control);

        result = ldc_clone.search(entryDN, ldc_clone.SCOPE_BASE,
								  filter, attrs, false, cons);

		while (!done && result.hasMoreElements ()) {
			entry = (LDAPEntry)result.nextElement ();

			value = DSUtil.getAttrValue (entry, attrname);

			if (!value.equalsIgnoreCase (ReplicationTool.REPLICA_CONSUMER_INIT_IN_PROGRESS))
				done = true;			
		}

		/* stop persistent serach */
		ldc_clone.abandon (result);
		ldc_clone.disconnect ();
	}

    /**
	 * Get the copiedFrom attribute for a particular subtree
	 *
	 * @param subtree Directory subtree
	 * @return The copiedFrom attribute
	 */
    public static String getCopiedFrom(LDAPConnection ld, String subtree) {
        String cfattr[] = { "copiedFrom" };
        String cf = null;
        try {
            LDAPEntry entry = ld.read(subtree, cfattr);
            if (entry != null) {
			    LDAPAttribute attr = entry.getAttribute( "copiedFrom");
                if ( attr != null ) {
                    Enumeration vals = attr.getStringValues();
                    cf = (String)vals.nextElement();
                }
            }
        } catch (LDAPException le) {
        }
        return cf;
    }    
    
    /**
     * Check to see if the consumer needs to be reinitialized based on
     * values changed.
	 *
	 * @return true if the consumer needs to be reinitialized
     */
    public boolean checkForReORC() {
		Debug.print("ReplicationAgreement.checkforReOrc: ");
        if (agreementIsNew) {
            needsReORC = false;
            Debug.println("agreement is new");
            return needsReORC;
        }
		// This method could return true after the first needsReOrc,
		// but it seems to continue for debugging purposes

        // Check if replicatedSubtree has changed
        Debug.println(replicatedSubtree + " " + origReplicatedSubtree);
        if (!stringsEqualIgnoreCase(replicatedSubtree,
			                        origReplicatedSubtree)) {
            Debug.println("replicatedSubtree changed");
            needsReORC = true;
        }
		// Check if consumerPort has changed
		if (consumerPort != origConsumerPort) {
			Debug.println("consumerPort changed");
			needsReORC = true;
		}
		// Check if consumerHost has changed
		if (!stringsEqualIgnoreCase(consumerHost, origConsumerHost)) {
			Debug.println("consumerHost changed");
			needsReORC = true;
		}
		// Check if entryFilter has changed
		if (!stringsEqualIgnoreCase(entryFilter, origEntryFilter)) {
			Debug.println("entryFilter changed");
			needsReORC = true;
		}
		// Check if selattrType has changed
		if (selattrType != origSelattrType) {
			Debug.println("selattrType: (" + selattrType +
						  ") != (" + origSelattrType + ")");
			needsReORC = true;
		}
		// Check if selectedAttrs has changed
		// Note: depends on the Vectors being sorted!
		if (selectedAttrs == null) {
			if (origSelectedAttrs != null) {
				Debug.println("selectedAttrs changed");
				needsReORC = true;
			}
		} else {
			if (origSelectedAttrs == null) {
				Debug.println("selectedAttrs changed");
				needsReORC = true;
			} else {
				// Both Vectors non-null.  Are they the same length?
				if (selectedAttrs.size() != origSelectedAttrs.size()) {
					Debug.println("selectedAttrs changed");
					needsReORC = true;
				} else {
					// Same length.  Compare all elements
					int max = selectedAttrs.size();
					int i = 0;
					while (i < max && !needsReORC) {
						String s1 = (String)selectedAttrs.elementAt(i);
						String s2 = (String)origSelectedAttrs.elementAt(i);
						if (!s1.equalsIgnoreCase(s2)) {
							Debug.println("selectedAttrs changed");
							needsReORC = true;
						}
						i++;
					}
				}
			}
		}
		if ( !needsReORC ) {
			Debug.println("no changes");
		}
        return needsReORC;
    }

   /**
     * Get replication status from Directory
     */
	public void updateAgreementFromServer() {
		LDAPConnection ldc = _serverInfo.getLDAPConnection();
        if (ldc == null) {
            return;
        }
		LDAPEntry entry = null;
		try {
			entry = ldc.read(getEntryDN());
		} catch (LDAPException e) {
			Debug.println("ReplicationAgreement.updateAgreementFromServer() "+e);
		}
		if (entry == null) {
			return;
		}

		/* Update Status */
		_status.clear();
		      String val = 
            DSUtil.getAttrValue(entry,
                                ReplicationTool.REPLICA_LAST_UPDATE_STATUS_ATTR);
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_LAST_UPDATE_STATUS_ATTR,
                        val);
        } else {
			Debug.println("ReplicationAgreement.updateAgreementFromServer: " +
						  "unable to read the replica status from " +
						  DSUtil.format(ldc));
		}

        val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_N_CHANGES_SENT_ATTR);
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_N_CHANGES_SENT_ATTR,
                        val);
        } else {
			Debug.println("ReplicationAgreement.updateAgreementFromServer: " +
						  "unable to read the replica number of changes from " +
						  DSUtil.format(ldc));
		}

        val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_LAST_UPDATE_START_ATTR);
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_LAST_UPDATE_START_ATTR,
                        val);
        } else {
			Debug.println("ReplicationAgreement.updateAgreementFromServer: " +
						  "unable to read the replica last update start from " +
						  DSUtil.format(ldc));
		}

        val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_LAST_UPDATE_END_ATTR);
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_LAST_UPDATE_END_ATTR,
                        val);
        } else {
			Debug.println("ReplicationAgreement.updateAgreementFromServer: " +
						  "unable to read the replica last update end from " +
						  DSUtil.format(ldc));
		}

        val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_UPDATE_IN_PROGRESS_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_UPDATE_IN_PROGRESS_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateAgreementFromServer: " +
						  "unable to read the replica in progress from " +
						  DSUtil.format(ldc));
		}

       val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_REFRESH_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_REFRESH_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateAgreementFromServer: " +
						  "unable to read the replica refresh attribute " +
						  DSUtil.format(ldc));
		}

		val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_CONSUMER_INIT_BEGIN_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_CONSUMER_INIT_BEGIN_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateAgreementFromServer: " +
						  "unable to read the consumer initialization begin attribute (" + 
						  ReplicationTool.REPLICA_CONSUMER_INIT_BEGIN_ATTR + ") "+
						  DSUtil.format(ldc));
		}		

		val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_CONSUMER_INIT_END_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_CONSUMER_INIT_END_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateAgreementFromServer: " +
						  "unable to read the consumer initialization end attribute (" + 
						  ReplicationTool.REPLICA_CONSUMER_INIT_END_ATTR + ") "+
						  DSUtil.format(ldc));
		}

		val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_CONSUMER_INIT_STATUS_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_CONSUMER_INIT_STATUS_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateAgreementFromServer: " +
						  "unable to read the consumer initialization status attribute (" + 
						  ReplicationTool.REPLICA_CONSUMER_INIT_STATUS_ATTR + ") "+
						  DSUtil.format(ldc));
		}



		nickname = DSUtil.getAttrValue(entry,
                                  ReplicationTool.MMR_NICKNAME_ATTR);

		_description = nickname;
		
		entryCN = DSUtil.getAttrValue(entry,
                                  ReplicationTool.MMR_NAME_ATTR);

		replicatedSubtree = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_ROOT_ATTR);
		origReplicatedSubtree = replicatedSubtree;

		String sConsumerPort = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_PORT_ATTR);
		try {
			int value = Integer.parseInt(sConsumerPort);
			consumerPort = value;
			origConsumerPort = consumerPort;
		} catch (Exception e) {
		}

		consumerHost = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_HOST_ATTR);
		


		supplierHost = ldc.getHost();
		origSupplierHost = origSupplierHost;
		
		supplierPort = ldc.getPort();
		origSupplierPort = supplierPort;

		bindDN = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_BINDDN_ATTR);

		bindCredentials = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_CRED_ATTR);

		setUseSSL(DSUtil.getAttrValue(entry,
				                  ReplicationTool.REPLICA_TRANSPORT_ATTR));
		setUseStartTLS(DSUtil.getAttrValue(entry,
				                  ReplicationTool.REPLICA_TRANSPORT_ATTR));

		setUseSSLAuth(DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_BINDMETHOD_ATTR));
		setUseGSSAPIAuth(DSUtil.getAttrValue(entry,
								  ReplicationTool.REPLICA_BINDMETHOD_ATTR));
		setUseDigestAuth(DSUtil.getAttrValue(entry,
				  				  ReplicationTool.REPLICA_BINDMETHOD_ATTR));

		String[] schedule = DSUtil.getAttrValues(entry,
                                  ReplicationTool.REPLICA_SCHEDULE_ATTR);
		
		Vector v = new Vector();
		if ((schedule != null) &&
			(schedule.length > 0)) {			
			for (int i=0; i<schedule.length; i++) {				
				v.addElement(schedule[i]);
			}			
		} else {
			v.addElement(new String(""));
		}
		setUpdateSchedule(v);
	}

    
    /**
     * Get replication status from Directory
     */
    public void updateReplicaStatus() {

        int lderr;
        String errmsg;

        LDAPConnection ld = _serverInfo.getLDAPConnection();
        if (ld == null) {
            return;
        }
        
        LDAPEntry entry = null;
        _status.clear();
        // Get the root DSE status attributes
        try {
            entry = ld.read(getEntryDN(), ReplicationTool.REPLICA_STATUS_ATTRS);			
        } catch (LDAPException le2) {
            Debug.println("ReplicationAgreement.updateReplicaStatus: "+
			le2.toString());
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the replication status from DN " +
						  getEntryDN() + ": " + DSUtil.format(ld));
			return;
        }

        Debug.println(9, "ReplicationAgreement.updateReplicaStatus: status " +
                      "entry is " + entry.toString());
        String val = 
            DSUtil.getAttrValue(entry,
                                ReplicationTool.REPLICA_LAST_UPDATE_STATUS_ATTR);
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_LAST_UPDATE_STATUS_ATTR,
                        val);
        } else {
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the replica status from " +
						  DSUtil.format(ld));
		}

        val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_N_CHANGES_SENT_ATTR);
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_N_CHANGES_SENT_ATTR,
                        val);
        } else {
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the replica number of changes from " +
						  DSUtil.format(ld));
		}

        val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_LAST_UPDATE_START_ATTR);
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_LAST_UPDATE_START_ATTR,
                        val);
        } else {
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the replica last update start from " +
						  DSUtil.format(ld));
		}

        val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_LAST_UPDATE_END_ATTR);
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_LAST_UPDATE_END_ATTR,
                        val);
        } else {
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the replica last update end from " +
						  DSUtil.format(ld));
		}

        val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_UPDATE_IN_PROGRESS_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_UPDATE_IN_PROGRESS_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the replica in progress from " +
						  DSUtil.format(ld));
		}

       val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_REFRESH_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_REFRESH_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the replica refresh attribute " +
						  DSUtil.format(ld));
		}

		val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_CONSUMER_INIT_BEGIN_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_CONSUMER_INIT_BEGIN_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the consumer initialization begin attribute (" + 
						  ReplicationTool.REPLICA_CONSUMER_INIT_BEGIN_ATTR + ") "+
						  DSUtil.format(ld));
		}		

		val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_CONSUMER_INIT_END_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_CONSUMER_INIT_END_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the consumer initialization end attribute (" + 
						  ReplicationTool.REPLICA_CONSUMER_INIT_END_ATTR + ") "+
						  DSUtil.format(ld));
		}

		val = DSUtil.getAttrValue(entry,
                                  ReplicationTool.REPLICA_CONSUMER_INIT_STATUS_ATTR);		
        if (val != null && val.length() > 0) {
            _status.put(ReplicationTool.REPLICA_CONSUMER_INIT_STATUS_ATTR,
                        val);			
        } else {
			Debug.println("ReplicationAgreement.updateReplicaStatus: " +
						  "unable to read the consumer initialization status attribute (" + 
						  ReplicationTool.REPLICA_CONSUMER_INIT_STATUS_ATTR + ") "+
						  DSUtil.format(ld));
		}
		
    }
    
    private String getStatusByKeyword(String keyword) {
        if (_status != null && _status.containsKey(keyword)) {
            return _status.get(keyword).toString();
        }
        return _resource.getString("replication-info-status-code",
                                   "unknown");
    }

    public String getLastUpdateStatus() {
        return getStatusByKeyword(ReplicationTool.REPLICA_LAST_UPDATE_STATUS_ATTR);
    }
    
    public String getInProgress() {
        return getStatusByKeyword(ReplicationTool.REPLICA_UPDATE_IN_PROGRESS_ATTR);
    }
    
    public String getNChangesLast() {
        return getStatusByKeyword(ReplicationTool.REPLICA_N_CHANGES_SENT_ATTR);
    }
    
    public String getLastUpdateBegin() {
        String sdt = getStatusByKeyword(ReplicationTool.REPLICA_LAST_UPDATE_START_ATTR);
		
		/* In this particular case, the date is not available */
		if (sdt.equals("0")) {
			return _resource.getString("replication-agreement", "lastupdatebegin-unknown-value");
		}
		
        Date dt = DSUtil.getDateTime(sdt);
        if (dt==null)
            return "null"; //XXX
        return dt.toString();
    }
    
    public String getLastUpdateEnd() {
        String sdt = getStatusByKeyword(ReplicationTool.REPLICA_LAST_UPDATE_END_ATTR);

		/* In this particular case, the date is not available */
		if (sdt.equals("0")) {
			return _resource.getString("replication-agreement", "lastupdateend-unknown-value");
		}

        Date dt = DSUtil.getDateTime(sdt);
            if (dt==null)
            return "null"; //XXX
        return dt.toString();
    }
    
	public String getConsumerInitializationBegin() {
		String sdt = getStatusByKeyword(ReplicationTool.REPLICA_CONSUMER_INIT_BEGIN_ATTR);
		
		/* In this particular case, the date is not available */
		if ((sdt.equals(_resource.getString("replication-info-status-code",
											"unknown"))) ||
			(sdt.equals("0"))) {			
			return _resource.getString("replication-agreement", "initconsumer-begin-unknown-value");		
		}
		
        Date dt = DSUtil.getDateTime(sdt);
            if (dt==null)
            return "null"; //XXX
        return dt.toString();
	}

	public String getConsumerInitializationEnd() {
		String sdt = getStatusByKeyword(ReplicationTool.REPLICA_CONSUMER_INIT_END_ATTR);
		
		/* In this particular case, the date is not available */
		if ((sdt.equals(_resource.getString("replication-info-status-code",
											"unknown"))) ||
			(sdt.equals("0"))) {
			return _resource.getString("replication-agreement", "initconsumer-end-unknown-value");
		}
		
		Date dt = DSUtil.getDateTime(sdt);
        if (dt==null)
            return "null"; //XXX
        return dt.toString();
	}

	public String getConsumerInitializationStatus() {
		String sdt = getStatusByKeyword(ReplicationTool.REPLICA_CONSUMER_INIT_STATUS_ATTR);		
		return sdt;
	}

	public String getConsumerInitializationInProgress() {
		String sdt = getStatusByKeyword(ReplicationTool.REPLICA_REFRESH_ATTR);
		if ((sdt == null) ||
			!sdt.equals(ReplicationTool.REPLICA_CONSUMER_INIT_IN_PROGRESS)) {
			return _resource.getString("replication-agreement", "initconsumer-inprogress-false-value");
		} else {
			return _resource.getString("replication-agreement", "initconsumer-inprogress-true-value");
		}
	}


	/*==========================================================
	 * protected methods
     *==========================================================*/  
   
    protected void parseReplAttrList(LDAPAttribute attr) {
        // Note: we only support a single replicatedAttributeList attribute,
	    // and that attribute must give a filter of "(objectclass=*)".
	    //If we encounter
        // multiple attributes, or a different filter, then warn the user.
        boolean attrParsed = false;

        if (attr.size() == 0) {
            selattrType = SELATTR_ALL;
            return;
        }

        if (attr.size() > 2) {
            selattrNotRepresentable = true;
            selattrType = SELATTR_ALL;
            return;
        }

        String selAttrFilter = (String)attr.getStringValues().nextElement();
        if (selAttrFilter == null || selAttrFilter.length() == 0) {
            // no value in entry
            selectedAttrs = null;
            selattrType = SELATTR_ALL;
        } else {
            StringTokenizer st =
			new StringTokenizer(selAttrFilter, "$", false);
            if (st.hasMoreTokens()) {
                String s = st.nextToken().trim();
                if (!"(objectclass=*)".equalsIgnoreCase(s)) {
                    selattrNotRepresentable = true;
                    return;
                }
            } else {
                selattrNotRepresentable = true;
                return;
            }
            if (st.hasMoreTokens()) {
                String s = st.nextToken();
                s.trim();
                st = new StringTokenizer(s, " ", false);
                if (st.hasMoreTokens()) {
                    String incOrExc = st.nextToken();
                    if ("include".equalsIgnoreCase(incOrExc)) {
                        selattrType = SELATTR_INCLUDE;
                    } else if ("exclude".equalsIgnoreCase(incOrExc)) {
                        selattrType = SELATTR_EXCLUDE;
                    } else {
                        selattrType = 0;
                    }
                    String attrString = "";
                    while (st.hasMoreTokens()) {
                        attrString += st.nextToken() + " ";
                    }
                    setSelectedAttributes(attrString);
                    attrParsed = true;
                }
            }
            if (!attrParsed) {
                selattrNotRepresentable = true;
            }
        }
    }

    protected String createReplicatedAttributesList() {
        if (selectedAttrs == null || selectedAttrs.size() == 0) {
            return null;
        }

        String attrList = "(objectclass=*) $ ";

        if (selattrType == SELATTR_INCLUDE) {
            attrList += "INCLUDE";
        } else if (selattrType == SELATTR_EXCLUDE) {
            attrList += "EXCLUDE";
        } else {
            return null;
        }

        Enumeration selAttrsEnum = selectedAttrs.elements();
        while (selAttrsEnum.hasMoreElements()) {
            attrList += " " + (String)selAttrsEnum.nextElement();
        }
        return attrList;
    }

    void updateEntry(LDAPModificationSet mods) throws LDAPException {
        if (mods == null) {
            return;
        }
	modsDump( mods );
        LDAPConnection ld = _serverInfo.getLDAPConnection();
		Debug.println( "Replication.updateEntry: " + entryDN );
        ld.modify(entryDN, mods);
    }



    protected void createNewEntry(LDAPEntry e) throws LDAPException {
        if (e == null) {
            return;
        }
        LDAPConnection ld = _serverInfo.getLDAPConnection();
		Debug.println( "Replication.createNewEntry: " + e.getDN() );
        ld.add(e);
    }



    protected void deleteOldEntry() throws LDAPException {
        LDAPConnection ld = _serverInfo.getLDAPConnection();
		Debug.println( "Replication.deleteOldEntry: " + getOrigEntryDN() );
        ld.delete(getOrigEntryDN());
    }


	protected void disconnect(LDAPConnection ld) {
		if (ld != null) {
			try {
				ld.disconnect();
			} catch ( LDAPException e ) {
			}
		}
	}

    /*==========================================================
	 * private methods
	 *==========================================================*/
	 
    private boolean isSpecialNamingContext(String nc) {
        return false;
    }

    // Compare 2 strings.  One or both can be null
    private boolean stringsEqualIgnoreCase(String s1, String s2) {
        boolean ret = false;
        
        if (s1 == null) {
            if (s2 == null) {
                ret = true;
            } else {
                ret = false;
            }
        } else {
            if (s2 == null) {
                ret = false;
            } else {
                if (s1.length() != s2.length()) {
                    ret = false;
                } else {
                    ret = s1.equalsIgnoreCase(s2);
                }
            }
        }
        return ret;
    }
    
    //convert General Time String to Date String
    static private String timeToString(String gt) {
        Calendar cal = Calendar.getInstance();
        try {
            cal.set(Integer.parseInt(gt.substring(0,4)),
                Integer.parseInt(gt.substring(4,6)) - 1,
                Integer.parseInt(gt.substring(6,8)),
                Integer.parseInt(gt.substring(8,10)),
                Integer.parseInt(gt.substring(10,12)),
                Integer.parseInt(gt.substring(12,14)));
        } catch (NumberFormatException e) {
            return " ";    
        }
        Date date = cal.getTime();
        DateFormat formatter =
		    DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM);
        return formatter.format(date);
    }


    /*==========================================================
	 * Debug methods
	 *==========================================================*/
	 
    public String toString() {
        String ret = "";
        ret += "Type: ";
        if (agreementType == AGREEMENT_TYPE_MMR) {
            ret += "Multi Master";
        } else if (agreementType == AGREEMENT_TYPE_LEGACYR) {
            ret += "Legacy";
        } else if (agreementType == AGREEMENT_TYPE_AD) {
            ret += "Windows";
        } else {
            ret += "(unknown)";
        }
        ret += "\n";
        ret += "Host: " + getRemoteHost() + "\n";
        ret += "Port: " + getRemotePort() + "\n";
        ret += "Subtree: " + replicatedSubtree + "\n";
        ret += "SSL: " + useSSL + "\n";
        ret += "TLS: " + useStartTLS + "\n";
        ret += "SSL Client Auth: " + useSSLAuth + "\n";
        ret += "SASL/GSSAPI Auth: " + useGSSAPIAuth + "\n";
        ret += "SASL/Digest-MD5 Auth: " + useDigestAuth + "\n";

        return ret;
    }



    /**
     * Dump out a mods set for debugging
     */
    private void modsDump(LDAPModificationSet mods) {
        Debug.println("ReplicationAgreement.modsDump:");
        if (mods == null) {
            Debug.println("  null");
        } else {
            LDAPModification lm;
            int op;
            LDAPAttribute attr;
            Enumeration valuesEnum;
            for (int i = 0; i < mods.size(); i++) {
                lm = mods.elementAt(i);
                op = lm.getOp();
                switch (op) {
                case LDAPModification.ADD:
                    Debug.print("add");
                    break;
                case LDAPModification.DELETE:
                    Debug.print("delete");
                    break;
                case LDAPModification.REPLACE:
                    Debug.print("replace");
                    break;
                default:
                    Debug.print("unknown op (" + op + "): ");
                }
                Debug.print(": ");
                attr = lm.getAttribute();
                Debug.println(attr.getName());
                valuesEnum = attr.getStringValues();
                while ( (valuesEnum != null) &&
						valuesEnum.hasMoreElements()) {
                    Debug.println(attr.getName() + ": " +
					(String)valuesEnum.nextElement());
                }
                Debug.println("-");
            }
        }
    }

	private String getDataVersion() {
		String baseDN = "cn=monitor";
		String filter = "objectclass=*";
		String[] attrs = {"dataversion"};
		String result;
        LDAPConnection ld = _serverInfo.getLDAPConnection();
        try {
            LDAPSearchResults res = ld.search(baseDN, ld.SCOPE_BASE, 
                filter, attrs, false);
            LDAPEntry entry = res.next();
			if (entry != null) {
				LDAPAttribute attr = entry.getAttribute(attrs[0]);
				if (attr != null) {
					Enumeration enums = attr.getStringValues();
					result = (String)enums.nextElement();
					Debug.println ("ReplicationAgreement.getDataversion: " + result);
					return result;
				}
			}
        } catch (LDAPException e) {
			Debug.println("ReplicationAgreement.getDataversion: " + e );   
        }
        return "";
    }

  /**
   * Getter for property domain.
   * @return Value of windows domain (if applicable)
   */
  public String getDomain() {
      return domain;
  }  

  /**
   * Setter for property domain.
   * @param domain New value of property domain.
   */
  public void setDomain(String domain) {
      this.domain = domain;
  }
  
    //==========================================================
    // UPDATED FUNCTIONALITY MOVED TO ReplicationTool.java
    //
    //      private String canonicalHost(String host)
    //      protected String fullyQualifyHostName(String host)
    //==========================================================
       
    /*==========================================================
     * variables
     *==========================================================*/    
    public static final int AGREEMENT_TYPE_LEGACYR = 1;
    public static final int AGREEMENT_TYPE_MMR = 2;
    public static final int AGREEMENT_TYPE_CIR = 3;
    public static final int AGREEMENT_TYPE_SIR = 4;
    
    public static final int AGREEMENT_TYPE_AD = 5; //Active Directory
    public static final int AGREEMENT_TYPE_NT = 6; //legacy nt replication (not used)
    
    protected static final int CLOSE_AGREEMENT = 1;
    protected static final int KEEP_AGREEMENT  = 2;

    protected String nickname;                      // A short, human-readable nickname for this agreement.
    protected String entryDN;                       // The DN of the entry containing this agreement.
    protected String origEntryDN;                   // DN of entry at time it was read from directory
    protected String entryCN;                       // CN of the entry containing this agreement.
    protected String replicatedSubtree;             // The DN of the top of the replicated subtree.
    protected String supplierHost;                  // The host name for the supplier server.
    protected int supplierPort;                     // The port number of the supplier server process.
    protected String consumerHost;                  // The host name of the consumer server
    protected int consumerPort;                     // The post number of the consumer server process.
    protected String bindDN;                        // The DN to bind as when connecting to the consumer. XXX LDAPDN?
    protected String bindCredentials;               // The credentials (password) used when connectin to the consumer.
    protected Vector updateSchedule;                // Schedule(s) for updates
    //protected ReplicationAgreementEditor editor;           // Reference to repl agrmt editor window.  If null, no window is open
    protected boolean useSSL;                       // true if SSL should be used when connecting to remote server
    protected boolean useStartTLS;                  // true if startTLS should be used when connecting to remote server
    protected boolean useSSLAuth;		            // true if useSSL and strong auth by SSL required
    protected boolean useGSSAPIAuth;		        // true if using SASL/GSSAPI for auth
    protected boolean useDigestAuth;		        // true if using SASL/Digest-MD5 for auth
    protected int agreementType;                    // mmr or legacyr
    //protected Vector namingContexts;                // Naming contexts supported by the server
    protected String orcValue;                      // non-null if replica is being reinitialized
    protected Vector selectedAttrs;                 // Attribute names for selective attribute replication
    protected int selattrType;                      // Either SELATTR_INCLUDE or SELATTR_EXCLUDE
    protected boolean selattrNotRepresentable;      // true if the selective attribute repl info found in
                                                    // config entry cannot be represented by this UI.
    protected String entryFilter;                   // LDAPFilter describing entries to replicate
    protected boolean filterNotRepresentable;       // true if fitlered repl info found in config entry
                                                    // cannot be represented by this UI.
    protected boolean agreementIsNew;               // true if this is a new agreement, e.g. not read from srvr
    protected AgreementTable repList;               // The other replication agreements (for validation)
        
    protected String origSupplierHost;              // Original host name
    protected int origSupplierPort;                 // Original supplier port number
    protected int origConsumerPort;                 // Original consumer port number.
    protected String origConsumerHost;              // Original consumer host name
    protected int origSelattrType;                  // Original type of selattr selection
    protected Vector origSelectedAttrs;             // Originally-selected attributes
    protected String origEntryFilter;               // Original entry filter
    protected String origReplicatedSubtree;         // Original replicated subtree.
    protected String machineDataDN;                 // Machine Data DN of the Directory Server
    protected String _description;
    private boolean needsReORC;                     // If true, a change has been made which requires re-initting the consumer
    private ConsoleInfo _serverInfo;
    
    // status
    private Hashtable _status = new Hashtable();
    
	//get resource bundle
    private static ResourceSet _resource =
	    new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");     
    
    /**
     * Holds value of property domain.
     */
    private String domain;
    
}

