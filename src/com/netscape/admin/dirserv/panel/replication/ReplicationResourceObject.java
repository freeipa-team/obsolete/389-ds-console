/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.util.Enumeration;
import java.util.Vector;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSContentListener;
import com.netscape.admin.dirserv.node.DSResourceObject;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.management.client.console.ConsoleInfo;
import netscape.ldap.*;

/**
 * Representation of the Replication Agreement Folder Node
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/14/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class ReplicationResourceObject extends DSResourceObject
                                       implements  ActionListener,	                                              
                                                   TreeExpansionListener,
                                                   IMenuInfo {
    
    /*==========================================================
     * constructors
     *==========================================================*/    
    private ReplicationResourceObject(IDSModel model) {
       this("",
			null,
			null,
			model);
    }

    private ReplicationResourceObject(String sDisplayName, IDSModel model) {
        this(sDisplayName,
			null,
			null,
			model);
    }

    public ReplicationResourceObject(String sDisplayName,
				     RemoteImage icon, RemoteImage largeIcon,
				     IDSModel model) {
        super(sDisplayName, 
			  icon,
			  largeIcon,
			  model);        
    }

    /*==========================================================
     * public methods
     *==========================================================*/

    public Component getCustomPanel() {
		if (_panel == null) {
			_panel = new ReplicationSettingPanel(_model);
		}
		return _panel;
    }

    /**
     *	Called when user wants to execute this object, invoked by a
     *  double-click or a menu action.
     */
    public boolean run(IPage viewInstance) {
		Debug.println("ReplicationResourceObject: run()");
		reload();
		if (super.getChildCount() != 0) {			
			expandPath((ResourcePage)viewInstance);
		}
		refreshTree();
		return true;
    }

    public boolean run(IPage viewInstance, IResourceObject selectionList[]) {
		return run( viewInstance );
    }
	
    /**
     * retrieve replica information from the replication server
     */
    public void reload() {
		cleanTree();
		_isLeaf = true;

		// set status bar in progress. Can't use framework status bar YET!!!
		// pop up in progress dialog status bar
		if (! ReplicationTool.authenticate(_model))
			return;
		// add Replica nodes
		addReplicaNodes();
    }

    /**
     * see if the replicas are loaded
     * @return true, if loaded
     */
    public boolean isLoaded() {
		return _isLoaded;
    }
													  
   /**
     * Override isLeaf so node will show up as expandable
     */
    public boolean isLeaf() {
		return _isLeaf;
    }

	/**
	 * Called when this object is selected.
     * Called by: ResourceModel
	 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		super.select( viewInstance );
	}

    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( "refresh" ) ) {
			if (! ReplicationTool.authenticate(_model))
				return;
			 refreshReplicationNode();
		}
    }

	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
		return new IMenuItem[]{			
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description")),
				};
    }

    public void actionMenuSelected(IPage viewInstance, IMenuItem item){
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((DSResourceModel)_model).actionMenuSelected(viewInstance, item);
		}
	}

	public void refreshReplicationNode() {
		reload();
		refreshTree();
	}
	
    /*==========================================================
     * private methods
     *==========================================================*/

    /**
     * Add the replica (i.e. backend instance) nodes
     */
    private void addReplicaNodes() {
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		String errorDetail = "LDAP server " + DSUtil.format(ldc);
 		try {
 			// first, get the list of LDBM backend instances
 			Vector ldbmInstanceList =
 				DSUtil.getLDBMInstanceList(ldc, true, null);
 			// add each instance to the tree
 			for (int ii = 0; ldbmInstanceList != null &&
 					 ii < ldbmInstanceList.size();
 				 ++ii) {
 				Vector v = (Vector)ldbmInstanceList.elementAt(ii);
 				LDAPEntry instEntry = (LDAPEntry)v.elementAt(0);
 				LDAPEntry mapTreeEntry = (LDAPEntry)v.elementAt(1);
				
 				add(new ReplicaResourceObject(_model, 
											  this,
 											  instEntry,
 											  mapTreeEntry));
 				_isLeaf = false;
 			} 			
 		} catch (LDAPException lde) {
 			Debug.println("Error: ReplicationResourceObject.addReplicaNodes:" +
 						  " could not " +
 						  "get the list of backend instances: detail: " +
 						  errorDetail + ": " + lde);
 		}
		refreshTree();
		_isLoaded = true;
    }						   
    

    /**
     * Refresh the tree view
     */
    void refreshTree() {
		_model.fireTreeStructureChanged(this);
    }
        
    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
        page.expandTreePath(path);
    }
    
    /**
     * Remove all agreements from the tree model and agreement table
     */
    private void cleanTree() {
		removeAllChildren();
    }

	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( equals( o ) ) {
			if ( !_isInitiallyExpanded ) {
				/* Prevent recursion caused by refreshTree() in reload() */
				_isInitiallyExpanded = true;
				Debug.println( "ReplicationResourceObject.treeExpanded: this" );
				if ( !isLoaded() ) {
					reload();
				}
			}
		}		
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {		
	}
    
    /*==========================================================
     * variables
     *==========================================================*/
	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;

    private boolean _isLoaded;
	private boolean _isInitiallyExpanded = false;
    private boolean _isLeaf = false;    
    Component _panel;
    
    //get resource bundle
    private static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");    

    static final String NEW     = "new";
    static final String REFRESH = "refreshagreements";
}
