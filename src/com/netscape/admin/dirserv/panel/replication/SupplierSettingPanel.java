/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;

/**
 * Panel for Directory Server Supplier Replication Setting
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class SupplierSettingPanel extends BlankPanel {
    
    /*==========================================================
     * constructors
     *==========================================================*/
     
    /**
     * Public Constructor
     * construction is delayed until selected.
     * @param parent parent panel
     */
	public SupplierSettingPanel(IDSModel model) {
	    super(model, "replication");
	    setTitle(_resource.getString(_section+"-setting","title"));
	    _model = model;
		_helpToken = "configuration-replication-suppliersettings-help";
		_refreshWhenSelect = false;
	}
	
	/*==========================================================
	 * public methods
     *==========================================================*/	
	
    //================== ITabPanel functions ===================
	
	/**
	 * Actual Panel construction
	 */
	public void init() {
		if (_isInitialized) {
			return;
		}
		// See if the changelog entry exists
		_clogExists = false;
		try {
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			LDAPEntry lde = ldc.read(CHANGELOG_DN);
			if (lde != null) {
				_clogExists = true;
				updateOriginalEntryValues(lde);
			}
		} catch (LDAPException lde) {
			_originalEntryValues = new Hashtable ();
			for (int i=0; i<ATTRIBUTES.length; i++) {
				_originalEntryValues.put(ATTRIBUTES[i], "");
			}
			_originalEntryValues.put(CHANGELOG_MAX_AGE_UNIT, 
									 String.valueOf(unitToChar(DEFAULT_AGE_UNIT)));
		}
		GridBagLayout gb = new GridBagLayout();
	    GridBagConstraints gbc = getGBC();
			
		_myPanel.setLayout(gb);
        _myPanel.setBackground(getBackground());
        _myPanel.setPreferredSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        _myPanel.setMaximumSize(ReplicationTool.DEFAULT_PANEL_SIZE);	    

		//add Description label
        JTextArea desc = makeMultiLineLabel( 2, 60,
										  _resource.getString(
											  _section,"setting-desc") );
		gbc.anchor = gbc.NORTHWEST;
        gbc.gridwidth = 2;
		gbc.weightx    = 1.0;
		gbc.fill = gbc.HORIZONTAL;
        _myPanel.add(desc, gbc);
		gbc.weightx    = 0.0;
		gbc.fill = gbc.HORIZONTAL;
		gbc.gridwidth = gbc.REMAINDER;
		_myPanel.add(Box.createGlue(),gbc);

		// Switch on/off
		_enableCheckbox = makeJCheckBox( _section,
										 "enable",
										 _clogExists,
										 _resource);	
		_enableCheckbox.setSelected(_clogExists);
		gbc.gridwidth = 2;
		gbc.weightx    = 0;
		gbc.fill = gbc.NONE;
		_myPanel.add(_enableCheckbox,gbc);
		gbc.weightx    = 1.0;
		gbc.fill = gbc.HORIZONTAL;
		gbc.gridwidth = gbc.REMAINDER;
		_myPanel.add(Box.createGlue(),gbc);	
        
        //add Changelog panel
		String title = _resource.getString(_section+"-changelog","label");
        JPanel normal = new GroupPanel(title);        
		ReplicationTool.resetGBC(gbc);
		gbc.insets = (Insets) ReplicationTool.BOTTOM_INSETS.clone();
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        _myPanel.add(normal, gbc);

        //add Changelog DB Directory		
        _dbLabel = makeJLabel(_section, "cLogDBDir", _resource);
		desc.setFont( _dbLabel.getFont() );
		ReplicationTool.resetGBC(gbc);
		gbc.insets = (Insets) ReplicationTool.BOTTOM_INSETS.clone();
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        normal.add(_dbLabel, gbc);

        _dbText= makeJTextField(_section, "cLogDBDir", _resource);
		_dbLabel.setLabelFor(_dbText);
		_dbText.getDocument().addDocumentListener(this);
		ReplicationTool.resetGBC(gbc);
		gbc.insets = (Insets) ReplicationTool.BOTTOM_INSETS.clone();
        gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 0.5;
        normal.add(_dbText, gbc);
		
		// Browse and Use default buttons

		// this is just an empty label to align buttons with text box

		_browseButton = UIFactory.makeJButton(this, "replication-supplier", "browseButton", _resource);
		_dbLabel.setLabelFor(_browseButton);
		_browseButton.setEnabled (isLocal());

        _defaultButton = UIFactory.makeJButton(this, "replication-supplier", "defaultButton", _resource);		
		_dbLabel.setLabelFor(_defaultButton);

		JButton[] buttons = {_browseButton, _defaultButton};
		JPanel buttonPanel = UIFactory.makeJButtonPanel( buttons );

		ReplicationTool.resetGBC(gbc);
		gbc.insets = (Insets) ReplicationTool.BOTTOM_INSETS.clone();
        gbc.gridwidth = _gbc.REMAINDER;
		gbc.fill = _gbc.HORIZONTAL;
		gbc.weightx = 1.0;
        normal.add(buttonPanel, gbc);

         //add Max Changelog Record
        _recordLabel = makeJLabel(_section, "cLogMaxRec", _resource);
		ReplicationTool.resetGBC(gbc);
		gbc.insets = (Insets) ReplicationTool.BOTTOM_INSETS.clone();
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        normal.add(_recordLabel, gbc);

        _recordText = makeNumericalJTextField(_section, "cLogMaxRec", _resource);
		_recordLabel.setLabelFor(_recordText);
		_recordText.getDocument().addDocumentListener(this);
		ReplicationTool.resetGBC(gbc);
		gbc.insets = (Insets) ReplicationTool.BOTTOM_INSETS.clone();
        gbc.gridwidth=1;
		gbc.weightx = 0.5;
        normal.add(_recordText, gbc);

		_recordCheckbox = makeJCheckBox (_resource.getString(
			              "replication-supplier-cUnlimited","label"));
		gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;	
		gbc.fill = gbc.NONE;
		normal.add(_recordCheckbox, gbc);
		gbc.fill = gbc.HORIZONTAL;
        
         //add Max Changelog Age
        _ageLabel = makeJLabel(_section, "cLogMaxAge", _resource);
		ReplicationTool.resetGBC(gbc);
		gbc.insets = (Insets) ReplicationTool.BOTTOM_INSETS.clone();
		gbc.gridwidth=1;
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        normal.add(_ageLabel, gbc);
        
        _ageText= makeNumericalJTextField(_section, "cLogMaxAge", _resource);
		_ageLabel.setLabelFor(_ageText);
		_ageText.getDocument().addDocumentListener(this);
		ReplicationTool.resetGBC(gbc);
		gbc.insets = (Insets) ReplicationTool.BOTTOM_INSETS.clone();
        gbc.gridwidth = 1;  
		gbc.weightx = 0.5;
        normal.add(_ageText, gbc);
      
        //add Time Unit
        _timeUnit = makeJComboBox(_section, "cLogMaxAgeUnit", null,
            _resource);
		_ageLabel.setLabelFor(_timeUnit);
		_timeUnit.addActionListener(this);
		ReplicationTool.resetGBC(gbc);
		gbc.insets = (Insets) ReplicationTool.BOTTOM_INSETS.clone();
        gbc.gridwidth = gbc.RELATIVE;  
        normal.add(_timeUnit, gbc);

		_ageCheckbox = makeJCheckBox (_resource.getString(
			           "replication-supplier-cUnlimited","label"));
		gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.NONE;
		normal.add(_ageCheckbox, gbc);
		gbc.fill = gbc.HORIZONTAL;
 
                 
		// No validation of attributes, because they are not necessarily
		// there
		LDAPEntry newEntry = null;
		if (!_clogExists) { // i.e. the entry exists
			// add the more well known attributes and values
			LDAPAttributeSet las = new LDAPAttributeSet();
			las.add(new LDAPAttribute("objectclass", DEFAULT_OBJECTCLASSES));
			las.add(new LDAPAttribute("cn", CHANGELOG_CN));
			newEntry = new LDAPEntry(CHANGELOG_DN, las);			
		}	

		updateFieldsFromOriginalValues();	
		validateFields();
        addBottomGlue ();

        _isInitialized =true;
	}

	/**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {		
        if (e.getSource().equals(_recordCheckbox)) {			
			validateFields();	
        } else if (e.getSource().equals(_ageCheckbox)) {			
			validateFields();
        } else if (e.getSource().equals(_browseButton)) {
			DisplayBrowseDialog ();			
			validateFields();
		}
		else if (e.getSource().equals(_defaultButton)) {
			SetDefaultDir ();			
			validateFields();
		} else if (e.getSource().equals(_timeUnit)) { 
			validateFields();
		} else if (e.getSource().equals(_enableCheckbox)) {			
			validateFields();
		}	
    }	

	private void DisplayBrowseDialog () {
		String dir = _dbText.getText();
		if ( File.separator.equals("\\") ) {
			dir = dir.replace( '/', '\\' );
		}
		dir = DSFileDialog.getDirectoryName( dir, true, this );
        if (dir != null) {
            _dbText.setText(dir);
		}
	}

	private void SetDefaultDir () {
                String dir = DSUtil.getDefaultChangelogPath( _model.getServerInfo() );
		_dbText.setText(dir);
	}

	private void RemoveChangelog () {
		// ask for confirmation
		if ( requiresConfirmation(
								  GlobalConstants.PREFERENCES_CONFIRM_MODIFY_CHANGELOG ) ) {
			int option = DSUtil.showConfirmationDialog(
													   SupplierSettingPanel.this,
													   "remove-change",
													   "",
													   _section,
													   _resource);
			if (option == JOptionPane.NO_OPTION) {
				return;
			}
		}
		try {
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			ldc.delete(CHANGELOG_DN);
			_clogExists = false;
			
			LDAPAttributeSet las = new LDAPAttributeSet();
			las.add(new LDAPAttribute("objectclass", DEFAULT_OBJECTCLASSES));
			las.add(new LDAPAttribute("cn", CHANGELOG_CN));
			LDAPEntry newEntry = new LDAPEntry(CHANGELOG_DN, las);
			updateOriginalEntryValues(newEntry);
			updateFieldsFromOriginalValues();
		} catch (LDAPException e) {
			Debug.println("SupplierSettingPanel.removeChangelog() "+e);
			/* If the changelog entry does not exists, it's already removed, and we don't display any error message */
			if (e.getLDAPResultCode() == LDAPException.NO_SUCH_OBJECT) {
				_clogExists = false;
				
				LDAPAttributeSet las = new LDAPAttributeSet();
				las.add(new LDAPAttribute("objectclass", DEFAULT_OBJECTCLASSES));
				las.add(new LDAPAttribute("cn", CHANGELOG_CN));
				LDAPEntry newEntry = new LDAPEntry(CHANGELOG_DN, las);
				updateOriginalEntryValues(newEntry);
				updateFieldsFromOriginalValues();
			} else {
				String ldapError =  e.errorCodeToString();
				String ldapMessage = e.getLDAPErrorMessage();
				if ((ldapMessage != null) &&
					(ldapMessage.length() > 0)) {
					ldapError = ldapError + ". "+ldapMessage;
				}
				String[] args_m = { ldapError };
				DSUtil.showErrorDialog( _model.getFrame(),
										"error-del-changelog",
										args_m,
										_section,
										_resource);
				validateFields();
				return;
			}
		}
		validateFields();
	}

    /**
     * Update on-screen data from Directory.
	 *
	 * Note: we overwrite the data that the user may have modified.  This is done in order to keep
	 * the coherency between the refresh behaviour of the different panels of the configuration tab.
     *
     **/
    public void refreshFromServer () {
		Debug.println("SupplierSettingPanel.refreshFromServer()");
		resetCallback();
	}


   /**
     * Update on-screen data from Directory.
     *
     **/
    public boolean refresh () {
		Debug.println("SupplierSettingPanel.refresh()");
		_clogExists = false;
		/* We reread from the server */
		try {
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			LDAPEntry lde = ldc.read(CHANGELOG_DN);
			if (lde != null) {
				_clogExists = true;
				updateOriginalEntryValues(lde);
			}
		} catch (LDAPException lde) {
			_originalEntryValues = new Hashtable ();
			for (int i=0; i<ATTRIBUTES.length; i++) {
				_originalEntryValues.put(ATTRIBUTES[i], "");
			}
			_originalEntryValues.put(CHANGELOG_MAX_AGE_UNIT, 
									 String.valueOf(unitToChar(DEFAULT_AGE_UNIT)));
		}		
		validateFields();
		return true;
	}

	public void okCallback() {		
		if (!_isValid ||
			!_isDirty) {
			return;
		}
		if (_enableCheckbox.isSelected()) {
			if (confirmStore()) {
				super.okCallback ();
				saveEntry();			
			}
		} else if (_clogExists) {
			RemoveChangelog();
		}
	}

    public void resetCallback() {
		_clogExists = false;
		/* We reread from the server */
		try {
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			LDAPEntry lde = ldc.read(CHANGELOG_DN);
			if (lde != null) {
				_clogExists = true;
				updateOriginalEntryValues(lde);
			}
		} catch (LDAPException lde) {
			_originalEntryValues = new Hashtable ();
			for (int i=0; i<ATTRIBUTES.length; i++) {
				_originalEntryValues.put(ATTRIBUTES[i], "");
			}
			_originalEntryValues.put(CHANGELOG_MAX_AGE_UNIT, 
									 String.valueOf(unitToChar(DEFAULT_AGE_UNIT)));
		}
		updateFieldsFromOriginalValues();
		validateFields();
        clearDirtyFlag ();		
	}

	public void updateFieldsFromOriginalValues() {
		if (_originalEntryValues != null) {
			_enableCheckbox.setSelected(_clogExists);
			_dbText.setText((String)_originalEntryValues.get(CHANGELOG_DIR_ATTR_NAME));

			String maxRecord = (String)_originalEntryValues.get(CHANGELOG_MAX_RECORD_ATTR_NAME);		
			if (maxRecord.equals("0") || maxRecord.equals("")) {
				_recordCheckbox.setSelected(true);
				_recordText.setText("");
			} else {
				_recordCheckbox.setSelected(false);
				_recordText.setText(maxRecord);
			}
			enableRecord(!_recordCheckbox.isSelected());
		

			String maxAge = (String)_originalEntryValues.get(CHANGELOG_MAX_AGE_ATTR_NAME);
			if (maxAge.equals("0") || maxAge.equals("")) {
				_ageCheckbox.setSelected(true);
				_ageText.setText("");
			} else {
				_ageCheckbox.setSelected(false);
				_ageText.setText(maxAge);
			}
			enableAge(!_ageCheckbox.isSelected());
			
			String unitString = (String)_originalEntryValues.get(CHANGELOG_MAX_AGE_UNIT);
			if (unitString != null) {
				if (unitString.length() >0) {
					char unit = unitString.charAt(0);
					int unitInt = charToUnit(unit);
					if (unitInt >= 0) {
						_timeUnit.setSelectedIndex(charToUnit(unit));
					} else {
						_timeUnit.setSelectedIndex(DEFAULT_AGE_UNIT);
					}
				}
			}
		}
	}

	private void saveEntry() {
		try {
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			LDAPEntry entry = ldc.read(CHANGELOG_DN);
			_clogExists = true;
			LDAPModificationSet mods = new LDAPModificationSet();
			for (int i=0; i<ATTRIBUTES.length; i++) {				
				LDAPAttribute attr = entry.getAttribute(ATTRIBUTES[i]);
				String value = null;
				if (attr != null) {				
					Enumeration e = attr.getStringValues();
					if (e != null) {
						value = (String)e.nextElement();
					}
				} 
				if (value != null) {
					// The value exists: we have to do a REPLACE (DELETE is not supported)
					if (ATTRIBUTES[i].equals(CHANGELOG_DIR_ATTR_NAME)) {
						String dirValue = _dbText.getText().trim();
						if (!dirValue.equals(value)) {
							mods.add(LDAPModification.REPLACE,
									 new LDAPAttribute(CHANGELOG_DIR_ATTR_NAME, dirValue));
						}
					} else if (ATTRIBUTES[i].equals(CHANGELOG_MAX_RECORD_ATTR_NAME)) {
						if (_recordCheckbox.isSelected()) {
							if (!value.equals("0")) {
								mods.add(LDAPModification.REPLACE,
										 new LDAPAttribute(CHANGELOG_MAX_RECORD_ATTR_NAME, "0"));
							}
						} else {
							String recordValue = _recordText.getText().trim();
							if (!recordValue.equals(value)) {
								mods.add(LDAPModification.REPLACE,
										 new LDAPAttribute(CHANGELOG_MAX_RECORD_ATTR_NAME, recordValue));
							}
						}
					} else if (ATTRIBUTES[i].equals(CHANGELOG_MAX_AGE_ATTR_NAME)) {
						if (_ageCheckbox.isSelected()) {							
							if (!value.equals("0")) {
								mods.add(LDAPModification.REPLACE,
										 new LDAPAttribute(CHANGELOG_MAX_AGE_ATTR_NAME, "0"));
							}
						} else {
							String ageNumber = _ageText.getText().trim();
							String ageUnit = String.valueOf(
														unitToChar(_timeUnit.getSelectedIndex()));
							String ageValue = ageNumber+ageUnit;
							if (!ageValue.equals(value)) {
								mods.add(LDAPModification.REPLACE,
										 new LDAPAttribute(CHANGELOG_MAX_AGE_ATTR_NAME, ageValue));
							}
						}
					}
				} else {
					// The value does not exist: we have to (maybe) ADD it
					if (ATTRIBUTES[i].equals(CHANGELOG_DIR_ATTR_NAME)) {
						String dirValue = _dbText.getText().trim();
						if (!dirValue.equals("")) {
							mods.add(LDAPModification.ADD,
									 new LDAPAttribute(CHANGELOG_DIR_ATTR_NAME, dirValue));
						}
					} else if (ATTRIBUTES[i].equals(CHANGELOG_MAX_RECORD_ATTR_NAME)) {
						/* If the attribute CHANGELOG_MAX_RECORD_ATTR_NAME is not present => UNLIMITED */ 
						if (!_recordCheckbox.isSelected()) {	
							String recordValue = _recordText.getText().trim();
							if (!recordValue.equals("")) {
								mods.add(LDAPModification.ADD,
										 new LDAPAttribute(CHANGELOG_MAX_RECORD_ATTR_NAME, recordValue));
							}
						}
					} else if (ATTRIBUTES[i].equals(CHANGELOG_MAX_AGE_ATTR_NAME)) {
						/* If the attribute CHANGELOG_MAX_AGE_ATTR_NAME is not present => UNLIMITED */ 
						if (!_ageCheckbox.isSelected()) {	
							String ageNumber = _ageText.getText().trim();
							if (!ageNumber.equals("")) {
								String ageUnit = String.valueOf(
														unitToChar(_timeUnit.getSelectedIndex()));

								String ageValue = ageNumber+ageUnit;
								mods.add(LDAPModification.ADD,
										 new LDAPAttribute(CHANGELOG_MAX_AGE_ATTR_NAME, ageValue));
							}
						}
					}
				}
			}
			if (mods.size() > 0) {
				Debug.println("SupplierSettingsPanel.saveEntry(): "+ CHANGELOG_DN +": "+ mods);
				ldc.modify(CHANGELOG_DN, mods);								
				entry = ldc.read(CHANGELOG_DN);
				_clogExists = true;
				updateOriginalEntryValues(entry);
				updateFieldsFromOriginalValues();
				validateFields();
			}
		} catch (LDAPException e) {
			Debug.println("SupplierSettingsPanel.saveEntry(): " + e);
			if (e.getLDAPResultCode() == LDAPException.NO_SUCH_OBJECT) {
				_clogExists = false;
				LDAPAttributeSet las = new LDAPAttributeSet();
				las.add(new LDAPAttribute("objectclass", DEFAULT_OBJECTCLASSES));
				las.add(new LDAPAttribute("cn", CHANGELOG_CN));
				las.add(new LDAPAttribute(CHANGELOG_DIR_ATTR_NAME, _dbText.getText().trim()));
				if (!_recordCheckbox.isSelected()) {
					las.add(new LDAPAttribute(CHANGELOG_MAX_RECORD_ATTR_NAME, _recordText.getText().trim()));
				}
				if (!_ageCheckbox.isSelected()) {
					String ageNumber = _ageText.getText().trim();
					if (!ageNumber.equals("")) {
						String ageUnit = String.valueOf(
													unitToChar(_timeUnit.getSelectedIndex()));
						
						String ageValue = ageNumber+ageUnit;
						las.add(new LDAPAttribute(CHANGELOG_MAX_AGE_ATTR_NAME, ageValue));
					}
				}
				LDAPEntry newEntry = new LDAPEntry(CHANGELOG_DN, las);
				try {
					LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
					ldc.add(newEntry);
					LDAPEntry entry = ldc.read(CHANGELOG_DN);
					_clogExists = true;
					updateOriginalEntryValues(entry);
					updateFieldsFromOriginalValues();
					validateFields();
				} catch (LDAPException e2) {
					Debug.println("SupplierSettingsPanel.saveEntry() (2): " + e2);
					String ldapError =  e2.errorCodeToString();
					String ldapMessage = e2.getLDAPErrorMessage();
					if ((ldapMessage != null) &&
						(ldapMessage.length() > 0)) {
						ldapError = ldapError + ". "+ldapMessage;
					}
					String[] args_m = { ldapError };					
					DSUtil.showErrorDialog( _model.getFrame(),
											"error-add-changelog",
											args_m,
											_section,
											_resource);
					validateFields();
					return;
				}
			} else {
				String ldapError =  e.errorCodeToString();
				String ldapMessage = e.getLDAPErrorMessage();
				if ((ldapMessage != null) &&
					(ldapMessage.length() > 0)) {
					ldapError = ldapError + ". "+ldapMessage;
				}
				String[] args_m = { ldapError };
				DSUtil.showErrorDialog( _model.getFrame(),
										"error-mod-changelog",
										args_m,
										_section,
										_resource);
				validateFields();
				return;
			}
		}
	}

	public void changedUpdate(DocumentEvent e) {				
		validateFields();
    }

	public void removeUpdate(DocumentEvent e) {
		changedUpdate(e);
    }
	
    public void insertUpdate(DocumentEvent e) {
		changedUpdate(e);			
    }

	private void checkEnable() {
		boolean state = _enableCheckbox.isSelected();
		_dbLabel.setEnabled(state);
		_ageLabel.setEnabled(state);
		_recordLabel.setEnabled(state);		
		_dbText.setEnabled(state);
		
		enableRecord(!_recordCheckbox.isSelected() && state);
		enableAge(!_ageCheckbox.isSelected() && state);
		
		_recordCheckbox.setEnabled(state);
		_ageCheckbox.setEnabled(state);
		_browseButton.setEnabled(state);
		_defaultButton.setEnabled(state);
	}

	private void validateFields() {
		checkEnable();
		_isValid = true;
	    _isDirty = false;
		
		boolean isEnableDirty = false;

		if (!_enableCheckbox.isSelected()) {
			if (_clogExists) {
				_isDirty = true;
				setChangeState(_enableCheckbox, CHANGE_STATE_MODIFIED );
				setDirtyFlag();
			} else {
				setChangeState(_enableCheckbox, CHANGE_STATE_UNMODIFIED );
				clearDirtyFlag();
			}
			setValidFlag();
			return;
		} else {
			if (!_clogExists) {
				isEnableDirty = true;
				setChangeState(_enableCheckbox, CHANGE_STATE_MODIFIED );				
			} else {
				setChangeState(_enableCheckbox, CHANGE_STATE_UNMODIFIED );				
			}
		}

		boolean isDBValid = true;
		boolean isDBDirty = false;
		String dbString = _dbText.getText().trim();
		if (!dbString.equals((String)_originalEntryValues.get(CHANGELOG_DIR_ATTR_NAME))) {			
			isDBDirty = true;
		}
		if (dbString.equals("")) {
			isDBValid = false;
		}

		if (isDBDirty && isDBValid) {
			super.setChangeState((JComponent) _dbLabel, CHANGE_STATE_MODIFIED );
		} else if (!isDBDirty && isDBValid) {
			super.setChangeState((JComponent) _dbLabel, CHANGE_STATE_UNMODIFIED );
		} else if (!isDBValid) {
			super.setChangeState((JComponent) _dbLabel, CHANGE_STATE_ERROR );
		}
		
		_isValid = _isValid && isDBValid;
		_isDirty = _isDirty || isDBDirty || isEnableDirty;
		
		boolean isRecordValid = true;
		boolean isRecordDirty = false;
		String recordString = _recordText.getText().trim();
		String originalRecordString = (String)_originalEntryValues.get(CHANGELOG_MAX_RECORD_ATTR_NAME);

		if (!_recordCheckbox.isSelected()) {
			if (!recordString.equals(originalRecordString) ||
				recordString.equals("") ||
				recordString.equals("0")) {			
				isRecordDirty = true;
			}
			try {
				int value = Integer.parseInt(recordString);
				if ((value < 1) || (value > LIMIT_MAX_VAL)) {
					isRecordValid = false;
				}
			} catch (NumberFormatException e) {
				isRecordValid = false;
			}
		} else {			
			if (!originalRecordString.equals("") && 
				!originalRecordString.equals("0")) {
				isRecordDirty = true;
			}
		}
		if (isRecordDirty && isRecordValid) {
			super.setChangeState((JComponent) _recordLabel, CHANGE_STATE_MODIFIED );
		} else if (!isRecordDirty && isRecordValid) {
			super.setChangeState((JComponent) _recordLabel, CHANGE_STATE_UNMODIFIED );
		} else if (!isRecordValid) {
			super.setChangeState((JComponent) _recordLabel, CHANGE_STATE_ERROR );
		}		
		_isValid = _isValid && isRecordValid;
		_isDirty = _isDirty || isRecordDirty;

 		boolean isAgeValid = true;
		boolean isAgeDirty = false;
		String ageString = _ageText.getText().trim();
		String originalAgeString = (String)_originalEntryValues.get(CHANGELOG_MAX_AGE_ATTR_NAME);

		if (!_ageCheckbox.isSelected()) {
			String originalAgeUnitString = (String)_originalEntryValues.get(CHANGELOG_MAX_AGE_UNIT);
			String ageUnitString = String.valueOf(unitToChar(
															 _timeUnit.getSelectedIndex()));
			if (!originalAgeUnitString.equals(ageUnitString)) {
				isAgeDirty = true;
			}
			if (!ageString.equals(originalAgeString) ||
				ageString.equals("") ||
				ageString.equals("0")  ) {
				isAgeDirty = true;
			}
			try {
				int value = Integer.parseInt(ageString);
				if ((value < 1) || (value > LIMIT_MAX_VAL)) {
					isAgeValid = false;
				}
			} catch (NumberFormatException e) {
				isAgeValid = false;
			}
		} else {
			if (!originalAgeString.equals("") && 
				!originalAgeString.equals("0")) {
				isAgeDirty = true;
			}
		}				

		if (isAgeDirty && isAgeValid) {
			super.setChangeState((JComponent) _ageLabel, CHANGE_STATE_MODIFIED );
		} else if (!isAgeDirty && isAgeValid) {
			super.setChangeState((JComponent) _ageLabel, CHANGE_STATE_UNMODIFIED );
		} else if (!isAgeValid) {
			super.setChangeState((JComponent) _ageLabel, CHANGE_STATE_ERROR );
		}
		
		_isValid = _isValid && isAgeValid;
		_isDirty = _isDirty || isAgeDirty;


		if (_isDirty) {
			setDirtyFlag();
		} else {
			clearDirtyFlag();
		}
		if (_isValid) {
			setValidFlag();
		} else {
			clearValidFlag();
		}	   
	}

	private boolean confirmStore () {
		String text = _dbText.getText().trim();		
		/* Have to warn against changing the changelog db */
		String originalDb = (String)_originalEntryValues.get(CHANGELOG_DIR_ATTR_NAME);
		if ( !originalDb.equals("") &&
			 !originalDb.equals( text ) ) {
			int option = DSUtil.showConfirmationDialog(
													   SupplierSettingPanel.this,
													   "change-db",
													   "",
													   _section,
													   _resource);
			
			if (option == JOptionPane.NO_OPTION) {
				return false;
			}
		}
		
		return true;	
	}

	private void enableRecord (boolean enable) {
		_recordText.setEnabled(enable);
        _recordText.repaint();
	}

	private void enableAge (boolean enable) {
		_ageText.setEnabled(enable);
		_ageText.repaint();
		_timeUnit.setEnabled (enable);
		_timeUnit.repaint ();
	}

	private void updateOriginalEntryValues(LDAPEntry lde) {
		_originalEntryValues = new Hashtable();
		for (int i=0; i<ATTRIBUTES.length; i++) {				
			LDAPAttribute attr = lde.getAttribute(ATTRIBUTES[i]);
			String value = null;
			if (attr != null) {				
				Enumeration e = attr.getStringValues();
				if (e != null) {
					value = (String)e.nextElement();
				}
			} 
			if (value != null) {
				if (ATTRIBUTES[i].equals(CHANGELOG_MAX_AGE_ATTR_NAME)) {
					if (value.length() > 0) {
						char unit = value.charAt(value.length()-1);
						int unitVal = charToUnit(unit);
						if ( unitVal == -1) {
							unitVal = DEFAULT_AGE_UNIT;
						} else {
							// Strip off the unit specifier
							value = value.substring(0, value.length()-1);
							unit = unitToChar(unitVal);
						}
						int val = 0;
						try {
							val = Integer.parseInt(value);
						} catch (NumberFormatException nfe) {
							val = 0;
						}
						value = Integer.toString(val);
						_originalEntryValues.put(CHANGELOG_MAX_AGE_ATTR_NAME, value);
						_originalEntryValues.put(CHANGELOG_MAX_AGE_UNIT, 
									 String.valueOf(unit));
					} else {
						_originalEntryValues.put(CHANGELOG_MAX_AGE_ATTR_NAME, "");
						_originalEntryValues.put(CHANGELOG_MAX_AGE_UNIT, 
												 String.valueOf(unitToChar(DEFAULT_AGE_UNIT)));
					}
				} else {
					_originalEntryValues.put(ATTRIBUTES[i], value);
				}
			} else {
				_originalEntryValues.put(ATTRIBUTES[i], "");
				if (ATTRIBUTES[i].equals(CHANGELOG_MAX_AGE_ATTR_NAME)) {
					_originalEntryValues.put(CHANGELOG_MAX_AGE_UNIT, 
											 String.valueOf(unitToChar(DEFAULT_AGE_UNIT)));
				}
			}
		}		
	}

	static private int charToUnit(char unit) {
		int unitVal = -1;
		if (unit == 'h') {
			unitVal = HOUR_AGE_UNIT;
		} else if (unit == 's') {
			unitVal = SECOND_AGE_UNIT;
		} else if (unit == 'm') {
			unitVal = MINUTE_AGE_UNIT;
		} else if (unit == 'd') {
			unitVal = DAY_AGE_UNIT;
		} else if (unit == 'w') {
			unitVal = WEEK_AGE_UNIT;
		}
		return unitVal;
	}

	static private char unitToChar(int unitVal) {
		char unit = 0;
		if (unitVal == HOUR_AGE_UNIT) {
			unit = 'h';
		} else if (unitVal == SECOND_AGE_UNIT) {
			unit = 's';
		} else if (unitVal == MINUTE_AGE_UNIT) {
			unit = 'm';
		} else if (unitVal == DAY_AGE_UNIT) {
			unit = 'd';
		} else if (unitVal == WEEK_AGE_UNIT) {
			unit = 'w';
		}
		return unit;
	}

    /*==========================================================
     * variables
     *==========================================================*/
    private IDSModel _model;
	
	private boolean _clogExists;

	
	private JLabel _dbLabel;
	private JLabel _ageLabel;
	private JLabel _recordLabel;

	private boolean _isValid;
	private boolean _isDirty;

	private JCheckBox  _enableCheckbox;
	private JTextField _dbText;
    private JTextField _recordText;
    private JTextField _ageText;
    private JComboBox  _timeUnit;
	private JCheckBox  _recordCheckbox;
	private JCheckBox  _ageCheckbox;
	private JButton    _browseButton;
	private JButton    _defaultButton;
	private Hashtable  _originalEntryValues;
   
    private static int SECOND_AGE_UNIT = 0;
	private static int MINUTE_AGE_UNIT = 1;
	private static int HOUR_AGE_UNIT = 2;
	private static int DAY_AGE_UNIT = 3;
	private static int WEEK_AGE_UNIT = 4;
	private static int DEFAULT_AGE_UNIT = DAY_AGE_UNIT; //day
    
	private static final String REMOVE = "remove";

	private static ResourceSet _resource =
       new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
	private static final String _section = "replication-supplier";
	private static final int LIMIT_MIN_VAL = 0;
	private static final int LIMIT_MAX_VAL = Integer.MAX_VALUE;
	private static final String CHANGELOG_CN = "changelog5";
	public  static final String CHANGELOG_DN = "cn=" + CHANGELOG_CN +",cn=config";
	private static final String CHANGELOG_DIR_ATTR_NAME =
	                                       "nsslapd-changelogdir";
	private static final String CHANGELOG_MAX_RECORD_ATTR_NAME =
                                           "nsslapd-changelogmaxentries";
	private static final String CHANGELOG_MAX_AGE_ATTR_NAME =
                                           "nsslapd-changelogmaxage";

	private final String[] ATTRIBUTES = {CHANGELOG_DIR_ATTR_NAME,
										 CHANGELOG_MAX_RECORD_ATTR_NAME,
										 CHANGELOG_MAX_AGE_ATTR_NAME};

	private final String CHANGELOG_MAX_AGE_UNIT = "unit";
    private static final String[] COMBO_ENTRIES =
	                                       {"sec", "min", "hr", "day", "week"};
	private static final String[] DEFAULT_OBJECTCLASSES = {"top", "extensibleObject"};
}
