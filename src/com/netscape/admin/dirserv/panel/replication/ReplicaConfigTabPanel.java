/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.IDSContentListener;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.panel.BlankPanel;
import com.netscape.admin.dirserv.panel.DSTabbedPanel;
import netscape.ldap.LDAPEntry;

/**
 * Panel for Replica Settings
 *
 * @author  richm
 * @version %I%, %G%
 * @date	 	2/24/00
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class ReplicaConfigTabPanel extends DSTabbedPanel
{
    public ReplicaConfigTabPanel(IDSModel model,
								 IDSContentListener idscl,
								 LDAPEntry instEntry,
								 LDAPEntry mapTreeEntry,
								 LDAPEntry replicaEntry) {
        super(model);
		BlankPanel selPanel = new ReplicaSettingPanel(model,
													  idscl,
													  instEntry,
													  mapTreeEntry,
													  replicaEntry);
        addTab(selPanel);
    BlankPanel winPanel = new WindowsSyncSettingPanel(model, 
                            idscl,   instEntry,
													  mapTreeEntry,
													  null);
        
    //addTab(winPanel);
 
		if (replicaEntry != null) {
//			addTab(new ConsumerSettingPanel(model, replicaEntry));
//			addTab(new LegacySupplierSettingPanel(model, replicaEntry));
		}
   
        _tabbedPane.setSelectedIndex(0);
        selPanel.invalidate();
        selPanel.validate();
        winPanel.invalidate();
        winPanel.validate();
        _tabbedPane.invalidate();
        _tabbedPane.validate();
    }

	/**
     *  handle incoming event
     *	 
	 * Overwrites DSTabbedPane to call the proper method to 
	 * refresh the Replica Config panel
	 * 
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {

        if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {
			int nTabs = _tabbedPane.getTabCount();
			for (int ii = 0; ii < nTabs; ++ii) {
				BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);				
				if (( p != null ) &&
					p.isInitialized()) {
					if (p instanceof ReplicaSettingPanel) {
						((ReplicaSettingPanel)p).refreshFromServer();
					} else if (p instanceof WindowsSyncSettingPanel){
             System.err.println("Made it");
          }
          else {
						p.refresh();
					}
				}
			}					
		} else {
			super.actionPerformed(e);
		}
	}

}
