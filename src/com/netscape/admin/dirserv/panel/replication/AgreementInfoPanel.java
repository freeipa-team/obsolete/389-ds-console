/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.*;
import java.text.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.logging.DSLogViewerModel;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;


/**
 * Information Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/11/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class AgreementInfoPanel extends ReplicationBlankPanel
    implements ITabPanel, DocumentListener {

    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    AgreementInfoPanel(IAgreementPanel parent, int index) {
        super(parent, index);
        setTitle(_resource.getString("replication-info-tab","label"));
        if(_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD)
        {
        	_helpToken = "configuration-sync-summary-help";
        } else {
        	_helpToken = "configuration-replication-summary-help";
        }		
		_refreshWhenSelect = false;
    }

	/*==========================================================
	 * public methods
     *==========================================================*/

    public void init() {
		int space = UIFactory.getComponentSpace();
		GridBagLayout gb = new GridBagLayout();
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
		_myPanel.setLayout(gb);
        _myPanel.setBackground(getBackground());

        //======== Top Panel ===========================
        JPanel top = new JPanel();
        GridBagLayout gb1 = new GridBagLayout();
        top.setLayout(gb1);
        top.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gb.setConstraints(top, gbc);
        _myPanel.add(top);

        //add icon
        RemoteImage img;
        String description;
        if (_agreement.getAgreementType() ==
			ReplicationAgreement.AGREEMENT_TYPE_MMR) {
            img = ReplicationTool.getImage(_resource.getString(
				"replication-info","mmrIcon"));
            description = _resource.getString("replication-info","mmrDesc");
        } else if (_agreement.getAgreementType() ==
        ReplicationAgreement.AGREEMENT_TYPE_AD) {
              img = ReplicationTool.getImage(_resource.getString(
              	"replication-info","mmrIcon"));
                  description = "This is an Active Directory agreement";
        } else {
            img = ReplicationTool.getImage(_resource.getString(
				"replication-info","legacyrIcon"));
            description = _resource.getString("replication-info","legacyrDesc");
        }

        JLabel iconLabel = makeJLabel(img);
        ReplicationTool.resetGBC(gbc);
        gbc.fill = gbc.NONE;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS; 
        gb1.setConstraints(iconLabel, gbc);
        top.add(iconLabel);

        JPanel titlePanel = new JPanel();
        GridBagLayout gb1a = new GridBagLayout();
        titlePanel.setLayout(gb1a);
        titlePanel.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.weightx = 1.0;
        gbc.gridwidth = gbc.REMAINDER;
        gb1.setConstraints(titlePanel, gbc);
        top.add(titlePanel);

        JLabel label = makeJLabel(description);
		label.setLabelFor(this);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.gridheight = gbc.REMAINDER;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS;
        gbc.weightx=1.0;
        gb1a.setConstraints(label, gbc);
        titlePanel.add(label);

        //name
        JLabel name = makeJLabel(_resource.getString(
			"replication-destination-description","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(10,space,0,space);
        gb1.setConstraints(name, gbc);
        top.add(name);

        _nameTextField= makeJTextField("");
		name.setLabelFor(_nameTextField);
        //_nameTextField.getDocument().addDocumentListener(this);
        _activeColor = name.getBackground();
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 1.0;
        gbc.insets = new Insets(10,space,0,10);
        gb1.setConstraints(_nameTextField, gbc);
        top.add(_nameTextField);


        // create DSEntry associated with name to ensure
        // proper coloring and button enabling
		entries = getDSEntrySet();
        _nameDSEntry = new DSEntryTextStrict (_agreement.getDescription(), _nameTextField, name);
        setComponentTable(_nameTextField, _nameDSEntry);
		entries.add( _agreement.getEntryDN(), ATTR_DESCRIPTION, _nameDSEntry );
		

        //========= summary panel =======================
		String title = _resource.getString("replication-info-summary","label");
        JPanel summary = new GroupPanel(title);        
		GridBagLayout gb2 = new GridBagLayout();
		summary.setLayout(gb2);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gb.setConstraints(summary, gbc);
        _myPanel.add(summary);

        //from
        JLabel from;
        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD) {
            from = makeJLabel(_resource.getString(
                    "sync-info-summary-from","label"));
        } else {
            from = makeJLabel(_resource.getString(
			"replication-info-summary-from","label"));
        }
        from.setToolTipText(_resource.getString(
			"replication-info-summary-from","ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gb2.setConstraints(from, gbc);
        summary.add(from);

        _fromText = makeJLabel("");
		from.setLabelFor(_fromText);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = gbc.WEST;
        gbc.weightx = 1.0;
        gb2.setConstraints(_fromText, gbc);
        summary.add(_fromText);

        //to
        JLabel to;
        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD) {
            to = makeJLabel(_resource.getString(
                    "sync-info-summary-to","label"));
        } else {
            to = makeJLabel(_resource.getString(
			"replication-info-summary-to","label"));
        }
        to.setToolTipText(_resource.getString(
			"replication-info-summary-to","ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gb2.setConstraints(to, gbc);
        summary.add(to);

        _toText = makeJLabel("");
		to.setLabelFor(_toText);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = gbc.WEST;
        gb2.setConstraints(_toText, gbc);
        summary.add(_toText);

        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD) {
            // DS Subtree
            JLabel dsRepSubtree = makeJLabel(_resource.getString("replication-info-summary-ds-subtree","label")); //XXX
            dsRepSubtree.setToolTipText(_resource.getString(
                "replication-info-summary-ds-subtree","ttip"));
            ReplicationTool.resetGBC(gbc);
            gbc.anchor = gbc.EAST;
            gbc.fill = gbc.NONE;
          //  gbc.insets = new Insets(space,space,10,space);
            gb2.setConstraints(dsRepSubtree, gbc);
            summary.add(dsRepSubtree);
    
            _dsRepSubtreeText = makeJLabel();
                dsRepSubtree.setLabelFor(_dsRepSubtreeText);
            ReplicationTool.resetGBC(gbc);
            gbc.gridwidth = gbc.REMAINDER;
           // gbc.insets = new Insets(space,space,10,space);
            gbc.anchor = gbc.EAST;
            gb2.setConstraints(_dsRepSubtreeText, gbc);
            summary.add(_dsRepSubtreeText);
            
            // Win Subtree
        JLabel winRepSubtree = makeJLabel(_resource.getString("replication-info-summary-win-subtree","label")); //XXX
        winRepSubtree.setToolTipText(_resource.getString(
			"replication-info-summary-win-subtree","ttip"));
        ReplicationTool.resetGBC(gbc);
            gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
      //  gbc.insets = new Insets(space,space,10,space);
        gb2.setConstraints(winRepSubtree, gbc);
        summary.add(winRepSubtree);

        _winRepSubtreeText = makeJLabel();
	     	winRepSubtree.setLabelFor(_winRepSubtreeText);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
       // gbc.insets = new Insets(space,space,10,space);
            gbc.anchor = gbc.EAST;
        gb2.setConstraints(_winRepSubtreeText, gbc);
        summary.add(_winRepSubtreeText);
        }
        
        //suffix
        JLabel suffix = makeJLabel(_resource.getString(
			"replication-info-summary-subtree","label"));
        suffix.setToolTipText(_resource.getString(
			"replication-info-summary-subtree","ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(space,space,10,space);
        gb2.setConstraints(suffix, gbc);
        summary.add(suffix);

        _suffixText = makeJLabel();
		suffix.setLabelFor(_suffixText);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(space,space,10,space);
        gbc.anchor = gbc.WEST;
        gb2.setConstraints(_suffixText, gbc);
        summary.add(_suffixText);
        
 

        //========== status panel ======================
		title = _resource.getString("replication-info-status","label");
        JPanel status = new GroupPanel(title);        
		GridBagLayout gb3 = new GridBagLayout();
		status.setLayout(gb3);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;        
        gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
	
        gb.setConstraints(status, gbc);
        _myPanel.add(status);

        //update in progress?
        JLabel lInProgress = makeJLabel(_resource.getString(
			"replication-info-status-inProgress","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
		gbc.insets = new Insets(space,space,0,space);
        gb3.setConstraints(lInProgress, gbc);
        status.add(lInProgress);

        _valInProgress = makeJLabel("");
		lInProgress.setLabelFor(_valInProgress);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 1.0;
		gbc.insets = new Insets(space,space,0,space);
        gb3.setConstraints(_valInProgress, gbc);
        status.add(_valInProgress);

        //lastUpdate
        JLabel lastUpdate = makeJLabel(_resource.getString(
			"replication-info-status-lastUpdate","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
		gbc.insets = new Insets(space,space,0,space);
        gb3.setConstraints(lastUpdate, gbc);
        status.add(lastUpdate);

        _lastUpdateText = makeJLabel("");
		lastUpdate.setLabelFor(_lastUpdateText);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 1.0;
		gbc.insets = new Insets(space,space,0,space);
        gb3.setConstraints(_lastUpdateText, gbc);
        status.add(_lastUpdateText);

        //n changes sent last update
        JLabel lNChangesLast = makeJLabel(_resource.getString(
			"replication-info-status-nChangesLast","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(space,space,0,space);
        gb3.setConstraints(lNChangesLast, gbc);
        status.add(lNChangesLast);

        _valNChangesLast = makeJLabel("");
		lNChangesLast.setLabelFor(_valNChangesLast);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(space,space,0,space);
        gb3.setConstraints(_valNChangesLast, gbc);
        status.add(_valNChangesLast);

        //last update begin time
        JLabel lLastUpdateBegin = makeJLabel(_resource.getString(
			"replication-info-status-lastUpdateBegin","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(space,space,0,space);
        gb3.setConstraints(lLastUpdateBegin, gbc);
        status.add(lLastUpdateBegin);

        _valLastUpdateBegin = makeJLabel("");
		lLastUpdateBegin.setLabelFor(_valLastUpdateBegin);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(space,space,0,space);
        gb3.setConstraints(_valLastUpdateBegin, gbc);
        status.add(_valLastUpdateBegin);

        //last update end time
        JLabel lLastUpdateEnd = makeJLabel(_resource.getString(
			"replication-info-status-lastUpdateEnd","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(space,space,0,space);
		gbc.insets.bottom = ReplicationTool.BOTTOM_INSETS.bottom;
        gb3.setConstraints(lLastUpdateEnd, gbc);
        status.add(lLastUpdateEnd);

        _valLastUpdateEnd = makeJLabel("");
		lLastUpdateEnd.setLabelFor(_valLastUpdateEnd);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(space,space,0,space);
		gbc.insets.bottom = ReplicationTool.BOTTOM_INSETS.bottom;
        gb3.setConstraints(_valLastUpdateEnd, gbc);
        status.add(_valLastUpdateEnd);



       //========== Consumer init status panel ======================
        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD) {
    		title = _resource.getString("sync-info-fullsync-status","label");
        } else {
		title = _resource.getString("replication-info-initconsumer-status","label");
        }
        _consumerInitPanel = new GroupPanel(title);				
		GridBagLayout gb4 = new GridBagLayout();
		_consumerInitPanel.setLayout(gb4);        
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.gridheight = gbc.REMAINDER;
        gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
		
        gb.setConstraints(_consumerInitPanel, gbc);
        _myPanel.add(_consumerInitPanel);
		
		gbc.weighty = 1.0;
		gbc.fill = gbc.VERTICAL;
		_myPanel.add(Box.createVerticalGlue(), gbc);

        //consumer initialization in progress?
        JLabel lConsumerInitializationInProgress;
        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD) {
            lConsumerInitializationInProgress = makeJLabel(_resource.getString(
                    "sync-info-fullsync-status-inProgress","label"));
        } else {
            lConsumerInitializationInProgress = makeJLabel(_resource.getString(
			"replication-info-initconsumer-status-inProgress","label"));
        }
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
		gbc.insets = new Insets(space,space,0,space);
        gb4.setConstraints(lConsumerInitializationInProgress, gbc);
        _consumerInitPanel.add(lConsumerInitializationInProgress);

        _valConsumerInitializationInProgress = makeJLabel("");
		lConsumerInitializationInProgress.setLabelFor(_valConsumerInitializationInProgress);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 1.0;
		gbc.insets = new Insets(space,space,0,space);
        gb4.setConstraints(_valConsumerInitializationInProgress, gbc);
        _consumerInitPanel.add(_valConsumerInitializationInProgress);		

        //last Consumer Initialization Status
        JLabel consumerInitializationStatus;
        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD) {
            consumerInitializationStatus = makeJLabel(_resource.getString(
                    "sync-info-fullsync-status-lastUpdate","label"));
        } else {
            consumerInitializationStatus = makeJLabel(_resource.getString(
			"replication-info-initconsumer-status-lastUpdate","label"));
        }
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
		gbc.insets = new Insets(space,space,0,space);
        gb4.setConstraints(consumerInitializationStatus, gbc);
        _consumerInitPanel.add(consumerInitializationStatus);

        _consumerInitializationStatus = makeJLabel("");
		consumerInitializationStatus.setLabelFor(_consumerInitializationStatus);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 1.0;
		gbc.insets = new Insets(space,space,0,space);
        gb4.setConstraints(_consumerInitializationStatus, gbc);
        _consumerInitPanel.add(_consumerInitializationStatus);   

        //last consumer initialization begin time
        JLabel lConsumerInitializationBegin;
        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD) {
            lConsumerInitializationBegin = makeJLabel(_resource.getString(
                    "sync-info-fullsync-status-begin","label"));
        } else {
            lConsumerInitializationBegin = makeJLabel(_resource.getString(
			"replication-info-initconsumer-status-begin","label"));
        }
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(space,space,0,space);
        gb4.setConstraints(lConsumerInitializationBegin, gbc);
        _consumerInitPanel.add(lConsumerInitializationBegin);

        _valConsumerInitializationBegin = makeJLabel("");
		lConsumerInitializationBegin.setLabelFor(_valConsumerInitializationBegin);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(space,space,0,space);
        gb4.setConstraints(_valConsumerInitializationBegin, gbc);
        _consumerInitPanel.add(_valConsumerInitializationBegin);

        //last consumer initialization end time
        JLabel lConsumerInitializationEnd;
        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD) {
            lConsumerInitializationEnd = makeJLabel(_resource.getString(
                    "sync-info-fullsync-status-end","label"));
        } else {
            lConsumerInitializationEnd = makeJLabel(_resource.getString(
			"replication-info-initconsumer-status-end","label"));
        }
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(space,space,0,space);
		gbc.insets.bottom = ReplicationTool.BOTTOM_INSETS.bottom;
        gb4.setConstraints(lConsumerInitializationEnd, gbc);
        _consumerInitPanel.add(lConsumerInitializationEnd);

        _valConsumerInitializationEnd = makeJLabel("");
		lConsumerInitializationEnd.setLabelFor(_valConsumerInitializationEnd);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(space,space,0,space);
		gbc.insets.bottom = ReplicationTool.BOTTOM_INSETS.bottom;
        gb4.setConstraints(_valConsumerInitializationEnd, gbc);
        _consumerInitPanel.add(_valConsumerInitializationEnd);

        initialize ();
    }

    /**
      * Called when the object is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
        super.select(parent, viewInstance);
        invalidate();
        validate();
        repaint();
    }

    //===== ITabPanel interface functions =========
    public boolean validateEntries() {

        /* now all validation is done through DSEntry
           provided mechanism before the Save button
           is enabled. So we don't need to do anyhting 
           here                                        */

        return true;
    }

    public void getUpdateInfo( Object inf ) {
		AgreementWizardInfo info = (AgreementWizardInfo)inf;
        info.setDescription(_nameTextField.getText());
    }

    public boolean refresh() {
		super.refresh();
        populateData();

	    return true;
    }

	public void resetCallback() {
		populateData();
		clearDirtyFlag();
	}

    /*==========================================================
     * private methods
     *==========================================================*/
    /**
     * Populate the UI with data
     */
    protected void populateData() {
		
	_agreement.updateAgreementFromServer();
	
	//		_nameDSEntry.fakeInitModel (_agreement.getNickname());
	_nameTextField.setText( _agreement.getDescription());
	_fromText.setText(_agreement.getSupplierHost() + ":" +
			  _agreement.getSupplierPort());
	_toText.setText(_agreement.getConsumerHost() + ":" +
			_agreement.getConsumerPort());	
	_suffixText.setText(_agreement.getReplicatedSubtree());
 if (_agreement.getAgreementType() ==
        ReplicationAgreement.AGREEMENT_TYPE_AD) {
  _winRepSubtreeText.setText(((ActiveDirectoryAgreement)_agreement).getWinRepArea());
  _dsRepSubtreeText.setText(((ActiveDirectoryAgreement)_agreement).getDSRepArea());
 }
  // status
	_lastUpdateText.setText(ReplicationTool.convertStatusMessage(
																_agreement.getLastUpdateStatus()));
	_valInProgress.setText(_agreement.getInProgress());
	_valNChangesLast.setText(_agreement.getNChangesLast());
	_valLastUpdateBegin.setText(_agreement.getLastUpdateBegin());
	_valLastUpdateEnd.setText(_agreement.getLastUpdateEnd());
  
	// consumer initialization status
	if (!_agreement.getConsumerInitializationBegin().equals(
															_resource.getString("replication-agreement", 
																				"initconsumer-begin-unknown-value"))) {
		_consumerInitPanel.setVisible(true);
		_consumerInitializationStatus.setText(ReplicationTool.convertStatusMessage(
																				  _agreement.getConsumerInitializationStatus()));
		_valConsumerInitializationInProgress.setText(_agreement.getConsumerInitializationInProgress());	
		_valConsumerInitializationBegin.setText(_agreement.getConsumerInitializationBegin());
		_valConsumerInitializationEnd.setText(_agreement.getConsumerInitializationEnd());
	} else {
		_consumerInitPanel.setVisible(false);		
	}
    }
    
    //get replication status
    private void updateReplicaStatus() {
        _agreement.updateReplicaStatus();
    }

    /*==========================================================
     * variables
     *==========================================================*/
    
    JLabel _fromText, _toText, _suffixText, _winRepSubtreeText, _dsRepSubtreeText;
    JTextField _nameTextField;
    // status GUI
    JLabel _lastUpdateText, _valInProgress, _valNChangesLast,
        _valLastUpdateBegin, _valLastUpdateEnd,
		_consumerInitializationStatus, _valConsumerInitializationInProgress, 
		_valConsumerInitializationBegin, _valConsumerInitializationEnd;
    JLabel domain;
    JPanel _consumerInitPanel;
		
    private DSEntryTextStrict _nameDSEntry;
    private DSEntrySet entries;
    private static final String ATTR_DESCRIPTION = "description";

}
