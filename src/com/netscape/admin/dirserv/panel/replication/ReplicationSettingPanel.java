/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;


import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;

/**
 * Panel for Replication Settings
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/26/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class ReplicationSettingPanel extends DSTabbedPanel
{
    public ReplicationSettingPanel(IDSModel model) {
        super(model);
        _tabbedPanel = _tabbedPane;
        _model = model;
        _supplierTab = new SupplierSettingPanel(_model);
		addTab(_supplierTab);
   
        _tabbedPane.setSelectedIndex(0);
        _supplierTab.invalidate();
        _supplierTab.validate();
        _tabbedPane.invalidate();
        _tabbedPane.validate();
    }

	/**
     *  handle incoming event
     *	 
	 * Overwrites DSTabbedPane to call the proper method to 
	 * refresh the Supplier Settings and the Legacy Consumer panel
	 * 
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {
			int nTabs = _tabbedPane.getTabCount();
			for (int ii = 0; ii < nTabs; ++ii) {
				BlankPanel p = (BlankPanel)_tabbedPane.getComponentAt(ii);
				if (( p != null ) &&
					p.isInitialized()) {
					if (p instanceof SupplierSettingPanel) {
						((SupplierSettingPanel)p).refreshFromServer();
					} else {
						p.refresh();
					}
				}
			}					
		} else {
			super.actionPerformed(e);
		}
	}
    
	BlankPanel _supplierTab;
	JTabbedPane _tabbedPanel;
	IDSModel _model;
    
	//get resource bundle
    public static ResourceSet _resource =
	                new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
    
}
