/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import netscape.ldap.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.console.ConsoleInfo;
import java.util.*;
import java.io.*;

public class ActiveDirectoryAgreement extends ReplicationAgreement {
    
    /**
     * Constructs a new, empty replication agreement
     */
    public ActiveDirectoryAgreement(ConsoleInfo info, LDAPEntry agreementEntry, LDAPEntry parentEntry) {
        super(ReplicationAgreement.AGREEMENT_TYPE_AD);
        supplierHost = info.getHost();
        supplierPort = info.getPort();
        setServerInfo(info);
        String entryDN = null;
        if (agreementEntry != null) {
            entryDN = agreementEntry.getDN();
            setOrigEntryDN(entryDN);
            setEntryDN(entryDN);
            readValuesFromEntry(agreementEntry);
            agreementIsNew = false;
        }
        else if (parentEntry != null) {
            entryDN = "cn=xxx," + parentEntry.getDN();
            agreementIsNew = true;
            setOrigEntryDN(entryDN);
        }
    }
    
    /**
     * Set the usePersistentSearch flag.
     */
    public void setUsePersistentSearch(String val) {
        if (val.equalsIgnoreCase("true")) {
            usePersistentSearch = true;
        } else {
            usePersistentSearch = false;
        }
    }
    
    /**
     * Get the usePersisentSearch flag.
     */
    public boolean getUsePersistentSearch() {
        return usePersistentSearch;
    }
    
    /**
     * Set the syncInterval
     */
    public void setSyncInterval(int val) {
        syncInterval = val;
    }
    
    /**
     * Get the sync interval
     */
    public int getSyncInterval() {
        return syncInterval;
    }
    
    public void setUnreachable(boolean x) {
        supplierUnreachable = x;
        Debug.println("ActiveDirectoryAgreement.setUnreachable: " + x);
    }
    
    public boolean getUnreachable() {
        return supplierUnreachable;
    }
    
    /**
     * Read a consumer-initiated replication agreement from the server.
     * If successful, read the attribute values from the entry and
     * populate the members of this MMRAgreement object.
     */
    public void readValuesFromEntry(LDAPEntry entry) {
        LDAPAttributeSet attrs = entry.getAttributeSet();
        Enumeration attrsEnum = (Enumeration)attrs.getAttributes();
        byte[] raw_value;
        String conv_value;
        String cn = null;
        
        //initialize values
        while (attrsEnum.hasMoreElements()) {
            LDAPAttribute attr = (LDAPAttribute)attrsEnum.nextElement();
            if (attr.getName().equalsIgnoreCase(ReplicationTool.REPLICA_SCHEDULE_ATTR)) {
                Enumeration valsEnum = attr.getStringValues();
                String val;
                while (valsEnum.hasMoreElements()) {
                    val = (String)valsEnum.nextElement();
                    addUpdateSchedule(val);
                }
                continue;
            }
            // The rest of the attributes are single-valued
            Enumeration en = attr.getStringValues();
            if( !en.hasMoreElements() ) {
                continue;
            }
            String val = (String)en.nextElement();
            if (attr.getName().equalsIgnoreCase(
            ReplicationTool.MMR_NAME_ATTR)) {
                setNickname(val);
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.MMR_NICKNAME_ATTR)) {
                setDescription(val);
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_ROOT_ATTR)) {
                setReplicatedSubtree(val);
                origReplicatedSubtree = getReplicatedSubtree();
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_HOST_ATTR)) {
                setConsumerHost(val);
                origConsumerHost = getConsumerHost();
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_PORT_ATTR)) {
                setConsumerPort(Integer.parseInt(val));
                origConsumerPort = getConsumerPort();
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_BINDDN_ATTR)) {
                setBindDN(val);
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_CRED_ATTR)) {
                setBindCredentials(val);
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_TRANSPORT_ATTR)) {
                setUseSSL(val);
                setUseStartTLS(val);
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.MMR_NAME_ATTR)) {
                cn = val;
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_BINDMETHOD_ATTR)) {
                setUseSSLAuth(val);
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_REFRESH_ATTR)) {
                setORCValue(val);
            }  else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_WINDOWS_SUBTREE)) {
                setWinRepArea(val);
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_DS_SUBTREE)) {
                setDSRepArea(val);
            } else if (attr.getName().equalsIgnoreCase(
            ReplicationTool.REPLICA_NEW_WIN_USER_SYNC)) {
                setNewWinUserSync(val);
            } else if (attr.getName().equalsIgnoreCase(
                    ReplicationTool.REPLICA_NEW_WIN_GROUP_SYNC)) {
                setNewWinGroupSync(val);
            } else if (attr.getName().equalsIgnoreCase(
                    ReplicationTool.REPLICA_WINDOWS_DOMAIN)) {
                setWindowsDomain(val);
            } 
        }
        if ( cn != null ) {
            setEntryCN(cn);
        }
    }
    
    public int writeToServer() throws IOException {
        LDAPAttribute attr;
        byte[] value;
        String stmp;
        String newDN = null;
        
        if ( agreementIsNew ) {
            
            computeNewEntryCNandDN();
            
            LDAPAttributeSet newAttrs = new LDAPAttributeSet();
            
            attr = new LDAPAttribute( "objectclass",
            ReplicationTool.WIN_AGGREEMENT_OBJECTCLASSES);
            newAttrs.add(attr);
            
            attr = new LDAPAttribute(ReplicationTool.MMR_NICKNAME_ATTR,
            _description);
            newAttrs.add(attr);
            
            attr = new LDAPAttribute(ReplicationTool.MMR_NAME_ATTR,
            entryCN);
            newAttrs.add(attr);
            
            attr = new LDAPAttribute(ReplicationTool.REPLICA_WINDOWS_SUBTREE, win_rep_area);
            newAttrs.add(attr);
            
            attr = new LDAPAttribute(ReplicationTool.REPLICA_DS_SUBTREE, ds_rep_area);
            newAttrs.add(attr);
            
            attr = new LDAPAttribute(ReplicationTool.REPLICA_NEW_WIN_USER_SYNC, new_win_user_sync);
            newAttrs.add(attr);
            
            attr = new LDAPAttribute(ReplicationTool.REPLICA_NEW_WIN_GROUP_SYNC, new_win_group_sync);
            newAttrs.add(attr);
            
            attr = new LDAPAttribute(ReplicationTool.REPLICA_WINDOWS_DOMAIN, win_domain);
            newAttrs.add(attr);
            
            if (replicatedSubtree == null) {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_ROOT_ATTR,
                "");
            } else {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_ROOT_ATTR,
                replicatedSubtree);
            }
            newAttrs.add(attr);
            
            attr = new LDAPAttribute(ReplicationTool.REPLICA_HOST_ATTR,
            consumerHost);
            newAttrs.add(attr);
            
            attr = new LDAPAttribute(ReplicationTool.REPLICA_PORT_ATTR,
            Integer.toString(consumerPort));
            newAttrs.add(attr);
            
            if (bindDN != null) {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_BINDDN_ATTR,
                bindDN);
                newAttrs.add(attr);
            }
            
            if (bindCredentials != null) {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_CRED_ATTR,
                bindCredentials);
                newAttrs.add(attr);
            }
            
            if( useSSL || useStartTLS ) {
            	if (useStartTLS) {
            		attr = new LDAPAttribute(ReplicationTool.REPLICA_TRANSPORT_ATTR,
            				ReplicationTool.REPLICA_TRANSPORT_TLS );
            	} else {
                    attr = new LDAPAttribute(ReplicationTool.REPLICA_TRANSPORT_ATTR,
                            ReplicationTool.REPLICA_TRANSPORT_SSL );
            	}
                newAttrs.add(attr);
                
            }
            if( (useSSL || useStartTLS) && useSSLAuth ) {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_BINDMETHOD_ATTR,
                ReplicationTool.REPLICA_BINDMETHOD_SSLCLIENTAUTH );
                newAttrs.add(attr);
            } else {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_BINDMETHOD_ATTR,
                ReplicationTool.REPLICA_BINDMETHOD_SIMPLE );
                newAttrs.add(attr);
            }
            
            
            // XXXggood filtered and selective attribute attributes would
            // get written
            // here, if we supported them in CIR
            
            String[] sched = getUpdateScheduleStrings();
            if (sched != null) {
                // first, see if any of the values are valid
                boolean valid = false;
                for (int i = 0; !valid && i < sched.length; i++) {
                    valid = (sched[i] != null) && (sched[i].length() > 0);
                }
                if (valid) {
                    attr = new LDAPAttribute(ReplicationTool.REPLICA_SCHEDULE_ATTR,
                    sched);
                    newAttrs.add(attr);
                }
            }
                 
            LDAPEntry newEntry = new LDAPEntry(entryDN, newAttrs);
            try {
                createNewEntry(newEntry);
            } catch (LDAPException e) {
                Debug.println("MMRAgreement.writeToServer: <" + entryDN +
                "> " + e.toString());
                return e.getLDAPResultCode();
            }
            // Delete the old entry
            if (!agreementIsNew) {
                try {
                    deleteOldEntry();
                    setOrigEntryDN( getEntryDN() );
                } catch (LDAPException de) {
                    Debug.println("MMRAgreement.writeToServer: <" + entryDN +
                    "> " + de.toString());
                    return de.getLDAPResultCode();
                }
            }
            
        } else {
            
            LDAPModification mod;
            LDAPModificationSet mods = new LDAPModificationSet();
            
            attr = new LDAPAttribute(ReplicationTool.MMR_NICKNAME_ATTR,
            _description);
            
            mods.add(LDAPModification.REPLACE, attr);
            
            // tmp until server support it
            //             attr = new LDAPAttribute(ReplicationTool.MMR_NAME_ATTR,
            // 				     entryCN);
            
            //             mods.add(LDAPModification.REPLACE, attr);
            
            //             attr = new LDAPAttribute(ReplicationTool.REPLICA_ROOT_ATTR,
            // 				     replicatedSubtree);
            //             mods.add(LDAPModification.REPLACE, attr);
            
            // 	    attr = new LDAPAttribute(ReplicationTool.REPLICA_TRANSPORT_ATTR,
            //  				     useSSL ? ReplicationTool.REPLICA_TRANSPORT_SSL : ReplicationTool.REPLICA_TRANSPORT_LDAP);
            // 	    mods.add(LDAPModification.REPLACE, attr);
            
            if (bindDN != null) {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_BINDDN_ATTR,
                bindDN);
            } else {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_BINDDN_ATTR);
            }
            mods.add(LDAPModification.REPLACE, attr);
            
            if (bindCredentials != null) {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_CRED_ATTR,
                bindCredentials);
                mods.add(LDAPModification.REPLACE, attr);
            }
            
            if (new_win_user_sync != null){
                attr = new LDAPAttribute(ReplicationTool.REPLICA_NEW_WIN_USER_SYNC, new_win_user_sync);
                mods.add(LDAPModification.REPLACE, attr);
            }
            
            if (new_win_group_sync != null){
                attr = new LDAPAttribute(ReplicationTool.REPLICA_NEW_WIN_GROUP_SYNC, new_win_group_sync);
                mods.add(LDAPModification.REPLACE, attr);
            }
            
            Debug.println("MMRAgreement.writeToServer: useSSLAuth="+ useSSLAuth );
            if( useSSLAuth ) {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_BINDMETHOD_ATTR,
                ReplicationTool.REPLICA_BINDMETHOD_SSLCLIENTAUTH );
            } else {
                attr = new LDAPAttribute(ReplicationTool.REPLICA_BINDMETHOD_ATTR,
                ReplicationTool.REPLICA_BINDMETHOD_SIMPLE );
            }
            mods.add(LDAPModification.REPLACE, attr);
            
            String[] sched = getUpdateScheduleStrings();
            attr = new LDAPAttribute(ReplicationTool.REPLICA_SCHEDULE_ATTR);
            if (sched != null) {
                // first, see if any of the values are valid
                boolean valid = false;
                for (int i = 0; !valid && i < sched.length; i++) {
                    valid = (sched[i] != null) && (sched[i].length() > 0);
                }
                if (valid) {
                    // easier than setting individual elements . . .
                    attr = new LDAPAttribute(ReplicationTool.REPLICA_SCHEDULE_ATTR,
                    sched);
                }
                
                mods.add(LDAPModification.REPLACE, attr);
                
            }
            
            
            try {
                Debug.println("MMRAgreement.writeToServer: upd:" + mods );
                updateEntry(mods);
            } catch (LDAPException me) {
                Debug.println("MMRAgreement.writeToServer: <" + entryDN +
                "> " + me.toString());
                return me.getLDAPResultCode();
            }
        }
        return 0;
    }
    
    /**
     * Getter for property win_domain.
     * @return Value of property win_domain.
     */
    public String getWindowsDomain() {
        return win_domain;
    }
    
    /**
     * Setter for property win_domain.
     * @param win_domain New value of property win_domain.
     */
    public void setWindowsDomain(String domain) {
        this.win_domain = domain;
    }
    
     /**
     * Getter for property win_rep_area.
     * @return Value of property win_rep_area.
     */
    public String getWinRepArea() {
        return win_rep_area;
    }
    
    /**
     * Setter for property win_rep_area.
     * @param win_rep_area New value of property win_rep_area.
     */
    public void setWinRepArea(String win_rep_area) {
        this.win_rep_area = win_rep_area;
    }
    
     /**
     * Getter for property ds_rep_area.
     * @return Value of property ds_rep_area.
     */
    public String getDSRepArea() {
        return ds_rep_area;
    }
    
    /**
     * Setter for property ds_rep_area.
     * @param ds_rep_area New value of property ds_rep_area.
     */
    public void setDSRepArea(String ds_rep_area) {
        this.ds_rep_area = ds_rep_area;
    }
    
     /**
     * Getter for property new_win_user_sync.
     * @return Value of property new_win_user_sync.
     */
    public String getNewWinUserSync() {
        return new_win_user_sync;
    }
    
    /**
     * Setter for property new_win_user_sync.
     * @param new_win_user_sync New value of property new_win_user_sync.
     */
    public void setNewWinUserSync(String new_win_user_sync) {
        this.new_win_user_sync = new_win_user_sync;
    }
    
    /**
     * Getter for property new_win_group_sync.
     * @return Value of property new_win_group_sync.
     */
    public String getNewWinGroupSync() {
        return new_win_group_sync;
    }
    
    /**
     * Setter for property new_win_group_sync.
     * @param new_win_user_sync New value of property new_win_group_sync.
     */
    public void setNewWinGroupSync(String new_win_group_sync) {
        this.new_win_group_sync = new_win_group_sync;
    }
    
    private String win_domain;
    private String win_rep_area;
    private String ds_rep_area;
    private String new_win_user_sync;
    private String new_win_group_sync;
    // objectClasses: ( 2.16.840.1.113730.3.2.103 NAME 'nsDS5ReplicationAgreement' DESC 'Standard Netscape Directory Server Schema' SUP top MUST ( cn ) MAY ( nsDS5ReplicaHost $ nsDS5ReplicaPort $ nsDS5ReplicaTransportInfo $ nsDS5ReplicaBindDN $ nsDS5ReplicaCredentials $ nsDS5ReplicaBindMethod $ nsDS5ReplicaRoot $ nsDS5ReplicatedAttributeList $ nsDS5ReplicaUpdateSchedule $ nsds5BeginReplicaRefresh $ description ) X-NSSTANDARD )
    
    //variables
    private boolean usePersistentSearch;   // true if client should keep
    //connection to server open
    private int syncInterval;              // time between updates (in minutes)
    private boolean supplierUnreachable;   // true iff we can't talk to the
    //remote supplier right now
    private static final int DEFAULT_SYNC_INTERVAL = 10;    // 10 minutes
    
    //get resource bundle
    private static ResourceSet _resource =
    new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
    // private static final String MMRAgreementClass = ReplicationTool.WIN_AGREEMENT_OBJECTCLASS;
}
