/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import netscape.ldap.*;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.DSUtil;

public class LDAPUtil
{
    /** Open a connection to the server.
     */
    public static LDAPConnection connectToServer( String host,
												  int port,
												  String binddn,
												  String bindpw,
												  boolean useSSL ) {
		try {
			return DSUtil.getLDAPConnection( host, port, binddn, bindpw,
											 useSSL );
		} catch ( LDAPException e ) {
			/* An error message is printed by DSUtil */
			return null;
		}
    }

    public static LDAPEntry readEntry(LDAPConnection ld, String dn, String[] retAttrs)
    {
        LDAPEntry retEntry = null;

        if (ld != null) {
            try {
                retEntry = ld.read(dn, retAttrs);
            } catch (LDAPException le) {
                Debug.println("Error: LDAPUtil: readEntry() - "+le.toString());
            }
        }
        return retEntry;
    }



    public static int disconnect(LDAPConnection ld, boolean reportErrors)
    {
        int rc = LDAPException.SUCCESS;

        if (ld != null) {
            try {
                ld.disconnect();
            } catch (LDAPException le) {
                Debug.println("Error: LDAPUtil: disconnect() - "+le.toString());
                //WE WON"T DO ANYTHING HERE
            }
        }
        return rc;
    }

    public static String LDAPErr2String(int lderr) {
		return LDAPException.errorCodeToString( lderr );
    }
}

