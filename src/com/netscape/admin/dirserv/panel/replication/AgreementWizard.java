/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;

import com.netscape.admin.dirserv.wizard.*;
import javax.swing.*;
import com.netscape.management.client.util.*;

/**
 * Wizard for creating new replication agreement
 * Pre-Condition: User auth as Directory Manager already
 *      - no auth check will be performed by the wizard
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/02/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class AgreementWizard extends WizardWidget {

    public AgreementWizard(JFrame parent, AgreementWizardInfo info) {
        super(parent, _resource.getString("replication-agreementWizard","label"), new Dimension(460,550));
		getAccessibleContext().setAccessibleDescription(_resource.getString("replication-agreementWizard",
																			"description"));
        //parent.setIconImage(ReplicationTool.getImage(_resource.getString("replication-agreementWizard","frameIcon")).getImage());
        setWizardInfo(info);
        if (! info.getWizardType().equals(info.NEW_WIZARD)) {
            //copy operation
            ServerInstance server = new ServerInstance( info.getServerInfo().getHost(),info.getServerInfo().getPort());
            if (info.getCopyAgreement().getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_MMR) {
                info.setAgreementType(info.MMR_AGREEMENT);
                info.setFromServer(server);
                ServerInstance toServer = 
                    new ServerInstance(info.getCopyAgreement().getConsumerHost(),
                                       info.getCopyAgreement().getConsumerPort());
                info.setToServer(toServer);
            } else {
                info.setAgreementType(info.LEGACYR_AGREEMENT);
                info.setFromServer(server);
                ServerInstance toServer = 
                    new ServerInstance( info.getCopyAgreement().getConsumerHost(),
                                        info.getCopyAgreement().getConsumerPort());
                info.setToServer(toServer);
            }
            info.setSubtree(info.getCopyAgreement().getReplicatedSubtree());
            info.setSSL(info.getCopyAgreement().getUseSSL());
            info.setStartTLS(info.getCopyAgreement().getUseStartTLS());
            if (info.getCopyAgreement().getBindDN()!= null) {
                info.setBindDN(info.getCopyAgreement().getBindDN());
            } else {
                info.setBindDN("");
            }
			// richm bug 600531 - the bindpwd may be hashed, so we should force the user to
			// type in the password again when copying an agreement
			// we could try to determine if the password is hashed or not, but in general
			// that is almost impossible to do
			String pwd = info.getCopyAgreement().getBindCredentials();
			if (ReplicationTool.isHashedPwd(pwd)) {
				info.setBindPWD("");
			} else {
				info.setBindPWD(pwd);
			}
            info.setFilter(info.getCopyAgreement().getEntryFilter());
            info.setSelectedAttr(info.getCopyAgreement().getSelectedAttributes());
            info.setAttrType(info.getCopyAgreement().getSelattrType());
            info.setDate(info.getCopyAgreement().getUpdateSchedule());
            info.setName(info.getCopyAgreement().getEntryCN());
			info.setDescription(info.getCopyAgreement().getDescription());
        } else {
            //new agreement
            String type = info.getAgreementType();
            if (type == null)
                type = info.MMR_AGREEMENT;    					
			panel1 = new WAgreementSelectionAndInfoPanel(this,true,type);	
        }
		if (panel1==null) {						
			panel1 = new WAgreementSelectionAndInfoPanel(this);
		}	
        panel2 = new WAgreementDestinationPanel(this);
        panel4 = new WAgreementSchedulePanel(this);
        panel5 = new WAgreementInitPanel(this);
        panel6 = new WAgreementSummaryPanel();

        addPage(panel1);
        addPage(panel2);
		if ( AgreementWizardInfo.doSelectiveReplication ) {
		    panel3 = new WAgreementAttributePanel();
			addPage(panel3);
		}
        addPage(panel4);
        addPage(panel5);
        addPage(panel6);
				
        show();
    }
    
    protected void callHelp() {
        if (_current instanceof IWizardPanel) {
			((IWizardPanel)_current).callHelp();
		}
    }
	
	public JButton getbNext_Done() {
		return super.getbNext_Done();
	}

    JPanel panel0, panel1, panel2, panel3, panel4, panel5, panel6;

	//get resource bundle
    public static ResourceSet _resource =
	    new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");

}
