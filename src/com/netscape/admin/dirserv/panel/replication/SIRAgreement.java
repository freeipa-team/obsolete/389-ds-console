/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import netscape.ldap.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.console.ConsoleInfo;
import java.util.*;
import java.io.*;

public class SIRAgreement extends ReplicationAgreement {

    /**
     * Consructs a new, empty replication agreement
     */
    public SIRAgreement(ConsoleInfo info, LDAPEntry agreementEntry,
						LDAPEntry parentEntry) {
        super(ReplicationAgreement.AGREEMENT_TYPE_LEGACYR);        
        supplierHost = info.getHost();
        supplierPort = info.getPort();
        setServerInfo(info);
		String entryDN = null;
		if (agreementEntry != null) {
			entryDN = agreementEntry.getDN();
			setOrigEntryDN(entryDN);
			setEntryDN(entryDN);
			readValuesFromEntry(agreementEntry);
			agreementIsNew = false;
		}
		else if (parentEntry != null) {
			entryDN = "cn=xxx," + parentEntry.getDN();
			agreementIsNew = true;
			setOrigEntryDN(entryDN);
		}
    }

    /**
     * Read a legacy replication agreement from the server.
	 * If successful, read the attribute values from the entry and
	 * populate the members of this SIRAgreement object.
     */
    public void readValuesFromEntry(LDAPEntry entry) {
		Debug.println( 7, "SIRAgreement.readValuesFromEntry: " + entry );
        LDAPAttributeSet attrs = entry.getAttributeSet();
        Enumeration attrsEnum = attrs.getAttributes();
		String cn = null;

        //should initialize values so we can re-read.
        while (attrsEnum.hasMoreElements()) {
            LDAPAttribute attr = (LDAPAttribute)attrsEnum.nextElement();
            if (attr.getName().equalsIgnoreCase("replicatedAttributeList")) {
                parseReplAttrList(attr);
                origSelattrType = getSelattrType();
                origSelectedAttrs = (Vector)getSelectedAttributes().clone();
            } else if (attr.getName().equalsIgnoreCase(
				"replicaUpdateSchedule")) {
                Enumeration valsEnum = attr.getStringValues();
                while (valsEnum.hasMoreElements()) {
                    addUpdateSchedule((String)valsEnum.nextElement());
                }
			} else {
				// The rest of the attributes are single-valued
				Enumeration en = attr.getStringValues();
				if( !en.hasMoreElements() ) {
					continue;
				}
				String val = (String)en.nextElement();
				if (attr.getName().equalsIgnoreCase( "replicaRoot")) {
					setReplicatedSubtree( val );
					//replicatedSubtree = conv_value;
					origReplicatedSubtree = val;
				} else if (attr.getName().equalsIgnoreCase("replicaHost")) {
					setConsumerHost(val);
					origConsumerHost = getConsumerHost();
				} else if (attr.getName().equalsIgnoreCase("replicaPort")) {
					try {
						setConsumerPort(Integer.parseInt(val));
					} catch (Exception e) {
					}
					origConsumerPort = getConsumerPort();
				} else if (attr.getName().equalsIgnoreCase("replicaBindDN")) {
					setBindDN(val);
				} else if (attr.getName().equalsIgnoreCase(
					"replicaCredentials")) {
					setBindCredentials(val);
				} else if (attr.getName().equalsIgnoreCase(
					"replicaNickname")) {
					setNickname(val);
				} else if (attr.getName().equalsIgnoreCase("replicaUseSSL")) {
					setUseSSL(val);
				} else if (attr.getName().equalsIgnoreCase(
					"replicaEntryFilter")) {
					setEntryFilter(val);
					origEntryFilter = getEntryFilter();
				} else if (attr.getName().equalsIgnoreCase("cn")) {
					cn = val;
				} else if (attr.getName().equalsIgnoreCase(
					"replicaBeginORC")) {
					setORCValue(val);
				}
            }
        }
		if ( cn != null ) {
			setEntryCN(cn);
		}
        //print();
    }


    public int writeToServer() throws IOException {
        LDAPAttribute attr;
        String stmp;
        
        if (agreementDNHasChanged() || agreementIsNew) {

            LDAPAttributeSet newAttrs = new LDAPAttributeSet();

			String[] vals = { "top", SIRAgreementClass };
            attr = new LDAPAttribute( "objectclass", vals );
            newAttrs.add(attr);
            
            attr = new LDAPAttribute("replicaNickname", nickname);
            newAttrs.add(attr);
            attr = new LDAPAttribute("cn", entryCN);
            newAttrs.add(attr);
            
            if (replicatedSubtree == null) {
				attr = new LDAPAttribute("replicaRoot", "");
            } else {
				attr = new LDAPAttribute("replicaRoot", replicatedSubtree);
            }
            newAttrs.add(attr);
            
            attr = new LDAPAttribute("replicaHost", consumerHost);
            newAttrs.add(attr);

            attr = new LDAPAttribute("replicaPort",
									 Integer.toString(consumerPort));
            newAttrs.add(attr);

            if (bindDN != null) {
                attr = new LDAPAttribute("replicaBindDN", bindDN);
                newAttrs.add(attr);
            }

            if (bindCredentials != null) {
                attr = new LDAPAttribute("replicaCredentials",
										 bindCredentials);
                newAttrs.add(attr);
            }

            attr = new LDAPAttribute("replicaUseSSL", useSSL ? "1" : "0");
            newAttrs.add(attr);

            stmp = createReplicatedAttributesList();
            if (stmp != null) {
                attr = new LDAPAttribute("replicatedAttributeList", stmp);
                newAttrs.add(attr);
            }

            if (entryFilter != null && !entryFilter.equals("")) {
                attr = new LDAPAttribute("replicaEntryFilter", entryFilter);
                newAttrs.add(attr);
            }

            String[] sched = getUpdateScheduleStrings();
            if (sched != null) {
				// first, see if any of the values are valid
				boolean valid = false;
                for (int i = 0; !valid && i < sched.length; i++) {
					valid = (sched[i] != null) && (sched[i].length() > 0);
                }
				if (valid) {
					attr = new LDAPAttribute("replicaUpdateSchedule",
											 sched);
					newAttrs.add(attr);
                }
            }
            
            LDAPEntry newEntry = new LDAPEntry(entryDN, newAttrs);
            try {
                createNewEntry(newEntry);
            } catch (LDAPException e) {
                Debug.println("SIRAgreement.writeToServer: <" +
							  entryDN + "> " + e.toString());
                return e.getLDAPResultCode();
            }
            // Delete the old entry
            if (!agreementIsNew) {
                try {
                    deleteOldEntry();
					setOrigEntryDN( getEntryDN() );
                } catch (LDAPException de) {
					Debug.println("SIRAgreement.writeToServer: <" +
								  entryDN + "> " + de.toString());
                    return de.getLDAPResultCode();
                }
            }

        } else {

            LDAPModification mod;
            LDAPModificationSet mods = new LDAPModificationSet();

            attr = new LDAPAttribute("replicaNickname", getNickname());
            mods.add(LDAPModification.REPLACE, attr);

            attr = new LDAPAttribute("cn", entryCN);
            mods.add(LDAPModification.REPLACE, attr);

            attr = new LDAPAttribute("replicaRoot", replicatedSubtree);
            mods.add(LDAPModification.REPLACE, attr);

	    if (bindDN != null) {
		attr = new LDAPAttribute("replicaBindDN", bindDN);
	    } else {
		attr = new LDAPAttribute("replicaBindDN");
	    }
	    mods.add(LDAPModification.REPLACE, attr);

	    if (bindCredentials != null) {
		attr = new LDAPAttribute("replicaCredentials",
			bindCredentials);
	    } else {
		attr = new LDAPAttribute("replicaCredentials");
	    }
	    mods.add(LDAPModification.REPLACE, attr);

            attr = new LDAPAttribute("replicaUseSSL", useSSL ? "1" : "0");
            mods.add(LDAPModification.REPLACE, attr);

            if (getSelattrType() == SELATTR_ALL ||
				selectedAttrs == null ||
				selectedAttrs.size() == 0) {
                attr = new LDAPAttribute("replicatedAttributeList");
                mods.add(LDAPModification.REPLACE, attr);
            } else {
                stmp = createReplicatedAttributesList();
                attr = new LDAPAttribute("replicatedAttributeList", stmp);
                mods.add(LDAPModification.REPLACE, attr);
            }

            if (entryFilter != null && !entryFilter.equals("")) {
                attr = new LDAPAttribute("replicaEntryFilter",
                        entryFilter);
                mods.add(LDAPModification.REPLACE, attr);
            } else {
                // Remove the attribute
                attr = new LDAPAttribute("replicaEntryFilter");
                mods.add(LDAPModification.REPLACE, attr);
            }

            String[] sched = getUpdateScheduleStrings();
            attr = new LDAPAttribute("replicaUpdateSchedule");
            if (sched != null) {
                // first, see if any of the values are valid
                boolean valid = false;
                for (int i = 0; !valid && i < sched.length; i++) {
                    valid = (sched[i] != null) && (sched[i].length() > 0);
                }
                if (valid) {
                    // easier than setting each value separately . . .
                    attr = new LDAPAttribute("replicaUpdateSchedule",
                                             sched);
                } 
            }

            mods.add(LDAPModification.REPLACE, attr);

            try {
                updateEntry(mods);
            } catch (LDAPException me) {
				Debug.println("SIRAgreement.writeToServer: <" +
							  entryDN + "> " + me.toString());
                return me.getLDAPResultCode();
            }
        }
        return 0;
    }
    
	//get resource bundle
    private static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");     
    private static final String SIRAgreementClass = "LDAPReplica";
}
