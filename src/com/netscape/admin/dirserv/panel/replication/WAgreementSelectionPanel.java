/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.admin.dirserv.wizard.*;
import com.netscape.management.client.util.*;


public class WAgreementSelectionPanel extends WAgreementPanel
                                      implements IWizardPanel {
    /*==========================================================
     * constructors
     *==========================================================*/
    public WAgreementSelectionPanel(String type) {
		_helpToken = "replication-wizard-cirsirselect-help";
		_section = "replication-selection";
		String _section2 = "replication-agreementWizard";
		setLayout(new GridBagLayout());
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
		int space = UIFactory.getComponentSpace();
		int different = UIFactory.getDifferentSpace();
		int large = 20;
		int tiny = 2;

        //wizard images
        //======== Top Panel ===========================
        JPanel top = new JPanel();
        top.setLayout(new GridBagLayout());
        top.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        add(top, gbc);

        //add icon
        RemoteImage img = ReplicationTool.getImage(
			_resource.getString(_section2,"wizardIcon"));
        JLabel iconLabel1 = UIFactory.makeJLabel(img);
        ReplicationTool.resetGBC(gbc);
        gbc.fill = gbc.NONE;
        top.add(iconLabel1, gbc);

        //wizard name
        JLabel name = UIFactory.makeJLabel(_section2,"wizard",_resource);
        ReplicationTool.resetGBC(gbc);
        gbc.weightx=1.0;
        gbc.gridwidth = gbc.REMAINDER;
        top.add(name, gbc);

        //==== wizard description =============
        JTextArea desc =
			UIFactory.makeMultiLineLabel( 2, 30,
										  _resource.getString(
											  _section2,"desc") );
        desc.setBackground(getBackground());
        desc.setEditable(false);
        desc.setCaretColor(getBackground());
        gbc.anchor = gbc.NORTH;
        gbc.weightx = 1.0;
        gbc.weighty = 0.1;
        gbc.insets = new Insets(different,large,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        add(desc, gbc);

        //==== agreement type selection panel =======
        JPanel bottom = new JPanel();
        bottom.setLayout(new GridBagLayout());
        bottom.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(10,40,5,10);
        gbc.weightx = 1.0;
        gbc.weighty = 0.9;
        add(bottom, gbc);

        //label
        JLabel select = UIFactory.makeJLabel(
			_section2, "selectType", _resource );
        ReplicationTool.resetGBC(gbc);
        gbc.weightx = 1.0;
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.gridwidth = gbc.REMAINDER;
        bottom.add(select, gbc);
		desc.setFont( select.getFont() );

        //radio buttons
        ButtonGroup group = new ButtonGroup();
        _cirButton = UIFactory.makeJRadioButton(
			this, _section2, "CIR", false, _resource );
        group.add(_cirButton);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(space,large,0,space);
        bottom.add(_cirButton, gbc);
        JTextArea label =
			UIFactory.makeMultiLineLabel( 2, 30,
									  _resource.getString(
										  _section2, "CIR-desc-label" ) );
        gbc.fill = gbc.HORIZONTAL;
        gbc.gridwidth = gbc.REMAINDER;
        bottom.add(label, gbc);

        //Subtree
        _sirButton = UIFactory.makeJRadioButton(
			this, _section2, "SIR", false, _resource );
        group.add(_sirButton);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(space,large,space,space);
        bottom.add(_sirButton, gbc);
        label =
			UIFactory.makeMultiLineLabel( 2, 30,
										_resource.getString(
										  _section2, "SIR-desc-label" ) );
        gbc.insets = new Insets(0,large,space,space);
        gbc.fill = gbc.HORIZONTAL;
        gbc.gridheight = gbc.REMAINDER;
        bottom.add(label, gbc);

        if (type.equals (AgreementWizardInfo.CIR_AGREEMENT)){
            _cirButton.setSelected(true);
            _cirButton.requestFocus();
        }else{ /* SIR */
            _sirButton.setSelected(true);
            _sirButton.requestFocus();
        }
    }

	/*==========================================================
	 * public methods
     *==========================================================*/

    //====== IWizardPanel =============================
    public boolean initializePanel(WizardInfo info) {
        Debug.println("AgreementSelectionPanel: Initialize()");
        _wizardInfo = (AgreementWizardInfo) info;
        if ((!_initialized) &&
			(_wizardInfo.getAgreementType()!= null)) {
            if (_wizardInfo.getAgreementType().equals(
				_wizardInfo.SIR_AGREEMENT))
                _sirButton.setSelected(true);
        }
        _initialized = true;
        return true;
    }

    public boolean validatePanel() {
        return true;
    }

    public boolean concludePanel(WizardInfo info) {
        return true;
    }

    public void getUpdateInfo(WizardInfo info) {
        Debug.println("AgreementSelectionPanel: getUpdateInfo()");
        if (_cirButton.isSelected()) {
            _wizardInfo.setAgreementType(_wizardInfo.CIR_AGREEMENT);  
        } else {
            _wizardInfo.setAgreementType(_wizardInfo.SIR_AGREEMENT);
        }
    }

    public String getErrorMessage() {
        return "AgreementSelectionPanel: Testing";
    }

    /*==========================================================
     * variables
     *==========================================================*/
    private JRadioButton _cirButton, _sirButton;
    private AgreementWizardInfo _wizardInfo;
    private boolean _initialized = false;
}
