/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.beans.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.admin.dirserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import java.util.*;

/**
 * Status Display Dialog
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/20/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class ProgressStatusDialog extends JDialog implements ActionListener
{
    /*==========================================================
     * constructors
     *==========================================================*/    
    ProgressStatusDialog(JFrame parent, String title, Thread reader) {
        super(parent, true);
        setSize(350, 150);
        setTitle(_resource.getString("replication-dialog","processing"));
        setLocationRelativeTo(parent);
        //getRootPane().setDoubleBuffered(true);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().setBackground(getBackground());
        _reader = reader;

		JPanel centerPanel = new JPanel();
		getContentPane().add("Center",centerPanel);

		GridBagLayout gb = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		centerPanel.setLayout(gb);

		//desc
        JLabel desc = new JLabel(title);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx=1.0;
        gbc.insets = new Insets(5,20,5,10);
        gb.setConstraints(desc, gbc);
        centerPanel.add(desc);

        //progress bar
		_bar1 = new JProgressBar();
		_bar1.setMinimum(0);
		_bar1.setMaximum(100);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.insets = new Insets(5,20,5,10);
        gb.setConstraints(_bar1, gbc);
        centerPanel.add(_bar1);
        
        //button panel
		JPanel buttonPanel = new JPanel();
		getContentPane().add("South",buttonPanel);
        buttonPanel.setLayout( new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.setAlignmentX(LEFT_ALIGNMENT);
        buttonPanel.setAlignmentY(TOP_ALIGNMENT);
        buttonPanel.add(Box.createGlue());
        
        _bCancel = new JButton();
        _bCancel.setText(DSUtil._resource.getString("general", "Cancel-label"));
        _bCancel.setPreferredSize(BUTTON_MIN_SIZE);
        _bCancel.addActionListener(this);
        buttonPanel.add(_bCancel);
        buttonPanel.add(Box.createGlue());
        
        /* Cancel if the window is closed */
		addWindowListener( new winAdapter() );
    }

	class winAdapter extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			setVisible(false);
			dispose();
			DSUtil.dialogCleanup();
			_isOk = false;
		}
    }
    
	/*==========================================================
	 * public methods
     *==========================================================*/
    /**
     * @return true if successful
     */
    public boolean isOk() {
        return _isOk;
    }
   
   /**
    * close this dialog
    */
    public void close() {
        setModal(false);
        //setVisible(false);
        dispose();    
		DSUtil.dialogCleanup();
    }
    
    // === ACTIONLISTENER ===============
    public void actionPerformed(ActionEvent e) {
        //Debug.println(e.toString());
        if (e.getSource().equals(_bCancel) || e.getActionCommand().equals("close")) {
            if (_reader != null)
                _reader.stop();
            _isOk = false;
            setModal(false);
            setVisible(false);
            dispose();
			DSUtil.dialogCleanup();
		}
    }
 
    /**
     * Adjust the progress percentage.
     * percentage of 100 will dispose the dialog
     * @param percentage completed
     */
    public void setPercent(int percent) {
        _bar1.setValue(percent);
		Rectangle r = _bar1.getBounds();
		_bar1.paintImmediately(r);
		if (percent >= 100)
		    _isOk = true;
	}
	
	/*==========================================================
	 * self-test methods
     *==========================================================*/	
	
	//Test Program
    public static void main(String arg[]) {
        ProgressStatusDialog dialog = 
            new ProgressStatusDialog(new JFrame(), "Reading Stuff...", null);
        dialog.show();
        System.exit(0);
    }
    
    /*==========================================================
     * variables
     *==========================================================*/     
    private static final Dimension BUTTON_MIN_SIZE = new Dimension(100,30);
    private JProgressBar _bar1;
    private JButton _bCancel;
    private boolean _isOk = false;
    private Thread _reader;
 
    //get resource bundle
    private static ResourceSet _resource =
	    new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
}

