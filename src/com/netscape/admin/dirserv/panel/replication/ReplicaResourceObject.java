/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/**
 * Representation of a Replica node
 *
 * @author  richm
 * @version %I%, %G%
 * @date	 	2/24/00
 * @see     com.netscape.admin.dirserv.panel.replication
 */

package com.netscape.admin.dirserv.panel.replication;

import java.util.Enumeration;
import java.util.Vector;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSContentListener;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.node.DSResourceObject;
import com.netscape.management.client.console.ConsoleInfo;
import netscape.ldap.*;


public class ReplicaResourceObject extends DSResourceObject
                            implements IMenuInfo,
	                                   ActionListener,
	                                   TreeExpansionListener,
                                       IDSContentListener,
                                       IReplicationResourceObject {
	public ReplicaResourceObject(IDSModel model, ResourceObject parent,
								  LDAPEntry instEntry, LDAPEntry mapTreeEntry) {
		super(LDAPDN.explodeDN(instEntry.getDN(), true)[0],
			  _replicaIcon,
			  _replicaIcon,
			  model);		
		_parent = parent;
		_instEntry = instEntry;
		_mapTreeEntry = mapTreeEntry;
		_isLeaf = checkIfLeaf();
    }

	/* This method looks in the server to see if there are replication agreements
	   for this Replica.
	   It is used on the constructor to initialize correctly _isLeaf */
	private boolean checkIfLeaf() {
		AgreementTable agmntTable = new AgreementTable();
    AgreementTable WinagmntTable = new AgreementTable();
		LDAPEntry replicaEntry = getReplicaEntry(); 
  
		if (replicaEntry != null) {
			AgreementReader reader = new AgreementReader(_model, 
														 agmntTable,
														 getReplicaReadDN());
    
			reader.readAgreements();
      
			if (reader.isSuccess()) {
				//add agreements
				agmntTable = reader.getAgreements();
				Enumeration e = agmntTable.elements();
				if (e.hasMoreElements()) {
					return false;
				}
			}
		}
    
		return true;
	}

    public IDSModel getModel() {
		return _model;
    }
										   
    public Component getCustomPanel() {
		Debug.println(8, "ReplicaResourceObject.getCustomPanel: panel = " +
					  _panel + " replica entry = " + _replicaEntry);
		if (_panel == null) {
			_panel = new ReplicaConfigTabPanel(_model, this, _instEntry,
											   _mapTreeEntry, getReplicaEntry());		
		}
		return _panel;
    }

	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
				ResourcePage.MENU_CONTEXT
				};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
		String section = "replication-node-agreementPopupMenu";
		return new IMenuItem[]{
			new MenuItemText( NEW,
			    _resource.getString( section, "New"),
                            _resource.getString( section,"New-description")),
      
                        new MenuItemText( NEWWINDOW, 
                            _resource.getString(section, "NewWin"), 
                            _resource.getString(section, "NewWin-description")), //XXX
      
			new MenuItemSeparator(),
			new MenuItemText( DSResourceModel.REFRESH,
                            DSUtil._resource.getString("menu", "refresh"),
                            DSUtil._resource.getString("menu", "refresh-description")),
				};
    }
										   
    public void actionMenuSelected(IPage viewInstance, IMenuItem item){
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((DSResourceModel)_model).actionMenuSelected(viewInstance, item);
		} else {
      
			/* We use this variables to keep the tree expanded OR to expand it 
			   automatically if a new replication agreements has been added */
			int oldChildCount = getChildCount();
			boolean wasCollapsed = _isTreeCollapsed;
			if (! ReplicationTool.authenticate(_model))
				return;
// XXX check 			item.getID(); here
                           
			if (getReplicaEntry() == null && item.getID().equals(NEW)) {
				// the backend is not configured as a replica; tell the
				// user to configure this as a replica
				ReplicationTool.displayError(
											 _model.getFrame(), "replication-err-noReplicaOrChangelog",
											 "title", "msg", null);
				return;
      }
			
      if (getReplicaEntry() == null && item.getID().equals(NEWWINDOW) ) {
          
          	ReplicationTool.displayError(
											 _model.getFrame(), "replication-err-noReplicaOrChangelog", //XXX
											 "title", "msg", null);
				return;
      }
      
			/* In order to configure a replication agreement the change log needs
			   to be configured */
			boolean logChangesExists = false;
			try {
				LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
				LDAPEntry entry = ldc.read(SupplierSettingPanel.CHANGELOG_DN);
				if (entry != null) {
					logChangesExists = true;
				}
			} catch (LDAPException lde) {
			}
			if (!logChangesExists) {
				ReplicationTool.displayError(
											 _model.getFrame(), "replication-err-noReplicaOrChangelog",
											 "title", "msg", null);
				return;
			}
			
      
			  if(item.getID().equals(NEW)) {
			AgreementWizardInfo info = new AgreementWizardInfo();
			
			info.setServerInfo(_model.getServerInfo());
			info.setConsoleInfo(_model.getConsoleInfo());
			info.setParentNode(this);
			info.setWizardType(info.NEW_WIZARD);
			info.setLDAPSchema(_model.getSchema());
			if (_instEntry != null) {
				String subtree = DSUtil.getAttrValue(
													 _instEntry, ReplicationTool.PUBLIC_SUFFIX_ATTR);
				info.setSubtree(subtree);
			}
			info.setReplicaEntry(getReplicaEntry());
	
			AgreementWizard wizard =
				new AgreementWizard(_model.getFrame(), info);
		
    } else if(item.getID().equals(NEWWINDOW)) {

      	AgreementWizardInfo info = new AgreementWizardInfo();
        info.setConsoleInfo(_model.getConsoleInfo());
		info.setServerInfo(_model.getServerInfo());
        info.setParentNode(this);
			info.setWizardType(info.NEW_WIZARD); 
			info.setLDAPSchema(_model.getSchema());
			if (_instEntry != null) {
				String subtree = DSUtil.getAttrValue(_instEntry, ReplicationTool.PUBLIC_SUFFIX_ATTR);
				info.setSubtree(subtree);
			}
			info.setWindowsReplicaEntry( getWindowsReplicaEntry() );
			
			WindowsAgreementWizard wizard = new WindowsAgreementWizard(_model.getFrame(), info);
		
      
      
    }
        	int newChildCount = getChildCount();
			
			/* If the number of children is bigger now: a new replication agreements has been added => WE EXPAND.
			 If it was Expanded we EXPAND to keep the previous state*/
			if ((newChildCount > oldChildCount) ||
				!wasCollapsed) {
				expandPath((ResourcePage)viewInstance);
      }
        
    } //REPLICA
    }
										   
										   
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {
			Debug.println("ReplicaResourceObject.actionPerformed(): Refresh Selected");			
			reload();
			refreshTree();			
		}
    }


    // get the entry corresponding to this replica, if any
    public LDAPEntry getReplicaEntry() {
		if (_mapTreeEntry != null && _replicaEntry == null) 
			try {
				 String replicaDN = ReplicationTool.REPLICA_RDN + "," + _mapTreeEntry.getDN();
        //  String replicaDN =  _mapTreeEntry.getDN();
				LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
				_replicaEntry = ldc.read(replicaDN);
			} catch (LDAPException lde) {
			}
			return _replicaEntry;
    }
    
     public LDAPEntry getWindowsReplicaEntry() {
		if (_mapTreeEntry != null && _winreplicaEntry == null) 
			try {
				String replicaDN = ReplicationTool.WINDOWS_REPLICA_RDN +"," + _mapTreeEntry.getDN();
				LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
				_winreplicaEntry = ldc.read(replicaDN);
			} catch (LDAPException lde) {
			}
			return _winreplicaEntry;
    }

    /**
     *	Called when user wants to execute this object, invoked by a
     *  double-click or a menu action.
     */
    public boolean run(IPage viewInstance) {
		Debug.println("ReplicaResourceObject: run()");
        reload();
        if (super.getChildCount() != 0) {            
			expandPath((ResourcePage)viewInstance);
        }
        refreshTree();
		return true;
    }

    public boolean run(IPage viewInstance, IResourceObject selectionList[]) {
		return run( viewInstance );
    }
	
    /**
     * retrieve replication agreements from the replication server
     */
    public void reload() {
		_replicaEntry = null; // force recalculation of rep entry
		Debug.println(8, "ReplicaResourceObject.reload: BEGIN");
		cleanTree();
		// set status bar in progress. Can't use framework status bar YET!!!
		// pop up in progress dialog status bar
		if (! ReplicationTool.authenticate(_model))
			return;
		
		// add replication agreement nodes
		Debug.println(8, "ReplicaResourceObject.reload: adding agreement nodes");
		addAgreementNodes();
		Debug.println(8, "ReplicaResourceObject.reload: END");
    }

    private void addAgreementNodes() {
		_isLeaf = true;
		if (getReplicaEntry() == null) { // not a replica, so no agreements
			return;
		}
		_model.fireChangeFeedbackCursor(
				null, FeedbackIndicator.FEEDBACK_WAIT );

    AgreementTable agmntTable = new AgreementTable();
    
    //changed from cn=replica,cn="dn",cn=config
    //          to            cn="dn",cn=config
    AgreementReader reader = new AgreementReader(_model, agmntTable, getReplicaReadDN());
    

		reader.readAgreements();
   
		if (reader.isSuccess() ) {
			//add agreements
			agmntTable = reader.getAgreements();
      Enumeration e = agmntTable.elements();
 
    	while( e.hasMoreElements() ) {
				Object obj = e.nextElement();
				AgreementResourceObject res = null;
				if ( obj instanceof MMRAgreement) {
					Debug.println("Add MMR Node");
					
					res = 
						new AgreementResourceObject(
													((ReplicationAgreement)obj).getNickname(),
													ReplicationTool.getImage("ds-rep-get-16.gif"),
													ReplicationTool.getImage("ds-rep-get-32.gif"),
													this,
													_parent);
					res.setAgreement((ReplicationAgreement)obj);
        }
        if (obj instanceof ActiveDirectoryAgreement) {
            Debug.println("Add AD Node");
            		res = 
						new AgreementResourceObject(
													((ReplicationAgreement)obj).getNickname(),
													ReplicationTool.getImage("ds-rep-get-16.gif"),
													ReplicationTool.getImage("ds-rep-get-32.gif"),
													this,
													_parent);
					res.setAgreement((ReplicationAgreement)obj);
        }
        
				if (obj instanceof SIRAgreement){
					Debug.println("Add Legacyr Node");
					res = 
						new AgreementResourceObject(
													((ReplicationAgreement)obj).getNickname(),
													ReplicationTool.getImage("ds-rep-push-16.gif"),
													ReplicationTool.getImage("ds-rep-push-32.gif"),
													this);
					res.setAgreement((ReplicationAgreement)obj);
				} else {
					Debug.println("Class is "+obj.getClass().getName());
				}
				if (res != null) {
					//_agreements.addElement(res);
					_isLeaf = false;
					add(res); 
				}
			}

      
      
      refreshTree();
      _isLoaded = true;
    }
    _model.fireChangeFeedbackCursor(
    null, FeedbackIndicator.FEEDBACK_DEFAULT );
}

    /**
     * see if the replicas are loaded
     * @return true, if loaded
     */
    public boolean isLoaded() {
		return _isLoaded;
    }
	
    /**
     * Override isLeaf so node will show up as expandable
     */
										   
    public boolean isLeaf() {		
		return _isLeaf;
    }
    
	/**
	 * Called when this object is selected.
     * Called by: ResourceModel
	 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		super.select( viewInstance );
	}

    /**
     * Refresh the tree view
     */
    void refreshTree() {	
		_model.fireTreeStructureChanged(this);
    }

    /**
     * Called primarily by children any peers to let us know that the children
     * have changed in some way i.e. agreement added or deleted, or the list
     * of children needs to be resorted and redisplayed
     */
	 
    public void contentChanged() {		
		reload();
		refreshTree();		
    }
	
    /**
     * Expand the tree path view
     */
    public void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
		page.expandTreePath(path);
    }
  
    /**
     * Remove all agreements from the tree model and agreement table
     */
    private void cleanTree() {	
		removeAllChildren();    
    }

	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( equals( o ) ) {
			if ( !_isInitiallyExpanded ) {
				/* Prevent recursion caused by refreshTree() in reload() */
				_isInitiallyExpanded = true;
				Debug.println( "ReplicaResourceObject.treeExpanded: this" );
				if ( !isLoaded() ) {
					reload();
				}
			}
		}
		_isTreeCollapsed = false;
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {
		_isTreeCollapsed = true;
	}
  
  public String getReplicaWriteDN() {
      String replicaDN =  _mapTreeEntry.getDN();
      return replicaDN;
  }
  
  public String getReplicaReadDN() {
      String replicaDN =  _mapTreeEntry.getDN();
      return replicaDN;
  }

	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;

    private ResourceObject _parent = null;
    private Component _panel = null;
    private boolean _isLoaded = false;
    private LDAPEntry _instEntry = null;
    private LDAPEntry _mapTreeEntry = null;
    private LDAPEntry _replicaEntry = null;
private LDAPEntry _winreplicaEntry = null;
	private boolean _isLeaf = false;
	private boolean _isInitiallyExpanded = false;
	private boolean _isTreeCollapsed = true;						   

    // same as a database instance
    static private final String _replicaImageName = "dbobj.gif";
    static private RemoteImage _replicaIcon = DSUtil.getPackageImage( _replicaImageName );
    protected static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");    

    static final String NEW     = "new";
    static final String NEWWINDOW = "newwindow";
}
