/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import netscape.ldap.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;


/**
 * Thread for Replication Agreement Reading
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/20/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class AgreementReader extends Thread {

    private ConsoleInfo _bindInfo;
    private IDSModel _owner;
    private ProgressStatusDialog _dialog;
    private int _type;
    private AgreementTable _agreements;
    private boolean _success = false;
	private String _baseDN = null;

    /**
     * Constructs a new agreement reader object.  The agreement reader is
     * a thread which knows how to connect to the local server and reads
     * replication agreements.
     */
    public AgreementReader(IDSModel owner,
						   AgreementTable table,
						   String baseDN) {
        _owner = owner;
        _bindInfo = owner.getServerInfo();
        _agreements = table;
		_baseDN = baseDN;
    }
    
    /**
     * Constructs a new agreement reader object.  The agreement reader is
     * a thread which knows how to connect to the local server and reads
     * replication agreements.
     */
    public AgreementReader(ConsoleInfo info, AgreementTable table,
						   String baseDN) {
        _bindInfo = info;
        _agreements = table;
		_baseDN = baseDN;
    }
    
    /**
     * Set Status Display Object
     * @param dialog status display object
     */
    public void setDisplay(ProgressStatusDialog dialog) {
        _dialog = dialog;
    }
    
    /**
     * Verify Status
     * @return true if reading successful; otherwise, false
     */
    public boolean isSuccess() {
        return _success;
    }
    
    /**
     * Get Agreement Tbale
     * @return agreement table
     */
    AgreementTable getAgreements() {
        return _agreements;
    }

    /**
     * Update Status Display 
     * @param percentage
     */
    void updateStatus(int percent) {
        if (_dialog != null)
            _dialog.setPercent(percent);       
    }
    
    /**
     * Connect to the local server and read all replication agreements
     * from the directory.
     */
    public void readAgreements() {
        Debug.println("AgreementReader: start readAgreements()");
        Vector store;
        LDAPConnection ld = null;
        ld = _bindInfo.getLDAPConnection();

        if (ld == null) {
            Debug.println("ERROR: AgreementReader: readAgreements() - " +
						  "Connection Failed");
            terminate(_resource.getString("replication",
										  "dialog-cannotConnect"));
            return;
        }
		
		try {
		    updateStatus(10);
		    store = getFromServer(ld, _baseDN);
		    updateStatus(50);
		    processAgreements(store);
		} catch (LDAPException e) {
		    Debug.println("ERROR: AgreementReader: readAgreements() "+
						  e.toString());
		    terminate(_resource.getString("replication",
										  "dialog-cannotConnect"));    
		}
		_success = true;
		Debug.println("AgreementReader: end readAgreements()");
    }

    /**
     * Run
     */
    public void run() {
        yield();
		_success = false;
        readAgreements();
        Debug.println(8, "AgreementReader: run() - Done Reading...");
        if (_dialog!= null)
            _dialog.close();
    }
    
    /**
     * Cleanup routine for forced termination
     */
    private void terminate(String msg) {
        if (_dialog != null)
            _dialog.close();
        JFrame frame;
        if (_owner!= null)
            frame = _owner.getFrame();
        else
            frame = null;
		DSUtil.showErrorDialog( frame, "error", msg,
								"replication-dialog" );
    }

    /**
     * Given an open connection and a base dn, search the directory for all
     * replication agreements. Returns vector of ldap entries for later
	 * processing.
     *
     * @param ld LDAPconnection
     * @param basedn Base DN
     * @return list of ldap entries
     */
    private Vector getFromServer(LDAPConnection ld, String basedn)
		throws LDAPException {
        LDAPSearchResults reslist = null;
        LDAPEntry entry;
        String[] attrs = {"objectclass"};
        LDAPSearchConstraints searchConstraints;
        Vector store = new Vector();
        String filter = "|(objectclass=" +
			ReplicationTool.MMR_AGREEMENT_OBJECTCLASS +
			")(objectclass=" +
			ReplicationTool.LEGACYR_AGREEMENT_OBJECTCLASS +
			")(objectclass=" +
        ReplicationTool.WIN_AGREEMENT_OBJECTCLASS + ")";
        
		int scope = ld.SCOPE_SUB;
                
        // Search directory
        Debug.println("AgreementReader: getFromServer()");
        Debug.println("     Agreement Base DN: "+basedn);
        Debug.println("     Filter:            "+filter);
        
        try {
            reslist = ld.search(basedn, scope, filter, null, false);
        } catch (LDAPException e) {
            ld = null;
            String alertString;
            if (e.getLDAPResultCode() == -1) {
                alertString = _resource.getString("replication",
												  "err-ReadAgreementsError");
            } else {
                alertString = _resource.getString("replication",
												  "err-ReadAgreementsError") +
					" \n (" + e.getLDAPResultCode() + ") " +
					e.errorCodeToString();
            }
            terminate(alertString);
        } catch (java.lang.NullPointerException e) {
            Debug.println("AgreementReader: getFromServer() "+e.toString());
        }

        boolean done = false;
        while ((reslist != null) && reslist.hasMoreElements()) {
            store.addElement(reslist.next());
        }

        //Debug.println("Number of Agreements: "+store.size());
        return store;
    }
    
    /**
     * Process LDAP Entry into agreements.
     * Also increment the status if display is specified.
     *
     * @param list of ldap entries
     */
    private void processAgreements(Vector list) {
        ReplicationAgreement agreement;
        
        //if no result
        if (list.size() == 0) {
            updateStatus(100);
            return;
        }
        
        //calculate percentage
        int current, inc;
        if (list.size() >= 50) {
            current = ( list.size() % 50 ); 
            inc = (50 - current)/50;
            current = current+50;
        } else {
            current = 50;
            inc = ( 50/list.size() )+1;
        }
        
        //loop through the list and create replication agreement objects
        for(int i=0; i< list.size(); i++) {
            
            //do agreement
            agreement = entry2Agreement((LDAPEntry)list.elementAt(i));
            if (agreement != null) {
                _agreements.addAgreement(agreement);
            }
            
            //update status
            if (_dialog != null) {
                current = current + inc;
                if ( current >= 100) {
                    updateStatus(100);
                    _success = true;
                } else {
                    updateStatus(current);
                }
            }
        }
    }
    
    /**
     * Convert an LDAP entry to a replication agreement. Actual parsing is
     * performed by the agreement object constructor.
     * 
     * @param entry ldap entry
     * @return replication agreement
     */
    private ReplicationAgreement entry2Agreement(LDAPEntry entry) {
        LDAPAttributeSet attrSet;
        Enumeration attrsEnum;

        if (entry == null) {
            return null;
        }

		LDAPAttribute attr = entry.getAttribute( "objectclass" );
		if ( attr != null ) {
			Enumeration vals = attr.getStringValues();
			while (vals.hasMoreElements()) {
				String val = (String)vals.nextElement();
				if (val.equalsIgnoreCase(
						ReplicationTool.LEGACYR_AGREEMENT_OBJECTCLASS)) {
					return new SIRAgreement(_bindInfo, entry, null);
				} else if (val.equalsIgnoreCase(
						ReplicationTool.MMR_AGREEMENT_OBJECTCLASS)) {
					return new MMRAgreement(_bindInfo, entry, null);
				} else if (val.equalsIgnoreCase(
        ReplicationTool.WIN_AGREEMENT_OBJECTCLASS)) {
            return new ActiveDirectoryAgreement(_bindInfo, entry,null); // XXX dont forget nt
            
        }
      }
        }
        return null;
    } 

	//get resource bundle
    private static ResourceSet _resource =
    new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
}
