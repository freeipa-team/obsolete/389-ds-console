/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.admin.dirserv.wizard.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.RemoteImage;
import netscape.ldap.*;


/**
 * Information Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/11/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class WAgreementInfoPanel extends WAgreementPanel
                                 implements IWizardPanel {

    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    public WAgreementInfoPanel() {
        super();
		_helpToken = "replication-wizard-cirsirname-help";
		_section = "replication-info";
    }

	/*==========================================================
	 * public methods
     *==========================================================*/

    public void init() {
		GridBagLayout gb = new GridBagLayout();
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
		setLayout(gb);

        JLabel ask = UIFactory.makeJLabel("replication",
										  "info-ask",
										  _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.insets = UIFactory.getBorderInsets();
        gbc.insets.left = UIFactory.getComponentSpace();
        gb.setConstraints(ask, gbc);
        add(ask);

        //======== Top Panel ===========================
        JPanel top = new JPanel();
        top.setLayout(new GridBagLayout());
        top.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gbc.weighty = 0.1;
        add(top, gbc);

        //add icon
        RemoteImage img1, img2;
        String desc1, desc2;
        img1 = ReplicationTool.getImage(_resource.getString(
			_section, "cirIcon"));
        desc1 = _resource.getString(_section,"cirDesc");
        img2 = ReplicationTool.getImage(_resource.getString(
			_section, "sirIcon"));
        desc2 = _resource.getString(_section, "sirDesc");

        iconLabel1 = UIFactory.makeJLabel(img1);
        ReplicationTool.resetGBC(gbc);
        gbc.fill = gbc.NONE;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS;
        top.add(iconLabel1, gbc);

        iconLabel2 = UIFactory.makeJLabel(img2);
        ReplicationTool.resetGBC(gbc);
        gbc.fill = gbc.NONE;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS;
        top.add(iconLabel2, gbc);
        
        JPanel titlePanel = new JPanel();
        GridBagLayout gb1a = new GridBagLayout();
        titlePanel.setLayout(gb1a);
        titlePanel.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.weightx = 1.0;
        gbc.gridwidth = gbc.REMAINDER;
        top.add(titlePanel, gbc);

        JLabel _nameText = UIFactory.makeJLabel("");
        ReplicationTool.resetGBC(gbc);
        gbc.weightx=1.0;
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS;
        titlePanel.add(_nameText, gbc);

        descLabel1 = UIFactory.makeJLabel(desc1);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.gridheight = gbc.REMAINDER;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS;
        gbc.weightx=1.0;
        titlePanel.add(descLabel1, gbc);
        
        descLabel2 = UIFactory.makeJLabel(desc2);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.gridheight = gbc.REMAINDER;
        gbc.insets = ReplicationTool.DEFAULT_EMPTY_INSETS;
        gbc.weightx=1.0;
        titlePanel.add(descLabel2, gbc);

        //name
        JLabel name = UIFactory.makeJLabel("replication",
										   "destination-name",
										   _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        gbc.insets = UIFactory.getComponentInsets();
        gbc.insets.bottom = 0;
	    gbc.insets.top = UIFactory.getDifferentSpace();
        top.add(name, gbc);

        _nameTextField= UIFactory.makeJTextField(null, "");
        _activeColor = _nameText.getBackground();
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx= 1.0;
        gbc.insets = UIFactory.getBorderInsets();
        gbc.insets.left = UIFactory.getComponentSpace();
        gbc.insets.bottom = 0;
        gbc.insets = new Insets(10,5,0,10);
        top.add(_nameTextField, gbc);

        populateData();
        _initialized = true;
    }

    //====== IWizardPanel =============================
    public boolean initializePanel(WizardInfo info) {
        Debug.println("WAgreementInfoPanel: initialize()");
        _wizardInfo = (AgreementWizardInfo) info;
        if (!_initialized)
            init();
        if (_wizardInfo.getAgreementType().equals(_wizardInfo.LEGACYR_AGREEMENT)) {
            setLEGACYR(true);
        } else {
            setLEGACYR(false);
        }
        return true;
    }

    public boolean validatePanel() {
        Debug.println("WAgreementInfoPanel: conclude()");
        if (_nameTextField.getText().equals("")) {
            _error = _resource.getString(_section, "dialog-nameEmpty");
            return false;
        }
       
        return true;
    }

    public boolean concludePanel(WizardInfo info) {
        Debug.println("WAgreementInfoPanel: conclude()");
        return true;
    }

    public void getUpdateInfo(WizardInfo info) {
        Debug.println("WAgreementInfoPanel: getUpdateInfo()");
        _wizardInfo.setName(_nameTextField.getText());
    }
    
    public String getErrorMessage() {
        return _error;
    }

    /*==========================================================
	 * private methods
	 *==========================================================*/

	/**
	 * Populate the UI with data
	 */
	private void populateData() {
        if ((_wizardInfo.getName()!= null) &&
			(!_wizardInfo.getName().equals(""))) {
            _nameTextField.setText(_wizardInfo.getName());
        }
    }

    private void setLEGACYR(boolean x) {
        iconLabel1.setVisible(x);
        iconLabel2.setVisible(!x);
        descLabel1.setVisible(x);
        descLabel2.setVisible(!x);
    }
    
    /*==========================================================
     * variables
     *==========================================================*/
    private Color _activeColor;
    private AgreementWizardInfo _wizardInfo;
    private boolean _initialized = false;
    private String _error = "WAgreementSchedulePanel: error message";

    private JTextField _nameTextField;
    private JLabel iconLabel1, iconLabel2, descLabel1, descLabel2;
}
