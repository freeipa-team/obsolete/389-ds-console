/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.Vector;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.admin.dirserv.wizard.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import netscape.ldap.*;

/**
 * Schedule Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class WAgreementSchedulePanel extends WAgreementPanel
    implements IWizardPanel, ActionListener, DocumentListener
{
    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    public WAgreementSchedulePanel(AgreementWizard wizard) {
        super();
		_helpToken = "replication-wizard-schedule-help";
		_section = "replication-schedule";
		_wizard = wizard;
    }

	/*==========================================================
	 * public methods
     *==========================================================*/

    /**
	 * Actual panel construction
	 */
    public void init() {
		GridBagLayout gb = new GridBagLayout();
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
		setLayout(gb);
        setPreferredSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        setMaximumSize(ReplicationTool.DEFAULT_PANEL_SIZE);
		int space = UIFactory.getComponentSpace();
		int different = UIFactory.getDifferentSpace();
		int large = 20;
		int tiny = 2;
        
        JLabel ask = UIFactory.makeJLabel( _section, "ask", _resource );
		ask.setLabelFor(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(different,space,different,different);
        add(ask, gbc);

        check = UIFactory.makeJLabel( _section, "checkSync", _resource );
		gbc.gridwidth = gbc.REMAINDER;
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        add(check, gbc);

        _intervalText = UIFactory.makeJTextField(null, "5", 3);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        add(_intervalText, gbc);

        //in sync button
        ButtonGroup timeGroup = new ButtonGroup();
        _inSync = UIFactory.makeJRadioButton(
			this, _section, "inSyncButton", false, _resource );
        timeGroup.add(_inSync);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(0,different,0,different);
        add(_inSync, gbc);

		//every radio
        _syncOn = UIFactory.makeJRadioButton(
			this, _section, "syncOnButton", false, _resource );
        timeGroup.add(_syncOn);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(0,different,0,different);
        add(_syncOn, gbc);

        //sync on date
        dayPanel = new JPanel();
        dayPanel.setLayout(new GridBagLayout());
        dayPanel.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;        
        add(dayPanel, gbc);

        JPanel timePanel = new JPanel();
        timePanel.setLayout(new GridBagLayout());
        timePanel.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        add(timePanel, gbc);

        //day buttons
        makeDayButton(1, "dateMon");
        makeDayButton(2, "dateTue");
        makeDayButton(3, "dateWed");
        makeDayButton(4, "dateThu");
        makeDayButton(5, "dateFri");
        makeDayButton(6, "dateSat");
        makeDayButton(0, "dateSun");

		gbc.weightx = 1.0;
        gbc.fill = gbc.NONE;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets.right = gbc.insets.left;
		gbc.anchor = gbc.EAST;
        _allButton = UIFactory.makeJButton(
			this, _section, "allButton", _resource);        
        dayPanel.add(_allButton, gbc);
		check.setLabelFor(dayPanel);

        //between
        JLabel between = UIFactory.makeJLabel(_section, "between", _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        timePanel.add(between, gbc);

        //add time selector
        _startHr = UIFactory.makeJTextField(null,"00",3);
		between.setLabelFor(_startHr);
		_startHr.getDocument().addDocumentListener(this);
        _activeColor = _startHr.getBackground();
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        timePanel.add(_startHr, gbc);

        _startMin = UIFactory.makeJTextField(null,"00",3);
		between.setLabelFor(_startMin);
		_startMin.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        timePanel.add(_startMin, gbc);

        JLabel and = UIFactory.makeJLabel(_section, "and", _resource);		
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        timePanel.add(and, gbc);		

        _endHr = UIFactory.makeJTextField(null,"00",3);
		and.setLabelFor(_endHr);
		_endHr.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        timePanel.add(_endHr, gbc);

        _endMin = UIFactory.makeJTextField(null,"00",3);
		and.setLabelFor(_endMin);
		_endMin.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        timePanel.add(_endMin, gbc);

		gbc.fill = gbc.HORIZONTAL;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;
		timePanel.add(Box.createHorizontalGlue(), gbc);

        populateData();
        _initialized = true;
    }

    //========= ACTIONLISTENER =================
    public void actionPerformed(ActionEvent e) {
        Debug.println("AgreementSchedulePanel: actionPerformed()"+e.toString());
        if (e.getSource().equals(_allButton)) {
            checkDate(true);
        }
        if (e.getSource().equals(_inSync)) {
            disableTimeSelection();
        }
        if (e.getSource().equals(_syncOn)) {
            enableTimeSelection();
        }
		checkNextButton();
    }

    //====== IWizardPanel =============================
    public boolean initializePanel(WizardInfo info) {
        Debug.println("WAgreementSchedulePanel: initialize()");
        _wizardInfo = (AgreementWizardInfo) info;
        if (!_initialized)
            init();
        if (_wizardInfo.getAgreementType().equals(_wizardInfo.LEGACYR_AGREEMENT)) {
            setTimeIntervalVisible(true);
            _intervalText.setText(Integer.toString(
				_wizardInfo.getSyncInterval()));
        } else {
            setTimeIntervalVisible(false);   
        }
		checkNextButton();
        return true;
    }

    public boolean validatePanel() {
        Debug.println("WAgreementSchedulePanel: validate()");
        if (_syncOn.isSelected()) {
            boolean check = false;
            for (int i=0; i<7; i++) {
                if (_dayButton[i].isSelected())
                    check = true;
            }
            if (!check) {
                _error = _resource.getString(_section, "noneSelected");
                return false;
            }
            int hr1, hr2, min1, min2;
            try {
                hr1 = Integer.parseInt(_startHr.getText());
                hr2 = Integer.parseInt(_endHr.getText());
                min1 = Integer.parseInt(_startMin.getText());
                min2 = Integer.parseInt(_endMin.getText());
            } catch(NumberFormatException e) {
                _error = _resource.getString(_section,"timeFormat");
                return false;
            }
            if ( (hr1<0)||(hr1>23)||(hr2<0)||(hr2>23)) {
                _error = _resource.getString(_section,"hrFormat");
                return false;
            }
            if ( (min1<0)||(min1>59)||(min2<0)||(min2>59)) {
                _error = _resource.getString(_section,"minFormat");
                return false;
            }
        }
        
        if (_wizardInfo.getAgreementType().equals(_wizardInfo.LEGACYR_AGREEMENT)) {
            try {
                int value = Integer.parseInt(_intervalText.getText());
            } catch(NumberFormatException e) {
                _error = _resource.getString(_section,"timeFormat");
                return false;
            }
        }
        return true;
    }

    public boolean concludePanel(WizardInfo info) {
        return true;
    }

    public void getUpdateInfo(WizardInfo info) {
        Debug.println("WAgreementSchedulePanel: getUpdateInfo()");
        Vector out = new Vector();
        out.addElement(createDateString());
        _wizardInfo.setDate(out);
/*
        if (_wizardInfo.getAgreementType().equals(_wizardInfo.LEGACYR_AGREEMENT)) {
            _wizardInfo.setSyncInterval(Integer.parseInt(
				_intervalText.getText()));
        }
*/
    }

    public String getErrorMessage() {
        return _error;
    }


    /*==========================================================
	 * private methods
	 *==========================================================*/
	 
    private void setTimeIntervalVisible(boolean x) {
        check.setVisible(x);
        _intervalText.setVisible(x);
    }
    
	private void makeDayButton(int index, String label) {
        _dayButton[index] = UIFactory.makeJCheckBox(
               this, _section, label, false, _resource );
        _dayButton[index].setVerticalTextPosition(JCheckBox.TOP);
        GridBagConstraints gbc = new GridBagConstraints();
        ReplicationTool.resetGBC(gbc);
		if( index == 0 )
			gbc.gridwidth = gbc.REMAINDER;
        dayPanel.add(_dayButton[index], gbc);
	}
	
    private void disableTimeSelection() {
        for (int i=0; i<7; i++) {
            _dayButton[i].setEnabled(false);    
        }    
        _allButton.setEnabled(false);
        _startHr.setEnabled(false);
        _startHr.setEditable(false);
        _startHr.setBackground(getBackground());
        _startMin.setEnabled(false);
        _startMin.setEditable(false);
        _startMin.setBackground(getBackground());
        _endHr.setEnabled(false);
        _endHr.setEditable(false);
        _endHr.setBackground(getBackground());
        _endMin.setEnabled(false);
        _endMin.setEditable(false);
        _endMin.setBackground(getBackground()); 
    }

    private void enableTimeSelection() {
        for (int i=0; i<7; i++) {
            _dayButton[i].setEnabled(true);    
        }
        _allButton.setEnabled(true);
        _startHr.setEnabled(true);
        _startHr.setEditable(true);
        _startHr.setBackground(_activeColor);
        _startMin.setEnabled(true);
        _startMin.setEditable(true);
        _startMin.setBackground(_activeColor);
        _endHr.setEnabled(true);
        _endHr.setEditable(true);
        _endHr.setBackground(_activeColor);
        _endMin.setEnabled(true);
        _endMin.setEditable(true);
        _endMin.setBackground(_activeColor);   
    }

    private void checkDate(boolean check) {
        for (int i=0; i<7; i++) {
            _dayButton[i].setSelected(check);    
        }
    }

	/**
	 * Populate the UI with data
	 */
	private void populateData() {
	    String time = "";
	    if ((_wizardInfo.getDate() != null) &&
			(( (Vector)_wizardInfo.getDate()).size() >0)) {
	        time = (String)((Vector)_wizardInfo.getDate()).firstElement();
	    }
	    setSchedule(time);
	}
	
	private void setSchedule(String s) {
	    Debug.println("Schedule: "+s);
		StringBuffer startHr = new StringBuffer();
		StringBuffer startMin = new StringBuffer();
		StringBuffer endHr = new StringBuffer();
		StringBuffer endMin = new StringBuffer();
		StringBuffer dow = new StringBuffer();
	    if (!ReplicationTool.parseReplicaSchedule(s, startHr, startMin,
												  endHr, endMin, dow)) {
            _inSync.setSelected(true);
            _startHr.setText("00");
            _startMin.setText("00");
            _endHr.setText("00");
            _endMin.setText("00");
            checkDate(false);
            disableTimeSelection();
        } else {
            _syncOn.setSelected(true);
            _startHr.setText(startHr.toString());
            _startMin.setText(startMin.toString());
            _endHr.setText(endHr.toString());
            _endMin.setText(endMin.toString());
			String d = dow.toString();
            for (int i = 0; i < d.length(); i++) {
                int v = Integer.parseInt(d.substring(i, i + 1));
                _dayButton[v].setSelected(true);
            }
        }
    }
    
    private String createDateString() {
        if (_inSync.isSelected()) {
            return "";    
        } else {
			return ReplicationTool.createDateString(_startHr, _startMin,
													_endHr, _endMin,
													_dayButton);
		}
    }
    
    private String pad4(String s) {
        if (s.length() == 3) {
            return "0" + s;
        } else if (s.length() == 2) {
            return "00" + s;
        } else if (s.length() == 1) {
            return "000" + s;
        } else if (s.length() == 0) {
            return "0000";
        }
        return s;
    }


	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void changedUpdate(DocumentEvent e) {		
		checkNextButton();
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void removeUpdate(DocumentEvent e) {
		changedUpdate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void insertUpdate(DocumentEvent e) {
		changedUpdate(e);
    }

	/**
	  * Method used to enable/disable properly the "Next" button of the wizard.
	  * If the user chooses to make the synchronization of replicas in a particular time,
	  * we check that at least one of the days are checked AND that the ending time is later
	  * than the beginning time */	  
	protected void checkNextButton() {
		if (_syncOn.isSelected()) {
			/* See if at least a day has been checked */
			boolean isDaySelectionValid = false;
			for (int i=0; i<_dayButton.length; i++) {				
				if (_dayButton[i].isSelected()) {
					isDaySelectionValid = true;
					break;
				}
			}

			if (!isDaySelectionValid) {
				_wizard.getbNext_Done().setEnabled(false);
				return;
			}
			
			int startHour = -1;			
			int startMin = -1;			
			int endHour = -1;			
			int endMin = -1;
			
			try {
				startHour = Integer.parseInt(_startHr.getText());		
				startMin = Integer.parseInt(_startMin.getText());			
				endHour = Integer.parseInt(_endHr.getText());			
				endMin = Integer.parseInt(_endMin.getText());
			} catch (Exception e) {
				/* Something went wrong: one of the text fields didn't contain a number...*/
				_wizard.getbNext_Done().setEnabled(false);
				return;
			}
			
			/* Check if the values are correct (minutes and hour limit values, beginning and ending
			   time of the synchronization, etc */
			if ((startMin < 0) || (startMin >= 60) ||
				(endMin < 0) || (endMin >= 60) ||
				(startHour < 0) || (startHour >= 24) ||
				(endHour < 0) || (endHour >= 24) ||
				(startHour > endHour) ||
				((startHour == endHour) && (startMin >= endMin))) {
				_wizard.getbNext_Done().setEnabled(false);
				return;
			}			
		}
		_wizard.getbNext_Done().setEnabled(true);
	}
    
    /*==========================================================
     * variables
     *==========================================================*/
    private AgreementWizardInfo _wizardInfo;
    private boolean _initialized = false;
    private String _error = "WAgreementSchedulePanel: error message";
	private Color _activeColor;

	private JRadioButton _inSync, _syncOn;
	private JButton _allButton;
	private JCheckBox _dayButton[] = new JCheckBox[7];
    private JTextField _startHr, _startMin, _endHr, _endMin;
    private JPanel dayPanel;
    private JLabel check;
    private JTextField _intervalText;

	private AgreementWizard _wizard;
}
