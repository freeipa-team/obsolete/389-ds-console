/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import netscape.ldap.util.RDN;
import com.netscape.admin.dirserv.wizard.*;

/**
 * Destination Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class WindowsAgreementDestinationPanel extends WAgreementPanel implements IWizardPanel, ActionListener, DocumentListener {
    
    /*==========================================================
     * constructors
     *==========================================================*/
    
    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    public WindowsAgreementDestinationPanel(WindowsAgreementWizard wizard) {
        super();
        _dsInstances = new Hashtable();
        _icon = ReplicationTool.getImage("DirectoryServer.gif");
        _helpToken = "sync-wizard-content-help";
        _section = "winsync-destination-dialog";
        _wizard = wizard;
    }
    
  /*==========================================================
   * public methods
   *==========================================================*/
    
    /**
     * Actual panel construction
     */
    public void init() {
        int space = UIFactory.getComponentSpace();
        int different = UIFactory.getDifferentSpace();
        int large = 40;
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        ReplicationTool.resetGBC(gbc);
        setPreferredSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        setMaximumSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        
        JLabel ask =
        new JLabel(_resource.getString("replication-destination-ask",
        "label"));
        ask.setLabelFor(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.insets = UIFactory.getBorderInsets();
        gbc.insets.left = space;
        add(ask, gbc);
        
        //======== Replicate From Panel =========================
        
        String title = _resource.getString("replication-info-summary-from","title");
        
        _fromPanel = new GroupPanel(title);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        add(_fromPanel, gbc);
        
        //from combo box
        _fromModel = new CustomComboBoxModel();
        _fromBox = new CustomComboBox(_fromModel);
        _fromBox.getAccessibleContext().setAccessibleDescription(_resource.getString("replication-info-summary-from",
        "title"));
        _fromBox.setMaximumRowCount(4);
        
        //from text box
        _fromText = new JLabel("",_icon, JLabel.LEFT);
        
        //======== Replicate To Panel ===========================
        title = _resource.getString("replication-info-summary-to","title");
        title = "Windows Domain Information";
        _toPanel = new GroupPanel(title);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        add(_toPanel, gbc);
        
        
        
        //Windows domain
        _domainLabel = new JLabel(_resource.getString(_section,"win-domain-name-label"));
        _domainLabel.setToolTipText(_resource.getString(_section,"win-domain-name-ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        
        // gbc.insets = new Insets(0,70,space,different);
        
        _toPanel.add(_domainLabel, gbc);
        _domain = UIFactory.makeJTextField(null,""); // XXX
        

        _domainLabel.setLabelFor(_domain);
        _domain.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        // gbc.fill = gbc.R
        gbc.anchor = gbc.SOUTHWEST;
        gbc.weightx=1.0;
        _toPanel.add(_domain, gbc);
        
        // Sync New Users Box
        _syncNewWinUsersLabel = new JLabel(_resource.getString(_section,"win-syncnew-label"));
        _syncNewWinUsersLabel.setToolTipText(_resource.getString(_section,"win-syncnew-ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        
        // gbc.insets = new Insets(0,70,space,different);
        
        _toPanel.add(_syncNewWinUsersLabel, gbc);
        _syncNewWinUsers = UIFactory.makeJCheckBox(null);
        

        _syncNewWinUsersLabel.setLabelFor(_syncNewWinUsers);
        _syncNewWinUsers.addActionListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = _wizard.getWidth() / 2; // Half the wizard width.
        // gbc.fill = gbc.R
        gbc.anchor = gbc.SOUTHWEST;
        gbc.weightx=1.0;
        _toPanel.add(_syncNewWinUsers, gbc);
        
        // Sync New Groups Box
        _syncNewWinGroupsLabel = new JLabel(_resource.getString(_section,"win-syncnewgroup-label"));
        _syncNewWinGroupsLabel.setToolTipText(_resource.getString(_section,"win-syncnewgroup-ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
              
        _toPanel.add(_syncNewWinGroupsLabel, gbc);
        _syncNewWinGroups = UIFactory.makeJCheckBox(null);
        

        _syncNewWinGroupsLabel.setLabelFor(_syncNewWinGroups);
        _syncNewWinGroups.addActionListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        // gbc.fill = gbc.R
        gbc.anchor = gbc.SOUTHWEST;
        gbc.weightx=1.0;
        _toPanel.add(_syncNewWinGroups, gbc);
        
        // Win Repl Area
        _windowsReplicaSubtreeLabel = new JLabel(_resource.getString(_section,"win-rep-area-label"));
        _windowsReplicaSubtreeLabel.setToolTipText(_resource.getString(_section,"win-rep-area-ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        
        // gbc.insets = new Insets(0,70,space,different);
        
        _toPanel.add(_windowsReplicaSubtreeLabel, gbc);
        _windowsReplicaSubtree = UIFactory.makeJTextField(null,""); // XXX
        

        _windowsReplicaSubtreeLabel.setLabelFor(_windowsReplicaSubtree);
        _windowsReplicaSubtree.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        // gbc.fill = gbc.R
        gbc.anchor = gbc.SOUTHWEST;
        gbc.weightx=1.0;
        _toPanel.add(_windowsReplicaSubtree, gbc);
        
        // DS Repl Area        
        _dsReplicaSubtreeLabel = new JLabel(_resource.getString(_section,"ds-rep-area-label"));
        _dsReplicaSubtreeLabel.setToolTipText(_resource.getString(_section,"ds-rep-area-ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        
        // gbc.insets = new Insets(0,70,space,different);
        
        _toPanel.add(_dsReplicaSubtreeLabel, gbc);
        _dsReplicaSubtree = UIFactory.makeJTextField(null,"ou=People, " + _wizardInfo.getSubtree()); // XXX
        

        _dsReplicaSubtreeLabel.setLabelFor(_dsReplicaSubtree);
        _dsReplicaSubtree.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        // gbc.fill = gbc.R
        gbc.anchor = gbc.SOUTHWEST;
        gbc.weightx=1.0;
        _toPanel.add(_dsReplicaSubtree, gbc);
        
        _domainControllerLabel = new JLabel(_resource.getString(_section,"win-domain-host-label"));
        _domainControllerLabel.setToolTipText(_resource.getString(_section,"win-domain-host-ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        
        // gbc.insets = new Insets(0,70,space,different);
        
        _toPanel.add(_domainControllerLabel, gbc);
        _domainController = UIFactory.makeJTextField(null,""); // XXX
        

        _domainControllerLabel.setLabelFor(_domainController);
        _domainController.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        // gbc.fill = gbc.R
        gbc.anchor = gbc.SOUTHWEST;
        gbc.weightx=1.0;
        _toPanel.add(_domainController, gbc);
        
        _domainControllerPortLabel = new JLabel(_resource.getString(_section,"win-domain-port-label"));
        _domainControllerPortLabel.setToolTipText(_resource.getString(_section,"win-domain-port-ttip"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        //gbc.fill = gbc.NONE;
        
        // gbc.insets = new Insets(0,70,space,different);
        
        _toPanel.add(_domainControllerPortLabel, gbc);
        _domainControllerPort = UIFactory.makeJTextField(null,ATTR_PORT); // XXX
        

        _domainControllerPortLabel.setLabelFor(_domainControllerPort);
        _domainControllerPort.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        // gbc.fill = gbc.R
        gbc.anchor = gbc.SOUTHWEST;
        gbc.weightx=1.0;
        _toPanel.add(_domainControllerPort, gbc);
        
        
        
        
        // "fetch via dns" button for AD ONLY
        //        _fetchDcButton = UIFactory.makeJButton(this, "fetch DCs via dns");
        //        JButtonFactory.resize( _fetchDcButton );
        //        if (_wizardInfo.getAgreementType() == _wizardInfo.AD_AGREEMENT){
        //            gbc.anchor = gbc.NORTHEAST;
        //
        //            _toPanel.add(_fetchDcButton);
        //        }
        //
        
        //to text box
        //  _toText = new JLabel("",_icon, JLabel.LEFT);
        //_toPanel.add(_toText);
        
        
        
        
        
        //        _DCmodel = new DefaultListModel();
        //        _domainControllersList = new JList(_DCmodel);
        //        _domainControllerListPane = new JScrollPane(_domainControllersList);
        //  _dcListModel = new  CustomComboBoxModel();
        
        //        _domainControllersList.setVisibleRowCount(3);
        
        
        //   _domainControllersList.validate();
        //      _domainControllersList.setModel( _dcListModel );
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.HORIZONTAL;
        
        
        //        _domainControllerLabel = new JLabel("found domain controllers");
        //        _toPanel.add(_domainControllerLabel);
        //        _domainControllerLabel.setLabelFor(_domainControllerListPane);
        //
        
        
        //     _domainControllerListPane.getViewport().setView(_domainControllersList);
        
        //        _toPanel.add(_domainControllerListPane);
        
        
        
        // gbc.gr
        
        //        _DomainControllerAddButton = UIFactory.makeJButton(this, "add DC");
        //        _DomainControllerDelButton  = UIFactory.makeJButton(this, "del DC");
        //
        //        _DomainControllerManualEntryText = UIFactory.makeJTextField(this);
        
        //        gbc.anchor = gbc.EAST;
        //        gbc.fill = gbc.NONE;
        //        _toPanel.add(_DomainControllerAddButton);
        //        _toPanel.add(_DomainControllerDelButton);
        //        gbc.anchor = gbc.WEST;
        //        gbc.fill = gbc.REMAINDER;
        //        _toPanel.add(_DomainControllerManualEntryText);
        //
        //==== Authentication =======
        title = _resource.getString("replication-destination-connection",
        "label");
        JPanel authPanel = new GroupPanel(title);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        add(authPanel, gbc);
        
        //connection radio buttons
        ButtonGroup connGroup = new ButtonGroup();
        //plain old ldap button
        _noEncrypt = new JRadioButton(_resource.getString(
			"replication-destination-noEncrypt","label"));
        _noEncrypt.setToolTipText(_resource.getString(
			"replication-destination-noEncrypt","ttip"));
        _noEncrypt.setSelected(true); // default is on
        _noEncrypt.addActionListener(this);
        connGroup.add(_noEncrypt);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0, space, 0, different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_noEncrypt, gbc);

        //ssl button
        _sslEncrypt = new JRadioButton(_resource.getString(
			"replication-destination-sslEncrypt","label"));
        _sslEncrypt.setToolTipText(_resource.getString(
    			"replication-destination-sslEncrypt","ttip"));
        _sslEncrypt.addActionListener(this);
        connGroup.add(_sslEncrypt);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0, space, 0, different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_sslEncrypt, gbc);

        //tls button
        _tlsEncrypt = new JRadioButton(_resource.getString(
			"replication-destination-startTLS","label"));
        _tlsEncrypt.setToolTipText(_resource.getString(
    			"replication-destination-startTLS","ttip"));
        _tlsEncrypt.addActionListener(this);
        connGroup.add(_tlsEncrypt);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0, space, space, different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_tlsEncrypt, gbc);
                
        //simp panel
        JPanel simpPanel = new JPanel();
        simpPanel.setLayout(new GridBagLayout());
        simpPanel.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.insets = new Insets(0,0,0,0);
        authPanel.add(simpPanel, gbc);
        
        //bind as
        _bind = new JLabel(_resource.getString(
        "replication-destination-bindAs","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.insets = new Insets(0,space+10,space,different);
        gbc.fill = gbc.NONE;
        simpPanel.add(_bind, gbc);
        
        _bindText = UIFactory.makeJTextField(null, "");
        _bind.setLabelFor(_bindText);
        _bindText.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx=1.0;
        simpPanel.add(_bindText, gbc);
        
        //password
        _pwd = new JLabel(_resource.getString(
        "replication-destination-bindPwd","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.insets = new Insets(0,space+10,space,different);
        gbc.fill = gbc.NONE;
        simpPanel.add(_pwd, gbc);
        
        _pwdText= UIFactory.makeJPasswordField(null,"");
        _pwd.setLabelFor(_pwdText);
        _pwdText.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gbc.fill = gbc.HORIZONTAL;
        simpPanel.add(_pwdText, gbc);
        
        //======== Context Panel ================================
        title = _resource.getString(
        "replication-content-replicate","label");
        JPanel contextPanel = new GroupPanel(title);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.gridheight = gbc.REMAINDER;
        gbc.weighty = 1.0;
        gbc.weightx = 1.0;
        add(contextPanel, gbc);
        
        //subtree text
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = 1;
        gbc.fill = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(0,space,different,different);
        Debug.println(8, "WindowsAgreementDestinationPanel.init: replica entry = " +
                _wizardInfo.getWindowsReplicaEntry());
        if (_wizardInfo.getWindowsReplicaEntry() == null) {
            _replicaPanel = new ReplicaPanel(_serverInfo.getLDAPConnection(),
            _wizardInfo.getWindowsReplicaEntry());
            contextPanel.add(_replicaPanel, gbc);
        } else {
            String suffix = _wizardInfo.getSubtree();
            contextPanel.add(new JLabel(suffix), gbc);
        }
        
        
        populateData();
        if(!_isCopy) {
            setSimpAuth();
        }
        checkNextButton();
        invalidate();
        validate();
        repaint(1);
        _initialized = true;
    }
    
    public String parse_domain()
    {
        String newVal = new String("");
        StringTokenizer parser = new StringTokenizer(_domain.getText(), ".");
        
        while(parser.hasMoreTokens()){
            newVal = newVal + ",dc=" + parser.nextToken();
        }
        
        return newVal;
    }
    
    public void update_win_rep_area()
    {
        _windowsReplicaSubtree.setText("cn=Users" + parse_domain());
    }
    
    
    //========= ACTIONLISTENER =================
    public void actionPerformed(ActionEvent e) {
        Debug.println("WindowsAgreementDestinationPanel: actionPerformed()"+
        e.toString());
        
        if (e.getSource().equals(_DomainControllerAddButton)) {
            String title = _resource.getString(_section, "hostInfo-consumer-title");
            
            JFrame frame = null;
            for (Container p = getParent(); p != null; p = p.getParent()) {
                if (p instanceof JFrame) {
                    frame = (JFrame)p;
                    break;
                }
            }
            HostInfoDialog dialog = new HostInfoDialog(frame, title);
            dialog.pack();
            dialog.show();
            
            if (! dialog.isOk()) {
                dialog = null;
                return;
            }
            
            //            _DCmodel.add(_DCmodel.size(),dialog.getHost() );
            //            _DomainControllerManualEntryText.setText("");
            
        }
        
        if (e.getSource().equals(_tlsEncrypt) && _tlsEncrypt.isSelected()) {
        	/* use regular LDAP port for startTLS */
            _domainControllerPort.setText(ATTR_PORT);
        } else if (e.getSource().equals(_sslEncrypt) && _sslEncrypt.isSelected()) {
        	/* set to use SSL port */
        	_domainControllerPort.setText(ATTR_SECURE_PORT);
        } else if (e.getSource().equals(_noEncrypt) && _noEncrypt.isSelected()) {
        	/* use regular LDAP port */
            _domainControllerPort.setText(ATTR_PORT);
        }
        
        if (e.getSource()==_syncNewWinUsers)
        {
            if (_syncNewWinUsers.isSelected())
            {
                _syncNewUsers = "on";
            }else{
                _syncNewUsers = "off";
            }
            
        }
        
        if (e.getSource()==_syncNewWinGroups)
        {
            if (_syncNewWinGroups.isSelected())
            {
                _syncNewGroups = "on";
            }else{
                _syncNewGroups = "off";
            }
            
        }
        checkNextButton();
    }
    
    //====== IWizardPanel =============================
    public boolean initializePanel(WizardInfo info) {
        boolean prevIsLEGACYR = _isLEGACYR;
        Debug.println("WindowsAgreementDestinationPanel: Initialize() info = " + info);
        
        _wizardInfo = (AgreementWizardInfo)info;
        _serverInfo = _wizardInfo.getServerInfo();
        _consoleInfo = _wizardInfo.getConsoleInfo();
        //
        //        if (_wizardInfo.getAgreementType().equals(_wizardInfo.AD_AGREEMENT))
        //            _isLEGACYR = true;
        //        else
        //            _isLEGACYR = false;
        //        Debug.println(8, "WindowsAgreementDestinationPanel: is legacy = " + _isLEGACYR);
        if (_wizardInfo.getWizardType().equals(_wizardInfo.COPY_WIZARD)) {
            _isCopy = true;
            _copy = _wizardInfo.getCopyAgreement();
        } else {
            _isCopy = false;
        }
        
        if (!_initialized) {
            init();
            return true;
        }
        
        /* agreement type changed - need to repopulate data */
        if (!_isCopy && prevIsLEGACYR != _isLEGACYR) {
            populateData();
        }
        checkNextButton();
        invalidate();
        validate();
        repaint(1);
        return true;
    }
    
    public boolean validatePanel() {
        Debug.println("WindowsAgreementDestinationPanel: validatePanel()");
        
        
        if (_domain != null)
            if (_domain.getText().equals("")) {
                _error = "must have domain  if doing windows replication";
                return false;
            }
        
        if (_domainController != null)
            if (_domainController.getText().equals("")) {
                _error = "must have domain controller if doing windows replication";
                return false;
            }
        
        //binddn and pwd field
        
        String dn = _bindText.getText();
        if ( dn.equals("") || !DSUtil.isValidDN(dn) ) {
            _error = _resource.getString(_section,
            "bindDNEmpty-msg");
            return false;
        } else if (_pwdText.getText().equals("")) {
            _error = _resource.getString(_section,
            "bindPasswordEmpty-msg");
            return false;
        }
        
        return true;
    }
    
    public boolean concludePanel(WizardInfo info) {
        Debug.println("WindowsAgreementDestinationPanel: conclude()");
        
        //check copied from
        int result = checkCopiedFrom();
        
        if (result == SKIP)
            return false;
        //
        if (result == PROMPT) {
            String msg = _resource.getString(_section,"mismatchCopiedFrom");
            int option = DSUtil.showConfirmationDialog(this, "warning",
            msg,
            "replication-dialog");
            if (option == JOptionPane.NO_OPTION)
                return false;
        }
        
        
        LDAPConnection ld = LDAPUtil.connectToServer(
        _domainController.getText(), Integer.parseInt(_domainControllerPort.getText()),
        _bindText.getText(), _pwdText.getText(),
        _sslEncrypt.isSelected());
        
        if (ld == null) {
            String msg = _resource.getString(_section, "clientUnavail");
            int option = DSUtil.showConfirmationDialog(this,
            "warning",
            msg,
            "replication-dialog");
            
            if (option != JOptionPane.YES_OPTION)
                return false;
        } else
            try { ld.disconnect(); }
            catch (LDAPException le) { }
        
        
        
        return true;
    }
    
    public void getUpdateInfo(WizardInfo info) {
        Debug.println("WindowsAgreementDestinationPanel: getUpdateInfo()");
        
        
        ServerInstance toServer = new ServerInstance();
        toServer.setHost(_domainController.getText());
        toServer.setPort(Integer.parseInt(_domainControllerPort.getText())); //xxx
        
        _wizardInfo.setFromServer(_server);
        _wizardInfo.setToServer(toServer);
        _wizardInfo.setStartTLS(_tlsEncrypt.isSelected());
        _wizardInfo.setSSL(_sslEncrypt.isSelected());
        
        /* none of these other auth methods are currently supported */
        _wizardInfo.setSSLAuth(false);
        _wizardInfo.setGSSAPIAuth(false);
        _wizardInfo.setDigestAuth(false);
        _wizardInfo.setBindDN(_bindText.getText());
        _wizardInfo.setBindPWD(_pwdText.getText());

        if (_replicaPanel != null) {
            _wizardInfo.setReplicaEntry(_replicaPanel.getReplicaEntry());
            _wizardInfo.setSubtree(_replicaPanel.getSuffix());
            _wizardInfo.setWindowsReplicaEntry(_replicaPanel.getReplicaEntry());
        }
        _wizardInfo.setWindowsRepArea(_windowsReplicaSubtree.getText());
        _wizardInfo.setDSRepArea(_dsReplicaSubtree.getText());
        _wizardInfo.setNewWinUserSync(_syncNewUsers);
        _wizardInfo.setNewWinGroupSync(_syncNewGroups);
        _wizardInfo.setWindowsDomain(_domain.getText());
        
        
    }
    
    public String getErrorMessage() {
        return _error;
    }
    
    /*==========================================================
     * private methods
     *==========================================================*/
    
    /**
     * Populate the UI with data
     */
    private void populateData() {
        GridBagConstraints gbc = new GridBagConstraints();
        int space = UIFactory.getComponentSpace();
        int different = UIFactory.getDifferentSpace();
        int large = 40;
        
        _fromPanel.removeAll();
        ReplicationTool.resetGBC(gbc);
        gbc.weightx=1.0;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(0,space,space,different);
        _fromPanel.add(_fromText, gbc);
        _fromText.setLabelFor(_fromPanel);
        // _toPanel.removeAll();
        ReplicationTool.resetGBC(gbc);
        gbc.insets = new Insets(0,space,space,different);
        gbc.weightx = 1.0;
        
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(0,0,space,different);
        //_toPanel.add(_toPanel, gbc);
        //  _toModel.removeAll();
        //to panel
        if (_isCopy) {
            copyData();
        }
        
        _server = new ServerInstance( _serverInfo.getHost(), _serverInfo.getPort());
        
        //form panel
        _fromText.setText(_server.getKey());
        
        
        loadDSInstances(_consoleInfo);
        
        //add remaining availabe server
        
        
        if (_isCopy) {
            ServerInstance toServer = _wizardInfo.getToServer();
            /* To avoid having duplicated hosts in the combo box */
            
        }
    }
    
    /**
     * Copy Rest of data from agreement
     */
    private void copyData() {
        _pwdText.setText(_wizardInfo.getBindPWD());
        _bindText.setText(_wizardInfo.getBindDN());
        _tlsEncrypt.setSelected(_wizardInfo.getStartTLS());
        _sslEncrypt.setSelected(_wizardInfo.getSSL());
        // cannot change to/from LDAPS in this panel because there
        // is no way to change the port number
        if (_sslEncrypt.isSelected()) {
            _noEncrypt.setEnabled(false);
            _tlsEncrypt.setEnabled(false);
        } else {
            _sslEncrypt.setEnabled(false);
        }
        enableSimpleAuth(true);
        /* none of the other auth methods are currently supported */
    }
    
    /**
     * Set simple auth selection
     */
    private void setSimpAuth() {
        //Simple Auth no SSL
        _sslEncrypt.setSelected(false);
        _tlsEncrypt.setSelected(false);
        
        enableSimpleAuth(true);
    }
    
    private void enableSimpleAuth(boolean enable){
        //_bind.setEnabled(enable);
        //_bindText.setEnabled(enable);
        //_pwd.setEnabled(enable);
        //_pwdText.setEnabled(enable);
        _pwdText.setBackground(_bindText.getBackground());
    }
    
    /**
     * populates instance combo box.
     */
    
    private void populateInstanceModel(ConsoleInfo ci){
        CustomComboBoxModel activeModel;
        JComboBox activeBox;
        String  key = null;
        boolean reselect;
        int     i, index, count;
        
        /* populate model */
        //  activeModel = _toModel;
        activeBox   = null; //_toBox;
        
        /* this function is called when connection type has
           changed from SSL to non-SSL or the other way around.
           In that, case we should remove all entries currently
           in _dsInstance because the port number has changed.   */
        index = activeBox.getSelectedIndex();
        if (index >= 0){
            Hashtable serverItem = (Hashtable) activeBox.getSelectedItem();
            ServerInstance activeServer = (ServerInstance) serverItem.get(
            CustomComboBoxModel.SELECTION_DATA);
            if (activeServer != null) {
                key = (String)activeServer.getKey();
            }
        }
        
        if (key != null && _dsInstances.containsKey(key))
            reselect = true;
        else
            reselect = false;
        
        Enumeration keys = _dsInstances.keys();
        //        while (keys.hasMoreElements()){
        //            key = (String)keys.nextElement();
        //            activeModel.removeEntry(key);
        //        }
        
        /* get the list of available server insatnces */
        loadDSInstances(ci);
        
        //add remaining availabe server
        for (Enumeration e = _dsInstances.keys() ; e.hasMoreElements() ;) {
            key = (String)e.nextElement();
            ServerInstance otherServer =
            (ServerInstance)_dsInstances.get(key);
            //    activeModel.addItem(otherServer);
        }
        //         if (reselect && activeModel.getSize() > 0)
        //             activeBox.setSelectedIndex(0);
    }
    
    /**
     * Retrieve all instances of directory server configuration managed by the
     * current KinPin admin console
     */
    private void loadDSInstances(ConsoleInfo ci) {
        
        //get connection from console
        //if null, problem with server forget it...
        LDAPConnection ld = ci.getLDAPConnection();
        if (ld == null) {
            Debug.println("ERROR: AgreementDestinationPanel: " +
            "loadDSInstances()- Connection Failed");
            return;
        }
        
        // clear the list
        _dsInstances.clear();
        
        //search
        try {
            String filter = "objectclass=" + DSClass;
            String root =
            com.netscape.management.client.util.LDAPUtil.getConfigurationRoot();
            String[] attrs = { "serverHostName", "" };
            String serverKey = ServerInstance.getKey(_server.getHost(),
            getPort());
            
            attrs [1] = _portAttr;
            
            LDAPSearchResults reslist =
            ld.search(root, ld.SCOPE_SUB, filter,
            attrs, false);
            
            if (!reslist.hasMoreElements()) {
                Debug.println("ERROR: WindowsAgreementDestinationPanel." +
                "loadDSInstances: could not get list of " +
                "servers");
            }
            
            while (reslist.hasMoreElements()) {
                LDAPEntry entry = reslist.next();
                
        /* discard entries match this server or already in
           the table. This will only work if the host name
           received is fully qualified name. Otherwise,
           this optimization will not work and we will
           have to go through full resolution */
                
                String thisKey = getKey(entry);
                if (thisKey.equals(serverKey)) {
                    Debug.println("ERROR: WindowsAgreementDestinationPanel." +
                    "loadDSInstances: server " + thisKey +
                    " equals server " + serverKey);
                    continue;
                }
                
                if (_dsInstances.containsKey(thisKey)) {
                    Debug.println("ERROR: WindowsAgreementDestinationPanel." +
                    "loadDSInstances: server " + thisKey +
                    " is already in the list");
                    continue;
                }
                
                ServerInstance s = entry2DSInstance( entry );
                if (s != null) {
                    _dsInstances.put(s.getKey(),s);
                    Debug.println(9, "WindowsAgreementDestinationPanel." +
                    "loadDSInstances: put server " +
                    s.getKey() + " in the list");
                } else {
                    Debug.println("ERROR: WindowsAgreementDestinationPanel." +
                    "loadDSInstances: could not convert " +
                    "entry " + entry + " to a server");
                }
            }
            
            if (_dsInstances.containsKey(serverKey)) {
                Debug.println(9, "WindowsAgreementDestinationPanel." +
                "loadDSInstances: removing " + serverKey +
                " from the list");
                _dsInstances.remove(serverKey);
            }
            
        } catch (Exception e) {
            Debug.println("ERROR: AgreementDestinationPanel: loadDSSIE()"+
            e.toString());
        }
        
        Debug.println(9, "WindowsAgreementDestinationPanel." +
        "loadDSInstances: there are " +
        ((_dsInstances == null) ? 0 : _dsInstances.size()) +
        " entries in the list");
        
    }
    
    /**
     * Checks if the entry corresponds to this server
     * @param entry LDAP Entry
     * @return whether it corresponds to this server or not
     */
    private String getKey(LDAPEntry entry) {
        LDAPAttribute attrHost = entry.getAttribute( "serverHostName" );
        LDAPAttribute attrPort = entry.getAttribute(_portAttr);
        String        host;
        int           port;
        
        if (attrHost != null)
            host = (String)attrHost.getStringValues().nextElement();
        else
            host="";
        
        if (attrPort != null)
            port = Integer.parseInt(
            (String)attrPort.getStringValues().nextElement());
        else
            port = 0;
        
        return ServerInstance.getKey(host, port);
    }
    
    /**
     * Convert LDAP entry to ServerInstance object
     * @param entry LDAP Entry
     * @return ServerInstance
     */
    private ServerInstance entry2DSInstance(LDAPEntry entry) {
        ServerInstance s = new ServerInstance();
        LDAPAttributeSet attrs = entry.getAttributeSet();
        LDAPAttribute attr = entry.getAttribute( "serverHostName" );
        if ( attr != null )
            s.setHost((String)attr.getStringValues().nextElement());
        attr = entry.getAttribute(_portAttr);
        if ( attr != null )
            s.setPort(Integer.parseInt(
            (String)attr.getStringValues().nextElement()));
        return s;
    }
    
    /**
     * this function returns normal or SSL port for this server
     * depending on whether SSL was requested for this
     * agreement.
     */
    
    private int getPort(){
        if (_portAttr.equals(ATTR_PORT))
            return _serverInfo.getPort();
        else /* SSL port */
            return DSUtil.getSSLPort(_serverInfo);
    }
    
    /**
     * check copiedFrom attribute from the subtree to see
     * if there are other replciation agreeement already
     * exist. If so, warm the user.
     * PRE_CONDITION: FROM/TO SELECTED
     */
    private int checkCopiedFrom() {
        String subtree = null;
        if (_replicaPanel != null) {
            subtree = _replicaPanel.getSuffix();
        } else {
            subtree = _wizardInfo.getSubtree();
        }
        String supplierName;
        int supplierPort;
        //get supplier
        
        supplierName = _serverInfo.getHost();
        supplierPort = _serverInfo.getPort();
        
        
        ////get consumer LDAPConnection
        //Hashtable serverItem = null; //(Hashtable) _toBox.getSelectedItem();
        //ServerInstance consumer =
        //(ServerInstance) serverItem.get(
        //CustomComboBoxModel.SELECTION_DATA);
        //LDAPConnection ld = LDAPUtil.connectToServer(
        //consumer.getHost(), consumer.getPort(),
        //_bindText.getText(), _pwdText.getText(),
        //_sslEncrypt.isSelected());
        //if (ld == null) {
        //String msg = _resource.getString(_section,
        //"clientUnavail");
        //int option = DSUtil.showConfirmationDialog(this,
        //"warning",
        //msg,
        //"replication-dialog");
        //if (option == JOptionPane.YES_OPTION)
        //return CONTINUE;
        //else
        //return SKIP;
        //}
        
        //get copied from string
        //        String x = ReplicationAgreement.getCopiedFrom(ld, subtree);
        //        if (x != null) {
        //            try {
        //                int cloc = x.indexOf(':');
        //                if (cloc != -1) {
        //                    String cfHost = x.substring(0, cloc);
        //                    int sploc = x.indexOf(' ');
        //                    if (sploc != -1) {
        //                        int cfPort =
        //                        Integer.parseInt(x.substring(cloc + 1, sploc));
        //                        if (!cfHost.equals(supplierName) ||
        //                        cfPort != supplierPort) {
        //                            Debug.println("copiedFrom attribute mismatch: " +
        //                            "copiedFrom " + cfHost + ":" +
        //                            cfPort + " != supplier " +
        //                            supplierName + ":" + supplierPort);
        //                            return PROMPT;
        //                        }
        //                    }
        //                }
        //            } catch (Exception e) {
        //                // ignore all exceptions, and assume we couldn't read
        //                // the copiedFrom
        //                Debug.println(e.toString());
        //            }
        //
        //        } else {
        //            Debug.println("WADP: getCopiedFrom() - return null");
        //        }
        return CONTINUE;
    }
    
    
    /**
     * Some text component changed
     *
     * @param e Event indicating what changed
     */
    public void changedUpdate(DocumentEvent e) {
        checkNextButton();
        if(e.getDocument().equals(_domain.getDocument()))
        {
            update_win_rep_area();
        }
    }
    
    /**
     * Some text component changed
     *
     * @param e Event indicating what changed
     */
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }
    
    /**
     * Some text component changed
     *
     * @param e Event indicating what changed
     */
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }
    
    /**
     * Method used to ensure domain controller field has proper form
     */
    
    private boolean domainControllerPortIsValid()
    {
        if(_domainControllerPort.getText().length() > 0) {
            try {
                if (Integer.parseInt(_domainControllerPort.getText()) > 0) {
                    return true;
                }
            } catch (NumberFormatException e) {
                /* Someone likely entered some non-numeric text.  Just
                 * do nothing here so we fall through and return false. */
            }
        }
        return false;
    }
    
    /**
     * Method used to update the next button of the wizard.
     * We check that (if necessary) the user has provided a correct dn to bind and
     * a password */
    protected void checkNextButton() {
        boolean state = true;
        if(_domain.getText().compareTo("")!= 0
        && _windowsReplicaSubtree.getText().compareTo("")!= 0
        && _dsReplicaSubtree.getText().compareTo("")!= 0
        && _domainController.getText().compareTo("")!= 0
        && domainControllerPortIsValid()
        && _pwdText.getText().compareTo("")!= 0
        && _bindText.getText().compareTo("")!= 0) 
        {
            _wizard.getbNext_Done().setEnabled(true);
        }else {
            _wizard.getbNext_Done().setEnabled(false);
        }
        //        Hashtable serverItem = (Hashtable)_toBox.getSelectedItem();
    /* IF nothing selected or if the message "No consumer list available" selected
       disable 'Next' button */
        //        if ((serverItem == null) ||
        //        serverItem.get(CustomComboBoxModel.SELECTION_TITLE).equals(CONSUMER_LIST_NOT_AVAILABLE)) {
        //            state = false;
        //        } else if (_simpAuth.isSelected()) {
        //            if (_bindText.getText().trim().equals("") ||
        //            !DSUtil.isValidDN(_bindText.getText()) ||
        //            _pwdText.getText().trim().equals("")) {
        //                state = false;
        //            }
        //        }
        
        
    }
    
    
    /*==========================================================
     * variables
     *==========================================================*/
    private Hashtable _dsInstances;
    private ConsoleInfo _serverInfo;
    private ConsoleInfo _consoleInfo;
    private boolean _isLEGACYR = false;
    private boolean _isCopy = false;
    private boolean _initialized = false;
    private ServerInstance _server;
    private ImageIcon _icon;
    
    private JLabel _fromText, _toText;
    private JTextField _bindText;
    
    private JButton _otherButton;
    
    //private JButton _fetchDcButton;
    private JComboBox _fromBox, _toBox;
    private JRadioButton _noEncrypt, _sslEncrypt, _tlsEncrypt;
    private JRadioButton _simpAuth = null, _sslAuth = null, _gssapiAuth = null, _digestAuth = null;
    private JPasswordField _pwdText;
    private JLabel _bind, _pwd;
    private CustomComboBoxModel _fromModel;
    //private DefaultListModel _DCmodel;
    private JPanel _toPanel, _fromPanel;
    private ReplicaPanel _replicaPanel;
    private JButton _browseButton;
    //private ListModel _dcListModel;
    private JTextField _domainController;
    private JTextField _domain;
    private JLabel _domainLabel;
    private JLabel _syncNewWinUsersLabel;
    private JCheckBox _syncNewWinUsers;
    private JLabel _syncNewWinGroupsLabel;
    private JCheckBox _syncNewWinGroups;
    private JTextField _windowsDomain;
    private JTextField _windowsReplicaSubtree;
    private JLabel _windowsReplicaSubtreeLabel;
    private JTextField _dsReplicaSubtree;
    private JLabel _dsReplicaSubtreeLabel;
    private JList _domainControllersList;
    private JScrollPane _domainControllerListPane;
    protected WindowsAgreementWizard _wizard;
    private JLabel _domainControllerLabel;
    private JLabel _domainControllerPortLabel;
    private JTextField _domainControllerPort;
    private JButton _DomainControllerAddButton ,_DomainControllerDelButton ;
    private JTextField _DomainControllerManualEntryText;
    
    private AgreementWizardInfo _wizardInfo = null;
    private ReplicationAgreement _copy = null;
    private String _error ="WindowsAgreementDestinationPanel";
    private String _portAttr = ATTR_PORT;
    private String _syncNewUsers = "off";
    private String _syncNewGroups = "off";
    
    private static final int CONTINUE = 0;
    private static final int PROMPT = 1;
    private static final int SKIP =3;
    private static final String DSClass = "nsDirectoryServer";
    private static final String ATTR_PORT = "389";
    private static final String ATTR_SECURE_PORT = "636";
    private static final String BACKEND_ATTR = "cn";
    private final String CONSUMER_LIST_NOT_AVAILABLE = _resource.getString("replication-destination-consumer-list-non-available",
    "label");
}

