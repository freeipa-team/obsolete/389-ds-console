/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.util.*;
import com.netscape.admin.dirserv.wizard.WizardInfo;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.ResourceSet;
import netscape.ldap.*;

/**
 * Data Container for Replication Agreement Wizard
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class WindowsAgreementWizardInfo extends WizardInfo {

    /*==========================================================
     * constructors
     *==========================================================*/

    public WindowsAgreementWizardInfo() {
        super();
    }

    /*==========================================================
     * public methods
     *==========================================================*/
     
    /**
     * Specify the ServerInfo Obejct
     * @param server info object
     */
    public void setServerInfo(ConsoleInfo info) {
        if (info != null)
            _content.put(AGREEMENT_EXTENSION+"serverinfo", info);
    }
     
    public ConsoleInfo getServerInfo() {
        return (ConsoleInfo) _content.get(AGREEMENT_EXTENSION+"serverinfo");
    }
    
    /**
     * Specify the ConsoleInfo Obejct
     * @param console info object
     */
    public void setConsoleInfo(ConsoleInfo info) {
        if (info != null)
            _content.put(AGREEMENT_EXTENSION+"consoleinfo", info);
    }
     
    public ConsoleInfo getConsoleInfo() {
        return (ConsoleInfo) _content.get(AGREEMENT_EXTENSION+"consoleinfo");
    }    

    /**
     * Specify the subtree if it is for SUPPLIER INITIATED REPLICATION
     * User can still change this information later
     * @param subtree DN of subtree to be replicated
     */
    public void setSubtree(String subtree) {
        if (subtree != null)
            _content.put(AGREEMENT_EXTENSION+"subtree", subtree);
    }
     
    public String getSubtree() {
        return (String)_content.get(AGREEMENT_EXTENSION+"subtree");
    }
    
     
    /**
     * Specify the Replica entry to be the parent of this agreement
     * @param entry the entry of the replica in the DIT
     */
    public void setReplicaEntry(LDAPEntry entry) {
        if (entry != null)
            _content.put(AGREEMENT_EXTENSION+"replicaentry", entry);
    }
     
    public LDAPEntry getReplicaEntry() {
        return (LDAPEntry)_content.get(AGREEMENT_EXTENSION+"replicaentry");
    }
    
    /**
     * Specify the agreement type.
     * User can still change this information later
     * @param type LEGACYR_AGREEMENT or MMR_AGREEMENT
     */
    public void setAgreementType(String type) {
        if (type != null)
            _content.put(AGREEMENT_EXTENSION+"type", type);
    }
     
    public String getAgreementType() {
        return (String)_content.get(AGREEMENT_EXTENSION+"type");
    }
    
    /**
     * specify wizard as NEW or COPY
     * @param type NEW_WIZARD or COPY_WIZARD
     */
    public void setWizardType(String type) {
        if (type != null)
            _content.put(AGREEMENT_EXTENSION+"wizard",type);
    }
    
    public String getWizardType() {
        return (String)_content.get(AGREEMENT_EXTENSION+"wizard");
    }
    
    /**
     * specify obejct schema to be used
     * @param obejct schema to be used
     */
    public void setLDAPSchema(LDAPSchema schema) {
        if (schema != null)
            _content.put(AGREEMENT_EXTENSION+"schema",schema);
    }
    
    public LDAPSchema getLDAPSchema() {
        return (LDAPSchema) _content.get(AGREEMENT_EXTENSION+"schema");
    }    
    
    /*==========================================================
     * package methods
     *==========================================================*/
     
    //agreement name
    void setName(String name) {
        if (name != null)
            _content.put(AGREEMENT_EXTENSION+"name", name);
    }
    
    String getName() {
        return (String) _content.get(AGREEMENT_EXTENSION+"name");
    }
    
    
    //agreement nick name
    String getNickName() {
        return getName();
    }

    //agreement description
    void setDescription(String descr) {
        if (descr != null)
            _content.put(AGREEMENT_EXTENSION+"descr", descr);
    }
    
    String getDescription() {
        return (String) _content.get(AGREEMENT_EXTENSION+"descr");
    }

     
    //from server
    void setFromServer(ServerInstance server) {
        if (server != null)
            _content.put(AGREEMENT_EXTENSION+"from", server);
    }
     
    ServerInstance getFromServer() {
        return (ServerInstance)_content.get(AGREEMENT_EXTENSION+"from");
    }
  
    //to server
    void setToServer(ServerInstance server) {
        if (server != null)
            _content.put(AGREEMENT_EXTENSION+"to", server);
    }
     
    ServerInstance getToServer() {
        return (ServerInstance)_content.get(AGREEMENT_EXTENSION+"to");
    }
     
    //tls
    void setStartTLS(boolean type) {
        _content.put(AGREEMENT_EXTENSION+"startTLS", new Boolean(type));
    }
    
    boolean getStartTLS() {
        if (_content.get(AGREEMENT_EXTENSION+"startTLS")!= null)
            return ((Boolean)_content.get(AGREEMENT_EXTENSION+"startTLS")).booleanValue();
        else
            return false;
    }

    //ssl
    void setSSL(boolean type) {
        _content.put(AGREEMENT_EXTENSION+"ssl", new Boolean(type));
    }
    
    boolean getSSL() {
        if (_content.get(AGREEMENT_EXTENSION+"ssl")!= null)
            return ((Boolean)_content.get(AGREEMENT_EXTENSION+"ssl")).booleanValue();
        else
            return false;
    }

    //Auth type
    void setSSLAuth(boolean type) {
	_content.put(AGREEMENT_EXTENSION+"sslauth", new Boolean(type));
    }

    boolean getSSLAuth() {
	if (_content.get(AGREEMENT_EXTENSION+"sslauth")!= null)
            return ((Boolean)_content.get(AGREEMENT_EXTENSION+"sslauth")).booleanValue();
        else
            return false;
    }
     
    //bindDN
    void setBindDN(String dn) {
        if (dn != null) {
            _content.put(AGREEMENT_EXTENSION+"binddn", dn);
	} else {
            _content.remove(AGREEMENT_EXTENSION+"binddn");
	}
    }
    
    String getBindDN() {
        return (String)_content.get(AGREEMENT_EXTENSION+"binddn");
    }
    
    //bindPWD
    void setBindPWD(String pwd) {
        if (pwd != null) {
            _content.put(AGREEMENT_EXTENSION+"bindpwd", pwd);
	} else {
            _content.remove(AGREEMENT_EXTENSION+"bindpwd");
	}
    }
    
    String getBindPWD() {
        return (String)_content.get(AGREEMENT_EXTENSION+"bindpwd");
    }
    
    //filter
    void setFilter(String filter) {
        if (filter != null)
            _content.put(AGREEMENT_EXTENSION+"filter", filter);
    }
    
    String getFilter() {
        return (String)_content.get(AGREEMENT_EXTENSION+"filter");
    }
    
    //AttrSelected
    void setSelectedAttr(Vector attrs) {
        if (attrs != null)
            _content.put(AGREEMENT_EXTENSION+"attrs", attrs);
    }
    
    Vector getSelectedAttr() {
        return (Vector)_content.get(AGREEMENT_EXTENSION+"attrs");
    }
    
    //attr type
    void setAttrType(int type) {
        _content.put(AGREEMENT_EXTENSION+"attrtype", Integer.toString(type));
    }
    
    int getAttrType() {
        return Integer.parseInt((String)_content.get(AGREEMENT_EXTENSION+"attrtype"));
    }
    
    //dateString
    void setDate(Vector date) {
        if (date != null)
            _content.put(AGREEMENT_EXTENSION+"date", date);
    }
    
    Vector getDate() {
        return (Vector)_content.get(AGREEMENT_EXTENSION+"date");
    }
    
    void setSyncInterval(int val) {
        _content.put(AGREEMENT_EXTENSION+"sync", Integer.toString(val));
    }
    
    int getSyncInterval() {
        try {
            return Integer.parseInt((String)_content.get(AGREEMENT_EXTENSION+"sync"));
        } catch (NumberFormatException e) {
            return 5;
        }
    }
    
    //initialize consumer
    void setInitialize(boolean init) {
        _content.put(AGREEMENT_EXTENSION+"init", new Boolean(init));
    }
    
    boolean getInitialize() {
        return ((Boolean)_content.get(AGREEMENT_EXTENSION+"init")).booleanValue();
    }    
    
    //Ldif file
    void setLDIFFilename(String filename) {
        _content.put(AGREEMENT_EXTENSION+"ldif", filename);
    }

    String getLDIFFilename() {
        return (String)_content.get(AGREEMENT_EXTENSION+"ldif");
    }

    void removeLDIFFilename ()
    {
	_content.remove (AGREEMENT_EXTENSION+"ldif");	
    }

    //clone agreement
    void setCopyAgreement(ReplicationAgreement agreement) {
        if (agreement != null)
            _content.put(AGREEMENT_EXTENSION+"copy", agreement);
    }
    
    ReplicationAgreement getCopyAgreement() {
        return (ReplicationAgreement) _content.get(AGREEMENT_EXTENSION+"copy");     
    } 
    
    //replication parent node
    //allows the reload of agreements
    void setParentNode(Object owner) {
        if (owner != null)
            _content.put(AGREEMENT_EXTENSION+"owner", owner);
    }
    
    Object getParentNode() {
        return _content.get(AGREEMENT_EXTENSION+"owner");
    }
    
    String prettyPrint() {
        StringBuffer buf = new StringBuffer();
        
        //agreement type
        if (getAgreementType() != null) {
            if (getAgreementType().equals(LEGACYR_AGREEMENT))
                buf.append(_resource.getString("replication-info","legacyrDesc")+"\n");
            else
                buf.append(_resource.getString("replication-info","mmrDesc")+"\n");
        }
        
        //name
        if ((getName() != null) && (!getName().equals(""))) {
            buf.append("   ");
            buf.append(_resource.getString("replication-destination-name","label")+": ");
            buf.append(getName()+"\n");
        }
         
        //replica entry
        if ((getReplicaEntry() != null)) {
            buf.append("   ");
            buf.append(_resource.getString("replication-destination-entry","label")+": ");
            buf.append(getReplicaEntry().getDN()+"\n");
        }
         
	//from
	if (getFromServer()!=null) {
            buf.append("   ");
            buf.append(_resource.getString("replication-info-summary-from","title")+": ");
            buf.append(getFromServer().getKey()+"\n");
	}
         
	//to
	if (getToServer()!=null) {
            buf.append("   ");
            buf.append(_resource.getString("replication-info-summary-to","title")+": ");
            buf.append(getToServer().getKey()+"\n");
	}
         
	//connection
	if (getStartTLS()) {
        buf.append("   ");
        buf.append(_resource.getString("replication-destination-startTLS","label")+"\n");
	}
	if (getSSL()) {
            buf.append("   ");
            buf.append(_resource.getString("replication-destination-sslEncrypt","label")+"\n");
	}
         
	//bind info
	buf.append("   ");
	buf.append(_resource.getString("replication-destination-authUsing","label")+" ");         
	if ((getStartTLS() || getSSL()) && getSSLAuth()) {
            buf.append(_resource.getString("replication-destination-sslClientAuth","label")+"\n");
	} else {
            buf.append(_resource.getString("replication-destination-simpleAuth","label")+"\n");
	}
         
	//subtree
	if ((getSubtree()!= null) && (!getSubtree().equals(""))) {
            buf.append("   ");
            buf.append(_resource.getString("replication-info-summary-subtree","label")+": ");
            buf.append(getSubtree()+"\n");            
	}
         
	//if MMR
	if (getAgreementType().equals(MMR_AGREEMENT)) {
            //filter
	    if ( doFilteredReplication ) {
		if ((getFilter()!= null)&& (!getFilter().equals(""))) {
		    buf.append("   ");
		    buf.append(_resource.getString("replication-attribute-matchFilter","label")+": ");
		    buf.append(getFilter()+"\n");
		} 
	    }
            //attr
	    if ( doSelectiveReplication ) {
		buf.append("   ");
		buf.append(_resource.getString("replication-attribute-attribute","label")+": ");
		if (getAttrType() == SELATTR_ALL)
		    buf.append(_resource.getString("replication-attribute-attributeAll","label")+"\n");
		if (getAttrType() == SELATTR_EXCLUDE)
		    buf.append(_resource.getString("replication-attribute-attributeExcept","label")+"\n");
		if (getAttrType() == SELATTR_INCLUDE)
		    buf.append(_resource.getString("replication-attribute-attributeInclude","label")+"\n");
		if (getSelectedAttr()!= null) {
		    for (Enumeration e = getSelectedAttr().elements(); e.hasMoreElements();) {
			buf.append("        "); 
			buf.append((String)e.nextElement()+"\n");
		    }
		}
	    }
	} else {
            //interval   
            buf.append("   ");
            buf.append(_resource.getString("replication-schedule-checkSync","label")+": ");
            buf.append(getSyncInterval()+"\n");
	}
         
	//schedule
	if (getDate()!= null) {
            String date = (String) getDate().firstElement();
            String printDate;
            buf.append("   ");
            buf.append(_resource.getString("replication-schedule-tab","label")+": ");
            if (!date.equals("")){
                printDate = date2Printable (date);
                buf.append(printDate+"\n");                        
            }
            else 
                buf.append(_resource.getString("replication-schedule-inSyncButton","label")+"\n");                                    
	}
         
        //init
        if (getLDIFFilename() != null) {
            String filename = (String)getLDIFFilename();
            buf.append("   ");
            buf.append(_resource.getString("replication-agreementWizard-initFile","title")+": ");
            buf.append(filename+"\n");
        }

        return buf.toString();
    }

    /* Original string contains digits for days of the week.
       New string contains Mon Tue, etc 
    */ 
    private String date2Printable (String date){
        int iDigit = 10; /* format: xxxx-xxxx days */
        int iDay;
        int len = date.length ();
        String day;
        String days [] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        StringBuffer out = new StringBuffer (date.substring (0, 10));

        while (iDigit < len && Character.isDigit (date.charAt(iDigit))){
            iDay = Character.digit (date.charAt(iDigit), 10);
            day = _resource.getString("replication-schedule-date" + days[iDay], "label");
            out.append (day + " ");
            iDigit ++;
        }

        return out.toString ();
    }
    
    /*==========================================================
     * variables
     *==========================================================*/
    private static final String AGREEMENT_EXTENSION = "replication-";
     
    public static final String LEGACYR_AGREEMENT = "LEGACYR";
    public static final String MMR_AGREEMENT = "MMR";
    public static final String NEW_WIZARD = "NEW";
    public static final String COPY_WIZARD = "COPY";
     
    public static final String AD_AGREEMENT = "AD";
    public static final String NT_AGREEMENT = "NT";
    
    static final int SELATTR_ALL     = ReplicationAgreement.SELATTR_ALL;
    static final int SELATTR_INCLUDE = ReplicationAgreement.SELATTR_INCLUDE;
    static final int SELATTR_EXCLUDE = ReplicationAgreement.SELATTR_EXCLUDE;
    
    public static final String CIR_AGREEMENT = "CIR";
    public static final String SIR_AGREEMENT = "SIR";
    
     
    //get resource bundle
    public static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
	                
    public static final boolean doFilteredReplication = false;
    public static final boolean doSelectiveReplication = false;
}
