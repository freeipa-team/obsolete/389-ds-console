/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;

/**
 * Panel for Directory Server Legacy Replication Setting
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class LegacySupplierSettingPanel extends BlankPanel {
    
    /*==========================================================
     * constructors
     *==========================================================*/
     
    /**
     * Public Constructor
     * construction is delayed until selected.
     * @param parent parent panel
     */
	public LegacySupplierSettingPanel(IDSModel model,
									  LDAPEntry replicaEntry) {
	    super(model, "replication");
	    setTitle(_resource.getString(_section+"-setting","title"));
	    _model = model;
		_helpToken = "configuration-replication-legacysuppliersettings-help";
		_replicaEntry = replicaEntry;
	}
	
	/*==========================================================
	 * public methods
     *==========================================================*/	
	
    //================== ITabPanel functions ===================
	
	/**
	 * Actual Panel construction
	 */
	public void init() {
		boolean replicaExists = true;
		String replicaDN = _replicaEntry.getDN();

		// search for the legacy changelog entry.  It should be a child of the
		// replica entry with the legacy changelog objectclass
		boolean clogExists = false;
		if (replicaExists) {
			LDAPSearchResults res = null;
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			try {
				String filter = "(objectclass=" + LEGACY_CHANGELOG_OBJECTCLASS + ")";
				res = ldc.search(replicaDN, ldc.SCOPE_SUB,
								 filter, null, false);
				clogExists = res != null && res.hasMoreElements();
				if (clogExists) {
					LDAPEntry lde = (LDAPEntry)res.nextElement();
					_clogSettingDN = lde.getDN();
				}
			} catch (LDAPException e) {
			} finally {
				try {
					if (res != null) {
						ldc.abandon(res);
					}
				} catch (LDAPException lde2) {
				}
			}
		}

		if (!clogExists) {
			_clogSettingDN = CHANGELOG_RDN + "," + replicaDN;
		}

		GridBagLayout gb = new GridBagLayout();
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
		_myPanel.setLayout(gb);
        _myPanel.setBackground(getBackground());
        _myPanel.setPreferredSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        _myPanel.setMaximumSize(ReplicationTool.DEFAULT_PANEL_SIZE);
	    
        JPanel top = new JPanel();
        top.setLayout(new GridBagLayout());
        top.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gbc.weighty = 0.2;
        gb.setConstraints(top, gbc);
        _myPanel.add(top);
		
		//add Description label
        JTextArea desc =
			UIFactory.makeMultiLineLabel( 2, 30,
										  _resource.getString(
											  _section,"setting-desc") );
        gbc.anchor = gbc.NORTH;
        gbc.weighty = 1.0;
		gbc.fill = gbc.HORIZONTAL;
        gbc.gridwidth = gbc.REMAINDER;
        top.add(desc, gbc);
        
		_use4Check = makeJCheckBox (_section, "usechangelog4", clogExists, _resource);
		gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
		_myPanel.add(_use4Check, gbc);

        //add Changelog panel
		String title = _resource.getString(_section+"-changelog","label");
        JPanel normal = new GroupPanel(title);        
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.weighty=1.0;
        _myPanel.add(normal, gbc);

        //add Changelog DB Directory
        JLabel db = makeJLabel(_section, "cLogDBDir", _resource);
		desc.setFont( db.getFont() );
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        normal.add(db, gbc);

        _dbText= makeJTextField(_section, "cLogDBDir", _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 0.5;
        normal.add(_dbText, gbc);

		// Browse and Use default buttons

		// this is just an empty label to align buttons with text box

		_browseButton = makeJButton (_resource.getString(
               "replication-supplier-browseButton","label"));
		_browseButton.setEnabled (isLocal ());

        _defaultButton = makeJButton (_resource.getString(
               "replication-supplier-defaultButton","label"));	
       
		JButton[] buttons = {_browseButton, _defaultButton};
		JPanel buttonPanel = UIFactory.makeJButtonPanel( buttons );
		
		ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = _gbc.REMAINDER;
		gbc.fill = _gbc.HORIZONTAL;
		gbc.weightx = 1.0;
        normal.add(buttonPanel, gbc);

		// add changelog suffix		        
		JLabel suffix = makeJLabel(_section, "cLogSuffix", _resource);
		desc.setFont( suffix.getFont() );
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        normal.add(suffix, gbc);

        _suffixText= makeJTextField(_section, "cLogSuffix", _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 0.5;
        normal.add(_suffixText, gbc);
        
         //add Max Changelog Record
        JLabel record = makeJLabel(_section, "cLogMaxRec", _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
        normal.add(record, gbc);

        _recordText = makeNumericalJTextField(_section, "cLogMaxRec", _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth=1;
		gbc.weightx = 0.5;
        normal.add(_recordText, gbc);

		_recordCheckbox = makeJCheckBox (_resource.getString(
			              "replication-supplier-cUnlimited","label"));
		gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
		normal.add(_recordCheckbox, gbc);
        
         //add Max Changelog Age
        JLabel age = makeJLabel(_section, "cLogMaxAge", _resource);
        ReplicationTool.resetGBC(gbc);
		gbc.gridwidth=1;
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;
		gbc.insets = ReplicationTool.BOTTOM_INSETS;
        normal.add(age, gbc);
        
        _ageText= makeNumericalJTextField(_section, "cLogMaxAge", _resource);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = 1;
       	gbc.insets = ReplicationTool.BOTTOM_INSETS;
		gbc.weightx = 0.5;
        normal.add(_ageText, gbc);
      
        //add Time Unit
        _timeUnit = makeJComboBox(_section, "cLogMaxAgeUnit", null,
            _resource);
		ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.RELATIVE;
        gbc.insets = ReplicationTool.BOTTOM_INSETS;
        normal.add(_timeUnit, gbc);

		_ageCheckbox = makeJCheckBox (_resource.getString(
			           "replication-supplier-cUnlimited","label"));
		gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
		normal.add(_ageCheckbox, gbc);

        //create DSEntry for the fields
		// No validation of attributes, because they are not necessarily
		// there
		LDAPEntry newEntry = null;
		if (!clogExists) { // i.e. the entry exists
			// add the more well known attributes and values
			LDAPAttributeSet las = new LDAPAttributeSet();
			las.add(new LDAPAttribute("objectclass", DEFAULT_OBJECTCLASSES));
			newEntry = new LDAPEntry(_clogSettingDN, las);
		}

        //create DSEntry for the fileds
		// No validation of attributes, because they are not necessarily
		// there; also, the entry may not exist either
		DSEntrySet entries = new DSEntrySet( false, true, newEntry );
		setDSEntrySet( entries );
        
		DSEntryBoolean enableEntry = new DSEntryBooleanFake(clogExists ? "on" : "off",
			_use4Check, CHANGELOG_CN);
		entries.add(_clogSettingDN, "cn", enableEntry);
		setComponentTable(_use4Check, enableEntry);

        //database location
        _lDSEntry = new DBChangeEntry("", _dbText, db);
        entries.add(_clogSettingDN, CHANGELOG_DIR_ATTR_NAME, _lDSEntry);
        setComponentTable(_dbText, _lDSEntry);
        
        //changelog suffix
        _suffixDSEntry = new SuffixChangeEntry(this, "", _suffixText,suffix);
        entries.add(_clogSettingDN, CHANGELOG_DN_ATTR_NAME, _suffixDSEntry);
        setComponentTable(_suffixText, _suffixDSEntry);
        
		//max changelog record
        DSEntryInteger recordDSEntry = new recordDSEntryInteger(null, _recordText,
            record, 1, LIMIT_MAX_VAL, 1, true);
        entries.add(_clogSettingDN, CHANGELOG_MAX_RECORD_ATTR_NAME, 
            recordDSEntry);
        setComponentTable(_recordText, recordDSEntry);

		// create entry for the checkbox - so it is correctly updated
		_recordCheckDSEntry = 
                           new DSEntryBoolean ("0", _recordCheckbox, record);
		setComponentTable(_recordCheckbox, _recordCheckDSEntry);
		                       
        //max changelog age

        DSEntryInteger ageDSEntry = new ageDSEntryInteger("", _ageText, age,
            1, LIMIT_MAX_VAL, 1, true);
        entries.add(_clogSettingDN, CHANGELOG_MAX_AGE_ATTR_NAME, ageDSEntry);
        setComponentTable(_ageText, ageDSEntry);

		// create entry for the checkbox - so it is correctly updated
		_ageCheckDSEntry =  new DSEntryBoolean ("0", _ageCheckbox, age);
		setComponentTable(_ageCheckbox, _ageCheckDSEntry);

        //changelog unit
        String[] str = null;
        DSEntryCombo timeDSEntry = new DSEntryCombo(str, _timeUnit, age, true) {
            public void show() {}
            public void store() {}
			protected void setInitModel () {}
        };
       
        setComponentTable(_timeUnit, timeDSEntry);

		// add Remove Changelog button
		_removeButton = makeJButton (_resource.getString(
               "replication-supplier-removeButton","label"));
		
        JButton[] removeButtons = {_removeButton};
		JPanel removeButtonPanel = UIFactory.makeJButtonPanel(removeButtons);
		
		//ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = _gbc.REMAINDER;
		gbc.fill = _gbc.HORIZONTAL;
		gbc.weightx = 1.0;
        _myPanel.add(removeButtonPanel, gbc);

        addBottomGlue ();

		clogEnableFields(clogExists);

        _initialized =true;
	}

	/**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(_recordCheckbox)) {
			enableRecord(!_recordCheckbox.isSelected());
			super.actionPerformed(e);  // pass to parent for correct update
        } else if (e.getSource().equals(_ageCheckbox)) {
			enableAge(!_ageCheckbox.isSelected());
			super.actionPerformed(e);  // pass to parent for correct update
        } 
		else if (e.getSource().equals(_browseButton)) {
			DisplayBrowseDialog ();
			super.actionPerformed(e);  // pass to parent for correct update
		}
		else if (e.getSource().equals(_defaultButton)) {
			SetDefaultDir ();
			super.actionPerformed(e);  // pass to parent for correct update
		}
		else if (e.getSource().equals(_removeButton)) {
			RemoveChangelog ();
		}
		else if (e.getSource().equals(_use4Check)) {
			clogEnableFields();
			super.actionPerformed(e);  // pass to parent for correct update
		}
    }

	private void clogEnableFields() {
		if (_use4Check == null) {
			return;
		}
		boolean state = _use4Check.isSelected();
		clogEnableFields(state);
	}

	private void clogEnableFields(boolean state) {
		_dbText.setEnabled(state);
		_browseButton.setEnabled(state && isLocal());
		_defaultButton.setEnabled(state);
		_suffixText.setEnabled(state);

		_recordCheckbox.setEnabled(state);
		boolean recState = state && !_recordCheckbox.isSelected();
		_recordText.setEnabled(recState);

		_ageCheckbox.setEnabled(state);
		boolean ageState = state && !_ageCheckbox.isSelected();
		_ageText.setEnabled(ageState);
		_timeUnit.setEnabled(ageState);

		return;
	}

	private void DisplayBrowseDialog () {
		String dir = _dbText.getText();
		if ( File.separator.equals("\\") ) {
			dir = dir.replace( '/', '\\' );
		}
		dir = DSFileDialog.getDirectoryName( dir, true, this );
        if (dir != null)
            _dbText.setText(dir);
	}

	private void SetDefaultDir () {
                String dir = DSUtil.getDefaultChangelogPath( getModel().getServerInfo() );
		_dbText.setText(dir);
	}

	private void RemoveChangelog () {
		// ask for confirmation
		if ( requiresConfirmation(
			GlobalConstants.PREFERENCES_CONFIRM_MODIFY_CHANGELOG ) ) {
			int option = DSUtil.showConfirmationDialog(
				LegacySupplierSettingPanel.this,
				"remove-change",
				"",
				_section,
				_resource);
			if (option == JOptionPane.NO_OPTION) {
				return;
			}
		}

        // reset the form
		_dbText.setText ("");
        _suffixText.setText ("");
        _recordText.setText ("");
        _ageText.setText ("");
        _recordCheckbox.setSelected (true);
        _ageCheckbox.setSelected (true);

		clearDirtyFlag ();

		_removeButton.setEnabled (false);
	}

	public void okCallback() {
		if (_lDSEntry.confirmStore() && _suffixDSEntry.dsValidate()) {
			super.okCallback ();
			_removeButton.setEnabled (true);
		}
	}

    public void resetCallback() {
	    
        clearDirtyFlag ();
	}

	private void enableRecord (boolean enable) {
		_recordText.setEnabled(enable);
        _recordText.repaint();
	}

	private void enableAge (boolean enable) {
		_ageText.setEnabled(enable);
		_ageText.repaint();
       	        _timeUnit.setEnabled (enable);
		_timeUnit.repaint ();
	}

	class DBChangeEntry extends DSEntryTextStrict {
		DBChangeEntry(String model, JTextField field) {
			super(model, field);
		}
		DBChangeEntry(String model, JComponent field1, JComponent field2) {
			super(model, field1, field2);
		}

		/**
		 * displays the data stored in the entry
		 */
		public void show() {
			super.show();
			JTextField tf = (JTextField)getView(0);
			_originalDb = tf.getText();
				
			_removeButton.setEnabled (!_originalDb.equals (""));
		}

		/**
		 * supposed to store data from the view to the model
		 * does nothing because the data is automatically
		 * updated when it is modified by updateModel function
		 */
		public void store() {
				
		}

		/**
		 * confirms whether the data should be updated
		 * @return true if it should be updated and false
		 * otherwise
		 */  
		public boolean confirmStore () {
			JTextField tf = (JTextField)getView(0);
			String   text = tf.getText().trim();

			/* Have to warn against changing the changelog db */
			if ( !_originalDb.equals("") &&
				 !_originalDb.equals( tf.getText().trim() ) ) {
				int option = DSUtil.showConfirmationDialog(
					LegacySupplierSettingPanel.this,
					"change-db", "", _section, _resource);

				if (option == JOptionPane.NO_OPTION) {
					return false;
				}
				else {
					show();
				}
			}

			return true;
		}
	}

	class SuffixChangeEntry extends DSEntryTextStrict {
		SuffixChangeEntry(LegacySupplierSettingPanel parent, String model, JTextField field) {
			super(model, field);
			_parent = parent;
		}
		SuffixChangeEntry(LegacySupplierSettingPanel parent, String model, 
						  JComponent field1, JComponent field2) {
			super(model, field1, field2);
			_parent = parent;
		}
         
		/**
		 * displays the data stored in the model
		 */

		public void show() {
			/* set default value of cn=changelog */
			JTextField tf	 = (JTextField)getView(0);
			String     value = getModel (0);

			if (value == null || value.equals (""))
				tf.setText(DEFAULT_SUFFIX);
			else
				tf.setText(value);
                    
			viewInitialized ();		
		}

		/**
		 * supposed to store data from the view to the model
		 * does nothing because the data is automatically
		 * updated when it is modified by updateModel function
		 */
		public void store () {
				
		}

		public boolean dsValidate(){               
			String value = getModel (0);
			JFrame frame = _parent._model.getServerInfo().getFrame();
			String msg;

			/* not modified or default is used - it's ok */
			if (!isModified () || value.equals (DEFAULT_SUFFIX))
				return true;

			/* make sure that this suffix is not in use */
			try{
				if (!publicSuffix (value) && !privateSuffix (value)){
					return true;
				}else{
					msg = _resource.getString("replication-supplier-dialog",
											  "duplicateSuffix");
					DSUtil.showErrorDialog(frame, "error", msg,
										   "replication-dialog" );

					return false;
				}
			}catch ( LDAPException lde ) {
				Debug.println("SuffixChangeEntry.dsValidate:" + lde.toString());

				if (lde.getLDAPResultCode() == LDAPException.INSUFFICIENT_ACCESS_RIGHTS){
					msg = _resource.getString("replication-supplier-dialog",
											  "suffixRights");                        
				}else{
					msg = _resource.getString("replication-supplier-dialog",
											  "suffixGeneral");    
				}            

				DSUtil.showErrorDialog( frame, "error", msg,
										"replication-dialog" );
			}

			return false;
		}

		private boolean publicSuffix (String value) throws LDAPException{
			return (isSuffix (value, CONFIG_DN, PRIVATE_SUFFIX_ATTR));
		}

		private boolean privateSuffix (String value) throws LDAPException{
			return (isSuffix (value, DBCONFIG_DN,
							  ReplicationTool.PUBLIC_SUFFIX_ATTR));
		}

		private boolean isSuffix (String value, String configSuffix, 
								  String suffixAttr) throws LDAPException{
			String attrs[] = {""};
			LDAPEntry configEntry;
			attrs[0] = suffixAttr;
			LDAPConnection ldc = _parent._model.getServerInfo().getLDAPConnection();
			configEntry = ldc.read( configSuffix, attrs );
			if ( configEntry != null ) {
				Enumeration e =
					configEntry.getAttribute( attrs[0] ).getStringValues();
				while( e.hasMoreElements() ) {
					if (value.equals (e.nextElement ())){
						return true;	
					}
				}
			}

			return false;
		}

		LegacySupplierSettingPanel _parent;
	}

	class recordDSEntryInteger extends DSEntryInteger {
		recordDSEntryInteger ( String model,
							   JComponent view1,
							   JComponent view2,
							   int minValue,
							   int maxValue,
							   int scaleFactor,
							   boolean blankOkay ) {
			super( model, view1, view2, minValue, maxValue,
				   scaleFactor, blankOkay );
		}

		/**
		 * displays the data stored in the model
		 */

		public void show() {
				
			// get the model as an integer
			String s = getModel(0);
			if (s.length() > 0) {
				int val = 0;
				try {
					val = Integer.parseInt(s);
				} catch (NumberFormatException nfe) {
					val = 0;
				}
				// convert using the scaleFactor
				if (_scaleFactor != 0)
					val /= _scaleFactor;
					
				if (val == 0)
					s = "";
				else
					s = Integer.toString(val);
			}

			// update the view
			JTextField tf = (JTextField)getView(0);
			tf.setText(s);

			if (s == ""){	// use default - unlimited
				_recordCheckDSEntry.fakeInitModel ("on");
				_recordCheckbox.setSelected (true);
				enableRecord (false);
			}
			else
				_recordCheckDSEntry.fakeInitModel ("off");

			viewInitialized ();
		}
				
		/**
		 * supposed to store data from the view to the model
		 * does nothing because the data is automatically
		 * updated when it is modified by updateModel function
		 */
		public void store() {
			
		}

		/**
		 * First, call validate to see if the value is valid.
		 * If not valid, don't do anything.  validate() will usually
		 * be called before store() by DSEntrySet.  If the field is
		 * valid, we can assume it is an integer. Get the value as an
		 * integer and apply the scale factor, then update
		 * the model.
		 */

		protected void updateModel () {
			String s;

			if (_recordCheckbox.isSelected ())
				s="";
			else {
				JTextField tf = (JTextField)getView(0);
				s = tf.getText();

				if (s.length() > 0) {
					int val = 0;
					try {
						val = Integer.parseInt(s);
					} catch (NumberFormatException nfe) {
						val = 0;
					}
					val *= _scaleFactor;
					s = Integer.toString(val);
				}
			}

			setModelAt(s, 0);
		}
	}

	class ageDSEntryInteger extends DSEntryInteger {
		ageDSEntryInteger ( String model,
							JComponent view1,
							JComponent view2,
							int minValue,
							int maxValue,
							int scaleFactor,
							boolean blankOkay ) {
			super( model, view1, view2, minValue, maxValue,
				   scaleFactor, blankOkay );
		}

		/**
		 * displays the data stored in the model
		 */

		public void show() {
			// get the model as a string.
			// It may be blank, or it may be a number followed by a
			// unit specifier
			String s = getModel(0);
			if ((s == null) || (s.length() < 1)) {
				s = "";
				_timeUnit.setSelectedItem(
										  _resource.getString(_section,
															  "cLogMaxAgeUnit-default"));

				_ageCheckDSEntry.fakeInitModel ("on");
				_ageCheckbox.setSelected (true);
				enableAge (false);
			}
			else
				_ageCheckDSEntry.fakeInitModel ("off");
				
			if (s.length() > 0) {				
				char unit = s.charAt(s.length()-1);
				int unitVal = charToUnit(unit);
				if ( unitVal == -1) {
					unitVal = DEFAULT_AGE_UNIT;
				} else {
					// Strip off the unit specifier
					s = s.substring(0, s.length()-1);
				}
				int val = 0;
				try {
					val = Integer.parseInt(s);
				} catch (NumberFormatException nfe) {
					val = 0;
				}
				s = Integer.toString(val);
				// Update combo box
				_timeUnit.setSelectedIndex(unitVal);
			}
			// update the view
			JTextField tf = (JTextField)getView(0);
			tf.setText(s);

			viewInitialized ();
		}
				
		/**
		 * supposed to store data from the view to the model
		 * does nothing because the data is automatically
		 * updated when it is modified by updateModel function
		 */
		public void store() {
				
		}

		/**
		 * First, call validate to see if the value is valid.
		 * If not valid, don't do anything.  validate() will usually
		 * be called before store() by DSEntrySet.  If the field is
		 * valid, we can assume it is an integer. Get the value as an
		 * integer and apply the scale factor, then update
		 * the model.
		 */

		protected void updateModel () {
			String s;

			if (_ageCheckbox.isSelected ())
				s="";
			else {
				JTextField tf = (JTextField)getView(0);
				s = tf.getText();
				if (s.length() > 0) {
					int val = 0;
					try {
						val = Integer.parseInt(s);
					} catch (NumberFormatException nfe) {
						val = 0;
					}
					// Tag on the unit specifier to the end
					s = Integer.toString(val) +
						unitToChar(_timeUnit.getSelectedIndex());
				}

				//Debug.println ("Age.updateModel: s = (" + s + ")");
				setModelAt(s, 0);
			}
		}
	}

	static private int charToUnit(char unit) {
		int unitVal = -1;
		if (unit == 'h') {
			unitVal = HOUR_AGE_UNIT;
		} else if (unit == 's') {
			unitVal = SECOND_AGE_UNIT;
		} else if (unit == 'm') {
			unitVal = MINUTE_AGE_UNIT;
		} else if (unit == 'd') {
			unitVal = DAY_AGE_UNIT;
		} else if (unit == 'w') {
			unitVal = WEEK_AGE_UNIT;
		}
		return unitVal;
	}

	static private char unitToChar(int unitVal) {
		char unit = 0;
		if (unitVal == HOUR_AGE_UNIT) {
			unit = 'h';
		} else if (unitVal == SECOND_AGE_UNIT) {
			unit = 's';
		} else if (unitVal == MINUTE_AGE_UNIT) {
			unit = 'm';
		} else if (unitVal == DAY_AGE_UNIT) {
			unit = 'd';
		} else if (unitVal == WEEK_AGE_UNIT) {
			unit = 'w';
		}
		return unit;
	}

    /*==========================================================
     * variables
     *==========================================================*/
    private IDSModel _model;
	private boolean _initialized = false;
	private LDAPEntry _replicaEntry = null;
	
	//XXX Data Temporary Stored here
	private String cLogDBDir;
	private String cLogSuffix;
	private String cLogMaxRec;
	private String cLogMaxAge;
	private int cLogMaxAgeUnit;
	private String _originalDb;
	private String _clogSettingDN;

	private JCheckBox _use4Check;
	private JTextField _dbText;
    private JTextField _suffixText;
    private JTextField _recordText;
    private JTextField _ageText;
    private JComboBox  _timeUnit;
	private JCheckBox  _recordCheckbox;
	private JCheckBox  _ageCheckbox;
	private JButton    _browseButton;
	private JButton    _defaultButton;
	private JButton    _removeButton;

	private DSEntryBoolean _recordCheckDSEntry;
	private DSEntryBoolean _ageCheckDSEntry;
	private DBChangeEntry  _lDSEntry;
    private SuffixChangeEntry _suffixDSEntry;
   
    private static int SECOND_AGE_UNIT = 0;
	private static int MINUTE_AGE_UNIT = 1;
	private static int HOUR_AGE_UNIT = 2;
	private static int DAY_AGE_UNIT = 3;
	private static int WEEK_AGE_UNIT = 4;
	private static int DEFAULT_AGE_UNIT = DAY_AGE_UNIT; //day
    
	private static final String REMOVE = "remove";
	private static final String DEFAULT_SUFFIX = "cn=changelog";
    private static final String CONFIG_DN = "cn=config";
    private static final String PRIVATE_SUFFIX_ATTR = "nsslapd-privatenamespaces";
    private static final String DBCONFIG_DN = "cn=config,cn=ldbm";
	private static final String LEGACY_CHANGELOG_OBJECTCLASS = "nsChangelog4Config";

	private static ResourceSet _resource =
       new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
	private static final String _section = "replication-legacysupplier";
	private static final int LIMIT_MIN_VAL = 0;
	private static final int LIMIT_MAX_VAL = Integer.MAX_VALUE;
	private static final String CHANGELOG_CN = "legacy changelog";
	private static final String CHANGELOG_RDN = "cn=" + CHANGELOG_CN;
	private static final String CHANGELOG_DIR_ATTR_NAME =
	                                       "nsslapd-changelogdir";
	private static final String CHANGELOG_DN_ATTR_NAME =
                                           "nsslapd-changelogsuffix";
	private static final String CHANGELOG_MAX_RECORD_ATTR_NAME =
                                           "nsslapd-changelogmaxentries";
	private static final String CHANGELOG_MAX_AGE_ATTR_NAME =
                                           "nsslapd-changelogmaxage";
    private static final String[] COMBO_ENTRIES =
	                                       {"sec", "min", "hr", "day", "week"};
	private static final String[] DEFAULT_OBJECTCLASSES = {"top",
														   "extensibleObject",
														   LEGACY_CHANGELOG_OBJECTCLASS};
}
