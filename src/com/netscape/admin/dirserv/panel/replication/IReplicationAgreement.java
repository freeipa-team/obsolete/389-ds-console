/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.util.*;
import java.text.*;
import java.net.*;
import java.io.*;
import javax.swing.*;
import netscape.ldap.*;
import netscape.ldap.controls.*;
import netscape.ldap.util.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.task.ReadOnly;

/**
 * Replication Agreement interface
 *
 * @author  richm
 * @version %I%, %G%
 * @date	 	02/25/00
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public interface IReplicationAgreement {
	/*==========================================================
	 * public methods
     *==========================================================*/
     
    /**
     * Set the current server information
     */
    public void setServerInfo(ConsoleInfo info);
    
    /**
     * Get server info
     */
    public ConsoleInfo getServerInfo();

    /**
     * Set the nickname for this agreement.
     */
    public void setNickname(String s);

    /**
     * Get the nickname for this agreement.
     */
    public String getNickname();

    /**
     * Set the original DN of the entry corresponding to this agreement.
     */
    public void setOrigEntryDN(String dn);

    /**
     * Get the original DN of the entry corresponding to this agreement.
     */
    public String getOrigEntryDN();

    /**
     * Get the DN of the entry corresponding to this agreement.
     */
    public String getEntryDN();

    /**
     * Set the root of the replicated subtree.  This routine
     * removes any extra spaces between DN components.
     */
    public void setReplicatedSubtree(String r);

    /**
     * Get the root of the replicated subtree.
     */
    public String getReplicatedSubtree();

    /**
     * Set the remote host name of the server
     */
    public void setRemoteHost(String h);

    /**
     * Get the remote host name
     */
	public String getRemoteHost();

    /**
     * Set the remote port of the server
     */
    public void setRemotePort(int p);

    /**
     * Get the remote port
     */
    public int getRemotePort();

    /**
     * Set the DN used when binding to the consumer.
     */
    public void setBindDN(String dn);

    /**
     * Get the DN used when binding to the consumer.
     */
    public String getBindDN();

    /**
     * Set the credentials used when binding to the consumer.
     */
    public void setBindCredentials(String cred);

    /**
     * Get the credentials used when binding to the consumer.
     */
    public String getBindCredentials();

    /**
     * Set the schedule used to update the consumer.
     */
    public void setUpdateSchedule(Vector sched);

    /**
     * Set the schedule used to update the consumer.
     */
    public void setUpdateSchedule(String s);

    /**
     * Add to the update schedule
     */
    public void addUpdateSchedule(String schedItem);

    /**
     * Get the schedule used to update the consumer.
     */
    public Vector getUpdateSchedule();

    /**
     * Get the replication schedule, as an array of Strings
     */
    public String[] getUpdateScheduleStrings();

    /**
     * Set the useStartTLS flag
     */
    public void setUseStartTLS(String val);

    /**
     * Set the useSSL flag
     */
    public void setUseStartTLS(boolean val);

    /**
     * Get the useSSL flag
     */
    public boolean getUseStartTLS();

    /**
     * Set the useSSL flag
     */
    public void setUseSSL(String val);

    /**
     * Set the useSSL flag
     */
    public void setUseSSL(boolean val);

    /**
     * Get the useSSL flag
     */
    public boolean getUseSSL();

    /**
     * Set the sslAuth flag
     */
    public void setUseSSLAuth(String val);

    /**
     * Set the sslAuth flag
     */
    public void setUseSSLAuth(boolean val);

    /**
     * Get the sslAuth flag
     */
    public boolean getUseSSLAuth();

    /**
     * Set the GSSAPIAuth flag
     */
    public void setUseGSSAPIAuth(String val);

    /**
     * Set the GSSAPIAuth flag
     */
    public void setUseGSSAPIAuth(boolean val);

    /**
     * Get the GSSAPIAuth flag
     */
    public boolean getUseGSSAPIAuth();

    /**
     * Set the DigestAuth flag
     */
    public void setUseDigestAuth(String val);

    /**
     * Set the DigestAuth flag
     */
    public void setUseDigestAuth(boolean val);

    /**
     * Get the DigestAuth flag
     */
    public boolean getUseDigestAuth();

    /**
     * Get the CN (common name) of this entry
     */
    public String getEntryCN();

    /**
     * Explicitly set the CN (common name) of this entry
     */
    public void setEntryCN(String cn);

    /**
     * Set the replica entry filter (for filtered replication)
     */
    public void setEntryFilter(String f);

    /**
     * Get the replica entry filter (for filtered replication)
     */
    public String getEntryFilter();

    /**
     * Get the type of attribute selection
     */
    public int getSelattrType();

    /**
     * Set the type of attribute selection
     */
    public void setSelattrType(int t);

    /**
     * Get the list of selected attributes
     */
    public Vector getSelectedAttributes();

    public String getSelectedAttributesString();

    /**
     * Set the list of selected attributes in Sorted Vector format
     */
    public void setSelectedAttributes(Vector s);

    /**
     * Set the list of selected attributes, sorting it on the
     * way in.
     */
    public void setSelectedAttributes(String s);

    /**
     * Return a long, descriptive string for this agreement
     */
    public String getDescription();

    /**
     * Set a reference to the other replciation agreements
     */
    public void setAgreementTable(AgreementTable r);

    /**
     * Return true if the agreement's DN has changed, false otherwise
     */
    public boolean agreementDNHasChanged();

    /**
     * Return true if the agreement is new (that is, it does not yet
     * exist as an entry in the server's database.
     */
    public boolean getAgreementIsNew();
    
    public int writeAgreementToServer();

    public void deleteAgreementFromServer() throws LDAPException;
    
    /**
     * Verify that the agreement is valid, and doesn't conflict with any
     * other agreements we know about.  "conflict" means has the same
     * server, port, and subtree.  Returns a vector of integers describing
     * the errors, which will be empty if there were no errors.
     */
    public Vector checkForErrors();

    /**
     * Cause the replica to be updated immediately
     */
    public void updateNow();

    /**
     * Cause online replica creation to begin for this agreement
	 * @return Status ot ORCTask
     */
    public int initializeConsumer();

	/**
	 * Create an LDIF file with data to initialize the consumer
	 *
	 * @param fileName Name of file for consumer data
	 * @return true if the file creation succeeded
	 */
	public boolean populateLDIFFile (String fileName);

    /**
     * Check to see if the consumer needs to be reinitialized based on
     * values changed.
	 *
	 * @return true if the consumer needs to be reinitialized
     */
    public boolean checkForReORC();
    
    /**
     * Get replication status from Directory
     */
    public void updateReplicaStatus();
    
    public String getLastUpdateStatus();
    public String getInProgress();
    public String getNChangesLast();
    public String getLastUpdateBegin();
    public String getLastUpdateEnd();

    public static final int AGREEMENT_EXISTS = 1;
    public static final int MISSING_HOST = 2;
    public static final int ILLEGAL_PORT = 3;
    public static final int MISSING_BINDDN = 4;
    public static final int MISSING_BINDCREDENTIALS = 5;
    public static final int MISSING_REPLICATED_SUBTREE = 6;
    public static final int SUPPLIER_LOOP = 7;

    static final int SELATTR_ALL     = 0;
    static final int SELATTR_INCLUDE = 1;
    static final int SELATTR_EXCLUDE = 2;
}

