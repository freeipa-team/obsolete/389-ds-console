/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.wizard.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;

/**
 * Content Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class WAgreementAttributePanel extends WAgreementPanel
    implements IWizardPanel, ActionListener
{
    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    public WAgreementAttributePanel() {
        super();
		_helpToken = "replication-wizard-attribute-help";
		_section = "replication-attribute";
    }

	/*==========================================================
	 * public methods
     *==========================================================*/

    /**
	 * Actual panel construction
	 */
    public void init() {
		GridBagLayout gb = new GridBagLayout();
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
		setLayout(gb);
        setPreferredSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        setMaximumSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        
        JLabel ask = new JLabel(_resource.getString(_section, "ask-label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(10,5,10,10);
        gb.setConstraints(ask, gbc);
        add(ask);

	String title = _resource.getString(_section, "entries-label");
        // For now we don't do filtered replication
        if (false)
        {
        //===========  Entries Panel ============================
        _entriesPanel = new GroupPanel(title);
        GridBagLayout gb1 = new GridBagLayout();
        _entriesPanel.setLayout(gb1);        
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gb.setConstraints(_entriesPanel, gbc);
        add(_entriesPanel);

        //entire radio
        ButtonGroup entriesGroup = new ButtonGroup();
        _allEntries = new JRadioButton(
			_resource.getString(_section, "entriesAll-label"));
        entriesGroup.add(_allEntries);
        _allEntries.addActionListener(this);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(0,20,0,5);
        gb1.setConstraints(_allEntries, gbc);
        _entriesPanel.add(_allEntries);

        //filter radio
        _filterEntries = new JRadioButton(
			_resource.getString(_section, "matchFilter-label"));
        entriesGroup.add(_filterEntries);
        _filterEntries.addActionListener(this);
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.insets = new Insets(0,20,0,5);
        gb1.setConstraints(_filterEntries, gbc);
        _entriesPanel.add(_filterEntries);

        //filter textfield
        _filterText = new JTextField("");
        _activeColor = _filterText.getBackground();
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,30,0,5);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gb1.setConstraints(_filterText, gbc);
        _entriesPanel.add(_filterText);
        }
        
        //========= Attribute Panel ============================
	title = _resource.getString(_section, "attribute-label");
        _attrPanel = new GroupPanel(title);
        GridBagLayout gb2 = new GridBagLayout();
        _attrPanel.setLayout(gb2);        
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.gridheight = gbc.REMAINDER;
        gbc.fill = gbc.BOTH;
        gbc.weightx=1.0;
        gbc.weighty=1.0;
        gb.setConstraints(_attrPanel, gbc);
        add(_attrPanel);

        //radio buttons panel
        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new BorderLayout());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.insets = new Insets(0,20,0,20);
        gb2.setConstraints(radioPanel, gbc);
        _attrPanel.add(radioPanel);

        //fractional replication check
        _enableFR = new JCheckBox(
			_resource.getString(_section, "enableFR-label"));
        _enableFR.setToolTipText(
			_resource.getString(_section, "enableFR-ttip"));
        _enableFR.addActionListener(this);
        radioPanel.add("West", _enableFR);
        
        _warn = new JLabel(_resource.getString(_section, "warn-label"));
        _warn.setVisible(false);
        radioPanel.add("South", _warn);

        //left box panel
		title = _resource.getString(_section, "excluded-label");
        _leftPanel = new GroupPanel(title);
        _leftPanel.setLayout(new BorderLayout());        
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.fill = gbc.BOTH;
        gbc.weighty=1.0;
        gbc.weightx=0.5;
        gb2.setConstraints(_leftPanel, gbc);
        _attrPanel.add(_leftPanel);

        //attr list box
        _excludeBox = createListBox( _excludeBoxModel, 10);
        _leftScroll = createScrollPane(_excludeBox);
        _leftPanel.add("Center", _leftScroll);

        //right box Panel
		title = _resource.getString(_section, "included-label");
        _rightPanel = new GroupPanel(title);
        _rightPanel.setLayout(new BorderLayout());        
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.BOTH;
        gbc.weightx=0.5;
        gbc.weighty=1.0;
        gb2.setConstraints(_rightPanel, gbc);
        _attrPanel.add(_rightPanel);

        //replicated list box
        _includeBox = createListBox( _includeBoxModel, 10);
        _rightScroll = createScrollPane(_includeBox);
        _rightPanel.add("Center", _rightScroll);

        //==== middle button panel ====
        JPanel midPanel = new JPanel();
        GridBagLayout gb2b = new GridBagLayout();
        midPanel.setLayout(gb2b);
        //midPanel.setBorder(ReplicationTool.ETCHED_BORDER);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridheight = gbc.REMAINDER;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gb2.setConstraints(midPanel, gbc);
        _attrPanel.add(midPanel);

        //add all button
        _addAllButton = new JButton(
            _resource.getString(_section, "attributeAddAllButton-label"));
        _addAllButton.addActionListener(this);
        _addAllButton.setSize(BUTTON_MIN_SIZE);
        ReplicationTool.resetGBC(gbc);
        gb2b.setConstraints(_addAllButton, gbc);
        midPanel.add(_addAllButton);

        //add button
        _addButton = new JButton(
			_resource.getString(_section, "attributeAddButton-label"));
        _addButton.addActionListener(this);
        _addButton.setSize(BUTTON_MIN_SIZE);
        ReplicationTool.resetGBC(gbc);
        gb2b.setConstraints(_addButton, gbc);
        midPanel.add(_addButton);

        //remove button
        _removeButton = new JButton(
			_resource.getString(_section, "attributeRemoveButton-label"));
        _removeButton.addActionListener(this);
        _removeButton.setSize(BUTTON_MIN_SIZE);
        ReplicationTool.resetGBC(gbc);
        gb2b.setConstraints(_removeButton, gbc);
        midPanel.add(_removeButton);

        //remove all button
        _removeAllButton = new JButton(
			_resource.getString(_section, "attributeRemoveAllButton-label"));
        _removeAllButton.addActionListener(this);
        _removeAllButton.setSize(BUTTON_MIN_SIZE);
        ReplicationTool.resetGBC(gbc);
        gbc.gridwidth = gbc.REMAINDER;
        gb2b.setConstraints(_removeAllButton, gbc);
        midPanel.add(_removeAllButton);

        populateData();
        _initialized = true;
    }

    //========= ACTIONLISTENER =================
    public void actionPerformed(ActionEvent e) {
        if (_initialized) {
            Debug.println("AgreementAttributePanel: actionPerformed()"+e.toString());
            if (e.getSource().equals(_allEntries)) {
                _filterText.setEditable(false);
                _filterText.setBackground(getBackground());
                _filterText.repaint(1);
            }
            if (e.getSource().equals(_filterEntries)) {
                _filterText.setEditable(true);
                _filterText.setBackground(_activeColor);
                _filterText.repaint(1);
            }
            if (e.getSource().equals(_enableFR)) {
                if (_enableFR.isSelected()) {
                    populateExcludeList(false);
                    enableAttrSelection();
                } else {
                    populateExcludeList(false);
                    disableAttrSelection();
                }
                _warn.setVisible(_enableFR.isSelected());
            }
            if (e.getSource().equals(_addAllButton)) {
                populateExcludeList(false);
            }
            if (e.getSource().equals(_addButton)) {
                //get selected and add them over
                Object obj[] = _excludeBox.getSelectedValues();
                if (obj.length > 0) {
                    for (int i=0; i< obj.length; i++) {
                        SchemaUtility.InsertElement(_includeBoxModel,obj[i]);
                        _excludeBoxModel.removeElement(obj[i]);
                    }
                }
                _excludeBox.invalidate();
                _excludeBox.validate();
                _excludeBox.repaint(1);
            }
            if (e.getSource().equals(_removeButton)) {
                //get selected and move them over
                Object obj[] = _includeBox.getSelectedValues();
                if (obj.length > 0) {
                    for (int i=0; i< obj.length; i++) {
                        SchemaUtility.InsertElement(_excludeBoxModel,obj[i]);
                        _includeBoxModel.removeElement(obj[i]);
                    }
                }
                _includeBox.invalidate();
                _includeBox.validate();
                _includeBox.repaint(1);
            }
            if (e.getSource().equals(_removeAllButton)) {
                populateExcludeList(true);
            }
        }
    }

    //====== IWizardPanel =============================
    public boolean initializePanel(WizardInfo info) {
        Debug.println("WAgreementAttributePanel: initialize()");
        _wizardInfo = (AgreementWizardInfo) info;
        if (_wizardInfo.getAgreementType().equals(_wizardInfo.LEGACYR_AGREEMENT))
            return false;
        if (!_initialized)
            init();
        return true;
    }
    
    public boolean validatePanel() {
        Debug.println("WAgreementAttributePanel: validate()");
        
        // We don't do filtered replication yet
        if (false) 
        {
        //filter
        if ((_filterEntries.isSelected()) && (_filterText.getText().equals(""))) {
            _error = _resource.getString(_section, "dialog-filterEmpty");
            return false;
        }
        }
        if (_includeBoxModel.size() == 0) {
            _error = _resource.getString(_section, "dialog-noAttr");
            return false;   
        }
        
        return true;
    }
    
    public boolean concludePanel(WizardInfo info) {
        Debug.println("WAgreementAttributePanel: conclude()");
        return true;
    }
    
    public void getUpdateInfo(WizardInfo info) {
        Debug.println("WAgreementAttributePanel: getUpdateInfo()");
        
        // We don't do filtered replication yet
        if (false)
        {
        //save filter
        if (_filterEntries.isSelected())
            _wizardInfo.setFilter(_filterText.getText());
        else
            _wizardInfo.setFilter("");
        }
        //save attr
        Vector attr = new Vector();
        if (_enableFR.isSelected() ) { 
            if (_excludeBoxModel.size() == 0) {
                _enableFR.setSelected(true);
                _wizardInfo.setAttrType(_wizardInfo.SELATTR_ALL);
                disableAttrSelection();
            } else {
            	// We never allow 'INCLUDE' lists at present: they're not supported in the server
                if (false && _includeBoxModel.size() <= _excludeBoxModel.size()) {
                    for(int i=0; i<_includeBoxModel.size(); i++) {
                       attr.addElement(_includeBoxModel.getElementAt(i)); 
                    }
                    _wizardInfo.setAttrType(_wizardInfo.SELATTR_INCLUDE);
                } else {
                    for(int i=0; i<_excludeBoxModel.size(); i++) {
                       attr.addElement(_excludeBoxModel.getElementAt(i)); 
                    }
                    _wizardInfo.setAttrType(_wizardInfo.SELATTR_EXCLUDE);                    
                }
            }
        } else {
            _wizardInfo.setAttrType(_wizardInfo.SELATTR_ALL);    
        }
        _wizardInfo.setSelectedAttr(attr);
    }
    
    public String getErrorMessage() {
        return _error;
    }
    /*==========================================================
	 * private methods
	 *==========================================================*/

	/**
	 * Populate the UI with data
	 */
	private void populateData() {
            
        // we don't support filtered replication as yet
        if (false) 
        {
        //entries
        if ( (_wizardInfo.getFilter()== null) || (_wizardInfo.getFilter().equals("")) ) {
            _allEntries.setSelected(true);
            _filterText.setEditable(false);
            _filterText.setBackground(getBackground());
        } else {
            _filterEntries.setSelected(true);
            _filterText.setEditable(true);
            _filterText.setBackground(_activeColor);
            _filterText.setText(_wizardInfo.getFilter());
        }
        }

        //attributes
        if (_wizardInfo.getSelectedAttr() == null) {
            populateExcludeList(false);
            _enableFR.setSelected(false);
            disableAttrSelection();
        } else {
            //populate replication list
            populateExcludeList(false);
            for (Enumeration e = _wizardInfo.getSelectedAttr().elements();
                e.hasMoreElements();) {
                String attr = (String) e.nextElement();
                _excludeBoxModel.removeElement(attr);
                SchemaUtility.InsertElement(_includeBoxModel,attr);
            }
            disableAttrSelection();
        }
	}

    //populate Attribute List
    private void populateExcludeList(boolean leftList) {
            //cleanup attr list
            _excludeBoxModel.removeAllElements();
            _includeBoxModel.removeAllElements();

            //get all attributes and populate them
		    LDAPSchema sch = _wizardInfo.getLDAPSchema();
		    if (sch == null)
		        return;
		    synchronized (sch) {
                for (Enumeration e = sch.getAttributes(); e.hasMoreElements();) {
                    LDAPAttributeSchema las = (LDAPAttributeSchema)e.nextElement();
                    if (leftList)
                        SchemaUtility.InsertElement(_excludeBoxModel,las.getName());
                    else
                        SchemaUtility.InsertElement(_includeBoxModel,las.getName());
                }
            }
            return;
    }

    //disable the attribute selection area
    private void disableAttrSelection() {
        _leftPanel.setEnabled(false);
        _leftScroll.setEnabled(false);
        _rightPanel.setEnabled(false);
        _rightScroll.setEnabled(false);
        _excludeBox.setEnabled(false);
        _includeBox.setEnabled(false);
        _addAllButton.setEnabled(false);
        _addButton.setEnabled(false);
        _removeButton.setEnabled(false);
        _removeAllButton.setEnabled(false);
    }

    //enable the attribute selection area
    private void enableAttrSelection() {
        _leftPanel.setEnabled(true);
        _leftScroll.setEnabled(true);
        _rightPanel.setEnabled(true);
        _rightScroll.setEnabled(true);
        _excludeBox.setEnabled(true);
        _includeBox.setEnabled(true);
        _addAllButton.setEnabled(true);
        _addButton.setEnabled(true);
        _removeButton.setEnabled(true);
        _removeAllButton.setEnabled(true);
    }

    JList createListBox(DefaultListModel listModel, int visibleCount) {
        JList listbox = new JList(listModel);
        listbox.setCellRenderer(new AttrCellRenderer());
        listbox.setSelectionModel(new DefaultListSelectionModel());
        listbox.setPrototypeCellValue("1234567890 1234567890");
        listbox.setVisibleRowCount(visibleCount);
        return listbox;
    }

    JScrollPane createScrollPane(JList listbox) {
        JScrollPane scrollPane = new JScrollPane(listbox,
            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setAlignmentX(LEFT_ALIGNMENT);
        scrollPane.setAlignmentY(TOP_ALIGNMENT);
        scrollPane.setBorder(BorderFactory.createLoweredBevelBorder());
        return scrollPane;
    }

    /*==========================================================
     * variables
     *==========================================================*/
    private AgreementWizardInfo _wizardInfo;
    private boolean _initialized = false;
    private String _error = "WAgreementAttributePanel: error message";
	private Color _activeColor;

    private JScrollPane _rightScroll, _leftScroll;
    private DefaultListModel _excludeBoxModel = new DefaultListModel();
    private DefaultListModel _includeBoxModel = new DefaultListModel();
    private JList _excludeBox, _includeBox;
    private JPanel _entriesPanel, _attrPanel;
    private JCheckBox _enableFR;
    private JRadioButton _allEntries, _filterEntries;
    private JButton _addButton, _removeButton, _addAllButton, _removeAllButton;
    private JLabel _warn;
    private JTextField _filterText;
    private JPanel _leftPanel, _rightPanel;
    private static final Dimension BUTTON_MIN_SIZE = new Dimension(100,25);
}
