/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.Enumeration;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.admin.dirserv.wizard.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;


/**
 * Information Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/11/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class WAgreementSummaryPanel extends WAgreementPanel
                                    implements IWizardPanel {

    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    public WAgreementSummaryPanel() {
        super();
		_helpToken = "replication-wizard-summary-help";
		_section = "replication-summary";
    }

	/*==========================================================
	 * public methods
     *==========================================================*/

    public void init() {
		GridBagLayout gb = new GridBagLayout();
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
		setLayout(gb);

        //====== summary description =============
        JTextArea desc =
			UIFactory.makeMultiLineLabel( 5, 30,
						  _resource.getString(
						  "replication-agreementWizard", "summary-label") );
        gbc.anchor = gbc.NORTH;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(10,20,5,10);
        gbc.gridwidth = gbc.REMAINDER;
        gb.setConstraints(desc, gbc);
        add(desc);

        //====== setting listing =============
        _list = new JTextArea();
        _list.setEditable(false);
        _list.setCaretColor(Color.white);
        JScrollPane scrollPanel = new JScrollPane(_list);
        scrollPanel.setAlignmentX(LEFT_ALIGNMENT);
        scrollPanel.setAlignmentY(TOP_ALIGNMENT);
        scrollPanel.setBorder(ReplicationTool.LOWERED_BORDER);
        scrollPanel.setPreferredSize(new Dimension(200,1));
        ReplicationTool.resetGBC(gbc);
        gbc.fill = gbc.BOTH;
        gbc.gridwidth =  gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.weighty=1.0;
        gbc.insets = new Insets(10,20,5,10);
        gb.setConstraints(scrollPanel, gbc);
        add(scrollPanel);
        
        _initialized = true;
    }

    //====== IWizardPanel =============================
    public boolean initializePanel(WizardInfo info) {
        Debug.println("WAgreementSummaryPanel: initialize()");
        _wizardInfo = (AgreementWizardInfo) info;
        _serverInfo = _wizardInfo.getServerInfo();
        if (!_initialized)
            init();
        populateData();
        return true;
    }

    public boolean validatePanel() {
        Debug.println("WAgreementSummaryPanel: conclude()");
        return true;
    }

    public boolean concludePanel(WizardInfo info) {
        Debug.println("WAgreementSummaryPanel: conclude()");
        ReplicationAgreement rep;
        //saving information
        if (_wizardInfo.getAgreementType().equals(_wizardInfo.MMR_AGREEMENT)) {
            //MMR Agreement
            MMRAgreement agreement = new MMRAgreement(_serverInfo, null,
													  _wizardInfo.getReplicaEntry());
            rep = agreement;
            agreement.setNickname(_wizardInfo.getNickName());
	    agreement.setDescription(_wizardInfo.getDescription());
            agreement.setReplicatedSubtree(_wizardInfo.getSubtree());
            agreement.setConsumerHost(_wizardInfo.getToServer().getHost());
            agreement.setConsumerPort(_wizardInfo.getToServer().getPort());

            agreement.setUseSSL(_wizardInfo.getSSL());
            agreement.setUseStartTLS(_wizardInfo.getStartTLS());

            agreement.setUseSSLAuth(_wizardInfo.getSSLAuth());
            agreement.setUseGSSAPIAuth(_wizardInfo.getGSSAPIAuth());
            agreement.setUseDigestAuth(_wizardInfo.getDigestAuth());
            
            if (!_wizardInfo.getBindDN().equals("")) {
                agreement.setBindDN(_wizardInfo.getBindDN());
                agreement.setBindCredentials(_wizardInfo.getBindPWD());
            }
            agreement.setUpdateSchedule(_wizardInfo.getDate());
            if ( AgreementWizardInfo.doSelectiveReplication ) {
                agreement.setSelattrType(_wizardInfo.getAttrType());
                agreement.setSelectedAttributes(_wizardInfo.getSelectedAttr());
            }
            int result = agreement.writeAgreementToServer();
            if (result != 0) {
                ReplicationTool.displayError(result);
                return false;
            }
        }  else if (_wizardInfo.getAgreementType().equals(_wizardInfo.AD_AGREEMENT)) {
            ActiveDirectoryAgreement agreement = new ActiveDirectoryAgreement(_serverInfo, null, _wizardInfo.getWindowsReplicaEntry());
            rep = agreement;
            agreement.setNickname(_wizardInfo.getNickName());
            agreement.setDescription(_wizardInfo.getDescription());
            agreement.setReplicatedSubtree(_wizardInfo.getSubtree());
            agreement.setWindowsDomain(_wizardInfo.getWindowsDomain());
            agreement.setConsumerHost(_wizardInfo.getToServer().getHost());
            agreement.setConsumerPort(_wizardInfo.getToServer().getPort());
            agreement.setUpdateSchedule(_wizardInfo.getDate());

            agreement.setUseSSL(_wizardInfo.getSSL());
            agreement.setUseStartTLS(_wizardInfo.getStartTLS());

            agreement.setUseSSLAuth(_wizardInfo.getSSLAuth());
            agreement.setUseGSSAPIAuth(_wizardInfo.getGSSAPIAuth());
            agreement.setUseDigestAuth(_wizardInfo.getDigestAuth());
            
            if (!_wizardInfo.getBindDN().equals("")) {
                agreement.setBindDN(_wizardInfo.getBindDN());
                agreement.setBindCredentials(_wizardInfo.getBindPWD());
            }
            agreement.setWinRepArea(_wizardInfo.getWindowsRepArea() );
            agreement.setDSRepArea(_wizardInfo.getDSRepArea());
            agreement.setNewWinUserSync(_wizardInfo.getNewWinUserSync());
            agreement.setNewWinGroupSync(_wizardInfo.getNewWinGroupSync());
            
            int result = agreement.writeAgreementToServer();
            if (result != 0) {
                ReplicationTool.displayError(result);
                return false;
            }
            
            
        }  else { //XXX don't forget NT
            //SIR Agreement
            SIRAgreement agreement = new SIRAgreement(_serverInfo, null,
													  _wizardInfo.getReplicaEntry());
            rep = agreement;
            agreement.setNickname(_wizardInfo.getNickName());
            agreement.setReplicatedSubtree(_wizardInfo.getSubtree());
            agreement.setConsumerHost(_wizardInfo.getToServer().getHost());
            agreement.setConsumerPort(_wizardInfo.getToServer().getPort());

            agreement.setUseSSL(_wizardInfo.getSSL());
            agreement.setUseStartTLS(_wizardInfo.getStartTLS());

            agreement.setUseSSLAuth(_wizardInfo.getSSLAuth());
            agreement.setUseGSSAPIAuth(_wizardInfo.getGSSAPIAuth());
            agreement.setUseDigestAuth(_wizardInfo.getDigestAuth());
            
            if (!_wizardInfo.getBindDN().equals("")) {
                agreement.setBindDN(_wizardInfo.getBindDN());
                agreement.setBindCredentials(_wizardInfo.getBindPWD());
            }
            agreement.setUpdateSchedule(_wizardInfo.getDate());
			if ( AgreementWizardInfo.doFilteredReplication ) {
				agreement.setEntryFilter(_wizardInfo.getFilter());
			}
			if ( AgreementWizardInfo.doSelectiveReplication ) {
				agreement.setSelattrType(_wizardInfo.getAttrType());
				agreement.setSelectedAttributes(_wizardInfo.getSelectedAttr());
			}
            int result = agreement.writeAgreementToServer();
            if (result != 0){
                ReplicationTool.displayError(result);
                return false;
            }
			
        }

		saveReplica(rep);		
        
        //refresh tree if ReplicationResourceObject exists
        if( _wizardInfo.getParentNode()!= null ) {			
			IDSContentListener o =
				(IDSContentListener)_wizardInfo.getParentNode();
			o.contentChanged();	   
        }
        
        return true;
    }

    public void getUpdateInfo(WizardInfo info) {
        Debug.println("WAgreementSummaryPanel: getUpdateInfo()");
    }

    public String getErrorMessage() {
        return _error;
    }

    /*==========================================================
	 * private methods
	 *==========================================================*/

   /**
	* Saves a replica agreement defined by the user.  It performs other operations (initialize consumer or
	* create LDIF file to initialize consumer) if it is indicated in the replication agreement.		 
	*/
    private void saveReplica(ReplicationAgreement agreement) {
		String ldifFilename = _wizardInfo.getLDIFFilename();
		
		if ( ldifFilename != null ) {
			if (!agreement.populateLDIFFile (ldifFilename)) {				
				String error = _resource.getString("replication-initialize", "error-1");
				ReplicationTool.displayError(error);
			}			
			/* Initialize consumer was requested - do ORC */
		} else if (_wizardInfo.getInitialize()) {
			agreement.initializeConsumer ();			
		}		
	}

	/**
	 * Populate the UI with data
	 */
	private void populateData() {
	    _list.setText(_wizardInfo.prettyPrint());
    }
	
    /*==========================================================
     * variables
     *==========================================================*/
    private Color _activeColor;
    private AgreementWizardInfo _wizardInfo;
    private ConsoleInfo _serverInfo;
    private boolean _initialized = false;
    private String _error = "WAgreementSummaryPanel: error message";

    private JTextArea _list;
}
