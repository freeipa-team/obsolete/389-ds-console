/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.JButtonFactory;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import netscape.ldap.util.RDN;
import com.netscape.admin.dirserv.wizard.*;

/**
 * Destination Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class WAgreementDestinationPanel extends WAgreementPanel
                                        implements IWizardPanel,
 	                                               ActionListener,
	                                               DocumentListener {

    /*==========================================================
     * constructors
     *==========================================================*/

    /**
     * public constructor
     * construction is delayed until selected.
     * @param owner resource object owner
     */
    public WAgreementDestinationPanel(AgreementWizard wizard) {
        super();
        _dsInstances = new Hashtable();
        _icon = ReplicationTool.getImage("DirectoryServer.gif");
		_helpToken = "replication-wizard-content-help";
		_section = "replication-destination-dialog";
		_wizard = wizard;
    }

	/*==========================================================
	 * public methods
     *==========================================================*/

    /**
	 * Actual panel construction
	 */
    public void init() {
		int space = UIFactory.getComponentSpace();
		int different = UIFactory.getDifferentSpace();
		int large = 40;
		setLayout(new GridBagLayout());
	    GridBagConstraints gbc = new GridBagConstraints();
		ReplicationTool.resetGBC(gbc);
        setPreferredSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        setMaximumSize(ReplicationTool.DEFAULT_PANEL_SIZE);
        
        JLabel ask =
			new JLabel(_resource.getString("replication-destination-ask",
										   "label"));
		ask.setLabelFor(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.NONE;
        gbc.insets = UIFactory.getBorderInsets();
        gbc.insets.left = space;
        add(ask, gbc);

		//======== Replicate From Panel =========================
		String title = _resource.getString("replication-info-summary-from","title");
		_fromPanel = new GroupPanel(title);		
		ReplicationTool.resetGBC(gbc);
		gbc.gridwidth = gbc.REMAINDER;
		gbc.weightx = 1.0;
		add(_fromPanel, gbc);
 
		//from combo box
		_fromModel = new CustomComboBoxModel();
		_fromBox = new CustomComboBox(_fromModel);
		_fromBox.getAccessibleContext().setAccessibleDescription(_resource.getString("replication-info-summary-from",
																					 "title"));
		_fromBox.setMaximumRowCount(4); 
         
		//from text box
		_fromText = new JLabel("",_icon, JLabel.LEFT);

        //======== Replicate To Panel ===========================
		title = _resource.getString("replication-info-summary-to","title");
        _toPanel = new GroupPanel(title);		        
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        add(_toPanel, gbc);

        //to combo box
        _toModel = new CustomComboBoxModel();
        _toBox = new CustomComboBox(_toModel);
		_toBox.setToolTipText(_resource.getString("replication-info-summary-to",
												  "combobox-ttip"));
        _toBox.setMaximumRowCount(4);

        //to button
        _toButton = UIFactory.makeJButton(this, "replication-destination", "otherButton", _resource);			
		JButtonFactory.resize( _toButton );        
        
        //to text box
        _toText = new JLabel("",_icon, JLabel.LEFT);

        //==== Authentication =======
		title = _resource.getString("replication-destination-connection",
								"label");
        JPanel authPanel = new GroupPanel(title);        
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        add(authPanel, gbc);

        //connection radio buttons
        ButtonGroup connGroup = new ButtonGroup();
        //plain old ldap button
        _noEncrypt = new JRadioButton(_resource.getString(
			"replication-destination-noEncrypt","label"));
        _noEncrypt.setToolTipText(_resource.getString(
			"replication-destination-noEncrypt","ttip"));
        _noEncrypt.setSelected(true); // default is on
        _noEncrypt.addActionListener(this);
        connGroup.add(_noEncrypt);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0, space, 0, different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_noEncrypt, gbc);

        //ssl button
        _sslEncrypt = new JRadioButton(_resource.getString(
			"replication-destination-sslEncrypt","label"));
        _sslEncrypt.setToolTipText(_resource.getString(
    			"replication-destination-sslEncrypt","ttip"));
        _sslEncrypt.addActionListener(this);
        connGroup.add(_sslEncrypt);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0, space, 0, different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_sslEncrypt, gbc);

        //tls button
        _tlsEncrypt = new JRadioButton(_resource.getString(
			"replication-destination-startTLS","label"));
        _tlsEncrypt.setToolTipText(_resource.getString(
    			"replication-destination-startTLS","ttip"));
        _tlsEncrypt.addActionListener(this);
        connGroup.add(_tlsEncrypt);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0, space, space, different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_tlsEncrypt, gbc);

        //auth using label
        JLabel auth = new JLabel(_resource.getString(
			"replication-destination-authUsing","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,space,0,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(auth, gbc);

        //ssl auth radio button
        ButtonGroup authGroup = new ButtonGroup();
        _sslAuth = new JRadioButton(_resource.getString(
			"replication-destination-sslClientAuth","label"));
        _sslAuth.setToolTipText(_resource.getString(
    			"replication-destination-sslClientAuth","ttip"));
        _sslAuth.addActionListener(this);
        authGroup.add(_sslAuth);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,space,0,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_sslAuth, gbc);

        //gssapi auth radio button
        _gssapiAuth = new JRadioButton(_resource.getString(
			"replication-destination-gssapiAuth","label"));
        _gssapiAuth.setToolTipText(_resource.getString(
    			"replication-destination-gssapiAuth","ttip"));
        _gssapiAuth.addActionListener(this);
        authGroup.add(_gssapiAuth);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,space,0,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_gssapiAuth, gbc);

        //digest auth radio button
        _digestAuth = new JRadioButton(_resource.getString(
			"replication-destination-digestAuth","label"));
        _digestAuth.setToolTipText(_resource.getString(
    			"replication-destination-digestAuth","ttip"));
        _digestAuth.addActionListener(this);
        authGroup.add(_digestAuth);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,space,0,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_digestAuth, gbc);

        //simple auth radio button
        _simpAuth = new JRadioButton(_resource.getString(
			"replication-destination-simpleAuth","label"));
        _simpAuth.setToolTipText(_resource.getString(
    			"replication-destination-simpleAuth","ttip"));
        _simpAuth.addActionListener(this);
        authGroup.add(_simpAuth);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,space,0,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = gbc.HORIZONTAL;
        gbc.weightx = 1.0;
        authPanel.add(_simpAuth, gbc);

        //simp panel
        JPanel simpPanel = new JPanel();
        simpPanel.setLayout(new GridBagLayout());
        simpPanel.setBackground(getBackground());
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx=1.0;
        gbc.insets = new Insets(0,0,0,0);
        authPanel.add(simpPanel, gbc);

        //bind as
        _bind = new JLabel(_resource.getString(
			"replication-destination-bindAs","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.insets = new Insets(0,space+10,space,different);
        gbc.fill = gbc.NONE;
        simpPanel.add(_bind, gbc);

        _bindText = UIFactory.makeJTextField(null, "");
		_bind.setLabelFor(_bindText);
		_bindText.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
        gbc.weightx=1.0;
        simpPanel.add(_bindText, gbc);

        //password
        _pwd = new JLabel(_resource.getString(
			"replication-destination-bindPwd","label"));
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.EAST;
        gbc.insets = new Insets(0,space+10,space,different);
        gbc.fill = gbc.NONE;
        simpPanel.add(_pwd, gbc);

        _pwdText= UIFactory.makeJPasswordField(null,"");
		_pwd.setLabelFor(_pwdText);
		_pwdText.getDocument().addDocumentListener(this);
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.insets = new Insets(0,0,space,different);
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
		gbc.fill = gbc.HORIZONTAL;
        simpPanel.add(_pwdText, gbc);

        //======== Context Panel ================================
		title = _resource.getString(
									"replication-content-replicate","label");
        JPanel contextPanel = new GroupPanel(title);        
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.NORTH;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.gridheight = gbc.REMAINDER;
        gbc.weighty = 1.0;
        gbc.weightx = 1.0;
        add(contextPanel, gbc);

        //subtree text
        ReplicationTool.resetGBC(gbc);
        gbc.anchor = gbc.WEST;
        gbc.gridwidth = 1;
		gbc.fill = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(0,space,different,different);
		Debug.println(8, "WAgreementDestinationPanel.init: replica entry = " +
					  _wizardInfo.getReplicaEntry());
		if (_wizardInfo.getReplicaEntry() == null) {
			_replicaPanel = new ReplicaPanel(_serverInfo.getLDAPConnection(),
											 _wizardInfo.getReplicaEntry());
			contextPanel.add(_replicaPanel, gbc);
		} else {
			String suffix = _wizardInfo.getSubtree();
			contextPanel.add(new JLabel(suffix), gbc);
		}

        populateData();
        if(!_isCopy) {
            setSimpAuth();
        }
		checkNextButton();
        invalidate();
        validate();
        repaint(1);
        _initialized = true;
    }

    //========= ACTIONLISTENER =================
    public void actionPerformed(ActionEvent e) {

        Debug.println("WAgreementDestinationPanel: actionPerformed()"+
					  e.toString());

        if (e.getSource().equals(_toButton)) {
			String title = _resource.getString(_section, "hostInfo-consumer-title");
			
			JFrame frame = null;
			for (Container p = getParent(); p != null; p = p.getParent()) {
				if (p instanceof JFrame) {					
					frame = (JFrame)p;
					break;
				}
			}
            HostInfoDialog dialog = new HostInfoDialog(frame, title);
			dialog.pack();
            dialog.show();			

            if (! dialog.isOk()) {
                dialog = null;				
                return;
            }
            //check if it is the same as the to server
            ServerInstance newDS =
				new ServerInstance(dialog.getHost(), dialog.getPort());
			dialog = null;		
            if (_server.equals(newDS)) {   
                AgreementDestinationPanel.showErrorDialog(
					frame,
					"sameServer",
					_section );				
                return;
            }
            
            //add to to-list
            _toModel.removeEntry(newDS.toString());
            _toModel.addItem(newDS);
			_toModel.removeEntry(CONSUMER_LIST_NOT_AVAILABLE);
            _toBox.setSelectedIndex(_toModel.getSize()-1);			
            //no need to get new context
        }
        if (e.getSource().equals(_sslAuth) && _sslAuth.isSelected()) {
        	//disable
        	enableSimpleAuth (false);
        }
        if (e.getSource().equals(_gssapiAuth) && _gssapiAuth.isSelected()) {
        	// enable
        	enableSimpleAuth (true);
        	// requires ldap
        	_noEncrypt.setSelected(true);
        	/* set to use non-SSL port LDAP */
        	_portAttr = ATTR_PORT;
        }
        if (e.getSource().equals(_simpAuth) && _simpAuth.isSelected()) {
        	//enable
        	enableSimpleAuth (true);
        }
        if (e.getSource().equals(_digestAuth) && _digestAuth.isSelected()) {
        	//enable
        	enableSimpleAuth (true);
        }

        if (e.getSource().equals(_noEncrypt) && _noEncrypt.isSelected()) {
        	/* set to use non-SSL port LDAP */
        	_portAttr = ATTR_PORT;
        	//disable
        	_sslAuth.setEnabled(false);
        	//enable
        	if (_sslAuth.isSelected()) {
        		// have to select something else
            	_simpAuth.setSelected(true);
        	}
        	enableSimpleAuth(true);
        	_gssapiAuth.setEnabled(true);
        	_digestAuth.setEnabled(true);

        	/* set to use non-SSL port */
        	_portAttr = ATTR_PORT;

            /* set appropriate to and from servers */
            populateInstanceModel(_consoleInfo);

			_toText.setText(_server.getKey());
        }
        boolean ssl_selected = false;
        if (e.getSource().equals(_sslEncrypt) && _sslEncrypt.isSelected()) {
            /* set to use SSL port */
            _portAttr = ATTR_SECURE_PORT;
            ssl_selected = true;
        }
        if (e.getSource().equals(_tlsEncrypt) && _tlsEncrypt.isSelected()) {
        	/* set to use non-SSL port for startTLS */
        	_portAttr = ATTR_PORT;
            ssl_selected = true;
        }
        if (ssl_selected) {
            _sslAuth.setEnabled(true);
            _gssapiAuth.setEnabled(false);
            if (_gssapiAuth.isSelected()) {
            	// have to select something else
            	_simpAuth.setSelected(true);
            	enableSimpleAuth(true);
            }
        }
		checkNextButton();
    }
    
    //====== IWizardPanel =============================
    public boolean initializePanel(WizardInfo info) {
        boolean prevIsLEGACYR = _isLEGACYR;
        Debug.println("WAgreementDestinationPanel: Initialize() info = " + info);
        _wizardInfo = (AgreementWizardInfo)info;
        _serverInfo = _wizardInfo.getServerInfo();
        _consoleInfo = _wizardInfo.getConsoleInfo();

        if (_wizardInfo.getAgreementType().equals(_wizardInfo.LEGACYR_AGREEMENT))
            _isLEGACYR = true;
        else
            _isLEGACYR = false;
        Debug.println(8, "WAgreementDestinationPanel: is legacy = " + _isLEGACYR);
        if (_wizardInfo.getWizardType().equals(_wizardInfo.COPY_WIZARD)) {    
            _isCopy = true;
            _copy = _wizardInfo.getCopyAgreement();
        } else {
            _isCopy = false;
        }
        if (!_initialized) {
            init();
            return true;
        }

        /* agreement type changed - need to repopulate data */
        if (!_isCopy && prevIsLEGACYR != _isLEGACYR) {
            populateData();
        }
		checkNextButton();
        invalidate();
        validate();
        repaint(1);
        return true;
    }

    public boolean validatePanel() {
        Debug.println("WAgreementDestinationPanel: validatePanel()");
        
        //binddn and pwd field
        if (_simpAuth.isSelected()) {
            String dn = _bindText.getText();
            if ( dn.equals("") || !DSUtil.isValidDN(dn) ) {
                _error = _resource.getString(_section,
											 "bindDNEmpty-msg");
                return false;
            } else if (_pwdText.getText().equals("")) {
                _error = _resource.getString(_section,
											 "bindPasswordEmpty-msg");
                return false;
            }
        }
        // else if digest or gssapi auth is selected, there really isn't much
        // we can do to validate the fields - password is not required, and
        // the format can be different

        //check box
		//check consumer
		Hashtable serverItem = (Hashtable)_toBox.getSelectedItem();
		/* IF nothing selected or if the message "No consumer list available" selected 
		   display an error */
		if ((serverItem == null) || 
			serverItem.get(CustomComboBoxModel.SELECTION_TITLE).equals(CONSUMER_LIST_NOT_AVAILABLE)) {			
			_error = _resource.getString(_section,
										 "consumerEmpty-msg");
			return false;
		}
        
        return true;
    }

    public boolean concludePanel(WizardInfo info) {
        Debug.println("WAgreementDestinationPanel: conclude()");
        
        //check copied from
        int result = checkCopiedFrom();
        
        if (result == SKIP)
            return false;
            
        if (result == PROMPT) {
			String msg = _resource.getString(_section,"mismatchCopiedFrom");
            int option = DSUtil.showConfirmationDialog(this, "warning",
													   msg,
													   "replication-dialog");
            if (option == JOptionPane.NO_OPTION)
                return false;
        }
        
        return true;
    }

    public void getUpdateInfo(WizardInfo info) {
    	Debug.println("WAgreementDestinationPanel: getUpdateInfo()");
    	Hashtable serverItem = (Hashtable) _toBox.getSelectedItem();
    	ServerInstance toServer =
    		(ServerInstance) serverItem.get(
    				CustomComboBoxModel.SELECTION_DATA);
    	_wizardInfo.setFromServer(_server);
    	_wizardInfo.setToServer(toServer);

    	boolean ssl = false;
    	if (_tlsEncrypt.isSelected()) {
    		_wizardInfo.setSSL(false);
    		_wizardInfo.setStartTLS(true);
    		ssl = true;
    	} else if (_sslEncrypt.isSelected()) {
    		_wizardInfo.setSSL(true);
    		_wizardInfo.setStartTLS(false);
    		ssl = true;
    	} else {
    		_wizardInfo.setSSL(false);
    		_wizardInfo.setStartTLS(false);
    	}

    	if (ssl) {
    		boolean need_dn_pw = false;
    		if (_sslAuth.isSelected()) {
    			_wizardInfo.setSSLAuth(true);
    			_wizardInfo.setDigestAuth(false);
    		} else {
    			_wizardInfo.setSSLAuth(false);
    			_wizardInfo.setDigestAuth(_digestAuth.isSelected());
    			need_dn_pw = true;
    		}
    		if (need_dn_pw) {
    			_wizardInfo.setBindDN(_bindText.getText());
    			_wizardInfo.setBindPWD(_pwdText.getText());			
    		} else {
    			_wizardInfo.setBindDN("");
    			_wizardInfo.setBindPWD("");			
    		}
    		_wizardInfo.setGSSAPIAuth(false);
    	} else {
    		if (_gssapiAuth.isSelected()) {
    			_wizardInfo.setGSSAPIAuth(true);
    			_wizardInfo.setDigestAuth(false);
    		} else {
    			_wizardInfo.setGSSAPIAuth(false);
    			_wizardInfo.setDigestAuth(_digestAuth.isSelected());
    		}
    		_wizardInfo.setBindDN(_bindText.getText());
    		_wizardInfo.setBindPWD(_pwdText.getText());			
    		_wizardInfo.setSSLAuth(false);
    	}
    	if (_replicaPanel != null) {
    		_wizardInfo.setReplicaEntry(_replicaPanel.getReplicaEntry());
		    _wizardInfo.setSubtree(_replicaPanel.getSuffix());
    	}
    }

    public String getErrorMessage() {
        return _error;
    }

    /*==========================================================
	 * private methods
	 *==========================================================*/

	/**
	 * Populate the UI with data
	 */
	private void populateData() {
	    GridBagConstraints gbc = new GridBagConstraints();
		int space = UIFactory.getComponentSpace();
		int different = UIFactory.getDifferentSpace();

		_fromPanel.removeAll();
		ReplicationTool.resetGBC(gbc);
		gbc.weightx=1.0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = new Insets(0,space,space,different);
		_fromPanel.add(_fromText, gbc);
		_fromText.setLabelFor(_fromPanel);
		_toPanel.removeAll();
		ReplicationTool.resetGBC(gbc);
		gbc.insets = new Insets(0,space,space,different);
		gbc.weightx = 1.0;
		_toPanel.add(_toBox, gbc);
		ReplicationTool.resetGBC(gbc);
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = new Insets(0,0,space,different);
		_toPanel.add(_toButton, gbc);
		_toModel.removeAll();
		//to panel
		if (_isCopy) {						
			copyData();
		}

		_server = new ServerInstance( _serverInfo.getHost(),
									  _serverInfo.getPort());

		//form panel
		_fromText.setText(_server.getKey());

		loadDSInstances(_consoleInfo);
             
		//add remaining availabe server
		if ((_dsInstances.size() == 0) && !_isCopy) {
			_toModel.addItem(CONSUMER_LIST_NOT_AVAILABLE);
			_toBox.setSelectedIndex(0);
		} else {
			for (Enumeration e = _dsInstances.keys() ; e.hasMoreElements() ;) {
				String key = (String)e.nextElement();
				ServerInstance otherServer =
					(ServerInstance)_dsInstances.get(key);
				_toModel.addItem(otherServer);    
			}
			
			if (_toModel.getSize() >0){
				if (_wizardInfo.getToServer () != null &&
					_toModel.isPresent (_wizardInfo.getToServer().getKey ()))
					_toBox.setSelectedItem (_wizardInfo.getToServer());
				else
					_toBox.setSelectedIndex(0);
			}
		}

		if (_isCopy) {
			ServerInstance toServer = _wizardInfo.getToServer();
			/* To avoid having duplicated hosts in the combo box */
			if (!_toModel.isPresent(toServer.getKey ())) {
				_toModel.addItem(toServer);
				_toBox.setSelectedItem (toServer);
			}
		}
	}

    /**
     * Copy Rest of data from agreement
     */
    private void copyData() {
        _pwdText.setText(_wizardInfo.getBindPWD());
        _bindText.setText(_wizardInfo.getBindDN());
        boolean ssl = false;
        if (_wizardInfo.getStartTLS()) {
        	_tlsEncrypt.setSelected(true);
            ssl = true;
        } else if (_wizardInfo.getSSL()) {
        	_sslEncrypt.setSelected(true);
        	ssl = true;
        } else {
        	_noEncrypt.setSelected(true);
        }
        if (ssl) {
        	if (_wizardInfo.getSSLAuth()) {
                //SSL client Auth
                enableSimpleAuth (false);
                _sslAuth.setEnabled(true);
                _sslAuth.setSelected(true);    
            } else {
                //id/pwd auth with ssl
                _sslAuth.setEnabled(false);
                _simpAuth.setSelected(!_wizardInfo.getDigestAuth());
                _digestAuth.setSelected(_wizardInfo.getDigestAuth());
                enableSimpleAuth (true);
            }
        	_gssapiAuth.setEnabled(false);
        } else {
        	if (_wizardInfo.getGSSAPIAuth()) {
        		_gssapiAuth.setEnabled(true);
        		_gssapiAuth.setSelected(true);
                enableSimpleAuth (false);
            } else {
                _simpAuth.setSelected(!_wizardInfo.getDigestAuth());
                _digestAuth.setSelected(_wizardInfo.getDigestAuth());
                enableSimpleAuth (true);
            }
        	_sslAuth.setEnabled(false);
        }
    }
    
    /**
     * Set simple auth selection
     */
    private void setSimpAuth() {
        //Simple Auth no SSL
        _sslEncrypt.setSelected(false);
        _tlsEncrypt.setSelected(false);
        _sslAuth.setEnabled(false);
        _simpAuth.setSelected(true);

        enableSimpleAuth (true);     
    }

    private void enableSimpleAuth (boolean enable){
        _bind.setEnabled(enable);
        _bindText.setEnabled(enable);
        _pwd.setEnabled(enable);
        _pwdText.setEnabled(enable);
		_pwdText.setBackground(_bindText.getBackground());
    }

    /**
     * populates instance combo box. 
     */

    private void populateInstanceModel (ConsoleInfo ci){
        CustomComboBoxModel activeModel;
        JComboBox activeBox;
        String  key = null;
        boolean reselect; 
        int     i, index, count;

        /* populate model */
		activeModel = _toModel;
		activeBox   = _toBox;

        /* this function is called when connection type has
           changed from SSL to non-SSL or the other way around. 
           In that, case we should remove all entries currently  
           in _dsInstance because the port number has changed.   */
        index = activeBox.getSelectedIndex ();
        if (index >= 0){ 
            Hashtable serverItem = (Hashtable) activeBox.getSelectedItem();
            ServerInstance activeServer = (ServerInstance) serverItem.get(
					                       CustomComboBoxModel.SELECTION_DATA);
			if (activeServer != null) {
				key = (String)activeServer.getKey ();
			}
        }        

        if (key != null && _dsInstances.containsKey (key))
            reselect = true;
        else
            reselect = false;
        
        Enumeration keys = _dsInstances.keys();
        while (keys.hasMoreElements ()){
            key = (String)keys.nextElement ();
            activeModel.removeEntry (key);
        }
        
        /* get the list of available server insatnces */
        loadDSInstances (ci);   
            
        //add remaining availabe server
        for (Enumeration e = _dsInstances.keys() ; e.hasMoreElements() ;) {
            key = (String)e.nextElement();
            ServerInstance otherServer =
				(ServerInstance)_dsInstances.get(key);
            activeModel.addItem(otherServer);
        }
//         if (reselect && activeModel.getSize() > 0)
//             activeBox.setSelectedIndex(0);       
    }

    /**
     * Retrieve all instances of directory server configuration managed by the
     * current KinPin admin console
     */
    private void loadDSInstances(ConsoleInfo ci) { 

        //get connection from console
        //if null, problem with server forget it...
        LDAPConnection ld = ci.getLDAPConnection();
        if (ld == null) {
            Debug.println("ERROR: AgreementDestinationPanel: " +
						  "loadDSInstances()- Connection Failed");
            return;
        } 

        // clear the list
        _dsInstances.clear ();

        //search
        try {
			String filter = "objectclass=" + DSClass;
            String root =
				com.netscape.management.client.util.LDAPUtil.getConfigurationRoot();
            String[] attrs = { "serverHostName", "" };
			String serverKey = ServerInstance.getKey (_server.getHost(),
													  getPort());

            attrs [1] = _portAttr;

			LDAPSearchResults reslist =
				ld.search(root, ld.SCOPE_SUB, filter,
						  attrs, false);

			if (!reslist.hasMoreElements()) {
				Debug.println("ERROR: WAgreementDestinationPanel." +
							  "loadDSInstances: could not get list of " +
							  "servers");
			}
			
            while (reslist.hasMoreElements()) {
                LDAPEntry entry = reslist.next();

				/* discard entries match this server or already in
				   the table. This will only work if the host name
				   received is fully qualified name. Otherwise,
				   this optimization will not work and we will
				   have to go through full resolution */
								
  			    String thisKey = getKey (entry);
				if (thisKey.equals (serverKey)) {
					Debug.println("ERROR: WAgreementDestinationPanel." +
								  "loadDSInstances: server " + thisKey +
								  " equals server " + serverKey);
					continue;
				}

				if (_dsInstances.containsKey (thisKey)) {
					Debug.println("ERROR: WAgreementDestinationPanel." +
								  "loadDSInstances: server " + thisKey +
								  " is already in the list");
					continue;
				}

				ServerInstance s = entry2DSInstance( entry );
				if (s != null) {
					_dsInstances.put (s.getKey(),s);
					Debug.println(9, "WAgreementDestinationPanel." +
								  "loadDSInstances: put server " +
								  s.getKey() + " in the list");
				} else {
					Debug.println("ERROR: WAgreementDestinationPanel." +
								  "loadDSInstances: could not convert " +
								  "entry " + entry + " to a server");
				}
            }

			if (_dsInstances.containsKey (serverKey)) {
				Debug.println(9, "WAgreementDestinationPanel." +
							  "loadDSInstances: removing " + serverKey +
							  " from the list");
				_dsInstances.remove (serverKey);
			}

        } catch (Exception e) {
            Debug.println("ERROR: AgreementDestinationPanel: loadDSSIE()"+
						  e.toString());
        }

		Debug.println(9, "WAgreementDestinationPanel." +
					  "loadDSInstances: there are " +
					  ((_dsInstances == null) ? 0 : _dsInstances.size()) +
					  " entries in the list");

    }

	/**
	 * Checks if the entry corresponds to this server
	 * @param entry LDAP Entry
	 * @return whether it corresponds to this server or not
	 */
	private String getKey (LDAPEntry entry)
	{
		LDAPAttribute attrHost = entry.getAttribute( "serverHostName" );
		LDAPAttribute attrPort = entry.getAttribute(_portAttr); 
		String        host;
		int           port;

		if (attrHost != null)
			host = (String)attrHost.getStringValues().nextElement();
		else
			host="";

		if (attrPort != null)
			port = Integer.parseInt(
				   (String)attrPort.getStringValues().nextElement());
		else
			port = 0;

		return ServerInstance.getKey (host, port);
	}

    /**
     * Convert LDAP entry to ServerInstance object
     * @param entry LDAP Entry
     * @return ServerInstance
     */
    private ServerInstance entry2DSInstance(LDAPEntry entry) {
        ServerInstance s = new ServerInstance();
        LDAPAttributeSet attrs = entry.getAttributeSet();
		LDAPAttribute attr = entry.getAttribute( "serverHostName" );
		if ( attr != null )
			s.setHost((String)attr.getStringValues().nextElement());
		attr = entry.getAttribute(_portAttr);
		if ( attr != null )
			s.setPort(Integer.parseInt(
				(String)attr.getStringValues().nextElement()));
        return s;
    }

    /**
     * this function returns normal or SSL port for this server
     * depending on whether SSL was requested for this 
     * agreement.
     */

    private int getPort (){
        if (_portAttr.equals (ATTR_PORT))
            return _serverInfo.getPort ();
        else /* SSL port */
            return DSUtil.getSSLPort (_serverInfo);
    }
   
    /**
     * check copiedFrom attribute from the subtree to see
     * if there are other replciation agreeement already
     * exist. If so, warm the user.
     * PRE_CONDITION: FROM/TO SELECTED
     */
    private int checkCopiedFrom() {
		String subtree = null;
		if (_replicaPanel != null) {
			subtree = _replicaPanel.getSuffix();
		} else {
			subtree = _wizardInfo.getSubtree();
		}
        String supplierName;
        int supplierPort;
        //get supplier
        if (_isLEGACYR) {
            Hashtable serverItem = (Hashtable) _fromBox.getSelectedItem();
            ServerInstance supplier =
				(ServerInstance) serverItem.get(
					CustomComboBoxModel.SELECTION_DATA);
            supplierName = supplier.getHost();
            supplierPort = supplier.getPort();
        } else {
            supplierName = _serverInfo.getHost();
            supplierPort = _serverInfo.getPort();
        }
        
        //get consumer LDAPConnection
		Hashtable serverItem = (Hashtable) _toBox.getSelectedItem();
		ServerInstance consumer =
			(ServerInstance) serverItem.get(
				CustomComboBoxModel.SELECTION_DATA);
		LDAPConnection ld = LDAPUtil.connectToServer(
			consumer.getHost(), consumer.getPort(), 
			_bindText.getText(), _pwdText.getText(),
			_sslEncrypt.isSelected());
		if (ld == null) {
			String msg = _resource.getString(_section,
											 "clientUnavail");
			int option = DSUtil.showConfirmationDialog(this,
													   "warning",
													   msg,
													   "replication-dialog");
			if (option == JOptionPane.YES_OPTION)
				return CONTINUE;
			else
				return SKIP;
		}

        //get copied from string
        String x = ReplicationAgreement.getCopiedFrom(ld, subtree);
        if (x != null) {
            try {
                int cloc = x.indexOf(':');
                if (cloc != -1) {
                    String cfHost = x.substring(0, cloc);
                    int sploc = x.indexOf(' ');
                    if (sploc != -1) {
                        int cfPort =
							Integer.parseInt(x.substring(cloc + 1, sploc));
                        if (!cfHost.equals(supplierName) ||
							cfPort != supplierPort) {
                            Debug.println("copiedFrom attribute mismatch: " +
										  "copiedFrom " + cfHost + ":" +
										  cfPort + " != supplier " +
										  supplierName + ":" + supplierPort);
                            return PROMPT;
                        }
                    }
                }
            } catch (Exception e) {
                // ignore all exceptions, and assume we couldn't read
				// the copiedFrom
                Debug.println(e.toString());
            }            
            
        } else {
            Debug.println("WADP: getCopiedFrom() - return null");    
        }
        return CONTINUE;
    }


	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void changedUpdate(DocumentEvent e) {		
		checkNextButton();
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void removeUpdate(DocumentEvent e) {
		changedUpdate(e);
    }

	/**
	 * Some text component changed
	 *
	 * @param e Event indicating what changed
	 */
    public void insertUpdate(DocumentEvent e) {
		changedUpdate(e);
    }

	/**
	 * Method used to update the next button of the wizard.
	 * We check that (if necessary) the user has provided a correct dn to bind and
	 * a password */
	protected void checkNextButton() {
		boolean state = true;
		Hashtable serverItem = (Hashtable)_toBox.getSelectedItem();		
		/* IF nothing selected or if the message "No consumer list available" selected 
		   disable 'Next' button */
		if ((serverItem == null) || 
			serverItem.get(CustomComboBoxModel.SELECTION_TITLE).equals(CONSUMER_LIST_NOT_AVAILABLE)) {
			state = false;
		} else if (_simpAuth.isSelected() || _digestAuth.isSelected()) {
			if (_bindText.getText().trim().equals("") ||
				!DSUtil.isValidDN(_bindText.getText()) ||
				_pwdText.getText().trim().equals("")) {
				state = false;
			}
		}
		_wizard.getbNext_Done().setEnabled(state);
	}
													   

    /*==========================================================
     * variables
     *==========================================================*/
    private Hashtable _dsInstances;
    private ConsoleInfo _serverInfo;
    private ConsoleInfo _consoleInfo;
    private boolean _isLEGACYR = false;
    private boolean _isCopy = false;
    private boolean _initialized = false;
    private ServerInstance _server;
    private ImageIcon _icon;
    
    private JLabel _fromText, _toText;
    private JTextField _bindText;
    private JButton _toButton;
    private JComboBox _fromBox, _toBox;
    private JRadioButton _noEncrypt, _sslEncrypt, _tlsEncrypt;
    private JRadioButton _simpAuth, _sslAuth, _gssapiAuth, _digestAuth;
    private JPasswordField _pwdText;
    private JLabel _bind, _pwd;
    private CustomComboBoxModel _fromModel, _toModel;
    private JPanel _toPanel, _fromPanel;
	private ReplicaPanel _replicaPanel;
    private JButton _browseButton;

	protected AgreementWizard _wizard;

    private AgreementWizardInfo _wizardInfo = null;
    private ReplicationAgreement _copy = null;
    private String _error ="WAgreementDestinationPanel";
    private String _portAttr = ATTR_PORT;
    
    private static final int CONTINUE = 0;
    private static final int PROMPT = 1;
    private static final int SKIP =3;
    private static final String DSClass = "nsDirectoryServer";
    private static final String ATTR_PORT = "nsserverport";
    private static final String ATTR_SECURE_PORT = "nssecureserverport";
	private static final String BACKEND_ATTR = "cn";
	private final String CONSUMER_LIST_NOT_AVAILABLE = _resource.getString("replication-destination-consumer-list-non-available",
															 "label");
}

