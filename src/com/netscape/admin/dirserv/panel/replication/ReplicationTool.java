/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.awt.*;
import java.net.*;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.AbstractButton;
import javax.swing.text.JTextComponent;
import javax.swing.border.*;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.ResourceSet;
import netscape.ldap.*;

/**
 * Image Retrieval Tools for the package
 *
 * @author  rweltman
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/25/97
 */
public class ReplicationTool {

	final static int borderWidth = UIFactory.getBorderInsets().left;
    final static Border EMPTY_BORDER =
	    BorderFactory.createEmptyBorder(borderWidth,borderWidth,
										borderWidth,borderWidth);
    final static Border LOWERED_BORDER =
	    BorderFactory.createLoweredBevelBorder();
    final static Border RAISED_BORDER =
	    BorderFactory.createRaisedBevelBorder();
    final static Border ETCHED_BORDER = BorderFactory.createEtchedBorder();
    final static Dimension DEFAULT_PANEL_SIZE = new Dimension(300,300);
    final static Dimension TALL_PANEL_SIZE = new Dimension(300,440);
    
    //copy from BlankPanel
    final static int DEFAULT_PADDING = 6;
    final static Insets DEFAULT_EMPTY_INSETS = new Insets(0,0,0,0);
    final static Insets BOTTOM_INSETS = new Insets(DEFAULT_PADDING,
												   DEFAULT_PADDING,
												   DEFAULT_PADDING,
												   DEFAULT_PADDING);
	final static Insets DEFAULT_INSETS = new Insets(DEFAULT_PADDING,
													DEFAULT_PADDING,
													DEFAULT_PADDING,
													0);

    // Used for getting/setting machine data DN in ConsoleInfo hashtable
    final static String MD_ENTRY_DN = "cn=replication, cn=config";
    final static String MD_ATTRIBUTE_KEY = "netscapeMDSuffix.key";

	// this has package scope, used by other classes in replication
	final static String REPLICA_CN = "replica";
	final static String REPLICA_RDN = "cn=" + REPLICA_CN;
  
  final static String WINDOWS_REPLICA_CN="replica";
  final static String WINDOWS_REPLICA_RDN="cn=" + WINDOWS_REPLICA_CN;
  
	final static String[] REPLICA_OBJECTCLASSES = {"top", "nsDS5Replica"};
	final static String REPLICA_ROOT_ATTR = "nsDS5ReplicaRoot";
	final static String REPLICA_TYPE_ATTR = "nsDS5ReplicaType";
	final static String REPLICA_BINDDN_ATTR = "nsDS5ReplicaBindDN";
	final static String PUBLIC_SUFFIX_ATTR = "nsslapd-suffix";
	final static String MMR_AGREEMENT_OBJECTCLASS = "nsDS5ReplicationAgreement";
	final static String[] MMR_AGREEMENT_OBJECTCLASSES = {"top", MMR_AGREEMENT_OBJECTCLASS};
  
  final static String WIN_AGREEMENT_OBJECTCLASS = "nsDSWindowsReplicationAgreement";
  final static String[] WIN_AGGREEMENT_OBJECTCLASSES = {"top", WIN_AGREEMENT_OBJECTCLASS};
  final static String REPLICA_WINDOWS_PRIMARY_HOST_ATTR = "nsDSWindowsReplicaPrimaryHost";
  final static String REPLICA_WINDOWS_DOMAIN = "nsds7WindowsDomain";
  final static String REPLICA_WINDOWS_SUBTREE = "nsds7WindowsReplicaSubtree";
  final static String REPLICA_DS_SUBTREE = "nsds7DirectoryReplicaSubtree";
  final static String REPLICA_NEW_WIN_USER_SYNC = "nsds7NewWinUserSyncEnabled";
  final static String REPLICA_NEW_WIN_GROUP_SYNC = "nsds7NewWinGroupSyncEnabled";
  final static String REPLICA_WINDOWS_ALTERNATE_HOST_ATTR = "nsDSWindowsReplicaAlternateHost";
	final static String REPLICA_HOST_ATTR = "nsDS5ReplicaHost";
	final static String REPLICA_PORT_ATTR = "nsDS5ReplicaPort";
	final static String REPLICA_TRANSPORT_ATTR = "nsDS5ReplicaTransportInfo";
	final static String REPLICA_TRANSPORT_SSL = "SSL";
	final static String REPLICA_TRANSPORT_TLS = "TLS";
    final static String REPLICA_TRANSPORT_LDAP= "LDAP";
	final static String REPLICA_CRED_ATTR = "nsDS5ReplicaCredentials";
	final static String REPLICA_BINDMETHOD_ATTR = "nsDS5ReplicaBindMethod";
	final static String REPLICA_REPLATTRS_ATTR = "nsDS5ReplicatedAttributeList"; 
	final static String REPLICA_BINDMETHOD_SIMPLE = "SIMPLE";
	final static String REPLICA_BINDMETHOD_SSLCLIENTAUTH = "SSLCLIENTAUTH";
	final static String REPLICA_BINDMETHOD_SASL_GSSAPI = "SASL/GSSAPI";
	final static String REPLICA_BINDMETHOD_SASL_DIGEST_MD5 = "SASL/DIGEST-MD5";
	final static String REPLICA_SCHEDULE_ATTR = "nsDS5ReplicaUpdateSchedule";
	final static String REPLICA_REFRESH_ATTR = "nsds5BeginReplicaRefresh";
	final static String MMR_NAME_ATTR = "cn";
	final static String MMR_NICKNAME_ATTR = "description";
	final static String LEGACYR_AGREEMENT_OBJECTCLASS = "LDAPReplica";
	final static String[] LEGACYR_AGREEMENT_OBJECTCLASSES = {"top", LEGACYR_AGREEMENT_OBJECTCLASS};
    final static String REPLICA_CONSUMER_INIT_BEGIN_ATTR = "nsds5replicaLastInitStart";
    final static String REPLICA_CONSUMER_INIT_END_ATTR = "nsds5replicaLastInitEnd";
    final static String REPLICA_CONSUMER_INIT_STATUS_ATTR = "nsds5replicalastinitstatus";
    final static String REPLICA_LAST_UPDATE_START_ATTR = "nsds5replicaLastUpdateStart";
    final static String REPLICA_LAST_UPDATE_END_ATTR = "nsds5replicaLastUpdateEnd";
    final static String REPLICA_N_CHANGES_SENT_ATTR = "nsds5replicaChangesSentSinceStartup";
    final static String REPLICA_LAST_UPDATE_STATUS_ATTR = "nsds5replicaLastUpdateStatus";
    final static String REPLICA_UPDATE_IN_PROGRESS_ATTR = "nsds5replicaUpdateInProgress";
	final static String REPLICA_PURGE_DELAY_ATTR = "nsds5ReplicaPurgeDelay";
	final static String DEFAULT_PURGE_DELAY = "604800";
	final static String REPLICA_ID_ATTR = "nsDS5ReplicaID";
	final static String REPLICA_LOG_CHANGES_ATTR = "nsDS5Flags";
	final static String REPLICA_REFERRAL_ATTR = "nsDS5ReplicaReferral";

	final static String REPLICA_CONSUMER_INIT_IN_PROGRESS = "start";
	
    final static String[] REPLICA_STATUS_ATTRS = {
        REPLICA_LAST_UPDATE_START_ATTR, REPLICA_LAST_UPDATE_END_ATTR,
        REPLICA_N_CHANGES_SENT_ATTR, REPLICA_LAST_UPDATE_STATUS_ATTR,
        REPLICA_UPDATE_IN_PROGRESS_ATTR,  REPLICA_CONSUMER_INIT_BEGIN_ATTR, 
		REPLICA_CONSUMER_INIT_END_ATTR,  REPLICA_CONSUMER_INIT_STATUS_ATTR,
		REPLICA_REFRESH_ATTR
    };

    static RemoteImage getImage( String name ) {
		RemoteImage i = (RemoteImage)_cPackageImages.get( name );
		if ( i != null )
			return i;
        i = new RemoteImage( _sImageDir + "/" + name );
		if ( i != null )
			_cPackageImages.put( name, i );
		return i;
	}

    public static void resetGBC(GridBagConstraints gbc) {
      		gbc.gridx      = gbc.RELATIVE;
      		gbc.gridy      = gbc.RELATIVE;
      		gbc.gridwidth  = 1;
      		gbc.gridheight = 1;
      		gbc.fill       = gbc.HORIZONTAL;
      		gbc.anchor     = gbc.CENTER;
//			gbc.ipadx      = 0;
//			gbc.ipady      = 0;
      		gbc.weightx    = 0.0;
      		gbc.weighty    = 0.0;
      		gbc.insets     = (Insets)DEFAULT_INSETS.clone();
	}    
    
    /**
     * Read the machine data dn from the root DSE
     */
    static String getMachineDataDN(ConsoleInfo info) {
	String machineDataDN = null;

	// ConsoleInfo extends HashTable, so we will just stuff the
	// Machine Data DN there the first time, and thereafter
	// just hand it out.
	if ((machineDataDN = (String)info.get(MD_ATTRIBUTE_KEY)) == null) {
	    // We haven't read it yet, so do so, and then save it
	    // in the hashtable.
	    LDAPConnection ld = info.getLDAPConnection();
	    if (ld != null) {
		LDAPEntry entry = null;
		try {
		    entry = ld.read(MD_ENTRY_DN);
		} catch (LDAPException le) {
		    Debug.println("fail read : " +  MD_ENTRY_DN + " error:"  + le.toString());
		}
		if(entry != null){
		    machineDataDN = entry.getDN();
		    info.put(MD_ATTRIBUTE_KEY, machineDataDN);
		}
	    }
	}
	Debug.println("getMachineDataDN: returning " + machineDataDN);
	return machineDataDN;
    }


    static private String lookupRealDSHost(ConsoleInfo info) {
        LDAPConnection ld = info.getLDAPConnection();
        LDAPEntry entry;
        String dataversion="";
        String[] attrList = {"dataversion"};
        try {
            entry = ld.read("", attrList);
            LDAPAttributeSet attrs = entry.getAttributeSet();
            Enumeration attrsEnum = (Enumeration)attrs.getAttributes();
            while (attrsEnum.hasMoreElements()){
                LDAPAttribute attr = (LDAPAttribute)attrsEnum.nextElement();
                if (attr.getName().equalsIgnoreCase("dataversion"))
                    dataversion = (String)attr.getStringValues().nextElement();
            }
        } catch (LDAPException e) {
            return "";    
        }
        //parse string
        int col = dataversion.indexOf(":");
        if (col == -1)
            return "";
        return dataversion.substring(0, col).trim();
        
    }
    
    // Try to "gently" fully-qualify a host name.
    // Algorithm.  Determine the canonical host name, using the
    // above method, then see if the leftmost component of the
    // host name is the same as the candidate host name.  If
    // so, then change the hsot name, otherwise leave it alone.
    // So, if we determine the canonical host name of
    // "foo" and it turns out to be "bar.mcom.com", leave it
    // alone.  But, it it returns "foo.mcom.com", then
    // update it.
    static String fullyQualifyHostName(String host) {
        String retHost = host;
        if (host != null) {
	    int host_len = host.indexOf('.');
	    if (host_len == -1)
		host_len = host.length();
            try {
                String newHost = DSUtil.canonicalHost(host);
                if (newHost != null) {
		    int new_host_len = newHost.length();
		    if (host_len > new_host_len)
			host_len = new_host_len;
                    if (newHost.substring(0, host_len).equalsIgnoreCase(host)) {
                        retHost = newHost;
                    }
                }
            } catch (UnknownHostException e) {
            }
        }
        Debug.println("fullyQualifyHostName: return " + retHost);
        return retHost;
    }
    
    /**
	 * Get Authentication as DM
	 * @return true if ok
	 */
    public static boolean authenticate(IDSModel model) {
        boolean success = true;
        while (!verifyDM(model.getServerInfo()) ) {

            LDAPConnection ld = model.getServerInfo().getLDAPConnection();
            if (!ld.isConnected()) {
                return false;
            }

			String msg =_resource.getString("replication-dialog",
											"authenticationAsDM");
			DSUtil.showErrorDialog( model.getFrame(), "error", msg,
									"replication-dialog" );
            if (! model.getNewAuthentication(false)) {
                success = false;
                break;
            }
        }
		model.notifyAuthChangeListeners();
        return success;
    }
    
    //verify DM authentication by searching the machine data
    static boolean verifyDM(ConsoleInfo info) {
        
        LDAPConnection ld = info.getLDAPConnection();
        //no bind null
        if (ld.getAuthenticationDN().trim().equals(""))
            return false;

		Debug.println( "ReplicationTool.verifyDM: authDN = <" +
					   ld.getAuthenticationDN() + ">, authPassword = <" +
					   ld.getAuthenticationPassword() + ">" );
        //try searching
        LDAPSearchResults reslist = null;
        LDAPSearchConstraints searchConstraints;
        String filter = "(objectclass=*)";
        try {
            reslist = ld.search(getMachineDataDN(info), ld.SCOPE_SUB,
								filter, null, false);
        } catch (LDAPException e) {
			Debug.println( "ReplicationTool.verifyDM: " + e );
            displayError(e.errorCodeToString());
            return false;
        }
        
        if (reslist.hasMoreElements()) {
            return true;
        }
		Debug.println( "ReplicationTool.verifyDM: no results on search" );
        
        return false;
    }
    
    /**
     * LDAP Error Message display
     * @param code LDAPException code
     */
    static void displayError( String msg ) {
        if ( (msg == null) || (msg.equals("")) ) {
            msg = _resource.getString("replication","err0080");
        }
        DSUtil.showErrorDialog( null, "error", msg,
								"replication-dialog" );
     }
     
    static void displayError( int code ) {
		displayError( LDAPException.errorCodeToString( code ) );
     }

	static void displayError(Component frame, String section,
							 String titleKey, String key,
							 String[] args) {
		DSUtil.showErrorDialog(frame, titleKey, key, args,
							   section, _resource);
	}

	/**
	 * This method converts the value of a replica schedule attribute to
	 * a form suitable for the GUI.  See also
	 * synchronize.c:read_schedule_item().
	 *
	 * @param  sched      The value of a replica schedule attribute from
	 *                    the agreement stored in the directory
	 * @param  fromHr     The hour part of the time to start replicating.
	 *                    The length will be set to 0 before any other data
	 *                    is written to it.
	 * @param  fromMin    The minutes part of the time to start replicating.
	 *                    The length will be set to 0 before any other data
	 *                    is written to it.
	 * @param  toHr       The hour part of the time to stop replicating.
	 *                    The length will be set to 0 before any other data
	 *                    is written to it.
	 * @param  toMin      The minutes part of the time to stop replicating.
	 *                    The length will be set to 0 before any other data
	 *                    is written to it.
	 * @param  daysOfWeek A list of the days of the week on which to
	 *                    replicate.  The length will be set to 0 before
	 *                    any other data is written to it.
	 * @return            true if sched contained something valid e.g. at least
	 *                    1 time
	 */
	static boolean parseReplicaSchedule(String sched,
										StringBuffer fromHr,
										StringBuffer fromMin,
										StringBuffer toHr,
										StringBuffer toMin,
										StringBuffer daysOfWeek) {
		// check for bogosity
		if (fromHr == null || fromMin == null || toHr == null ||
			toMin == null || daysOfWeek == null) {
			return false;
		}

		// clear the buffers
		fromHr.setLength(0);
		fromMin.setLength(0);
		toHr.setLength(0);
		toMin.setLength(0);
		daysOfWeek.setLength(0);

		// assumptions:
		// if sched is not null, it contains at least a 4 character
		// fromTime.  If a '-' is present, a 4 character toTime follows.
		// If a ' ' is present, the daysOfWeek string follows.

		boolean status = false; // true if sched contained something . . .
		if (sched != null && !sched.equals ("") && sched.charAt(0) == '*') {
			Debug.println("ReplicationTool.parseReplicaSchedule(): invalid" +
						  " replica schedule [" + sched + "]");
			sched = "0000-2359" + sched.substring(1);
		}

		if (sched != null && !sched.equals ("") && sched.length() >= 4) {
			fromHr.append(sched.substring(0, 2));
			fromMin.append(sched.substring(2, 4));

			int dashIndex = sched.indexOf('-');
			if (dashIndex > 0) {
				toHr.append(sched.substring(dashIndex+1,
											dashIndex+3));
				toMin.append(sched.substring(dashIndex+3,
											 dashIndex+5));
			} else {
				toHr.append("00");
				toMin.append("00");
			}

			int spaceIndex = sched.indexOf(' ');
			if (spaceIndex > 0)
				daysOfWeek.append(sched.substring(spaceIndex+1).trim());
			else
				daysOfWeek.append("0123456");

			status = true;
		} else {
			fromHr.append("00");
			fromMin.append("00");
			toHr.append("23");
			toMin.append("59");
			daysOfWeek.append("0123456");
		}

		return status;
	}

	static String createDateString(JTextComponent startHr,
								   JTextComponent startMin,
								   JTextComponent endHr,
								   JTextComponent endMin,
								   AbstractButton[] buttons) {
        String dowString = "";
        String startTime = pad4(startHr.getText(), startMin.getText());
        String endTime = pad4(endHr.getText(), endMin.getText());
        for (int i = 0; i < 7; i++) {
            if (buttons[i].isSelected()) {
                dowString += Integer.toString(i);
            }
        }
        return startTime + "-" + endTime + " " + dowString;
	}

    static String pad4(String hour, String min) {
		String res;

		if (hour.length () == 0)
			res = "00";
		else if	(hour.length () == 1)
			res = "0" + hour;
		else
			res = hour;

        if (min.length() == 0)
            return res + "00";
        else if (min.length() == 1)
            return res + "0" + min;
		else
			return res + min;
    }

	/**
	  * Takes a message with format <Error Code> <Error message> and creates a more user friendly message 
	  * with it.
	  * If there is an error (error code different than 0) the result is  a message of type  <Error message>. Code: <Error Code>.
	  * If there is not an error the result is a message of type <Error message>.
	  *
	  * If something goes wrong we return the original message.
	  * The following examples correspond to the english version:
	  * Example 1: "0 Incremental Update Succeeded"  becomes "Incremental Update Succeeded."
	  * Example 2: "82 Could not connect to LDAP Server" becomes "Could not connect to LDAP Server. Error Code: 82"
	  *
	  * @param originalMessage String the original message
	  * @return the message with the new format
	  */
	public static String convertStatusMessage(String originalMessage) {	
		StringTokenizer st = new StringTokenizer(originalMessage, " ");
		String code = st.nextToken();
		if ((code != null) &&
			(code.length() > 0)) {
			try {
				int theCode = Integer.parseInt(code);
				/* We could parse the token => until here everything is fine! */				
				String errorMessage = originalMessage.substring(code.length() + 1);
				if ((errorMessage != null) &&
					(errorMessage.length() > 0)) {
					if (theCode != 0) {
						String[] args = {errorMessage, code};
						return _resource.getString("replication", "convertupdatemessage-error-label", args);
					} else {
						return errorMessage;
					}
				}
			} catch (Exception ex) {
				Debug.println("ReplicationTool.convertStatusMessage() exception parsing "+code +": "+ex);				
			}
		}
		return originalMessage;
	}

	/**
	 * Tries to determine if the given string is a hashed password.  Assumes the hashed password is
	 * in a form like {[a-zA-Z0-9]+} e.g. {crypt} or {SSHA} or {DES}
	 * null and empty strings are considered not hashed
	 * The string must begin with the '{' character, followed by alphanumeric characters
	 * followed by '}' followed by more alphanum to be considered a hashed password
	 *
	 * @param pwd The hashed password
	 * @return true if the string is a hashed password, false otherwise
	 */
	public static boolean isHashedPwd(String pwd) {
		boolean ret = false;
		if (pwd != null && pwd.length() > 0) {
			if (pwd.charAt(0) == '{') {
				int ii;
				for (ii = 1; ii < pwd.length() &&
				     Character.isLetterOrDigit(pwd.charAt(ii)); ++ii) {
				}
				// there should be more characters in the string after the \}
				ret = ((ii+1) < pwd.length() && pwd.charAt(ii) == '}');
			}
		}
		return ret;
	}

	//get resource bundle
    private static ResourceSet _resource =
	    new ResourceSet(
			"com.netscape.admin.dirserv.panel.replication.replication");
	private static Hashtable _cPackageImages = new Hashtable();
    private static String _sImageDir = "com/netscape/admin/dirserv/images";
}
