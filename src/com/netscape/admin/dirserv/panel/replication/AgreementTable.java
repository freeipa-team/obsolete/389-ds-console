/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.util.*;
import java.net.*;

/**
 * Container for  Replication Agreements
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/11/97
 * @see     com.netscape.admin.dirserv.panel
 */
public class AgreementTable extends Vector{
    
    /*==========================================================
     * constructors
     *==========================================================*/    
    public AgreementTable() {
        super();
    }

	/*==========================================================
	 * public methods
     *==========================================================*/
     
    /**
     * add replication agreement
     * @param agreement replication agreement
     */
    void addAgreement(ReplicationAgreement agreement) {
       addElement(agreement);
    }
    
    /**
     * remove replication agreement with specified nickname
     * @param nickname nickname of the replication agreement
     */
    void removeAgreement(ReplicationAgreement agreement) {
        removeElement(agreement);
    }
    
    /**
     * check agreement
     * @param agreement replication agreement
     * return true if agreement is associated with particular
     *        nickname already; otherwise false.
     */
    boolean contains(ReplicationAgreement agreement) {
        return contains(agreement);
    }
   
    
    /**
     * remove all agreement from this table
     */
    void removeAll() {
        removeAllElements();
    }
    
}