/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.util.*;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.tree.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSFileDialog;
import com.netscape.admin.dirserv.GlobalConstants;
import com.netscape.admin.dirserv.DSResourceModel;
import netscape.ldap.*;

/**
 * Representation of the Replication Agreement Node
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/19/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class AgreementResourceObject extends ResourceObject 
                                     implements IMenuInfo, ActionListener {   
    
    /*==========================================================
     * constructors
     *==========================================================*/
    AgreementResourceObject() {
        super();   
    }
    
    AgreementResourceObject(String sDisplayName) {
        super(sDisplayName);   
    }
    
    AgreementResourceObject(String sDisplayName,
							Icon icon, Icon largeIcon,
							IReplicationResourceObject owner) {
        super(sDisplayName, icon, largeIcon);
        _owner = owner;
		_gparent = null;
    }
    
    AgreementResourceObject(String sDisplayName,
							Icon icon, Icon largeIcon,
							IReplicationResourceObject owner,
							ResourceObject gparent ) {
        super(sDisplayName, icon, largeIcon);  
        _owner = owner;
		_gparent = gparent;
    }    
    /*==========================================================
     * public methods
     *==========================================================*/    
	
	void setAgreement(ReplicationAgreement agreement) {
        _agreement = agreement;        
    }
	
	IDSModel getResourceModel() {
		return _owner.getModel();
	}
	
	JFrame getFrame() {
		return _owner.getModel().getFrame();
    }
	
    public Component getCustomPanel() {
		if ( _panel == null )
			_panel = new AgreementPanel( getFrame(),
										 getResourceModel(),
										 _agreement);
		return _panel;
    }

	public boolean isLeaf() {
		return true;
	}

										 
    /**
	 * Handle incoming event.
	 *
	 * @param e event
	 */
	public void actionPerformed(ActionEvent e) {
		getResourceModel().setWaitCursor(true);
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {
			LDAPEntry entry = reloadEntry();
			/* If the entry corresponding to this agreement does not exist anymore,
			   we update all the agreements tree */
			if (entry == null) {
				if (_owner instanceof ReplicaResourceObject) {
					((ReplicaResourceObject)_owner).reload();
					((ReplicaResourceObject)_owner).refreshTree();
				}
			}
		}
		getResourceModel().setWaitCursor(false);
	}
										 
	private LDAPEntry reloadEntry() {
		LDAPConnection ldc = getResourceModel().getServerInfo().getLDAPConnection();
		LDAPEntry entry = null;
		try {
			if (_agreement != null) {
				entry = ldc.read(_agreement.getOrigEntryDN());
			}
		} catch (LDAPException e) {			
			Debug.println("AgreementResourceObject.reloadEntry(): "+e);
			if (e.getLDAPResultCode() != LDAPException.NO_SUCH_OBJECT) {
				DSUtil.showLDAPErrorDialog(getResourceModel().getFrame(),
										   e,
										   "fetching-directory");
			}
		}
		return entry;
	}
										 
	
    //============  IMENUINFO  ==================
	public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}

	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}
	
	private IMenuItem[] createMenuItems() {
        if (this._agreement.getAgreementType() == this._agreement.AGREEMENT_TYPE_AD)
        {
    		String section = "sync-node-agreementPopupMenu";
    		return new IMenuItem[] {
     				new MenuItemText( DELETE,
    								  _resource.getString( section, "Delete"),
    								  _resource.getString( section, 
                                                           "Delete-description")),                                                  

    				new MenuItemText( UPDATE,
    								  _resource.getString( section, "SendUpdatesNow"),
    								  _resource.getString( section,
    													   "SendUpdatesNow-description")),
    				new MenuItemText( INIT,
    								  _resource.getString( section, "ReSync"),
    								  _resource.getString( section,
    													   "ReSync-description")),
                    new MenuItemSeparator(),
                    new MenuItemText( DSResourceModel.REFRESH,
                                      DSUtil._resource.getString("menu", "refresh"),
                                      DSUtil._resource.getString("menu",
                                                                 "refresh-description"))
    				};
        } else {
		String section = "replication-node-agreementPopupMenu";
		return new IMenuItem[] {
			new MenuItemText( NEW,
							  _resource.getString( section, "New"),
							  _resource.getString( section,
												   "New-description")),
				new MenuItemText( DELETE,
								  _resource.getString( section, "Delete"),
								  _resource.getString( section,
													   "Delete-description")),
				new MenuItemText( COPY,
								  _resource.getString( section, "Copy"),
								  _resource.getString( section,
													   "Copy-description")),
				new MenuItemText( UPDATE,
								  _resource.getString( section, "SendUpdatesNow"),
								  _resource.getString( section,
													   "SendUpdatesNow-description")),
				new MenuItemText( INIT,
								  _resource.getString( section, "InitConsumer"),
								  _resource.getString( section,
													   "InitConsumer-description")),		
				new MenuItemText( CREATE,
								  _resource.getString( section, "CreateLDIF"),
								  _resource.getString( section,
													   "CreateLDIF-description")),
				new MenuItemSeparator(),
				new MenuItemText( DSResourceModel.REFRESH,
								  DSUtil._resource.getString("menu", "refresh"),
								  DSUtil._resource.getString("menu",
															 "refresh-description"))
				
				};
	}
	} // End createMenuItems();
										 
	
    public void actionMenuSelected(IPage viewInstance, IMenuItem item)
		{			
			getResourceModel().setWaitCursor(true);
			if (item.getID().equals(DSResourceModel.REFRESH)) {
				LDAPEntry entry = reloadEntry();
				/* If the entry corresponding to this agreement does not exist anymore,
				   we update all the agreements tree */
				if (entry == null) {
					if (_owner instanceof ReplicaResourceObject) {
						((ReplicaResourceObject)_owner).reload();
						((ReplicaResourceObject)_owner).refreshTree();
					}
				}
				/* We refresh the panels */
				((ActionListener)_panel).actionPerformed(
									   new ActionEvent( this,
														ActionEvent.ACTION_PERFORMED,
														DSResourceModel.REFRESH ) );
			} else if (item.getID().equals(NEW)) {				
				if (! ReplicationTool.authenticate(getResourceModel())) {
					getResourceModel().setWaitCursor(false);
					return;								
				}
				/* We use this variable to keep the tree expanded OR to expand it 
				   automatically if a new replication agreements has been added */
				int oldChildCount = ((ReplicaResourceObject) _owner).getChildCount();

				AgreementWizardInfo info = new AgreementWizardInfo();
				info.setServerInfo(getResourceModel().getServerInfo());
				info.setConsoleInfo(getResourceModel().getConsoleInfo());
				info.setParentNode(_owner);
				info.setWizardType(info.NEW_WIZARD);
				info.setLDAPSchema(getResourceModel().getSchema());
				AgreementWizard wizard = new AgreementWizard(getFrame(), info);

				int newChildCount = ((ReplicaResourceObject) _owner).getChildCount();
				/* Has the replication agreement been added to our replica??*/
				Debug.println("AgreementResourceObject.actionMenuSelected() newChildCount "+newChildCount+" oldChildCount "+oldChildCount);
				if (newChildCount <= oldChildCount) {
					/* We have two cases here: the replication agreement has not been created OR
					   a new replication agreement under another replica has been created.
					   As we don't know which is the case we have to refresh the grand parent (replication
					   node) */
					if (_gparent instanceof ReplicationResourceObject) {
						((ReplicationResourceObject)_gparent).reload();
						((ReplicationResourceObject)_gparent).refreshTree();
					}				
				} else if (_owner instanceof ReplicaResourceObject) {												
					/* The owner was expanded (this resource object was visible). */								   					
					((ReplicaResourceObject) _owner).expandPath((ResourcePage)viewInstance);						
				}
			}    
			if (item.getID().equals(DELETE)) {
				if (! ReplicationTool.authenticate(getResourceModel())) {
					getResourceModel().setWaitCursor(false);
					return;
				}
				int option = JOptionPane.YES_OPTION;
				if ( DSUtil.requiresConfirmation(GlobalConstants.PREFERENCES_CONFIRM_DELETE_AGREEMENT ) ) {
                    String msg;
                    if (_agreement.getAgreementType() == _agreement.AGREEMENT_TYPE_AD) {
                        msg = _resource.getString("sync-node-dialog",
													 "deleteAgreement");
                    } else {
                        msg = _resource.getString("replication-node-dialog",
                                                     "deleteAgreement");
                    }
					option = DSUtil.showConfirmationDialog(getFrame(),
														   "warning",
														   msg,
														   "replication-dialog");
				}
				if (option == JOptionPane.YES_OPTION) {
					boolean success = false;
					try {						
						_agreement.deleteAgreementFromServer();
						success = true;
					} catch (LDAPException e) {
						Debug.println("AgreementResourceObject: " +
									  "actionMenuSelected() -delete" +
									  e.toString());
						String msg;
                        if (_agreement.getAgreementType() == _agreement.AGREEMENT_TYPE_AD) {
                            msg = _resource.getString("sync-node-dialog",
                                    "cannotDeleteAgreement");
                        } else {
                            msg = _resource.getString("replication-node-dialog",
														 "cannotDeleteAgreement");
                        }
						DSUtil.showErrorDialog( getFrame(), "error", msg,
												"replication-dialog" );
					} finally {
						if (success) {
							if (_owner instanceof ReplicaResourceObject) {							
								((ReplicaResourceObject) _owner).reload();
								((ReplicaResourceObject) _owner).refreshTree();								
								/* The owner was expanded (this resource object was visible).  
								 If there are other agreements We keep them visible*/
								if (!((ReplicaResourceObject) _owner).isLeaf()) {
									((ReplicaResourceObject) _owner).expandPath((ResourcePage)viewInstance);
								}
							}
						}												
					}
				}
			}
			if (item.getID().equals(COPY)) {
				if (! ReplicationTool.authenticate(getResourceModel())) {
					getResourceModel().setWaitCursor(false);
					return;
				}
				/* We use this variable to keep the tree expanded OR to expand it 
				   automatically if a new replication agreements has been added */
				int oldChildCount = ((ReplicaResourceObject) _owner).getChildCount();

				((ReplicaResourceObject) _owner).refreshTree();	

				AgreementWizardInfo info = new AgreementWizardInfo();
				info.setServerInfo(getResourceModel().getServerInfo());
				info.setConsoleInfo(getResourceModel().getConsoleInfo());
				info.setCopyAgreement(_agreement);
				info.setParentNode(_owner);
				info.setWizardType(info.COPY_WIZARD);
				info.setLDAPSchema(getResourceModel().getSchema());
        if ( _agreement.getAgreementType() == _agreement.AGREEMENT_TYPE_AD ) {
            WindowsAgreementWizard wizard = new WindowsAgreementWizard(getFrame(), info); 
 
				int newChildCount = ((ReplicaResourceObject) _owner).getChildCount();
				/* Has the replication agreement been added to our replica??*/
				Debug.println("AgreementResourceObject.actionMenuSelected() newChildCount "+newChildCount+" oldChildCount "+oldChildCount);
				if (newChildCount <= oldChildCount) {
					/* We have two cases here: the replication agreement has not been created OR
					   a new replication agreement under another replica has been created.
					   If the user has not cancelled, we assume the second case. */
					if (!wizard.isCancelled() && 
						(_gparent instanceof ReplicationResourceObject)) {
						((ReplicationResourceObject)_gparent).reload();
						((ReplicationResourceObject)_gparent).refreshTree();
					}				
				} else if (_owner instanceof ReplicaResourceObject) {												
					/* The owner was expanded (this resource object was visible). */								   					
					((ReplicaResourceObject) _owner).expandPath((ResourcePage)viewInstance);						
        }           
        } else {
				AgreementWizard wizard = new AgreementWizard(getFrame(), info); 

				int newChildCount = ((ReplicaResourceObject) _owner).getChildCount();
				/* Has the replication agreement been added to our replica??*/
				Debug.println("AgreementResourceObject.actionMenuSelected() newChildCount "+newChildCount+" oldChildCount "+oldChildCount);
				if (newChildCount <= oldChildCount) {
					/* We have two cases here: the replication agreement has not been created OR
					   a new replication agreement under another replica has been created.
					   If the user has not cancelled, we assume the second case. */
					if (!wizard.isCancelled() && 
						(_gparent instanceof ReplicationResourceObject)) {
						((ReplicationResourceObject)_gparent).reload();
						((ReplicationResourceObject)_gparent).refreshTree();
					}				
				} else if (_owner instanceof ReplicaResourceObject) {												
					/* The owner was expanded (this resource object was visible). */								   					
					((ReplicaResourceObject) _owner).expandPath((ResourcePage)viewInstance);						
        }
        }	   
			}
			if (item.getID().equals(UPDATE)) {
				if (! ReplicationTool.authenticate(getResourceModel())) {
					getResourceModel().setWaitCursor(false);
					return;		    
				}
				_agreement.updateNow();
			}
			if (item.getID().equals(INIT)) {
				if (! ReplicationTool.authenticate(getResourceModel())) {
					getResourceModel().setWaitCursor(false);
					return;
				}
                String msg;
                if (_agreement.getAgreementType() == _agreement.AGREEMENT_TYPE_AD) {
                    msg = _resource.getString("sync-node-dialog", "ReSync");
                } else {
                    msg = _resource.getString("replication-node-dialog", "InitConsumer");                                       
                }
                
                int option = DSUtil.showConfirmationDialog(getFrame(), 
                        "warning", msg, "replication-dialog");
                
				if (option == JOptionPane.YES_OPTION) {
					_agreement.initializeConsumer();					
				}
			}
			/* create LDIF file             */
			if (item.getID().equals (CREATE)) 
				{		
					_agreement.populateLDIFFile(getResourceModel());
				}
			getResourceModel().setWaitCursor(false);	   
		}

    
    private String filterStatusMessage(String msg)
		{
			String retStr = null;
			int cns, cne;
			String cn;
			
			if (msg == null) {
				return null;
			}
			
        // Find the start of the replica CN
        if ((cns = msg.indexOf(' ')) == -1) {
            return null;
        }
        cns++;
        // Find the end of the replica CN
        if ((cne = msg.indexOf(' ', cns)) == -1) {
            return null;
        }
        cn = msg.substring(cns, cne);
        if (cn.equalsIgnoreCase(_agreement.getEntryCN())) {
            retStr= msg;
        }

        return retStr;
    }
	
	
	
    /*==========================================================
     * variables
     *==========================================================*/
	protected String[] _categoryID;	
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;

    ReplicationAgreement _agreement;
    IReplicationResourceObject _owner;
    private ResourceObject _gparent;
    private AgreementPanel _panel = null;
	
    //get resource bundle
    public static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
    
    static final String NEW    = "newAgreement";
    static final String DELETE = "deleteAgreement";
    static final String COPY   = "copyAgreement";
    static final String UPDATE = "updateAgreement";
    static final String INIT   = "initAgreement";
    static final String CREATE = "createLDIF";
}
