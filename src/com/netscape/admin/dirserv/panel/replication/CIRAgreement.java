/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import netscape.ldap.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.console.ConsoleInfo;
import java.util.*;
import java.io.*;

public class CIRAgreement extends ReplicationAgreement {

    /**
     * Constructs a new, empty replication agreement
     */
    public CIRAgreement(ConsoleInfo info) {
        super(ReplicationAgreement.AGREEMENT_TYPE_CIR);
        consumerHost = info.getHost();
        consumerPort = info.getPort();
        setServerInfo(info);
        setOrigEntryDN("cn=xxx, "+ReplicationTool.getMachineDataDN(info));
        supplierUnreachable = false;
        syncInterval = DEFAULT_SYNC_INTERVAL;
    }


    /**
     * Constructs a new replication agreement
     */
    public CIRAgreement(LDAPEntry entry, ConsoleInfo info) {
        super(ReplicationAgreement.AGREEMENT_TYPE_CIR);
        
        syncInterval = DEFAULT_SYNC_INTERVAL;
        consumerHost = info.getHost();
        consumerPort = info.getPort();
        setServerInfo(info);
        setOrigEntryDN(entry.getDN());
        setEntryDN(entry.getDN());
        readValuesFromEntry(entry);
        agreementIsNew = false;
    }

    /**
     * Set the usePersistentSearch flag.
     */
    public void setUsePersistentSearch(String val) {
        if (val.equalsIgnoreCase("true")) {
            usePersistentSearch = true;
        } else {
            usePersistentSearch = false;
        }
    }

    /**
     * Get the usePersisentSearch flag.
     */
    public boolean getUsePersistentSearch() {
        return usePersistentSearch;
    }

    /**
     * Set the syncInterval
     */
    public void setSyncInterval(int val) {
        syncInterval = val;
    }

    /**
     * Get the sync interval
     */
    public int getSyncInterval() {
        return syncInterval;
    }

    public void setUnreachable(boolean x) {
        supplierUnreachable = x;
        Debug.println("CIRAgreement.setUnreachable: " + x);
    }

    public boolean getUnreachable() {
        return supplierUnreachable;
    }

    /**
     * Return a long, descriptive string for this agreement
     */
    public String getDescription() {
        String r;

        r = getSupplierHost();
        if (r != null && getSupplierPort() != 0) {
            r = r + " : " + getSupplierPort();
        }
        if (getNickname() != null) {
            r = r + " (" + getNickname() + ")";
        }
        return r;
    }


    /**
     * Read a consumer-initiated replication agreement from the server.
	 * If successful, read the attribute values from the entry and
	 * populate the members of this CIRAgreement object.
     */
    public void readValuesFromEntry(LDAPEntry entry) {
        LDAPAttributeSet attrs = entry.getAttributeSet();
        Enumeration attrsEnum = (Enumeration)attrs.getAttributes();
        byte[] raw_value;
        String conv_value;
		String cn = null;

        //initialize values
        while (attrsEnum.hasMoreElements()) {
            LDAPAttribute attr = (LDAPAttribute)attrsEnum.nextElement();
            if (attr.getName().equalsIgnoreCase("cirUpdateSchedule")) {
                Enumeration valsEnum = attr.getStringValues();
                while (valsEnum.hasMoreElements()) {
                    addUpdateSchedule((String)valsEnum.nextElement());
                }
				continue;
			}
			// The rest of the attributes are single-valued
			Enumeration en = attr.getStringValues();
			if( !en.hasMoreElements() ) {
				continue;
			}
			String val = (String)en.nextElement();
            if (attr.getName().equalsIgnoreCase("replicaNickname")) {
				setNickname(val);
            } else if (attr.getName().equalsIgnoreCase("cirReplicaRoot")) {
				setReplicatedSubtree(val);
				origReplicatedSubtree = getReplicatedSubtree();
            } else if (attr.getName().equalsIgnoreCase("cirHost")) {
				setSupplierHost(val);
				origSupplierHost = getSupplierHost();
            } else if (attr.getName().equalsIgnoreCase("cirPort")) {
				setSupplierPort(Integer.parseInt(val));
				origSupplierPort = getSupplierPort();
            } else if (attr.getName().equalsIgnoreCase("cirBindDN")) {
				setBindDN(val);
            } else if (attr.getName().equalsIgnoreCase("cirBindCredentials")) {
				setBindCredentials(val);
            } else if (attr.getName().equalsIgnoreCase(
				"cirUsePersistentSearch")) {
					setUsePersistentSearch(val);
            } else if (attr.getName().equalsIgnoreCase("cirUseSSL")) {
				setUseSSL(val);
            } else if (attr.getName().equalsIgnoreCase("cirSyncInterval")) {
				setSyncInterval(Integer.parseInt(val) / 60);
            } else if (attr.getName().equalsIgnoreCase("cn")) {
				cn = val;
            } else if (attr.getName().equalsIgnoreCase("cirBeginORC")) {
				setORCValue(val);
            }
        }
		if ( cn != null ) {
			setEntryCN(cn);
		}
    }

    public int writeToServer() throws IOException {
        LDAPAttribute attr;
        byte[] value;
        String stmp;

        if ( agreementDNHasChanged() || agreementIsNew ) {

            LDAPAttributeSet newAttrs = new LDAPAttributeSet();

			String[] vals = { "top", CIRAgreementClass };
            attr = new LDAPAttribute( "objectclass", vals );
            newAttrs.add(attr);

            attr = new LDAPAttribute("replicaNickname", nickname);
            newAttrs.add(attr);

            attr = new LDAPAttribute("cn", entryCN);
            newAttrs.add(attr);

            if (replicatedSubtree == null) {
				attr = new LDAPAttribute("cirReplicaRoot", "");
            } else {
				attr = new LDAPAttribute("cirReplicaRoot", replicatedSubtree);
            }
            newAttrs.add(attr);

            attr = new LDAPAttribute("cirHost", supplierHost);
            newAttrs.add(attr);

            attr = new LDAPAttribute("cirPort",
									 Integer.toString(supplierPort));
            newAttrs.add(attr);

            if (bindDN != null) {
                attr = new LDAPAttribute("cirBindDN", bindDN);
                newAttrs.add(attr);
            }

            if (bindCredentials != null) {
                attr = new LDAPAttribute("cirBindCredentials",
										 bindCredentials);
                newAttrs.add(attr);
            }

            attr = new LDAPAttribute("cirUseSSL", useSSL ? "1" : "0");
            newAttrs.add(attr);

            attr = new LDAPAttribute("cirSyncInterval",
                    String.valueOf(syncInterval * 60 ));
            newAttrs.add(attr);

            // XXXggood filtered and selective attribute attributes would
			// get written
            // here, if we supported them in CIR

            String[] sched = getUpdateScheduleStrings();
            if (sched != null) {
				// first, see if any of the values are valid
				boolean valid = false;
                for (int i = 0; !valid && i < sched.length; i++) {
					valid = (sched[i] != null) && (sched[i].length() > 0);
                }
				if (valid) {
                    attr = new LDAPAttribute("cirUpdateSchedule", sched);
                    newAttrs.add(attr);
                }
            }

            LDAPEntry newEntry = new LDAPEntry(entryDN, newAttrs);
            try {
                createNewEntry(newEntry);
            } catch (LDAPException e) {
                Debug.println("CIRAgreement.writeToServer: <" + entryDN +
							  "> " + e.toString());
                return e.getLDAPResultCode();
            }
            // Delete the old entry
            if (!agreementIsNew) {
                try {
                    deleteOldEntry();
					setOrigEntryDN( getEntryDN() );
                } catch (LDAPException de) {
					Debug.println("CIRAgreement.writeToServer: <" + entryDN +
								  "> " + de.toString());
                    return de.getLDAPResultCode();
                }
            }

        } else {

            LDAPModification mod;
            LDAPModificationSet mods = new LDAPModificationSet();

            attr = new LDAPAttribute("replicaNickname", getNickname());
            mods.add(LDAPModification.REPLACE, attr);

            attr = new LDAPAttribute("cn", entryCN);
            mods.add(LDAPModification.REPLACE, attr);

            attr = new LDAPAttribute("cirReplicaRoot", replicatedSubtree);
            mods.add(LDAPModification.REPLACE, attr);

	    if (bindDN != null) {
		attr = new LDAPAttribute("cirBindDN", bindDN);
	    } else {
		attr = new LDAPAttribute("cirBindDN");
	    }
	    mods.add(LDAPModification.REPLACE, attr);
	    if (bindCredentials != null) {
		attr = new LDAPAttribute("cirBindCredentials",
					    bindCredentials);
	    } else {
		attr = new LDAPAttribute("cirBindCredentials");
	    }
	    mods.add(LDAPModification.REPLACE, attr);

            attr = new LDAPAttribute("cirUseSSL", useSSL ? "1" : "0");
            mods.add(LDAPModification.REPLACE, attr);

            String[] sched = getUpdateScheduleStrings();
			attr = new LDAPAttribute("cirUpdateSchedule");
            if (sched != null) {
				// first, see if any of the values are valid
				boolean valid = false;
                for (int i = 0; !valid && i < sched.length; i++) {
					valid = (sched[i] != null) && (sched[i].length() > 0);
                }
                if (valid) {
					// easier than setting individual elements . . .
                    attr = new LDAPAttribute("cirUpdateSchedule", sched);
                }
            }

			mods.add(LDAPModification.REPLACE, attr);

            attr = new LDAPAttribute("cirSyncInterval",
                    String.valueOf(syncInterval * 60 ));
            mods.add(LDAPModification.REPLACE, attr);

            try {
                updateEntry(mods);
            } catch (LDAPException me) {
				Debug.println("CIRAgreement.writeToServer: <" + entryDN +
							  "> " + me.toString());
                return me.getLDAPResultCode();
            }
        }
        return 0;
    }
    
    //variables
    private boolean usePersistentSearch;   // true if client should keep
                                           //connection to server open
    private int syncInterval;              // time between updates (in minutes)
    private boolean supplierUnreachable;   // true iff we can't talk to the
                                           //remote supplier right now
    private static final int DEFAULT_SYNC_INTERVAL = 10;    // 10 minutes    
    
	//get resource bundle
    private static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication"); 
    private static final String CIRAgreementClass = "cirReplicaSource";
}
