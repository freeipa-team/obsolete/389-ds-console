/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

/**
 * Representation of Directory Server Managed by KingPin
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/24/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class ServerInstance {

    ServerInstance() {
        
    }
    
    ServerInstance(String host, int port) {
        _host = ReplicationTool.fullyQualifyHostName(host);
        _port = port;
    }

    String getHost() {
        return _host;
    }
    
    void setHost(String host) {
        _host = ReplicationTool.fullyQualifyHostName(host.trim());
    }

    int getPort() {
        return _port;
    }
    
    void setPort(int port) {
        _port = port;
    }

    public String toString() {
        return _host+ ":" +_port;
    }
    
    public String getKey() {
        return toString().toLowerCase();
    }

	public static String getKey (String host, int port)
	{
		String result;

		result = host + ":" + port;

		return result.toLowerCase();
	}
    
    public boolean equals(ServerInstance in) {
        if ( (_host.equalsIgnoreCase(in._host)) && (_port == in._port) )
            return true;
        return false;
    }
    
    String _host;
    int _port;
    
}

