/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.event.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;


/**
 * Panel for Replication Agreement
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	11/11/97
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class AgreementPanel extends DSTabbedPanel
    implements IAgreementPanel {   
    
    /*==========================================================
     * constructors
     *==========================================================*/
     
    /**
     * public constructor
     * @param owner resource object owner
     */     
    AgreementPanel( JFrame frame, IDSModel model,
		    ReplicationAgreement agreement) {
        super( model );
        _frame = frame;
        _agreement = agreement;
        _model = model;
        _tabbedPanel = _tabbedPane;
        int nexttab=0;
        
        _infoTab = new AgreementInfoPanel(this, nexttab);
        addTab(_infoTab);
        nexttab += 1;
        
        /* Do not draw the schedule tab for an AD agreement. */
        if (_agreement.getAgreementType() != ReplicationAgreement.AGREEMENT_TYPE_AD)
        {
            _scheduleTab = new AgreementSchedulePanel(this, nexttab);
        addTab(_scheduleTab);
            nexttab += 1;
        }
        _destTab = new AgreementDestinationPanel(this, nexttab);
        addTab(_destTab);
        nexttab += 1; 

        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_MMR ) {
	    if ( _agreement.getSelattrType() == AgreementWizardInfo.SELATTR_EXCLUDE ) {
        		_attrTab = new AgreementAttributePanel(this, nexttab);
		addTab(_attrTab);
                nexttab += 1;
	    }
        }
        _tabbedPane.setSelectedIndex(0);
        _infoTab.invalidate();
        _infoTab.validate();
        _infoTab.repaint(1);
        _tabbedPane.invalidate();
        _tabbedPane.validate();
        _tabbedPane.repaint(1);
    }
    
    /*==========================================================
     * public methods
     *==========================================================*/    
    
    /**
     * Overwrite DSTabbed Panel okCallback function,
     * since the saving model is completely different.
     */
    public void okCallback()
    {
        AgreementWizardInfo info = new AgreementWizardInfo();
        
        if ( _agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD ) {
            
            ActiveDirectoryAgreement _adAgreement = (ActiveDirectoryAgreement)_agreement;
            
            // Obtain the local info.
            ServerInstance server = new ServerInstance( _model.getServerInfo().getHost(),
                    _model.getServerInfo().getPort());
            
            // Set the agreement type to AD.
            info.setAgreementType(info.AD_AGREEMENT);
            
            // Obtain and set the basic remote host information.
            ServerInstance toServer = new ServerInstance(_agreement.getConsumerHost(),
                    _adAgreement.getConsumerPort());
            info.setToServer(toServer);
            
            // Set the bind credentials.
            if ( (_adAgreement.getBindDN() != null) && (_adAgreement.getBindCredentials() != null) ) {
                info.setBindDN(_adAgreement.getBindDN());
                info.setBindPWD(_adAgreement.getBindCredentials());
            } else {
                info.setBindDN("");
                info.setBindPWD("");
            }
            
            // Determine whether SSL is used and what type.
            info.setSSL(_adAgreement.getUseSSL());
            info.setStartTLS(_adAgreement.getUseStartTLS());
            info.setSSLAuth(_adAgreement.getUseSSLAuth());
            
            // Set the description.
            info.setDescription(_adAgreement.getDescription());
            
            // Set the New Win Group Sync
            info.setNewWinGroupSync(_adAgreement.getNewWinGroupSync());
            
            // Set the New Win User Sync
            info.setNewWinUserSync(_adAgreement.getNewWinUserSync());
            
            // Set local replication area.
            info.setDSRepArea(_adAgreement.getDSRepArea());
            
            // Set the replicated subtree.
            info.setSubtree(_adAgreement.getReplicatedSubtree());
            
            // Set the Windows domain name (e.g. magpie.com or MYCORP)
            info.setWindowsDomain(_adAgreement.getWindowsDomain());
            
            // Set the Windows replicated area.
            info.setWindowsRepArea(_adAgreement.getWinRepArea());         
                          
        } else { // Not an AD agreement.         
        //init info
        ServerInstance server =
	    new ServerInstance( _model.getServerInfo().getHost(),
				_model.getServerInfo().getPort());
        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_MMR) {
            info.setAgreementType(info.MMR_AGREEMENT);
        } else {
            info.setAgreementType(info.LEGACYR_AGREEMENT);
        }
	info.setFromServer(server);
	ServerInstance toServer = 
	    new ServerInstance( _agreement.getConsumerHost(),
				_agreement.getConsumerPort());
	info.setToServer(toServer);
        info.setSubtree(_agreement.getReplicatedSubtree());
        info.setSSL(_agreement.getUseSSL());
        if ((_agreement.getBindDN()!= null) && (_agreement.getBindCredentials()!= null)){
            info.setBindDN(_agreement.getBindDN());
            info.setBindPWD(_agreement.getBindCredentials());
        } else {
            info.setBindDN("");
            info.setBindPWD("");
        }	
		info.setSSLAuth( _agreement.getUseSSLAuth());
        if (_agreement.getEntryFilter()!= null)
            info.setFilter(_agreement.getEntryFilter());
        else
            info.setFilter("");
        if (_agreement.getSelectedAttributes()!= null)
            info.setSelectedAttr(_agreement.getSelectedAttributes());
        info.setAttrType(_agreement.getSelattrType());
        if (_agreement.getUpdateSchedule()!= null)
            info.setDate(_agreement.getUpdateSchedule());
        info.setDescription(_agreement.getDescription());
        }
        
        //PRE: at least one tab dirty
        
        //loop through and retrieve info, stop when encounter problem
        //problem page is selected
        for (int ii = 0; ii < _tabbedPane.getTabCount(); ++ii) {
            BlankPanel x= (BlankPanel)_tabbedPane.getComponentAt(ii);
            ITabPanel p = (ITabPanel) _tabbedPane.getComponentAt(ii);
            if (x.isDirty()) {
                if (p.validateEntries()) {
                    p.getUpdateInfo(info);
                } else {
                    //something is wrong: go to that panel
                    _tabbedPane.setSelectedIndex(ii);
                    return;
                }
            }
        }
        
        //saving
        if (saveAgreement(info)) {
            //save successful - clean up dirty flags
            for (int ii = 0; ii < _tabbedPane.getTabCount(); ++ii) {
                ITabPanel p = (ITabPanel) _tabbedPane.getComponentAt(ii);
                p.okCallback();
            }
        }
        
    }
    
    /*==========================================================
     * private methods
     *==========================================================*/    
    
    private boolean saveAgreement(AgreementWizardInfo info) {
        //saving information
        if (_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_MMR) {
            //MMR Agreement
            MMRAgreement mmr_agreement = (MMRAgreement)_agreement;
            mmr_agreement.setConsumerHost(info.getToServer().getHost());
            mmr_agreement.setConsumerPort(info.getToServer().getPort());
            mmr_agreement.setSelattrType(info.getAttrType());
            mmr_agreement.setSelectedAttributes(info.getSelectedAttr());
        } else if(_agreement.getAgreementType() == ReplicationAgreement.AGREEMENT_TYPE_AD){
            ((ActiveDirectoryAgreement)_agreement).setNewWinUserSync(info.getNewWinUserSync());
            ((ActiveDirectoryAgreement)_agreement).setNewWinGroupSync(info.getNewWinGroupSync());
        } else {
            //SIR Agreement
            SIRAgreement sir_agreement = (SIRAgreement)_agreement;
            sir_agreement.setConsumerHost(info.getToServer().getHost());
            sir_agreement.setConsumerPort(info.getToServer().getPort());
            if ( !info.getFilter().equals("") ) {
		sir_agreement.setEntryFilter(info.getFilter());
	    } else {
		sir_agreement.setEntryFilter(null);
	    }
            sir_agreement.setSelattrType(info.getAttrType());
            sir_agreement.setSelectedAttributes(info.getSelectedAttr());
        }

	_agreement.setDescription(info.getDescription());
	_agreement.setReplicatedSubtree(info.getSubtree());
	_agreement.setUpdateSchedule(info.getDate());	
	_agreement.setUseSSL(info.getSSL());
	_agreement.setUseStartTLS(info.getStartTLS());
	_agreement.setUseSSLAuth( info.getSSLAuth());
	_agreement.setUseGSSAPIAuth( info.getGSSAPIAuth());
	_agreement.setUseDigestAuth( info.getDigestAuth());
       
	if (info.getBindDN().equals("")) {
	    _agreement.setBindDN(null);
	} else {
	    _agreement.setBindDN(info.getBindDN());
	}

	/* We consider the password dirty if the user has gone to the Destination Tab (and so _destTab != null and _destTab.isInitialized()) 
	   AND he/she has effectively modified it (_destTab.isDirty()) */
	boolean isPwdDirty = ((_destTab != null) &&
						  (_destTab.isInitialized()) &&
						  _destTab.isPwdDirty());
	if (!isPwdDirty) 
		{
			_agreement.setBindCredentials(null);
		} else {
			_agreement.setBindCredentials(info.getBindPWD());
		}
         
	
	int result = _agreement.writeAgreementToServer();
	if (result != 0){
	    ReplicationTool.displayError(result);
	    return false;
	}
        
        //don't refresh the tree here
        
        return true;
    }
    
    public Object getAgreement() {
	return _agreement;
    }

    public IDSModel getModel() {
	return _model;
    }

    public JTabbedPane getTabbedPane() {
	return _tabbedPanel;
    }

    public JFrame getFrame() {
	return _frame;
    }

    public Object getInfoTab() {
	return _infoTab;
    }

    /*==========================================================
     * variables
     *==========================================================*/    			

    ReplicationAgreement _agreement;
    JFrame _frame;
    IDSModel _model;
    JTabbedPane _tabbedPanel;
    AgreementInfoPanel _infoTab;                    //Usually 0
    AgreementSchedulePanel _scheduleTab;            //Usually 1, not present for AD.
    AgreementDestinationPanel _destTab;             //Usually 2
    AgreementAttributePanel _attrTab;               //Usually 3, LAST TAB, not present for AD.
        
    //get resource bundle
    public static ResourceSet _resource =
	new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
	                
}
