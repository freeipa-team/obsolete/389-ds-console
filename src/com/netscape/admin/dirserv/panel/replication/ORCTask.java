/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel.replication;

import java.util.*;
import java.text.*;
import java.net.*;
import java.io.*;
import javax.swing.*;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;
import com.netscape.admin.dirserv.*;

/* This file implements ORCTask class - the class that performs its *
 * task in conjunction with progress dialog (SimpleProgressDialog)  *
 * version 1.0                                                      *
 * author olga                                                      *
 */

public class ORCTask extends SimpleProgressTask implements Stoppable
{
	public ORCTask (ReplicationAgreement agreement)
	{
		m_agreement = agreement;
		m_status	= STATUS_OK;
	}

	public void run ()
	{
		String attrname;
		m_status = STATUS_OK;

        if (m_agreement.getAgreementType() == 
                   ReplicationAgreement.AGREEMENT_TYPE_LEGACYR) {
            attrname = "replicaBeginORC";
        } else if (m_agreement.getAgreementType() == 
                   ReplicationAgreement.AGREEMENT_TYPE_MMR || m_agreement.getAgreementType() == 
                   ReplicationAgreement.AGREEMENT_TYPE_AD) {
            attrname = ReplicationTool.REPLICA_REFRESH_ATTR;
        } else {            
			return;
        }

        LDAPModificationSet mods = new LDAPModificationSet();
        LDAPAttribute attr = new LDAPAttribute(attrname, "start");
        mods.add(LDAPModification.REPLACE, attr);
        try {
			/* send start replication request */
            m_agreement.updateEntry(mods);
			
            /* sleep for a moment to let status accrue */
            Thread.sleep(3000);
			/* The consumer initialization attribute was correctly added.
			   We check until we have the first update result... */
			m_firstUpdateStatus = 0;
			boolean isUpdateStatusReceived = false;
			boolean isInitializationFinished = false;
			String firstUpdateStatus = null;
			ResourceSet resource = new ResourceSet("com.netscape.admin.dirserv.panel.replication.replication");
			while (!isUpdateStatusReceived && 
				   !isInitializationFinished) {
				if (m_dlg.isCancelled()) {
					isInitializationFinished = true;
				} else {
					m_agreement.updateReplicaStatus();
					if (m_agreement.getConsumerInitializationInProgress().equals(
																				 resource.getString("replication-agreement",
																									"initconsumer-inprogress-false-value"))) {
						isInitializationFinished = true;
					}
					firstUpdateStatus = m_agreement.getConsumerInitializationStatus();				
					if ((firstUpdateStatus  != null) &&
						(firstUpdateStatus.length() > 0) &&
						!firstUpdateStatus.equals(resource.getString("replication-info-status-code", "unknown"))) {					
						isUpdateStatusReceived = true;
						StringTokenizer st = new StringTokenizer(firstUpdateStatus, " ");
						String code = st.nextToken();
						if ((code != null) &&
							(code.length() > 0)) {
							try {
								m_firstUpdateStatus = Integer.parseInt(code);
								/* If the first update status is not correct, we look for the error message */
								if (m_firstUpdateStatus != 0) {
									m_firstUpdateMessage = firstUpdateStatus;
								}
							} catch (Exception ex) {
								Debug.println("ORCTask.run() exception parsing "+code +": "+ex);
							}
						}
					}					
				}
			}						
        } catch (LDAPException me) {
			Debug.println ("Exception occured during ORC: " + me);
            m_status = me.getLDAPResultCode();
			m_firstUpdateMessage = me.getLDAPErrorMessage();
			m_dlg.stop ();
        } catch (InterruptedException e) {
            Debug.println ("Sleep interrupted: Exception occured during ORC: " + e);
            m_status = -1;
        }

		m_dlg.stop ();

		Debug.println ("ORCTask: all done");
	}

	public void stop ()
	{
		String attrname;

        if (m_agreement.getAgreementType() == 
                   ReplicationAgreement.AGREEMENT_TYPE_LEGACYR) {
            attrname = "replicaBeginORC";
        } else if (m_agreement.getAgreementType() == 
                   ReplicationAgreement.AGREEMENT_TYPE_MMR || m_agreement.getAgreementType() == 
                   ReplicationAgreement.AGREEMENT_TYPE_AD) {
            attrname = ReplicationTool.REPLICA_REFRESH_ATTR;
        } else {
            m_status= STATUS_OK;
			return; 
        }

        LDAPModificationSet mods = new LDAPModificationSet();
        LDAPAttribute attr = new LDAPAttribute(attrname, "cancel");
        mods.add(LDAPModification.REPLACE, attr);
        try {
            m_agreement.updateEntry(mods);
            m_status = STATUS_OK;
        } catch (LDAPException me) {
			Debug.println ("Exception occured while stopping ORC: " + me);

            m_status = me.getLDAPResultCode();
			m_firstUpdateMessage = me.getLDAPErrorMessage();
        }
	}

	public int getStatus ()
	{
		return m_status;
	}

	public int getFirstUpdateStatus ()
	{
		return m_firstUpdateStatus;
	}

	public String getFirstUpdateMessage ()
	{
		return m_firstUpdateMessage;
	}


	public static final int STATUS_OK     = 0;

	private ReplicationAgreement m_agreement;
	private int                  m_status;
	private int m_firstUpdateStatus;
	private String m_firstUpdateMessage;
}
