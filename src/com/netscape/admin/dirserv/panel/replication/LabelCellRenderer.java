/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.panel.replication;

import java.awt.*;
import java.lang.*;
import javax.swing.table.*;
import javax.swing.*;
import java.io.Serializable;

/**
 * Class that will render label correctly in table
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	1/10/98
 * @see     com.netscape.admin.dirserv.panel.replication
 */
public class LabelCellRenderer 
    implements TableCellRenderer, Serializable 
{
    
    /*==========================================================
     * constructors
     *==========================================================*/
     
    public LabelCellRenderer(JLabel x) {
        component = x;
	    x.setOpaque(true);
        value = new ValueProperty() {
            public void setValue(Object x) {
		        if (x == null)
		        x = "";
                super.setValue(x);
		        if (x instanceof Icon)
		            ((JLabel)component).setIcon((Icon)x);
		        if (x instanceof String)
		            ((JLabel)component).setText(x.toString());
		        if (x instanceof JLabel) {
		            ((JLabel)component).setIcon(((JLabel)x).getIcon());
		            ((JLabel)component).setText(((JLabel)x).getText());
		            ((JLabel)component).setForeground(((JLabel)x).getForeground());
                    ((JLabel)component).setBackground(((JLabel)x).getBackground());                    
		            ((JLabel)component).setHorizontalAlignment(((JLabel)x).getHorizontalAlignment());
		            ((JLabel)component).setToolTipText(((JLabel)x).getToolTipText());
		        }
            }
        };
    }
    
	/*==========================================================
	 * public methods
     *==========================================================*/    
    
    public void setToolTipText(String text) {
	if (component instanceof JComponent)
	    ((JComponent)component).setToolTipText(text);
    }

    public Component getComponent() {
	return component;
    }

//
// Implementing TableCellRenderer
//

    public Component getTableCellRendererComponent(JTable table, Object value,
						   boolean isSelected, boolean hasFocus,
						   int row, int column) {
						    
        this.value.setValue(value);
        return component;
    }


    protected class ValueProperty implements Serializable {
        protected Object value;

        public void setValue(Object x) {
            this.value = x;
        }
    }
    
    /*==========================================================
     * variables
     *==========================================================*/
    protected JComponent component;
    protected ValueProperty value;
}


