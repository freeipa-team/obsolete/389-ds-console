/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.net.MalformedURLException;
import javax.swing.*;
import netscape.ldap.LDAPUrl;
import netscape.ldap.LDAPException;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.MultilineLabel;
import com.netscape.management.client.components.ErrorDialog;
import com.netscape.management.nmclf.SuiConstants;
import com.netscape.admin.dirserv.DSUtil;


public class NonResolvedReferralDialog extends AbstractDialog
                                       implements ActionListener {

	public NonResolvedReferralDialog(JFrame frame, LDAPUrl displayedURL, 
	                                 String errorUrl, Exception errorException) {
		super( frame, "", true, OK | CANCEL);
		_frame = frame;
		_displayedURL = displayedURL;
		_errorURL = errorUrl;
		_errorException = errorException;
		_bShowError = new JButton(_resource.getString(_section, "show-error-label"));
		_bShowError.addActionListener(this);
		init();
	}
	
	public void packAndShow() {
		pack();
		show();
	}


	protected void init() {
		setTitle(_resource.getString(_section, "title-label"));
		JPanel panel = new JPanel(new GridBagLayout());
		setComponent(panel);

        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
		gbc.insets     = new Insets(0, 0, DIFFERENT_COMPONENT_SPACE, COMPONENT_SPACE);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;

		JComponent l = new MultilineLabel(_resource.getString(_section, "main-message-label"), 3, 30);
		panel.add(l, gbc);

		gbc.gridx++;
        gbc.weightx = 0;
		gbc.insets.right = 0;
		panel.add(_bShowError, gbc);
		
		l = new MultilineLabel(_resource.getString(_section, "first-alternative-label"), 2, 35);
		gbc.gridx = 0;
		gbc.gridy++;
        gbc.gridwidth  = gbc.REMAINDER;
		panel.add(l, gbc);
		
		String[] arg = new String[] {
			_displayedURL.getDN(), 
			_displayedURL.getHost() + ":" + String.valueOf(_displayedURL.getPort())
		};
		l = new MultilineLabel(_resource.getString(_section, "second-alternative-label", arg), 3, 35);
		gbc.gridy++;
		panel.add(l, gbc);
		
		setOKButtonText(_resource.getString("general", "Continue-label"));
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == _bShowError) {
			String title = _resource.getString(_section, "show-error-title");
			String errorText, tipText, detailText;
			if (_errorException instanceof MalformedURLException) {
				errorText = _resource.getString(_section, "show-error-invalid-url-text");
				detailText = null;
			}
			else if (_errorException instanceof LDAPException) {
				String ldapError = ((LDAPException)_errorException).errorCodeToString();
				errorText = _resource.getString(_section, "show-error-ldap-text", ldapError);
				detailText = _errorException.toString();
			}
			else {
				errorText = _resource.getString(_section, "show-error-unknown-text");
				detailText = _errorException.toString();
			}
			tipText = _resource.getString(_section, "show-error-tip-text", _errorURL);
			ErrorDialog dlg = new ErrorDialog(_frame, title, errorText, tipText, detailText,
			                                  ErrorDialog.DEFAULT, ErrorDialog.DEFAULT);

			dlg.show();
		}
	}


	JFrame _frame;
	LDAPUrl _displayedURL;
	String _errorURL;
	Exception _errorException;
	JButton _bShowError;

	ResourceSet _resource = DSUtil._resource;
	String _section = "NonResolvedReferralDialog";
}
