/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;
import java.util.Vector;
import java.util.Hashtable;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.management.client.console.ConsoleInfo;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.panel.UIFactory;


public class BackendPanel extends JPanel {
	
	public BackendPanel(ConsoleInfo consoleInfo, JLabel label) {
		
		_ldc = consoleInfo.getLDAPConnection();
		_label = label;
		
		init();
	}

	public BackendPanel(ConsoleInfo consoleInfo) {
		this(consoleInfo, new JLabel(""));
	}

	public BackendPanel(ConsoleInfo consoleInfo, String section, String keyword) {
		this(consoleInfo, UIFactory.makeJLabel(section, keyword));
	}

	public BackendPanel(LDAPConnection ldc, JLabel label) {
                _ldc = ldc;
                _label = label;
                init();
	}
	public BackendPanel(LDAPConnection ldc) {
		this(ldc, new JLabel(""));
	}
	
	public BackendPanel(LDAPConnection ldc, String section, String
						keyword) {
		this(ldc, UIFactory.makeJLabel(section, keyword));
	}


	protected void init() {
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = getGBC();
		
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 0.0;
		gbc.anchor = gbc.EAST;
		gbc.gridwidth = gbc.RELATIVE;
		
		if (_label != null) {
			add(_label, gbc);
		}
				
		
		_theChoice = UIFactory.makeJComboBox(null, null);
		_theChoice.setEditable(false);
		gbc.weightx = 1.0;
		gbc.gridwidth = gbc.REMAINDER;
		add(_theChoice, gbc);
		
		try {
			// returns either a vector of vectors of LDAPEntry objects or
			// a vector of LDAPEntry objects
			boolean getMappingTreeNode = false;
			String LDAPFilter = null;
			Vector v = DSUtil.getLDBMInstanceList(_ldc, getMappingTreeNode,
												  LDAPFilter);
			if (v != null && v.size() > 0) {
				for (Enumeration e = v.elements(); e.hasMoreElements();) {
					Object o = e.nextElement();
					LDAPEntry lde = null;
					if (o instanceof Vector) {
						Vector vv = (Vector)o;
						lde = (LDAPEntry)vv.elementAt(0);
					} else { // must be an LDAPEntry
						lde = (LDAPEntry)o;
					}
					String suffix = DSUtil.getAttrValue(lde, SUFFIX_ATTR);
					String backend = DSUtil.getAttrValue(lde, BACKEND_ATTR);
					/* Here we make the choice of what we show: the instance name, the
					   suffix, a combination of both...*/
					//_theChoice.addItem(backendString+SEPARATOR+suffixString);
					/* In this implementation we show just the suffix*/
					_theChoice.addItem(suffix);
					if (_htSuffixAndBackends == null) {
						_htSuffixAndBackends = new Hashtable();
					}
					Hashtable ht = new Hashtable();
					ht.put(SUFFIX_ATTR, suffix);
					ht.put(BACKEND_ATTR, backend);
					String key = ""+(_theChoice.getItemCount()-1);
					_htSuffixAndBackends.put(key, ht);
				}
			}
		} catch (LDAPException e) {
			Debug.println( "BackendPanel.init: could not load backend instances " +
						   "from server " + DSUtil.format(_ldc) + " exception: " +
						   e);
			e.printStackTrace();
		}
	}
	
    protected GridBagConstraints getGBC() {
        resetGBC();
		return _gbc;
	}

	protected void resetGBC() {
		if ( _gbc == null )
			_gbc = new GridBagConstraints();
  		_gbc.gridwidth  = 1;
  		_gbc.gridheight = 1;
  		_gbc.fill       = _gbc.NONE;
  		_gbc.ipady      = 0;
  		_gbc.weightx    = 1.0;
  		_gbc.weighty    = 0.0;
  		_gbc.insets     = getComponentInsets();
		_gbc.insets.right = _gbc.insets.bottom = 0;
		_gbc.anchor     = _gbc.WEST;
   		_gbc.gridx      = _gbc.RELATIVE;
      	_gbc.gridy      = _gbc.RELATIVE;
	}


	 protected Insets getComponentInsets() {
		return UIFactory.getComponentInsets();
	}

	public String getBackend() {
		if ((_htSuffixAndBackends != null) && (_theChoice != null)) {
			String key = String.valueOf(_theChoice.getSelectedIndex());
			Hashtable ht = (Hashtable)_htSuffixAndBackends.get(key);
			return (String)ht.get(BACKEND_ATTR);	
		}
		return null;
	}

	public String getSuffix() {
		if ((_htSuffixAndBackends != null) && (_theChoice != null)) {
			String key = String.valueOf(_theChoice.getSelectedIndex());
			Hashtable ht = (Hashtable)_htSuffixAndBackends.get(key);
			return (String)ht.get(SUFFIX_ATTR);
		}
		return null;
	}

	public boolean setSelectedBackend(String backend) {
		if (_htSuffixAndBackends != null) {
			for (int i=0 ; i<_htSuffixAndBackends.size(); i++) {
				String key = String.valueOf(i);
				String currentBackend = (String)(((Hashtable)(_htSuffixAndBackends.get(key))).get(BACKEND_ATTR));
				if (currentBackend.equals(backend.trim())) {
					_theChoice.setSelectedIndex(i);
					return true;
				}
			}
		}
		return false;
	}
				
		

	public void setEnabled(boolean state) {
		if (_theChoice != null) {
			_theChoice.setEnabled(state);
		}

		if (_label != null) {
			_label.setEnabled(state);
		}
		super.setEnabled(state);
	}

	public Enumeration getAllSuffixes() {
		Vector allSuffixes = new Vector();
		if (_htSuffixAndBackends != null) {
			for (int i=0; i<_htSuffixAndBackends.size(); i++) {
				String key = String.valueOf(i);
				allSuffixes.add(((Hashtable)(_htSuffixAndBackends.get(key))).get(SUFFIX_ATTR));			
			}
		}
		return allSuffixes.elements();
	}	

	public Enumeration getAllBackends() {
		Vector allBackends = new Vector();
		if (_htSuffixAndBackends != null) {
			for (int i=0; i<_htSuffixAndBackends.size(); i++) {
				String key = String.valueOf(i);
				allBackends.add(((Hashtable)(_htSuffixAndBackends.get(key))).get(BACKEND_ATTR));			
			}
		}
		return allBackends.elements();
	}		

	protected JComboBox _theChoice;
	protected LDAPConnection _ldc;
	protected GridBagConstraints _gbc;
	protected JLabel _label;
	protected final String  SEPARATOR = " / ";
	protected Hashtable _htSuffixAndBackends;
	protected final String SUFFIX_ATTR = "nsslapd-suffix";
	protected final String BACKEND_ATTR = "cn";
}
