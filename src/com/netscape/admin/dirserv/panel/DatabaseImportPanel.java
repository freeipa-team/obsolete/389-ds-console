/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
package com.netscape.admin.dirserv.panel;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.io.FileInputStream;
import java.net.URL;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.table.*;
import javax.swing.border.EmptyBorder;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.LDAPUtil;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.components.Table;
import com.netscape.admin.dirserv.*;
import netscape.ldap.*;
import netscape.ldap.util.*;
import java.text.SimpleDateFormat;
import com.netscape.admin.dirserv.task.LDAPImport;

public class DatabaseImportPanel extends FilePanel {

	public DatabaseImportPanel(IDSModel model, String backend) {
		super(model, "import");
		if (backend != null) {
			_helpToken = "configuration-database-initialize-backend-dbox-help";
		} else {
			_helpToken = "configuration-database-import-fastwire-dbox-help";
		}
		_backend = backend;
	}

	public DatabaseImportPanel(IDSModel model) {
		this(model, null);
	}

	public void init() {
		JPanel grid = _myPanel;
        grid.setLayout( new GridBagLayout() );
		createFileArea( grid );

		if (_backend == null) {
			/* We add a separator to make it clear that the radio buttons with message 'on server machine' and 'on console machine'
			   refer to the LDIF file */
			if ( !isLocal() && 
				 !_onlyRemote &&
				 !_onlyLocal) {
				createSeparator(grid);
			}
			createBackendSelectionPanel(grid);
		}
		addBottomGlue();
		setLocalState(_rbLocal.isSelected());
		getAbstractDialog().setFocusComponent(_tfExport);
		getAbstractDialog().getAccessibleContext().setAccessibleDescription(_resource.getString(_section,
																								"initializedb-description"));
	}


	/* This function is used to enable/disable the different options, considering if the file to import is local to the console */
	/* or not, and if the console is local to the server or not */
    protected void setLocalState( boolean state ) {
		if ( isLocal() ) {
			return;
		}
		super.setLocalState( state );
    }
   
    
    protected void checkOkay() {
	    String path = getFilename();	   
		AbstractDialog dlg = getAbstractDialog();
		if ( dlg != null ) {
			boolean state = ( (path != null) &&
							  (path.length() > 0) &&
							  isBackendSelected());
			dlg.setOKButtonEnabled( state );
		} else {
		}
	}

    public void changedUpdate(DocumentEvent e) {
		super.changedUpdate( e );
		checkOkay();
	}

    public void removeUpdate(DocumentEvent e) {
		super.removeUpdate( e );
		checkOkay();
	}

    public void insertUpdate(DocumentEvent e) {
		super.insertUpdate( e );
		checkOkay();
	}

    /**
     *  handle incoming event
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
        if ( e.getSource().equals(_bExport) ) {
    	    String file = getFilename();
			String[] extensions = { "ldif" };
			String[] descriptions = { _resource.getString( "filefilter", "ldif-label" ) };
			String defaultPath = getDefaultPath(getModel());
			if ( (file == null) || (file.trim().length() < 1) ) {
				file = DSFileDialog.getFileName(false, extensions,
												descriptions, (Component)this, "*.ldif", defaultPath);
			} else {
				File theFile = new File(file);
				if (theFile.isAbsolute()) {
					file = DSFileDialog.getFileName(file, false, extensions,
													descriptions, (Component)this);
				} else {
					file = DSFileDialog.getFileName(false, extensions,
													descriptions, (Component)this, file, defaultPath);
				}
			}
			if (file != null)
				setFilename(file);         
		} else if ( e.getSource().equals( _rbLocal ) ) {
            setLocalState( true );
        } else if ( e.getSource().equals( _rbRemote ) ) {
            setLocalState( false );
        } else {
			super.actionPerformed(e);
		}
		 
		revalidate();
		repaint();		
    }
  
	
    public void okCallback() {
    	boolean status = false;
		
		boolean fileInConsole = _rbLocal.isSelected() || isLocal();	    
		
	    String importPath = getFilename();
		/*We only test if the file name is not empty*/
		if (importPath.trim().equals("")) {
			if (!validateFilename()) {
				return;
			}
		}
		

		String base = null;
		ConsoleInfo info = getModel().getServerInfo();
        LDAPConnection ldc = info.getLDAPConnection();
		
		if (fileInConsole) {
			/* If no path was provided, we try first with the path of the DSFileDialog 
			   and then with the default path of the console.  The user has to give the full path
			   in the case the file is not in the console's machine */
			File file = new File( importPath );
			if ( !file.isAbsolute() ) {
				if (DSFileDialog.getPath() != null) {
					importPath = DSFileDialog.getPath() + importPath;
				} else {
					importPath = getDefaultPath(getModel()) + importPath;
				}
			}
			File auxFile = new File(importPath);
			importPath = auxFile.getAbsolutePath();
			
			/* Check if the file exists */			
			if ( !DSUtil.fileExists(auxFile) ) {					
				DSUtil.showErrorDialog( getModel().getFrame(),
										"file-does-not-exist", importPath, _section );
				return;
			}
		}
		if (isLocal() || _rbRemote.isSelected()) {				
			/* We are going to call to the import from the server, we don't know if it has the rights to access to the file.
			   That's why we show a warning */				
			Hashtable attributes = new Hashtable();
			attributes.put(LDAPImport.FILENAME, new LDAPAttribute(LDAPImport.FILENAME, importPath));
			if (_backend != null) {
					attributes.put(LDAPImport.INSTANCE, new LDAPAttribute(LDAPImport.INSTANCE, _backend));
					if ( requiresConfirmation(GlobalConstants.PREFERENCES_CONFIRM_OVERWRITE_DATABASE) ) {
						if (_backend.equalsIgnoreCase(CONFIG_BACKEND.trim())) {
							int result = DSUtil.showConfirmationDialog(getModel().getFrame(), "remoteoverwritewarning-configbackend", 
																	   "", _section);
							if( JOptionPane.OK_OPTION != result) {
								return;
							}						
						} else {
							int result = DSUtil.showConfirmationDialog(getModel().getFrame(), "remoteoverwritewarning", 
																	   "", _section);
							if( JOptionPane.OK_OPTION != result) {
								return;
							}
						}
					}					
			} else {
				String[] backends = getSelectedBackendList();
				attributes.put(LDAPImport.INSTANCE, new LDAPAttribute(LDAPImport.INSTANCE, backends));
				
				if ( requiresConfirmation(GlobalConstants.PREFERENCES_CONFIRM_OVERWRITE_DATABASE) ) {
					int result = DSUtil.showConfirmationDialog(getModel().getFrame(), "remoteoverwritewarning-confignotimported", 
															   "", _section);
					if( JOptionPane.OK_OPTION != result) {
						return;
					}
				}
			}
			/* No state to preserve */
			clearDirtyFlag();				
			hideDialog();
			
			LDAPImport task = new LDAPImport(getModel(), attributes);				
		} else {
			/* We are going to do a Bulk Import (a.k.a. over the wire import.
			   Check if the file can be read */
			File auxFile = new File( importPath );			
			try {
				FileInputStream test = new FileInputStream(auxFile);
			} catch (Exception e) {
				DSUtil.showErrorDialog( getModel().getFrame(),
										"cantopen", importPath, _section );
				return;
			}
			String[] backends;
			if (_backend != null) {
				if ( requiresConfirmation(GlobalConstants.PREFERENCES_CONFIRM_OVERWRITE_DATABASE) ) {
					if (_backend.equalsIgnoreCase(CONFIG_BACKEND.trim())) {
						int result = DSUtil.showConfirmationDialog(getModel().getFrame(), "localoverwritewarning-configbackend", 
																   "", _section);
						if( JOptionPane.OK_OPTION != result) {
							return;
						}
					} else {
						int result = DSUtil.showConfirmationDialog(getModel().getFrame(), "localoverwritewarning", 
																   "", _section);
						if( JOptionPane.OK_OPTION != result) {
							return;
						}
					}
				}
				backends = new String[1];
				backends[0] = _backend;
			} else {					
				if ( requiresConfirmation(GlobalConstants.PREFERENCES_CONFIRM_OVERWRITE_DATABASE) ) {
					int result = DSUtil.showConfirmationDialog(getModel().getFrame(), "localoverwritewarning-confignotimported", 
															   "", _section);
					if( JOptionPane.OK_OPTION != result) {
						return;
					}
				}
				backends = getSelectedBackendList();
			}
			/* No state to preserve */
			clearDirtyFlag();
			
			hideDialog();
			bulkImport(backends, importPath, null);				
		}
		
		
		if ( status ) {
			getModel().contentChanged();
		}		
    }
	
    public void resetCallback() {
		/* No state to preserve */
		clearDirtyFlag();
		hideDialog();
    }

	private int bulkImport(String backend, String importPath, String rejects) {
		String[] backends = {backend};
		return bulkImport(backends, importPath, rejects);
	}

	private int bulkImport(String[] backends, String importPath, String rejects) {
		GenericProgressDialog dlg = new GenericProgressDialog(getModel().getFrame(),
															  true,
															  GenericProgressDialog.TEXT_FIELD_AND_CANCEL_BUTTON_OPTION,
															  getTitle());		
		BulkImport bulk = new BulkImport(backends,
										importPath,
										rejects,
										getModel(),
										dlg);
		try {
			Thread th = new Thread(bulk);
			th.start();
			dlg.packAndShow();
		} catch ( Exception e ) {
			Debug.println("DatabaseImportPanel.bulkImport: " +
						  e );
			e.printStackTrace();	
		}
		return bulk.getResult();
	}

	/**
	  * Returns true if at least the user gave a backend to initialize. False otherwise.
	  */
	protected boolean isBackendSelected() {
		boolean isBackendSelected = false;
		if (_backend == null) {
			if ((_model != null) && (_backends != null)) {				
				for (int i=0; (i < _backends.length) && !isBackendSelected; i++) {
					/* The second column is the column of the checkboxes: this has to be in sync with the creation
					   of the table */
					if (((Boolean)(_model.getValueAt(i,2))).booleanValue()) {
						isBackendSelected = true;
					}
				}
			}
		} else {
			isBackendSelected = true;
		}
		return isBackendSelected;
	}

	/**
	  * Returns the selected Backend List 
	  */
	protected String[] getSelectedBackendList() {
		String[] selectedBackendList = null;
		
		if ((_model != null) && 
			(_backends != null)) {
			Vector vSelectedBackendList = new Vector();
			for (int i=0; i < _backends.length; i++) {
				/* The second column is the column of the checkboxes: this has to be in sync with the creation
				   of the table */
				if (((Boolean)(_model.getValueAt(i,2))).booleanValue()) {
					vSelectedBackendList.addElement(_backends[i]);
				}
			}
			selectedBackendList = new String[vSelectedBackendList.size()];
			vSelectedBackendList.copyInto(selectedBackendList);			
			Debug.println("DatabaseImportPanel.getSelectedBackendList(): "+vSelectedBackendList);
		}		
		return selectedBackendList;
	}

	/**
	  * Creates the zone with a list of the backends we want to initialize
	  */
	protected void createBackendSelectionPanel(JPanel grid) {
        GridBagConstraints gbc = getGBC();
		LDAPConnection ldc = getModel().getServerInfo().getLDAPConnection();
		_backends = MappingUtils.getBackendList(ldc, MappingUtils.LDBM);
		if (_backends != null) {			
			String[] suffixes = MappingUtils.getSuffixesForBackends(ldc, _backends);			
			Vector dataVector = new Vector();
			if ((suffixes != null) &&
				(suffixes.length == _backends.length)) {	
				for (int i=0; i<_backends.length; i++) {
					Vector rowData = new Vector();		
					Boolean bool = new Boolean(false);					
					rowData.addElement(_backends[i]);					
					rowData.addElement(suffixes[i]);
					rowData.addElement(bool);
					dataVector.addElement(rowData);
				}				
			} else {
				for (int i=0; i<_backends.length; i++) {
					Vector rowData = new Vector();
					Boolean bool = new Boolean(false);					
					rowData.addElement(_backends[i]);					
					rowData.addElement("");
					rowData.addElement(bool);
					dataVector.addElement(rowData);
				}
			}
			Vector columnNames = new Vector();
			columnNames.addElement(_resource.getString(_section, 
													   "initialize-multiple-backends-table-dbName-label"));
			columnNames.addElement(_resource.getString(_section, 
													   "initialize-multiple-backends-table-dbSuffix-label"));
			columnNames.addElement(_resource.getString(_section, 
													   "initialize-multiple-backends-table-dbInitialize-label"));
			
			_model = new CustomTableModel(dataVector, columnNames);
			Table backendToInitializeTable = new Table(_model);			
			backendToInitializeTable.setToolTipText(_resource.getString(_section,
																		"initialize-multiple-backends-table-description"));
			for (int i=0; i<columnNames.size(); i++) {
				TableColumn tcol = backendToInitializeTable.getColumn(columnNames.elementAt(i));
				tcol.setPreferredWidth(TABLE_WIDTH[i]);
				if (i == 2) {							
					tcol.setCellRenderer(  new CustomCheckBoxTableCellRenderer() );
					tcol.setHeaderRenderer( new CenterAlignedHeaderRenderer ());					
				}
			}			
			
			backendToInitializeTable.setColumnSelectionAllowed(false);
			backendToInitializeTable.getSelectionModel().addListSelectionListener( this );
			backendToInitializeTable.setAutoResizeMode( JTable.AUTO_RESIZE_NEXT_COLUMN );

			int width = backendToInitializeTable.getPreferredScrollableViewportSize().width;
			int numberOfRows = Math.min(TABLE_ROWS, _backends.length);
			int height = TABLE_ROWS * (backendToInitializeTable.getRowMargin() + backendToInitializeTable.getRowHeight());
			Dimension preferredDimension = new Dimension (width, height);
			backendToInitializeTable.setPreferredScrollableViewportSize(preferredDimension);
			JScrollPane scroll = new JScrollPane(backendToInitializeTable);
			scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

			gbc.gridwidth = gbc.REMAINDER;
			gbc.fill = gbc.HORIZONTAL;
			gbc.anchor = gbc.NORTHWEST;
			gbc.weightx = 1.0;	
			grid.add(scroll, gbc);		
		} else {	
			gbc.gridwidth = gbc.REMAINDER;
			gbc.fill = gbc.HORIZONTAL;
			gbc.anchor = gbc.NORTHWEST;
			gbc.weightx = 1.0;	
			grid.add(makeMultiLineLabel(2, 50, _resource.getString(_section, 
																   "initialize-multiple-backends-list-not-found-label")),
					 gbc);
		}
	}

	public String getTitle() {
		String title;
		if (_backend != null) {
			String[] args = {_backend};						
			title = _resource.getString(_section, "initializebackend-title", args);
		} else {
			title = _resource.getString(_section, "initialize-multiple-backends-title");
		}
		return title;
	}

	/**
	  * A variation of the DefaultTableModel that makes the columns of the checkboxes to be editable.
	  */
	protected class CustomTableModel extends DefaultTableModel {
		public CustomTableModel(Vector data, Vector columnNames) {
			super(data, columnNames);
		}
		public boolean isCellEditable(int row, int column) {
			/**
			  * This should be in sync with the construction of the table that contains the list of backends to initialize
			  */
			boolean isCellEditable = false;
			if (column == 2) {
				isCellEditable = true;
			}
			return isCellEditable;
		}
		
		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}
	}

	/**
	  * A variation of the CheckBoxTableCellRenderer that makes a call to checkOkay() to correclty update the state
	  * of the OK button
	  */
	protected class CustomCheckBoxTableCellRenderer extends CheckBoxTableCellRenderer {
		public CustomCheckBoxTableCellRenderer() {
			_oldValues = new Vector();
			_oldValues.setSize(_backends.length);
		}
		public Component getTableCellRendererComponent(
													   JTable table, Object value,
													   boolean isSelected, boolean hasFocus, int row, int column) {
			/* Done to minimize calls to checkOkay */
			
			if ((value != null) &&
				!value.equals(_oldValues.elementAt(row))) {
				Debug.println("CheckBoxTableCellRenderer.getTableCellRendererComponent(): checkOkay()");
				checkOkay();				
				_oldValues.insertElementAt(value, row);			
			}
			
			return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);			
		}
		private Vector _oldValues;
	}

	private String _backend;
	private String[] _backends;
	protected CustomTableModel _model;

	private static final String _section = "import";
	private final int TABLE_ROWS = 5;
	private final int[] TABLE_WIDTH = {70, 100, 50};
	public static final int ERROR = -1;
	public static final int SUCCESS = 0;
	
	
	private final String CONFIG_BACKEND= "NetscapeRoot";
}
