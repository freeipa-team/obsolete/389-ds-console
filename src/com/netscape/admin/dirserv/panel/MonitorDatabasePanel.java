/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.Hashtable;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import netscape.ldap.*;

/**
 * Panel for Directory Server resource page
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	9/15/97
 * @see     com.netscape.admin.dirserv
 */
public class MonitorDatabasePanel extends MonitorBasePanel {
    /**
     * Standard panel constructor.
     *
     * @param model The resource model for this panel.
     */
    public MonitorDatabasePanel( IDSModel model ) {
        super(model, _section);
		_helpToken = "status-perfcounters-database-help";
		_entryName = null;
    }

    /**
     * Standard panel constructor.
     *
     * @param model The resource model for this panel.
     * @param dn DN of the entry containing config info.
     */
    public MonitorDatabasePanel( IDSModel model, String dn ) {
        super(model, _section);
	_helpToken = "status-perfcounters-database-help";
	_entryName = dn;
	reload();
	/* If we were assigned a configuration at construction time,
	   use it to set the title of this tab (the default is that
	   the title is just "Database") */
	setTitle( getAttrValue( "database" ) );
    }

    /**
     * Initialize, called once when the panel is first selected.
     */
    public void init() {
        _myPanel.setLayout(new GridBagLayout());
	if ( _entryName == null ) reload();

	if( _entry == null) return;
        createVersionArea( _myPanel );

        JPanel panel = createRefreshArea();
	GridBagConstraints gbc = getGBC();
	gbc.gridx = 0;
	int different = UIFactory.getDifferentSpace();
	gbc.insets = new Insets(0,different,0,different);
        gbc.gridwidth = gbc.REMAINDER;
	_myPanel.add( panel, gbc );

        createSummaryArea( _myPanel );

	_hTables = new Hashtable();
        createFileAreas( _myPanel );

	validate();
    }

    /**
     * Refetch data from the Directory.
     */
    private boolean reload() {
	if ( _ldc == null )
	    _ldc = getModel().getServerInfo().getLDAPConnection();
	if ( _entryName == null ) {
	    try {
		LDAPEntry entry = _ldc.read("cn=monitor"); // get monitor data
		if( entry != null ) {
		    _entryName = DSUtil.getAttrValue( entry, 
						      "backendmonitordn" );
		} else {
		    return false;
		}
	    } catch (LDAPException e) {
		Debug.println("ERROR: LDAPConnection.read:" + e);
		return false;
	    }
	}
	try {
	    _entry = _ldc.read( _entryName ); // get monitor data
        } catch (LDAPException e) {
            Debug.println("ERROR: LDAPConnection.read:" + e);
	    return false;
        }
	return true;
    }

    /**
     * Refetch data from the directory and update the panel.
     */
    public boolean refresh() {
	/* Get new data from the server */
	if (!reload()) return false;
	if ( _summaryTable != null ){
	    ((PerfTableModel)_summaryTable.getModel()).updEntry( _entry );
	}
	updateFileAreas();	
	/* Now pass the changes on to the UI */
	return super.refresh();
    }

    private void updateFileAreas(){

	String[] dNames = { "dbfilecachehit",
			    "dbfilecachemiss",
			    // "dbfilemap",
			    // "dbfilepagesize",
			    // "dbfilepagecreate",
			    "dbfilepagein",
			    "dbfilepageout" };

	int NROWS = dNames.length;
	String[] labels = new String[NROWS];
	for( int i = 0; i < NROWS; i++ )
	    labels[i] = _resource.getString( _section,
					     "fileTable-" +
					     dNames[i] + "-label");	
	int NCOLS = 2;
	String[] headers = new String[NCOLS];
	for( int i = 0; i < NCOLS; i++ )
	    headers[i] = _resource.getString( _section,
					      "fileHeader" +
					      i + "-label" );
	int[] widths = { 250, 100 };
	
	int nFiles = 0;
	LDAPAttributeSet attrSet = _entry.getAttributeSet();
	
	for(int i=0; i<attrSet.size(); i++) {
	    LDAPAttribute attrTmp =  attrSet.elementAt(i);
	    String[] dataNames = new String[NROWS];
	    if(attrTmp != null) {
		String attrName = attrTmp.getName();
		if( attrName.startsWith( "dbfilename-" )){
		    // get the filename number
		    String num = attrName.substring(11);
		    // check if it already exit
		    String attrVal = getAttrValue( attrName );
		    // let see if a table already managed this index
		    JTable curTable = 
			(JTable) _hTables.get( attrVal );
		    // prepare data
		    for( int j = 0; j < NROWS; j++ ) {
			dataNames[j] = dNames[j] + "-" + num;
		    }

		    if( curTable != null){			
			// names change all the TIME !!!!
			PerfTableModel mod = (PerfTableModel)curTable.getModel();
			if( mod != null){
			    mod.updEntry( _entry , false);
			    mod.updDataNames( dataNames, true );
			} 
		    } else {
			// Here new index have been found, we generate new 
			// associated grp panel
			JTable newTable = makeTable( labels,
						     headers,
						     dataNames,
						     widths );
			/* Numerical fields should be right-aligned */
			rightAlignColumns( newTable, 1, NCOLS-1 );
			addTable( newTable, _myPanel,
				  getAttrValue( attrName ));
			rightAlignColumns( newTable, 1, NCOLS-1 );
			_hTables.put(getAttrValue( attrName ), newTable);
			_myPanel.revalidate();
		    
		    }
		}
	    }
	}
    }

    private JTable makeTable( String[] labels, String[] headers,
			      String[] dataNames, int[] widths ) {
	PerfTableModel model = new PerfTableModel( labels,
						   headers,
						   dataNames );
        JTable table = new JTable( model );
	_tables.addElement( table );
	setColumnWidths( table, widths );
	setColumnHeaders( table, headers );
	return table;
    }

    /**
     * Create the static version panel.
     *
     * @param myContainer The parent component.
     */
    private void createVersionArea( JComponent myContainer ) {
	JPanel panel = new GroupPanel( _resource.getString(_section,
							   "versionGroup-title"));
	GridBagConstraints gbc = getGBC();
	gbc.gridx = 0;
        gbc.gridwidth = gbc.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
	myContainer.add( panel, gbc );

	JLabel label;
	String s;

	s = _resource.getString( _section, "summary-database-label" );
	s += " " + getAttrValue( "database" );
	label = new JLabel( s );
	label.setLabelFor(panel);
	panel.add( label, gbc );

	s = _resource.getString( _section, "summary-configDN-label" );
	s += " " + _entryName;
	label = new JLabel( s );
	label.setLabelFor(panel);
	panel.add( label, gbc );
    }

    /**
     * Create the summary resource panel.
     *
     * @param myContainer The parent component.
     */
    private void createSummaryArea( JComponent myContainer ) {
	String[] dataNames = { "readonly",
			       "entrycachehits",
			       "entrycachetries",
			       "entrycachehitratio",
			       "currententrycachesize",
			       "maxentrycachesize" ,
			       "currententrycachecount",
			       "maxentrycachecount" };
	int NROWS = dataNames.length;
	String[] labels = new String[NROWS];
	for( int i = 0; i < NROWS; i++ )
	    labels[i] = _resource.getString( _section,
					     "summaryTable-" +
					     dataNames[i] + "-label");
	int NCOLS = 2;
	String[] headers = new String[NCOLS];
	for( int i = 0; i < NCOLS; i++ )
	    headers[i] = _resource.getString( _section,
					      "summaryHeader" +
					      i + "-label" );
	int[] widths = { 250, 100 };

	
        _summaryTable = makeTable( labels,
				  headers,
				  dataNames,
				  widths );
		_summaryTable.getAccessibleContext().setAccessibleDescription(_resource.getString(_section,
																						 "summaryGroup-title") );
		/* Numerical fields should be right-aligned */
		rightAlignColumns( _summaryTable, 1, NCOLS-1 );
		
		addTable( _summaryTable, myContainer,
				  _resource.getString(_section, "summaryGroup-title") );
    }


    /**
     * Create the db file info panel.
     *
     * @param myContainer The parent component.
     */
    private void createFileAreas( JComponent myContainer ) {
	String[] dNames = { "dbfilecachehit",
			    "dbfilecachemiss",
			    // "dbfilemap",
			    // "dbfilepagesize",
			    // "dbfilepagecreate",
			    "dbfilepagein",
			    "dbfilepageout" };
	int NROWS = dNames.length;
	String[] labels = new String[NROWS];
	for( int i = 0; i < NROWS; i++ )
	    labels[i] = _resource.getString( _section,
					     "fileTable-" +
					     dNames[i] + "-label");
	
	int NCOLS = 2;
	String[] headers = new String[NCOLS];
	for( int i = 0; i < NCOLS; i++ )
	    headers[i] = _resource.getString( _section,
					      "fileHeader" +
					      i + "-label" );
	int[] widths = { 250, 100 };
	
	int nFiles = 0;

	LDAPAttributeSet attrSet = _entry.getAttributeSet();
	
	for(int i=0; i<attrSet.size(); i++) {
	    String[] dataNames = new String[NROWS];
	    LDAPAttribute attrTmp =  attrSet.elementAt(i);
	    if(attrTmp != null) {
		String attrName = attrTmp.getName();
		if( attrName.startsWith( "dbfilename-" )){
		    // get the filename number
		    String num = attrName.substring(11);
		    for( int j = 0; j < NROWS; j++ ) {
			dataNames[j] = dNames[j] + "-" + num;
		    }
		    JTable table = makeTable( labels,
					      headers,
					      dataNames,
					      widths );
		    /* Numerical fields should be right-aligned */
		    rightAlignColumns( table, 1, NCOLS-1 );
		    
		    addTable( table, myContainer,
			      getAttrValue( attrName ));
			table.getAccessibleContext().setAccessibleDescription(_resource.getString(_section,
																					 "file-table-description",
																					 getAttrValue( attrName )));
		    _hTables.put(getAttrValue( attrName ), table);
		}
	    }
	}
    }

    // name of panel in resource file
    final static private String	_section = "monitordatabase";
    static final ResourceSet	_resource = DSUtil._resource;
    private Hashtable		_hTables = null;
    private JTable		_summaryTable = null;
}
