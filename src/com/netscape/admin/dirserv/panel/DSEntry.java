/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.panel;

import java.util.Vector;
import java.util.Enumeration;
import java.awt.*;
import javax.swing.*;
import javax.swing.text.JTextComponent;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.client.util.Debug;


/**
 * This class represents the application's "view" of an attribute stored in
 * an entry of a directory server.  A DSEntry consists of two parts: a model
 * and a view.  The model consists of a list of values contained in the
 * corresponding attribute in the directory.  The values are stored in the
 * local representation, which is not necessarily the same as the
 * representation in the directory, but it may be, especially for string
 * values.  The view consists of one or more GUI widgets derived from
 * JComponent.  DSEntry is an abstract class, and the abstract part is
 * how the user interacts with the attribute.  Thus, the show(), validate(),
 * and store() methods must be supplied by the application programmer.
 * The DSEntry and DSEntrySet take care of the database interaction for the
 * programmer.  Here is an example of using DSEntrySet.add() for an
 * attribute which is represented in the directory by a text string and
 * in the GUI by three radio buttons.  The radio buttons are created
 * separately.
 * <PRE>
 * ...
 * JComponent[] views = {_rbReset, _rbMayChange, _rbMayNotChange};
 * entries.add(CHANGE_DN, CHANGE_ATTR_NAME,
 *     new DSEntry("must", views) {
 *         public void show() {
 *             String val = getModel(0);
 *             JRadioButton rb0 = (JRadioButton)getView(0);
 *             JRadioButton rb1 = (JRadioButton)getView(1);
 *             JRadioButton rb2 = (JRadioButton)getView(2);
 *             if (val.equals("must")) {
 *                 rb0.setSelected(true);
 *             } else if (val.equals("may")) {
 *                 rb1.setSelected(true);
 *             } else { // no
 *                 rb2.setSelected(true);
 *             }
 *         }
 *         public void store() {
 *             JRadioButton rb0 = (JRadioButton)getView(0);
 *             JRadioButton rb1 = (JRadioButton)getView(1);
 *             if (rb0.isSelected()) {
 *                 setModelAt("must", 0);
 *             } else if (rb1.isSelected()) {
 *                 setModelAt("may", 0);
 *             } else {
 *                 setModelAt("no", 0);
 *             }
 *         }
 *         // its pretty hard to invalidate a radio button . . .
 *         public boolean validate() {return true;}
 *     }
 * );
 * </PRE>
 * There are also several concrete subclasses of DSEntry which implement
 * the model/view functionality for various types of common widget/value
 * combinations.  The DSEntryBoolean class implements a DSEntry for a
 * boolean DS value represented in the GUI by some sort of toggle button.
 * The DSEntryInteger class implements a DSEntry for an integer DS value
 * represented in the GUI by a text entry field with some built in
 * validation.
 *
 * @version %I%, %G%
 * @author Richard Megginson
 * @see com.netscape.admin.dirserv.panel.DSEntrySet
 * @see com.netscape.admin.dirserv.panel.DSEntryBoolean
 * @see com.netscape.admin.dirserv.panel.DSEntryInteger
 */
public abstract class DSEntry implements IDSEntry {
    /**
     * Constructs a DSEntry with an empty initial local model
     * and the widget view
     *
     * @param JComponent view The GUI representation of the model
     */
    public DSEntry(JComponent view) {
        JComponent[] views = {view};
        init(null, views);
    }

    /**
     * Constructs a DSEntry based on the given initial local model
     * value and the widget view
     *
     * @param Object model The default value for the model
     * @param JComponent view The GUI representation of the model
     */
    public DSEntry(String model, JComponent view) {
        JComponent[] views = {view};
        if (model == null) {
            init(null, views);
        } else {
            String[] models = {model};
            init(models, views);
        }
    }

    /**
     * Constructs a DSEntry based on the given initial local model
     * values and the widget view
     *
     * @param model The default values for the model
     * @param JComponent view The GUI representation of the model
     */
    public DSEntry(String[] model, JComponent view) {
        JComponent[] views = {view};
        init(model, views);
    }

    /**
     * Constructs a DSEntry based on the given initial local model
     * value and the widget view
     *
     * @param Object model The default value for the model
     * @param JComponent view The GUI representation of the model
     */
    public DSEntry(String model, JComponent[] view) {
        if (model == null) {
            init(null, view);
        } else {
            String[] models = {model};
            init(models, view);
        }
    }

    /**
     * Constructs a DSEntry based on the given initial local model
     * value and the widget view
     *
     * @param Object model The default value for the model
     * @param JComponent view The GUI representation of the model
     */
    public DSEntry(String model, JComponent view1, JComponent view2) {
        JComponent[] view = new JComponent[2];
        view[0] = view1;
        view[1] = view2;
        if (model == null) {
            init(null, view);
        } else {
            String[] models = {model};
            init(models, view);
        }
    }

    /**
     * Constructs a DSEntry based on the given initial local model
     * values and the widget view
     *
     * @param model The default values for the model
     * @param JComponent view The GUI representation of the model
     */
    public DSEntry(String[] model, JComponent[] view) {
        init(model, view);
    }

	/**
     * Sets initial model for the entry. Useful, when model is
     * known at the construction time
     *
     * @param model - initial values for the model
     */

	/**
     * This method converts the String representation of the attribute
     * stored in the Directory Server to the local model representation
     *
     * @param String remoteValue This is the value as stored in the DS
     */
    public void remoteToLocal(String remoteValue) {
        // removes the previously set default value, if any
        _model = new Vector(1);
        _model.addElement(remoteValue);
		setInitModel ();
        _origModel = (Vector)_model.clone();
        _viewInit = false;
    }

    /**
     * This method is used to convert multivalued attributes
     * stored in the Directory Server to the local model representation
     *
     * @param remoteValue An enumeration of Strings
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#show
     */
    public void remoteToLocal(Enumeration remoteValues) {
        _model = new Vector();
        for (int ii = 0; remoteValues.hasMoreElements(); ++ii) {
            _model.addElement(remoteValues.nextElement());
        }
        
		if (_model.size() < 1)
		{
			// Always allocate one element with empty string.
			// this is to keep _origModel and _model in sync
			// when no value is specified. Otherwise, _model
			// gets set from updateModel, while _origModel
			// stays without a value and they don't match
			_model = new Vector ();
			_model.addElement("");
		}

		setInitModel ();
        _origModel = (Vector)_model.clone();
        _viewInit = false;		
    }

    /**
     * This method is used to get the local values in the form used to
     * store the values in the Directory Server
     *
     * @return An array of String values
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#store
     */
    public String[] localToRemote() {
		String retval[] = null;

		retval = new String[_model.size()];
        _model.copyInto(retval);

        return retval;
    }

	/**
     * Validates entry.
     * @return  DSE_VALID_NOMOD(0)   - data is not modified and valid
     *          DSE_INVALID_NOMOD(1) - data is not modified, but is not valid (empty)
     *          DSE_VALID_MOD(2)     - data is modified and valid
     *          DSE_INVALID_MOD(3)   - data is modified but not valid
     */

	 public int doValidate () {
		boolean modified;
		boolean valid;    

		/* bring the model in sync with current panel content */
        if (_viewInit )
		    updateModel ();

		modified = dirty || isModified ();
		valid = (validate () == 0);

		if (modified && valid) {
			//Debug.println ("doValidate: modified and valid. " + getClass().getName());
			return DSE_VALID_MOD;
		}

		if (modified && !valid) {
			//Debug.println ("doValidate: modified and invalid. " + getClass().getName());
			return DSE_INVALID_MOD;
		}

		if (!modified && valid) {
			//Debug.println ("doValidate: not modified and valid. " + getClass().getName());
			return DSE_VALID_NOMOD;
		}

		/* !modified && !valid */
		//Debug.println ("doValidate: not modified and invalid. " + getClass().getName());
		return DSE_INVALID_NOMOD;
     }

	/** 
	 * Abstract method that must be defined by derived classes
	 * Used by IsModified to decide whether the model was
     * modified
     */

	abstract protected void updateModel ();

	/* 
     * This function is intended to allow derived classes to overwrite
	 * the initial model. This is useful when the data that comes from
     * the server, need to be interpreted before it is displayed.
     * Derived class that need to do so, should overwrite this function
     */
	
	protected void setInitModel () {
	}

	/* 
	 * This function is useful for entries that corespond 
     * to visual elements that don't actually carry any
     * data exchange and whose value depends on some 
     * other field. The examples would be Replication
     * panels that perform their own data exchange, but
     * take advantage validation scheme provided by DSEntry
     * 
     * @param value - value of initial model
     */

	public void fakeInitModel (String value) {
		setModelAt (value, 0);

		_origModel = null;
		_origModel = (Vector)_model.clone();

        _viewInit = false;
	}

	/** 
     * Checks if the data was modified.
     * Considers disabled views as containing
     * unmodified data.
     */

	public boolean isModified () {
		int i, j;
		boolean found;

		/* check if any views are disabled */
		for (i = 0; _view != null && i < _view.length; ++i) {
			if (_view[i] != null && !_view[i].isEnabled ()) {
				Debug.println(8, "DSEntry.isModified: some views are null " +
							  "disabled: " + toString());
				return false;
			}
        }
		
		/* check if data has changed       */

		/* some trivial cases              */
		if (_model == null || _origModel == null) {
			Debug.println(8, "DSEntry.isModified: model is null " + _model + " " +
						  _origModel + " " + toString());
			return false;
		}

		if (_model.size () != _origModel.size ()) {
			Debug.println(8, "DSEntry.isModified: model is null " + _model + " " +
						  _origModel + " " + toString());
			return true;
		}

		for (i = 0; i < _model.size (); i++) {
			found = false;

			for (j = 0; j < _origModel.size (); j++) {
				if (_model.elementAt (i).equals (_origModel.elementAt (j))) {
					found = true;
					break;
				}
			}

			if (!found) {
				return true;
			}
		}

		Debug.println(8, "DSEntry.isModified: model size = " + _model.size() + " orig " +
					  _origModel.size() + " every element is unmodified " + toString());
		/* found every element */
		return false;
	}

    /**
     * Check if the model has been modified.
     */
    public boolean getDirty() {
        return (dirty || isModified ());
    }


    /**
     * This function should be used to overwrite default
     * "dirtiness" criteria based on the comparison of
     * the original and default models.
	 *
     */
    public void setDirty(boolean b) {
        dirty = b;
    }

     /**
     * should be called once the changes are saved on the server 
     * and current model becomes "original"
     */

	public void reset () {
		dirty = false;
		_origModel=null;
		_origModel = (Vector)_model.clone();
	}

    /**
     * This function can be called from a panel Reset button, to revert
     * the model back to the original model.  After calling this, the
     * panel should show() to change the UI components back to their
     * original states
     */

    public void revert () {
        dirty = false;
        _model=null;
        _model = (Vector)_origModel.clone();
    }

    /**
     * This method updates the view to reflect the contents of the
     * local model.  This must be defined for the particular model/view combo
     *
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#show
     */
    abstract public void show();

    /**
     * This method updates the model to reflect changes in the view.
     * This must be defined for the particular model/view combo
     *
     * @see com.netscape.admin.dirserv.panel.DSEntrySet#store
     */
    abstract public void store();

    /**
     * This function is not used and is left here just because it is
     * part of the IDSEntry interface. You should use validate directly
     * instead.
     */
    public boolean dsValidate(){
        return (validate () == 0);
    }

    /**
     * This method does the validation on the fly. It checks the view to see
     * if the values being entered by the user are valid.
     *
     * @return The error code, 0 represents success; otherwise fail.
     */
    abstract public int validate();

    /**
     * Initializes a DSEntry based on the given initial local model
     * values and the widget views
     *
     * @param model The default values for the model
     * @param view The GUI representations of the model
     */
    private void init(String[] model, JComponent[] view) {
        _model = null; // wipes out existing values, if any
        if (model != null && model[0] != null) {
            _model = new Vector(model.length);
            for (int ii = 0; ii < model.length; ++ii) {
                _model.addElement(model[ii]);
            }
        }
		else	
		{
			// Always allocate one element with empty string.
			// this is to keep _origModel and _model in sync
			// when no value is specified. Otherwise, _model
			// gets set from updateModel, while _origModel
			// stays without a value and they don't match
			_model = new Vector ();
			_model.addElement("");
		}

        _origModel = (Vector)_model.clone();
		_view = view;
		setDirty (false);
    }

    /**
     * This method is used for debugging
     *
     */
    public String toString() {
        StringBuffer retval = new StringBuffer();
        retval.append("Model {");
        for (int ii = 0; _model != null && ii < _model.size(); ++ii) {
            retval.append("{model[" + ii + "]=" + _model.elementAt(ii) + "}");
        }
        if (_model == null) {
            retval.append("null");
        }
        retval.append("Original Model {");
        for (int ii = 0; _origModel != null && ii < _origModel.size(); ++ii) {
            retval.append("{model[" + ii + "]=" + _origModel.elementAt(ii) + "}");
        }
        if (_origModel == null) {
            retval.append("null");
        }
        retval.append("} View {");
        for (int ii = 0; _view != null && ii < _view.length; ++ii) {
            retval.append("{view[" + ii + "]=" + _view[ii] + "}");
        }
        retval.append("} ");
        return retval.toString();
    }

    /**
     * This method is used by the subclasses to access the model
     *
     * @param index (zero based) into internal _model array of value to access
     * @return value at that index or null if index is invalid or out of bounds
     */
    protected String getModel(int index) {
        String retval = null;
        if ((_model != null) && (index >= 0) && (index < _model.size()))
            retval = (String)_model.elementAt(index);
        return retval;
    }

	/**
     * This method is used by the subclasses to determine the number of
     * elements in the model
     *
     * @return number of elements
     */
    protected int getModelSize() {
        int size = 0;
        if (_model != null)
            size = _model.size();
        return size;
    }

    /**
     * This method is used by the subclasses to access the view
     *
     * @param index (zero based) into internal _view array of view to access
     * @return view at that index or null if index is invalid or out of bounds
     */
    public JComponent getView(int index) {
        JComponent retval = null;
        if (index >= 0 && index < _view.length)
            retval = _view[index];
        return retval;
    }

    /**
     * This method is used by the subclasses to query the number of views
     *
     * @return number of views
     */
	public int getViewCount() {
		return _view.length;
	}

    /**
     * This method is used by the subclasses to change the model
     *
     * @param value to set in that slot
     * @param index (zero based) into internal _model array of value to set
     */
    protected void setModelAt(String val, int index) {
        if (_model == null) {
            _model = new Vector();
        }
        if (_model.size() <= index) {
            _model.setSize(index+1);
        }
        _model.setElementAt(val, index);

        return;
    }

    /**
     * This method is used by the subclasses to clear the model
     *
     */
    protected void clearModel() {
        if (_model != null) {
            _model.removeAllElements();
        }

        return;
    }

    /**
     * Used by the subclasses to report various types of validation errors.
     * For example, suppose DSEntryInteger detects a non integer value
     * in a text field.
     * <pre>
     * String args[] = new String[1];
     * args[0] = tf.getValue();
     * reportError("102", args, tf, null); // msg is in general section
     * </pre>
     *
     * @param err string version of error number from properties file
     * @param args parameterized additional arguments for error message
     * @param w JComponent widget which contains bogus value
     * @param section section of properties file in which to look for the
     *                error number; if null, general is used
     */
    public static void reportError(String err, String[] args,
                               JComponent w, String section) {
		if ( section == null ) {
			section = "general";
		}
		DSUtil.showErrorDialog( null, err, args, section );
		if (w != null) {
			if (w instanceof JTextComponent) {
                ((JTextComponent)w).selectAll(); // select all text in field
            }
            w.grabFocus(); // put the keyboard focus in this field
        }
    }

    /**
     * Calls reportError(String, String[], JComponent, String) with null
     * section argument.
     *
     * @see reportError(String, String[], JComponent, String)
     */
    public static void reportError(String err, String[] args, JComponent w) {
        reportError(err, args, w, null);
    }

    public static void reportError(String err) {
        reportError(err, null, null, null);
    }

    /* every derived class that overwrites show function must
       call this function from withtin show once the model
       information is displayed.

       The reason for this is that sometimes entry validation
       is triggered before an entry is actually displayed. 
       Validation code calls updateModel which copies the
       data from the corresponding control to the entry's 
       model. If the control is still empty, the initial
       entry data is unintentionally lost */
    protected void viewInitialized () {
        _viewInit = true;
    }

    protected void disableAllComponents() {
		for( int i = 0; (_view != null) && (i < _view.length); i++ ) {
			_view[i].setEnabled( false );
		}
	}

	// Return values from doValidate function
	public static final int DSE_VALID_NOMOD   = 0;
    public static final int DSE_INVALID_NOMOD = 1;
    public static final int DSE_VALID_MOD     = 2;
    public static final int DSE_INVALID_MOD   = 3;

	public static final int DSE_INVALID = 1;
	public static final int DSE_MOD     = 2;

    // Attributes may have more than 1 value
    private Vector _model = null; // a Vector of Strings
    private Vector _origModel = null;  // original Model
    // Models may have more than 1 widget supporting the view
    private JComponent[] _view = null;
    private boolean dirty = false;
    private boolean _viewInit = false;
}
