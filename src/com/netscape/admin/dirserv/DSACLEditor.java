/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.acleditor.*;
import com.netscape.management.client.acl.*;
import com.netscape.management.client.comm.HttpsChannel;
import com.netscape.management.client.util.KingpinLDAPConnection;
import com.netscape.management.client.acl.Rule;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.UtilConsoleGlobals;
import netscape.ldap.*;

/**
 *	Netscape Directory Sever 4.0 ACL Editor. Extends ACLEditor in order
 *  to provide the rights used by Directory Server.
 *
 * @author  rweltman
 * @version 1.2, 03/21/00
 * @date	 	03/06/98
 */
public class DSACLEditor extends ACLEditor {
	/**
	 *	Constructor.
	 *
	 * @param info	Global console connection information
	 */
    public DSACLEditor( ConsoleInfo info ) {
		super( info, new DSDataModelFactory(
			info.getLDAPConnection().getSocketFactory() != null
			) );
	}

}

class DSDataModelFactory extends DefaultDataModelFactory  {
	DSDataModelFactory(boolean ssl) {
		super();
		_ssl = ssl;
	}
    public DataModelAdapter getRightsDataModel(Rule rule) { 
		return new RightsDataModel(
			_resource,
			_section,
			rule,
			customRights); 
	}

	/**
	 * Get the ACL object for this session of the ACL Editor.  This
	 * method overrides the one in the super class because we need
	 * to create a new DSLdapACL in order to create an SSL connection
	 * to the console.
	 *
	 * @param ci a ConsoleInfo object for the ACL Editor session.
	 * @param wf a WindowFactory object for the ACL Editor session.
	 * @return an ACL object.
	 */
	public ACL getACL(ConsoleInfo ci, WindowFactory wf)
	{
		if (acl != null)
			return acl;

		return (acl = new DSLdapACL(
			ci.getHost(), ci.getPort(),
			ci.getAuthenticationDN(),
			ci.getAuthenticationPassword(),
			(LdapACLSelector)(wf.createACLSelectorWindow(null)), _ssl));
	}
   
	static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "acleditor";
	static String[] customRights = {
       "read",
	   "write",
	   "search",
	   "compare",
	   "selfwrite",
	   "delete",
	   "add",
	   "proxy" };

	protected boolean _ssl = false; // if true, console uses SSL
}

class DSLdapACL extends LdapACL {
	public DSLdapACL(String _host, int _port, String _dn, String _pw,
					 LdapACLSelector las, boolean ssl)
	{
		super(_host, _port, _dn, _pw, las);
		_ssl = ssl;
	}

	/**
	 * The newConnection method in the superclass doesn't know how to
	 * create an ssl connection
	 */
	protected LDAPConnection newConnection() throws LDAPException
	{
		LDAPConnection ldc = null;
		if (_ssl) {
			LDAPSSLSocketFactory sfactory = new             
				LDAPSSLSocketFactory();
			ldc=new KingpinLDAPConnection(sfactory, dn, pw);
		} else {
			ldc=new KingpinLDAPConnection(dn, pw);
		}
		ldc.connect(host, port, dn, pw);
		return ldc;
	}

	protected boolean _ssl = false; // if true, newConnection will use ssl
}
