/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;
import java.util.*;
import java.io.UnsupportedEncodingException;
import java.awt.Component;
import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.Frame;
import java.awt.Event;
import java.awt.event.FocusEvent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.datatransfer.*;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.tree.TreePath;
import com.netscape.management.client.*;
import com.netscape.management.client.ug.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.LDAPUtil;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.console.*;
import com.netscape.management.client.ace.ACIManager;
import com.netscape.management.client.util.UITools;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.admin.dirserv.task.CreateVLVIndex;
import com.netscape.admin.dirserv.attredit.AttributeEditorFactory;
import com.netscape.admin.dirserv.roledit.RoleEditorDialog;
import com.netscape.admin.dirserv.propedit.ChooseObjectClassDialog;
import com.netscape.admin.dirserv.account.AccountInactivation;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
  * Directory Content model is a tree model which describes how to
  * display the directory contents.
  *
  * @author  rweltman
  * @see     com.netscape.admin.dirserv.DSBaseModel
  * @see     com.netscape.management.client.ResourceModel
  */
  
public class DSContentModel extends DSBaseModel
                            implements IDSContentListener {

	/**
	 *	Constructor for the content model.
	 *
	 * @param admin The server instance.
	 * @param info	Global console connection information
	 * @param serverInfo Server instance connection information
	 */
	public DSContentModel( ConsoleInfo info,
						   ConsoleInfo serverInfo ) {
		this( info, serverInfo, null );
	}

	/**
	 * Constructor of the model with the root object passed in.
	 *
	 * @param admin The server instance.
	 * @param info	Global console connection information
	 * @param serverInfo Server instance connection information
	 *	@param root root object
	 */
	public DSContentModel( ConsoleInfo info,
						   ConsoleInfo serverInfo,
						   Object root ) {
		super( info, serverInfo );
		initialize( root );
	}

    public void initialize( Object root ) {
		if ( root == null ) {
			DSEntryObject newRoot = new DSEntryObject( this, "" );
			newRoot.setReferralsEnabled(getReferralsEnabled());
			root = newRoot;
			Debug.println(9, "DSContentModel.initialize: new root");
		} else {
			((IDSEntryObject)root).setModel( this );
			Debug.println(9, "DSContentModel.initialize: old root=" +
						  root);
		}
		setRoot( root );

		/* Get any custom attribute editors */
		if ( !_initalizedGlobals && (getConsoleInfo() != null) ) {
			_initalizedGlobals = true;
			String adminGlobals = LDAPUtil.getAdminGlobalParameterEntry();
			int ind = adminGlobals.indexOf( ',' );
			if ( ind < 0 ) {
				return;
			}
			String ourGlobals = "cn=AttributeEditors,ou=Directory" +
				adminGlobals.substring( ind );
			Debug.println( "DSContentModel.initialize: attribute editor " +
						   "entries at " + ourGlobals );
			LDAPConnection ldc = getConsoleInfo().getLDAPConnection();
			try {
				String[] attrNames = { "nsclassname" };
				LDAPEntry entry = ldc.read( ourGlobals, attrNames );
				LDAPAttribute attr = entry.getAttribute( attrNames[0] );
				if ( attr != null ) {
					Enumeration en = attr.getStringValues();
					while( en.hasMoreElements() ) {
						AttributeEditorFactory.addClass(
							(String)en.nextElement() );
					}
				}
			} catch ( Exception e ) {
				Debug.println( "DSContentModel.initialize: " + e );
			}
		}
		_suffixes = MappingUtils.getSuffixList(getServerInfo().getLDAPConnection(), MappingUtils.ALL);
	}

	/**
	 *	get the node of the specified index.
	 *
	 * @param node request the child of this node
	 * @param index position of the child
	 * @return return the child of the specified node
	 */
	public Object getChild(Object node, int index) {
		IDSEntryObject sn = (IDSEntryObject) node;
		return sn.getChildAt(index);
	}

	/**
	 *	return the number of children.
	 * 
	 * @param node node to be checked
	 * @return number of children of the specified node.
	 */
	public int getChildCount(Object node) {
		IDSEntryObject sn = (IDSEntryObject) node;
		return sn.getChildCount();
	}

	/**
	 *	check whether the node is a leaf node or not
	 *
	 * @param node node to be checked
	 * @return true if the node is leaf, false otherwise.
	 */
	public boolean isLeaf(Object node) {
		IDSEntryObject sn = (IDSEntryObject) node;
		return ( sn.isLeaf() );
	}

    /**
      * Returns supported menu categories
      */
	public String[] getMenuCategoryIDs() {
		if (_categoryID == null) {
			_categoryID = new String[] {
				Framework.MENU_FILE,
					Framework.MENU_EDIT,
					Framework.MENU_VIEW,	
					ResourcePage.MENU_CONTEXT,			
					ResourcePage.MENU_OBJECT,				
					CONTEXTNEW,
					CONTEXTNEWROOTENTRY,
					OBJECTNEW,
					OBJECTNEWROOTENTRY,
					PARTITIONVIEW
					};
		}
		return _categoryID;
	}

	/**
	 * add menu items for this page.
	 */
	public IMenuItem[] getMenuItems(String category) {		
		if(category.equals(Framework.MENU_FILE)) {
			return _fileMenuItems;
		} else if ( category.equals(ResourcePage.MENU_CONTEXT) ) { 			
			return 	_contextMenuItems;	 
		} else if( category.equals(CONTEXTNEW)) { 			
			return _contextNewMenuItems;
		} else if ( category.equals(OBJECTNEW)) {
			return _objectNewMenuItems;
		} else if (category.equals(CONTEXTNEWROOTENTRY)) {
			if (_contextNewRootEntryMenuItems == null) {
				createNewRootEntryMenuItems();
			}				
			return _contextNewRootEntryMenuItems;
		} else if (category.equals(OBJECTNEWROOTENTRY)) {
			if (_objectNewRootEntryMenuItems == null) {
				createNewRootEntryMenuItems();
			}
			return _objectNewRootEntryMenuItems;
			
		} else if( category.equals(Framework.MENU_EDIT) ) {
			return _editMenuItems;
		} else if(category.equals(Framework.MENU_VIEW)) {
			if (_viewMenuItems == null) {
				_viewMenuItems = createViewMenuItems();
			}			
			return _viewMenuItems;
		} else if(category.equals(ResourcePage.MENU_OBJECT)) {			
			return _objectMenuItems;  		
		} else if(category.equals(PARTITIONVIEW)) {
		   if (_partitionViewMenuItems == null) {
			   _partitionViewMenuItems = createPartitionViewMenuItems();
		   }
		   return _partitionViewMenuItems;
		}
		return null;
	}

	/**
	 * Called by a client other than the ResourcePage - the DSEntryList
	 */
	public void setSelected( IPage viewInstance,
							 IResourceObject[] selection,
							 IResourceObject[] previousSelection) {		
		if ( (selection == null) || (selection.length < 1) || (selection[0] != null) ) {
			super.actionObjectSelected(viewInstance, selection,
									   previousSelection);
		}
		_selection = selection;
		if ( _selection == null )
			_selection = new IResourceObject[0];
		if ( _selection.length > 0 )
			Debug.println( "DSContentModel.setSelected: [" +
						   _selection.length + "] " + _selection[0] );

		/* Check if there is a browsing index on all selections */
		LDAPConnection ldc = getServerInfo().getLDAPConnection();
		boolean hasIndex = true;		
		for( int i = 0; i < _selection.length; i++ ) {
			if ( selection[i] == null ) {
				hasIndex = false;
				break;
			}	
			hasIndex = CreateVLVIndex.hasIndex(((IDSEntryObject)selection[i]).getDN(), getServerInfo());
			if (!hasIndex) {
				break;
			}
		}
		if ( hasIndex) {			
			fireEnableMenuItem( viewInstance, DELETE_INDEX );
			fireDisableMenuItem( viewInstance, CREATE_INDEX );
		} else {
			fireDisableMenuItem( viewInstance, DELETE_INDEX );
			fireEnableMenuItem( viewInstance, CREATE_INDEX );
		}		
		
		/* Check if the entries are activated or inactivated.  We enable the activate menu item if there's at least an object inactivated
		 and the inactivate menu item if there's at least an object activated*/		
		IDSEntryObject deo = null;
		if (_selection != null) {
			if (_selection.length > 0) {
				Debug.println( "DSContentModel.setSelected: " + _selection[0] );
				boolean oneLocked = false;
				boolean oneNotLocked = false;
				boolean isLocked = false;
				for( int i = 0; i < _selection.length; i++ ) {			
					deo = (IDSEntryObject)_selection[i] ;					
					if (deo != null) {						
						LDAPEntry entry = deo.getEntry();
						if (entry != null) {
							AccountInactivation account = new AccountInactivation(entry);
							try {
								isLocked = account.isLocked(getServerInfo().getLDAPConnection());
								oneLocked = oneLocked || isLocked;
								oneNotLocked = oneNotLocked || !isLocked;
							} catch (LDAPException ex) {
								Debug.println("DSContentModel.setSelected() "+ex);
							}
						}			
					}		
				}
				if ( oneLocked ) {
					fireEnableMenuItem( viewInstance, ACTIVATE );									
				} else {
					fireDisableMenuItem( viewInstance, ACTIVATE );
				}
				if ( oneNotLocked ) {
					fireEnableMenuItem( viewInstance, INACTIVATE );									
				} else {
					fireDisableMenuItem( viewInstance, INACTIVATE);
				}
			}
		}

		/* Update the Create New Root entry for suffix menu */
		if (isRoot()) {
			if (_updateNewRootEntryMenu) {				
				fireRemoveMenuItems(viewInstance, (IMenuInfo)this);
				/* We recalculate the menu for creating a new root entry */
				_suffixWithNoEntryList = null;
				_objectNewRootEntryMenuItems = null;
				_contextNewRootEntryMenuItems = null;
				fireAddMenuItems(viewInstance, (IMenuInfo)this);
				_updateNewRootEntryMenu = false;
			}
			if (_suffixWithNoEntryList != null) {
				if (_suffixWithNoEntryList.size() < 1) {
					fireDisableMenuItem(viewInstance, CONTEXTNEWROOTENTRY);
					fireDisableMenuItem(viewInstance, OBJECTNEWROOTENTRY);
				} else {								
					fireEnableMenuItem(viewInstance, CONTEXTNEWROOTENTRY);
					fireEnableMenuItem(viewInstance, OBJECTNEWROOTENTRY);
				}
			}
			fireDisableMenuItem(viewInstance, CONTEXTNEW);
			fireDisableMenuItem(viewInstance, OBJECTNEW);
		} else {
			fireEnableMenuItem(viewInstance, CONTEXTNEW);
			fireEnableMenuItem(viewInstance, OBJECTNEW);
			
			fireDisableMenuItem(viewInstance, CONTEXTNEWROOTENTRY);
			fireDisableMenuItem(viewInstance, OBJECTNEWROOTENTRY);
		}
		
		/* Update the Cut/Copy/Paste menu items */
		if (_selection.length > 0) {
			fireEnableMenuItem(viewInstance, CUT);
			fireEnableMenuItem(viewInstance, COPY);
		} else {
			fireDisableMenuItem(viewInstance, CUT);
			fireDisableMenuItem(viewInstance, COPY);
		}
		if (isClipboardEmpty()) {
			fireDisableMenuItem(viewInstance, PASTE);
		} else {
			fireEnableMenuItem(viewInstance, PASTE);
		}				
	}

	public void actionObjectSelected( IPage viewInstance,
									  IResourceObject[] selection,
									  IResourceObject[] previousSelection) {
		setSelected( viewInstance, selection, previousSelection );
		/* Save the ResourcePage selection */
		_localSelection = selection;
	}


	private void initKeyListeners() {
		if ( !_keyListenersInitialized && (getSelectedPage() != null) ) {
			_keyListenersInitialized = true;
			/* Copy DN to clipboard on Shift-Ctrl-C */
			((DSResourcePage)getSelectedPage()).getTree().
				registerKeyboardAction(
									   new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						actionSelected( COPYDN, _selection,
										getSelectedPage() );
					}
				},
					KeyStroke.getKeyStroke(KeyEvent.VK_C,
										   Event.SHIFT_MASK|Event.CTRL_MASK),
					JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT );

		}
	}

	public void removeElement( IDSEntryObject deo ) {
		removeFromSelection( deo );
		IDSEntryObject parent = (IDSEntryObject)deo.getParentNode();
		DSEntryList list = null;
		if ( parent != null ) {
			Component p = parent.getCustomPanel();
			if ( p instanceof DSEntryList )
				list = (DSEntryList)p;
		}
		if ( parent != null ) {
			parent.reload();
			repaintObject( parent );
		}
		if ( list != null ) {
			list.removeElement( deo );
			list.repaint();
		}
	}

	/**
	 * Remove an object from the selection list.
	 *
	 * @param o The object to be removed.
	 */
    private void removeFromSelection( IResourceObject o ){
		Vector v = new Vector();
		for( int i = 0; i < _selection.length; i++ )
			v.addElement( _selection[i] );
		if ( v.contains( o ) ) {
			v.removeElement( o );
			_selection = new IResourceObject[v.size()];
			v.copyInto( _selection );
		}
	}

    /**
      * Called internally when page is selected
      */
	public void pageSelected( IFramework framework, IPage viewInstance ) {		
		Debug.println( "DSContentModel.pageSelected" );		
		if (_updateOnSelect)
			setRefreshUponSelect(_updateOnSelect); // force update
		super.pageSelected(framework, viewInstance);
		((Framework)framework).setBusyCursor(true);
		/* We initialize the key listeners */
		initKeyListeners();
		/* We recalculate the menu for viewing partitions, and the menu for creating new root entries*/		
		fireRemoveMenuItems(viewInstance, (IMenuInfo)this);
		_backends = MappingUtils.getBackendList(getServerInfo().getLDAPConnection(), MappingUtils.LDBM);
		_suffixes = MappingUtils.getSuffixList(getServerInfo().getLDAPConnection(), MappingUtils.ALL);
		_partitionViewMenuItems = null;
		_suffixWithNoEntryList = null;
		_objectNewRootEntryMenuItems = null;
		_contextNewRootEntryMenuItems = null;
		fireAddMenuItems(viewInstance, (IMenuInfo)this);
		_updateNewRootEntryMenu = false;
		
		if (isRoot()) {
			if (_suffixWithNoEntryList != null) {
				if (_suffixWithNoEntryList.size() < 1) {
					fireDisableMenuItem(viewInstance, CONTEXTNEWROOTENTRY);
					fireDisableMenuItem(viewInstance, OBJECTNEWROOTENTRY);
				} else {				
					fireEnableMenuItem(viewInstance, CONTEXTNEWROOTENTRY);
					fireEnableMenuItem(viewInstance, OBJECTNEWROOTENTRY);
				}
			}
			fireDisableMenuItem(viewInstance, CONTEXTNEW);
			fireDisableMenuItem(viewInstance, OBJECTNEW);	
		} else {
			fireEnableMenuItem(viewInstance, CONTEXTNEW);
			fireEnableMenuItem(viewInstance, OBJECTNEW);
			
			fireDisableMenuItem(viewInstance, CONTEXTNEWROOTENTRY);
			fireDisableMenuItem(viewInstance, OBJECTNEWROOTENTRY);
		}
		((Framework)framework).setBusyCursor(false);
	}


	public void refreshView() {
		initKeyListeners();
		newTree();
	}

	/**
	 * Currently called from DSResourcePage.focusGained, so we can keep
	 * track of whether the selected items are in the tree (here) or in
	 * the DSEntryList.
	 */
	public void focusGained( FocusEvent e ) {
		_selection = _localSelection;
		/* Check if the entries are activated or inactivated.  We enable the activate menu item if there's at least an object inactivated
		 and the inactivate menu item if there's at least an object activated*/
		IDSEntryObject deo = null;
		if (_selection != null) {
			if (_selection.length > 0) {
				Debug.println("DSContentModel.focusGained()");
				boolean oneLocked = false;
				boolean oneNotLocked = false;
				boolean isLocked = false;
				/* This is necessary: in the case where we have two objects selected (one on the left (tree) and the other
				   on the right (DSEntryList), and one is activated and the other not, we have to update the menu considering
				   the focus */
				for( int i = 0; i < _selection.length; i++ ) {			
					deo = (IDSEntryObject)_selection[i] ;					
					if (deo != null) {						
						LDAPEntry entry = deo.getEntry();
						if (entry != null) {
							AccountInactivation account = new AccountInactivation(entry);
							try {
								isLocked = account.isLocked(getServerInfo().getLDAPConnection());
								oneLocked = oneLocked || isLocked;
								oneNotLocked = oneNotLocked || !isLocked;
							} catch (LDAPException ex) {
								Debug.println("DSContentModel.focusGained() "+ex);
							}					
						}
					}
				}
				if ( oneLocked ) {
					fireEnableMenuItem( getSelectedPage(), ACTIVATE );									
				} else {
					fireDisableMenuItem( getSelectedPage(), ACTIVATE );
				}
				if ( oneNotLocked ) {
					fireEnableMenuItem( getSelectedPage(), INACTIVATE );									
				} else {
					fireDisableMenuItem( getSelectedPage(), INACTIVATE);
				}
			}
		} else
			Debug.println( "DSContentModel.focusGained: null" );
	}

    /**
	 * Notification that a menu item has been selected.
	 *
	 * @param viewInstance Parent object.
	 * @param item The selected menu item.
	 */
	public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
		super.actionMenuSelected(viewInstance, item);
		actionSelected(item.getID(), _selection, viewInstance );
	}

	public void contentChanged() {
		newTree();
	}

    void repaintObject( IResourceObject deo ) {
		Debug.println( "DSContentModel.repaintObject: " +
					   deo );
		if (deo == null)
			return;

		fireTreeStructureChanged( (ResourceObject)deo );
		Component c = deo.getCustomPanel();
		if ( c != null ) {
			c.repaint();
		}
		setSelectedNode( deo );
	}

	/**
	 * Some nodes are part of the JTree that JFC knows about, others are
	 * virtual nodes managed by us
	 */
	void updateObjectInTree( IResourceObject node ) {
		IDSEntryObject deo = (IDSEntryObject)node;
		if (deo.getParent() != null) {
			repaintObject( (IDSEntryObject)deo.getParent() );
			repaintObject( deo );
		}
		else if (deo.getParentNode() != null) {
			repaintObject( deo.getParentNode() );
		} else {
			repaintObject(deo);
		}
	}

	private void actionCopy( IResourceObject[] selection,
						 IPage viewInstance ) {
		_clipboard.removeAllElements();
		for( int i = 0; i < selection.length; i++ ) {
			IDSEntryObject deo = (IDSEntryObject)selection[i];
			if ( deo != null ) {
				String dn = ((IDSEntryObject)_selection[i]).getDN();
				boolean isRoot = (dn == null) || dn.equals("");
				if (!isRoot) {
					/* We don't allow to copy the root entry */
					copyTree( deo );
				} else {
					/* If the user chose to copy the root entry we
					   display an error message */
					DSUtil.showErrorDialog( getFrame(),
											"copy-root-error-title", 
											"copy-root-error-msg", 
											(String[]) null, 
											"browser" );
				}
			}
		}
		if (isClipboardEmpty()) {
			fireDisableMenuItem(viewInstance, PASTE);
		} else {
			fireEnableMenuItem(viewInstance, PASTE);
		}
	}

	private void actionCopyDN( IResourceObject[] selection,
						 IPage viewInstance ) {
		if ( selection.length > 0 ) {
			String text = ((IDSEntryObject)selection[0]).getDN();
			if ( text != null ) {
				StringSelection ss = new StringSelection( text );
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
					ss,
					ss );
			}
		}
	}

	private void actionPaste( IResourceObject[] selection,
						 IPage viewInstance ) {
		if ( _clipboard.size() > 0 ) {
			if ( (selection == null) || (selection.length < 1) ) {
				Debug.println( "No selection!" );
				return;
			}
			IDSEntryObject parent = (IDSEntryObject)selection[0];			
			DSEntryList list = null;
			Component p = parent.getCustomPanel();
			if ( p instanceof DSEntryList )
				list = (DSEntryList)p;
			String dn = parent.getDN();
			boolean isRoot = (dn == null) || dn.equals("");
			if (isRoot) {
				/* If the user chose to paste in the root entry we
				   display an error message */
				DSUtil.showErrorDialog( getFrame(),
										"paste-in-root-error-title", 
										"paste-in-root-error-msg", 
										(String[]) null, 
										"browser" );
			} else {
				Vector v = new Vector();
				for( int i = 0; i < _clipboard.size(); i++ ) {
					EntryNode node = (EntryNode)_clipboard.elementAt(i);
					try {
						LDAPEntry newEntry = pasteTree( dn, node );
						IDSEntryObject newObject =
							DSEntryObjectFromEntry( newEntry );
						newObject.initializeFromEntry( newEntry );
						v.addElement( newObject );
						if ( list != null ) {
							list.addElement( newObject );
						}
					} catch ( LDAPException e ) {
						// display a message
						String err = null;
						String title = "add-failed";
						if ( e.getLDAPResultCode() == e.CONSTRAINT_VIOLATION ) {
							String msg = e.getLDAPErrorMessage();
							if ( msg != null ) {
								if ( msg.endsWith(
												  "the same attribute value already exists" ) ) {
								}
								err = "duplicate-uid";
							}
						}
						if ( err != null ) {
							DSUtil.showErrorDialog( getFrame(),
													title, err, "", "general" );
						} else {
							DSUtil.showLDAPErrorDialog( getFrame(),
														e, title );
						}
						break;
					}
				}
				parent.reload();
				repaintObject( parent );
				_selection = new IResourceObject[v.size()];
				v.copyInto( _selection );
			}
		}
	}

	private void actionCutDelete( IResourceObject[] selection,
						 IPage viewInstance, boolean cut ) {
		if ( selection.length < 1 ) {
			return;
		}

		if (selection.length == 1) {
			/* We can't delete the root entry (and the server when we try to
			   does not return any error message) */
			if (isRoot()) {
				DSUtil.showErrorDialog( getFrame(),
										"delete-root-error-title", 
										"delete-root-error-msg", 
										(String[]) null, 
										"browser" );
				return;
			}
		}

		boolean many = ( selection.length >= 2 );
		boolean hasChildren = false;
		for( int i = 0; i < selection.length; i++ ) {
			IDSEntryObject deo = (IDSEntryObject)selection[i];
			if ( deo != null ) {
				LDAPEntry selEntry = deo.getEntry();
				if ( (selEntry != null) && entryHasChildren( selEntry ) ) {
					hasChildren = true;
					break;
				}
			}
		}	
		boolean confirm = DSUtil.requiresConfirmation(
			GlobalConstants.PREFERENCES_CONFIRM_DELETE_ENTRY );


		/* Show confirmation dialog to delete entry and subtree */
		if ((confirm ||
			 DSUtil.requiresConfirmation(
										 GlobalConstants.PREFERENCES_CONFIRM_DELETE_SUBTREE)) &&
			hasChildren ) {
			String msg = null;
			String args[] = null;
			if (many) {					
				msg = "confirmDeleteTrees";				
			} else {
				args = new String[1];
				args[0] = DSUtil.abreviateString(
												 ((IDSEntryObject)selection[0]).getDN(),
												 30);
				msg = "confirmDeleteTree";				
			}		
			int option = DSUtil.showConfirmationDialog(
													   getFrame(),
													   msg,
													   args,
													   "browser");
            if (option != JOptionPane.YES_OPTION) {
				return;
			}
		/* Show confirmation dialog to delete entry(ies) with no children */	
		} else if ( confirm && !hasChildren) {
			String msg = null;
			String args[] = null;
			if (many) {				
				msg = "confirmDeleteObjects";				
			} else {
				args = new String[1];
				args[0] = DSUtil.abreviateString(
												 ((IDSEntryObject)selection[0]).getDN(),
												 30);
				msg = "confirmDeleteObject";				
			}
			int option = DSUtil.showConfirmationDialog(
				                             getFrame(),
											 msg,
											 args,
											 "browser");
			if ( option != JOptionPane.YES_OPTION ) {
				return;
			}
		}
		
		setWaitCursor(true);

		if( cut ) {
			/* Make sure we have the entry to paste back */
			actionCopy( selection, viewInstance );
		}

		_updateNewRootEntryMenu = true;
		int ret = -1;
		IDSEntryObject deo = (IDSEntryObject)selection[0];
		IDSEntryObject parent = (IDSEntryObject)deo.getParent();
		String title = _resource.getString("browser", "delete-objects-title");
		GenericProgressDialog dlg = new GenericProgressDialog(getFrame(), 
																  true, 
																  GenericProgressDialog.TEXT_FIELD_AND_CANCEL_BUTTON_OPTION, 
																  title,
																  null,
																  null);
		dlg.setTextInTextAreaLabel(_resource.getString("browser", "non-deleted-objects-label")); 
		dlg.setTextAreaRows(3);			
		dlg.setLocationRelativeTo((DSResourcePage)getSelectedPage());
		dlg.setLabelColumns(50);
		TreeDeleter deleter =
			new TreeDeleter( selection, this, dlg );
		many = many || hasChildren;
		if ( many ) {						
			/* Let the dialog run the deleter thread */
			try {	
				Thread th = new Thread(deleter);
				th.start();
				setWaitCursor(false);
				dlg.packAndShow();                          							
			} catch ( Exception e ) {
				Debug.println("DSContentModel.actionInactivate(): " +
							  e );
				e.printStackTrace();
			}
			ret = deleter.getStatus();
			if ( ret != 0 ) {
				Debug.println( "DSContentModel.actionCutDelete: " +
							   "deleteTree returned " + ret +
							   ", viewInstance = " + viewInstance );				
			}			
		} else {
			/* Run the deleter explicitly, not as a thread */
			deleter.run();
			setWaitCursor(false);
			ret = deleter.getStatus();		
			if ( ret == 0 ) {
				String[] args = {DSUtil.abreviateString(deo.getDN(), 45)};
				DSUtil.showInformationDialog( getFrame(), "deleted-object",
											  args, "browser" );
			} else {
				Debug.println( "DSContentModel.actionCutDelete: " +
							   "deleteTrees returned " + ret +
							   ", viewInstance = " + viewInstance );
				dlg.packAndShow();
			}
		}
		setWaitCursor(true);
        if (parent == null)
    		parent = (IDSEntryObject)deo.getParentNode();		
		deo.removeFromParent();
		fireTreeStructureChanged( (ResourceObject)deo );
		if ( parent != null ) {			
            parent.reload();			
			repaintObject( parent );
		} else {
			Debug.println( "DSContentModel.actionCutDelete: deleted " +
						   "entry has no parent node" );
		}
		setWaitCursor(false);
	}

	private void actionEdit( IResourceObject[] selection ) {
		Debug.println( "DSContentModel.actionEdit: " + selection.length +
					   " selections" );
		if ( selection.length == 1 ) {
			IDSEntryObject deo = (IDSEntryObject)selection[0];
			if ( deo != null ) {
				deo.editProperties();
			}
		} else if ( selection.length > 1 ) {
			if ( isCompatibleList( selection ) ) {
				IDSEntryObject deo = getCommonAttributes(
					selection, this, getServerInfo().getBaseDN(),
					getReferralsEnabled());
				if ( deo != null ) {
					deo.editGeneric( selection, true );
				}
			}
		}
	}

	private void actionACL( IResourceObject[] selection,
						 IPage viewInstance ) {
		String dn = null;
		if ( selection.length > 0 ) {
			dn = ((IDSEntryObject)selection[0]).getDN();
		}
		if ( dn != null ) {
			LDAPConnection aciLdc = getServerInfo().getLDAPConnection();
			LDAPConnection ugLdc = Console.getConsoleInfo().getUserLDAPConnection();
			//
			// Let's select the base dn for searching users.
			// Note that we implement a temporary solution here: 
			// for 5.0 RTM we should be able to pass all the accessible 
			// suffixes to the ACI editor. See:
			// https://scopus.mcom.com/bugsplat/show_bug.cgi?id=523566
			//
			String ugDn;
			if (ugLdc.getHost().equals(aciLdc.getHost()) && 
			    (ugLdc.getPort() == aciLdc.getPort())) {
				Debug.println("DSContentModel.actionACL: ACI and users are on the same directory");
				ugDn = Console.getConsoleInfo().getUserBaseDN();
			}
			else {
				Debug.println("DSContentModel.actionACL: ACI and users are on different directories");
				ugDn = MappingUtils.getTopSuffixForEntry(aciLdc, dn);
			}
			Debug.println("DSContentModel.actionACL: users will be search from " + ugDn);
			ACIManager acm = new ACIManager(getFrame(), dn, aciLdc, dn, aciLdc, ugDn);
			acm.show();
			acm.dispose();
		}
	}

	private void actionRoles( IResourceObject[] selection,
						 IPage viewInstance ) {
		String dn = null;
		if ( selection.length > 0 ) {
			dn = ((IDSEntryObject)selection[0]).getDN();
		}
		if ( dn != null ) {
			ConsoleInfo ci = new ConsoleInfo(
				getServerInfo().getHost(),
				getServerInfo().getPort(),
				getServerInfo().getAuthenticationDN(),
				getServerInfo().getAuthenticationPassword(),
				dn );
			ci.setAclDN( dn );
			ci.setCurrentDN( dn );
			ci.setUserGroupDN( dn );
			ci.setLDAPConnection( getServerInfo().getLDAPConnection() );
            RoleEditorDialog ed = new RoleEditorDialog(getFrame(), ci);
            ed.show();
		}
	}

	private void actionCreateIndex( IResourceObject[] selection,
									IPage viewInstance ) {
		if ( selection.length > 0 ) {
			final String dn = ((IDSEntryObject)selection[0]).getDN();
			final IPage fViewInstance = viewInstance;
			Thread thread = new Thread(new Runnable() {
				public void run() {					
					if ( dn != null ) {
						CreateVLVIndex task =
							new CreateVLVIndex( getServerInfo() );
						if (task.execute(dn)) {
							/* Update the menu items */
							fireEnableMenuItem( fViewInstance, DELETE_INDEX );
							fireDisableMenuItem( fViewInstance, CREATE_INDEX );
						}					
					}				
				}
			});
			thread.start();
		}
	}

	private void actionDeleteIndex( IResourceObject[] selection,
									IPage viewInstance ) {
		for( int i = 0; i < selection.length; i++ ) {
			CreateVLVIndex.deleteIndex(((IDSEntryObject)selection[i]).getDN(), getServerInfo() );
			if (!CreateVLVIndex.hasIndex(((IDSEntryObject)selection[i]).getDN(), getServerInfo() )) {
				fireDisableMenuItem( viewInstance, DELETE_INDEX );
				fireEnableMenuItem( viewInstance, CREATE_INDEX );
			}
		}
	}

	private void actionInactivate( IResourceObject[] selection,
									IPage viewInstance ) {
		if (selection != null) {
			if ( selection.length > 0 ) {
				String title = _resource.getString("accountinactivation-inactivate", "title");
				GenericProgressDialog dlg = new GenericProgressDialog(getFrame(), 
																	  true, 
																	  GenericProgressDialog.TEXT_FIELD_AND_CANCEL_BUTTON_OPTION, 
																	  title,
																	  null,
																	  null);
				dlg.setTextInTextAreaLabel(_resource.getString("accountinactivation-inactivate", "rejected-objects"));
				dlg.setTextAreaRows(5);
				dlg.setTextAreaColumns(30);
				dlg.setLabelColumns(50);
				dlg.setLocationRelativeTo((DSResourcePage)getSelectedPage());
				try {
					InactivateRunnable task = new InactivateRunnable(dlg, selection, viewInstance);
					Thread th = new Thread(task);
					th.start();		
					dlg.packAndShow();							
				} catch ( Exception e ) {
					Debug.println("DSContentModel.actionInactivate(): " +
								  e );
					e.printStackTrace();
				} 				
			}
		}			
	}


	private void actionActivate( IResourceObject[] selection,
									IPage viewInstance ) {
		if (selection != null) {
			if ( selection.length > 0 ) {
				String title = _resource.getString("accountinactivation-activate", "title");
				GenericProgressDialog dlg = new GenericProgressDialog(getFrame(), 
																	  true, 
																	  GenericProgressDialog.TEXT_FIELD_AND_CANCEL_BUTTON_OPTION, 
																	  title,
																	  null,
																	  null);
				dlg.setTextInTextAreaLabel(_resource.getString("accountinactivation-activate", "rejected-objects"));
				dlg.setTextAreaRows(5);
				dlg.setTextAreaColumns(30);
				dlg.setLabelColumns(50);
				dlg.setLocationRelativeTo((DSResourcePage)getSelectedPage());
				try {
					ActivateRunnable task = new ActivateRunnable(dlg, selection, viewInstance);
					Thread th = new Thread(task);
					th.start();					
					dlg.packAndShow(); 	
				} catch ( Exception e ) {
					Debug.println("DSContentModel.actionActivate(): " +
								  e );
					e.printStackTrace();	
				}
			}
		}		
	}

	private void actionNewUser( IResourceObject[] selection,
						 IPage viewInstance ) {
		if ( selection.length > 0 ) {
			IDSEntryObject deo = (IDSEntryObject)selection[0];
			deo.newUser();
			updateObjectInTree( deo );
		}
	}

	private void actionNewGroup( IResourceObject[] selection,
						 IPage viewInstance ) {
		if ( selection.length > 0 ) {
			IDSEntryObject deo = (IDSEntryObject)selection[0];
			deo.newGroup();
			updateObjectInTree( deo );			
		}
	}

	private void actionNewRole( IResourceObject[] selection,
						 IPage viewInstance ) {
		if ( selection.length > 0 ) {
			IDSEntryObject deo = (IDSEntryObject)selection[0];
			deo.newRole();
			updateObjectInTree( deo );
		}
	}

	private void actionNewCos( IResourceObject[] selection,
						 IPage viewInstance ) {
		if ( selection.length > 0 ) {
			IDSEntryObject deo = (IDSEntryObject)selection[0];
			deo.newCos();
			updateObjectInTree( deo );
		}
	}

	private void actionNewOrganizationalUnit( IResourceObject[] selection,
						 IPage viewInstance ) {
		if ( selection.length > 0 ) {
			IDSEntryObject deo = (IDSEntryObject)selection[0];
			deo.newOrganizationalUnit();
			updateObjectInTree( deo );
		}
	}

	private void actionNewObject( IResourceObject[] selection,
						 IPage viewInstance ) {
		if ( selection.length > 0 ) {
			IDSEntryObject deo = (IDSEntryObject)selection[0];
			deo.newObject();
			updateObjectInTree( deo );
		}
	}

	private void actionAuthenticate( IResourceObject[] selection,
						 IPage viewInstance ) {
		/* Get an LDAPConnection from the root node */
		IDSEntryObject deo = (IDSEntryObject)getRoot();
		if ( deo != null ) {
			LDAPConnection ldc = deo.getLDAPConnection();
			if ( ldc == null )
				return;
			/* This will be our new one */
			getServerInfo().setLDAPConnection( ldc );
			/* getNewAuthentication will propagate the changes */
			boolean status = getNewAuthentication();
			if ( status ) {
				/* Start a fresh tree, since we don't know what the
				   new user should be  able to see */
				newTree();
			}
		}
	}

	private Vector getSuffixesWithNoEntry() {
		LDAPConnection ldc = getServerInfo().getLDAPConnection();

		String[] ldbmSuffixes = MappingUtils.getSuffixList(ldc, MappingUtils.LDBM);
		Vector list = new Vector();
		if (ldbmSuffixes != null) {
			for (int i=0; i<ldbmSuffixes.length; i++) {
				String suffix = ldbmSuffixes[i];
				try {
					LDAPSearchConstraints cons =
						(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
					cons.setMaxResults( 0 );					
					LDAPEntry entry = DSUtil.readEntry(ldc, suffix, null, cons);
					if (entry == null) {
						if (list.indexOf(suffix) < 0 ) {
							Debug.println("DSContentModel.getSuffixesWithNoEntry(): entry null with suffix "+suffix);
							list.addElement(suffix);
						}
					}
				} catch (LDAPException e) {
					Debug.println("DSContentModel.getSuffixesWithNoEntry(): with suffix "+suffix+ " "+e);
					if (list.indexOf(suffix) < 0 ) {
						list.addElement(suffix);
					}
				}
			}
		}
		return list;
	}

    class Updater implements Runnable {
		Updater( IResourceObject deo ) {
		   this.deo = deo;
		}
	    public void run() {
			DSContentModel.this.repaintObject( deo );
		}
		IResourceObject deo;
	}

    private void newTree() {
		LDAPConnection ldc = null;
		IDSEntryObject deo = (IDSEntryObject)getRoot();
		if ( deo != null ) {
			ldc = deo.getLDAPConnection();
		}
		if ( ldc == null ) {
			Debug.println( "DSContentModel.newTree: no LDAPConnection " +
						   "at root" );
			return;
		}
		DSEntryObject root = new DSEntryObject( this, "" );
		Debug.println(9, "DSContentModel.newTree: new root");
		root.setReferralsEnabled(getReferralsEnabled());
		setRoot( root );
		refreshTree();
		setSelectedNode( root );
	}

    private void refreshNode( IDSEntryObject deo ) {		
		deo.load();
		if (deo.getEntry() != null) {
			repaintObject( deo );
			fireTreeStructureChanged( (ResourceObject)deo );
			setSelectedNode( (ResourceObject)deo );
		} else {
			DSEntryObject parent = (DSEntryObject) deo.getParentNode();
			if (parent != null) {
				refreshNode( parent );
			}
		}
	}

    private void refreshTree() {
		refreshNode( (IDSEntryObject)getRoot() );
	}

	public Object getRoot() {
		Object root = super.getRoot();
		return root;
	}							

	/** 
	  * Tells if the user has chosen to see the state of the account or not
	  *
	  * @return true if the user wants to see the state of the account.  False otherwise
	  */
	public boolean isViewAccountInactivationSelected() {
		return _isViewAccountInactivationSelected;
	}

    private void actionReferrals() {
		boolean referrals = _menuReferrals.isChecked();
		Debug.println( "DSContentModel.actionReferrals: " + referrals );
		setReferralsEnabled( referrals );
		newTree();
	}

	private void actionRefresh( IResourceObject[] selection,
								IPage viewInstance ) {
		if ( selection.length > 0 ) {			
			IDSEntryObject deo = (IDSEntryObject)selection[0];
			deo.reset();					
			refreshNode( deo );
		}
	}

	private void actionSearchUG( IResourceObject[] selection,
						 IPage viewInstance ) {
		if ( selection.length < 1 ) {
			return;
		}
		ConsoleInfo info = (ConsoleInfo)getServerInfo().clone();		
		IDSEntryObject deo = (IDSEntryObject)selection[0];
		info.setUserBaseDN( deo.getDN() );
		info.setUserLDAPConnection(info.getLDAPConnection());
		info.setUserHost(info.getLDAPConnection().getHost());
		info.setUserPort(info.getLDAPConnection().getPort());
		// user/group window search
		ResourcePickerDlg resourcePickerDlg =
			new ResourcePickerDlg( info, new SearchUG( this, info ),
								   getFrame() );
		resourcePickerDlg.appendSearchInterface( new DSSearchPanel() );
		resourcePickerDlg.show();
	}

	private void actionCreateRootEntry(String dn, IPage viewInstance) {
		/* See if it has the rights (only directory manager can perform this task) */
		LDAPConnection ldc = getServerInfo().getLDAPConnection();
		if (!DSUtil.isLocalDirectoryManager(ldc)) {
			DSUtil.showInformationDialog(getFrame(), "addRootEntry-needtobedirectorymanager", (String[])null, "dscontentmodel");
			return;
		}
		

		// Display the class chooser dialog
		LDAPSchema schema = getSchema();
		ChooseObjectClassDialog dlg = new ChooseObjectClassDialog(getFrame(), schema);
		dlg.show();
		dlg.dispose();
		
		if ( ! dlg.isCancel() ) {
			String selectedValue = dlg.getSelectedValue();
			String[] rdns = LDAPDN.explodeDN( dn, false );
			
			String namingAttribute = rdns[0].substring( 0, rdns[0].indexOf('=') );
			String value = rdns[0].substring( rdns[0].indexOf('=')+1 );
			
			Hashtable ht = new Hashtable();
			DSSchemaHelper.allRequiredAttributes(selectedValue, schema, ht);
			Enumeration e = ht.elements();
			
			LDAPAttributeSet set = new LDAPAttributeSet();
			String anAttr = null;

			LDAPAttribute attr = new LDAPAttribute( namingAttribute );
			attr.addValue(value);

			set.add(attr);			

			String[] aciValues = {"(targetattr != \"userPassword\") (version 3.0; acl \"Anonymous access\"; allow (read, search, compare)userdn = \"ldap:///anyone\";)",
								  "(targetattr != \"nsroledn||aci\")(version 3.0; acl \"Allow self entry modification except for nsroledn and aci attributes\"; allow (write)userdn =\"ldap:///self\";)",
								  "(targetattr = \"*\")(version 3.0; acl \"Configuration Adminstrator\"; allow (all) userdn = \"ldap:///"+getAdminDN()+"\";)",
								  "(targetattr =\"*\")(version 3.0;acl \"Configuration Administrators Group\";allow (all) (groupdn = \"ldap:///cn=Configuration Administrators, ou=Groups, ou=TopologyManagement, o=NetscapeRoot\");)",
								  "(targetattr = \"*\")(version 3.0; acl \"SIE Group\"; allow (all)groupdn = \"ldap:///"+getServerInfo().getCurrentDN()+"\";)"
			};


			attr = new LDAPAttribute ("aci", aciValues);
			
			set.add(attr);
			
			while( e.hasMoreElements() ) {
				String name = (String)e.nextElement();
				
				if (!name.equalsIgnoreCase (namingAttribute)) {
					attr = new LDAPAttribute( name );
					/* Initialize the objectclass attribute with the
					   chain of superiors */
					if ( name.equalsIgnoreCase( "objectclass" ) ) {
						Vector v = DSSchemaHelper.getObjectClassVector( selectedValue, schema );
						for( int i = v.size() - 1; i >= 0; i-- ) {
							attr.addValue( (String)v.elementAt( i ) );
						}
					} else  {
						/* Not objectclass nor the attribute we already added, initialize with the empty string */
						attr.addValue( "" );
						if ( (anAttr == null) && !name.equalsIgnoreCase( "aci" ) ) {
							anAttr = name;
						}
					}
					set.add( attr );
				}
			}
			LDAPEntry entry = new LDAPEntry( dn, set );
			IDSEntryObject parentNode = getParentNode(dn, (IDSEntryObject)getRoot());
			if (parentNode == null) {
				parentNode = (IDSEntryObject)getRoot();
			} 
			DSEntryObject deo = new DSEntryObject( this, 
												   entry,
												   parentNode.getReferralsEnabled());				
			deo.setParentNode( parentNode );
			
			if ( deo.addGenericNoNaming( true ) ) {
				_updateNewRootEntryMenu = true;
				parentNode.reload();
				updateObjectInTree(parentNode);				
			} else {
				return;
			}
			
			/* If we are here we added the suffix root entry:  now we try to see if the user can see it or not,
			   in order to display a dialog telling him/her why he/she can't see it */
			if (!_selectedPartitionView.equals(ALL)) {					
				LDAPSearchConstraints cons =
					(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
				byte[] vals = null;
				try {
					vals = _selectedPartitionView.getBytes( "UTF8" );
				} catch ( UnsupportedEncodingException uEEx ) {
					Debug.println( "DSContentModel.actionCreateRootEntry() "+uEEx);
					return;
				}			
				LDAPControl searchControl = new LDAPControl(DSEntryObject.SEARCH_OID, true, vals);
				cons.setServerControls(searchControl);
				cons.setMaxResults( 0 );
				try {										
					entry = DSUtil.readEntry(ldc, dn, null, cons);
					if (entry == null) {																			
						DSUtil.showInformationDialog(getFrame(),"add-entry-to-different-partition", (String)null);
					}
				} catch (LDAPException lde) {
					if (lde.getLDAPResultCode() == LDAPException.NO_SUCH_OBJECT) {
						DSUtil.showInformationDialog(getFrame(),"add-entry-to-different-partition", (String)null);
					} else {
						Debug.println("DSContentModel.actionCreateRootEntry() " +lde);
					}
				}
			}
		}
	}

	private void actionSelectView(String backend, IPage viewInstance) {
		if (!backend.equals(_selectedPartitionView)) {
			_selectedPartitionView = backend;
			newTree();
		}
	}

	private String getAdminDN() {
		LDAPConnection ldc = getConsoleInfo().getLDAPConnection();
		String adminDN ="uid=admin,ou=Administrators, ou=TopologyManagement, o=NetscapeRoot";
		
		LDAPSearchConstraints cons =
			(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
		cons.setMaxResults( 0 );


		String[] attrs = {"uniquemember"};
		try {
			LDAPEntry findEntry = DSUtil.readEntry(ldc, "cn=Configuration Administrators, ou=Groups, ou=TopologyManagement, o=NetscapeRoot", attrs, cons);
			if (findEntry!=null) {
				LDAPAttribute attr = findEntry.getAttribute( "uniquemember" );
				if ( attr != null ) {
					Enumeration en = attr.getStringValues();
					if ( (en != null) && en.hasMoreElements() )
						adminDN = (String)en.nextElement();
				}
			}
		} catch (LDAPException e) {
			Debug.println("DSContentModel.getAdminDN(): "+e);
		}

		return adminDN;
	}

    static boolean isCompatibleList( IResourceObject[] list ) {
		if ( list.length < 2 ) {
			return true;
		} else {
			long code = ((IDSEntryObject)list[0]).getObjectClassCode();
			for( int i = 1; i < list.length; i++ ) {
				if ( code != ((IDSEntryObject)list[i]).getObjectClassCode() )
					return false;
			}
			return true;
		}
	}

	/**
	 * Scan the list of entries and compose a new entry consisting of
	 * only the ones that have the same values.
	 *
	 * @param list List of directory entry objects.
	 * @param model Content Model.
	 * @param dn DN of the Directory entry
	 * @return A new object consisting of the intersection of the others.
	 */
	static IDSEntryObject getCommonAttributes( IResourceObject[] list,
											   IDSModel model,
											   String dn,
											   boolean referralsEnabled ) {
		if ( list.length < 1 )
			return null;
		else if ( list.length == 1 )
			return (IDSEntryObject)list[0];
		/* Clone the first one to use as a model */
		Hashtable h = new Hashtable();
		LDAPAttributeSet set = new LDAPAttributeSet();
		LDAPEntry entry = ((IDSEntryObject)list[0]).getEntry();
		if ( entry == null )
			return null;
		Enumeration e = entry.getAttributeSet().getAttributes();
//		Debug.println( "Reference attributes for " + entry.getDN() + ":" );
		while( e.hasMoreElements() ) {
			LDAPAttribute attr = (LDAPAttribute)e.nextElement();
			String attrName = attr.getName().toLowerCase();
			set.add( attr );
			int crc = DSUtil.getCRC32( attr );
			h.put( attrName, new Integer(crc) );
//			Debug.println( "  " + attrName + ": " + crc );
		}
		/* Now process the remaining entries, making sure they have
		   the same attributes and values */
		Vector v = new Vector(); /* Keep track of rejects */
		for( int i = 1; i < list.length; i++ ) {
			entry = ((IDSEntryObject)list[i]).getEntry();
			if (entry != null) {
	//			Debug.println( "Attributes of " + entry.getDN() + ":" );
				e = set.getAttributes();
				v.removeAllElements();
				while( e.hasMoreElements() ) {
					/* Get the next attribute from the reference set */
					LDAPAttribute setAttr = (LDAPAttribute)e.nextElement();
					String attrName = setAttr.getName().toLowerCase();
					/* Is the attribute present in the next entry in the list? */
					LDAPAttribute attr = entry.getAttribute( attrName );
					if ( attr == null ) {
						/* Not present; stop tracking this attribute */
						v.addElement( attrName );
					} else {
						/* Check if the checksums are the same */
						Integer oldInt = (Integer)h.get( attrName );
						int crc = DSUtil.getCRC32( attr );
	//					Debug.println( "  " + attrName + ": " + crc );
						if ( oldInt.intValue() != crc ) {
							/* Not the same, so stop tracking this attribute */
							v.addElement( attrName );
						}
					}
				}
				/* Remove all rejects */
				e = v.elements();
				while ( e.hasMoreElements() ) {
					String attrName = (String)e.nextElement();
					set.remove( attrName );
	//				Debug.println( "Removed " + attrName );
				}
			}
		}
		/* Now create a new entry with what is left */
		entry = new LDAPEntry( "", set );
		IDSEntryObject deo = new DSEntryObject( model, entry,
												referralsEnabled );
		/* Set its display name to the concatenation of all components */
		String s = ((IDSEntryObject)list[0]).getDisplayName();
		for( int i = 1; i < list.length; i++ ) {
			s += ", " + ((IDSEntryObject)list[i]).getDisplayName();
			if ( s.length() >= 40 ) {
				s += ", ...";
				break;
			}
		}
		deo.setDisplayName( s );
		return deo;
	}

	class SearchUG implements IRPCallBack {

		SearchUG( DSContentModel model, ConsoleInfo info ) {
			_model = model;
			_info = info;
		}

		class ResultsDialog extends AbstractDialog {
			ResultsDialog( Frame parentFrame, String title, int buttons ) {
				super( parentFrame, title, buttons );
			}

			ResultsDialog( Frame parentFrame, String title, int buttons,
						   Component focusComponent ) {
				this( parentFrame, title, buttons );
				_focusComponent = focusComponent;
			}

			class Fronter implements Runnable {
				Fronter() {
				}
				public void run() {
					try {
						Thread.sleep( 100 );
					} catch ( Exception e ) {
					}
					toFront();
					Thread.yield();
					_focusComponent.requestFocus();
					Debug.println(9,
						"DSContentModel.SearchUG.ResultsDialog.Fronter.run:" +
						"1 focus owner=" + getFocusOwner() +
						" isShowing=" + isShowing());
					_focusComponent.dispatchEvent(
						new FocusEvent(_focusComponent, FocusEvent.FOCUS_GAINED,
									   true));
					Thread.yield();
					Debug.println(9,
						"DSContentModel.SearchUG.ResultsDialog.Fronter.run:" +
						"2 focus owner=" + getFocusOwner() +
						" isShowing=" + isShowing());
				}
			}

			/**
			 * Override for JDialog
			 */
			public void show() {
				// This prevents the dialog from immediately going behind other windows
				// on the screen on unix
				if ( ! _isWindows ) {
					try {
						SwingUtilities.invokeLater(new Fronter());
					} catch ( Exception e ) {
					}
				}
				super.show();
			}

			private Component _focusComponent;
		}

		/**
		 * Callback from User/Group search dialog.
		 *
		 * @param vResult Selected items.
		 */
        public void getResults(Vector vResult) {
			Enumeration e = vResult.elements();
			Vector v = new Vector();
			while ( e.hasMoreElements() ) {
				LDAPEntry entry = (LDAPEntry)e.nextElement();
				Debug.println( "DSContentModel.getResults: selected " +
							   entry );
				v.addElement( DSEntryObjectFromEntry( entry ) );
			}
			vResult = null;

			DSEntryList list = new DSEntryList( v, _model, null );
 			String title = _resource.getString( "searchResults", "title" );
			ResultsDialog dlg = new ResultsDialog( _model.getFrame(),
												   title,
												   AbstractDialog.CLOSE,
												   list );
			dlg.setComponent( list );
			dlg.setFocusComponent( list );
 			dlg.setSize(400,200);
			dlg.show();
		}
		DSContentModel _model;
		ConsoleInfo _info;
	}

	void actionSelected( String cmd, IResourceObject[] selection,
						 IPage viewInstance ) {
		Debug.println( "DSContentModel.actionSelected: " +
					   cmd + " " + selection + 
                       "[" + ((selection != null) ? selection.length : 0) +
					   "] " + viewInstance );
		if ((cmd == null) || (selection == null)) {
			return;
		}
		/* Filter out null elements */
		boolean hasNulls = false;
		for( int i = 0; (i < selection.length) && !hasNulls; i++ ) {
			hasNulls = ( selection[i] == null );
		}
		if ( hasNulls ) {
			Vector v = new Vector();
			for( int i = 0; i < selection.length; i++ ) {
				if ( selection[i] != null ) {
					v.addElement( selection[i] );
				}
			}
			selection = new IResourceObject[v.size()];
			v.copyInto( selection );
		}
		if ( cmd.equals( OPEN ) ) {
			actionEdit( selection );

		} else if ( cmd.equals( AUTHENTICATE ) ) {
			actionAuthenticate( selection, viewInstance );

		} else if( cmd.equals( COPY ) ) {
			actionCopy( selection, viewInstance );

		} else if( cmd.equals( PASTE ) ) {
			setWaitCursor(true);
			actionPaste( selection, viewInstance );
			setWaitCursor(false);

		} else if( cmd.equals( CUT ) ) {
			actionCutDelete( selection, viewInstance, true );

		} else if( cmd.equals( DELETE ) ) {
			actionCutDelete( selection, viewInstance, false );

		} else if( cmd.equals( COPYDN ) ) {
			actionCopyDN( selection, viewInstance );

		} else if( cmd.equals( NEW_USER ) ) {
			actionNewUser( selection, viewInstance );

		} else if( cmd.equals( NEW_GROUP ) ) {
			actionNewGroup( selection, viewInstance );

		} else if( cmd.equals( NEW_ORGANIZATIONALUNIT ) ) {
			actionNewOrganizationalUnit( selection, viewInstance );

		} else if( cmd.equals( NEW_ROLE ) ) {
			actionNewRole( selection, viewInstance );

		} else if( cmd.equals( NEW_COS ) ) {
			actionNewCos( selection, viewInstance );

		} else if( cmd.equals( NEW_OBJECT ) ) {
			actionNewObject( selection, viewInstance );

		} else if( cmd.equals( ACL ) ) {
			actionACL( selection, viewInstance );

		} else if( cmd.equals( ROLES ) ) {
			actionRoles( selection, viewInstance );

		} else if( cmd.equals( SEARCH_UG ) ) {
			actionSearchUG( selection, viewInstance );

		} else if( cmd.equals( CREATE_INDEX ) ) {
			actionCreateIndex( selection, viewInstance );

		} else if( cmd.equals( DELETE_INDEX ) ) {
			actionDeleteIndex( selection, viewInstance );

		} else if ( cmd.equals( REFRESH ) ) {			
			if (selection != null) {
				if (selection != _localSelection) {
					/* We are calling the method from the right side (the DSEntryList) */
					for (int i=0; i < selection.length; i++) {
						if (selection[i] instanceof IDSEntryObject) {
							((IDSEntryObject)selection[i]).reset();
							if (((IDSEntryObject)selection[i]).getEntry() == null) {
								_dsEntryList.removeElement(selection[i]);
							}
						}
					}
					if (_dsEntryList != null) {
						_dsEntryList.repaint();
					}
				} else {					
					actionRefresh( selection, viewInstance );				
				}
			}
		} else if ( cmd.equals( REFERRALS ) ) {
		    actionReferrals();

		} else if ( cmd.equals( REFRESHTREE ) ) {			
			setSchema(null); /* Refresh the schema */
			_backends = MappingUtils.getBackendList(getServerInfo().getLDAPConnection(), MappingUtils.LDBM);
			_suffixes = MappingUtils.getSuffixList(getServerInfo().getLDAPConnection(), MappingUtils.ALL);
			_updateNewRootEntryMenu = true;			
		    newTree();

		} else if ( cmd.equals( ACTIVATE ) ) {
		    actionActivate(selection, viewInstance);
			refreshAfterActivation(selection, viewInstance);

		} else if ( cmd.equals( INACTIVATE ) ) {
		    actionInactivate(selection, viewInstance);
			refreshAfterActivation(selection, viewInstance);
			
		}  else if ( cmd.equals( ALL )) {
			actionSelectView(ALL, viewInstance);

		} else if (cmd.equals( VIEWACCOUNTINACTIVATION )) {			
			_isViewAccountInactivationSelected = !_isViewAccountInactivationSelected;
			_backends = MappingUtils.getBackendList(getServerInfo().getLDAPConnection(), MappingUtils.LDBM);
			_suffixes = MappingUtils.getSuffixList(getServerInfo().getLDAPConnection(), MappingUtils.ALL);
			_updateNewRootEntryMenu = true;
		    newTree();
		
		} else {
			if (_suffixWithNoEntryList != null) {
				if (_suffixWithNoEntryList.indexOf(cmd) >= 0) {
					actionCreateRootEntry(cmd, viewInstance);
				} else if (_backends != null) {
					for (int i=0; i<_backends.length; i++) {
						if (_backends[i].equals(cmd)) {
							actionSelectView(cmd, viewInstance);
							break;
						}
					}
				}
			}
		}
	}

/**
 * Method used to refresh the tree after inactivating/activating an entry.
 *
 * @param IResourceObject[] selection the nodes that have been activated/inactivated
 */
	private void refreshAfterActivation(IResourceObject[] selection, IPage viewInstance) {	
		if (selection != null) {
			boolean oneLocked = false;
			boolean oneNotLocked = false;
			boolean isLocked = false;
			IDSEntryObject deo;
			for(int i = 0; i < selection.length; i++ ) {			
				deo = (IDSEntryObject)selection[i] ;					
				if (deo != null) {
					LDAPEntry entry = deo.getEntry();
					if (entry != null) {
						AccountInactivation account = new AccountInactivation(entry);
						try {
							isLocked = account.isLocked(getServerInfo().getLDAPConnection());
							oneLocked = oneLocked || isLocked;
							oneNotLocked = oneNotLocked || !isLocked;
						} catch (LDAPException ex) {
							Debug.println("DSContentModel.refreshAfterActivation() "+ex);
						}
					}
				}	
			}
			if ( oneLocked ) {
				fireEnableMenuItem( viewInstance, ACTIVATE );									
			} else {
				fireDisableMenuItem( viewInstance, ACTIVATE );
			}
			if ( oneNotLocked ) {
				fireEnableMenuItem( viewInstance, INACTIVATE );									
			} else {
				fireDisableMenuItem( viewInstance, INACTIVATE);
			}
				
			if (selection == _localSelection) {				
				DN entryListParentDN = _dsEntryList.getParentDN();				
				boolean needToRepaintEntryList = false;
				for (int i=0; i<selection.length; i++) {
					deo = (IDSEntryObject)selection[i];
					if (deo != null) {					
						fireTreeNodeChanged((ResourceObject)deo);
						/* We check if the entry (on the tree on the left side) has a representation in the right panel (DSEntryList) we are displaying.
						   We see if the parent of the DSEntryList is the same parent of the entry: if it is the case the entry has a representation
						   on the right panel */
						DN entryDN = new DN(deo.getDN());						
						if ((entryDN != null) && (entryListParentDN != null)) {
							if (entryDN.getParent() != null) {
								if (entryDN.getParent().equals(entryListParentDN)) {
									/* We get the entry with the same dn in the right pane and we update it */
									IDSEntryObject entryListdeo = _dsEntryList.getEntryObject(deo.getDN());									
									if (entryListdeo != null) {
										needToRepaintEntryList = true;
										entryListdeo.reset();
										entryListdeo.getEntry();
									}
								}
							}
						}
					}
				}
				if (needToRepaintEntryList) {
					_dsEntryList.repaint();
				}
			} else {
				/* We are calling the method from the right side (the DSEntryList) */
				if (_dsEntryList != null) {
					_dsEntryList.repaint();
				}
				/* In the _localSelection we have the list of selected nodes on the left.  If one of the nodes that we have activated/inactivated
				   in the right panel is on the left, then this node is the child of one of those nodes. We look for these duplicated nodes in order
				   to set properly the icon (be coherent with what we have on the right and on the left of the pane) */
				if (_localSelection != null) {
					for (int k=0; k<_localSelection.length; k++) {
						IDSEntryObject leftSideParentNode = (IDSEntryObject)_localSelection[k];
						if (leftSideParentNode == null) {
							continue;
						}
						int childCount = leftSideParentNode.getChildCount();
						DN leftSideParentNodeDN = new DN(leftSideParentNode.getDN());											
						DN leftSideChildNodeDN;
						DN rightSideNodeDN;
						for (int j=0; j<selection.length; j++) {
							IDSEntryObject rightSideNode = (IDSEntryObject)selection[j];
							if (rightSideNode != null) {
								rightSideNodeDN = new DN(rightSideNode.getDN());
								/* If the node in _localSelection (selected on the left) we are handling is not the parent of one 
								   of the nodes in the selection (on the right pane),
								   then is not the parent of none of the entries in the right pane => we break*/
								if (!rightSideNodeDN.isDescendantOf(leftSideParentNodeDN)) {
									break;
								}								
								for (int i=0; i< childCount; i++) {
									IDSEntryObject leftSideChildNode = (IDSEntryObject) leftSideParentNode.getChildAt(i);		
									if (leftSideChildNode != null) {										
										leftSideChildNodeDN = new DN(leftSideChildNode.getDN());												
										if (rightSideNodeDN.equals(leftSideChildNodeDN)) {
											LDAPEntry entry = rightSideNode.getEntry();											
											/* We update the entry in the node in the left pane */
											if (entry != null) {												
												leftSideChildNode.setEntry(entry);
												leftSideChildNode.initializeFromEntry(entry);
											} else {
												leftSideChildNode.reset();
												leftSideChildNode.getEntry();
											}
											fireTreeNodeChanged((ResourceObject)leftSideChildNode);											
										}
									}
								}
							}
						}
					}
				}				
			}
		}
	}

    /**
      * Notification that the framework window is closing.
      * Called by ResourcePage
      */
	public void actionViewClosing(IPage viewInstance)
		        throws CloseVetoException {
		/* No unsaved changes in this model */
	}

    private IDSEntryObject DSEntryObjectFromEntry( LDAPEntry entry ) {
		DSEntryObject dseo = new DSEntryObject( this, entry,
												getReferralsEnabled() );
		return dseo;
	}

    static boolean entryHasChildren( LDAPEntry entry ) {
		boolean hasChildren = false;
		LDAPAttribute attr = entry.getAttribute(
			"numsubordinates" );
		if ( attr != null ) {
			Enumeration e = attr.getStringValues();
			if ( e.hasMoreElements() ) {
				String s = (String)e.nextElement();
				int count = Integer.parseInt( s );
				if ( count > 0 ) {
					hasChildren = true;
				}
			}
		}
		return hasChildren;
	}
	/**
	 * Class which can run as a thread under a progress dialog.  Used to inactivate a set of entries
	 */
class InactivateRunnable implements Runnable, ActionListener {
	InactivateRunnable(GenericProgressDialog dlg,
					   IResourceObject[] selection,
					   IPage viewInstance) {
		this.dlg = dlg;
		this.selection = selection;
		this.viewInstance = viewInstance;
	}
	public void run() {
		_taskCancelled = false;
		dlg.addActionListener(this);
		dlg.enableButtons(true);
		String[] attrs = {"nsrole", "nsroledn", "objectclass", "nsAccountLock"};
		LDAPConnection ldc = getServerInfo().getLDAPConnection();
		LDAPSearchConstraints cons =
			(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
		for (int i=0; i<selection.length; i++) {
			if (_taskCancelled) {
				break;				
			}
			/* We make sure we read all the attributes of the entry. 
			   We need this because when we activate/inactivate we need the values of nsrole... */
			LDAPEntry entry = null;
			try {				
				entry = DSUtil.readEntry(ldc,
										 ((IDSEntryObject)selection[i]).getDN(),
										 attrs,
										 cons);
				if (entry != null) {				
					AccountInactivation account = new AccountInactivation(entry);
					int result = account.inactivate(getServerInfo().getLDAPConnection());
					String abreviatedDN = DSUtil.abreviateString(entry.getDN(), 45);
					String[] arg = {abreviatedDN};
					dlg.setTextInLabel(_resource.getString("accountinactivation-inactivate", "entry", arg));
					switch (result) {              
					case AccountInactivation.INACTIVATED_THROUGH_UNKNOWN_MECHANICS:
						dlg.appendTextToTextArea(_resource.getString(
																	 "accountinactivation-inactivate", "inactivatedthroughunknownmechanics", abreviatedDN)+"\n");
						break;
					case AccountInactivation.ERROR:
						dlg.appendTextToTextArea(_resource.getString(
																	 "accountinactivation-inactivate", "unknown-error", abreviatedDN)+"\n");
						break;
					case AccountInactivation.CAN_NOT_INACTIVATE:
						dlg.appendTextToTextArea(_resource.getString(
																	 "accountinactivation-inactivate", "cannotinactivate", abreviatedDN)+"\n");
						break;
					case AccountInactivation.ROOT_OR_CONFIG_ENTRY:
						/* Assume we could not find the suffixes because the entry is in the config, is cn=config, is cn=schema, cn=monitor... */
						dlg.appendTextToTextArea(_resource.getString(
																	 "accountinactivation-inactivate", "rootorconfigentry", abreviatedDN)+"\n");
						break;
					case AccountInactivation.SUCCESS:						
						/* Update the DSEntryObject and to repaint the icon */
						IDSEntryObject deo = (IDSEntryObject)selection[i];
						deo.reset();
						deo.getEntry();				
						break;
					}
				}
			} catch (LDAPException e) {
				String abreviatedDN = DSUtil.abreviateString(entry.getDN(), 45);
				String ldapError =  e.errorCodeToString();
				String ldapMessage = e.getLDAPErrorMessage();
				if ((ldapMessage != null) &&
					(ldapMessage.length() > 0)) {
					ldapError = ldapError + ". "+ldapMessage;
				}
				String[] arg = {((IDSEntryObject)selection[i]).getDN(), ldapError};
				dlg.appendTextToTextArea(_resource.getString(
															 "accountinactivation-inactivate", "errorreadingentry", arg)+"\n");
			}
			String[] arg = {((IDSEntryObject)selection[i]).getDN(), "pipon"};
			dlg.appendTextToTextArea(_resource.getString(
														 "accountinactivation-inactivate", "errorreadingentry", arg)+"\n");
		}
		
		if (_taskCancelled) {
			dlg.closeCallBack();
		} else {
			dlg.setTextInLabel(_resource.getString("accountinactivation-inactivate", "finished"));
			dlg.waitForClose();
		} 
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(GenericProgressDialog.CANCEL)) {
			_taskCancelled = true;
			dlg.disableCancelButton();
		} else if (e.getActionCommand().equals(GenericProgressDialog.CLOSE)) {
			dlg.closeCallBack();
		}
	}

	boolean _taskCancelled = false;
	GenericProgressDialog dlg;
	IResourceObject[] selection;
	IPage viewInstance;
}


	/**
	 * Class which can run as a thread under a progress dialog.  Used to activate a set of entries
	 */
class ActivateRunnable implements Runnable, ActionListener {
	ActivateRunnable(GenericProgressDialog dlg,
					   IResourceObject[] selection,
					   IPage viewInstance) {
		this.dlg = dlg;
		this.selection = selection;
		this.viewInstance = viewInstance;
	}
	public void run() {
		_taskCancelled = false;
		dlg.addActionListener(this);
		
		dlg.enableButtons(true);
		String[] attrs = {"nsrole", "nsroledn", "objectclass", "nsAccountLock"};
		LDAPConnection ldc = getServerInfo().getLDAPConnection();
		LDAPSearchConstraints cons =
			(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
		for (int i=0; i<selection.length; i++) {
			if (_taskCancelled) {
				break;				
			}
			/* We make sure we read all the attributes of the entry. 
			   We need this because when we activate/inactivate we need the values of nsrole... */
			LDAPEntry entry = null;
			try {				
				entry = DSUtil.readEntry(ldc,
										 ((IDSEntryObject)selection[i]).getDN(),
										 attrs,
										 cons);			
				if (entry != null) {				
					AccountInactivation account = new AccountInactivation(entry);
					int result = account.activate(getServerInfo().getLDAPConnection());
					String abreviatedDN = DSUtil.abreviateString(entry.getDN(), 45);
					String[] arg = {abreviatedDN};
					dlg.setTextInLabel(_resource.getString("accountinactivation-activate", "entry", arg));
					switch (result) {            
					case AccountInactivation.INACTIVATED_THROUGH_UNKNOWN_MECHANICS:
						dlg.appendTextToTextArea(_resource.getString(
																	 "accountinactivation-activate", "inactivatedthroughunknownmechanics", abreviatedDN)+"\n");
					case AccountInactivation.ERROR:
						dlg.appendTextToTextArea(_resource.getString(
																	 "accountinactivation-activate", "unknown-error", abreviatedDN)+"\n");
						break;
					case AccountInactivation.CAN_NOT_ACTIVATE:
						Vector vLockingRoles = null;
						try {
							vLockingRoles = account.getLockingRoles(getServerInfo().getLDAPConnection());
						} catch (LDAPException ex) {
							Debug.println("DSContentModel.actionActivate() "+ex);					
						}
						if (vLockingRoles != null) {
							String sLockingRoles = "";
							for (int j=0; j < vLockingRoles.size(); j++) {
								sLockingRoles = sLockingRoles + 
									"'" + 
									DSUtil.abreviateString((String)vLockingRoles.elementAt(j), 45) +
									"'"+ 
									"\n";
							}
							String[] attributes = {abreviatedDN, sLockingRoles};
							dlg.appendTextToTextArea(_resource.getString(
																		 "accountinactivation-activate", "cannotactivate", attributes)+"\n");
						} else {
							dlg.appendTextToTextArea(_resource.getString(
																		 "accountinactivation-activate", "cannotactivate-lockingrolesnotfound", abreviatedDN)+"\n");
						}
						break;
					case AccountInactivation.ROOT_OR_CONFIG_ENTRY:
						/* Assume we could not find the suffixes because the entry is in the config, is cn=config, is cn=schema, cn=monitor... */
						dlg.appendTextToTextArea(_resource.getString(
																	 "accountinactivation-activate", "rootorconfigentry", abreviatedDN)+"\n");
						break;
					case AccountInactivation.SUCCESS:
						/* Update the DSEntryObject and to repaint the icon */
						IDSEntryObject deo = (IDSEntryObject)selection[i];					
						deo.reset();
						deo.getEntry();									
						break;					
					}
				}
			} catch (LDAPException e) {
				String abreviatedDN = DSUtil.abreviateString(entry.getDN(), 45);
				String ldapError =  e.errorCodeToString();
				String ldapMessage = e.getLDAPErrorMessage();
				if ((ldapMessage != null) &&
					(ldapMessage.length() > 0)) {
					ldapError = ldapError + ". "+ldapMessage;
				}
				String[] arg = {((IDSEntryObject)selection[i]).getDN(), ldapError};
				dlg.appendTextToTextArea(_resource.getString(
															 "accountinactivation-activate", "errorreadingentry", arg)+"\n");
			}
			String[] arg = {((IDSEntryObject)selection[i]).getDN(), "pipon"};
			dlg.appendTextToTextArea(_resource.getString(
														 "accountinactivation-activate", "errorreadingentry", arg)+"\n");
		} 
		if (_taskCancelled) {
			dlg.closeCallBack();
		} else {
			dlg.setTextInLabel(_resource.getString("accountinactivation-activate", "finished"));
			dlg.waitForClose();
		} 
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(GenericProgressDialog.CANCEL)) {
			_taskCancelled = true;
			dlg.disableCancelButton();
		} else if (e.getActionCommand().equals(GenericProgressDialog.CLOSE)) {
			dlg.closeCallBack();
		}
	}

	boolean _taskCancelled = false;
	GenericProgressDialog dlg;
	IResourceObject[] selection;
	IPage viewInstance;
}




	/**
	 * Class which can run as a thread under a progress dialog
	 */
    class TreeDeleter implements Runnable, ActionListener {
        TreeDeleter( IResourceObject[] deos, DSContentModel model, GenericProgressDialog dlg ) {
            this.deos = deos;
            this.model = model;
			_treeDeleterDlg = dlg;			
        }
	    public void run() {
			_numberDeletedObjects = 0;
			_taskCancelled = false;
			_treeDeleterDlg.addActionListener(this);
			boolean done = false;
			int i = 0;
			IDSEntryObject deo = (IDSEntryObject)deos[i];
			IDSEntryObject parent = (IDSEntryObject)deo.getParentNode();
			String dn = (String)getServerInfo().get( "rootdn" );
			while ( !done ) {				
				deo = (IDSEntryObject)deos[i];
				try {
					_status = deleteTree( deo );
					if ( _status == 0 ) {
						removeFromSelection( deo );
					}
					i++;
					if ( (_status == -1) || (i >= deos.length) || _taskCancelled )
						done = true;
				} catch ( LDAPException e ) {
					Debug.println("TreeDeleter.run(): "+e);
					_treeDeleterDlg.appendTextToTextArea(deo.getDN()+": "+e+"\n");
					i++;
					
					if ((i >= deos.length) || _taskCancelled) {
						done = true;
					}
				}
			}
			if (_taskCancelled) {
				_treeDeleterDlg.closeCallBack();
			} else {				
				String[] arg = {String.valueOf(getNumberDeletedObjects())};				
				_treeDeleterDlg.setTextInLabel(_resource.getString("browser",
																   "deleted-objects-label",
																   arg));
				_treeDeleterDlg.waitForClose();
			} 
			model.notifyAuthChangeListeners();
		}
   
	    public int getStatus() {
			return _status;
		}
		
		public int getNumberDeletedObjects() {
			return _numberDeletedObjects;
		}

		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals(GenericProgressDialog.CANCEL)) {
				_taskCancelled = true;
				_treeDeleterDlg.disableCancelButton();
			} else if (e.getActionCommand().equals(GenericProgressDialog.CLOSE)) {
				_treeDeleterDlg.closeCallBack();
			}
		}

	    int _numberDeletedObjects = 0;
		private int _status = 1;
		private IResourceObject[] deos;
		private DSContentModel model;
		private boolean _taskCancelled;
		private GenericProgressDialog _treeDeleterDlg;
		/**
		 * Delete a leaf node or an entire subtree.
		 *
		 * @param deo Directory Entry Object.
		 */
        int deleteTree( IDSEntryObject deo )
			throws LDAPException {
				Debug.println( "DSContentModel.deleteTree:  " +
							   deo.getEntry() );				
				String s = deo.getDN();
				int ret = 1;
				LDAPConnection ldc = model.getServerInfo().getLDAPConnection();
				if ( ldc == null )
					return ret;
				LDAPSearchConstraints cons =
					(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
				if ( !getReferralsEnabled() ) {
					cons.setServerControls( _manageDSAITControl );
				}
				Vector containerChildren = new Vector();
				// If this is a container node, delete the contents first
				LDAPEntry entry = deo.getEntry();
				if ( (entry != null) && model.entryHasChildren( entry ) ) {
					LDAPSearchResults search_results = null;
					String[] attrs = { "numsubordinates" };
					cons.setMaxResults( 0 );
					search_results = ldc.search( s,
												 LDAPConnection.SCOPE_ONE,
												 "(|(objectclass=*)(objectclass=ldapsubentry))",
												 attrs,
												 false,
												 cons );
					
					/* Delete each child found, recursively */
					while ( search_results.hasMoreElements() ) {
						/* Get the next child */
						entry = (LDAPEntry)search_results.nextElement();
						if ( model.entryHasChildren( entry ) ) {
							if (_taskCancelled) {
								Debug.println("TreeDeleter.deleteTree() cancelled after searching " + entry.getDN());
								ldc.abandon(search_results);
								return -1;
							}
							containerChildren.addElement( entry );							
						} else {
							try {
								String dn = entry.getDN();								
								ldc.delete( dn, cons );
								String[] arg = {DSUtil.abreviateString(dn, 45)};
								String msg = _resource.getString("browser", 
																 "deleting-object-label",
																 arg);
								_treeDeleterDlg.setTextInLabel(msg);
								_numberDeletedObjects++;
								if (_taskCancelled) {
									Debug.println("TreeDeleter.deleteTree() cancelled after deleting " + dn);
									ldc.abandon(search_results);
									return -1;
								}
							} catch ( LDAPException e ) {
								Debug.println("TreeDeleter.deleteTree: " +
											  "error deleting entry=" +
											  entry.getDN() +
											  ":" + e);
								_treeDeleterDlg.appendTextToTextArea(entry.getDN()+": "+e+"\n");
								ldc.abandon(search_results);
								return 2;
							}
						}
					}					
				}

				/* Recursively delete container entries bellow this entry...*/
				for (int i=0; i<containerChildren.size(); i++) {
					IDSEntryObject newObject =
						model.DSEntryObjectFromEntry( (LDAPEntry)containerChildren.elementAt(i) );
					deleteTree( newObject );
					if (_taskCancelled) {						
						return -1;
					}
				}

				// And so this container node				
				ldc.delete( s, cons );
				ret = 0; // if we got here, assume success i.e. delete did not
						 // throw an exception . . .
				String[] arg = {DSUtil.abreviateString(s, 45)};
				String msg = _resource.getString("browser", 
												 "deleting-object-label",
												 arg);
				_treeDeleterDlg.setTextInLabel(msg);
				_numberDeletedObjects++;
				if (_taskCancelled) {
					Debug.println("TreeDeleter.deleteTree() cancelled after deleting " + s);
					return -1;
				}
				return ret;
		}
    }


	/**
	 * Paste a leaf node or an entire subtree.
	 *
	 * @param dn Distinguished name of parent.
	 * @param rootNode Entry node of root of tree.
	 */
	LDAPEntry pasteTree( String dn, EntryNode node ) throws LDAPException {
		LDAPEntry entry = node.getEntry();
		if (entry == null) {
			// We throw an LDAPException to make the caller fail cleanly
			throw new LDAPException("getEntry() returned null to pasteTree()");
		}
		
		/* Create a new entry with the specified parent */
		String[] rdns = LDAPDN.explodeDN( entry.getDN(),
										  false );
		String newDN = rdns[0] + "," + dn;
		LDAPAttributeSet set = entry.getAttributeSet();
		/* We get rid of the operational attributes (trying
		 to add them generates a protocol error) */
		String[] operationalAttributes = DSSchemaHelper.getOperationalAttributes(getSchema());
		if (operationalAttributes != null) {
			for (int i=0; i < operationalAttributes.length; i++) {
				set.remove(operationalAttributes[i]);				
			}
		}
		LDAPEntry newEntry = new LDAPEntry( newDN, set);						    
		Debug.println( "DSContentModel.pasteTree: Adding " + newDN );		
		LDAPConnection ldc = getServerInfo().getLDAPConnection();
		try {
			ldc.add( newEntry );
		} catch (LDAPException e) {
			Debug.println(0, "DSContentModel.pasteTree() adding "+newEntry+" Exception: " +e);
			throw e;
		}
		// If this is a container node, probe the children
		for ( int i = 0; i < node.getChildCount(); i++ ) {
			EntryNode child = node.getChildAt( i );
			pasteTree( newDN, child );
		}
		return newEntry;
	}

	/**
	 * Copy a leaf node or an entire subtree to the clipboard.
	 *
	 * @param deo Directory Entry Object.
	 */
    private void copyTree( IDSEntryObject deo ) {
		/* Force fetching of entire entry if not already loaded */
		if ( !deo.isLoaded() ) {
			deo.reset();
		}
		LDAPEntry entry = deo.getEntry();
		if (entry != null) {
			EntryNode o = copyRecord( null, entry );
			_clipboard.addElement( o );
		}		
	}

	/**
	 * Paste a leaf node or an entire subtree.
	 *
	 * @param deo Directory Entry Object.
	 */
    private EntryNode copyRecord( EntryNode parent, LDAPEntry rootEntry ) {
//		Debug.println( "To clipboard: " + rootEntry.getDN() + ": " +
//			rootEntry );
		EntryNode node = new EntryNode( parent, rootEntry );
		// If this is a container node, probe the children
		if ( entryHasChildren( rootEntry ) ) {
			LDAPConnection ldc = getServerInfo().getLDAPConnection();
			LDAPSearchConstraints cons =
				(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
			cons.setMaxResults( 0 );
			if ( !getReferralsEnabled() ) {
				cons.setServerControls( _manageDSAITControl );
			}
			LDAPSearchResults search_results = null;
			String[] attrs = { "*", "numsubordinates" };
			try {
				search_results = ldc.search( rootEntry.getDN(),
											 ldc.SCOPE_ONE,
											 "(|(objectclass=*)(objectclass=ldapsubentry))",
											 attrs,
											 false,
											 cons );
			} catch (LDAPException e) {
				Debug.println( "Failed to search - " + e.toString() );
				return null;
			}

			// Add each record found
			while ( search_results.hasMoreElements() ) {
				/* retrieve the search response */
				LDAPEntry entry = (LDAPEntry)search_results.nextElement();
				node.addChild( copyRecord( node, entry ) );
			}
		}
		return node;
	}

	/**
	 * Set a parameter for future searches, which determines if the
	 * ManagedSAIT control is sent with each search. If referrals are
	 * disabled, the control is sent and you will receive the referring
	 * entry back.
	 *
	 * @param on true (the default) if referrals are to be followed
	 */
    public void setReferralsEnabled( boolean on ) {
		_followReferrals = on;
		/* Notify nodes */
		IDSEntryObject deo = (IDSEntryObject)getRoot();
		if ( deo != null ) {
			Debug.println(9, "DSContentModel.setReferralsEnabled: " +
						  "setting referrals on=" + on + " for " +
						  deo);
			deo.propagateReferralsEnabled( on );
		} else {
			Debug.println(9, "DSContentModel.setReferralsEnabled: " +
						  "deo is null");
		}
	}

	/**
	 * Get the parameter which determines if the
	 * ManagedSAIT control is sent with each search.
	 *
	 * @returns true if referrals are to be followed
	 */
    public boolean getReferralsEnabled() {
		return _followReferrals;
	}

	/**
	 * Get the parameter which determines which is the type of view (all partitions
	 * or single partition view) we have.
	 *
	 * @returns a String saying which is the view, the name of the partition or DSContentModel.ALL if we are viewing all the partitions
	 */
    public String getSelectedPartitionView() {
		return _selectedPartitionView;
	}
	/**
	 * Get in a Vector the list of suffix without an associated LDAPEntry
	 *
	 * @returns a Vector with the list of suffix without an associated LDAPEntry
	 */
	public Vector getSuffixWithNoEntryList() {
		return _suffixWithNoEntryList;
	}
								
	/**
	 * Tells that the new Root Entry Menu should be updated or not
	 *
	 * @param boolean saying if we want to update or not
	 */
	public void updateNewRootEntryMenu(boolean state) {
		_updateNewRootEntryMenu = state;		
	}

	/** 
	  * Tells if the clipboard is empty or not
	  *
	  * @return true if the clipboard is null or empty.  False otherwise
	  */
	public boolean isClipboardEmpty() {
		return ((_clipboard == null) || (_clipboard.size() < 1));
	}

	private void createNewRootEntryMenuItems() {
		Debug.println("DSContentModel.createNewRootEntryMenuItems()");
		if (_suffixWithNoEntryList == null) {
			_suffixWithNoEntryList = getSuffixesWithNoEntry();
		}
		int size = _suffixWithNoEntryList.size();
	   
		_objectNewRootEntryMenuItems = new IMenuItem[size];
		_contextNewRootEntryMenuItems = new IMenuItem[size];

		for (int i=0; i< _objectNewRootEntryMenuItems.length ; i++) {
			String suffix = (String)(_suffixWithNoEntryList.elementAt(i));
			_objectNewRootEntryMenuItems[i] = (IMenuItem) new MenuItemText((String)(suffix),
																		   (String)(suffix),
																		   _resource.getString("menu",
																							   "EditNewRoot-description",suffix));
			_contextNewRootEntryMenuItems[i] = (IMenuItem) new MenuItemText((String)(suffix),
																		   (String)(suffix),
																		   _resource.getString("menu",
																							   "EditNewRoot-description",suffix));
		}
	}

	private IMenuItem[] createPartitionViewMenuItems() {
		Debug.println("DSContentModel.createPartitionViewMenuItems()");
		if (_backends == null) {
			_backends = MappingUtils.getBackendList(getServerInfo().getLDAPConnection(), MappingUtils.LDBM);
		}
		IMenuItem[] items = null;
		if ((_backends != null) && (_backends.length > 0)) {
			items = new IMenuItem[_backends.length + 2];
		} else {
			items = new IMenuItem[1];
		}
		boolean state = false;						
		
		ButtonGroup group = new ButtonGroup();
		
		state = _selectedPartitionView.equals(ALL);
		String backend = _resource.getString("menu", "PartitionView-all");
		items[0] = new MenuItemRadioButton(ALL,
										   backend,
											_resource.getString("menu",
																"PartitionView-all-description",backend),
										   state);
		group.add((MenuItemRadioButton)items[0]);
		if (items.length > 1) {
			items[1] = new MenuItemSeparator();
		}
		for (int i=2; i< items.length ; i++) {
			backend = _backends[i-2];
			state = _selectedPartitionView.equals(backend);
			items[i] = new MenuItemRadioButton(backend,
											   backend,
											   _resource.getString("menu",
																   "PartitionView-description",backend),
											   state);
			group.add((MenuItemRadioButton)items[i]);
		}			
		return (IMenuItem[])items;
	}
	private IMenuItem[] createViewMenuItems() {
		String ref = _resource.getString( "menu", "referrals" );
		String refDesc =
			_resource.getString("menu", "referrals-description");
		_menuReferrals = new MenuItemCheckBox( REFERRALS, ref, refDesc,
											   getReferralsEnabled() );
		return new IMenuItem[] {				
			_menuReferrals,
				new MenuItemCheckBox( VIEWACCOUNTINACTIVATION,
									  _resource.getString("menu", "viewaccountinactivation"),
									  _resource.getString("menu",
														  "viewaccountinactivation-description"),
									  _isViewAccountInactivationSelected ),
				new MenuItemSeparator(),
				new MenuItemCategory( PARTITIONVIEW,
									  _resource.getString("menu",
														  "PartitionView") ),
				new MenuItemSeparator(),
				new MenuItemText( REFRESHTREE,
								  _resource.getString("menu", "refresh-all"),
								  _resource.getString("menu",
													  "refresh-all-description")),				
				};
	}
						
								/** 
								 *  If one of the selected nodes is the root entry this function returns true.
								 */
	private boolean isRoot() {
		boolean isRoot = false;
		if (_selection != null) {
			for( int i = 0; i < _selection.length; i++ ) {
				if ( _selection[i] == null ) {				
					break;
			}
				String dn = ((IDSEntryObject)_selection[i]).getDN();
				isRoot = (dn == null) || dn.equals("");
			if (isRoot) {
				break;
			}
			}
		}
		return isRoot;
	}

    /**
	 * This method returns the list of suffixes.  It is used by DSEntryObject.  This has been done in order to minimize the calls
	 * to MappingUtils.getSuffixList() as we only update it when this page is selected, or the view refresh menu item is 
	 * selected.
	 */
	public static String[] getSuffixes() {
		return _suffixes;
	}

	/**
	 * Method called by DSEntryObject.  It is used to have a pointer to the currently displaying 
	 * DSEntryList (in the right side panel).  This pointer is mainly used to repaint the objects.
	 */
	public void setDSEntryList(DSEntryList dsEntryList) {
		_dsEntryList = dsEntryList;
	}

	/**
	 * This method returns the parent node in the JTree for a given dn.  The user has to provide the
	 * node in JTree from which we have to start.
	 *
	 * @param String dn, the dn of the entry 
	 * @param startingNode: the node of the JTree from which we start looking
	 * @returns the node that is the parent of the given dn.  If this node is not found, we return the most
	 * near parent (for example if we look for the parent of dn="dc=sun, dc=com" which is a naming context,
	 * we will return the node from which we start the research (should be the root node).
	 **/
	private IDSEntryObject getParentNode(String dn, IDSEntryObject startingNode) {		
		DN entryDN = new DN(dn);
		DN parentDN = entryDN.getParent();
		for (int i = 0; i< startingNode.getChildCount(); i++) {
			DN nodeDN = new DN(((IDSEntryObject)startingNode.getChildAt(i)).getDN());
			if (nodeDN.equals(parentDN)) {
				return (IDSEntryObject)startingNode.getChildAt(i);
			} else if (parentDN.isDescendantOf(nodeDN)) {
				return getParentNode(dn, (IDSEntryObject)startingNode.getChildAt(i));
			}
		}
		return startingNode;
	}

    /**
	 * Light-weight tree node class to track subtrees of entries.
	 */
	class EntryNode {
		EntryNode( EntryNode parent, LDAPEntry entry ) {
			_parent = parent;
			_entry = entry;
		}

		void addChild( EntryNode child ) {
			if ( child != null ) {
				if ( _children == null )
					_children = new Vector();
				_children.addElement( child );
			}
		}

		EntryNode getChildAt( int index ) {
			if ( _children != null )
				return (EntryNode)_children.elementAt( index );
			return null;
		}

		int getChildCount() {
			if ( _children == null )
				return 0;
			return _children.size();
		}

		EntryNode getParent() {
			return _parent;
		}

		LDAPEntry getEntry () {
			return _entry;
		}

	    private EntryNode _parent = null;
		private LDAPEntry _entry = null;
		private Vector _children = null;
	}
	private DSEntryList _dsEntryList;
	private boolean _updateNewRootEntryMenu = true;
	private boolean _isViewAccountInactivationSelected = false;
	private Vector _suffixWithNoEntryList = null;
	private String[] _backends = null;
	private static String[] _suffixes = null;
	private String _selectedPartitionView = ALL;
	private MenuItemCheckBox _menuReferrals = null;
	private String _refText = null;
	private String _refDescText = null;
	static final private boolean _updateOnSelect =
	             (System.getProperty("updateOnSelect") != null);
	static private boolean _initalizedGlobals = false;
	private Vector _clipboard = new Vector(); /* For copy/paste */
	private boolean _keyListenersInitialized = false;
	private IResourceObject[] _localSelection = new IResourceObject[0];
	private static LDAPControl _manageDSAITControl =
			new LDAPControl( LDAPControl.MANAGEDSAIT, true, null );
	private boolean _followReferrals = true;

	static final String CREATE_INDEX = "createIndex";
	static final String DELETE_INDEX = "deleteIndex";
	public static final String CONTEXTNEW = "CONTEXTNEW";
	static final String CONTEXTNEWROOTENTRY = "CONTEXTNEWROOTENTRY";
	static final String OBJECTNEW = "OBJECTNEW";
	static final String OBJECTNEWROOTENTRY = "OBJECTNEWROOTENTRY";
	static final String REFRESHTREE = "REFRESHTREE";
	static final String REFERRALS = "REFERRALS";
	static final String COPYDN = "copydn";
	static final String PARTITIONVIEW = "PARTITIONVIEW";
	static final String VIEWACCOUNTINACTIVATION = "VIEWACCOUNTINACTIVATION";
	public static final String ACTIVATE = "activate";
	public static final String INACTIVATE = "inactivate";
	public static final String ALL = "Show All Partitions";

	// this is required for the dialog funny business
    private static final boolean _isWindows =
	    ( java.io.File.separator.equals("\\") );

	private String[] _categoryID = null;
	IMenuItem[] _viewMenuItems;
	IMenuItem[] _partitionViewMenuItems;
	IMenuItem[] _objectNewRootEntryMenuItems;
	IMenuItem[] _contextNewRootEntryMenuItems;
	IMenuItem[] _fileMenuItems = new IMenuItem[] {
				new MenuItemText( AUTHENTICATE,
								  _resource.getString("menu", "authenticate"),
								  _resource.getString("menu",
											  "authenticate-description")),
				new MenuItemSeparator(),
			};

	 IMenuItem[] _contextMenuItems = new IMenuItem[] {			
				new MenuItemText( OPEN,
								  _resource.getString("menu", "properties"),
								  _resource.getString("menu",
													  "properties-description")),
				new MenuItemText( SEARCH_UG,
								  _resource.getString("menu", "EditFindUG"),
								  _resource.getString("menu",
													  "EditFindUG-description")),
				new MenuItemSeparator(),										
				new MenuItemCategory( CONTEXTNEW,
									  _resource.getString("menu",
														  "EditNew") ),		
				new MenuItemCategory( CONTEXTNEWROOTENTRY,
									  _resource.getString("menu",
														  "EditNewRootEntry") ),
				
				new MenuItemSeparator(),
				new MenuItemText( ACL,
								  _resource.getString("menu", "editacls"),
								  _resource.getString("menu",
													  "editacls-description")),
				new MenuItemText( ROLES,
								  _resource.getString("menu", "editroles"),
								  _resource.getString("menu",
													  "editroles-description")),
				new MenuItemSeparator(),
				new MenuItemText( CREATE_INDEX,
								  _resource.getString("menu", "createIndex"),
								  _resource.getString("menu",
													  "createIndex-description")),
				new MenuItemText( DELETE_INDEX,
								  _resource.getString("menu", "deleteIndex"),
								  _resource.getString("menu",
													  "deleteIndex-description")),
				new MenuItemSeparator(),
				new MenuItemText( ACTIVATE,
								  _resource.getString("menu", "activate"),
								  _resource.getString("menu", "activate-description")),
				new MenuItemText( INACTIVATE,
								  _resource.getString("menu", "inactivate"),
								  _resource.getString("menu", "inactivate-description")),
				new MenuItemSeparator(),
					new MenuItemText( CUT,
							  _resource.getString("menu", "EditCut"),
							  _resource.getString("menu",
												  "EditCut-description")),
				new MenuItemText( COPY,
								  _resource.getString("menu", "EditCopy"),
								  _resource.getString("menu",
													  "EditCopy-description")),
				new MenuItemText( PASTE,
								  _resource.getString("menu", "EditPaste"),
								  _resource.getString("menu",
													  "EditPaste-description")),
				new MenuItemText( DELETE,
								  _resource.getString("menu", "EditDelete"),
								  _resource.getString("menu",
													  "EditDelete-description")),
				new MenuItemSeparator(),
				new MenuItemText( REFRESH,
								  _resource.getString("menu", "refresh"),
								  _resource.getString("menu",
													  "refresh-description")), 	
				};

	  IMenuItem[]	_contextNewMenuItems =  new IMenuItem[] {
			
			new MenuItemText( NEW_USER,
							  _resource.getString("menu", "EditNewUser"),
							  _resource.getString("menu",
												  "EditNewUser-description")),
				
				new MenuItemText( NEW_GROUP, 
								  _resource.getString("menu", "EditNewGroup"),
								  _resource.getString("menu",
													  "EditNewGroup-description")),
				new MenuItemText( NEW_ORGANIZATIONALUNIT, 
								  _resource.getString("menu", "EditNewOu"),
								  _resource.getString("menu",
													  "EditNewOu-description")),
				new MenuItemSeparator(),
				new MenuItemText( NEW_ROLE, 
								  _resource.getString("menu", "EditNewRole"),
								  _resource.getString("menu",
													  "EditNewRole-description")),
				new MenuItemText( NEW_COS, 
								  _resource.getString("menu", "EditNewCos"),
								  _resource.getString("menu",
													  "EditNewCos-description")),
				new MenuItemSeparator(),
				new MenuItemText( NEW_OBJECT,
								  _resource.getString("menu", "EditNewObject"),
								  _resource.getString("menu",
													  "EditNewObject-description")),
				};

	IMenuItem[] _objectNewMenuItems =  new IMenuItem[] {

				new MenuItemText( NEW_USER,
								  _resource.getString("menu", "EditNewUser"),
								  _resource.getString("menu",
											  "EditNewUser-description")),

				new MenuItemText( NEW_GROUP, 
								  _resource.getString("menu", "EditNewGroup"),
								  _resource.getString("menu",
											  "EditNewGroup-description")),
				new MenuItemText( NEW_ORGANIZATIONALUNIT, 
								  _resource.getString("menu", "EditNewOu"),
								  _resource.getString("menu",
											  "EditNewOu-description")),
				new MenuItemSeparator(),
				new MenuItemText( NEW_ROLE, 
								  _resource.getString("menu", "EditNewRole"),
								  _resource.getString("menu",
											  "EditNewRole-description")),
				new MenuItemText( NEW_COS, 
								  _resource.getString("menu", "EditNewCos"),
								  _resource.getString("menu",
											  "EditNewCos-description")),
				new MenuItemSeparator(),
				new MenuItemText( NEW_OBJECT,
								  _resource.getString("menu", "EditNewObject"),
								  _resource.getString("menu",
											  "EditNewObject-description")),
					};

         IMenuItem[] _editMenuItems = new IMenuItem[] {
                                new MenuItemSeparator(),
/*
                                new MenuItemText( UNDO,
                                                                  _resource.getString("menu", "EditUndo"),
                                                                  _resource.getString("menu",
                                                                                          "EditUndo-description")),
*/
                                new MenuItemText( CUT,
                                                                  _resource.getString("menu", "EditCut"),
                                                                  _resource.getString("menu",
                                                                                          "EditCut-description")),
                                new MenuItemText( COPY,
                                                                  _resource.getString("menu", "EditCopy"),
                                                                  _resource.getString("menu",
                                                                                          "EditCopy-description")),
                                new MenuItemText( PASTE,
                                                                  _resource.getString("menu", "EditPaste"),
                                                                  _resource.getString("menu",
                                                                                          "EditPaste-description")),
                                new MenuItemText( DELETE,
                                                                  _resource.getString("menu", "EditDelete"),
                                                                  _resource.getString("menu",
                                                                                          "EditDelete-description")),
                                new MenuItemText( COPYDN,
                                                                  _resource.getString("menu", "EditCopyDN"),
                                                                  _resource.getString("menu",
                                                                                          "EditCopyDN-description")),
                        };


	 IMenuItem[] _objectMenuItems = new IMenuItem[] {				
				new MenuItemText( OPEN,
								  _resource.getString("menu", "properties"),
								  _resource.getString("menu",
													  "properties-description")),
					new MenuItemText( SEARCH_UG,
									  _resource.getString("menu", "EditFindUG"),
									  _resource.getString("menu",
														  "EditFindUG-description")),
					new MenuItemSeparator(),
					new MenuItemCategory( OBJECTNEW,
											  _resource.getString("menu",
																  "EditNew") ),
					new MenuItemCategory( OBJECTNEWROOTENTRY,
										  _resource.getString("menu",
															  "EditNewRootEntry") ),					
					new MenuItemSeparator(),
					new MenuItemText( ACL,
									  _resource.getString("menu", "editacls"),
									  _resource.getString("menu",
														  "editacls-description")),
					new MenuItemText( ROLES,
									  _resource.getString("menu", "editroles"),
									  _resource.getString("menu",
														  "editroles-description")),
					new MenuItemSeparator(),
					new MenuItemText( CREATE_INDEX,
									  _resource.getString("menu", "createIndex"),
									  _resource.getString("menu",
														  "createIndex-description")),
					new MenuItemText( DELETE_INDEX,
									  _resource.getString("menu", "deleteIndex"),
									  _resource.getString("menu",
														  "deleteIndex-description")),
					new MenuItemSeparator(),
					new MenuItemText( ACTIVATE,
									  _resource.getString("menu", "activate"),
									  _resource.getString("menu", "activate-description")),
					new MenuItemText( INACTIVATE,
									  _resource.getString("menu", "inactivate"),
									  _resource.getString("menu", "inactivate-description")),
					new MenuItemSeparator(),
					new MenuItemText( REFRESH,
									  _resource.getString("menu", "refresh"),
									  _resource.getString("menu",
														  "refresh-description")),					

					};



	public class MenuItemRadioButton extends JRadioButtonMenuItem implements IMenuItem {
		String _id = null;
		String _description = null;
		
		public MenuItemRadioButton(String label, String description, boolean state) {
			this("<noID>", label, description, state);
		}
		
		public MenuItemRadioButton(String id, String label,
								String description, boolean checked) {
			setID(id);
			setText(UITools.getDisplayLabel(label));
			setMnemonic(UITools.getMnemonic(label));
			setDescription(description);
			setChecked(checked);
		}
		
		public void setText(String label) {
			super.setText(UITools.getDisplayLabel(label));
		}
		
		public Component getComponent() {
			return this;
		}
		  
		public String getID() {
			return _id;
		}
 
		public void setID(String id) {
			_id = id;
		}
		
		public String getDescription() {
			return _description;
		}

		public void setDescription(String description) {
			_description = description;
		}

		public boolean isChecked() {
			return isSelected();
		}
 
		public void setChecked(boolean checked) {
			super.setSelected(checked);
		}
	}
}
