/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;

/**
 * Subtree DN selection Panel
 *
 * This class implements the minimum to take the DSContentModel
 * in as TreeModel and use its functionalities.
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 */
public interface ISubtreeSelectionDialog {

	/**
     * Returns array of selected IResourceObjects.
     */
	public IResourceObject[] getSelection();

	/**
     * Returns array of previously selected IResourceObjects.
     */
	public IResourceObject[] getPreviousSelection();
    
    /**
     * @return true if successful
     */
    public boolean isOk();
    
    /**
     * @return dn string if successful
     */
    public String getDN();

	/**
	 * Display dialog
	 */
	public void show();


	public void packAndShow();

}
