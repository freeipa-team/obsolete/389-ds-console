/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.management.nmclf.SuiConstants;
import com.netscape.management.client.Framework;
import com.netscape.management.client.util.MultilineLabel;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.preferences.Preferences;
import com.netscape.management.client.preferences.PreferenceManager;
import com.netscape.management.client.preferences.AbstractPreferencesTab;
import com.netscape.admin.dirserv.panel.BlankPanel;
import com.netscape.admin.dirserv.panel.GroupPanel;

class DSPreferencesTab extends AbstractPreferencesTab
                       implements ChangeListener {


	public DSPreferencesTab() {
		setTitle(_resource.getString(_section, "title"));
		PreferenceManager pm = PreferenceManager.getPreferenceManager(Framework.IDENTIFIER, Framework.VERSION);
		_preferences = pm.getPreferences(PREFERENCES_GROUP);
	}
	
	

	public void tabSelected() {
	
		// Note: apparently all the components need to be rebuilt
		// each time tabSelected() is called (else layout is
		// no good). So we call buildContentPanel() unconditionally.
		buildContentPanel();
		setComponent(_contentPanel);

		switch(_preferences.getInt(PREFERENCES_CHANGE_INDICATOR, BlankPanel.CHANGE_INDICATOR_COLOR)) {
			case BlankPanel.CHANGE_INDICATOR_COLOR :
				_rbUseColor.setSelected(true);
				break;

			case BlankPanel.CHANGE_INDICATOR_FONT:
				_rbUseFontStyle.setSelected(true);
				break;

			case BlankPanel.CHANGE_INDICATOR_NONE:
				_rbUseNothing.setSelected(true);
				break;
		}
		stateChanged(null); // To update the restartNeeded() property
	}


	void buildContentPanel() {
		 
		_rbUseColor = new JRadioButton(_resource.getString(_section, "use-color"));
		_rbUseColor.addChangeListener(this);
		_rbUseFontStyle = new JRadioButton(_resource.getString(_section, "use-font-style"));
		_rbUseFontStyle.addChangeListener(this);
		_rbUseNothing = new JRadioButton(_resource.getString(_section, "use-nothing"));
		_rbUseNothing.addChangeListener(this);

		JPanel groupPanel = new GroupPanel(_resource.getString(_section, "group-title"), true);
		groupPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(0, 0, COMPONENT_SPACE, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
		
		MultilineLabel l = new MultilineLabel(_resource.getString(_section, "desc"));
		groupPanel.add(l, gbc);
		
		gbc.gridy++;
		gbc.insets.bottom = 0;
		groupPanel.add(_rbUseColor, gbc);
		
		gbc.gridy++;
		groupPanel.add(_rbUseFontStyle, gbc);
		
		gbc.gridy++;
		groupPanel.add(_rbUseNothing, gbc);
		
		_contentPanel = new JPanel();
		_contentPanel.setLayout(new GridBagLayout());
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 1;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(0, 0, COMPONENT_SPACE, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
		_contentPanel.add(groupPanel, gbc);

		ButtonGroup bg = new ButtonGroup();
		bg.add(_rbUseColor);
		bg.add(_rbUseFontStyle);
		bg.add(_rbUseNothing);
	}
	
	
	public void okInvoked() {
		int newType = getIndicatorTypeFromComponents();
		if (newType > 0)	/* < 0: transient state */
		{
			_preferences.set(PREFERENCES_CHANGE_INDICATOR, newType);
		}
	}
	
	
	public void helpInvoked() {
	
	}


	public void stateChanged(ChangeEvent e) {
		int newType = getIndicatorTypeFromComponents();
		if (newType < 0) /* transient state */
		{
			return;
		}
		int currentType = _preferences.getInt(PREFERENCES_CHANGE_INDICATOR, BlankPanel.CHANGE_INDICATOR_COLOR);
		if (newType != currentType) {
			fireStateChanged(); // Is it necessary ?
		}
		setRestartRequired(newType != currentType);
	}


	int getIndicatorTypeFromComponents() {
		int indicatorType = -1;
		if (_rbUseColor == null || _rbUseFontStyle == null ||
			_rbUseNothing == null) {
			return _preferences.getInt(PREFERENCES_CHANGE_INDICATOR, BlankPanel.CHANGE_INDICATOR_COLOR);
		}
		if (_rbUseColor.isSelected()) {
			indicatorType = BlankPanel.CHANGE_INDICATOR_COLOR;
		}
		else if (_rbUseFontStyle.isSelected()) {
			indicatorType = BlankPanel.CHANGE_INDICATOR_FONT;
		}
		else if (_rbUseNothing.isSelected()) {
			indicatorType = BlankPanel.CHANGE_INDICATOR_NONE;
		}
		/* blackflag 624316: radio button: unselect all, then select a new one
		 * Thus, this is not a "Bug".
		else {
			indicatorType = -1;
			throw new IllegalStateException("Bug");
		}
		*/
		return indicatorType;
	}
	
	
	Preferences _preferences;
	JPanel _contentPanel;
	JRadioButton _rbUseColor;
	JRadioButton _rbUseFontStyle;
	JRadioButton _rbUseNothing;
	ResourceSet _resource = DSUtil._resource;
	String _section = "DSPreferencesTab";

    public static final String PREFERENCES_GROUP = "DS_MISCELLANEOUS";
    public static final String PREFERENCES_CHANGE_INDICATOR = "CHANGE_INDICATOR_TYPE";
	
}
