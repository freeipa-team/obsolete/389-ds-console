/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import javax.swing.*;
import javax.swing.tree.TreePath;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.event.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import netscape.ldap.LDAPSchema;

/**
 *	Netscape Directory Server 4.0 base resource model.
 *
 * @author  rweltman
 * @date	 	11/1/97
 */

abstract public class DSBaseModel extends ResourceModel
                       implements IDSModel,
                                  IDSResourceSelectionModel,
                                  IMenuInfo,
                                  TreeExpansionListener,
                                  IAuthenticationChangeListener {
	/**
	 * Constructor - initialize all internal variables
	 *
	 * @param info	Global console connection information
	 * @param serverInfo Server instance connection information
	 */
	public DSBaseModel( ConsoleInfo info,
						ConsoleInfo serverInfo ) {
		_info = info;
		_serverInfo = serverInfo;
		_serverInfo.put( "dsresmodel", this );
		addAuthenticationChangeListener(this);
	}


    public void initialize( Object root ) {
	}

	public void removeElement( IDSEntryObject deo ) {
	}

    /**
     * Return the global console information reference.
     *
     * @return The global console information reference.
     **/
    public ConsoleInfo getConsoleInfo() {
		return _info;
    }

    /**
     * Return the Directory-specific information reference.
     *
     * @return The Directory-specific information reference.
     **/
    public ConsoleInfo getServerInfo() {
		return _serverInfo;
    }
    
    /**
	 * Get new authentication credentials from a dialog, reauthenticate
	 * the connection in _serverInfo.
	 *
	 * @return <CODE>true</CODE> if the connection was reauthenticated.
	 */
    public boolean getNewAuthentication() {
		return getNewAuthentication(true);
	}

    /**
	 * Get new authentication credentials from a dialog, reauthenticate
	 * the connection in _serverInfo.
	 *
	 * @param notifyListeners If true, the authentication change listeners
	 *                        will be updated immediately.  If false, the
	 *                        caller will have to call the
	 *                        notifyAuthChangeListeners() method of this
	 *                        class.  This is useful if the reauthentication
	 *                        happens during some extended operation, such as
	 *                        subtree deletion, and it is not desirable to
	 *                        notify the listeners until the operation is
	 *                        completed.
	 *
	 * @return <CODE>true</CODE> if the connection was reauthenticated.
	 */
    public boolean getNewAuthentication(boolean notifyListeners) {
		if (_authenticating) { // this prevents getting new authentication
			return false;      // while authenticating
		}

		if (_deferAuthListeners != null) {
			Debug.println("DSBaseModel.getNewAuthentication: this method was" +
						  " previously called with the notifyListener flag" +
						  " set to false, but the notifyAuthChangeListeners" +
						  " method was not called to notify them.");
			_deferAuthListeners = null;
		}

		boolean status = false;
		try {
			_authenticating = true; // we are in the process of authenticating
			setWaitCursor(true); // this may take a while . . .

			String dn = (String)getServerInfo().get( "rootdn" );
			Debug.println(9, "DSBaseModel.getNewAuthentication: number " +
						  "of authlisteners=" + _authListeners.size());
			_deferAuthListeners =
				DSUtil.reauthenticateDefer(_serverInfo.getLDAPConnection(),
										   getFrame(),
										   _authListeners, dn, null);

			status = (_deferAuthListeners != null);
			if (status && notifyListeners) {
				notifyAuthChangeListeners();
			}
		} finally {
			setWaitCursor(false);
			_authenticating = false;
		}

		return status;
	}

	/**
	 * If getNewAuthentication was called with the notifyListeners set to false,
	 * this method must be called.
	 */
	public void notifyAuthChangeListeners() {
		// use temporary to call deferred auth listeners in case we are called
		// recursively
		if (_deferAuthListeners != null) {
			DSUtil.DeferAuthListeners temp = _deferAuthListeners;
			_deferAuthListeners = null;
			temp.notifyListeners();
		}
	}
			
	
	public void setFrame(JFrame frame) {
		if (frame == null) {
			Debug.println(0, "DSBaseModel.setFrame: warning null frame specified");
		}
		_frame = frame;
	}
			
	
	public JFrame getFrame() {
		if (_frame == null) {
			Debug.println(0, "DSBaseModel.getFrame: warning frame is not yet known");
			_frame = UtilConsoleGlobals.getActivatedFrame();
		}
		return _frame;
	}
	
    public void setSelectedNode( IResourceObject deo ) {
		DSResourcePage page = (DSResourcePage)getSelectedPage();
		if ( (page != null) && (deo != null) ) {
			TreePath path =
				new TreePath( ((DefaultMutableTreeNode)deo).getPath() );
			page.getTree().expandPath( path );
			page.getTree().makeVisible( path );
			page.getTree().scrollPathToVisible( path );
			page.getTree().repaint();
			page.getTree().setSelectionPath( path );
		}
	}

	/**
	 * Default action does nothing
	 */
	public void actionObjectSelected( IPage viewInstance,
									  IResourceObject[] selection,
									  IResourceObject[] previousSelection) {
		super.actionObjectSelected(viewInstance, selection,
								   previousSelection);
		setSelectedPage(viewInstance);
	}

	public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
		setSelectedPage(viewInstance);
	}

	/**
	 * Default action does nothing
	 */
	public void focusGained( FocusEvent e ) {
	}

	/**
	 * Default action does nothing
	 */
	public void setSelected( IPage viewInstance,
							 IResourceObject[] selection,
							 IResourceObject[] previousSelection) {
		setSelectedPage(viewInstance);
	}

    /**
      * Adds a listener that is interested in receiving selection events.
	  * Called by panels
      */
	public void addIDSResourceSelectionListener(
	                                    IDSResourceSelectionListener l) {
		_selectionListeners.addElement(l);
	}

    /**
      * Removes previously added IDSResourceSelectionListener.
	  * Called by panels
      */
	public void removeIDSResourceSelectionListener(
	                                    IDSResourceSelectionListener l) {
		_selectionListeners.removeElement(l);
    }	                                        

	/**
     *   Returns list of listeners for this model.
	 */
	public Enumeration getSelectionListeners() {
		return _selectionListeners.elements();
	}

	/**
	 * Get the schema of the Directory instance
	 *
	 * @return A reference to a schema object.
	 */
	public LDAPSchema getSchema() {
		return DSUtil.getSchema( _serverInfo );
	}

	/**
	 * Sets a reference to the schema of the Directory instance
	 *
	 * @param schema A reference to a schema object.
	 */
	public void setSchema( LDAPSchema schema ) {
		DSUtil.setSchema( _serverInfo, schema );
	}

	/**	
	 * Add an object to be notified on authentication changes.
	 *
	 * @param l Object to be notified.
	 */
	public void addAuthenticationChangeListener(
		                        IAuthenticationChangeListener l ) {
		Debug.println(9, "DSBaseModel.addAuthenticationChangeListener: " +
					  "new auth change listener=" + l + " this=" + this);
		_authListeners.addElement( l );
	}

	/**	
	 * Set the list of objects to be notified on authentication changes.
	 *
	 * @param v Vector of objects to be notified.
	 */
	public void setAuthenticationChangeListener(Vector v) {
		Debug.println(9, "DSBaseModel.setAuthenticationChangeListener: " +
					  "new auth change listeners=" + v + " this=" + this);
		_authListeners = v;
	}

    public IPage getSelectedPage() {
		return _viewInstance;
	}

    public void setSelectedPage(IPage selectedPage) {
		if (selectedPage != null) {
			_viewInstance = selectedPage;
		}
	}

    /**
      * Adds a client who can report unsaved changes.
	  * Called by panels
      */
    public void addChangeClient( IChangeClient client ) {
		if ( _changeClients.indexOf( client ) == -1 )
			_changeClients.addElement( client );
	}

    /**
      * Removes previously added Change client.
	  * Called by panels
      */
    public void removeChangeClient( IChangeClient client ) {
		if ( _changeClients.indexOf( client ) == -1 )
			_changeClients.removeElement( client );
	}

    public void setWaitCursor( boolean on ) {
		fireChangeFeedbackCursor(
			null,
			on ? FeedbackIndicator.FEEDBACK_WAIT :
			FeedbackIndicator.FEEDBACK_DEFAULT );

		if (on) {
			fireChangeStatusItemState( getSelectedPage(),
									   ResourcePage.STATUS_PROGRESS,
									   StatusItemProgress.STATE_BUSY );
		}
		else {
			fireChangeStatusItemState( getSelectedPage(),
									   ResourcePage.STATUS_PROGRESS,
									   new Integer(0) );
		}
	}

    /**
      * Called internally when page is selected
      */
	public void pageSelected( IFramework framework, IPage viewInstance ) {
		Debug.println(9, "DSBaseModel.pageSelected: framework=" +
					  framework + " viewInstance=" + viewInstance +
					  " this=" + this + " getRefreshUponSelect()=" +
					  getRefreshUponSelect());
		if ( !_registeredExpansionListener ) {
			DSResourcePage page = (DSResourcePage)viewInstance;
			if ( page != null ) {
				page.getTree().addTreeExpansionListener( this );
				_registeredExpansionListener = true;
			}
		}
		if (getRefreshUponSelect()) {
			refreshView();
			Debug.println(9, "DSBaseModel.pageSelected: " +
						  "the view was refreshed");
			setRefreshUponSelect(false);
		}
	}

	void actionSelected( String cmd,
						 IResourceObject[] selection,
						 IPage viewInstance ) {
		Debug.println(1, "DSBaseModel.actionSelected should never be " +
					  "called");
		if (Debug.isEnabled()) {
			Thread.dumpStack();
		}
	}

	/**
	 * Notifies that the root DN changed
	 */
    public void rootDNChanged( String rootDN ) {
		getServerInfo().put( "rootdn", rootDN );
	}

	/**
	 * Notifies that directory contents have changed
	 */
	public void contentChanged() {
	}

	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( o instanceof TreeExpansionListener ) {
			((TreeExpansionListener)o).treeExpanded( tee );
		}
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( o instanceof TreeExpansionListener ) {
			((TreeExpansionListener)o).treeCollapsed( tee );
		}
	}

	public void setRefreshUponSelect(boolean val) {
		_refreshUponSelect = val;
	}

	public boolean getRefreshUponSelect() {
		return _refreshUponSelect;
	}

	/**
	 * Called when authentication changes.
	 *
	 * @param oldAuth Previous authentication DN.
	 * @param newAuth New authentication DN.
	 */
	public void authenticationChanged(String oldAuth,
									  String newAuth,
									  String oldPassword,
									  String newPassword) {
		Debug.println("DSBaseModel.authenticationChanged(): new bind DN = " +
					  newAuth + " old bind DN = " + oldAuth);
		// If this model is selected, refresh the current view.  Otherwise,
		// just set the flag which tells this model to force its view
		// to be refreshed the next time it's selected
		IPage ip = getSelectedPage();
		if (ip != null && ip instanceof DSResourcePage) {
			DSResourcePage dsrp = (DSResourcePage)ip;
			if (dsrp.isPageSelected()) {
				refreshView();
			} else {
				setRefreshUponSelect(true);
			}
		}
	}

	protected static ResourceSet _resource = DSUtil._resource;
	protected Vector _selectionListeners = new Vector();
	protected Vector _changeClients = new Vector();
	private ConsoleInfo _info = null;
	private ConsoleInfo _serverInfo = null;
	private JFrame _frame = null;
	/* Objects interested in authentication changes */
	private Vector _authListeners = new Vector();
	/* Keep track of selections here, so we can notify panels */
	protected IResourceObject[] _selection;
	// this is the view for this model
	private IPage _viewInstance = null;
	private boolean _registeredExpansionListener = false;
	// This flag will be true if the view for this model needs
	// to be refreshed the next time it's displayed e.g. because
	// the authentication changed and the new user may not have
	// permission to see the same view the old user saw
	private boolean _refreshUponSelect = false;
	static final String OPEN = "open";
	static final String ACL = "acl";
	static final String ROLES = "roles";
	static final String AUTHENTICATE = "authenticate";
	static final String CUT = "cut";
	static final String MOVE = "move";
	static final String COPY = "copy";
	static final String PASTE = "paste";
	static final String MOVEPASTE = "movepaste";
	static final String UNDO = "undo";
	static final String DELETE = "delete";
	static final String NEW_USER = "newuser";
	static final String NEW_GROUP = "newgroup";
	static final String NEW_ORGANIZATIONALUNIT = "newou";
	static final String NEW_ROLE = "newrole";
	static final String NEW_COS = "newcos";
	static final String NEW_OBJECT = "newobject";
	static final String REFRESH = "refresh";
	static final String SEARCH_UG = "search";
	protected Hashtable _cmdTable = new Hashtable();
	// this is true if we are in the process of authenticating; this is
	// used to prevent more than one authenticaiton from occuring
	private boolean _authenticating = false;
	// if authentication change listener notification is deferred, this member
    // holds the notify-ees
	private DSUtil.DeferAuthListeners _deferAuthListeners;
}

