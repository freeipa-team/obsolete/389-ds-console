/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import com.netscape.management.client.*;
import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPEntry;

/**
 *	Netscape Directory Server 4.0 Directory Entry interface.
 *
 * @author  rweltman
 * @version 1.2, 03/21/00
 * @date	 	11/1/97
 */

public interface IDSEntryObject extends IResourceObject,
                                        javax.swing.tree.MutableTreeNode {

    public void editProperties();
    public void setModel( IDSModel model );
    public String getDN();
    public void setDN( String dn );
    public long getObjectClassCode();
	public LDAPEntry getEntry();
	public void setEntry(LDAPEntry entry);
	public void reset();
	public boolean isLoaded();
	public void newUser();
    public void newGroup();
    public void newOrganizationalUnit();
    public void newRole();
    public void newCos();
    public void newObject();
    public String getDisplayName ();
    public void setDisplayName ( String s );
	public void reload();
    public DSEntryDialog editGeneric( boolean modal, boolean save );
    public DSEntryDialog editGeneric( IResourceObject[] list, boolean modal );
    public void initializeFromEntry( LDAPEntry findEntry );
    public IDSEntryObject getParentNode();
    public void setParentNode( IDSEntryObject o );
	public LDAPConnection getLDAPConnection();
	public void load();
    public String getHost();
    public int getPort();
    public void setReferralsEnabled( boolean on );
    public boolean getReferralsEnabled();
    public void propagateReferralsEnabled( boolean on );
	public String getSelectedPartitionView();
}

