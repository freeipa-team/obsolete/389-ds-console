/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import netscape.ldap.LDAPSchemaElement;
import netscape.ldap.LDAPObjectClassSchema;
import netscape.ldap.LDAPSchema;
import netscape.ldap.LDAPAttributeSchema;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Stack;
import com.netscape.management.client.util.Debug;
import java.util.Hashtable;
import java.util.Enumeration;


public class DSSchemaHelper extends LDAPSchemaElement {
	public static String[] stringToArray(String aliasString, String delim) {
		StringTokenizer st = new StringTokenizer(aliasString, delim);
		Vector v = new Vector();
		Debug.println(8, "DSSchemaHelper.stringToArray: orig string = " +
					  aliasString);
		while (st.hasMoreTokens()) {
			Object o = st.nextToken();
			v.addElement(o);
			Debug.println(8, "DSSchemaHelper.stringToArray: added item " +
						  o);
		}
		String[] ret = null;
		if (v.size() > 0) {
			ret = new String[v.size()];
			v.copyInto(ret);
		}
		return ret;
	}

	public static String[] stringToArray(String aliasString) {
		String[] stringToArray = stringToArray(aliasString, ALIAS_DELIMITER);
		if (stringToArray != null) {
			for (int i=0; i < stringToArray.length; i++) {
				stringToArray[i] = stringToArray[i].trim();
			}
		}
		return stringToArray;
	}

	public static String arrayToString(String[] aliases, String delim) {
		StringBuffer ret = new StringBuffer();
		for (int ii = 0; (aliases != null) && (ii < aliases.length); ++ii) {
			if (ii > 0)
				ret.append(delim);
			ret.append(aliases[ii]);
		}
		return ret.toString();
	}

	public static String arrayToString(String[] aliases) {
		return arrayToString(aliases, ALIAS_DELIMITER);
	}

	/* Method that puts in a Hashtable all the required attributes for an objectclass.
	 * The resulting hashtable's keys and values are the required attributes names
	 * If the schema, the hashtable or the objectclass are null a null pointer exception is generated
	 * @param objectclass String with the objectclass name.
	 * @param schema the LDAPSchema
	 * @param attributes the Hashtable where we put the required attributes
	 */

	public static void allRequiredAttributes(String objectclass, LDAPSchema schema, Hashtable attributes) {	    
		LDAPObjectClassSchema oc;
		oc = schema.getObjectClass(objectclass);
		
		allRequiredAttributes(oc, schema, attributes);
	}
	
	/* Method that puts in a Hashtable all the required attributes for an objectclass.
	 * The resulting hashtable's keys and values are the required attributes names
	 * If the schema, the hashtable or the objectclass are null a null pointer exception is generated
	 * @param oc LDAPObjectClassSchema containing the objectclass to handle
	 * @param schema the LDAPSchema
	 * @param attributes the Hashtable where we put the required attributes
	 */

	public static void allRequiredAttributes(LDAPObjectClassSchema oc, LDAPSchema schema, Hashtable attributes) {
		String attribute;
		Stack stack = new Stack();
		Enumeration e;
		e = oc.getRequiredAttributes();
		while( e.hasMoreElements() ) {
			attribute = ((String)e.nextElement()).toLowerCase();
			attributes.put(attribute, attribute);
		}
		
		String[] superiors;
		Integer integer;
		superiors = getSuperiors(oc);
		if (superiors != null) {
			int i;
			for (i=0; i<superiors.length; i++) {
				stack.push(new Integer(i));
				stack.push(superiors);
				allRequiredAttributes(superiors[i], schema, attributes);
				superiors = (String[])stack.pop();
				integer = (Integer)stack.pop();
				i = integer.intValue();
			}
		}
	}

	/* Method that returns a Hashtable with all the required attributes for an objectclass.
	 * The resulting hashtable's keys and values are the required attributes names
	 * If the schema or the objectclass are null a null pointer exception is generated
	 * @param objectclass String containing the name of the objectclass to handle
	 * @param schema the LDAPSchema
	 * @return a Hashtable with the required attributes
	 */

	public static Enumeration allRequiredAttributes(String objectclass, LDAPSchema schema) {	    		
		Hashtable ht;
		ht = new Hashtable();
		allRequiredAttributes(objectclass, schema, ht);

		Enumeration attributes;
		attributes = ht.elements();

		return attributes;
	}

	/* Method that puts in a Hashtable all the required attributes for an objectclass.  The case of the attributes 
	 * from the server is respected.
	 * The resulting hashtable's keys and values are the required attributes names
	 * If the schema, the hashtable or the objectclass are null a null pointer exception is generated
	 * @param oc LDAPObjectClassSchema containing the objectclass to handle
	 * @param schema the LDAPSchema
	 * @param attributes the Hashtable where we put the required attributes
	 */

	public static void allRequiredAttributesWithCase(LDAPObjectClassSchema oc, LDAPSchema schema, Hashtable attributes) {
		String attribute;
		Stack stack = new Stack();
		Enumeration e;
		e = oc.getRequiredAttributes();
		while( e.hasMoreElements() ) {
			attribute = (String)e.nextElement();
			attributes.put(attribute, attribute);
		}
		
		String[] superiors;
		Integer integer;
		superiors = getSuperiors(oc);
		if (superiors != null) {
			int i;
			for (i=0; i<superiors.length; i++) {
				stack.push(new Integer(i));
				stack.push(superiors);
				allRequiredAttributesWithCase(schema.getObjectClass(superiors[i]), schema, attributes);
				superiors = (String[])stack.pop();
				integer = (Integer)stack.pop();
				i = integer.intValue();
			}
		}
	}



	/* Method that puts in a Hashtable all the optional attributes for an objectclass.
	 * The resulting hashtable's keys and values are the required attributes names
	 * If the schema, the hashtable or the objectclass are null a null pointer exception is generated
	 * @param objectclass String with the objectclass name.
	 * @param schema the LDAPSchema
	 * @param attributes the Hashtable where we put the required attributes
	 */
	public static void allOptionalAttributes(String objectclass, LDAPSchema schema, Hashtable attributes) {	    
		LDAPObjectClassSchema oc;
		oc = schema.getObjectClass(objectclass);
		
		allOptionalAttributes(oc, schema, attributes);
	}

	/* Method that puts in a Hashtable all the optional attributes for an objectclass.
	 * The resulting hashtable's keys and values are the required attributes names
	 * If the schema, the hashtable or the objectclass are null a null pointer exception is generated
	 * @param oc LDAPObjectClassSchema containing the objectclass to handle
	 * @param schema the LDAPSchema
	 * @param attributes the Hashtable where we put the required attributes
	 */
	public static void allOptionalAttributes(LDAPObjectClassSchema oc, LDAPSchema schema, Hashtable attributes) {
		String attribute;

		Enumeration e;
		if (oc.getName().equalsIgnoreCase("extensibleobject")) {			
			e = schema.getAttributeNames();
			while( e.hasMoreElements() ) {
				attribute = ((String)e.nextElement()).toLowerCase();								
				attributes.put(attribute, attribute);									
			}
			attributes.remove("objectclass");
		} else {
			Stack stack = new Stack();			
			e = oc.getOptionalAttributes();
			while( e.hasMoreElements() ) {
				attribute = ((String)e.nextElement()).toLowerCase();
				attributes.put(attribute, attribute);
			}
			
			String[] superiors;			
			Integer integer;
			superiors = getSuperiors(oc);
			if (superiors != null) {
				int i;
				for (i=0; i<superiors.length; i++) {
					stack.push(new Integer(i));
					allOptionalAttributes(superiors[i], schema, attributes);
					integer = (Integer)stack.pop();
					i = integer.intValue();
				}
			}
		}
	}


	/* Method that returns a Hashtable with all the optional attributes for an objectclass.
	 * The resulting hashtable's keys and values are the required attributes names
	 * If the schema or the objectclass are null a null pointer exception is generated
	 * @param objectclass String containing the name of the objectclass to handle
	 * @param schema the LDAPSchema
	 * @return a Hashtable with the required attributes
	 */
	public static Enumeration allOptionalAttributes(String objectclass, LDAPSchema schema) {	    		
		Hashtable ht;
		ht = new Hashtable();
		allOptionalAttributes(objectclass, schema, ht);

		Enumeration attributes;
		attributes = ht.elements();

		return attributes;
	}


	/* Method that puts in a Hashtable all the optional attributes for an objectclass.  The case of the attributes 
	 * from the server is respected.
	 * The resulting hashtable's keys and values are the required attributes names
	 * If the schema, the hashtable or the objectclass are null a null pointer exception is generated
	 * @param oc LDAPObjectClassSchema containing the objectclass to handle
	 * @param schema the LDAPSchema
	 * @param attributes the Hashtable where we put the required attributes
	 */
	public static void allOptionalAttributesWithCase(LDAPObjectClassSchema oc, LDAPSchema schema, Hashtable attributes) {
		String attribute;

		Enumeration e;
		if (oc.getName().equalsIgnoreCase("extensibleobject")) {			
			e = schema.getAttributeNames();
			while( e.hasMoreElements() ) {
				attribute = (String)e.nextElement();	
				attributes.put(attribute, attribute);									
			}
			attributes.remove("objectclass");
		} else {
			Stack stack = new Stack();
			e = oc.getOptionalAttributes();
			while( e.hasMoreElements() ) {
				attribute = (String)e.nextElement();
				attributes.put(attribute, attribute);
			}
			
			String[] superiors;			
			Integer integer;
			superiors = getSuperiors(oc);
			if (superiors != null) {
				int i;
				for (i=0; i<superiors.length; i++) {
					stack.push(new Integer(i));
					allOptionalAttributesWithCase(schema.getObjectClass(superiors[i]), schema, attributes);
					integer = (Integer)stack.pop();
					i = integer.intValue();
				}
			}
		}
	}


	/* Method that puts in a Hashtable all the required and optional attributes for an objectclass.
	 * The resulting hashtable's keys and values are the required attributes names
	 * If the schema, the hashtable or the objectclass are null a null pointer exception is generated
	 * @param oc LDAPObjectClassSchema containing the objectclass to handle
	 * @param schema the LDAPSchema
	 * @param attributes the Hashtable where we put the required attributes
	 */
	public static void allAttributes(LDAPObjectClassSchema oc, LDAPSchema schema, Hashtable attributes) {
		String attribute;
		Debug.println("DSSchemaHelper.allAttributes.  Objectclass = "+oc.getName());
		
		Hashtable htOptional;
		htOptional = new Hashtable();
		
		allOptionalAttributes(oc, schema, htOptional);
		allRequiredAttributes(oc, schema, attributes);		

		Enumeration e;
		e = htOptional.elements();

		while( e.hasMoreElements() ) {
			attribute = ((String)e.nextElement()).toLowerCase();
			attributes.put(attribute, attribute);
		}
		
		e = attributes.elements();
		while( e.hasMoreElements() ) {
			attribute = (String)e.nextElement();			
		}
	}
		
	static public void getObjectClassVector( String name, 
											 LDAPSchema schema, 
											 Vector v) {
		String value;
		value = name;
		if ( (value != null) && (value.length() > 0) ) {
			if (v. indexOf(value) < 0) {
				v.addElement( value );
			}
			if ( value.equals( "top" ) ) {
				return;
			}
			LDAPObjectClassSchema oc;
			oc = schema.getObjectClass( value );
			String[] superiors;
			if ( oc != null ) {
				superiors = getSuperiors(oc);
				if (superiors != null) {
					int i;
					Integer integer;
					Stack stack = new Stack();
					for (i=0; i<superiors.length; i++) {
						stack.push(new Integer(i));
						stack.push(superiors);
						getObjectClassVector( superiors[i], schema, v);
						superiors = (String[])stack.pop();
						integer = (Integer)stack.pop();
						i = integer.intValue();
					}
				}
			}
		}
	}
	
	static public Vector getObjectClassVector( String name,
											   LDAPSchema schema ) {
		Vector v = new Vector();
		getObjectClassVector(name, schema, v);
		return v;
	}


	/* Method that returns in a String[] the names of the operational attributes that can be editable by the user.
	 * The names of the attributes are in lowecase.  This list is calculated only once.  To recalculate it, we have to call
	 * resetEditableOperationalAttributes() and then this method again.
	 * @returns the list of the names (in lower case) of the editable operational attributes.  Null if no editable operational attribute was found.
	 * @param schema the LDAPSchema
	 */
	static public String[] getEditableOperationalAttributes(LDAPSchema schema) {
		if (_editableOperationalAttributes == null) {			
			Vector vOperationAttributes = new Vector();
			Enumeration e;
			e = schema.getAttributes();
			if (e == null) {
				return null;
			}
			LDAPAttributeSchema attribute;			
			while( e.hasMoreElements() ) {
				attribute = (LDAPAttributeSchema) e.nextElement();
				if (attribute != null) {				
					String[] usage = attribute.getQualifier(attribute.USAGE);
					if (usage != null) {
						for (int i=0; i< usage.length; i++) {											
							if (usage[i].equalsIgnoreCase("directoryOperation") ||
								usage[i].equalsIgnoreCase("distributedOperation") ||
								usage[i].equalsIgnoreCase("dSAOperation")) {								
								String[] noUserModificationQualifier = attribute.getQualifier(attribute.NO_USER_MODIFICATION);
								boolean noUserModification = true;
								if (noUserModificationQualifier == null) {
									noUserModification = false;
								} else if (noUserModificationQualifier.length ==0) {
									noUserModification = false;
								}
								if (!noUserModification) {
									vOperationAttributes.addElement(attribute.getName().toLowerCase());									
								}
								break;
							}						
						}
					}
				}
			}
			if (vOperationAttributes.size() > 0) {
				_editableOperationalAttributes = new String[vOperationAttributes.size()];
				vOperationAttributes.copyInto(_editableOperationalAttributes);
			} else {
				return null;
			}
		}
		return _editableOperationalAttributes;		
	}

	/**
	  * Resets the list of editable operational attributes to null, in order to be able to recalculate calling to
	  * getEditableOperationalAttributes(LDAPSchema schema)
	  */
	public void resetEditableOperationalAttributes() {
		_editableOperationalAttributes = null;
	}



	/* Method that returns in a String[] the names of the operational attributes.
	 * The names of the attributes are in low case.  This list is calculated only once.  To recalculate it, we have to call
	 * resetOperationalAttributes() and then this method again.
	 * @returns the list of the names (in lower case) of the operational attributes.  Null if no operational attribute was found.
	 * @param schema the LDAPSchema
	 */
	static public String[] getOperationalAttributes(LDAPSchema schema) {
		if (_operationalAttributes == null) {			
			Vector vOperationAttributes = new Vector();
			Enumeration e;
			e = schema.getAttributes();
			if (e == null) {
				return null;
			}
			LDAPAttributeSchema attribute;			
			while( e.hasMoreElements() ) {
				attribute = (LDAPAttributeSchema) e.nextElement();
				if (attribute != null) {				
					String[] usage = attribute.getQualifier(attribute.USAGE);
					if (usage != null) {
						for (int i=0; i< usage.length; i++) {											
							if (usage[i].equalsIgnoreCase("directoryOperation") ||
								usage[i].equalsIgnoreCase("distributedOperation") ||
								usage[i].equalsIgnoreCase("dSAOperation")) {								
								vOperationAttributes.addElement(attribute.getName().toLowerCase());
								break;
							}						
						}						
					}
				}
			}			
			if (vOperationAttributes.size() > 0) {
				_operationalAttributes = new String[vOperationAttributes.size()];
				vOperationAttributes.copyInto(_operationalAttributes);
			} else {
				return null;
			}
		}
		return _operationalAttributes;		
	}

	/**
	  * Resets the list of operational attributes to null, in order to be able to recalculate calling to
	  * getOperationalAttributes(LDAPSchema schema)
	  */
	public void resetOperationalAttributes() {
		_operationalAttributes = null;
	}

    public static String[] getSuperiors(LDAPObjectClassSchema oc) {
        String[] superiors = oc.getSuperiors();
        if ((superiors == null) && (oc.getType() == LDAPObjectClassSchema.STRUCTURAL)) {
            // a structural objectclass must always have top as a superior
            // due to https://bugzilla.redhat.com/show_bug.cgi?id=170791 the SUP
            // may not have shown up, so we add it if necessary
            superiors = new String[1];
            superiors[0] = "top";
        }

        return superiors;
    }

	private static String[] _editableOperationalAttributes = null;
	private static String[] _operationalAttributes = null;
	public static final String ALIAS_DELIMITER = ",";
	public static String[] FORBIDDEN_OBJECTCLASSES = {		
		"glue",
		"nstombstone",
		"top"
	};
}
