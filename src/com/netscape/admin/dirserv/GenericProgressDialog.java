/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Vector;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ModalDialogUtil;
import com.netscape.management.client.util.MultilineLabel;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.admin.dirserv.panel.GroupPanel;
import com.netscape.admin.dirserv.panel.DatabaseImportPanel;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.management.nmclf.SuiConstants;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * GenericProgressDialog
 * Simple cancellable export/import progress dialog
 *
 * It contains one or two buttons: Cancel button and/or Show Logs Button
 *
 * It contains a multilineLabel than can be used to show messages
 *
 * It may contain a scrollable text area
 *
 * User can add different "steps" to this progress dialog.  Each step is an checkbox and a message describing the step.
 *
 * @version 1.0
 * @author jvergara
**/

public class GenericProgressDialog extends JDialog {

	public GenericProgressDialog(JFrame parent) {
		this(parent, true);
	}

	public GenericProgressDialog(JFrame parent, boolean modal) {
		this(parent, modal, DEFAULT_OPTION);
	}

	public GenericProgressDialog(JFrame parent, int options) {
		this(parent, true, options);
	}
	
	public GenericProgressDialog(JFrame parent, boolean modal, int options) {
		this(parent, modal, options, null);				
	}

	public GenericProgressDialog(JFrame parent, boolean modal, int options, String title) {
		this(parent, modal, options, title, null);
	}
	
	public GenericProgressDialog(JFrame parent, boolean modal, int options, String title, Component comp) {
		this(parent, modal, options, title, comp, null);
	}

	public GenericProgressDialog(JFrame parent, boolean modal, int options, String title, Component comp, ActionListener listener) {
		super(parent, modal);
		_options = options;
		if (title!=null) {
			setTitle(title);
		}
		if ( comp != null ) {
			setLocationRelativeTo( comp );
		} else {
			ModalDialogUtil.setWindowLocation(this);
		}

		_listener = listener;

		_panel = new JPanel();
		_panel.setLayout(new GridBagLayout());
		
		_stepsPanel = new JPanel();
		_stepsPanel.setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		
		_label = new JTextArea(2, 50);
		_label.setEditable(false);
                _label.setBackground(getBackground());
		
		_label.setBorder(new EmptyBorder(SuiConstants.COMPONENT_SPACE,0,0,0));
		
		JScrollPane jscroll = new JScrollPane( _label );
		jscroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jscroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		jscroll.setBorder(new EmptyBorder(0, 0, 0, 0));
		jscroll.setBackground(getBackground());		
	
		gbc.fill = gbc.HORIZONTAL;
		gbc.fill = gbc.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		gbc.anchor = gbc.NORTH;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = new Insets(
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE,
								0,
								SuiConstants.COMPONENT_SPACE );
		_panel.add(jscroll, gbc);
		gbc.insets = new Insets(
								0,
								SuiConstants.COMPONENT_SPACE,
								0,
								SuiConstants.COMPONENT_SPACE );
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.anchor = gbc.NORTHWEST;
		
		_panel.add(_stepsPanel, gbc);

		gbc.insets = new Insets(
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE );
		
		if ((_options == TEXT_FIELD_OPTION) ||
			(_options == TEXT_FIELD_AND_CANCEL_BUTTON_OPTION)) {
			gbc.fill = gbc.BOTH;
			gbc.anchor = gbc.NORTHWEST;
			gbc.gridwidth = gbc.REMAINDER;
			gbc.weightx = 1.0;			
			_panel.add(new JSeparator(), gbc);
			_textAreaLabel = new JLabel("");
			gbc.insets = new Insets(
									SuiConstants.COMPONENT_SPACE,
									SuiConstants.COMPONENT_SPACE,
									0,
									SuiConstants.COMPONENT_SPACE );
			_panel.add(_textAreaLabel, gbc);
			gbc.insets = new Insets(
									SuiConstants.COMPONENT_SPACE,
									SuiConstants.COMPONENT_SPACE,
									SuiConstants.COMPONENT_SPACE,
									SuiConstants.COMPONENT_SPACE );
			gbc.weighty = 1.0;
			_panel.add(createFieldsPanel(), gbc);			
		} else {			
			gbc.gridwidth = gbc.REMAINDER;
			gbc.weighty = 1.0;			
			_panel.add(Box.createVerticalGlue(), gbc);
		}
		
		if (_options != NO_BUTTON_OPTION) {
			gbc.gridwidth = gbc.REMAINDER;
			gbc.fill = gbc.HORIZONTAL;
			gbc.weightx = 1.0;
			gbc.weighty = 0.0;
			gbc.anchor = gbc.SOUTH;
			_panel.add(createButtonPanel(), gbc);			
		}

		setContentPane(_panel);

		_stepNumber = 0;
		_currentStep = 0;
	}

	protected JPanel createButtonPanel() {
		JPanel panel = new JPanel();	
		
		JButton[] buttons;
		
		/* We always add this button, because is the button that is used to close when the operation has finished (see method waitForClose)*/
		_bCancel = UIFactory.makeJButton(this, "general", "Stop");
		_bCancel.setVisible(false);
		if (_listener!= null) {
			_bCancel.addActionListener(_listener);
		}

		if (_options != NO_BUTTON_OPTION) {
			Vector vButtons = new Vector();
			if ( (_options == ONLY_CLOSE_BUTTON_OPTION) || (_options == CLOSE_AND_LOG_BUTTON_OPTION)) {
				_bCancel.setActionCommand( CLOSE );
				_bCancel.setEnabled( false );
				_bCancel.setVisible(true);
				_bCancel.setText(DSUtil._resource.getString("general","Close-label"));
			} else if ( _options != ONLY_LOG_BUTTON_OPTION) {			   				
				_bCancel.setActionCommand( CANCEL );
				_bCancel.setOpaque( true );
				_bCancel.setVisible(true);			
			}
			vButtons.addElement(_bCancel);

			if ((_options != ONLY_CANCEL_BUTTON_OPTION) && 
				(_options != TEXT_FIELD_AND_CANCEL_BUTTON_OPTION) &&
				(_options != ONLY_CLOSE_BUTTON_OPTION)) {
				_bShowLogs = UIFactory.makeJButton(this, "general", "ShowLogs");
				
				_bShowLogs.setActionCommand( SHOW_LOGS );
				_bShowLogs.setOpaque( true );
				_bShowLogs.setVisible( true);

				if (_listener!= null) {
					_bShowLogs.addActionListener(_listener);
				}
						   
				vButtons.addElement(_bShowLogs);
			}

			buttons = new JButton[vButtons.size()];
			vButtons.toArray(buttons);
							
			return UIFactory.makeJButtonPanel(buttons);
		} 
		return panel;
	}

	protected JScrollPane createFieldsPanel() {
		_textArea = new JTextArea();		
		_textArea.setRows(ROWS);
		
		_textArea.setEditable(false);		

		JScrollPane jscroll = new JScrollPane( _textArea );
		jscroll.setBorder( BorderFactory.createLoweredBevelBorder());
		
		return jscroll;
	}

	/**
	 * Adds a new step to this progress dialog (step=row with a JCheckbox on the left and a line of text in the right)
	 *
	 * @param step String with the messages associated with the step
	 */
	public void addStep(final String step) {			
		if (SwingUtilities.isEventDispatchThread()) {
			addStep_(step);
		} else {
			SwingUtilities.invokeLater( new Runnable() {
				public void run() {
					addStep_(step);
				}
			});
		}
	}

	void addStep_(String step) {			
		JCheckBox cbox = UIFactory.makeJCheckBox(null);
		cbox.setEnabled(false);

		if (_cboxVector == null) {
			_cboxVector = new Vector();
		}

		_cboxVector.addElement(cbox);		

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE,
								SuiConstants.COMPONENT_SPACE );

		gbc.fill = gbc.NONE;
		gbc.anchor = gbc.NORTHWEST;
		gbc.gridwidth = 3;
		gbc.weightx = 0.0;			
		_stepsPanel.add(cbox, gbc);

		gbc.gridwidth--;
		_stepsPanel.add(UIFactory.makeJLabel(step), gbc);
		
		gbc.gridwidth = gbc.REMAINDER;
		gbc.fill = gbc.HORIZONTAL;
		gbc.weightx = 1.0;
		_stepsPanel.add(Box.createHorizontalGlue(), gbc);

		_stepNumber++;
	}

	/** 
	 * Method used to close the dialog.
	 * It makes an invokeLater call.
	 */

	public void closeCallBack() {
		Disposer disposer = new Disposer();
		if (SwingUtilities.isEventDispatchThread()) {
			disposer.run();
		} else {
			try {
				SwingUtilities.invokeLater(disposer);
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.closeBack(): "+e);
			}		
		}
	}

	/** 
	 * Method used to check the next step.  The next checkbox is checked.
	 * It makes an invokeLater call.
	 */
	public void nextStepCompleted() {
		CheckBoxUpdater checkBoxUpdater = new CheckBoxUpdater();
		if (SwingUtilities.isEventDispatchThread()) {
			checkBoxUpdater.run();
		} else {
			try {
				SwingUtilities.invokeLater( checkBoxUpdater );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.nextStepCompleted(): "+e);
			}
		}
	}

	/** 
	 * Method used to check the next step.  The next checkbox is checked or unchecked.
	 * It makes an invokeLater call.
	 * @param state boolean telling wether we want to check or uncheck the checkbox
	 */
	public void nextStepCompleted(boolean state) {
		CheckBoxUpdater checkBoxUpdater = new CheckBoxUpdater(state);
		if (SwingUtilities.isEventDispatchThread()) {
			checkBoxUpdater.run();
		} else {
			try {
				SwingUtilities.invokeLater( checkBoxUpdater );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.nextStepCompleted(): "+e);
			}
		}
	}

	/** 
	 * Method used to check a step
	 * It makes an invokeLater call.
	 * @param number int telling which is the position of the step to complete (the first one is '0')
	 */
	public void stepCompleted(int number) {
		CheckBoxUpdater checkBoxUpdater = new CheckBoxUpdater(number);
		if (SwingUtilities.isEventDispatchThread()) {
			checkBoxUpdater.run();
		} else {
			try {
				SwingUtilities.invokeLater( checkBoxUpdater );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.stepCompleted(): "+e);
			}
		}
	}

	/** 
	 * Method used to check the next step.  A checkbox is checked or unchecked.
	 * It makes an invokeLater call.
	 * @param state boolean telling wether we want to check or uncheck the checkbox
	 * @param number int telling which is the position of the step to complete (the first one is '0')
	 */
	public void stepCompleted(boolean state, int number) {
		CheckBoxUpdater checkBoxUpdater = new CheckBoxUpdater(state, number);
		if (SwingUtilities.isEventDispatchThread()) {
			checkBoxUpdater.run();
		} else {
			try {
				SwingUtilities.invokeLater( checkBoxUpdater );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.stepCompleted(): "+e);
			}
		}
	}

	/** 
	 * Method used to append text to the text Area
	 * It makes an invokeLater call.
	 * @param text String containing the text to append
	 */
	public void appendTextToTextArea(String text) {
		TextFieldAppender textFieldAppender = new TextFieldAppender(text);
		if (SwingUtilities.isEventDispatchThread()) {
			textFieldAppender.run();
		} else {
			try {
				SwingUtilities.invokeLater( textFieldAppender );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.appendTextToFileArea(): "+e);
			}	
		}
	}

	/** 
	 * Method used to put text into the text Area (overwrites existing text)
	 * It makes an invokeLater call.
	 * @param text String containing the text to put
	 */
	public void setTextInTextArea(String text) {
		TextFieldUpdater textFieldUpdater = new TextFieldUpdater(text);
		if (SwingUtilities.isEventDispatchThread()) {
			textFieldUpdater.run();
		} else {
			try {
				SwingUtilities.invokeLater( textFieldUpdater );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.setTextInFileArea(): "+e);
			}	
		}
	}
	
	/** 
	 * Method used to put text into the text Label (overwrites existing text)
	 * It makes an invokeLater call.
	 * @param text String containing the text to put
	 */
	public void setTextInLabel(String text) {
		LabelUpdater labelUpdater = new LabelUpdater(text);
		if (SwingUtilities.isEventDispatchThread()) {
			labelUpdater.run();
		} else {
			try {
				SwingUtilities.invokeLater( labelUpdater );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.setTextInLabel(): "+e);
			}
		}
	}

	/**
	 * This method is used to change the label of the cancel button to "close".
	 * The action command of the button cancel changes from CANCEL to CLOSE.
	 * Should be called when the task is over.
	 */
	public void waitForClose() {
		WaitAndCloser waitAndCloser = new WaitAndCloser();
		if (SwingUtilities.isEventDispatchThread()) {
			waitAndCloser.run();
		} else {
			try {
				SwingUtilities.invokeLater( waitAndCloser );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.waitForClose(): "+e);
			}
		}
	}		

	/**
	  * Disables Cancel button
	  */
	public void disableCancelButton() {
		ButtonDisabler disabler = new ButtonDisabler(_bCancel, false);
		if (SwingUtilities.isEventDispatchThread()) {
			disabler.run();
		} else {
			try {
				SwingUtilities.invokeLater( disabler );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.disableCancelButton(): "+e);
			}			
		}
	}
	/**
	  * Disabled Log button
	  */
	public void disableLogButton() {
		ButtonDisabler disabler = new ButtonDisabler(_bShowLogs, false);
		if (SwingUtilities.isEventDispatchThread()) {
			disabler.run();
		} else {
			try {
				SwingUtilities.invokeLater( disabler );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.disableLogButton(): "+e);
			}
		}
	}

	/** 
	  * Enables/Disables all the buttons
	  * @param state boolean giving the state of the buttons
	  */
	public void enableButtons(boolean state) {		
		if (SwingUtilities.isEventDispatchThread()) {
			ButtonDisabler disabler = new ButtonDisabler(_bShowLogs, false);
			disabler.run();
			disabler = new ButtonDisabler(_bCancel, false);
			disabler.run();
		} else {
			try {
				SwingUtilities.invokeLater( new ButtonDisabler(_bShowLogs, state) );
				SwingUtilities.invokeLater( new ButtonDisabler(_bCancel, state) );
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.enableButtons(): "+e);
			}
		}
	}

	/**
	 * Resets the panel:  all the checkboxes are unset
	 */
	public void reset() {
		_currentStep = 0;
		for (int i=0; i<_stepNumber; i++) {
			stepCompleted(false, i);
		}
	}

	/** 
	 * Gives the number of the current Step (the first non completed step)
	 *
	 * @return an int with the number of the current step
	 */
	public int getCurrentStepNumber() {
		return _currentStep;
	}

	/**
	 * Gives the total number of steps
	 *
	 * @return an int with the total number of steps
	 */
	public int getStepNumber() {
		return _stepNumber;
	}
	
	/**
	  *  Performs a pack and then a show of this dialog
	  */
	public void packAndShow() {
		if (SwingUtilities.isEventDispatchThread()) {
			pack();
			show();
		}
		else {
			try {
				SwingUtilities.invokeAndWait( new Runnable() {
					public void run() {
						GenericProgressDialog.this.pack();
						GenericProgressDialog.this.show();
					}
				});
			} catch ( Exception e ) {
				Debug.println("GenericProgressDialog.packAndShow(): "+e);
			}
		}
	}

	/**
	  * Used to set de number of rows of the label
	  *
	  * @param int rows number of rows for the label
	  */
	public void setLabelRows(final int rows) {
		if (SwingUtilities.isEventDispatchThread()) {
			_label.setRows(rows);
		} else {
			SwingUtilities.invokeLater( new Runnable() {
				public void run() {
					_label.setRows(rows);
				}
			});
		}
	}

	/**
	  * Used to set de number of columns of the label
	  *
	  * @param int rows number of columns for the label
	  */
	public void setLabelColumns(final int columns) {
		if (SwingUtilities.isEventDispatchThread()) {
			_label.setColumns(columns);
		} else {
			SwingUtilities.invokeLater( new Runnable() {
				public void run() {
					_label.setColumns(columns);
				}
			});
		}
	}

	/**
	  * Used to set de number of rows of the textarea
	  *
	  * @param int rows number of rows for the textarea
	  */
	public void setTextAreaRows(final int rows) {
		if (SwingUtilities.isEventDispatchThread()) {
			_textArea.setRows(rows);
		} else {
			SwingUtilities.invokeLater( new Runnable() {
				public void run() {
					_textArea.setRows(rows);
				}
			});
		}
	}

	/**
	  * Used to set de number of columns of the textarea
	  *
	  * @param int rows number of columns for the textarea
	  */
	public void setTextAreaColumns(final int columns) {
		if (SwingUtilities.isEventDispatchThread()) {
			_textArea.setColumns(columns);
		} else {
			SwingUtilities.invokeLater( new Runnable() {
				public void run() {
					_textArea.setColumns(columns);
				}
			});
		}
	}

	/**
	 * Used to put text in the text area's label
	 *
	 * @param String text, the text
	 */
	public void setTextInTextAreaLabel(final String text) {
		if ((_textAreaLabel!= null) && ( text != null)) {
			if (SwingUtilities.isEventDispatchThread()) {
				_textAreaLabel.setText(text);
			} else {
				SwingUtilities.invokeLater( new Runnable() {
					public void run() {
						_textAreaLabel.setText(text);
					}
				});
			}
		}
	}

	/**
	  * Adds an actionListener to the buttons of this dialog
	  *
	  * @param listener the ActionListener
	  */	  
	public void addActionListener(ActionListener listener) {		
		_listener = listener;
		
		if (_listener != null) {
			if (_bCancel != null) {
				_bCancel.addActionListener(_listener);
			}
			if (_bShowLogs != null) {
				_bShowLogs.addActionListener(_listener);
			}
		}
	}

    class Disposer implements Runnable {
		Disposer() {
		}
	    public void run() {			
			GenericProgressDialog.this.setVisible( false );
			GenericProgressDialog.this.dispose();			
		}
	}

	class CheckBoxUpdater implements Runnable {		
		CheckBoxUpdater() {
			_state = true;
		}

		CheckBoxUpdater(boolean state) {
			_state = state;
		}

		CheckBoxUpdater(int number) {
			_state = true;
			_number = number;
		}

		CheckBoxUpdater(boolean state, int number) {
			_state = state;
			_number = number;
		}

		public void run() {
			if (_number >= 0) {
				_currentStep = _number;
			}

			if (_currentStep < _stepNumber) {
				JCheckBox cbox = (JCheckBox) _cboxVector.elementAt(_currentStep);
				cbox.setSelected(_state);
				_currentStep++;
			} else {
				Debug.println("CheckBoxUpdater.run(): ERROR given index is out of bounds");
			}		
		}
		boolean _state;
		int _number = -1;
	}

	class TextFieldAppender implements Runnable {
		TextFieldAppender(String text) {
			_text = text;
		}
		
		public void run() {
			if (_textArea!=null) {
				int currentTextLength = _textArea.getText().length();
				int textToAppendLength = _text.length();
				if ((currentTextLength + textToAppendLength) > TEXT_AREA_MAX_SIZE) {
					if (textToAppendLength > TEXT_AREA_MAX_SIZE) {
						_textArea.setText(_text.substring(textToAppendLength - TEXT_AREA_MAX_SIZE));
					} else if (currentTextLength/5 < textToAppendLength) {						
						_textArea.replaceRange("", 0, textToAppendLength);
						_textArea.append(_text);
					} else {
						_textArea.replaceRange("", 0, currentTextLength/5);
						_textArea.append(_text);
					}					
				} else {
					_textArea.append(_text);
				}				 
			}
		}
		String _text;
	}

	class TextFieldUpdater implements Runnable {
		TextFieldUpdater(String text) {
			_text = text;
		}
		
		public void run() {
			if (_textArea!=null) {
				int textLength = _text.length();
				if (textLength > TEXT_AREA_MAX_SIZE) {
					_textArea.setText(_text.substring(textLength - TEXT_AREA_MAX_SIZE));
				} else {
					_textArea.setText(_text);
				}
			}
		}
		String _text;
	}

	class LabelUpdater implements Runnable {
		LabelUpdater(String text) {
			_text = text;
		}
		
		public void run() {
			if (_label!=null) {
				_label.setText(_text);
			}
		}
		String _text;
	}

	class WaitAndCloser implements Runnable {
		WaitAndCloser() {
		}
		public void run() {
			if (_bCancel != null) {
				if (!_bCancel.isEnabled()) 
					_bCancel.setEnabled(true);
				if (!_bCancel.isVisible()) {
					_bCancel.setVisible(true);
					invalidate();
					validate();
				}
				_bCancel.setText(DSUtil._resource.getString("general","Close-label"));
				_bCancel.setActionCommand(CLOSE);
				if (_listener != null) {
					_bCancel.addActionListener(_listener);
				}
				_bCancel.grabFocus();
			}
		}
	}

	class ButtonDisabler implements Runnable {
		ButtonDisabler(JButton button, boolean state) {	
			_button = button;
			_state = state;
		}
		public void run() {
			if (_button != null) {
				_button.setEnabled(_state);
			}
		}
		JButton _button;
		boolean _state;
	}
    
	protected int _options;
	protected int _currentStep;
	protected int _stepNumber;
	protected JPanel _panel;
	protected JPanel _stepsPanel;
	protected JButton _bCancel;
	protected JButton _bShowLogs;
	protected JTextArea _textArea;
	protected JLabel _textAreaLabel;
	protected JTextArea _label;
	protected Vector _cboxVector;
	protected ActionListener _listener;

	public final static int ONLY_CANCEL_BUTTON_OPTION = 0;  // The dialog only has the cancel button (user can add steps if he/she wants to)
	public final static int DEFAULT_OPTION = 0; // The dialog only has the cancel button (user can add steps if he/she wants to)
	public final static int NO_BUTTON_OPTION = 1; // The dialog only no buttons (user can add steps if he/she wants to)
	public final static int ONLY_LOG_BUTTON_OPTION = 2; // The dialog only has the log button (user can add steps if he/she wants to)
	public final static int CANCEL_AND_LOG_BUTTON_OPTION = 3; // The dialog has the log and cancel buttons (user can add steps if he/she wants to)
	public final static int TEXT_FIELD_OPTION = 4; // The dialog only has the log and cancel buttons (user can add steps if he/she wants to) and the text field
	public final static int ONLY_CLOSE_BUTTON_OPTION = 5; // The dialog only has a close button
	public final static int CLOSE_AND_LOG_BUTTON_OPTION = 6; // The dialog has a close and a log button
	public final static int TEXT_FIELD_AND_CANCEL_BUTTON_OPTION = 7; // The dialog only has the cancel button (user can add steps if he/she wants to) and the text field

	final int ROWS = 15;
	final int TEXT_AREA_MAX_SIZE = 80 * 500; // The maximum size of the text area is of ~ 500 lines

	public final static String CANCEL = "Cancel-GenericProgressDialog";
	public final static String CLOSE = "Close-GenericProgressDialog";
	public final static String SHOW_LOGS = "Show_Logs-GenericProgressDialog";
}
