/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/


/**
 * Children List Model
 *
 */
package com.netscape.admin.dirserv.browser;
import java.util.Vector;
import java.util.Enumeration;

import javax.swing.AbstractListModel;
import javax.swing.event.*;


/**
 * This class implements a list model for the list controlled by
 * the ChildrenController class. The VListModel extends the class
 * to provide a VLV list implementation. 
 */
class ChildrenListModel extends AbstractListModel {

    Vector _entries = new Vector();

    /**
     * Returns the number of components in this list.
     *
     * @return  the number of components in this list.
     */
    public int getSize() {
        return _entries.size();
    }

    /**
     * Returns the component at the specified index.
     * @param      index   an index into this list.
     * @return     the component at the specified index.
     * @exception  ArrayIndexOutOfBoundsException  if the <tt>index</tt> 
     *             is negative or not less than the current size of this 
     *             list.
     *             given.
     */
    public Object getElementAt(int index) {
        return _entries.elementAt(index);
    }

    /**
     * Returns an enumeration of the components of this list.
     *
     * @return  an enumeration of the components of this list.
     */
    public Enumeration elements() {
        return _entries.elements();
    }

    /**
     * Fires a change even after a node has been updated
     * <p>
     * 
     * Throws an <tt>ArrayIndexOutOfBoundsException</tt> if the index 
     * is invalid.
     * @param      obj     the component is to update.
     */
    public void updateElement(Object obj) {
        int index = _entries.indexOf(obj);
        if (index >=0) {
            fireContentsChanged(this, index, index);
        }
    }

    /**
     * Fires a change even after a node has been updated
     * <p>
     * 
     * Throws an <tt>ArrayIndexOutOfBoundsException</tt> if the index 
     * is invalid.
     * @param      obj     the component is to update.
     */
    public void replaceElement(Object obj, Object newObj) {
        int index = _entries.indexOf(obj);
        if (index >=0) {
            _entries.setElementAt(newObj, index);
            fireContentsChanged(this, index, index);
        }
    }


    /**
     * Adds the specified component to the end of this list. 
     *
     * @param   obj   the component to be added.
     */
    public void addElement(Object obj) {
        int index = _entries.size();
        _entries.addElement(obj);
        fireIntervalAdded(this, index, index);
    }

    /**
     * Removes the first (lowest-indexed) occurrence of the argument 
     * from this list.
     *
     * @param   obj   the component to be removed.
     * @return  <code>true</code> if the argument was a component of this
     *          list; <code>false</code> otherwise.
     */
    public boolean removeElement(Object obj) {
        int index = _entries.indexOf(obj);
        boolean rv = _entries.removeElement(obj);
        if (index >= 0) {
	        fireIntervalRemoved(this, index, index);
	    }
        return rv;
    }

    /**
     * Adds a vector of components to the end of this list. 
     *
     * @param   objs   the component vector to be added.
     */
    public void addElements(Vector objs) {
        int index = _entries.size();
        int addCount = objs.size();
        for (int i=0; i< addCount; i++) {
            _entries.addElement(objs.elementAt(i));
        }
        if (addCount > 0) {
            fireIntervalAdded(this, index, (index+addCount-1));
        }
    }


    /**
     * Removes all of the elements from this list.  The list will
     * be empty after this call returns (unless it throws an exception).
     */
    public void clear() {
        int index1 = getSize()-1;
        _entries.removeAllElements();
        if (index1 >= 0) {
	        fireIntervalRemoved(this, 0, index1);
	    }
    }
}
