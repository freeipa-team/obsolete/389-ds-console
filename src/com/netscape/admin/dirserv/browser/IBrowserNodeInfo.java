/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

/**
 * Info about an entry displayed by the BrowserController.
 */
 
package com.netscape.admin.dirserv.browser;

import java.util.Vector;
import javax.swing.tree.TreePath;
import netscape.ldap.LDAPUrl;

public interface IBrowserNodeInfo {

	/**
	 * URL of the displayed entry.	 
	 */
	public LDAPUrl getURL();


	/**
	 * Return true if the displayed entry is the top entry
	 * of a suffix.
	 */
	public boolean isSuffix();


	/**
	 * Return true if the displayed entry is the root node 
	 * of the server (the dn="" entry).
	 */
	public boolean isRootNode();

	/**
	 * Return true if the displayed entry is not located on
	 * the current server. An entry is declared 'remote'
	 * when the host/port of getURL() is different from
	 * the host/port of the LDAPConnection associated to 
	 * the browser controller.
	 */
	public boolean isRemote();


	/**
	 * Value of numsubordinates for the entry.
	 * -1 if the numsubordinates attribute is not defined.
	 */
	public int getNumSubOrdinates();


	/**
	 * Referral attached to the displayed entry.
	 * This is the value of the 'ref' attribute.
	 * It's null if the attribute is not present.
	 */
	public String[] getReferral();


	/**
	 * Returns the error detected while reading this entry.
	 */
	public int getErrorType();
	
	
	/**
	 * Returns the exception associated to the error.
	 * Null if getErrorType() == ERROR_NONE.
	 */
	public Exception getErrorException();
	
	
	/**
	 * Returns the argument associated to an error/exception.
	 * Always null except when errorType == ERROR_SOLVING_REFERRAL, 
	 * errorArg contains the String representing the faulty LDAP URl.
	 */
	public Object getErrorArg();

	public BasicNode getNode();
	
	
	/**
	 * Returns the TreePath corresponding to the displayed entry.
	 */
	public TreePath getTreePath();
	
	
	/**
	 * Error types
	 */
	public static final int ERROR_NONE					= 0;
	public static final int ERROR_READING_ENTRY			= 1;
	public static final int ERROR_SOLVING_REFERRAL		= 2;
	public static final int ERROR_SEARCHING_CHILDREN	= 3;
	


}

