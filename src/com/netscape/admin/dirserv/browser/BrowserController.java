/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/


/**
 * This controller object displays LDAP entries inside a JTree
 * using a given LDAPConnection.
 *
 * Each time a node is expanded in the JTree, the controller
 * sends some LDAP searches to retreive the child entries and their
 * attributes. The searches are done in background threads (without
 * blocking the Event thread).
 *
 * The controller must be specified a set of 'LDAP suffixes' to be 
 * searched using addSuffix() and removeSuffix().
 *
 */

package com.netscape.admin.dirserv.browser;

import java.util.Vector;
import java.util.Enumeration;
import java.text.Collator;
import java.net.MalformedURLException;
import java.lang.reflect.InvocationTargetException;
import java.awt.Font;

import javax.swing.JTree;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;

import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import netscape.ldap.LDAPAttribute;
import netscape.ldap.LDAPEntry;
import netscape.ldap.LDAPSearchResults;
import netscape.ldap.LDAPSearchConstraints;
import netscape.ldap.LDAPControl;
import netscape.ldap.LDAPSortKey;
import netscape.ldap.LDAPUrl;
import netscape.ldap.util.DN;
import netscape.ldap.controls.LDAPSortControl;



public class BrowserController implements TreeExpansionListener {


	public final static int DISPLAY_ACI_COUNT        = 0x01;
	public final static int DISPLAY_ROLE_COUNT       = 0x02;
	public final static int DISPLAY_ACTIVATION_STATE = 0x04;

	public static final String[] SORT_ATTRIBUTES = {"cn", "givenname", "o", "ou", "sn"};
	
	JTree _tree;
	DefaultTreeModel _treeModel;
	RootNode _rootNode;
	int _displayFlags;
	LDAPConnection _ldc;
	boolean _followReferrals;
	boolean _sorted;
	boolean _showContainerOnly;
	String[] _containerClasses;
	NumSubordinateHacker _numSubordinateHacker;
	int _queueTotalSize;
	int _maxChildren = 0;
	Vector _listeners;
	LDAPConnectionPool _connectionPool;
	IconPool _iconPool;
	LDAPSearchConstraints _searchConstraints;
	
	NodeTaskQueue _refreshQueue;
	


	/**
	 * Public API
	 * ==========
	 */


	public BrowserController(JTree tree, LDAPConnectionPool pool) {
		this(tree, pool, new IconPool());
	}


	public BrowserController(JTree tree, LDAPConnectionPool cpool, IconPool ipool) {
		_tree = tree;
		_iconPool = ipool;
		_rootNode = new RootNode();
		_rootNode.setIcon(_iconPool.getIconForRootNode());
		_treeModel = new DefaultTreeModel(_rootNode);
		_tree.setModel(_treeModel);
		_tree.addTreeExpansionListener(this);
		_tree.setCellRenderer(new BrowserCellRenderer(this));
// TODO: should we call JTree.setRowHeight()
//		_tree.setRowHeight(BrowserCellRenderer.calculateRowHeight());
		_displayFlags = DISPLAY_ACI_COUNT;
		_followReferrals = true;
		_sorted = false;
		_showContainerOnly = true;
		_containerClasses = new String[0];
		_queueTotalSize = 0;
		_listeners = new Vector(2);
		_connectionPool = cpool;
		_searchConstraints = null; // Will be computed on the fly

		_refreshQueue = new NodeTaskQueue("New red", 2);

		// NUMSUBORDINATE HACK
		// Create an empty hacker to avoid null value test.
		// However this value will be overriden by full hacker.
		_numSubordinateHacker = new NumSubordinateHacker();
	}
	
	/**
	 * For compatibility. To be removed later.
	 */
	public BrowserController(JTree tree) {
		this(tree, new LDAPConnectionPool());
	}


	/**
	 * Set the connection for accessing the directory.
	 */
	public void setLDAPConnection(LDAPConnection ldc) {
		String rootNodeName;
		_ldc = ldc;
		if (_ldc != null) {
            _connectionPool.registerDefaultAuth(_ldc.getAuthenticationDN(),
                                           _ldc.getAuthenticationPassword());
			_connectionPool.registerAuth(_ldc);
			rootNodeName = _ldc.getHost() + ":" + _ldc.getPort();
		}
		else {
			rootNodeName = "";
		}
		_rootNode.setDisplayName(rootNodeName);
		startRefresh(null);
	}
	
	
	/**
	 * Return the connection for accessing the directory.
	 */
	public LDAPConnection getLDAPConnection() {
		return _ldc;
	}
	
	
	/**
	 * Return the JTree controlled by this controller.
	 */
	public JTree getTree() {
		return _tree;
	}
	
	
	/**
	 * Return the connection pool used by this controller.
	 * If a client class adds authentication to the connection
	 * pool, it must inform the controller by calling
	 * notifyAuthDataChanged().
	 */
	public LDAPConnectionPool getConnectionPool() {
		return  _connectionPool;
	}


	/**
	 * Return the icon pool used by this controller.
	 */
	public IconPool getIconPool() {
		return  _iconPool;
	}


	/**
	 * Add an LDAP suffix to this controller.
	 * A new node is added in the JTree and a refresh is started.
	 * Return the TreePath of the new node.
	 */
	public TreePath addSuffix(String suffixDn, String parentSuffixDn) {		
		SuffixNode parentNode;
		if (parentSuffixDn != null) {
			parentNode = findSuffixNode(parentSuffixDn, _rootNode);
			if (parentNode == null) {
				throw new IllegalArgumentException("Invalid suffix dn " + parentSuffixDn);
			}
		}
		else {
			parentNode = _rootNode;
		}
		int index = findChildNode(parentNode, suffixDn);
		if (index >= 0) { // A node has alreay this dn -> bug
			throw new IllegalArgumentException("Duplicate suffix dn " + parentSuffixDn);
		}
		else {
			index = - (index + 1);
		}
		SuffixNode newNode = new SuffixNode(suffixDn);
		_treeModel.insertNodeInto(newNode, parentNode, index);
		startRefreshNode(newNode, null, true);
		
		return new TreePath(_treeModel.getPathToRoot(newNode));
	}
	
	

	/**
	 * Remove the an LDAP suffix from this controller.
	 * The controller updates the JTree and returns the TreePath
	 * of the parent node.
	 */
	public TreePath removeSuffix(String suffixDn) {
		BasicNode node = findSuffixNode(suffixDn, _rootNode);		
		TreeNode parentNode = node.getParent();
		removeOneNode(node);
		return new TreePath(_treeModel.getPathToRoot(parentNode));
	}


	/**
	 * Remove all the suffixes.
	 * The controller removes all the nodes from the JTree
	 * except the root.
	 * Return the TreePath of the root node.
	 */
	public TreePath removeAllSuffixes() {
		stopRefresh();
		removeAllChildNodes(_rootNode, false /* Delete suffixes */);
		return new TreePath(_treeModel.getPathToRoot(_rootNode));
	}


	/**
	 * Return the display flags.
	 */
	public int getDisplayFlags() {
		return _displayFlags;
	}
	

	/**
	 * Set the display flags and call startRefresh().
	 */
	public void setDisplayFlags(int flags) {
		_displayFlags = flags;
		startRefresh(null);
	}

	/**
	  * Sets the maximum number of children to display for a node.
	  * 0 if there is no limit 
	  */
	public void setMaxChildren(int maxChildren) {
		_maxChildren = maxChildren;
	}

	/**
	  * Return the maximum number of children to display
	  */
	public int getMaxChildren() {
		return _maxChildren;
	}

	/**
	 * Return true if this controller follows referrals.
	 */
	public boolean getFollowReferrals() {
		return _followReferrals;
	}

	
	/**
	 * Enable/display the following of referrals.
	 * This routine starts a refresh on each referral node.
	 */
	public void setFollowReferrals(boolean yes) {
		_followReferrals = yes;
		startRefreshReferralNodes(_rootNode);
	}

	
	/**
	 * Return true if entries are displayed sorted.
	 */
	public boolean isSorted() {
		return _sorted;
	}


	/**
	 * Enable/disable entry sort.
	 * This routine collapses the JTree and 
	 * invokes startRefresh().
	 */
	public void setSorted(boolean yes) {
		stopRefresh();		
		removeAllChildNodes(_rootNode, true /* Keep suffixes */);
		_sorted = yes;
		_searchConstraints = null; // Force the reconstruction
		startRefresh(null);
	}
	
	
	/**
	 * Return true if only container entries are displayed.
	 * An entry is a container if:
	 *		- it has some children
	 *		- or its class is one of the container classes
	 *		  specified with setContainerClasses().
	 */
	public boolean isShowContainerOnly() {
		return _showContainerOnly;
	}


	/**
	 * Enable or disable container display and call
	 * startRefresh().
	 */
	public void setShowContainerOnly(boolean yes) {
		_showContainerOnly = yes;
		startRefresh(null);
	}


	/**
	 * Find the LDAP entry associated to a TreePath and returns
	 * the describing IBrowserNodeInfo.
	 */
	public IBrowserNodeInfo getNodeInfoFromPath(TreePath path) {
		BasicNode node = (BasicNode)path.getLastPathComponent();
		if (node != null)
			return new BrowserNodeInfo(node);
		else
			return null;
	}


	/**
	 * Return the array of container classes for this controller.
	 * Warning: the returned array is not cloned.
	 */
	public String[] getContainerClasses() {
		return _containerClasses;
	}


	/**
	 * Set the list of container classes and calls startRefresh().
	 * Warning: the array is not cloned.
	 */
	public void setContainerClasses(String[] containerClasses) {
		_containerClasses = containerClasses;
		startRefresh(null);
	}


	/**
	 * NUMSUBORDINATE HACK
	 * Make the hacker public so that RefreshTask can use it.
	 */
	public NumSubordinateHacker getNumSubordinateHacker() {
		return _numSubordinateHacker;
	}


	/**
	 * NUMSUBORDINATE HACK
	 * Set the hacker. Note this method does not trigger any
	 * refresh. The caller is supposed to do it afterward.
	 */
	public void setNumSubordinateHacker(NumSubordinateHacker h) {
		if (h == null) {
			throw new IllegalArgumentException("hacker cannot be null");
		}
		_numSubordinateHacker = h;
	}

	/**
	 * Add a BrowserEventListener to this controller.
	 */
	public void addBrowserEventListener(BrowserEventListener l) {
		_listeners.addElement(l);
	}
	
	
	/**
	 * Remove a BrowserEventListener from this controller.
	 */
	public void removeBrowserEventListener(BrowserEventListener l) {
		_listeners.removeElement(l);
	}
	

	/**
	 * Notify this controller that an entry has been added.
	 * The controller adds a new node in the JTree and
	 * starts refreshing this new node.
	 * This routine returns the tree path about the new entry.
	 */
	public TreePath notifyEntryAdded(IBrowserNodeInfo parentInfo, String newEntryDn) {
		BasicNode parentNode = ((BrowserNodeInfo)parentInfo).getNode();
		BasicNode childNode = new BasicNode(newEntryDn);
		int childIndex;
		if (_sorted) {
			childIndex = findChildNode(parentNode, newEntryDn);
			if (childIndex >= 0) {
				throw new IllegalArgumentException("Duplicate DN " + newEntryDn);
			}
			else {
				childIndex = - (childIndex + 1);
			}
		}
		else {
			childIndex = parentNode.getChildCount();
		}
		parentNode.setLeaf(false);
		_treeModel.insertNodeInto(childNode, parentNode, childIndex);
		startRefreshNode(childNode, null, false);
		return new TreePath(_treeModel.getPathToRoot(childNode));
	}
	

	/**
	 * Notify this controller that a entry has been deleted.
	 * The controller removes the corresponding node from the
	 * JTree and returns the TreePath of the parent node.
	 */
	public TreePath notifyEntryDeleted(IBrowserNodeInfo nodeInfo) {
		BasicNode node = ((BrowserNodeInfo)nodeInfo).getNode();
		if (node == _rootNode) {
			throw new IllegalArgumentException("Root node cannot be removed");
		}
		TreeNode parentNode = node.getParent();
		removeOneNode(node);
		return new TreePath(_treeModel.getPathToRoot(parentNode));
	}
	
	
	/**
	 * Notify this controller that an entry has changed.
	 * The controller starts refreshing the corresponding node.
	 * Child nodes are not refreshed.
	 */
	public void notifyEntryChanged(IBrowserNodeInfo nodeInfo) {
		BasicNode node = ((BrowserNodeInfo)nodeInfo).getNode();
		startRefreshNode(node, null, false);
	}
	
	/**
	 * Notify this controller that a child entry has changed.
	 * The controller has to refresh the corresponding node and (if necessary) itself.	 
	 */
	public void notifyChildEntryChanged(IBrowserNodeInfo nodeInfo, String dn) {
		BasicNode node = ((BrowserNodeInfo)nodeInfo).getNode();
		startRefreshNode(node, null, true);
	}

	/**
	 * Notify this controller that a child entry has been added.
	 * The controller has to refresh the corresponding node and (if necessary) itself.	 
	 */
	public void notifyChildEntryAdded(IBrowserNodeInfo nodeInfo, String dn) {
		BasicNode node = ((BrowserNodeInfo)nodeInfo).getNode();
		startRefreshNode(node, null, true);
	}

	/**
	 * Notify this controller that a child entry has been deleted.
	 * The controller has to refresh the corresponding node and (if necessary) itself.	 
	 */
	public void notifyChildEntryDeleted(IBrowserNodeInfo nodeInfo, String dn) {
		BasicNode node = ((BrowserNodeInfo)nodeInfo).getNode();
		if (node.getParent() != null) {
			startRefreshNode((BasicNode) node.getParent(), null, true);
		} else {
			startRefreshNode(node, null, true);
		}
	}
	
	
	/**
	 * Notify this controller that authentication data
	 * have changed in the connection pool for the specified
	 * url.
	 * The controller starts refreshing the node which
	 * represent entries from the url.
	 */
	public void notifyAuthDataChanged(LDAPUrl url) {
		// TODO: temporary implementation
		//		we should refresh only nodes :
		//		- whose URL matches 'url'
		//		- whose errorType == ERROR_SOLVING_REFERRAL and
		//		  errorArg == url
		startRefreshReferralNodes(_rootNode);
	}
	
	
	/**
	 * Start a refresh from the specified node.
	 * If some refresh are on-going on descendent nodes,
	 * they are stopped.
	 * If nodeInfo is null, refresh is started from the root.
	 */
	public void startRefresh(IBrowserNodeInfo nodeInfo) {
		BasicNode node;
		if (nodeInfo == null) {
			node = _rootNode;
		}
		else {
			node = ((BrowserNodeInfo)nodeInfo).getNode();
		}
		stopRefreshNode(node);
		startRefreshNode(node, null, true);
	}


	/**
	 * Equivalent to startRefresh(null).
	 */
	public void startRefresh() {
		startRefresh(null);
	}
	

	/**
	 * Stop the current refreshing.
	 * Nodes being expanded are collapsed.
	 */
	public void stopRefresh() {
		stopRefreshNode(_rootNode);
		// TODO: refresh must be stopped in a clean state.
	}
	
	
	/**
	 * Shutdown the controller : all the backgroup threads are stopped.
	 * After this call, the controller is no longer usable.
	 */
	public void shutDown() {
		_tree.removeTreeExpansionListener(this);
		_refreshQueue.shutdown();
		_connectionPool.flush();
	}
	



	/**
	 * Start refreshing the whole tree from the specified node.
	 * We queue a refresh which:
	 *		- updates the base node
	 *		- is recursive
	 */
	void startRefreshNode(BasicNode node, LDAPEntry localEntry, boolean recursive) {
		if (node == _rootNode) {
			// For the root node, readBaseEntry is meaningless.
			if (recursive) {
				// The root cannot be queued directly.
				// We need to queue each child individually.
				Enumeration e = _rootNode.children();
				while (e.hasMoreElements()) {
					BasicNode child = (BasicNode)e.nextElement();
					startRefreshNode(child, null, true);
				}
			}
		}
		else {
			_refreshQueue.queue(new RefreshTask(node, this, localEntry, recursive));
			// The task does not *see* suffixes.
			// So we need to propagate the refresh on 
			// the subsuffixes if any.
			if (recursive && (node instanceof SuffixNode)) {
				Enumeration e = node.children();
				while (e.hasMoreElements()) {
					BasicNode child = (BasicNode)e.nextElement();
					if (child instanceof SuffixNode) {
						startRefreshNode(child, null, true);
					}
				}
			}
		}
	}
	



	/**
	 * Stop refreshing below this node.
	 * TODO: this method is very costly when applied to 
	 * something else than the root node.
	 */
	void stopRefreshNode(BasicNode node) {
		if (node == _rootNode) {
			_refreshQueue.cancelAll();
		}
		else {
			Enumeration e = node.children();
			while (e.hasMoreElements()) {
				BasicNode child = (BasicNode)e.nextElement();
				stopRefreshNode(child);
			}
			_refreshQueue.cancelForNode(node);
		}
	}
	

	
	/**
	 * Call startRefreshNode() on each referral node accessible
	 * from parentNode.
	 */
	void startRefreshReferralNodes(BasicNode parentNode) {
		Enumeration e = parentNode.children();
		while (e.hasMoreElements()) {
			BasicNode child = (BasicNode)e.nextElement();
			if ((child.getReferral() != null) || (child.getRemoteUrl() != null)) {
				startRefreshNode(child, null, true);
			}
			else {
				startRefreshReferralNodes(child);
			}
		}
	}



	/**
	 * Remove all the children below parentNode *without changing 
	 * the leaf state*.
	 * If specified, it keeps the SuffixNode and recurse on
	 * them. Inform the tree model.
	 */
	void removeAllChildNodes(BasicNode parentNode, boolean keepSuffixes) {
		for (int i = parentNode.getChildCount() - 1; i >= 0; i--) {
			BasicNode child = (BasicNode)parentNode.getChildAt(i);
			if ((child instanceof SuffixNode) && keepSuffixes) {
				removeAllChildNodes(child, true);
				child.setRefreshNeededOnExpansion(true);
			}
			else {
				child.removeFromParent();
			}
		}
		_treeModel.nodeStructureChanged(parentNode);
	}

	/**
	 * TreeExpansionListener implementation
	 * ====================================
	 */
	 
	
	/**
	 * For BrowserController private use.
	 */
	public void treeExpanded(TreeExpansionEvent event) {
		BasicNode basicNode = (BasicNode)event.getPath().getLastPathComponent();
		if (basicNode.isRefreshNeededOnExpansion()) {
			basicNode.setRefreshNeededOnExpansion(false);
			// Starts a recursive refresh which does not read the base entry
			startRefreshNode(basicNode, null, true);
		}
	}


	/**
	 * For BrowserController private use.
	 */
	public void treeCollapsed(TreeExpansionEvent event) {
		Object node = event.getPath().getLastPathComponent();
		if (!(node instanceof RootNode)) {
			BasicNode basicNode = (BasicNode)node;
			stopRefreshNode(basicNode);
		}
	}



	/**
	 * Routines for the task classes
	 * =============================
	 *
	 * Note that these routines only read controller variables.
	 * They do not alter any variable: so they can be safely
	 * called by task threads without synchronize clauses.
	 */


	/**
	 * The tree model created by the controller and assigned
	 * to the JTree.
	 */
	DefaultTreeModel getTreeModel() {
		return _treeModel;
	}
	

	/**
	 * Return the LDAP search filter to use for searching child entries.
	 * If _showContainerOnly is true, the filter will select only the
	 * container entries. If not, the filter will select all the children.
	 */
	String getChildSearchFilter() {
		String result;
		if (_showContainerOnly) {
			result = "(|(&(numsubordinates=*)(numsubordinates>=1)(|(objectclass=*)(objectclass=ldapsubentry)))";
			for (int i = 0; i < _containerClasses.length; i++) {
				result += "(objectclass=" + _containerClasses[i] + ")";
			}
			result += ")";
		}
		else {
			result = "(|(objectclass=*)(objectclass=ldapsubentry))";
		}
		
		return result;
	}




	/**
	 * Return the LDAP connection to reading the base entry of a node.
	 */
	LDAPConnection findConnectionForLocalEntry(BasicNode node)
	throws LDAPException {
		LDAPConnection result;
		if (node == _rootNode || node == null) {
			result = _ldc;	
		}
		else {
			result = findConnectionForDisplayedEntry((BasicNode)node.getParent());
		}
		return result;
	}
	

	/**
	 * Return the LDAP connection to search the displayed entry
	 * (which can be the local or remote entry).
	 */
	LDAPConnection findConnectionForDisplayedEntry(BasicNode node)
	throws LDAPException {
		LDAPConnection result;
		if (_followReferrals && node != null &&
			(node.getRemoteUrl() != null)) {
			result = _connectionPool.getConnection(node.getRemoteUrl());
		}
		else {
			result = findConnectionForLocalEntry(node);
		}
		return result;
	}
	
	
	
	/**
	 * Release a connection returned by  
	 *		selectConnectionForChildEntries()
	 * or
	 *		selectConnectionForBaseEntry()
	 */
	void releaseLDAPConnection(LDAPConnection ldc) {
		if (ldc != _ldc) { // Thus it comes from the connection pool
			_connectionPool.releaseConnection(ldc);
		}
	}


	/**
	 *
	 */
	LDAPUrl findUrlForLocalEntry(BasicNode node) {
		LDAPUrl result;
		if (node == _rootNode || node == null) {
			result = LDAPConnectionPool.makeLDAPUrl(_ldc, "");
		}
		else {
			BasicNode parent = (BasicNode)node.getParent();
			LDAPUrl parentUrl = findUrlForDisplayedEntry(parent);
			result = LDAPConnectionPool.makeLDAPUrl(parentUrl, node.getDN());
		}
		return result;
	}


	/**
	 *
	 */
	LDAPUrl findUrlForDisplayedEntry(BasicNode node) {
		LDAPUrl result;
		if (_followReferrals && node != null &&
			(node.getRemoteUrl() != null)) {
			result = node.getRemoteUrl();
		}
		else {
			result = findUrlForLocalEntry(node);
		}
		return result;
	}


	/**
	 * Returns the DN to use for searching children of a given node.
	 * In most cases, it's node.getDN(). However if node has referral data
	 * and _followReferrals is true, the result is calculated from the
	 * referral resolution.
	 */
	String findBaseDNForChildEntries(BasicNode node) {
		String result;
		
		if (_followReferrals && (node.getRemoteUrl() != null)) {
			result = node.getRemoteUrl().getDN();
		}
		else {
			result = node.getDN();
		}
		return result;
	}




	boolean isDisplayedEntryRemote(BasicNode node) {
		boolean result;
		if (node == null) {
			return false;
		}
		if (_followReferrals) {
			if (node == _rootNode) {
				result = false;
			}
			else if (node.getRemoteUrl() != null) {
				result = true;
			}
			else {
				result = isDisplayedEntryRemote((BasicNode)node.getParent());
			}
		}
		else {
			result = false;
		}
		
		return result;
	}


	/**
	 * Returns the list of attributes for the red search.
	 */
	String[] getAttrsForRedSearch() {
		Vector v = new Vector();
		
		v.addElement("objectclass");
		v.addElement("numsubordinates");
		v.addElement("ref");
		if ((_displayFlags & DISPLAY_ACI_COUNT) != 0) {
			v.addElement("aci");
		}
		if ((_displayFlags & DISPLAY_ROLE_COUNT) != 0) {
			v.addElement("nsrole");
		}
		if ((_displayFlags & DISPLAY_ACTIVATION_STATE) != 0) {
			v.addElement("nsaccountlock");
		}
		
		String[] result = new String[v.size()];
		v.toArray(result);
		return result;
	}


	/**
	 * Returns the list of attributes for the green search.
	 */
	String[] getAttrsForGreenSearch() {
		return new String[] {
			"aci",
			"nsrole",
			"nsaccountlock"
		};
	}

	/**
	 * Returns the list of attributes for the black search.
	 */
	String[] getAttrsForBlackSearch() {
		return new String[] {
			"objectclass",
			"numsubordinates",
			"ref",
			"aci",
			"nsrole",
			"nsaccountlock"
		};
	}

	/**
	 * Returns the list of attributes for the black search.
	 */
	LDAPSearchConstraints getSearchConstraints() {
		if (_searchConstraints == null) {
			LDAPControl ctls[] = new LDAPControl[_sorted ? 2 : 1];
			ctls[0] = new LDAPControl(LDAPControl.MANAGEDSAIT, true, null);
			if (_sorted) {
				LDAPSortKey[] keys = new LDAPSortKey[SORT_ATTRIBUTES.length];
				for (int i=0; i<keys.length; i++) {
					keys[i] = new LDAPSortKey(SORT_ATTRIBUTES[i]);
				}
				ctls[1] = new LDAPSortControl(keys, true);
			}
			_searchConstraints = (LDAPSearchConstraints)_ldc.getSearchConstraints().clone();
			_searchConstraints.setMaxResults(_maxChildren);
			_searchConstraints.setServerControls(ctls); // Return referral entries
		}
		return _searchConstraints;
	}


	/**
	 * Callbacks invoked by task classes
	 * =================================
	 *
	 * The routines below are invoked by the task classes; they
	 * update the nodes and the tree model. 
	 *
	 * To ensure the consistency of the tree model, these routines
	 * are not invoked directly by the task classes: they are
	 * invoked using SwingUtilities.invokeAndWait() (each of the
	 * methods XXX() below has a matching wrapper invokeXXX()).
	 * 
	 */


	/**
	 * Invoked when the refresh task has finished the red operation:
	 * it has read the attributes of the base entry ; the result of the
	 * operation is:
	 *		- an LDAPEntry if successful
	 *		- an Exception if failed
	 */

	private void refreshTaskDidProgress(RefreshTask task, int oldState, int newState) {
		BasicNode node = task.getNode();
		boolean nodeChanged = false;
		
		//task.dump();
		
		// Manage events
		if (oldState == RefreshTask.QUEUED) {
			checkUpdateEvent(true);
		}
		if (task.isInFinalState()) {
			checkUpdateEvent(false);
		}
		
		if (newState == RefreshTask.FAILED) {
			// In case of NoSuchObject, we simply remove the node from the tree.
			// Except when it's due a to referral resolution: we keep the node
			// in order the user can fix the referral. 
			if (isNoSuchObjectLDAPException(task.getException()) &&
			    (oldState != RefreshTask.SOLVING_REFERRAL)) {
				removeOneNode(node);
			}
			else {				
				if (oldState == RefreshTask.SOLVING_REFERRAL) {		
					node.setRemoteUrl(task.getRemoteUrl());
					if (task.getRemoteEntry() != null) {
						/* This is the case when there are multiple hops in the referral
						   and so we have a remote referral entry but not the entry that it
						   points to */
						updateNodeRendering(node, task.getRemoteEntry());
					}
				}
				node.setError(new BasicNode.Error(oldState, task.getException(), task.getExceptionArg()));
				nodeChanged = updateNodeRendering(node, task.getDisplayedEntry());
			}
		}
		else if ((newState == RefreshTask.CANCELLED) &&
		         (newState == RefreshTask.INTERRUPTED)) {

			// Let's collapse task.getNode()
			_tree.collapsePath(new TreePath(_treeModel.getPathToRoot(node)));
			
			// TODO: should we reflect this situation visually ?
		}
		else {
		
			if ((oldState != RefreshTask.SEARCHING_CHILDREN) &&
			    (newState == RefreshTask.SEARCHING_CHILDREN)) {
				// The children search is going to start
				if (canDoDifferentialUpdate(task)) {
					Enumeration e = node.children();
					while (e.hasMoreElements()) {
						BasicNode child = (BasicNode)e.nextElement();
						child.setObsolete(true);
					}
				}
				else {
					removeAllChildNodes(node, true /* Keep suffixes */);
				}
			}
			
			if (oldState == RefreshTask.READING_LOCAL_ENTRY) {
				/* The task is going to try to solve the referral if there's one.  
				   If succeeds we will update the remote url.  Set it to null for 
				   the case when there was a referral and it has been deleted */
				node.setRemoteUrl(null); 
				LDAPEntry localEntry = task.getLocalEntry();
				nodeChanged = updateNodeRendering(node, localEntry);
			}
			else if (oldState == RefreshTask.SOLVING_REFERRAL) {				
				node.setRemoteUrl(task.getRemoteUrl());
				updateNodeRendering(node, task.getRemoteEntry());
				nodeChanged = true;
			}
			else if (oldState == RefreshTask.DETECTING_CHILDREN) {
				if (node.isLeaf() != task.isLeafNode()) {
					node.setLeaf(task.isLeafNode());
					updateNodeRendering(node, task.getDisplayedEntry());
					nodeChanged = true;
					if (node.isLeaf()) {
						removeAllChildNodes(node, false /* Remove suffixes */);
					}
				}
			}
			else if (oldState == RefreshTask.SEARCHING_CHILDREN) {

				updateChildNodes(task);
				if (newState == RefreshTask.FINISHED) { // The children search is finished
					if (canDoDifferentialUpdate(task)) {
						// Remove obsolete child nodes
						// Note: we scan in the reverse order to preserve indices
						for (int i = node.getChildCount()-1; i >= 0; i--) {
							BasicNode child = (BasicNode)node.getChildAt(i);
							if (child.isObsolete()) {
								removeOneNode(child);
							}
						}
					}
					// The node may have become a leaf.
					if (node.getChildCount() == 0) {
						node.setLeaf(true);
						updateNodeRendering(node, task.getDisplayedEntry());
						nodeChanged = true;
					}
				}
			}

			if (newState == RefreshTask.FINISHED) {
				if (node.getError() != null) {
					node.setError(null);
					nodeChanged = updateNodeRendering(node, task.getDisplayedEntry());
				}
			}
		}
		
		
		if (nodeChanged) {
			_treeModel.nodeChanged(task.getNode());
		}

		if (node.isLeaf() && (node.getChildCount() >= 1)) {
			throw new IllegalStateException("Inconsistent node: " + node.getDN());
		}
		
	}



	void invokeRefreshTaskDidProgress(final RefreshTask task, final int oldState, final int newState)
	throws InterruptedException {
	
		Runnable r = new Runnable() {
			RefreshTask _task = task;
			int _oldState = oldState;
			int _newState = newState;
			public void run() {
				try {					
					refreshTaskDidProgress(_task, _oldState, _newState);
				}
				catch(Exception x) {
					x.printStackTrace();
				}
			}
		};
		swingInvoke(r);						   
	}
	
	

	/**
	 * Core routines shared by the callbacks above
	 * ===========================================
	 */
	 

	private void updateChildNodes(RefreshTask task) {
		BasicNode parent = task.getNode();
		Vector insertIndex = new Vector();
		Vector changedIndex = new Vector();
		boolean differential = canDoDifferentialUpdate(task);
		
		// NUMSUBORDINATE HACK
		// To avoid testing each child to the hacker,
		// we verify here if the parent node is parent of
		// any entry listed in the hacker.
		// In most case, the dontTrust flag will false and
		// no overhead will be caused in the child loop.
		LDAPUrl parentUrl = findUrlForDisplayedEntry(parent);
		boolean dontTrust = _numSubordinateHacker.containsChildrenOf(parentUrl);

		// Walk through the entries
		Enumeration e = task.getChildEntries().elements();
		while (e.hasMoreElements()) {
			LDAPEntry entry = (LDAPEntry) e.nextElement();
			BasicNode child;

			// Search a child node matching the DN of the entry
			int index;
			if (differential) {
//				System.out.println("Differential mode -> starting to search");
				index = findChildNode(parent, entry.getDN());
//				System.out.println("Differential mode -> ending to search");
			}
			else {
				index = - (parent.getChildCount() + 1);
			}

			// If no node matches, we create a new node
			if (index < 0) {
				// -(index + 1) is the location where to insert the new node
				index = -(index + 1);
				child = new BasicNode(entry.getDN());
				parent.insert(child, index);
				updateNodeRendering(child, entry);
				insertIndex.addElement(new Integer(index));
//				System.out.println("Inserted " + child.getDN() + " at " + index);
			}
			else { // Else we update the existing one
				child = (BasicNode)parent.getChildAt(index);
				if (updateNodeRendering(child, entry)) {
					changedIndex.addElement(new Integer(index));
				}
				// The node is no longer obsolete
				child.setObsolete(false);
			}

			// NUMSUBORDINATE HACK
			// Let's see if child has subordinates or not.
			// Thanks to slapd, we cannot always trust the
			// numSubOrdinates attribute. If the child entry's DN
			// is found in the hacker's list, then we ignore
			// the numSubordinate attribute... :((
			int numSubOrdinates = child.getNumSubOrdinates();
			boolean hasNoSubOrdinates;
			if ((numSubOrdinates == 0) && dontTrust) {
				LDAPUrl childUrl = findUrlForDisplayedEntry(child);
				if (_numSubordinateHacker.contains(childUrl)) {
					// The numSubOrdinates we have is unreliable.
					// child may potentially have subordinates.
					hasNoSubOrdinates = false;
//					System.out.println("numSubordinates of " + childUrl +
//										" is not reliable");
				}
				else {
					// We can trust this 0 value
					hasNoSubOrdinates = true;
				}
			}
			else {
				hasNoSubOrdinates = (numSubOrdinates == 0);
			}
			


			// Propagate the refresh
			// Note: logically we should unconditionaly call:
			//	startRefreshNode(child, false, true);
			//
			// However doing that saturates _refreshQueue
			// with many nodes. And, by design, RefreshTask 
			// won't do anything on a node if:
			//		- this node has no subordinates
			//		- *and* this node has no referral data
			// So we test these conditions here and 
			// skip the call to startRefreshNode() if
			// possible.
			//
			if (!hasNoSubOrdinates ||
			    (child.getReferral() != null)) {
				startRefreshNode(child, entry, true);
			}
		}


		// Inform the tree model that we have created some new nodes
		if (insertIndex.size() >= 1) {
			_treeModel.nodesWereInserted(parent, intArrayFromVector(insertIndex));
		}
		if (changedIndex.size() >= 1) {
			_treeModel.nodesChanged(parent, intArrayFromVector(changedIndex));
		}
	}



	private boolean canDoDifferentialUpdate(RefreshTask task) {
		return (
			(task.getNode().getChildCount() >= 1) &&
			(task.getNode().getNumSubOrdinates() <= 100)
		);
	}


	/**
	 * Recompute the rendering props of a node (text, style, icon) according:
	 *		- the state of this node
	 *		- the LDAPEntry displayed by this node
	 */
	private boolean updateNodeRendering(BasicNode node, LDAPEntry entry) {
		if (entry != null) {
			// Get the numsubordinates
			node.setNumSubOrdinates(getNumSubOrdinates(entry));
			node.setReferral(getReferral(entry));
		}			
		// Get the aci count
		int aciCount;
		
		if (((_displayFlags & DISPLAY_ACI_COUNT) != 0) && (entry != null)) {
			LDAPAttribute aciAttr = entry.getAttribute("aci");
			if (aciAttr != null) {
				aciCount = aciAttr.size();
			}
			else {
				aciCount = 0;
			}
		}
		else {
			aciCount = 0;
		}
			
		// Get the role count
		int roleCount;
		if (((_displayFlags & DISPLAY_ROLE_COUNT) != 0) && (entry != null)) {
			LDAPAttribute roleAttr = entry.getAttribute("nsrole");
			if (roleAttr != null) {
				roleCount = roleAttr.size();
			}
			else {
				roleCount = 0;
			}
		}
		else {
			roleCount = 0;
		}
		

		// Get the nsaccountlock
		boolean nsaccountlock;
		if (((_displayFlags & DISPLAY_ACTIVATION_STATE) != 0) && (entry != null)) {
			LDAPAttribute lockAttr = entry.getAttribute("nsaccountlock");
			if (lockAttr != null) {
				String[] values = lockAttr.getStringValueArray();
				if (values.length >= 1) {				    
					nsaccountlock = values[0].equalsIgnoreCase("true");
					if (nsaccountlock) {
						/* Check if is a role... in this case nsaccountlock
						   makes no sense */						
						LDAPAttribute objectclassAttr = entry.getAttribute("objectclass");
						if (objectclassAttr != null) {
							Enumeration e = objectclassAttr.getStringValues();
							String value;
							boolean isRoleDefinition=false;
							boolean isLDAPSubentry=false;
							while (e.hasMoreElements() && 
								   (!isRoleDefinition || !isLDAPSubentry)) {
								value = (String)e.nextElement();								
								if (value.equalsIgnoreCase("nsroledefinition")) {
									isRoleDefinition=true;
								} else if (value.equalsIgnoreCase("ldapsubentry")) {
									isLDAPSubentry = true;
								}								
							}							
							/* It's a role -> we don't know about the state of the role */
							if (isRoleDefinition && isLDAPSubentry) {
								nsaccountlock = false;
							}
						}
					}					
				}
				else {
					nsaccountlock = false;
				}
			}
			else {
				nsaccountlock = false;
			}
		}
		else {
			nsaccountlock = false;
		}


		// Select the icon according the objectclass,...
		int modifiers = 0;
		if (node.isLeaf() && (node.getNumSubOrdinates() <= 0)) {
			modifiers |= _iconPool.MODIFIER_LEAF;
		}
		if (node.getReferral() != null) {
			modifiers |= _iconPool.MODIFIER_REFERRAL;
		}
		if (node.getError() != null) {
			modifiers |= _iconPool.MODIFIER_ERROR;
		}
		if (nsaccountlock) {
			modifiers |= _iconPool.MODIFIER_INACTIVATED;			
		}
		LDAPAttribute objectClass = null;
		if (entry != null) {
			objectClass = entry.getAttribute("objectclass");
		}
		Icon newIcon = _iconPool.getIcon(objectClass, modifiers);


		// Contruct the icon text according the dn, the aci count...
		StringBuffer sb2 = new StringBuffer();
		if (aciCount >= 1) {
			sb2.append(String.valueOf(aciCount));
			if (aciCount == 1) {
				sb2.append(" aci");
			}
			else {
				sb2.append(" acis");
			}
		}
		if (roleCount >= 1) {
			if (sb2.length() >= 1) sb2.append(", ");
			sb2.append(String.valueOf(roleCount));
			if (roleCount == 1) {
				sb2.append(" role");
			}
			else {
				sb2.append(" roles");
			}
		}
		StringBuffer sb1 = new StringBuffer();
		String rdn;
		if (_followReferrals && (node.getRemoteUrl() != null)) {
			rdn = node.getRemoteRDN();
		}
		else {
			rdn = node.getRDN();
		}
		sb1.append(rdn);
		if (sb2.length() >= 1) {
			sb1.append("  (");
			sb1.append(sb2);
			sb1.append(")");
		}
		String newDisplayName = sb1.toString();
		
		// Select the font style according referral
		int newStyle = 0;
		if (isDisplayedEntryRemote(node)) {
			newStyle |= Font.ITALIC;
		}
		if (node instanceof SuffixNode) {
			newStyle |= Font.BOLD;
		}
		
		// Determine if the rendering needs to be updated
		boolean changed = (
			(node.getIcon() != newIcon) ||
		    (node.getDisplayName() != newDisplayName) ||
			(node.getFontStyle() != newStyle)
		);
		if (changed) {
			node.setIcon(newIcon);
			node.setDisplayName(newDisplayName);
			node.setFontStyle(newStyle);
		}


		return changed;
	}

	
	/**
	 * Find a child node matching a given DN.
	 * Choose the most efficient way according the state of the _sorted property.
	 *
	 * result >= 0		result is the index of the node matching childDn
	 * result < 0		-(result + 1) is the index at which the new node must be inserted
	 */
	int findChildNode(BasicNode parent, String childDn) {
		int result;
		if (_sorted) {
			result = findSortedChildNode(parent, childDn, 0, parent.getChildCount()-1);
		}
		else {
			result = findUnSortedChildNode(parent, childDn);
		}
		return result;
	}


	/**
	 */
	int findSortedChildNode(BasicNode parent, String childDn, int start, int end) {
		int result;
		// INVARIANT : start-1 < childDn < end+1
		if (start > end) {
			if (start != end+1) throw new IllegalStateException("Bug in findSortedChildNode");
			result = -(start+1);
		}
		else {
			int cr = compareDnToChildNode(childDn, parent, start);
			if (cr < 0) { // start-1 < childDn < start
				result = -(start + 1);
			}
			else if (cr == 0) { // childDn == start
				result = start;
			}
			else if (start == end) { // end < childDn < end+1
				result = -(end+1 + 1);
			}
			else { // childDn must be compared to the 'end' child
				cr = compareDnToChildNode(childDn, parent, end);
				if (cr > 0) { // end < childDn < end+1
					result = -(end+1 + 1);
				}
				else if (cr == 0) { // childDn == end;
					result = end;
				}
				else { // start < childDn < end
					   // childDn must be compared with the median child
					int median = (start + end) / 2;
					cr = compareDnToChildNode(childDn, parent, median);
					if (cr < 0) { // start < childDn < median
						result = findSortedChildNode(parent, childDn, start+1, median-1);
					}
					else if (cr == 0) { // childDn == median
						result = median;
					}
					else { // median < childDn < end
						result = findSortedChildNode(parent, childDn, median+1, end-1);
					}
				}
			}
		}
		return result;
	}


	/**
	 * We assume that parent.getChildCount() >= 1
	 */
	int findUnSortedChildNode(BasicNode parent, String childDn) {
		int childCount = parent.getChildCount();
		int i = 0, cr = -1;
		while ((i < childCount) && 
		       !childDn.equals(((BasicNode)parent.getChildAt(i)).getDN())) {
			i++;
		}
		if (i >= childCount) { // Not found
			i = -(childCount + 1);
		}
		
		return i;
	}
	
	
	/**
	 * Follow the String.compare() result convention.
	 * result < 0	-> dn < parent.getChild[childIndex]
	 * result == 0 	-> dn == parent.getChild[childIndex]
	 * result > 0	-> dn > parent.getChild[childIndex]
	 */
	int compareDnToChildNode(String dn, BasicNode parent, int childIndex) {
		BasicNode node = (BasicNode)parent.getChildAt(childIndex);
		return Collator.getInstance().compare(dn, node.getDN());
	}
	
	

	/**
	 * Remove a single node from the tree model.
	 * It takes care to cancel all the tasks associated to this node.
	 */
	private void removeOneNode(BasicNode node) {
		stopRefreshNode(node);
		_treeModel.removeNodeFromParent(node);
	}


	/**
	 * BrowserEvent management
	 * =======================
	 *
	 * This method computes the total size of the queues,
	 * compares this value with the last computed and 
	 * decides if an update event should be fired or not.
	 *
	 * It's invoked by task classes through SwingUtilities.invokeLater()
	 * (see the wrapper below). That means the event handling routine
	 * (processBrowserEvent) is executed in the event thread.
	 */
	private void checkUpdateEvent(boolean taskIsStarting) {
		int newSize = _refreshQueue.size();
		if (!taskIsStarting) {
			newSize = newSize - 1;
		}
		if (newSize != _queueTotalSize) {
			if ((_queueTotalSize == 0) && (newSize >= 1)) {
				fireEvent(BrowserEvent.UPDATE_START);
			}
			else if ((_queueTotalSize >= 1) && (newSize == 0)) {
				fireEvent(BrowserEvent.UPDATE_END);
			}
			_queueTotalSize = newSize;
		}
	}
	
	
	private void fireEvent(int id) {
		BrowserEvent event = new BrowserEvent(this, id);
		Enumeration e = _listeners.elements();
		while (e.hasMoreElements()) {
			BrowserEventListener l = (BrowserEventListener)e.nextElement();
			l.processBrowserEvent(event);
		}
	}
	

	/**
	 * Miscellaneous private routines
	 * ==============================
	 */
	 
	
	/**
	 * Find a SuffixNode in the tree model.
	 */
	SuffixNode findSuffixNode(String suffixDn, SuffixNode suffixNode) {
		SuffixNode result;
		
		if (suffixNode.dnEquals(suffixDn)) {
			result = suffixNode;
		}
		else {			
			int childCount = suffixNode.getChildCount();			
			if (childCount == 0) {
				result = null;
			}
			else {
				BasicNode child;
				int i = 0;
				boolean found = false;
				do {
					child = (BasicNode)suffixNode.getChildAt(i) ;
					if (child.dnEquals(suffixDn)) {
						found = true;
					}
					i++;
				}
				while ((i < childCount) && !found);
				if (!found) {
					result = null;
				}
				else if (child instanceof SuffixNode) {
					result = (SuffixNode)child;
				}
				else {
					// A node matches suffixDn however it's not a suffix node.
					// There's a bug in the caller.
					throw new IllegalArgumentException(suffixDn + " is not a suffix node");
				}
			}
		}
		
		return result;
	}



	/**
	 * Return true if x is an LDAPException with resultCode = NO_SUCH_OBJECT.
	 */
	private boolean isNoSuchObjectLDAPException(Object x) {
		boolean result;
		if (x instanceof LDAPException) {
			LDAPException lx = (LDAPException)x;
			result = (lx.getLDAPResultCode() == lx.NO_SUCH_OBJECT);
		}
		else {
			result = false;
		}
		return result;
	}



	/**
	 * Get the value of the numsubordinates attribute.
	 * If numsubordinates is not present, returns 0.
	 */
	public static int getNumSubOrdinates(LDAPEntry entry) {
		int result;
		
		LDAPAttribute attr = entry.getAttribute("numsubordinates");
		if (attr == null) {
			result = 0;
		}
		else {
			try {
				result = Integer.parseInt(attr.getStringValueArray()[0]);
			}
			catch(NumberFormatException x) {
				result = 0;
			}
		}
		
		return result;
	}


	/**
	 * Returns the value of the 'ref' attribute.
	 * null if the attribute is not present.
	 */
	public static String[] getReferral(LDAPEntry entry) {
		String[] result = null;
		LDAPAttribute attr = entry.getAttribute("objectclass");
		if (attr != null) {
			Enumeration objectclasses = attr.getStringValues();
			if (objectclasses != null) {
				boolean isReferral = false;
				while (objectclasses.hasMoreElements() && !isReferral) {
					String className = (String)objectclasses.nextElement();
					isReferral = className.equalsIgnoreCase("referral");
				}
				if (isReferral) {
					attr = entry.getAttribute("ref");
					if ((attr != null) && (attr.size() >= 1)) {
						result = attr.getStringValueArray();
					}
				}
			}
		}
		return result;
	}


	/**
	 * Returns true if the node is expanded.
	 */
	public boolean nodeIsExpanded(BasicNode node) {
		TreePath tp = new TreePath(_treeModel.getPathToRoot(node));
		return _tree.isExpanded(tp);
	}



	/**
	 * Vector utilities
	 */
	static int[] intArrayFromVector(Vector v) {
		int[] result = new int[v.size()];
		for (int i = 0; i < result.length; i++) {
			result[i] = ((Integer)v.elementAt(i)).intValue();
		}
		return result;
	}
	
	static LDAPEntry[] entryArrayFromVector(Vector v) {
		LDAPEntry[] result = new LDAPEntry[v.size()];
		v.toArray(result);
		return result;
	}
	
	static BasicNode[] nodeArrayFromVector(Vector v) {
		BasicNode[] result = new BasicNode[v.size()];
		v.toArray(result);
		return result;
	}
	
	
	
	/**
	 * For debugging purpose: allows to switch easily
	 * between invokeLater() and invokeAndWait() for
	 * experimentation...
	 */
	static void swingInvoke(Runnable r)
	throws InterruptedException {
		try {
			SwingUtilities.invokeAndWait(r);
		}
		catch(InterruptedException x) {
			throw x;
		}
		catch(InvocationTargetException x) {
			// Probably a very big trouble...
			x.printStackTrace();
		}
	}



	class BrowserNodeInfo implements IBrowserNodeInfo {

		BasicNode _node;
		LDAPUrl _url;
		boolean _isRemote;
		boolean _isSuffix;
		boolean _isRootNode;
		String[] _referral;
		int _numSubOrdinates;
		int _errorType;
		Exception _errorException;
		Object _errorArg;


		public BrowserNodeInfo(BasicNode node) {
			_node = node;			
			_url = findUrlForDisplayedEntry(node);
			
			_isRootNode = node instanceof RootNode;
			_isRemote = isDisplayedEntryRemote(node);
			_isSuffix = node instanceof SuffixNode;
			_referral = node.getReferral();
			_numSubOrdinates = node.getNumSubOrdinates();
			if (node.getError() != null) {
				BasicNode.Error error = node.getError();
				switch(error.getType()) {
					case RefreshTask.READING_LOCAL_ENTRY:
						_errorType = ERROR_READING_ENTRY;
						break;
					case RefreshTask.SOLVING_REFERRAL:
						_errorType = ERROR_SOLVING_REFERRAL;
						break;
					case RefreshTask.DETECTING_CHILDREN:
					case RefreshTask.SEARCHING_CHILDREN:
						_errorType = ERROR_SEARCHING_CHILDREN;
						break;
						
				}
				_errorException = error.getException();
				_errorArg = error.getArg();
			}
		}

		public BasicNode getNode() {
			return _node;
		}
		
		
		public LDAPUrl getURL() {
			return _url;
		}
		
		public boolean isRootNode() {
			return _isRootNode;
		}

		public boolean isSuffix() {
			return _isSuffix;
		}

		public boolean isRemote() {
			return _isRemote;
		}

		public String[] getReferral() {
			return _referral;
		}

		public int getNumSubOrdinates() {
			return _numSubOrdinates;
		}
		
		public int getErrorType() {
			return _errorType;
		}

		public Exception getErrorException() {
			return _errorException;
		}

		public Object getErrorArg() {
			return _errorArg;
		}

		public TreePath getTreePath() {
			return new TreePath(_treeModel.getPathToRoot(_node));
		}
		
		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append(getURL());
			if (getReferral() != null) {
				sb.append(" -> ");
				sb.append(getReferral());
			}
			return sb.toString();
		}
		
	}


}













