/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.browser;

import java.util.Hashtable;
import java.util.Enumeration;

import java.awt.Canvas;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.MediaTracker;
import java.awt.image.ImageObserver;
import java.awt.image.PixelGrabber;
import java.awt.image.ColorModel;
import java.awt.image.MemoryImageSource;
import javax.swing.Icon;

import netscape.ldap.LDAPAttribute;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.admin.dirserv.DSUtil;


public class IconPool {

	public static final int MODIFIER_LEAF		= 0x01;
	public static final int MODIFIER_REFERRAL	= 0x02;
	public static final int MODIFIER_INACTIVATED= 0x04;
	public static final int MODIFIER_ERROR		= 0x08;
	
	private Hashtable _iconTable = new Hashtable();
	private Hashtable _pathTable = new Hashtable();
	private RemoteImage _defaultLeafIcon;
	private RemoteImage _defaultContainerIcon;
	private RemoteImage _rootNodeIcon;
	private RemoteImage _errorIcon;
	private RemoteImage _errorMaskIcon;
	private RemoteImage _referralMaskIcon;
	private RemoteImage _inactivatedMaskIcon;

	private static final String IMAGE_PATH = "com/netscape/admin/dirserv/images";


	private static final String[] ICON_PATH = {
		"person",					"alluser16n.gif",
		"organization",				"folder.gif",
		"organizationalunit",		"ou16.gif",
		"groupofuniquenames",		"allgroup16n.gif",
		"nsmanagedroledefinition",	"mrole.gif",
		"nsfilteredroledefinition", "frole.gif",
		"nsnestedroledefinition",	"nrole.gif",
		"cossuperdefinition",		"cos16.gif",
		"nsview",					"view.gif"
	};


	public IconPool() {
		// Recopy ICON_PATH in _pathTable for fast access
		for (int i = 0; i < ICON_PATH.length; i = i+2) {
			_pathTable.put(ICON_PATH[i], ICON_PATH[i+1]);
		}
	}
	
	
	/**
	 * If objectclass is null, a default icon is used.
	 */
	public RemoteImage getIcon(LDAPAttribute objectClass, int modifiers) {
		RemoteImage result;
		
		Object key = makeKey(objectClass, modifiers);
		result = (RemoteImage)_iconTable.get(key);
		if (result == null) {
			result = makeIcon(objectClass, modifiers);
			_iconTable.put(key, result);
		}
		
//		System.out.println("objectClass =  " + objectClass);
//		System.out.println("modifiers = " + modifiers);
//		System.out.println("result = " + result);

		return result;
	}


	public RemoteImage getDefaultLeafIcon() {
		if (_defaultLeafIcon == null) {
			_defaultLeafIcon = new RemoteImage(IMAGE_PATH + "/genobject.gif"); 
		}
		return _defaultLeafIcon;
	}


	public RemoteImage getDefaultContainerIcon() {
		if (_defaultContainerIcon == null) {
			_defaultContainerIcon = new RemoteImage(IMAGE_PATH + "/folder.gif"); 
		}
		return _defaultContainerIcon;
	}
	
	
	public RemoteImage getIconForRootNode() {
		if (_rootNodeIcon == null) {
			_rootNodeIcon = new RemoteImage(IMAGE_PATH + "/directory.gif"); 
		}
		return _rootNodeIcon;
	}
	
	
	public RemoteImage getErrorIcon() {
		if (_errorIcon == null) {
			_errorIcon = new RemoteImage(IMAGE_PATH + "/error16.gif"); 
		}
		return _errorIcon;
	}
	
	
	public RemoteImage getErrorMaskIcon() {
		if (_errorMaskIcon == null) {
			_errorMaskIcon = new RemoteImage(IMAGE_PATH + "/failed.gif"); 
		}
		return _errorMaskIcon;
	}
	
	
	public RemoteImage getReferralMaskIcon() {
		if (_referralMaskIcon == null) {
			_referralMaskIcon = new RemoteImage(IMAGE_PATH + "/referral.gif"); 
		}
		return _referralMaskIcon;
	}


	public RemoteImage getInactivatedMaskIcon() {
		if (_inactivatedMaskIcon == null) {
			_inactivatedMaskIcon = new RemoteImage(IMAGE_PATH + "/inactivated.gif"); 
		}
		return _inactivatedMaskIcon;
	}
	
	private RemoteImage makeIcon(LDAPAttribute objectClass, int modifiers) {
		RemoteImage result;
		
		// Find the icon associated to the object class
		if (objectClass == null) {
			result = getDefaultContainerIcon();
		}
		else {
			String iconFile = null;
			Enumeration e = objectClass.getStringValues();
			while (e.hasMoreElements()) {
				String className = ((String)e.nextElement()).toLowerCase();
				if (className.equals("nsview")) {
					iconFile = (String)_pathTable.get(className);
					break; // do not allow view icon to be overriden
				}
				else if (iconFile == null) {
					iconFile = (String)_pathTable.get(className);
				}                
			}

			if (iconFile == null) {
				if ((modifiers & MODIFIER_LEAF) != 0) {
					result = getDefaultLeafIcon();
				}
				else {
					result = getDefaultContainerIcon();
				}
			}
			else {
				result = new RemoteImage(IMAGE_PATH + "/" + iconFile);
				// TODO : we could test result.getImageLoadStatus()
			}
		}

		// Alter this icon according the modifiers
		if ((modifiers & MODIFIER_REFERRAL) != 0) {
// TODO: improve the icon mask for referral
//			result = maskedIcon(result, getReferralMaskIcon());
			result = getReferralMaskIcon();
		}
		if ((modifiers & MODIFIER_INACTIVATED) != 0) {
			result = maskedIcon(result, getInactivatedMaskIcon());
		}
		if ((modifiers & MODIFIER_ERROR) != 0) {
			result = maskedIcon(result, getErrorMaskIcon());
		}
		
		return result;
	}
	
	
	private Object makeKey(LDAPAttribute objectClass, int modifiers) {
		// TODO: verify the performance of IconPool.makeKey()
		StringBuffer result = new StringBuffer();
		if(objectClass != null) {
			Enumeration e = objectClass.getStringValues();
			while (e.hasMoreElements()) {
				result.append(((String)e.nextElement()).toLowerCase());
			}
		}
		result.append(String.valueOf(modifiers));
		return result.toString();
	}



    /**
     *  Returns a RemoteImage corresponding to the superposition of the icon Image and the mask Image
     *
     * @param icon  the RemoteImage that we want to bar
     */
	public static RemoteImage maskedIcon(RemoteImage icon, RemoteImage mask) {
		RemoteImage fReturn;
		int TRANSPARENT = 16711165;  // The value of a transparent pixel	
				
		int h = icon.getIconHeight();
		int w = icon.getIconWidth();
			
		if (mask.getImageLoadStatus() != MediaTracker.COMPLETE) {
			return null;
		}
		Image maskImage = mask.getImage();
		
		Image scaledMaskImage = maskImage.getScaledInstance(w, h , maskImage.SCALE_SMOOTH);

		RemoteImage scaledMask = new RemoteImage(scaledMaskImage);
		if (scaledMask.getImageLoadStatus() != MediaTracker.COMPLETE) {
			return null;
		}

		int[] iconPixels = new int[w * h];
		try {			
			PixelGrabber pg = new PixelGrabber( icon.getImage(), 0, 0, w, h, iconPixels, 0, w);
			pg.grabPixels();
			
			if ((pg.status() & ImageObserver.ABORT) !=0) {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		int[] filterPixels = new int[w * h];
		try {			
			PixelGrabber pgf = new PixelGrabber( scaledMask.getImage(), 0, 0, w, h, filterPixels, 0, w);
			pgf.grabPixels();
			
			if ((pgf.status() & ImageObserver.ABORT) !=0) {
				fReturn = null;
				return fReturn;
			}
		} catch (Exception e) {
			e.printStackTrace();
			fReturn = null;
			return fReturn;
		}


		int[] newPixels = new int[w * h];

		for( int i = 0; i < h; i++) 
			for (int j = 0; j < w; j++)
				if (filterPixels[j + i*w] != TRANSPARENT) {
					newPixels[j + i*w] = filterPixels[j + i*w];
				} else {
					newPixels[j + i*w] = iconPixels[j + i*w];
				}
		Canvas component = new Canvas();
		
		Image newImage = component.getToolkit().createImage(new MemoryImageSource(w, h, ColorModel.getRGBdefault(), newPixels, 0, w));
		fReturn = new RemoteImage(newImage);

		return fReturn;
	}
}
