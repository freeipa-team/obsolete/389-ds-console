/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/


/**
 * This controller object displays LDAP entries inside a JList
 * using a given LDAPConnection.
 *
 * Each time a IBrowserNodeInfo is passed, the controller
 * sends some LDAP searches to retreive the child entries of the node and their
 * attributes. The searches are done in background threads (without
 * blocking the Event thread).
 *
 *
 */

package com.netscape.admin.dirserv.browser;

import java.util.Vector;
import java.util.Enumeration;
import java.text.Collator;
import java.net.MalformedURLException;
import java.lang.reflect.InvocationTargetException;
import java.awt.Font;
import java.awt.Component;

import javax.swing.JList;
import javax.swing.Icon;
import javax.swing.SwingUtilities;


import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import netscape.ldap.LDAPAttribute;
import netscape.ldap.LDAPEntry;
import netscape.ldap.LDAPSearchResults;
import netscape.ldap.LDAPSearchConstraints;
import netscape.ldap.LDAPControl;
import netscape.ldap.LDAPSortKey;
import netscape.ldap.LDAPUrl;
import netscape.ldap.util.DN;
import netscape.ldap.controls.LDAPSortControl;

import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.RemoteImage;

import com.netscape.admin.dirserv.DSUtil;

public class ChildrenController {	
	JList _list;	
	ChildrenListModel _listModel;
	int _displayFlags;
	LDAPConnection _ldc;
	boolean _followReferrals;
	boolean _sorted;	
	String[] _containerClasses;	
	int _maxChildren = 0;
	Vector _listeners;
	LDAPConnectionPool _connectionPool;
	IconPool _iconPool;
	LDAPSearchConstraints _searchConstraints;		
	BasicNode _parentNode;

	boolean _parentHasIndex;
    
	NodeTaskQueue _childrenQueue;
	int _queueTotalSize;

	/**
	 * Public API
	 * ==========
	 */
	public ChildrenController(JList list, 
							  LDAPConnectionPool pool,
							  IconPool iconPool) {
		_list = list;
		_listModel = new ChildrenListModel();
		_list.setModel(_listModel);

		BasicNode prototypeNode = new BasicNode("");
		prototypeNode.setIcon(new RemoteImage(
		    "com/netscape/admin/dirserv/images/person.gif"));
		_list.setPrototypeCellValue(prototypeNode);

		_iconPool = iconPool;		
		_displayFlags = BrowserController.DISPLAY_ACI_COUNT;
		_followReferrals = true;
		_sorted = false;		
		_containerClasses = new String[0];		
		_connectionPool = pool;
		_searchConstraints = null; // Will be computed on the fly
		
		_list.setCellRenderer(new ChildrenCellRenderer(this));
		_listeners = new Vector(2);
		_queueTotalSize = 0;

		_childrenQueue = new NodeTaskQueue("New child", 1);
	}	


	/**
	 * Set the connection for accessing the directory.
	 */
	public void setLDAPConnection(LDAPConnection ldc) {
		String rootNodeName;
		_ldc = ldc;
		if (_ldc != null) {
			_connectionPool.registerAuth(_ldc);
			rootNodeName = _ldc.getHost() + ":" + _ldc.getPort();
		}
		else {
			rootNodeName = "";
		}		
		startRefresh();
	}
	
    
	/**
	 * Return the connection for accessing the directory.
	 */
	public LDAPConnection getLDAPConnection() {
		return _ldc;
	}
	
	
	/**
	 * Return the JList controlled by this controller.
	 */
	public JList getList() {
		return _list;
	}
	
	
	/**
	 * Return the connection pool used by this controller.
	 * If a client class adds authentication to the connection
	 * pool, it must inform the controller by calling
	 * notifyAuthDataChanged().
	 */
	public LDAPConnectionPool getConnectionPool() {
		return  _connectionPool;
	}




	/**
	 * Return the display flags.
	 */
	public int getDisplayFlags() {
		return _displayFlags;
	}
	

	/**
	 * Set the display flags.
	 */
	public void setDisplayFlags(int flags) {
		_displayFlags = flags;		
	}
	
	/**
	  * Sets the maximum number of children to display for a node.
	  * 0 if there is no limit 
	  */
	public void setMaxChildren(int maxChildren) {
		_maxChildren = maxChildren;
	}

	/**
	  * Return the maximum number of children to display
	  */
	public int getMaxChildren() {
		return _maxChildren;
	}
	

	/**
	 * Return true if this controller follows referrals.
	 */
	public boolean getFollowReferrals() {
		return _followReferrals;
	}

	
	/**
	 * Enable/display the following of referrals.
	 * This routine starts a refresh on each referral node.
	 */
	public void setFollowReferrals(boolean yes) {
		_followReferrals = yes;
		startRefreshReferralNodes();
	}

	
	/**
	 * Return true if entries are displayed sorted.
	 */
	public boolean isSorted() {
		return _sorted;
	}


	/**
	 * Enable/disable entry sort.	 
	 */
	public void setSorted(boolean yes) {
		stopRefresh();		
		_sorted = yes;
		_searchConstraints = null; // Force the reconstruction		
	}


	/**
	 * Find the IBrowserNodeInfo associated to an index and returns
	 * the describing IBrowserNodeInfo.
	 */
	public IBrowserNodeInfo getNodeInfoFromIndex(int index) {		
		BasicNode node = (BasicNode)_listModel.getElementAt(index);
		return new ChildrenNodeInfo(node);
	}


	/**
	 * Return the array of container classes for this controller.
	 * Warning: the returned array is not cloned.
	 */
	public String[] getContainerClasses() {
		return _containerClasses;
	}


	/**
	 * Set the list of container classes and calls startRefresh().
	 * Warning: the array is not cloned.
	 */
	public void setContainerClasses(String[] containerClasses) {
		_containerClasses = containerClasses;
		startRefresh();
	}


	/**
	 * Add a BrowserEventListener to this controller.
	 */
	public void addBrowserEventListener(BrowserEventListener l) {
		_listeners.addElement(l);
	}
	
	
	/**
	 * Remove a BrowserEventListener from this controller.
	 */
	public void removeBrowserEventListener(BrowserEventListener l) {
		_listeners.removeElement(l);
	}
	

	/**
	 * Notify this controller that an entry has been added.
	 * The controller adds a new node in the JList at the end and
	 * starts refreshing this new node.  There's no check about the validity
	 * of the dn of the added entry.
	 * This routine returns the index about the new entry.
	 */
	public int notifyEntryAdded(String newEntryDn) {
		BasicNode childNode = new BasicNode(newEntryDn);
        int index = _listModel.getSize();

		_listModel.addElement(childNode);
		startRefreshNode(childNode);        
        if (!_parentHasIndex) {
            _list.ensureIndexIsVisible(index);
        }
        return index;
	}
	

	/**
	 * Notify this controller that a entry has been deleted.
	 * The controller removes the corresponding node from the
	 * JList.
	 */
	public void notifyEntryDeleted(IBrowserNodeInfo nodeInfo) {
		if ( !(nodeInfo instanceof ChildrenNodeInfo)) {
			return;
		}
		BasicNode node = ((ChildrenNodeInfo)nodeInfo).getNode();		
		stopRefreshNode(node);
		_listModel.removeElement(node);
	}
	
	
	/**
	 * Notify this controller that an entry has changed.
	 * The controller starts refreshing the corresponding node.
	 * Child nodes are not refreshed.
	 */
	public void notifyEntryChanged(IBrowserNodeInfo nodeInfo) {
		if ( !(nodeInfo instanceof ChildrenNodeInfo)) {
			return;
		}
		BasicNode node = ((ChildrenNodeInfo)nodeInfo).getNode();
		startRefreshNode(node);
		_listModel.updateElement(node);
	}


	/**
	 * Notify this controller that the entry DN has changed.
	 */
	public void notifyEntryDNChanged(IBrowserNodeInfo nodeInfo, String newDN) {
		if ( !(nodeInfo instanceof ChildrenNodeInfo)) {
			return;
		}
		BasicNode node = ((ChildrenNodeInfo)nodeInfo).getNode();
	    BasicNode newNode = new BasicNode(newDN);
	    startRefreshNode(newNode);
	    _listModel.replaceElement(node, newNode);
	}


	/**
	 * Notify this controller that the VLV index presence has changed.
	 */
	public void notifyIndexChanged(boolean hasIndex) {
		_parentHasIndex = hasIndex;
        startRefresh();
	}

	/**
	 * Notify this controller that authentication data
	 * have changed in the connection pool for the specified
	 * url.
	 * The controller starts refreshing the node which
	 * represent entries from the url.
	 */
	public void notifyAuthDataChanged(LDAPUrl url) {
		// TODO: temporary implementation
		//		we should refresh only nodes :
		//		- whose URL matches 'url'
		//		- whose errorType == ERROR_SOLVING_REFERRAL and
		//		  errorArg == url
		startRefreshReferralNodes();
	}
	
	/**
	 * Updates the panel with a new parent.  If node is null, empties the list.
	 */
	public void setBaseNodeInfo(IBrowserNodeInfo node, boolean hasIndex) {
        _parentHasIndex = hasIndex;        
		// We don't want to search for children of the
		// root node since it doesn't represent an actual
		// entry.
		if ((node == null) || node.isRootNode()) {
			_parentNode = null;
		} else {
			_parentNode = node.getNode();
		}
		startRefresh();
	}
	

	/**
	 * Refresh the whole panel
	 */
	public void startRefresh() {
		stopRefresh();

		_listModel.clear();
		
		if (_parentNode != null) {

			if (_parentHasIndex) {
				// VLV List
				try {
					_listModel = new VListModel(_parentNode, this, null);
					_list.setModel(_listModel);
				}
				catch (LDAPException ex) {
					Debug.println("ChildrenController.startRefresh " + ex);
				}
			}
			else {
				_list.setModel(_listModel = new ChildrenListModel());
				_childrenQueue.queue(new ChildrenTask(_parentNode, this));
			}
		}
	}

	/**
	 * Start refreshing the node.	 
	 */
	public void startRefresh(IBrowserNodeInfo node) {		
		startRefreshNode(node.getNode());
	}
	
	/**
	 * Shutdown the controller.
	 */
	public void shutDown() {		
		stopRefresh();		
	}	


	/**
	 * Start refreshing the node.	 
	 */
	public void startRefreshNode(BasicNode node) {		
		_childrenQueue.queue(new ChildrenTask(this, node));
	}

	/**
	 * Start refreshing the node.	 
	 */
	void startRefreshNode(BasicNode node, LDAPEntry entry) {		
		_childrenQueue.queue(new ChildrenTask(this, node, entry));
	}


	void stopRefresh() {
		_childrenQueue.cancelAll();		
	}


	/**
	 * Stop refreshing below this node.	 
	 */
	void stopRefreshNode(BasicNode node) {		
		_childrenQueue.cancelForNode(node);	
	}
	

	
	/**
	 * Call startRefreshNode() on each referral node accessible
	 * from parentNode.
	 */
	void startRefreshReferralNodes() {
		Enumeration e = _listModel.elements();
		while (e.hasMoreElements()) {
			BasicNode child = (BasicNode)e.nextElement();
			if ((child.getReferral() != null) || (child.getRemoteUrl() != null)) {
				startRefreshNode(child);
			}			
		}
	}


	
	/**
	 * Return the icon pool used by this controller.
	 */
	IconPool getIconPool() {
		return  _iconPool;
	}

	/**
	 * Return the LDAP search filter to use for searching child entries.
	 * If _showContainerOnly is true, the filter will select only the
	 * container entries. If not, the filter will select all the children.
	 */
	String getChildSearchFilter() {
		String result = "(|(objectclass=*)(objectclass=ldapsubentry))";		
		
		return result;
	}

	/**
	 * Return the LDAP connection to reading the base entry of a node.
	 */
	LDAPConnection findConnectionForLocalEntry(BasicNode node)
	throws LDAPException {	
		LDAPConnection result;
		if (node instanceof RootNode) {
			result = _ldc;
		} else if (node.getParent() != null) {
			result = findConnectionForDisplayedEntry((BasicNode)node.getParent());
		} else if ((_parentNode != null) &&
				   (node != _parentNode)) {
			result = findConnectionForDisplayedEntry(_parentNode);
		} else {
			result = _ldc;
		}
		
		return result;
	}
	

	/**
	 * Return the LDAP connection to search the displayed entry
	 * (which can be the local or remote entry).
	 */
	LDAPConnection findConnectionForDisplayedEntry(BasicNode node)
	throws LDAPException {
		LDAPConnection result;
		if (_followReferrals && (node.getRemoteUrl() != null)) {
			result = _connectionPool.getConnection(node.getRemoteUrl());
		}
		else {
			result = findConnectionForLocalEntry(node);
		}
		return result;
	}
	
	
	
	/**
	 * Release a connection returned by  
	 *		selectConnectionForChildEntries()
	 * or
	 *		selectConnectionForBaseEntry()
	 */
	void releaseLDAPConnection(LDAPConnection ldc) {
		if (ldc != _ldc) { // Thus it comes from the connection pool
			_connectionPool.releaseConnection(ldc);
		}
	}


	/**
	 *
	 */
	LDAPUrl findUrlForLocalEntry(BasicNode node, int searchDepth) {
		LDAPUrl result = null;
		if (node instanceof RootNode) {
			result = LDAPConnectionPool.makeLDAPUrl(_ldc, "");
		} else if (node.getParent() != null) {
			BasicNode parent = (BasicNode)node.getParent();
			LDAPUrl parentUrl = findUrlForDisplayedEntry(parent, searchDepth);
			result = LDAPConnectionPool.makeLDAPUrl(parentUrl, node.getDN());
		} else if (_parentNode != null && searchDepth == 1) {
			LDAPUrl parentUrl = findUrlForDisplayedEntry(_parentNode, searchDepth);
			result = LDAPConnectionPool.makeLDAPUrl(parentUrl, node.getDN());
		}
        else {
            result = LDAPConnectionPool.makeLDAPUrl(_ldc, "");
        }
		return result;
	}


	/**
	 *
	 */
	LDAPUrl findUrlForDisplayedEntry(BasicNode node, int searchDepth) {

        // (miodrag) searchDepth was added as a safty brake to this method and
        // findUrlForLocalEntry as under some circumstances (do not know how to 
        // reproduce it) there will be a endless loop between the two methods
        // calling each other.
        if (++searchDepth > 100) {
            return LDAPConnectionPool.makeLDAPUrl(_ldc, "");
        }

		LDAPUrl result;
		if (_followReferrals && (node.getRemoteUrl() != null)) {
			result = node.getRemoteUrl();
		}
		else {
			result = findUrlForLocalEntry(node, searchDepth);
		}
		return result;
	}


	/**
	 * Returns the DN to use for searching children of a given node.
	 * In most cases, it's node.getDN(). However if node has referral data
	 * and _followReferrals is true, the result is calculated from the
	 * referral resolution.
	 */
	String findBaseDNForChildEntries(BasicNode node) {
		String result;
		
		if (_followReferrals && (node.getRemoteUrl() != null)) {
			result = node.getRemoteUrl().getDN();
		}
		else {
			result = node.getDN();
		}
		return result;
	}




	boolean isDisplayedEntryRemote(BasicNode node) {
		boolean result = false;
		if (_followReferrals &&
			(node != null)) {
			if (node instanceof RootNode) {
				result = false;
			} else if (node.getRemoteUrl() != null) {
				result = true;
			} else {
				if (node.getParent() != null) {
					result = isDisplayedEntryRemote((BasicNode)node.getParent());
				} else if ((_parentNode != null) &&
						   (node != _parentNode)) {

					if (node instanceof SuffixNode) {
					    result = false;
					}
					else {
					    result = isDisplayedEntryRemote(_parentNode);
					}
				}
			}
		}					
		
		return result;
	}
		
		
	/**
	 * Returns the list of attributes for the red search.
	 */
	String[] getAttrsForRedSearch() {
		Vector v = new Vector();
		
		v.addElement("objectclass");
		v.addElement("numsubordinates");
		v.addElement("ref");
		if ((_displayFlags & BrowserController.DISPLAY_ACI_COUNT) != 0) {
			v.addElement("aci");
		}
		if ((_displayFlags & BrowserController.DISPLAY_ROLE_COUNT) != 0) {
			v.addElement("nsrole");
		}
		if ((_displayFlags & BrowserController.DISPLAY_ACTIVATION_STATE) != 0) {
			v.addElement("nsaccountlock");
		}
		
		String[] result = new String[v.size()];
		v.toArray(result);
		return result;
	}

	/**
	 * Returns the list of attributes for the black search.
	 */
	String[] getAttrsForBlackSearch() {
		return new String[] {
			"objectclass",
			"numsubordinates",
			"ref",
			"aci",
			"nsrole",
			"nsaccountlock"
		};
	}

	/**
	 * Returns the list of attributes for the black search.
	 */
	LDAPSearchConstraints getSearchConstraints() {
		if (_searchConstraints == null) {
			LDAPControl ctls[] = new LDAPControl[_sorted ? 2 : 1];
			ctls[0] = new LDAPControl(LDAPControl.MANAGEDSAIT, true, null);
			if (_sorted) {
				LDAPSortKey[] keys = new LDAPSortKey[BrowserController.SORT_ATTRIBUTES.length];
				for (int i=0; i<keys.length; i++) {
					keys[i] = new LDAPSortKey(BrowserController.SORT_ATTRIBUTES[i]);
				}
				ctls[1] = new LDAPSortControl(keys, true);
			}
			_searchConstraints = (LDAPSearchConstraints)_ldc.getSearchConstraints().clone();
			_searchConstraints.setMaxResults(_maxChildren);
			_searchConstraints.setServerControls(ctls); // Return referral entries
		}
		return _searchConstraints;
	}


	/**
	 * Callbacks invoked by task classes
	 * =================================
	 *
	 * The routines below are invoked by the task classes; they
	 * update the nodes and the tree model. 
	 *
	 * To ensure the consistency of the tree model, these routines
	 * are not invoked directly by the task classes: they are
	 * invoked using SwingUtilities.invokeAndWait() (each of the
	 * methods XXX() below has a matching wrapper invokeXXX()).
	 * 
	 */


	/**
	 * Invoked when the refresh task has finished the red operation:
	 * it has read the attributes of the base entry ; the result of the
	 * operation is:
	 *		- an LDAPEntry if successful
	 *		- an Exception if failed
	 */

	private void childrenTaskDidProgress(ChildrenTask task, int oldState, int newState) {
	    BasicNode node = task.getNode();
		boolean nodeChanged = false;	   		
		
		// Manage events
		if (oldState == ChildrenTask.QUEUED) {
			checkUpdateEvent(true);
		}
		if (task.isInFinalState()) {
			checkUpdateEvent(false);
		}
		
		if (newState == ChildrenTask.FAILED) {
			if (oldState == ChildrenTask.SOLVING_REFERRAL) {		
				node.setRemoteUrl(task.getRemoteUrl());
				if (task.getRemoteEntry() != null) {
					/* This is the case when there are multiple hops in the referral
					   and so we have a remote referral entry but not the entry that it
					   points to */
					updateNodeRendering(node, task.getRemoteEntry());				
				}
				node.setError(new BasicNode.Error(oldState, task.getException(), task.getExceptionArg()));
				nodeChanged = updateNodeRendering(node, task.getDisplayedEntry());
			}
            else if (oldState == ChildrenTask.SEARCHING_CHILDREN) {
                addErrorNode(task, oldState);
            }
		} else if (newState == ChildrenTask.CANCELLED) {	
			/* Clear the list... */			
			_listModel.clear();
		}
		else {			
			if (oldState == ChildrenTask.READING_LOCAL_ENTRY) {
				/* The task is going to try to solve the referral if there's one.  
				   If succeeds we will update the remote url.  Set it to null for 
				   the case when there was a referral and it has been deleted */
				node.setRemoteUrl(null); 
				LDAPEntry localEntry = task.getLocalEntry();				
				if (localEntry == null) {                    
					_listModel.removeElement(task.getNode()); // entry deleted
				}
				else { 
					nodeChanged = updateNodeRendering(node, localEntry);
				}
			}
			else if (oldState == ChildrenTask.SOLVING_REFERRAL) {				
				node.setRemoteUrl(task.getRemoteUrl());
				updateNodeRendering(node, task.getRemoteEntry());
				nodeChanged = true;
			}		
			else if (oldState == ChildrenTask.SEARCHING_CHILDREN) {
				updateChildNodes(task);
			}

			if (newState == ChildrenTask.FINISHED) {
				if ((node.getError() != null) &&
					(node != _parentNode)) {
					node.setError(null);
					nodeChanged = updateNodeRendering(node, task.getDisplayedEntry());
				}
			}
		}
		if (nodeChanged) {
			_listModel.updateElement(task.getNode());		
		}
	}



	void invokeChildrenTaskDidProgress(final ChildrenTask task, final int oldState, final int newState)
	throws InterruptedException {
	
		Runnable r = new Runnable() {
			ChildrenTask _task = task;
			int _oldState = oldState;
			int _newState = newState;
			public void run() {
				try {					
					childrenTaskDidProgress(_task, _oldState, _newState);
				}
				catch(Exception x) {
					x.printStackTrace();
				}
			}
		};
		swingInvoke(r);						   
	}
	
	

	/**
	 * Core routines shared by the callbacks above
	 * ===========================================
	 */
	 

	private void updateChildNodes(ChildrenTask task) {
		BasicNode parent = task.getNode();
		
		// Walk through the entries
		Enumeration e = task.getChildEntries().elements();
        //Vector newEntries = new Vector(task.getChildEntries().size());
		while (e.hasMoreElements()) {
			LDAPEntry entry = (LDAPEntry) e.nextElement();
			BasicNode child;
						
			child = new BasicNode(entry.getDN());
			_listModel.addElement(child);
			updateNodeRendering(child, entry);			

			// Propagate the refresh
			// Note: logically we should unconditionaly call:
			//	startRefreshNode(child, false, true);
			//
			// However doing that saturates _redQueue
			// with many nodes. And, by design, ChildrenTask 
			// won't do anything on a node if:
			//		- this node has no subordinates
			//		- *and* this node has no referral data
			// So we test these conditions here and 
			// skip the call to startRefreshNode() if
			// possible.
			if (child.getReferral() != null) {
				startRefreshNode(child, entry);
			}
            
            //newEntries.add(child);
		}
        
        //_listModel.addElements(newEntries);
	}

    /**
     * Add error node if a limit exceeded error was received when
     * listing children. This is an indication that the list was
     * truncated and that a browsing index is required.
     */
	void addErrorNode(ChildrenTask task, int oldState) {
        Exception ex = task.getException();
        if (ex == null || !(ex instanceof LDAPException)) {
            return;
        }

        LDAPException lex = (LDAPException) task.getException();
        int rc = lex.getLDAPResultCode();
        String dspName = null;

        if (rc == lex.NO_SUCH_OBJECT) {
            return;
        }

        boolean limitError = rc == lex.ADMIN_LIMIT_EXCEEDED ||                             
                             rc == lex.TIME_LIMIT_EXCEEDED  ||
                             rc == lex.SIZE_LIMIT_EXCEEDED;
        
        if (rc == lex.ADMIN_LIMIT_EXCEEDED) {
            dspName = DSUtil._resource.getString("browser", "admin-limit-err");
        }
        else if (rc == lex.TIME_LIMIT_EXCEEDED) {
            dspName = DSUtil._resource.getString("browser", "time-limit-err");
        }
        else if (rc == lex.SIZE_LIMIT_EXCEEDED) {
            dspName = DSUtil._resource.getString("browser", "size-limit-err");
        }        
        else {
            dspName = lex.errorCodeToString();
        }

        if (limitError && !_parentHasIndex) {
            dspName += " " + DSUtil._resource.getString("browser", "no-index-err");
        }

		BasicNode node = new BasicNode("ERROR-NODE");
		node.setIcon(_iconPool.getErrorIcon());            
        node.setDisplayName(dspName);
        node.setError(new BasicNode.Error(oldState, task.getException(), task.getExceptionArg()));

		_listModel.addElement(node);
	}

	/**
	 * Create a list node from an LDAP entry
	 */
	BasicNode createNodeFromEntry(LDAPEntry entry) {
		BasicNode node = null;
		if (entry == null) {
			node = new BasicNode("BOGUS_NODE"); // VListModel bogus node
		}
		else {
			node = new BasicNode(entry.getDN());
		}
		updateNodeRendering(node, entry);
		// Propagate the refresh
		// Note: logically we should unconditionaly call:
		//	startRefreshNode(child, false, true);
		//
		// However doing that saturates _redQueue
		// with many nodes. And, by design, ChildrenTask 
		// won't do anything on a node if:
		//		- this node has no subordinates
		//		- *and* this node has no referral data
		// So we test these conditions here and 
		// skip the call to startRefreshNode() if
		// possible.
		if (node.getReferral() != null) {
			startRefreshNode(node, entry);
		}        
		return node;
	}


	/**
	 * Recompute the rendering props of a node (text, style, icon) according:
	 *		- the state of this node
	 *		- the LDAPEntry displayed by this node
	 */
	private boolean updateNodeRendering(BasicNode node, LDAPEntry entry) {		
		if (entry != null) {
			// Get the numsubordinates
			node.setNumSubOrdinates(getNumSubOrdinates(entry));
			node.setReferral(BrowserController.getReferral(entry));
		}			
		// Get the aci count
		int aciCount;
		
		if (((_displayFlags & BrowserController.DISPLAY_ACI_COUNT) != 0) && (entry != null)) {
			LDAPAttribute aciAttr = entry.getAttribute("aci");
			if (aciAttr != null) {
				aciCount = aciAttr.size();
			}
			else {
				aciCount = 0;
			}
		}
		else {
			aciCount = 0;
		}
			
		// Get the role count
		int roleCount;
		if (((_displayFlags & BrowserController.DISPLAY_ROLE_COUNT) != 0) && (entry != null)) {
			LDAPAttribute roleAttr = entry.getAttribute("nsrole");
			if (roleAttr != null) {
				roleCount = roleAttr.size();
			}
			else {
				roleCount = 0;
			}
		}
		else {
			roleCount = 0;
		}
		

		// Get the nsaccountlock
		boolean nsaccountlock;
		if (((_displayFlags & BrowserController.DISPLAY_ACTIVATION_STATE) != 0) && (entry != null)) {
			LDAPAttribute lockAttr = entry.getAttribute("nsaccountlock");
			if (lockAttr != null) {
				String[] values = lockAttr.getStringValueArray();
				if (values.length >= 1) {				    
					nsaccountlock = values[0].equalsIgnoreCase("true");
					if (nsaccountlock) {
						/* Check if is a role... in this case nsaccountlock
						   makes no sense */						
						LDAPAttribute objectclassAttr = entry.getAttribute("objectclass");
						if (objectclassAttr != null) {
							Enumeration e = objectclassAttr.getStringValues();
							String value;
							boolean isRoleDefinition=false;
							boolean isLDAPSubentry=false;
							while (e.hasMoreElements() && 
								   (!isRoleDefinition || !isLDAPSubentry)) {
								value = (String)e.nextElement();								
								if (value.equalsIgnoreCase("nsroledefinition")) {
									isRoleDefinition=true;
								} else if (value.equalsIgnoreCase("ldapsubentry")) {
									isLDAPSubentry = true;
								}								
							}							
							/* It's a role -> we don't know about the state of the role */
							if (isRoleDefinition && isLDAPSubentry) {
								nsaccountlock = false;
							}
						}
					}					
				}
				else {
					nsaccountlock = false;
				}
			}
			else {
				nsaccountlock = false;
			}
		}
		else {
			nsaccountlock = false;
		}


		// Select the icon according the objectclass,...
		int modifiers = 0;
		if (node.isLeaf() && (node.getNumSubOrdinates() <= 0)) {
			modifiers |= _iconPool.MODIFIER_LEAF;
		}
		if (node.getReferral() != null) {
			modifiers |= _iconPool.MODIFIER_REFERRAL;
		}
		if (node.getError() != null) {
			modifiers |= _iconPool.MODIFIER_ERROR;
		}
		if (nsaccountlock) {
			modifiers |= _iconPool.MODIFIER_INACTIVATED;			
		}
		LDAPAttribute objectClass = null;
		if (entry != null) {
			objectClass = entry.getAttribute("objectclass");
		}
		Icon newIcon = _iconPool.getIcon(objectClass, modifiers);		

		// Contruct the icon text according the dn, the aci count...
		StringBuffer sb2 = new StringBuffer();
		if (aciCount >= 1) {
			sb2.append(String.valueOf(aciCount));
			if (aciCount == 1) {
				sb2.append(" aci");
			}
			else {
				sb2.append(" acis");
			}
		}
		if (roleCount >= 1) {
			if (sb2.length() >= 1) sb2.append(", ");
			sb2.append(String.valueOf(roleCount));
			if (roleCount == 1) {
				sb2.append(" role");
			}
			else {
				sb2.append(" roles");
			}
		}
		StringBuffer sb1 = new StringBuffer();
		String rdn;
		if (_followReferrals && (node.getRemoteUrl() != null)) {
			rdn = node.getRemoteRDN();
		}
		else {
			rdn = node.getRDN();
		}
		sb1.append(rdn);
		if (sb2.length() >= 1) {
			sb1.append("  (");
			sb1.append(sb2);
			sb1.append(")");
		}
		String newDisplayName = sb1.toString();
		
		// Select the font style according referral
		int newStyle = 0;
		if (isDisplayedEntryRemote(node)) {
			newStyle |= Font.ITALIC;
		}
		if (node instanceof SuffixNode) {
			newStyle |= Font.BOLD;
		}
		
		// Determine if the rendering needs to be updated
		boolean changed = (
			(node.getIcon() != newIcon) ||
		    (node.getDisplayName() != newDisplayName) ||
			(node.getFontStyle() != newStyle)
		);
		if (changed) {
			node.setIcon(newIcon);				
			node.setDisplayName(newDisplayName);
			node.setFontStyle(newStyle);
		}


		return changed;
	}

	
	/**
	 * Find a child node matching a given DN.
	 * Choose the most efficient way according the state of the _sorted property.
	 *
	 * result >= 0		result is the index of the node matching childDn
	 * result < 0		-(result + 1) is the index at which the new node must be inserted
	 */
	int findChildNode(BasicNode parent, String childDn) {
		int result;
		if (_sorted) {
			result = findSortedChildNode(parent, childDn, 0, parent.getChildCount()-1);
		}
		else {
			result = findUnSortedChildNode(parent, childDn);
		}
		return result;
	}


	/**
	 */
	int findSortedChildNode(BasicNode parent, String childDn, int start, int end) {
		int result;
		// INVARIANT : start-1 < childDn < end+1
		if (start > end) {
			if (start != end+1) throw new IllegalStateException("Bug in findSortedChildNode");
			result = -(start+1);
		}
		else {
			int cr = compareDnToChildNode(childDn, parent, start);
			if (cr < 0) { // start-1 < childDn < start
				result = -(start + 1);
			}
			else if (cr == 0) { // childDn == start
				result = start;
			}
			else if (start == end) { // end < childDn < end+1
				result = -(end+1 + 1);
			}
			else { // childDn must be compared to the 'end' child
				cr = compareDnToChildNode(childDn, parent, end);
				if (cr > 0) { // end < childDn < end+1
					result = -(end+1 + 1);
				}
				else if (cr == 0) { // childDn == end;
					result = end;
				}
				else { // start < childDn < end
					   // childDn must be compared with the median child
					int median = (start + end) / 2;
					cr = compareDnToChildNode(childDn, parent, median);
					if (cr < 0) { // start < childDn < median
						result = findSortedChildNode(parent, childDn, start+1, median-1);
					}
					else if (cr == 0) { // childDn == median
						result = median;
					}
					else { // median < childDn < end
						result = findSortedChildNode(parent, childDn, median+1, end-1);
					}
				}
			}
		}
		return result;
	}


	/**
	 * We assume that parent.getChildCount() >= 1
	 */
	int findUnSortedChildNode(BasicNode parent, String childDn) {
		int childCount = parent.getChildCount();
		int i = 0, cr = -1;
		while ((i < childCount) && 
		       !childDn.equals(((BasicNode)parent.getChildAt(i)).getDN())) {
			i++;
		}
		if (i >= childCount) { // Not found
			i = -(childCount + 1);
		}
		
		return i;
	}
	
	
	/**
	 * Follow the String.compare() result convention.
	 * result < 0	-> dn < parent.getChild[childIndex]
	 * result == 0 	-> dn == parent.getChild[childIndex]
	 * result > 0	-> dn > parent.getChild[childIndex]
	 */
	int compareDnToChildNode(String dn, BasicNode parent, int childIndex) {
		BasicNode node = (BasicNode)parent.getChildAt(childIndex);
		return Collator.getInstance().compare(dn, node.getDN());
	}
	
	

	/**
	 * BrowserEvent management
	 * =======================
	 *
	 * This method computes the total size of the queues,
	 * compares this value with the last computed and 
	 * decides if an update event should be fired or not.
	 *
	 * It's invoked by task classes through SwingUtilities.invokeLater()
	 * (see the wrapper below). That means the event handling routine
	 * (processBrowserEvent) is executed in the event thread.
	 */
	private void checkUpdateEvent(boolean taskIsStarting) {
		int newSize = _childrenQueue.size();
		if (!taskIsStarting) {
			newSize = newSize - 1;
		}
		if (newSize != _queueTotalSize) {
			if ((_queueTotalSize == 0) && (newSize >= 1)) {
				fireEvent(BrowserEvent.UPDATE_START);
			}
			else if ((_queueTotalSize >= 1) && (newSize == 0)) {
				fireEvent(BrowserEvent.UPDATE_END);
			}
			_queueTotalSize = newSize;
		}
	}
	
	
	private void fireEvent(int id) {
		BrowserEvent event = new BrowserEvent(this, id);
		Enumeration e = _listeners.elements();
		while (e.hasMoreElements()) {
			BrowserEventListener l = (BrowserEventListener)e.nextElement();
			l.processBrowserEvent(event);
		}
	}
	

	/**
	 * Miscellaneous private routines
	 * ==============================
	 */
	 
	

	/**
	 * Return true if x is an LDAPException with resultCode = NO_SUCH_OBJECT.
	 */
	private boolean isNoSuchObjectLDAPException(Object x) {
		boolean result;
		if (x instanceof LDAPException) {
			LDAPException lx = (LDAPException)x;
			result = (lx.getLDAPResultCode() == lx.NO_SUCH_OBJECT);
		}
		else {
			result = false;
		}
		return result;
	}



	/**
	 * If numsubordinates is not available, returns -1.
	 */
	public static int getNumSubOrdinates(LDAPEntry entry) {
		int result;
		
		LDAPAttribute attr = entry.getAttribute("numsubordinates");
		if (attr == null) {
			result = -1;
		}
		else {
			try {
				result = Integer.parseInt(attr.getStringValueArray()[0]);
			}
			catch(NumberFormatException x) {
				result = 0;
			}
		}
		
		return result;
	}

	/**
	 * Vector utilities
	 */
	static int[] intArrayFromVector(Vector v) {
		int[] result = new int[v.size()];
		for (int i = 0; i < result.length; i++) {
			result[i] = ((Integer)v.elementAt(i)).intValue();
		}
		return result;
	}
	
	static LDAPEntry[] entryArrayFromVector(Vector v) {
		LDAPEntry[] result = new LDAPEntry[v.size()];
		v.toArray(result);
		return result;
	}
	
	static BasicNode[] nodeArrayFromVector(Vector v) {
		BasicNode[] result = new BasicNode[v.size()];
		v.toArray(result);
		return result;
	}
	
	
	
	/**
	 * For debugging purpose: allows to switch easily
	 * between invokeLater() and invokeAndWait() for
	 * experimentation...
	 */
	static void swingInvoke(Runnable r)
	throws InterruptedException {
		try {
			SwingUtilities.invokeAndWait(r);
		}
		catch(InterruptedException x) {
			throw x;
		}
		catch(InvocationTargetException x) {
			// Probably a very big trouble...
			x.printStackTrace();
		}
	}



	class ChildrenNodeInfo implements IBrowserNodeInfo {

		BasicNode _node;
		LDAPUrl _url;
		boolean _isRemote;
		boolean _isSuffix;
		boolean _isRootNode;
		String[] _referral;
		int _numSubOrdinates;
		int _errorType;
		Exception _errorException;
		Object _errorArg;


		public ChildrenNodeInfo(BasicNode node) {
			_node = node;			
			_url = findUrlForDisplayedEntry(node, 0);
			
			_isRootNode = node instanceof RootNode;
			_isRemote = isDisplayedEntryRemote(node);
			_isSuffix = node instanceof SuffixNode;
			_referral = node.getReferral();
			_numSubOrdinates = node.getNumSubOrdinates();
			if (node.getError() != null) {
				BasicNode.Error error = node.getError();
				switch(error.getType()) {
					case ChildrenTask.READING_LOCAL_ENTRY:
						_errorType = ERROR_READING_ENTRY;
						break;
					case ChildrenTask.SOLVING_REFERRAL:
						_errorType = ERROR_SOLVING_REFERRAL;
						break;					
					case ChildrenTask.SEARCHING_CHILDREN:
						_errorType = ERROR_SEARCHING_CHILDREN;
						break;
						
				}
				_errorException = error.getException();
				_errorArg = error.getArg();
			}
		}

		public BasicNode getNode() {
			return _node;
		}
		
		
		public LDAPUrl getURL() {
			return _url;
		}
		
		public boolean isRootNode() {
			return _isRootNode;
		}

		public boolean isSuffix() {
			return _isSuffix;
		}

		public boolean isRemote() {
			return _isRemote;
		}

		public String[] getReferral() {
			return _referral;
		}

		public int getNumSubOrdinates() {
			return _numSubOrdinates;
		}
		
		public int getErrorType() {
			return _errorType;
		}

		public Exception getErrorException() {
			return _errorException;
		}

		public Object getErrorArg() {
			return _errorArg;
		}

		public javax.swing.tree.TreePath getTreePath() {
			return null;
		}
		
		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append(getURL());
			if (getReferral() != null) {
				sb.append(" -> ");
				sb.append(getReferral());
			}
			return sb.toString();
		}		
	}	
}
