/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.browser;

import java.awt.Font;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTree;
import javax.swing.Icon;
//import javax.swing.tree.DefaultTreeCellRenderer;
import com.netscape.management.client.ResourceCellRenderer;



class BrowserCellRenderer extends ResourceCellRenderer {

	BrowserController _controller; // Do we need it ?
	Font _defaultFont;
	Font _italicFont;
	Font _boldFont;
	Font _italicBoldFont;
	Font _currentFont;
	Icon _currentIcon;
	
	public BrowserCellRenderer(BrowserController controller) {
		_controller = controller;
	}

	public Component getTreeCellRendererComponent(
    	JTree tree,              
    	Object value,
    	boolean isSelected,
		boolean isExpanded,
		boolean isLeaf,
		int row,
    	boolean cellHasFocus)
	{
		BasicNode node = (BasicNode)value;
		
		// Set the icon and text
		Icon newIcon = node.getIcon();
		if (newIcon != _currentIcon) {
			_currentIcon = newIcon;
			setIcon(_currentIcon);
		}
		setText(node.getDisplayName());
		
		// Set the font
		if (_defaultFont == null) {
			_defaultFont = _controller.getTree().getFont();
			_italicFont = _defaultFont.deriveFont(Font.ITALIC);
			_boldFont = _defaultFont.deriveFont(Font.BOLD);
			_italicBoldFont = _defaultFont.deriveFont(Font.ITALIC|Font.BOLD);
			_currentFont = _defaultFont;
		}
		int style = node.getFontStyle();
		Font newFont;
		if ((style & Font.ITALIC & Font.BOLD) != 0) {
			newFont = _italicBoldFont;
		}
		else if ((style & Font.ITALIC) != 0) {
			newFont = _italicFont;
		}
		else if ((style & Font.BOLD) != 0) {
			newFont = _boldFont;
		}
		else {
			newFont = _defaultFont;
		}
		if (newFont != _currentFont) {
			_currentFont = newFont;
			setFont(_currentFont);
		}
		
    	return super.getTreeCellRendererComponent(tree, node, isSelected,
                                            	  isExpanded, isLeaf,
                                            	  row, cellHasFocus);
	}
	
	
	public static int calculateRowHeight(BrowserController controller) {
		return 16;
	}
}
