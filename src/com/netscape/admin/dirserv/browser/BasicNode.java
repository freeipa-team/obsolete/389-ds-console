/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.browser;

import java.util.Vector;
import java.awt.Font;
import javax.swing.Icon;
import javax.swing.tree.DefaultMutableTreeNode;

import com.netscape.admin.dirserv.DSUtil;

import netscape.ldap.util.DN;
import netscape.ldap.LDAPUrl;

class BasicNode extends DefaultMutableTreeNode {

	protected String _localDn;
	protected String _localRdn;
	protected LDAPUrl _remoteUrl;
	protected String  _remoteRdn;
	
	protected boolean _isLeaf;
	protected boolean _refreshNeededOnExpansion = true;
	protected boolean _obsolete;
	protected Error _error;

	protected String[] _referral;
	protected int _numSubOrdinates;

	protected String _displayName;
	protected Icon _icon;
	protected int _fontStyle;


	public BasicNode(String dn) {
		_localDn = dn;
		_localRdn = extractRDN(_localDn);
//		_remoteUrl = null;
//		_remoteRdn = null;

		_isLeaf = true;
		_refreshNeededOnExpansion = true;
//		_obsolete = false;

//		_referralData = null;
		_numSubOrdinates = -1;
		
//		_errorType = 0;
//		_errorException = null;
//		_errorArg = null;
		
		_displayName = ""; 
//		_icon = null;
//		_fontStyle = 0;
	}
	
	
	/**
	 * Core properties
	 */
	public String getDN() {
		return _localDn;
	}
	
	public String getRDN() {
		return _localRdn;
	}
	
	public LDAPUrl getRemoteUrl() {
		return _remoteUrl;
	}
	
	public void setRemoteUrl(LDAPUrl url) {
		_remoteUrl = url;
		if (_remoteUrl == null) {
			_remoteRdn = null;
		}
		else {
			_remoteRdn = extractRDN(_remoteUrl.getDN());
		}
	}
	
	public String getRemoteRDN() {
		return _remoteRdn;
	}


	/**
	 * Node management
	 */
	public void setLeaf(boolean yes) {
		_isLeaf = yes;
	}
	
	public boolean isLeaf() {
		return _isLeaf;
	}

	public boolean isRefreshNeededOnExpansion() {
		return _refreshNeededOnExpansion;
	}

	public void setRefreshNeededOnExpansion(boolean yes) {
		_refreshNeededOnExpansion = yes;
	}
	
	public boolean isObsolete() {
		return _obsolete;
	}
	
	public void setObsolete(boolean yes) {
		_obsolete = yes;
	}

	public Error getError() {
		return _error;
	}
	
	public void setError(Error error) {
		_error = error;
	}


	/**
	 * Cached LDAP attributes
	 */

	public int getNumSubOrdinates() {
		return _numSubOrdinates;
	}

	public void setNumSubOrdinates(int number) {
		_numSubOrdinates = number;
	}
	
	public String[] getReferral() {
		return _referral;
	}
	
	public void setReferral(String[] referral) {
		_referral = referral;
	}
	
	
	/**
	 * Rendering
	 */
	public String toString() {
		return getDisplayName();
	}
	
	public String getDisplayName() {
		return _displayName;
	}
	
	public void setDisplayName(String name) {
		_displayName = name;
	}
	
	public Icon getIcon() {
		return _icon;
	}
	
	
	public void setIcon(Icon icon) {
		_icon = icon;
	}
	
	public int getFontStyle() {
		return _fontStyle;
	}
	
	
	public void setFontStyle(int style) {
		_fontStyle = style;
	}


	/**
	 * Utilities
	 */
	public boolean dnEquals(String dn) {
		return _localDn.equalsIgnoreCase(dn);
	}
	
	public static String extractRDN(String dn) {
		String result;
		if (dn == null) {
			result = null;
		}
		else {
			DN dnObj = new DN(dn);
			if (dnObj.countRDNs() >= 1) {
				result = DSUtil.unEscapeRDNVal(dnObj.explodeDN(true)[0]);
			}
			else {
				result = "";
			}
		}
		return result;
	}

	/**
	 * Error record.
	 * We group all the error variables in one class
	 * to decrease the number of variables in BasicNode.
	 */
	static class Error {
		int _type;
		Exception _exception;
		Object _arg;
		
		public Error(int type, Exception x, Object arg) {
			_type = type;
			_exception = x;
			_arg = arg;
		}
		
		public int getType() {
			return _type;
		}
		
		public Exception getException() {
			return _exception;
		}
		
		public Object getArg() {
			return _arg;
		}
	}
}
