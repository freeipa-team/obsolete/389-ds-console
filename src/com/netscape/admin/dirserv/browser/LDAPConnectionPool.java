/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.browser;

import java.util.Enumeration;
import java.util.Hashtable;

import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import netscape.ldap.LDAPUrl;
import netscape.ldap.LDAPSocketFactory;

import com.netscape.management.client.util.UtilConsoleGlobals;
import com.netscape.management.client.util.Debug;



/**
 * An LDAPConnectionPool is a pool of LDAPConnection.
 * <BR><BR>
 * When a client class needs to access an LDAPUrl, it simply passes
 * this URL to getConnection() and gets an LDAPConnection back.
 * When the client has finished with this LDAPConnection, it *must*
 * pass it releaseConnection() which will take care of its disconnection
 * or caching.
 * <BR><BR>
 * LDAPConnectionPool maintains a pool of authentications. This pool
 * is populated using registerAuth(). When getConnection() has created
 * a new connection for accessing a host:port, it looks in the authentication 
 * pool if any authentication is available for this host:port and, if yes,
 * tries to bind the connection. If no authentication is available, the
 * returned connection is simply connected (ie anonymous bind).
 * <BR><BR>
 * LDAPConnectionPool shares connections and maintains a usage counter
 * for each connection: two calls to getConnection() withe the same URL 
 * will return the same connection. Two calls to releaseConnection() will 
 * be needed to make the connection 'potentially disconnectable'.
 * <BR><BR>
 * releaseConnection() does not disconnect systematically a connection
 * whose usage counter is null. It keeps it connected a while (TODO:
 * to be implemented).
 * <BR><BR>
 * TODO: synchronization is a bit simplistic...
 */
public class LDAPConnectionPool {

	Hashtable _authTable = new Hashtable(); // Populated with AuthRecord
	Hashtable _connectionTable = new Hashtable(); // Populated with ConnectionRecord
    String _defaultBindDN;
    String _defaultBindPWD;


	/**
	 * Returns an LDAPConnection for accessing the specified url.
	 * If no connection are available for the protocol/host/port
	 * of the URL, getConnection() makes a new one and call connect().
	 * If authentication data available for this protocol/host/port, 
	 * getConnection() call bind() on the new connection.
	 * If connect() or bind() failed, getConnection() forward the
	 * LDAPException.
	 * When getConnection() succeeds, the returned connection must
	 * be passed to releaseConnection() after use.
	 */
	public LDAPConnection getConnection(LDAPUrl ldapUrl)
	throws LDAPException {
		String key = makeKeyFromLDAPUrl(ldapUrl);
		ConnectionRecord cr;
		
		synchronized(this) {
			cr = (ConnectionRecord)_connectionTable.get(key);
			if (cr == null) {
				cr = new ConnectionRecord();
				cr.ldc = null;
				cr.counter = 1;
				cr.disconnectAfterUse = false;
				_connectionTable.put(key, cr);
			}
			else {
				cr.counter++;
			}
		}

		synchronized(cr) {
			try {
				if (cr.ldc == null) {
					cr.ldc = createLDAPConnection(ldapUrl, (AuthRecord)_authTable.get(key));
				}
			}
			catch(LDAPException x) {
				synchronized (this) {
					cr.counter--;
					if (cr.counter == 0) {
						_connectionTable.remove(key);
					}
				}
				throw x;
			}
		}
		
		
		return cr.ldc;
	}


	/**
	 * Release an LDAPConnection created by getConnection().
	 * The connection should be considered as virtually disconnected
	 * and not be used anymore.
	 */
	public synchronized void releaseConnection(LDAPConnection ldc) {
	
		String targetKey = null;
		ConnectionRecord targetRecord = null;
		synchronized(this) {
			Enumeration e = _connectionTable.keys();
			while (e.hasMoreElements() && (targetKey == null)) {
				String key = (String)e.nextElement();
				ConnectionRecord cr = (ConnectionRecord)_connectionTable.get(key);
				if (cr.ldc == ldc) {
					targetKey = key;
					targetRecord = cr;
				}
			}
		}
		
		if (targetRecord == null) { // ldc is not in _connectionTable -> bug
			throw new IllegalArgumentException("Invalid LDAP connection");
		}
		else {
			synchronized(targetRecord) {
				targetRecord.counter--;
				if ((targetRecord.counter == 0) && targetRecord.disconnectAfterUse) {
					disconnectAndRemove(targetRecord);
				}
			}
		}
	}
	
	
	/**
	 * Disconnect the connections which are not being used.
	 * Connections being used will be disconnected as soon
	 * as they are released.
	 */
	public synchronized void flush() {
		Enumeration e = _connectionTable.elements();
		while (e.hasMoreElements()) {
			ConnectionRecord cr = (ConnectionRecord)e.nextElement();
			if (cr.counter <= 0) {
				disconnectAndRemove(cr);
			}
			else {
				cr.disconnectAfterUse = true;
			}
		}
	}



    /**
     * Auth to be used by default. It does not make sense to use the anonymous
     * auth as the default one.
     */
    public void registerDefaultAuth(String dn, String pw) {
        _defaultBindDN = dn;
        _defaultBindPWD = pw;
    }
    
	/**
	 * Register authentication data.
	 * If authentication data are already available for the protocol/host/port
	 * specified in the LDAPURl, they are replaced by the new data.
	 * If true is passed as 'connect' parameter, registerAuth() creates the
	 * connection and attemps to connect() and bind() . If connect() or bind() 
	 * fail, registerAuth() forwards the LDAPException and does not register 
	 * the authentication data.
	 */
	public void registerAuth(LDAPUrl ldapUrl, String dn, String pw, boolean connect) 
	throws LDAPException {
		String key = makeKeyFromLDAPUrl(ldapUrl);
		AuthRecord ar;
		ar = new AuthRecord();
		ar.ldapUrl  = ldapUrl;
		ar.dn       = dn;
		ar.password = pw;

		if (connect) {
			LDAPConnection ldc = createLDAPConnection(ldapUrl, ar);
			ldc.disconnect();
		}

		synchronized(this) {
			_authTable.put(key, ar);
			ConnectionRecord cr = (ConnectionRecord)_connectionTable.get(key);
			if (cr != null) {
				if (cr.counter <= 0) {
					disconnectAndRemove(cr);
				}
				else {
					cr.disconnectAfterUse = true;
				}
			}
		}

	}
	
	
	/**
	 * Register authentication data from an existing connection.
	 * This routine recreates the LDAP URL corresponding to 
	 * the connection and passes it to registerAuth(LDAPURL).
	 */
	public void registerAuth(LDAPConnection ldc) {
		LDAPUrl url = makeLDAPUrl(
			ldc.getHost(),
			ldc.getPort(),
			"",
			(ldc.getSocketFactory() != null)
		);
		try {
			registerAuth(url, ldc.getAuthenticationDN(), ldc.getAuthenticationPassword(), false);
		}
		catch(LDAPException x) {
			throw new IllegalStateException("Bug");
		}
	}
	
	
	/**
	 * Get authentication DN registered for this url.
	 */
	public synchronized String getAuthDN(LDAPUrl ldapUrl) {
		String result;
		String key = makeKeyFromLDAPUrl(ldapUrl);
		AuthRecord ar = (AuthRecord)_authTable.get(key);
		if (ar == null) {
			result = _defaultBindDN; //null;
		}
		else {
			result = ar.dn;
		}
		return result;
	}
	
	
	/**
	 * Get authentication password registered for this url.
	 */
	public synchronized String getAuthPassword(LDAPUrl ldapUrl) {
		String result;
		String key = makeKeyFromLDAPUrl(ldapUrl);
		AuthRecord ar = (AuthRecord)_authTable.get(key);
		if (ar == null) {
			result = _defaultBindPWD; //null;
		}
		else {
			result = ar.password;
		}
		return result;
	}


	/**
	 * UTILITIES
	 */


	/**
	 * Check if an LDAP connection is secure or not.
	 */
	static boolean isSecureConnection(LDAPConnection ldc) {
		return (ldc.getSocketFactory() != null);
	}


	/**
	 * Disconnect the connection associated to a record
	 * and remove the record from _connectionTable.
	 */
	private void disconnectAndRemove(ConnectionRecord cr) {
		String key = makeKeyFromRecord(cr);
		_connectionTable.remove(key);
		try {
			cr.ldc.disconnect();
		}
		catch(LDAPException x) {
			// Bizarre. However it's not really a problem here.
		}
	}
	
	
	/**
	 * Make the key string for an LDAP URL
	 */
	private static String makeKeyFromLDAPUrl(LDAPUrl url) {
		String protocol = isSecureLDAPUrl(url) ? "LDAPS" : "LDAP"; 
		return protocol + ":" + url.getHost() + ":" + url.getPort();
	}
	
	
	/**
	 * Make the key string for an connection record.
	 */
	private static String makeKeyFromRecord(ConnectionRecord rec) {
		String protocol = isSecureConnection(rec.ldc) ? "LDAPS" : "LDAP"; 
		return protocol + ":" + rec.ldc.getHost() + ":" + rec.ldc.getPort();
	}


	/**
	 * Create an LDAP connection.
	 * Dependency: we rely UtilConsoleGlobals.getLDAPSSLSocketFactory()
	 * for creating LDAPS connection.
	 */
	private LDAPConnection createLDAPConnection(LDAPUrl ldapUrl, AuthRecord ar) 
	throws LDAPException {
		LDAPConnection result = new LDAPConnection();

        if ( isSecureLDAPUrl(ldapUrl) ) {
	    	LDAPSocketFactory sfactory = getLDAPSSLSocketFactory();
	    	if (sfactory == null) {
				throw new LDAPException("Can't get LDAPS socket factory", LDAPException.CONNECT_ERROR);
			}
            result.setSocketFactory(sfactory);
		}
        
		if (Debug.isTraceTypeEnabled(Debug.TYPE_LDAP)) {
			result.setProperty(result.TRACE_PROPERTY, System.err);
		}
		result.connect(ldapUrl.getHost(), ldapUrl.getPort());
		if (ar != null) {
			result.bind(3, ar.dn, ar.password);
		}
        else {
            result.bind(3, _defaultBindDN, _defaultBindPWD);
        }

		return result;
	}

	
	/**
	 * Encapsulate UtilConsoleGlobals.getLDAPSSLSocketFactory()
	 * in a try clause. If something goes wrong (like jss is missing), 
	 * return null.
	 */
	private static LDAPSocketFactory getLDAPSSLSocketFactory() {
		LDAPSocketFactory result;
	    try {
	    	result = UtilConsoleGlobals.getLDAPSSLSocketFactory();
	    }
	    catch(Throwable x) {
	    	result = null;
	    }
		return result;
	}


	public static boolean isSecureLDAPUrl(LDAPUrl url) {
		return url.isSecure();
	}


	/**
	 * Make an url from the specified arguments.
	 */
	public static LDAPUrl makeLDAPUrl(String host, int port, String dn, boolean secure) {
		return new LDAPUrl(
			host, port, dn, 
			null, // No attributes
			LDAPConnection.SCOPE_BASE,
			null, // No filter
			secure);
	}


	/**
	 * Make an url from the specified arguments.
	 * This 
	 */
	public static LDAPUrl makeLDAPUrl(LDAPConnection ldc, String dn) {
		return new LDAPUrl(
			ldc.getHost(), ldc.getPort(), dn, 
			null, // No attributes
			LDAPConnection.SCOPE_BASE,
			null, // No filter
			isSecureConnection(ldc));
	}


	/**
	 * Make an url from the specified arguments.
	 * This 
	 */
	public static LDAPUrl makeLDAPUrl(LDAPUrl url, String dn) {
		return new LDAPUrl(
			url.getHost(), url.getPort(), dn, 
			null, // No attributes
			LDAPConnection.SCOPE_BASE,
			null, // No filter			
			url.isSecure());
	}
}


/**
 * A struct representing authentication data.
 */
class AuthRecord {
	LDAPUrl ldapUrl;
	String dn;
	String password;
}

/**
 * A struct representing an active connection.
 */
class ConnectionRecord {
	LDAPConnection ldc;
	int counter;
	boolean disconnectAfterUse;
}
