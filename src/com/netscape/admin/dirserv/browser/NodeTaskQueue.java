/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

/**
 * This class represents a queue of Objects.
 *
 * This queue is composed of two logical sub-queues:
 *		- the 'waiting queue'
 *		- the 'working queue'
 *
 * Three basic operations are available:
 *		- queue() adds an Object to the 'waiting queue'.
 *		- fetch() moves an Object from the 'waiting queue'
 *		  to the 'working queue' and returns its reference
 *		- flush() removes an Object from the 'working queue'.
 *
 * The cancel() operation marks an Object in the 'working queue'
 * as cancelled (but it does not remove it from the queue).
 *
 * Usage scenario : one or more producers generate some data
 * and queue them using queue(). One or more consumers fetch()
 * this data and apply a (lengthy) processing ; when they have
 * finished, they flush() the data.
 *
 * Property: an Object may appear in both the 'waiting queue'
 * and the 'working queue'. However fetch() will not return
 * this object until it is flushed from the 'working queue'.
 * In the scenario above, this property guaranties that two
 * consumers will never process the same data at the same time.
 * 
 */
package com.netscape.admin.dirserv.browser;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;


class NodeTaskQueue implements Runnable {

	String _name;
	Vector _waitingQueue = new Vector();
	Hashtable _workingList = new Hashtable();
	Hashtable _cancelList = new Hashtable();
	ThreadGroup _threadGroup;


	/**
	 * Construct a queue with the specified name.
	 * The name is for debugging purpose only.
	 */
	public NodeTaskQueue(String name, int threadCount) {
		_name = name;
		_threadGroup = new ThreadGroup(name);
		for (int i = 0; i < threadCount; i++) {
			Thread t = new Thread(_threadGroup, this, name + "[" + i + "]");
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
		}
	}
	
	/**
	 * Returns the name of this queue.
	 */
	public String getName() {
		return _name;
	}
	
	
	/** 
	 * Shutdown this queue.
	 * All the associated threads are stopped.
	 */
	public void shutdown() {
		_threadGroup.interrupt();
	}


	/**
	 * Add an object in this queue.
	 * If the object is already in the waiting sub-queue, it is
	 * silently ignored.
	 */
	public synchronized void queue(AbstractNodeTask nodeTask) {
		if (nodeTask == null) throw new IllegalArgumentException("null argument");
		_waitingQueue.addElement(nodeTask);
		notify();
//		System.out.println("Queued " + nodeTask + " in " + _name);
	}


	/**
	 * Cancel an object.
	 * If the object is in the waiting sub-queue, it's simply
	 * removed from.
	 * If the object is in the working subqueue, it's kept in
	 * place but marked as cancelled. It's the responsibility
	 * of the consumer to detect that and flush the object asap.
	 */
	public synchronized void cancelForNode(BasicNode node) {
		if (node == null) throw new IllegalArgumentException("null argument");
		// Remove all the associated tasks from the waiting queue
		for (int i = _waitingQueue.size()-1; i >= 0; i--) {
			AbstractNodeTask task = (AbstractNodeTask)_waitingQueue.elementAt(i);
			if (task.getNode() == node) {
				_waitingQueue.removeElementAt(i);
			}
		}
		// Mark the on-going task as cancelled
		AbstractNodeTask task = (AbstractNodeTask)_workingList.get(node);
		if (task != null) {
			_cancelList.put(node, node);
			task.cancel();
		}
		notify();
	}
	
	
	/**
	 * Cancel all the object from this queue.
	 */
	public synchronized void cancelAll() {
		_waitingQueue.removeAllElements();
		Enumeration e = _workingList.keys();
		while (e.hasMoreElements()) {
			Object node = e.nextElement();
			AbstractNodeTask task = (AbstractNodeTask)_workingList.get(node);
			_cancelList.put(node, node);
			task.cancel();
		}
	}
	


	/**
	 * Fetch an object from this queue.
	 * The returned object is moved from the waiting sub-queue
	 * to the working sub-queue.
	 */
	private synchronized AbstractNodeTask fetch() throws InterruptedException {
		AbstractNodeTask result = null;

		// Get the first obj from _waitingQueue which is
		// not in _workingList yet.
		do {
			int waitingSize = _waitingQueue.size();
			int i = 0;
			while ((i < waitingSize) && !canBeFetched(i)) {
				i++;   
			}
			if (i == waitingSize) { // Nothing found
				wait();
			}
			else {
				result = (AbstractNodeTask)_waitingQueue.elementAt(i);
				_waitingQueue.removeElementAt(i);
				_workingList.put(result.getNode(), result);
			}
		}
		while (result == null);
		
//		System.out.println("Fetched " + result + " from " + _name);
		
		return result;
	}

	private boolean canBeFetched(int i) {
		AbstractNodeTask task = (AbstractNodeTask)_waitingQueue.elementAt(i);
		return _workingList.get(task.getNode()) == null;
	}
	
	
	/**
	 * Flush an object from this queue.
	 * The object is removed from the working sub-queue.
	 */
	private synchronized void flush(AbstractNodeTask task) {
		if (task == null) throw new IllegalArgumentException("null argument");
		_workingList.remove(task.getNode());
		_cancelList.remove(task.getNode());
		notify();
//		System.out.println("Flushed " + task + " from " + _name);
	}
	
	
	/**
	 * Return the number of object in this queue ie the
	 * number of object in both sub-queues.
	 */
	public int size() {
		return _waitingQueue.size() + _workingList.size();
	}
	
	
	
	public void run() {
		boolean interrupted = false;
		
		while (!interrupted) {
			try {
				// Fetch and process a node also
				// taking care of update events
				AbstractNodeTask task = fetch();
				task.run();
				flush(task);
			}
			catch(InterruptedException x) {
				interrupted = true;
			}
			catch(Exception x) {
				// At this level, either it's an interruption
				// either it's a bug...
				if (! (x instanceof InterruptedException)) {
					x.printStackTrace();
				}
			}
		}
	}
	
}


