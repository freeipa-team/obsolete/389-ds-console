/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.browser;

import java.util.Vector;
import java.util.Enumeration;
import netscape.ldap.LDAPUrl;
import netscape.ldap.util.DN;

/**
  * This class has been created in order to be able to hack in the browser
  * the bug that does not allow numsubordinate to work between databases.
  */
public class NumSubordinateHacker {
	String _serverHost;
	int _serverPort;
	Vector _unreliableEntryList;
	boolean _isUnreliableEntryListEmpty;

	public NumSubordinateHacker() {
		_serverHost = "not-initialized";
		_serverPort = -1;
		_unreliableEntryList = new Vector();
	}

	/**
	  * Tells wether the list of unreliable contains children of
	  * the entry with LDAPUrl parentUrl.
	  * @param parentURL the LDAPURL of the parent.
	  *
	  * @returns true if the list of unreliable entries contains a children of the parentUrl
	  */
	public boolean containsChildrenOf(LDAPUrl parentUrl) {
		boolean containsChildren = false;

		if (!_isUnreliableEntryListEmpty) {
			boolean isInServer = _serverHost.equals(parentUrl.getHost()) && (_serverPort == parentUrl.getPort());
			
			if (isInServer) {
				DN parentDN = new DN(parentUrl.getDN());
				Enumeration e = _unreliableEntryList.elements();
				while (e.hasMoreElements() && !containsChildren) {			
					DN currentDN = (DN)e.nextElement();
					if (parentDN.equals(currentDN.getParent())) {
						containsChildren = true;
					}
				}			
			}
		}
		return containsChildren;
	}

	/**
	  * Tells wether the list of unreliable contains the entry with LDAPUrl parentUrl.
	  * It assumes that we previously called containsChildrenOf (there's no check of the host/port)
	  * @param parentURL the LDAPURL of the parent.
	  *
	  * @returns true if the ldapUrl correspond to an unreliable entry
	  */
	public boolean contains(LDAPUrl url) {
		boolean contains = false;
		if (!_isUnreliableEntryListEmpty) {
			boolean isInServer = _serverHost.equals(url.getHost()) && (_serverPort == url.getPort());
			
			if (isInServer) {
				DN urlDN = new DN(url.getDN());
				Enumeration e = _unreliableEntryList.elements();
				while (e.hasMoreElements() && !contains) {			
					DN currentDN = (DN)e.nextElement();
					if (urlDN.equals(currentDN)) {
						contains = true;
					}
				}
			}
		}
		return contains;
	}

	/**
	 * This method construct a list with the netscape.ldap.util.DN of the entries that are parents of the 
	 * suffix entries.  This list is needed to overpass the fact that numsubordinates does not work
	 * between databases
	 */
	public void update(Vector allSuffixes, 
	                   Vector rootSuffixes,
					   String serverHost,
					   int serverPort
	) {		
		for (int i=0; i<rootSuffixes.size(); i++) {
			allSuffixes.removeElement(rootSuffixes.elementAt(i));
		}
		Vector subSuffixes = allSuffixes;
		synchronized (_unreliableEntryList) {
			_unreliableEntryList.clear();
			
			for (int i=0; i<subSuffixes.size(); i++) {
				DN subSuffixDN = new DN((String)subSuffixes.elementAt(i));
				_unreliableEntryList.addElement(subSuffixDN.getParent());
			}
			_isUnreliableEntryListEmpty = _unreliableEntryList.isEmpty();
		}
		_serverHost = serverHost;
		_serverPort = serverPort;
	}
}
