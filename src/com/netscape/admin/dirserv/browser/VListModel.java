/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.browser;

import java.util.*;
import java.io.UnsupportedEncodingException;
import javax.swing.*;
import javax.swing.event.*;
import netscape.ldap.*;
import netscape.ldap.controls.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.task.CreateVLVIndex;

import com.netscape.admin.dirserv.*;

class VListModel extends ChildrenListModel {

    private boolean _initialized = false;
    private ChildrenController _controller;

    private int _pageSize = 40;    
    private int _firstIndex = 0;
    private int _totalCount = 0;

    private String _base;
    private int _scope;
    private String _filter;
    private String[] _attrs;
    private LDAPSearchConstraints _cons;
    private LDAPConnection _ldc;
    private LDAPVirtualListControl _vlc;
    
    /**
     * Constructor
     */
    VListModel(BasicNode parentNode, ChildrenController controller,
               String searchPartition) throws LDAPException {

        _controller = controller;
        _ldc  = _controller.findConnectionForDisplayedEntry(parentNode);
        _base = _controller.findBaseDNForChildEntries(parentNode);
        _scope = _ldc.SCOPE_ONE;
        _filter = _controller.getChildSearchFilter();
        _attrs = _controller.getAttrsForRedSearch();

        // Specify necessary controls for vlv
        _cons  = _ldc.getSearchConstraints();
        _cons.setServerControls(createServerControls(searchPartition));
        _cons.setMaxResults( 0 );        
    }    
    
    /**
     * Create search request controls
     */
    private LDAPControl[] createServerControls (String searchPartition) {

        LDAPSortControl sortCtrl = null;
        LDAPVirtualListControl vlvCtrl = null;
        LDAPControl manageDSIT = null;
        LDAPControl searchCtrl = null;        
        LDAPControl[] ctrls = null;

        // Sort Control (VLV control requires a Sort control).
        LDAPSortKey[] keys = new LDAPSortKey[CreateVLVIndex.DEFAULT_ATTRIBUTES.length];
        for (int i=0; i<keys.length; i++) {
            keys[i] = new LDAPSortKey(CreateVLVIndex.DEFAULT_ATTRIBUTES[i]);
        }
        sortCtrl = new LDAPSortControl( keys, true );

        // VLV Control
        vlvCtrl = _vlc = new LDAPVirtualListControl(0, 0, 0, 0);

        // ManageDSIT contol (do not follow referrals)
        manageDSIT = new LDAPControl( LDAPControl.MANAGEDSAIT, true, null );

        // Search partition control 
        if (searchPartition != null) {
            try {
                byte[] vals = searchPartition.getBytes( "UTF8" );
                searchCtrl = new LDAPControl(DSEntryObject.SEARCH_OID, true, vals);
            } catch ( UnsupportedEncodingException e ) {
                Debug.println( "VListModel.getEntries(): Error: UTF8 not supported" );
            }
        }

        ctrls = (searchCtrl == null) ?  new LDAPControl[3] : new LDAPControl[4];
        
        ctrls[0] = sortCtrl;
        ctrls[1] = vlvCtrl;
        ctrls[2] = manageDSIT;
        if (searchCtrl != null) {
            ctrls[3] = searchCtrl;
        }
        
        return ctrls;
    }
    
    /**
     * Called by JList to get virtual list size
     * @return Virtual list size
     */
    public int getSize() {
        if ( !_initialized ) {
            getPage( 0 );
            _initialized = true;            
        }
        Debug.println(9, "VListModel.getSize()=" + _totalCount);
        return _totalCount;
    }
    
    // Called by JList to fetch data to paint a single list item
    public Object getElementAt(int index) {

        int maxIdx = _firstIndex + _entries.size() - 1;
        
        // Need a new page ?
        if (!(index >= _firstIndex && index <= maxIdx)) {
            if ( !getPage( index ) ) {
                Debug.println("VListModel.getElementAt: " +
                              "getPage(" + index + ") is null");
                return _controller.createNodeFromEntry(null); // Bogus Node
            }
            maxIdx = _firstIndex + _entries.size() - 1;            
        }

        if (index >= _firstIndex && index <= maxIdx) {
            int offset = index - _firstIndex;
            BasicNode node = (BasicNode)_entries.elementAt( offset );
            Debug.println(8, "VListModel.getElementAt(" + index + 
                          ") object=" + node);
            return node;
        }
        else {        
            Debug.println("VListModel.getElementAt(" + index + 
                          ") object=<BOGUS>");
            return _controller.createNodeFromEntry(null); // Bogus Node
        }
    }

    private LDAPVirtualListResponse getEntries() throws LDAPException {

        LDAPVirtualListResponse responseCtrl = null;

        // Empty the buffer
        _entries.removeAllElements();
        
        // Do a search
        LDAPSearchResults result =
            _ldc.search( _base, _scope, _filter, _attrs, false, _cons );

        while ( result.hasMoreElements() ) {
            LDAPEntry entry = result.next();
            _entries.addElement(_controller.createNodeFromEntry(entry));
        }

        Debug.println("VListModel.getEntries: read " + _entries.size() + " entries");

        // Get the VLV Response control
        LDAPControl[] c = _ldc.getResponseControls();
        LDAPVirtualListResponse response = null;
        if ( c != null ) {
            for( int i = 0; i < c.length; i++ ) {
                if ( c[i] instanceof LDAPVirtualListResponse ) {
                    responseCtrl = (LDAPVirtualListResponse)c[i];
                    break;
                }
            }
        }

        return responseCtrl;
    }

    // Get a page starting at first (although we may also fetch
    // some preceding entries)
    private boolean getPage( int first ) {
        LDAPVirtualListResponse nextCont = null;

        Debug.println("VListModel.getPage: load pages for index " + first);

        // Set VLV control parameters; VLV server side offset starts with 1 not 0
        int offset = (first > _pageSize) ? (first - _pageSize) : 0;
        _vlc.setRange(offset+1, /*beforeCnt=*/0, /*afterCnt=*/_pageSize*3);
        if (!_initialized) {
            _vlc.setListSize(0);
        }
        Debug.println("VListModel.getPage: " + _vlc);

        // Get the actual entries
        try {
            nextCont = getEntries();
        }
        catch (Exception ex) {
            Debug.println("VListModel.getEntries " + ex);
            _totalCount = 0;
            _firstIndex = 0;
            return false;
        }

        // Check if we have a control returned
        if ( nextCont == null ) {
            _firstIndex = 0;
            _totalCount = _entries.size();
            _vlc.setListSize(_totalCount);
            return true;
        }

        Debug.println("VListModel.getPage: " + nextCont);
            
        // VLV server side offset starts with 1 not 0
        _firstIndex = nextCont.getFirstPosition() - 1;
        Debug.println("VListModel.getPage: _firstIndex=" + _firstIndex);

        // Now we know the total size of the virtual list box
        _totalCount = nextCont.getContentCount();
        Debug.println("VListModel.getPage: _totalCount=" + _totalCount);            
        if ( _totalCount < _entries.size() ) {
            _totalCount = _entries.size();
        }
        if (!_initialized) {
            _vlc.setListSize( _totalCount );
        }

        return true;
    }

    /**
     * Called by application to find out the top of the buffer
     */    
    public int getFirstIndex() {
        return _firstIndex;
    }

    /**
     * 
     */
    public void setPageSize( int size ) {
        _pageSize = size;
    }
    
    /**
     * 
     */
    public int getPageSize() {
        return _pageSize;
    }

    /**
     * 
     */    
    public void clear() {
        super.clear();
        _firstIndex = 0;
        _totalCount = 0;
    }

    /**
     * Force recalculation of list size
     */
    public void recalculate() {
        clear();
        _initialized = false;
        getSize();
    }

    /**
     * Fires a change even after a node has been updated
     * <p>
     * 
     * Throws an <tt>ArrayIndexOutOfBoundsException</tt> if the index 
     * is invalid.
     * @param      obj     the component is to update.
     */
    public void updateElement(Object obj) {
        int index = _entries.indexOf(obj);
        if (index >=0) {
            fireContentsChanged(this, index + _firstIndex, index + _firstIndex);
        }
    }

    /**
     * Adds the specified component to the end of this list. 
     *
     * @param   obj   the component to be added.
     */
    public void addElement(Object obj) {
        /*int index = _entries.size();
        _entries.addElement(obj);
        fireIntervalAdded(this, index, index);*/
        recalculate();
    }

    /**
     * Removes the first (lowest-indexed) occurrence of the argument 
     * from this list.
     *
     * @param   obj   the component to be removed.
     * @return  <code>true</code> if the argument was a component of this
     *          list; <code>false</code> otherwise.
     */
    public boolean removeElement(Object obj) {
        int index = _entries.indexOf(obj);
        boolean rv = _entries.removeElement(obj);
        if (index >= 0) {
            _vlc.setListSize(--_totalCount);
            fireIntervalRemoved(this, index + _firstIndex, index + _firstIndex);
        }
        return rv;
    }    
}
