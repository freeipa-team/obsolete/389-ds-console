/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

/**
 * BrowserEvent is use to notify listeners that a BrowserController
 * has started/finished to update its associated JTree.
 */
package com.netscape.admin.dirserv.browser;

import java.util.EventObject;

public class BrowserEvent extends EventObject {

	public static final int UPDATE_START = 1;
	public static final int UPDATE_END   = 2;
	
	int _id;
	
	public BrowserEvent(Object source, int id) {
		super(source);
		_id = id;
	}
	
	public int getID() {
		return _id;
	}
}
