/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.browser;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import netscape.ldap.*;

class TestBrowser {

	static LDAPConnection _ldc;
	
	public static void main(String[] args) {
	
		// Open the LDAP Connection
		String host = "frog";
		int port = 389;
		String bindDN = "cn=directory manager";
		String bindPW = "ecureuil";
		try {
			_ldc = new LDAPConnection();
			_ldc.connect(host, port);
			_ldc.bind(3, bindDN, bindPW);
		}
		catch(LDAPException x) {
			x.printStackTrace();
			_ldc = null;
		}
	
	
		// Create and display the frame
		if (_ldc != null) {
			final BasicBrowserFrame frame = new BasicBrowserFrame(_ldc);
			frame.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					try {
						frame.beforeExit();
						_ldc.disconnect();
					}
					catch(LDAPException x) {
						x.printStackTrace();
					}
					System.exit(0);
				}
			});  
			frame.setVisible(true);
		}
   }

}


class BasicBrowserFrame extends JFrame 
implements BrowserEventListener, TreeSelectionListener, ActionListener {

	JTree _tree;
	BrowserController _controller;
	NumSubordinateHacker _numSubordinateHacker;
	IBrowserNodeInfo _selectedNodeInfo;
	Menu _selectedMenu;
	MenuItem _quitMenuItem;
	MenuItem _removeAllSuffixesMenuItem;
	MenuItem _shutdownMenuItem;
	MenuItem _startRefreshMenuItem;
	MenuItem _stopRefreshMenuItem;
	MenuItem _notifyEntryAddedMenuItem;
	MenuItem _notifyEntryDeletedMenuItem;
	MenuItem _notifyEntryChangedMenuItem;
	CheckboxMenuItem _aciCountMenuItem;
	CheckboxMenuItem _roleCountMenuItem;
	CheckboxMenuItem _activationStateMenuItem;
	CheckboxMenuItem _followRefMenuItem;
	CheckboxMenuItem _sortMenuItem;
	CheckboxMenuItem _showContainerOnlyMenuItem;
	JLabel _status;
	JLabel _selectedDn;
	
	public BasicBrowserFrame(LDAPConnection ldc) {
		_tree = new JTree();
		_tree.addTreeSelectionListener(this);
		_status = new JLabel("Status: idle");
		_selectedDn = new JLabel("DN:");

		JScrollPane scrollPane = new JScrollPane(_tree);

		getContentPane().add(scrollPane, BorderLayout.CENTER);
		getContentPane().add(_status, BorderLayout.NORTH);
		getContentPane().add(_selectedDn, BorderLayout.SOUTH);
		setTitle(ldc.getHost() + ":" + String.valueOf(ldc.getPort()));

		if (!SwingUtilities.isEventDispatchThread()) {
			System.out.println("Not the event dispatch thread");
		}
		
		// NUMSUBORDINATE HACK
		// Life could be simple...
		Vector allSuffixes = new Vector();
		allSuffixes.addElement("o=stress.com");
		allSuffixes.addElement("o=netscaperoot");
		allSuffixes.addElement("cn=config");
		allSuffixes.addElement("dc=france,dc=sun,dc=com");
		allSuffixes.addElement("dc=gnb,dc=france,dc=sun,dc=com");
		allSuffixes.addElement("dc=b14,dc=gnb,dc=france,dc=sun,dc=com");
		allSuffixes.addElement("dc=austin,dc=france,dc=sun,dc=com");
		allSuffixes.addElement("dc=lab,dc=austin,dc=france,dc=sun,dc=com");
		Vector rootSuffixes = new Vector();
		rootSuffixes.addElement("o=stress.com");
		rootSuffixes.addElement("o=netscaperoot");
		rootSuffixes.addElement("cn=config");
		rootSuffixes.addElement("dc=france,dc=sun,dc=com");
		_numSubordinateHacker = new NumSubordinateHacker();
		_numSubordinateHacker.update(allSuffixes, rootSuffixes, ldc.getHost(), ldc.getPort());
		
		_controller = new BrowserController(_tree);
		_controller.addBrowserEventListener(this);
		_controller.setLDAPConnection(ldc);
		_controller.addSuffix("dc=france,dc=sun,dc=com", null);
		_controller.addSuffix("o=stress.com", null);
		_controller.addSuffix("o=netscaperoot", null);
		_controller.addSuffix("cn=config", null);
		_controller.setContainerClasses(new String[] {
			"organization",
			"organizationalUnit",
			"netscapeServer",
			"netscapeResource"
		});
		_controller.setShowContainerOnly(false);
		_controller.setDisplayFlags(_controller.DISPLAY_ACTIVATION_STATE);
		_controller.setFollowReferrals(true);
		_controller.setNumSubordinateHacker(_numSubordinateHacker);
		
		createMenuBar();

		pack();
	}
	
	private void createMenuBar() {
		_quitMenuItem = new MenuItem("Quit");
		_quitMenuItem.addActionListener(this);
		_removeAllSuffixesMenuItem = new MenuItem("Remove All Suffixes");
		_removeAllSuffixesMenuItem.addActionListener(this);
		_shutdownMenuItem = new MenuItem("Shutdown");
		_shutdownMenuItem.addActionListener(this);
		
		_startRefreshMenuItem = new MenuItem("Start Refresh");
		_startRefreshMenuItem.addActionListener(this);
		_stopRefreshMenuItem = new MenuItem("Stop Refresh");
		_stopRefreshMenuItem.addActionListener(this);
		_notifyEntryAddedMenuItem = new MenuItem("Notify Entry Added");
		_notifyEntryAddedMenuItem.addActionListener(this);
		_notifyEntryDeletedMenuItem = new MenuItem("Notify Entry Deleted");
		_notifyEntryDeletedMenuItem.addActionListener(this);
		_notifyEntryChangedMenuItem = new MenuItem("Notify Entry Changed");
		_notifyEntryChangedMenuItem.addActionListener(this);
		
		_aciCountMenuItem = new CheckboxMenuItem("ACI #");
		_aciCountMenuItem.addActionListener(this);
		_roleCountMenuItem = new CheckboxMenuItem("Role #");
		_roleCountMenuItem.addActionListener(this);
		_activationStateMenuItem = new CheckboxMenuItem("Activation State");
		_activationStateMenuItem.addActionListener(this);
		_followRefMenuItem = new CheckboxMenuItem("Follow Referrals");
		_followRefMenuItem.addActionListener(this);
		_sortMenuItem = new CheckboxMenuItem("Sort");
		_sortMenuItem.addActionListener(this);
		_showContainerOnlyMenuItem = new CheckboxMenuItem("Show Container Only");
		_showContainerOnlyMenuItem.addActionListener(this);

		_aciCountMenuItem.setState((_controller.getDisplayFlags() & _controller.DISPLAY_ACI_COUNT) != 0);
		_roleCountMenuItem.setState((_controller.getDisplayFlags() & _controller.DISPLAY_ROLE_COUNT) != 0);
		_activationStateMenuItem.setState((_controller.getDisplayFlags() & _controller.DISPLAY_ACTIVATION_STATE) != 0);
		_followRefMenuItem.setState(_controller.getFollowReferrals());
		_sortMenuItem.setState(_controller.isSorted());
		_showContainerOnlyMenuItem.setState(_controller.isShowContainerOnly());

		Menu fileMenu = new Menu("File");
		fileMenu.add(_removeAllSuffixesMenuItem);
		fileMenu.add(_shutdownMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(_quitMenuItem);
		
		_selectedMenu = new Menu("Selected");
		_selectedMenu.setEnabled(false);
		_selectedMenu.add(_startRefreshMenuItem);
		_selectedMenu.add(_stopRefreshMenuItem);
		_selectedMenu.addSeparator();
		_selectedMenu.add(_notifyEntryAddedMenuItem);
		_selectedMenu.add(_notifyEntryDeletedMenuItem);
		_selectedMenu.add(_notifyEntryChangedMenuItem);
		
		Menu viewMenu = new Menu("View");
		viewMenu.add(_aciCountMenuItem);
		viewMenu.add(_roleCountMenuItem);
		viewMenu.add(_activationStateMenuItem);
		viewMenu.addSeparator();
		viewMenu.add(_followRefMenuItem);
		viewMenu.add(_sortMenuItem);
		viewMenu.add(_showContainerOnlyMenuItem);
		
		MenuBar menuBar = new MenuBar();
		menuBar.add(fileMenu);
		menuBar.add(_selectedMenu);
		menuBar.add(viewMenu);
		
		setMenuBar(menuBar);
	}
	
	public void processBrowserEvent(BrowserEvent e) {
		switch(e.getID()) {
			case BrowserEvent.UPDATE_START:
				_status.setText("Updating...");
				break;
			case BrowserEvent.UPDATE_END:
				_status.setText("Ok");
				break;
		}
	}
	
	
	public void valueChanged(TreeSelectionEvent e) {
		TreePath path = e.getPath();
		if (path == null) {
			_selectedNodeInfo = null;
			_selectedDn.setText("DN:");
		}
		else {
			_selectedNodeInfo = _controller.getNodeInfoFromPath(path);
			_selectedDn.setText("DN: " + _selectedNodeInfo);
		}
		_selectedMenu.setEnabled(path != null);
	}


	public void beforeExit() {
		_controller.shutDown();
	}


	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == _quitMenuItem) {
			quitAction();
		}
		else if (source == _shutdownMenuItem) {
			shutdownAction();
		}
		else if (source == _removeAllSuffixesMenuItem) {
			removeAllSuffixesAction();
		}
		else if (source == _startRefreshMenuItem) {
			startRefreshAction();
		}
		else if (source == _stopRefreshMenuItem) {
			stopRefreshAction();
		}
		else if (source == _notifyEntryAddedMenuItem) {
			notifyEntryAddedAction();
		}
		else if (source == _notifyEntryDeletedMenuItem) {
			notifyEntryDeletedAction();
		}
		else if (source == _notifyEntryChangedMenuItem) {
			notifyEntryChangedAction();
		}
		else if (source == _aciCountMenuItem) {
			aciCountAction();
		}
		else if (source == _roleCountMenuItem) {
			roleCountAction();
		}
		else if (source == _activationStateMenuItem) {
			activationStateAction();
		}
		else if (source == _followRefMenuItem) {
			followRefAction();
		}
		else if (source == _sortMenuItem) {
			sortAction();
		}
		else if (source == _showContainerOnlyMenuItem) {
			showContainerOnlyAction();
		}
	}
	
	
	private void quitAction() {
		try {
			beforeExit();
			_controller.getLDAPConnection().disconnect();
		}
		catch(LDAPException x) {
			x.printStackTrace();
		}
		System.exit(0);
	}
	
	
	private void shutdownAction() {
		System.out.println("shutdownAction");
		_controller.shutDown();
	}
	
	
	private void removeAllSuffixesAction() {
		System.out.println("removeAllSuffixesAction");
		_controller.removeAllSuffixes();
	}
	
	
	private void startRefreshAction() {
		System.out.println("startRefreshAction");
		_controller.startRefresh(_selectedNodeInfo);
	}
	
	
	private void stopRefreshAction() {
		System.out.println("stopRefreshAction");
		_controller.stopRefresh();
	}
	
	
	private void notifyEntryAddedAction() {
		System.out.println("notifyEntryAddedAction");
		_controller.notifyEntryAdded(_selectedNodeInfo, "ou=basque,ou=people,dc=france,dc=sun,dc=com");
	}
	
	
	private void notifyEntryDeletedAction() {
		System.out.println("notifyEntryDeletedAction");
		_controller.notifyEntryDeleted(_selectedNodeInfo);
	}
	
	
	private void notifyEntryChangedAction() {
		System.out.println("notifyEntryChangedAction");
		_controller.notifyEntryChanged(_selectedNodeInfo);
	}
	
	
	private void aciCountAction() {
		System.out.println("aciCountAction");
		int flags = _controller.getDisplayFlags();
		if (_aciCountMenuItem.getState()) {
			flags = flags | _controller.DISPLAY_ACI_COUNT;
		}
		else {
			flags = flags & ~_controller.DISPLAY_ACI_COUNT;
		}
		_controller.setDisplayFlags(flags);
	}
	
	
	private void roleCountAction() {
		System.out.println("roleCountAction");
		int flags = _controller.getDisplayFlags();
		if (_roleCountMenuItem.getState()) {
			flags = flags | _controller.DISPLAY_ROLE_COUNT;
		}
		else {
			flags = flags & ~_controller.DISPLAY_ROLE_COUNT;
		}
		_controller.setDisplayFlags(flags);
	}
	
	
	private void activationStateAction() {
		System.out.println("activationStateAction");
		int flags = _controller.getDisplayFlags();
		if (_activationStateMenuItem.getState()) {
			flags = flags | _controller.DISPLAY_ACTIVATION_STATE;
		}
		else {
			flags = flags & ~_controller.DISPLAY_ACTIVATION_STATE;
		}
		_controller.setDisplayFlags(flags);
	}
	
	
	private void followRefAction() {
		System.out.println("followRefAction");
		_controller.setFollowReferrals(_followRefMenuItem.getState());
	}
	
	
	private void sortAction() {
		System.out.println("sortAction");
		_controller.setSorted(_sortMenuItem.getState());
	}
	
	
	private void showContainerOnlyAction() {
		System.out.println("showContainerOnlyAction");
		_controller.setShowContainerOnly(_showContainerOnlyMenuItem.getState());
		_controller.setSorted(_sortMenuItem.getState());
	}
	
	
}

