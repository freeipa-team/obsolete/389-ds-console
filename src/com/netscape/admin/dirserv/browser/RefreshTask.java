/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/


/**
 * Refresh Task
 *
 */
package com.netscape.admin.dirserv.browser;

import java.util.Vector;
import java.util.Enumeration;
import java.text.Collator;
import java.net.MalformedURLException;

import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import netscape.ldap.LDAPInterruptedException;
import netscape.ldap.LDAPEntry;
import netscape.ldap.LDAPAttribute;
import netscape.ldap.LDAPSearchResults;
import netscape.ldap.LDAPSearchConstraints;
import netscape.ldap.LDAPUrl;

class RefreshTask extends AbstractNodeTask {

	public static final int QUEUED				= 0;
	public static final int READING_LOCAL_ENTRY	= 1;
	public static final int SOLVING_REFERRAL	= 2;
	public static final int DETECTING_CHILDREN	= 4;
	public static final int SEARCHING_CHILDREN	= 5;
	public static final int FINISHED			= 7;
	public static final int CANCELLED			= 8;
	public static final int INTERRUPTED			= 9;
	public static final int FAILED				= 10;


	BrowserController _controller;
	int _state;
	boolean _recursive;
	
	LDAPEntry _localEntry;
	LDAPEntry _remoteEntry;
	LDAPUrl   _remoteUrl;
	boolean _isLeafNode;
	Vector _childEntries = new Vector(50);
	boolean _differential;
	Exception _exception;
	Object _exceptionArg;
	

	public RefreshTask(BasicNode node, BrowserController ctlr, LDAPEntry localEntry, boolean recursive) {
		super(node);
		_controller = ctlr;
		_state = QUEUED;
		_recursive = recursive;
		
		_localEntry = localEntry;
	}
	
	
	public LDAPEntry getLocalEntry() {
		return _localEntry;
	}


	public LDAPEntry getRemoteEntry() {
		return _remoteEntry;
	}
	
	
	public LDAPUrl getRemoteUrl() {
		return _remoteUrl;
	}
	
	
	public boolean isLeafNode() {
		return _isLeafNode;
	}


	public Vector getChildEntries() {
		return _childEntries;
	}
	
	public boolean isDifferential() {
		return _differential;
	}
	
	
	public Exception getException() {
		return _exception;
	}
	
	
	public Object getExceptionArg() {
		return _exceptionArg;
	}


	public LDAPEntry getDisplayedEntry() {
		LDAPEntry result;
		if (_controller.getFollowReferrals() && (_remoteEntry != null)) {
			result = _remoteEntry;
		}
		else {
			result = _localEntry;
		}
		return result;
	}


	public LDAPUrl getDisplayedUrl() {
		LDAPUrl result;
		if (_controller.getFollowReferrals() && (_remoteUrl != null)) {
			result = _remoteUrl;
		}
		else {
			result = _controller.findUrlForLocalEntry(getNode());
		}
		return result;
	}


	public boolean isInFinalState() {
		return (
			(_state == FINISHED) ||
		    (_state == CANCELLED) ||
		    (_state == FAILED) ||
		    (_state == INTERRUPTED)
		);
	}
	
	
	public void run() {
		BasicNode node = getNode();

		try {
			if (_localEntry == null) {
				changeStateTo(READING_LOCAL_ENTRY);
				runReadLocalEntry();
			}
			if (_controller.getFollowReferrals() && isReferralEntry(_localEntry)) {
				changeStateTo(SOLVING_REFERRAL);
				runSolveReferral();
			}
			if (node.isLeaf()) {
				changeStateTo(DETECTING_CHILDREN);
				runDetectChildren();
			}
			if (_controller.nodeIsExpanded(node) && _recursive) {
				changeStateTo(SEARCHING_CHILDREN);
				runSearchChildren();
			}
			changeStateTo(FINISHED);
		}
		catch(TaskAbandonException x) {
			_exception = x.getException();
			_exceptionArg = x.getArg();
			try {
				changeStateTo(x.getState());
			}
			catch(TaskAbandonException xx) {
				// We've done all what we can...
			}
		}
	}


	/**
	 * Read the local entry associated to the current node.
	 */
	private void runReadLocalEntry() throws TaskAbandonException {
		BasicNode node = getNode();
		LDAPConnection ldc = null;
		
		try {
			LDAPSearchConstraints sc = (LDAPSearchConstraints)_controller.getSearchConstraints().clone();
			sc.setTimeLimit(5*1000);
			ldc = _controller.findConnectionForLocalEntry(node);
			_localEntry = ldc.read(node.getDN(),
							  _controller.getAttrsForRedSearch(), sc);
			throwAbandonIfNeeded(null);
		}
		catch(LDAPException x) {
			throwAbandonIfNeeded(x);
		}
		finally {
			if (ldc != null) {
				_controller.releaseLDAPConnection(ldc);
			}
		}
	}


	/**
	 * Solve the referral associated to the current node.
	 * This routine assumes that node.getReferral() is non null
	 * and that BrowserController.getFollowReferrals() == true.
	 * It also protect the browser against looping referrals by
	 * limiting the number of hops.
	 */
	private void runSolveReferral() throws TaskAbandonException {
		int hopCount = 0;
		String[] referral = getNode().getReferral();		
		while ((referral != null) && (hopCount < 10)) {			
			readRemoteEntry(referral);									
			referral = BrowserController.getReferral(_remoteEntry);
			hopCount++;										
		}
		if (referral != null) { // -> hopCount has reached the max
			throwAbandonIfNeeded(
								 new LDAPException("Referral limit (" + hopCount + ") reached",
												   LDAPException.REFERRAL_LIMIT_EXCEEDED)
								 );
		}
	}   

	private void readRemoteEntry(String[] referral) 
	throws TaskAbandonException {
		LDAPConnectionPool connectionPool = _controller.getConnectionPool();
		LDAPUrl url = null;
		LDAPEntry entry = null;
		String remoteDn = null;
		Exception lastException = null;
		Object lastExceptionArg = null;
		
		int i = 0;
		while ((i < referral.length) && (entry == null)) {
			LDAPConnection ldc = null;
			try {
				url = new LDAPUrl(referral[i]);

                // Check for ldap:/// referrals. host:port is the
                // same as for the current ldap connection.
                String host = url.getHost();
                int port = url.getPort();
                if (host == null || host.length() == 0) {
                    ldc = _controller.findConnectionForLocalEntry(getNode());
                }                
                else {
                    ldc = connectionPool.getConnection(url);
                }
                host = ldc.getHost();
                port = ldc.getPort();

				remoteDn = url.getDN();				
				if ((remoteDn == null) ||
					remoteDn.equals("")) {
					/* The referral has not a target DN specified: we
					   have to use the DN of the entry that contains the 
					   referral... */
					if (_remoteEntry != null) {						
						remoteDn = _remoteEntry.getDN();						
					} else {
						remoteDn = _localEntry.getDN();						
					}
				}				

				/* We have to recreate the url */
				url = new LDAPUrl(host,
								  port,
								  remoteDn,
								  url.getAttributeArray(),
								  url.getScope(),
								  url.getFilter(),
								  url.isSecure());									
                
                entry = ldc.read(remoteDn,
								 _controller.getAttrsForBlackSearch(),
								 _controller.getSearchConstraints());
				throwAbandonIfNeeded(null);
			}
			catch(LDAPInterruptedException x) {				
				throwAbandonIfNeeded(x);
			}
			catch(MalformedURLException x) {				
				lastException = x;
				lastExceptionArg = referral[i];
			}
			catch(LDAPException x) {				
				lastException = x;
				lastExceptionArg = referral[i];
			}
			finally {
				if (ldc != null) {
                    try {
					    connectionPool.releaseConnection(ldc);
                    }
                    catch (Exception ignore) {}
				}
			}
			i = i + 1;
		}
		if (entry == null) {
			throw new TaskAbandonException(FAILED, lastException, lastExceptionArg);
		}
		else {
			_remoteUrl = url;
			_remoteEntry = entry;
		}
	}
	

	private void runDetectChildren() throws TaskAbandonException {
		if (_controller.isShowContainerOnly() || !isNumSubOrdinatesUsable()) {
			runDetectChildrenManually();
		}
		else {
			LDAPEntry entry = getDisplayedEntry();
			_isLeafNode = (BrowserController.getNumSubOrdinates(entry) == 0);
		}
	}


	private void runDetectChildrenManually() throws TaskAbandonException {
		BasicNode parentNode = getNode();
		LDAPConnection ldc = null;
		LDAPSearchResults searchResults = null;
		boolean ok = false;
		Object detectionResult = null;

		try {
			// We set the search constraints so that only one entry is returned.
			// It's enough to know if the entry has children or not.
			LDAPSearchConstraints sc = (LDAPSearchConstraints)_controller.getSearchConstraints().clone();
			sc.setMaxResults(1);
			sc.setTimeLimit(5*1000);
			String[] attrs = {"dn"};
			// Send an LDAP search
			ldc = _controller.findConnectionForDisplayedEntry(parentNode);
			searchResults = ldc.search(_controller.findBaseDNForChildEntries(parentNode),
			                                  ldc.SCOPE_ONE,
											  _controller.getChildSearchFilter(),
											  attrs,
											  false,
											  sc);

			throwAbandonIfNeeded(null);
			
			if (searchResults.hasMoreElements()) { // May be parentNode has children
				Object firstResult = searchResults.nextElement();
				_isLeafNode = !(firstResult instanceof LDAPEntry);
			}
			else { // parentNode has no children
				_isLeafNode = true;
			}
		}
		catch(LDAPException x) {
			throwAbandonIfNeeded(x);
		}
		finally {
			if (ldc != null) {
// [blackflag 624234] this abandon is not needed since setMaxResults(1) is set.
//                    when it comes to this point, the server had done its job.
//				if (searchResults != null) {
//					try {
//						ldc.abandon(searchResults); // TODO: Is it required ?
//					}
//					catch(LDAPException x) {
//						throwAbandonIfNeeded(x);
//					}
//				}
				_controller.releaseLDAPConnection(ldc);
			}
		}
	}
	
	
	// NUMSUBORDINATE HACK
	// numsubordinates is not usable if the displayed entry
	// is listed in in the hacker.
	// Note: *usable* means *usable for detecting children presence*.
	private boolean isNumSubOrdinatesUsable() {
		boolean result;
		LDAPEntry entry = getDisplayedEntry();
		int numSubOrdinates = BrowserController.getNumSubOrdinates(entry);
		if (numSubOrdinates == 0) { // We must check
			LDAPUrl url = getDisplayedUrl();
			if (_controller.getNumSubordinateHacker().contains(url)) {
				// The numSubOrdinate we have is unreliable.
				result = false;
//				System.out.println("numSubOrdinates of " + url + 
//				                   " is not reliable");
			}
			else {
				result = true;
			}
		}
		else { // Other values are usable
			result = true;
		}
		return result;
	}
	
	
	
	private void runSearchChildren() throws TaskAbandonException {
		LDAPConnection ldc = null;
		LDAPSearchResults sr = null;
		BasicNode parentNode = getNode();
		Object searchResult = null;
		
		try {
			// Send an LDAP search
			ldc = _controller.findConnectionForDisplayedEntry(parentNode);
			sr = ldc.search(_controller.findBaseDNForChildEntries(parentNode),
			                ldc.SCOPE_ONE,
						    _controller.getChildSearchFilter(),
							_controller.getAttrsForRedSearch(),
						    false,
							_controller.getSearchConstraints());

			// Fetch incoming entries and periodically update the
			// browser _controller.
			while (sr.hasMoreElements()) {
				Object r = sr.nextElement();
				if (r instanceof LDAPEntry) {
					_childEntries.addElement(r);
				}
				else if (r instanceof LDAPException) {
					throw (LDAPException)r;
				}
				// Time to time we update the display
				if (_childEntries.size() >= 20) {
					changeStateTo(SEARCHING_CHILDREN);
					_childEntries.removeAllElements();
				}
				throwAbandonIfNeeded(null);
			}
		}
		catch(LDAPException x) {
			if (x.getLDAPResultCode() != LDAPException.SIZE_LIMIT_EXCEEDED) {
				throwAbandonIfNeeded(x);
			}			
		} 
		finally {
			if (ldc != null) {
				if (sr != null) {
					try {
						ldc.abandon(sr);
					}
					catch(LDAPException xx) {
						throwAbandonIfNeeded(xx);
					}
				}
				_controller.releaseLDAPConnection(ldc);
			}
		}
	}



	/**
	 * Utilities
	 */


	/**
	 * Change the state of the task and inform the BrowserController.
	 */
	private void changeStateTo(int newState) throws TaskAbandonException {
		int oldState = _state;
		_state = newState;
		try {
			_controller.invokeRefreshTaskDidProgress(this, oldState, newState);
		}
		catch(InterruptedException x) {
			throwAbandonIfNeeded(x);
		}
	}
	
	
	/**
	 * Transform an exception into a TaskAbandonException.
	 * If no exception is passed, the routine checks if the task has
	 * been cancelled and throws an TaskAbandonException accordingly.
	 */
	private void throwAbandonIfNeeded(Exception x) throws TaskAbandonException {
		TaskAbandonException tax = null;
		if (x != null) {
			if ((x instanceof InterruptedException) ||
			    (x instanceof LDAPInterruptedException)) {
				tax = new TaskAbandonException(INTERRUPTED, x, null);
			}
			else {
				tax = new TaskAbandonException(FAILED, x, null);
			}
		}
		else if (isCancelled()) {
			tax = new TaskAbandonException(CANCELLED, null, null);
		}
		if (tax != null) {
			throw tax;	
		}
	}


	/**
	 * DEBUG : Dump the state of the task.
	 */
	void dump() {
		System.out.println("=============");
		System.out.println("         node: " + getNode().getDN());
		System.out.println("   _recursive: " + _recursive);
		System.out.println("_differential: " + _differential);
		
		System.out.println("       _state: " + stateToString(_state));
		System.out.println("  _localEntry: " + _localEntry);
		System.out.println(" _remoteEntry: " + _remoteEntry);
		System.out.println("   _remoteUrl: " + _remoteUrl);
		System.out.println("  _isLeafNode: " + _isLeafNode);
		System.out.println("   _exception: " + _exception);
		System.out.println("_exceptionArg: " + _exceptionArg);
		System.out.println("=============");
	}
	
	static String stateToString(int state) {
		String result;
		switch(state) {
			case QUEUED:
				result = "QUEUED";
				break;
			case READING_LOCAL_ENTRY:
				result = "READING_LOCAL_ENTRY";
				break;
			case SOLVING_REFERRAL:
				result = "SOLVING_REFERRAL";
				break;
			case DETECTING_CHILDREN:
				result = "DETECTING_CHILDREN";
				break;
			case SEARCHING_CHILDREN:
				result = "SEARCHING_CHILDREN";
				break;
			case FINISHED:
				result = "FINISHED";
				break;
			case CANCELLED:
				result = "CANCELLED";
				break;
			case INTERRUPTED:
				result = "INTERRUPTED";
				break;
			case FAILED:
				result = "FAILED";
				break;
			default:
				result = "INVALID (" + state + ")";
				break;
		}
		
		return result;
	}

	/**
	 * Checks that the entry's objectclass contains
	 * 'referral' and that the attribute 'ref' is present.
	 */
	static boolean isReferralEntry(LDAPEntry entry) {
		boolean result = false;
		if (entry == null) {
			return result; // false - null entry is not a referral
		}
		LDAPAttribute attr = entry.getAttribute("objectclass");
		if (attr != null) {
			Enumeration objectclasses = attr.getStringValues();
			if (objectclasses != null) {
				boolean isReferral = false;
				while (objectclasses.hasMoreElements() && !isReferral) {
					String className = (String)objectclasses.nextElement();
					isReferral = className.equalsIgnoreCase("referral");
				}
				if (isReferral) {
					result = (entry.getAttribute("ref") != null);
				}
			}
		}
		return result;
	}
	
}




class TaskAbandonException extends Exception {

	int _state;
	Exception _x;
	Object _arg;

	TaskAbandonException(int state, Exception x, Object arg) {
		_state = state;
		_x = x;
		_arg = arg;
	}
	
	int getState() {
		return _state;
	}
	
	Exception getException() {
		return _x;
	}
	
	Object getArg() {
		return _arg;
	}
}
