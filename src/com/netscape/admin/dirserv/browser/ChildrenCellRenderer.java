/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.browser;

import java.awt.Font;
import java.awt.Color;
import java.awt.Component;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.Icon;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;



class ChildrenCellRenderer implements ListCellRenderer {
	JLabel _label;
	Font _defaultFont;
	Font _italicFont;
	Font _boldFont;
	Font _italicBoldFont;
	Border _noFocusBorder;
	Border _focusBorder;

	Font _currentFont;
	Icon _currentIcon;	
	boolean _currentSelected;
	boolean _currentFocus;

	ChildrenController _controller;	

	public ChildrenCellRenderer(ChildrenController controller) {
		_controller = controller;
	}

	public Component getListCellRendererComponent(JList list, 
												  Object value, 
												  int index, 
												  boolean isSelected,
												  boolean hasFocus) {

		if (_label == null) {
			_focusBorder = UIManager.getBorder("List.focusCellHighlightBorder");
			_noFocusBorder = new EmptyBorder(1, 1, 1, 1);

			_label = new JLabel();		
			_label.setOpaque(true);
			
			// To force update of border and background the first time the label is created
			_currentFocus = !hasFocus;
			_currentSelected = !isSelected;
		}		
		
		BasicNode node = (BasicNode)value;
				
		// Set the foreground and background colors: change them only if necessary
		if (_currentSelected != isSelected) {
			_currentSelected = isSelected;
			if (isSelected) {
				_label.setBackground(list.getSelectionBackground());
				_label.setForeground(list.getSelectionForeground());
			}
			else {
				_label.setBackground(list.getBackground());
				_label.setForeground(list.getForeground());
			}
		}

		// Update the border if necessary		
		if (hasFocus != _currentFocus) {
			_currentFocus = hasFocus;
			Border borderToUse = hasFocus ? _focusBorder : _noFocusBorder;			
			_label.setBorder(borderToUse);
		}
		
		// Set the icon and text
		Icon newIcon = node.getIcon();
		
		if (newIcon != _currentIcon) {
			_currentIcon = newIcon;
			_label.setIcon(_currentIcon);
		}	
		
		_label.setText(node.getDisplayName());
			
		// Set the font
		if (_defaultFont == null) {
			_defaultFont = _controller.getList().getFont();
			_italicFont = _defaultFont.deriveFont(Font.ITALIC);
			_boldFont = _defaultFont.deriveFont(Font.BOLD);
			_italicBoldFont = _defaultFont.deriveFont(Font.ITALIC|Font.BOLD);
			_currentFont = _defaultFont;
		}
		int style = node.getFontStyle();
		Font newFont;
		if ((style & Font.ITALIC & Font.BOLD) != 0) {
			newFont = _italicBoldFont;
		}
		else if ((style & Font.ITALIC) != 0) {
			newFont = _italicFont;
			}
		else if ((style & Font.BOLD) != 0) {
			newFont = _boldFont;
		}
		else {
			newFont = _defaultFont;
		}
		if (newFont != _currentFont) {
			_currentFont = newFont;
			_label.setFont(_currentFont);
		}				 				
		return _label;
	}	
}
