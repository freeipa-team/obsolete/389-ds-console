/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.panel.UIFactory;

/**
 * ExportImportProgressDialog
 * Simple cancellable export/import progress dialog
 *
 * @version 1.0
 * @author rweltman
 **/
public class ExportImportProgressDialog extends JDialog
                                        implements IEntryChangeListener,
                                                   ActionListener {
	public ExportImportProgressDialog( JFrame parent,
									   String title ) {
		this( parent, null, title );
	}

	public ExportImportProgressDialog( JFrame parent,
									   Runnable task,
									   String title ) {
		this( parent, task, title, true );
	}

	public ExportImportProgressDialog( JFrame parent,
									   Runnable task,
									   String title,
									   boolean allowCancel ) {
		this( parent, task, title, allowCancel, null );
	}

	public ExportImportProgressDialog( JFrame parent,
									   Runnable task,
									   String title,
									   boolean allowCancel,
									   Component comp ) {
		super(parent, true);
		setTitle(title);

		setContentPane(new JPanel());
		getContentPane().setLayout(new GridBagLayout());
//		getContentPane().setBackground(SystemColor.control);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = new Insets(0,PAD,3,8);
		gbc.anchor     = gbc.CENTER;
		gbc.weightx = 1;
		gbc.fill       = gbc.HORIZONTAL;

		_currentDN = new JLabel( " " );
		_currentDN.setHorizontalAlignment( SwingConstants.CENTER );
		_currentDN.setOpaque( true );
		getContentPane().add(_currentDN, gbc);
		gbc.gridy++;
		_error = new JLabel();
		_error.setHorizontalAlignment( SwingConstants.CENTER );
		_error.setOpaque( true );
		getContentPane().add(_error, gbc);
		gbc.gridy++;

		_allowCancel = allowCancel;
		if ( allowCancel ) {
			JButton bCancel =
				UIFactory.makeJButton(this, "general", "Cancel");
			bCancel.setActionCommand( CANCEL );
			bCancel.setOpaque( true );
			gbc.weightx = 0;
			gbc.fill       = gbc.NONE;
			gbc.insets = new Insets(PAD,PAD,3,8);
			getContentPane().add(bCancel, gbc);
		}

		setSize( WIDTH, HEIGHT );
		if ( comp != null ) {
			setLocationRelativeTo( comp );
		} else {
			setLocation( 200, 200 );
		}

		/* Handle window closing events explicitly */
		setDefaultCloseOperation( DO_NOTHING_ON_CLOSE );
		addWindowListener( new winAdapter() );

		this.task = task;
	}

	public void setVisible(boolean visible) {
		if ( visible && (task != null) ) {
			try {
				Thread th = new Thread(task);
				th.start();
				/* java.awt.Dialog crashes if it is asked to dispose before
				   it is good and ready. But in that case we're done anyway. */
				super.setVisible( visible );
			} catch ( Exception e ) {
				Debug.println( "ExportImportProgressDialog.setVisible: " +
							   e );
				e.printStackTrace();
			}
		} else {
			super.setVisible( visible );
		}
	}

	/**
	 *	Handle incoming event from button.
	 *
	 * @param e event
	 */
	public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( CANCEL ) ) {
			_continue = false;
			if ( _done ) {
				setVisible( false );
				dispose();
				DSUtil.dialogCleanup();
			}
		}
	}

    class Updater implements Runnable {
		Updater( String dn, String msg ) {
		   this.dn = dn;
		   this.msg = msg;
		}
	    public void run() {
			_currentDN.setText( dn );
			if ( msg != null ) {
				_error.setText( msg );
			}
		}
		String dn;
		String msg;
	}

    class Disposer implements Runnable {
		Disposer() {
		}
	    public void run() {
			try {
				Thread.sleep(200);
			} catch ( InterruptedException e ) {
			}
			ExportImportProgressDialog.this.setVisible( false );
			ExportImportProgressDialog.this.dispose();
			DSUtil.dialogCleanup();
		}
	}

    private void updateFields( String dn, String msg ) {
		Updater doUpdate = new Updater( dn, msg );
		try {
//			SwingUtilities.invokeAndWait(doUpdate);
			SwingUtilities.invokeLater(doUpdate);
		} catch ( Exception e ) {
		}
	}

	/**
	 * Called when an entry changes, e.g. on export/import. Return
	 * true to continue, false to stop.
	 */
    public boolean entryChanged( String dn, String msg ) {
		if ( dn == null ) {
			_done = true;
		}
		if ( _continue && (dn != null) ) {
			updateFields( dn, msg );
			return true;
		} else {
			try {
				SwingUtilities.invokeLater( new Disposer() );
			} catch ( Exception e ) {
			}
			return false;
		}
	}

    public boolean entryChanged( String dn ) {
		return entryChanged( dn, null );
	}

	class winAdapter extends WindowAdapter {
	    public void windowClosing(WindowEvent e) {
			Debug.println( "winAdapter.windowClosing: _allowCancel = " +
						   _allowCancel );
			if ( _allowCancel ) {
				_continue = false;
				setVisible( false );
				dispose();
			} else {
			}
		}
    }

    public static void main( String[] args ) {
       try {
            UIManager.setLookAndFeel(
				"com.netscape.management.nmclf.SuiLookAndFeel" );
	   } catch (Exception e) {
		   Debug.println("Cannot load nmc look and feel.");
		   System.exit(-1);
	   }
		ExportImportProgressDialog dlg =
			new ExportImportProgressDialog( new JFrame(), "Export" );
		dlg.show();
	}

	private boolean _continue = true;
	private boolean _done = false;
	private JLabel _currentDN;
	private JLabel _error;
    private Runnable task = null;
	private boolean _allowCancel;
	private static final int PAD = 10;
	private static final int WIDTH = 600;
	private static final int HEIGHT = 150;
	private static final ResourceSet _resource = DSUtil._resource;
	private static final String CANCEL = "cancel";
}
