/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.topology.*;
import com.netscape.admin.dirserv.panel.*;

/**
  * Netscape Directory Server component - Information Panel
  *
  * The information panel for the Netscape Directory Server 4.0
  *
  * @author  rweltman
  * @version %I%, %G%
  * @date	 9/15/97
  */
public class InfoPanel extends JPanel {

    private void addLabel( JPanel bPanel,
						   String lead, String text) {
        GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = gbc.EAST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
		gbc.insets = new Insets( 12, 10, 6, 12 );
		JLabel lLabel =
			new JLabel(_resource.getString("infopanel",
										   lead));
		bPanel.add( lLabel, gbc );
		lLabel = new JLabel(text);
		gbc.anchor = gbc.WEST;        
        gbc.gridwidth = gbc.REMAINDER;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
		bPanel.add( lLabel, gbc );
	}

	/**
	 *	Construct the information panel with the passed in parameters.
	 *
	 * @param	sHost	Host name
	 * @param	iPort	port number
	 * @param	sVersion	version number
	 * @param	sInstallationDate installation date
	 * @param	sAdminURL	administration URL
	 */
	public InfoPanel(IServerObject sServer,
						  String sHost,
						  int iPort,
						  String sVersion,
						  String sInstallationDate,
						  String sAdminURL)
	{
		_serverObject = sServer;

		setLayout( new GridBagLayout() );
        GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets( 16, 12, 4, 16 );

//		setBackground( SystemColor.control );

		RemoteImage iServer = DSUtil.getPackageImage("directm.gif");
		JLabel lTitle =
			new JLabel( _resource.getString("infopanel",
											"NetscapeDirServer"),
						iServer,JLabel.LEFT );
		lTitle.setFont( new Font("TimesRoman", Font.BOLD, 24 ));
		gbc.anchor = gbc.WEST;        
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = gbc.REMAINDER;
		add( lTitle, gbc );

		JPanel bServerPanel =
				new GroupPanel( _resource.getString( _section,
													 "ServerInformation" ) );
		bServerPanel.setLayout( new GridBagLayout() );
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
		add( bServerPanel, gbc );


		addLabel( bServerPanel, "VersionNumber", sVersion );
		addLabel( bServerPanel, "InstallationDate", sInstallationDate );
		addLabel( bServerPanel, "AdminURL", sAdminURL );
        gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 1.0;
		add( new JPanel(), gbc );
	}

	/**
	 *	refresh the information in the panel. We need to get the server
	 * status again and redisplay it.
	 */
	public void refresh()
	{
/*
		if (_serverObject.getServerStatus() == _serverObject.STATUS_STARTED)
		{
			_lStatus.setIcon(_iOn);
			_lStatus.setText( _sServerOn );
		} else
		{
			_lStatus.setIcon(_iOff);
			_lStatus.setText( _sServerOff );
		}
*/
	}

	IServerObject _serverObject;	// this page's parent
	String _section = "infopanel";
	ResourceSet _resource = DSUtil._resource;
}
