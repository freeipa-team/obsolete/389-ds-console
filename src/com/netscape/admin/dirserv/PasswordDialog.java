/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.management.client.console.Console;
import com.netscape.management.client.console.LoginDialog;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.management.client.util.GridBagUtil;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.Help;
import com.netscape.management.nmclf.SuiConstants;
import com.netscape.admin.dirserv.panel.UIFactory;

/**
 * Display this dialog to get a password.
 * This code is entirely taken from
 * com.netscape.management.client.console.LoginDialog. That one is not
 * extensible, and it has an admin URL field which we don't want. This
 * version just has DN and password fields, and no Help button.
 *
 */

public class PasswordDialog extends AbstractDialog
                            implements SwingConstants, SuiConstants {
	/**
	 * constructor
	 *
	 * @param parentFrame parent frame
	 * @param authName User ID
	 */
	public PasswordDialog(JFrame parent, String authName) {
		this(parent, authName,
			 resource.getString("PasswordDialog", "title"));
	}

	/**
	 * constructor which let the caller decides the title
	 *
	 * @param parentFrame parent frame
	 * @param authName User ID
	 * @param title dialog title
	 */
	/**
	 * @param parent Parent container.
	 */
	public PasswordDialog( JFrame parent, String authName, String title ) {
		this(parent, authName, null,
			 resource.getString("PasswordDialog", "title"));
	}

	/**
	 * constructor which let the caller decides the title
	 *
	 * @param parentFrame parent frame
	 * @param authName User ID
	 * @param title dialog title
	 */
	/**
	 * @param parent Parent container.
	 */
	public PasswordDialog( JFrame parent, String authName, String authPassword, String title ) {
		super(parent, title, true, OK | CANCEL);
		getAccessibleContext().setAccessibleDescription(resource.getString("PasswordDialog",
																		   "description"));
		createDialogPanel();
		if (authName != null) {
			_useridField.setText(authName);
		}
		if (authPassword != null) {
			_passwordField.setText(authPassword);
		}

        // Do not allow anonymous bind with empty password/userid
        DocumentListener _emptyFieldListener = new EmptyFieldListener();
        _useridField.getDocument().addDocumentListener(_emptyFieldListener);
        _passwordField.getDocument().addDocumentListener(_emptyFieldListener);
        _useridField.setText(_useridField.getText()); // force empty field check
        
		setMinimumSize(getPreferredSize());
		setResizable(true);
	}

    public void show() {
        _useridField.selectAll();
        super.show();        
    }    
    
	/**
	 * create the actual dialog
	 */
	protected void createDialogPanel() {
		JPanel panel = new JPanel();
		GridBagLayout gridbag = new GridBagLayout();
		panel.setLayout(gridbag);
		commonPanelLayout(panel);
		setComponent(panel);
	}

	protected void commonPanelLayout(JPanel panel) {
		JLabel usernameLabel = new JLabel(resource.getString("PasswordDialog",
															 "dn"));
		usernameLabel.setLabelFor(_useridField);
		GridBagUtil.constrain(panel, usernameLabel, 
							  0, GridBagConstraints.RELATIVE, 
							  1, 1, 0.0, 0.0, 
							  GridBagConstraints.EAST,
							  GridBagConstraints.NONE, 
							  0, 0, 0, DIFFERENT_COMPONENT_SPACE);

		GridBagUtil.constrain(panel, _useridField, 
							  1, GridBagConstraints.RELATIVE, 
							  1, 1, 1.0, 0.0, 
							  GridBagConstraints.NORTHWEST,
							  GridBagConstraints.HORIZONTAL, 
							  0, 0, 0, 0);

		setFocusComponent(_useridField);

		JLabel passwordLabel = new JLabel(resource.getString("PasswordDialog",
															 "password"));
		passwordLabel.setLabelFor(_passwordField);
		GridBagUtil.constrain(panel, passwordLabel, 
							  0, GridBagConstraints.RELATIVE, 
							  1, 1, 0.0, 0.0, 
							  GridBagConstraints.EAST,
							  GridBagConstraints.NONE, 
							  COMPONENT_SPACE, 0, 0,
							  DIFFERENT_COMPONENT_SPACE);

		GridBagUtil.constrain(panel, _passwordField, 
							  1, GridBagConstraints.RELATIVE, 
							  1, 1, 1.0, 0.0, 
							  GridBagConstraints.NORTHWEST,
							  GridBagConstraints.HORIZONTAL, 
							  COMPONENT_SPACE, 0, 0, 0);

		if (_useridField.getText().length() > 0)
			setFocusComponent(_passwordField);
	}

	/**
	 * return the login user name or full dn
	 *
	 * @return return the user id field value
	 */
	public String getUsername() {
		return _useridField.getText();
	}

	/**
	 * return the user password
	 *
	 * @return return the password field value
	 */
	public String getPassword() {
		return _passwordField.getText();
	}

	/**
	 * set the dialog location
	 *
	 * @param parentFrame parent frame
	 */
	protected void setDialogLocation(Frame parentFrame) {
		if (_x > 0 && _y > 0)
			setLocation(_x, _y);
		else
			setLocationRelativeTo(parentFrame);
	}

	/**
	 * invoke help
	 */
	protected void helpInvoked() {
		Help.showContextHelp("login","help");
	}

    // Monitor if input fields are empty
    class EmptyFieldListener implements DocumentListener {
        public void insertUpdate(DocumentEvent e) {
            stateChanged();
        }

        public void changedUpdate(DocumentEvent e) {
            stateChanged();
        }

        public void removeUpdate(DocumentEvent e) {
            stateChanged();
        }

        void stateChanged() {
            boolean passwdEmpty = _passwordField.getText().trim().length() == 0;
            boolean useridEmpty = _useridField.getText().trim().length() == 0;
            setOKButtonEnabled( !passwdEmpty && !useridEmpty);
        }
    }

	public static void main( String[] args ) {
		Debug.setTrace( true );
       try {
            UIManager.setLookAndFeel(
				"com.netscape.management.nmclf.SuiLookAndFeel" );
	   } catch (Exception e) {
		   System.err.println("Cannot load nmc look and feel.");
		   System.exit(-1);
	   }
 		PasswordDialog dlg = new PasswordDialog( new JFrame(),
 												 "cn=Directory Manager" );
		dlg.show();
		System.out.println( "Password = " + dlg.getPassword() );
		System.exit( 0 );
	}

	static ResourceSet resource = /* DSUtil._resource; */
		new ResourceSet("com.netscape.admin.dirserv.dirserv");
	JTextField _useridField = new JTextField(22);
	JTextField _passwordField = new JPasswordField(22);
	protected static String _resourcePrefix = "login";
	int _x = -1;
	int _y = -1;
}
