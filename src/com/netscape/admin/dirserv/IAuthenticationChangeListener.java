/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv;


/**
 * IAuthenticationChangeListener
 * For object wishing to be notified when authentication changes.
 *
 * @version 1.0
 * @author rweltman
 **/
public interface IAuthenticationChangeListener {
	/**
	 * Called when authentication changes.
	 *
	 * @param oldAuth Previous authentication DN.
	 * @param newAuth New authentication DN.
	 */
    public void authenticationChanged( String oldAuth,
									   String newAuth,
									   String oldPassword,
									   String newPassword );
}
