/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.cosedit;

import java.util.*;
import java.awt.*;
import java.awt.event.*; 

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import netscape.ldap.*;

import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.*;
import com.netscape.management.client.components.*;
import com.netscape.management.nmclf.*;

import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DefaultResourceModel;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.admin.dirserv.propedit.NewAttributeDialog;

/**
 * ResEditorCosAttributes is a plugin for the ResourceEditor.
 * It is used to edit information specific to nscosdefinition entries.
 */
public class ResEditorCosAttributes extends DefaultResEditorPage
                                    implements ListSelectionListener,
									           ActionListener,
											   SuiConstants,
											   Observer
{

	public ResEditorCosAttributes() {
		super();
		
		// Fetch the labels for attribute qualifier.
		// Note: array indices match the values DEFAULT, OVERRIDE...
		_qualifierLabels = new Vector(8);
		_qualifierLabels.addElement(_resource.getString(_section, "qualifier-default"));
		_qualifierLabels.addElement(_resource.getString(_section, "qualifier-default-merge-schemes"));
		_qualifierLabels.addElement(_resource.getString(_section, "qualifier-override"));
		_qualifierLabels.addElement(_resource.getString(_section, "qualifier-override-merge-schemes"));
		_qualifierLabels.addElement(_resource.getString(_section, "qualifier-operational"));
		_qualifierLabels.addElement(_resource.getString(_section, "qualifier-operational-merge-schemes"));
		_qualifierLabels.addElement(_resource.getString(_section, "qualifier-operational-default"));
		_qualifierLabels.addElement(_resource.getString(_section, "qualifier-operational-default-merge-schemes"));
	}
	
	
    /**
     * Implements the IResourceEditorPage interface.
     */
	public void initialize(ResourcePageObservable observable, ResourceEditor parent) {
		
		_id = _resource.getString(_section, "id");
		_resourceEditor = parent;
		_observable = observable;
		_info = observable.getConsoleInfo();

		// Build and layout components if not already done
		if (_table == null) {
			_table = new Table();
			_table.setPreferredScrollableViewportSize(new Dimension(10, 10));
        	_table.getSelectionModel().addListSelectionListener(this);
			_tableModel = new CosAttributesTableModel();
        	_table.setModel(_tableModel);
			// Tune the cell editor for the qualifier column.
			JComboBox combox = new JComboBox(_qualifierLabels);
			TableCellEditor editor = new DefaultCellEditor(combox);
			_table.getColumnModel().getColumn(1).setCellEditor(editor);

        	_addButton = UIFactory.makeJButton(this, _section, "addButton", _resource);        	
        	_removeButton = UIFactory.makeJButton(this, _section, "removeButton", _resource);     	

        	JButtonFactory.resizeGroup(_addButton, _removeButton);

        	Box buttonBox = new Box(BoxLayout.X_AXIS);
        	buttonBox.add(Box.createHorizontalGlue());
        	buttonBox.add(_addButton);
        	buttonBox.add(Box.createHorizontalStrut(SuiLookAndFeel.SEPARATED_COMPONENT_SPACE));
        	buttonBox.add(_removeButton);

	        layoutComponents();
		}

		// Load values from observable
		loadFromObservable();
		valueChanged(null); // To initialize the state of _removeButton
    }
    
    
    /**
     * Layout the components
     */
    void layoutComponents() {

		JPanel panel = new JPanel();
    	GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);

        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.BOTH;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(VERT_WINDOW_INSET, HORIZ_WINDOW_INSET, 0, HORIZ_WINDOW_INSET);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
        
        JLabel l = new JLabel(_resource.getString(_section, "label"));
        panel.add(l) ;
		l.setLabelFor(_table);
        gbl.setConstraints(l, gbc);

		JScrollPane scrollPane = new JScrollPane(_table);
        scrollPane.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));
		panel.add(scrollPane);
        gbc.gridy++;
        gbc.weighty = 1;
        gbc.insets = new Insets(0, HORIZ_WINDOW_INSET, 0, HORIZ_WINDOW_INSET);
        gbl.setConstraints(scrollPane, gbc);

        Box box = new Box(BoxLayout.X_AXIS);
        box.add(Box.createHorizontalGlue());
        box.add(_addButton);
        box.add(Box.createHorizontalStrut(SEPARATED_COMPONENT_SPACE));
        box.add(_removeButton);
       
		panel.add(box);
        gbc.gridy++;
        gbc.weighty = 0;
        gbc.fill = gbc.NONE;
        gbc.anchor = gbc.EAST;
        gbc.insets = new Insets(VERT_WINDOW_INSET, HORIZ_WINDOW_INSET, VERT_WINDOW_INSET, HORIZ_WINDOW_INSET);
        gbl.setConstraints(box, gbc);
		
		JScrollPane sp = new JScrollPane(panel);
		sp.setBorder(null);

		setLayout(new BorderLayout());
		add("Center", sp);
	}

    /**
     * Implements the IResourceEditorPage interface.
     */
	public String getID() {
		return _id;
	}


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public boolean save(ResourcePageObservable observable) throws Exception {
		// Make a String vector from _attributeVector
		Vector attrValues = new Vector();
		Enumeration e = _attributeVector.elements();
		while (e.hasMoreElements()) {
			CosAttributeRec r = (CosAttributeRec)e.nextElement();
			attrValues.add(r.toString());
		}
		
		// Update _observable
		// Note: attrValues should contains at least one item
		// since isComplete() is supposed to have return true.
		_observable.replace("cosAttribute", attrValues);

		return true;
	}


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public boolean isComplete() {
		return (_attributeVector.size() >= 1);
	}


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public String getDisplayName() {
        return _id;
    }


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public void help()
	{
		DSUtil.help("configuration-cos-attributes-help");
	}


    /**
	 * Implements the ActionListener interface.
	 */
    public void actionPerformed(ActionEvent e) 
    {
    	if (e.getSource() == _addButton) {
        	actionAdd();
        }
        else if (e.getSource() == _removeButton) {
        	actionRemove();
        }
	}


    /**
     * Process a click on Add
     */
    void actionAdd() {
		_resourceEditor.setBusyCursor(true);
		
		//
		// Build the dialog if needed
		//
		if (_attributeDialog == null) {
		
			_attributeDialog = new NewAttributeDialog(_resourceEditor.getFrame(),
			                                       _resourceEditor.getConsoleInfo());
			_attributeDialog.setTitle(_resource.getString(_section, "add-attribute-title"));
			_attributeDialog.getAccessibleContext().setAccessibleDescription(_resource.getString(_section,
																								 "add-attribute-description"));
		}
		
		//
		// Build a hashtable containing the attributes already used by
		// this cos.
		//
		Hashtable usedTable = new Hashtable(_attributeVector.size()+1);
		Enumeration e = _attributeVector.elements();
		while (e.hasMoreElements()) {
			CosAttributeRec r = (CosAttributeRec)e.nextElement();
			usedTable.put(r.name, r.name);
		}

		// Update the 'used table' and show the dialog
		_attributeDialog.setUsed(usedTable);
		_attributeDialog.show();
		_attributeDialog.dispose();
		
		
		//
		// Get the selected attributes and update _attributeVector.
		//
		if ( ! _attributeDialog.isCancel() ) {
			String[] names = _attributeDialog.getSelectedAttributes();
			if ( names != null ) {
				for (int i=0; i< names.length; i++) {
					CosAttributeRec r = new CosAttributeRec(names[i]);
					_attributeVector.addElement(r);
					_tableModel.fireTableRowInserted();
				}
			}
		}
		
		_resourceEditor.setBusyCursor(false);
    }

 
  
    /**
     * Process a click on Remove
     */
    void actionRemove() {
		// Note: we assume here that the result of
		// JTable.getSelectedRows() is sorted ie
		// the lowest index is the first, the highest
		// the last.
		int[] indices = _table.getSelectedRows();
		for (int i = indices.length-1; i >= 0; i--) {
			_attributeVector.removeElementAt(indices[i]);
			_tableModel.fireTableRowDeleted(indices[i]);
		}
	}
    

    /**
     * Implements ListSelectionListener.
     * Called when the user clicks in the table.
     * We update the tooltip text.
     */
    public void valueChanged(ListSelectionEvent e) {
    	// Note: initialize() invokes this method with e == null        
        
        // _removeButton is disabled if the selection is empty: if we use
		// the tab and the table gets the focus JTable.getSelectedRowCount() is 1
		// (even if the row count is 0 !!)
        _removeButton.setEnabled((_table.getSelectedRowCount() > 0) &&
								 (_table.getRowCount() > 0));
    }
    
	
	/**
	 * Implements Observer.
	 * Only to workaround a console sdk bug.
	 * To be remove when fixed.
	 */
	public void update(Observable observable, Object arg) {
	
	}

	/**
	 * Load _attributeVector from _observable.
	 */
	void loadFromObservable() {
	
		_attributeVector.removeAllElements();

		Vector v = _observable.get("cosAttribute");
		if (v != null) {
			Enumeration e = v.elements();
			while (e.hasMoreElements()) {
				String s = (String)e.nextElement();
				_attributeVector.addElement(new CosAttributeRec(s));
			}
		}
	}
    
	
	/**
	 * Data model which expose a two columns table.
	 *	- first column is the attribute name
	 *  - second column is the override flag
	 */
    class CosAttributesTableModel extends AbstractTableModel {
    
        public String getColumnName(int col) {
        	if (col == 0)
            	return _resource.getString(_section, "header-name");
            else // if (col == 1)
            	return _resource.getString(_section, "header-qualifier");
        }
        
        public int getColumnCount() {
    	    return 2;
        }
        
	    public int getRowCount() {
    	    return _attributeVector.size();
        }

		// For column 1 (qualifier), we map the CosAttributeRec.qualifier
		// into a displayable label (_qualifierLabels).
        public Object getValueAt(int row, int col) {
			Object result = null;
			if (row < _attributeVector.size()) {
				CosAttributeRec rec = (CosAttributeRec)_attributeVector.elementAt(row);
				switch(col) {
					case 0:
            			result = rec.name;
						break;
            		case 1:
            			result = (String)_qualifierLabels.elementAt(rec.qualifier);
						break;
				}
			}
			else {
				Debug.println(0, "ResEditorCosAttributes.CosAttributesTableModel.getValueAt: ignored row = " +row);
			}
			return result;
        }

        public Class getColumnClass(int c) {
           return String.class;
        }
		

        public boolean isCellEditable(int row, int col) {
			return (col == 1);
        }

		// We map the selected qualifier label into DEFAULT, OPERATIONAL...
        public void setValueAt(Object value, int row, int col) {
			CosAttributeRec rec = (CosAttributeRec)_attributeVector.elementAt(row);
			if ((col == 1) && (value instanceof String)) {
				int i = _qualifierLabels.indexOf(value);
				if (i != -1)
					rec.qualifier = i;
				else { // Vegra
					rec.qualifier = rec.DEFAULT;
					Debug.println(0, "CosAttributesTableModel.setValueAt: ignored value " + value);
				}
			}
			else{
				Debug.println(0, "CosAttributesTableModel.setValueAt: ignored change in col" + col);
			}
        }        
		
		public void fireTableRowInserted() {
			int lastRow = _attributeVector.size() - 1;
			fireTableRowsInserted(lastRow, lastRow);
		}
		
		public void fireTableRowDeleted(int row) {
			fireTableRowsDeleted(row, row);
		}
    }

	// State variables
    ResourceEditor _resourceEditor;
    ResourcePageObservable _observable;
    ConsoleInfo _info;
    String _id;
	Vector _attributeVector = new Vector(); // of CosAttributeRec
	NewAttributeDialog _attributeDialog;
	CosAttributesTableModel _tableModel;

	// Components
	JTable _table;
    JButton _addButton;
    JButton _removeButton;

	// I18N
	static Vector _qualifierLabels;
	static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "cosAttributesPage";
}




/**
 * A record grouping the name of an attribute and
 * its various flags.
 */
class CosAttributeRec {
	public static final int DEFAULT     = 0;
	public static final int DEFAULT_MERGE_SCHEMES = 1;
	public static final int OVERRIDE    = 2;
	public static final int OVERRIDE_MERGE_SCHEMES = 3;
	public static final int OPERATIONAL = 4;
	public static final int OPERATIONAL_MERGE_SCHEMES = 5;
	public static final int OPERATIONAL_DEFAULT = 6;
	public static final int OPERATIONAL_DEFAULT_MERGE_SCHEMES = 7;
	
	String name;
	int qualifier;

	public CosAttributeRec(String s) {
		StringTokenizer stk = new StringTokenizer(s, " ");
		while (stk.hasMoreElements()) {
			String token = stk.nextToken();
			if (name == null) 
				name = token;
			else if (token.equalsIgnoreCase("default"))
				qualifier = DEFAULT;
			else if (token.equalsIgnoreCase("override"))
				qualifier = OVERRIDE;
			else if (token.equalsIgnoreCase("operational"))
				qualifier = OPERATIONAL;
			else if (token.equalsIgnoreCase("operational-default"))
				qualifier = OPERATIONAL_DEFAULT;
			else if (token.equalsIgnoreCase("merge-schemes")){
				// Now check if we already have a qualifier set, and modify it if necessary
				if(qualifier == DEFAULT){
					qualifier = DEFAULT_MERGE_SCHEMES;
				} else if(qualifier == OVERRIDE){
					qualifier = OVERRIDE_MERGE_SCHEMES;
				} else if(qualifier == OPERATIONAL){
					qualifier = OPERATIONAL_MERGE_SCHEMES;
				} else if(qualifier == OPERATIONAL_DEFAULT){
					qualifier = OPERATIONAL_DEFAULT_MERGE_SCHEMES;
				}
			} else {
				Debug.println(0, "ResEditorCosAttributes.CosAttributeRec: " + 
					             "ignored invalid cosAttribute qualifier (" + token + ")");
			}
		}
	}

	public String toString() {
		StringBuffer sb = new StringBuffer(name);
		switch(qualifier) {
			case DEFAULT_MERGE_SCHEMES:
				sb.append(" merge-schemes");
				break;
			case OVERRIDE:
				sb.append(" override");
				break;
			case OVERRIDE_MERGE_SCHEMES:
				sb.append(" override merge-schemes");
				break;
			case OPERATIONAL:
				sb.append(" operational");
				break;
			case OPERATIONAL_MERGE_SCHEMES:
				sb.append(" operational merge-schemes");
				break;
			case OPERATIONAL_DEFAULT:
				sb.append(" operational-default");
				break;
			case OPERATIONAL_DEFAULT_MERGE_SCHEMES:
				sb.append(" operational-default merge-schemes");
				break;
			// case DEFAULT: nothing needed
		}

		return sb.toString();
	}
};
