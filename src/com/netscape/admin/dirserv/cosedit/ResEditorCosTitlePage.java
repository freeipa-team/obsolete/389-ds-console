/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.cosedit;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import com.netscape.management.nmclf.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.ug.ResourcePageObservable;
import javax.swing.*;
import com.netscape.admin.dirserv.DSUtil;

/**
 * ResEditorCosTitlePage is used when editing a role entry. This panel
 * occupies the top portion of the ResourceEditor.
 *
 * @see ResourceEditor
 */
public class ResEditorCosTitlePage extends JPanel implements Observer
{
    /**
	 * Constructor
	 *
	 * @param observable  the observable object
	 */
	public ResEditorCosTitlePage(ResourcePageObservable observable) {
		super(true);

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		setLayout(layout);

		_cosName = new SuiTitle("");
		_cosName.setText(observable.get(CN_ATTR, 0));
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.insets = new Insets(0,SuiLookAndFeel.COMPONENT_SPACE,0,SuiLookAndFeel.COMPONENT_SPACE);
		c.anchor = GridBagConstraints.NORTHWEST;
		c.weightx = 1;
		c.weighty = 0;
		layout.setConstraints(_cosName,c);
		add(_cosName);

		_cosIcon = new JLabel();
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight=1;
		c.weightx = 0;
		c.insets = new Insets(SuiLookAndFeel.COMPONENT_SPACE,SuiLookAndFeel.COMPONENT_SPACE,SuiLookAndFeel.COMPONENT_SPACE,SuiLookAndFeel.COMPONENT_SPACE);
		c.anchor = GridBagConstraints.CENTER;
		layout.setConstraints(_cosIcon,c);
		add(_cosIcon); 
		
		setupIcon(COSSUPERDEFINITION_CLASS);
	}

    /**
	 * Sets the name of the group.
	 *
	 * @param name  the name of the group
	 */
	public void setName(String name) {
		_cosName.setText(name);
	}

    /**
	 * Implements the Observer interface. Updates the information in
	 * this pane when called.
	 *
	 * @param o    the observable object
	 * @param arg  argument
	 */
	public void update(Observable o, Object arg) {
		ResourcePageObservable observable = (ResourcePageObservable)o;

		Debug.println(6, "ResEditorCosTitlePage.update: o = " + o + ", arg = " + arg);
		
		if (arg != null) {
			String attrName = arg.toString();
			if (attrName.equals(CN_ATTR)) {
				_cosName.setText(observable.get(CN_ATTR, 0));
			}
			else if (attrName.equals(OBJECTCLASS_ATTR)) {
				Vector objectClass = observable.get(OBJECTCLASS_ATTR);
				String className = null;
				if (DSUtil.indexOfIgnoreCase(objectClass, COSSUPERDEFINITION_CLASS) != -1)
					className = COSSUPERDEFINITION_CLASS;
				
				setupIcon(className);
			}
		}
	}


	/**
	 * The sequence which setup the icon according
	 * a specific object class (nsmanagedroledefinition, ...)
	 */
	void setupIcon(String className) {
		String iconName = null;
        if (className != null) {
			iconName = DSUtil._resource.getString(_section, className + "-icon-24");
        }
		RemoteImage imageIcon = null ;
		if (iconName != null) {
        	imageIcon = DSUtil.getPackageImage( iconName );
		}
		if (imageIcon != null) {
			_cosIcon.setIcon(imageIcon);
		}
	}
	
	
	// Components
	JLabel _cosName;
	JLabel _cosIcon;


	// LDAP schema
	private static final String CN_ATTR = "cn";
	private static final String OBJECTCLASS_ATTR = "objectclass";
	private static final String COSSUPERDEFINITION_CLASS = "cossuperdefinition";
	
	// I18N
	private static final String _section = "EntryObject";
}
