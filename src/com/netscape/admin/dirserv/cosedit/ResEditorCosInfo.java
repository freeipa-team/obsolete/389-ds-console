/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.cosedit;

import java.util.*;
import java.awt.*;
import java.awt.event.*; 

import javax.swing.*;

import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.*;
import com.netscape.management.nmclf.*;

import com.netscape.admin.dirserv.DSUtil;

/**
 * ResEditorCosInfo is a plugin for the ResourceEditor.
 * It is used to edit information specific to nscosdefinition entries.
 */
public class ResEditorCosInfo extends DefaultResEditorPage implements Observer
{
	/**
	 * Used to notify the ResourcePageObservable when a value has changed.
	 * Note that this updates all observers.
	 */
	FocusAdapter _focusAdaptor = new FocusAdapter() {
		public void focusLost(FocusEvent e) {
			if (_observable == null)
			{
				return;
			}
			Component src = e.getComponent();
			if (src == _cosName)
			{
				_observable.replace(ATTR_CN, _cosName.getText());
			}
			else if (src == _cosDescription)
			{
				Vector vTmp = new Vector();
				StringTokenizer st=new StringTokenizer(_cosDescription.getText(),"\n\r");
				while(st.hasMoreTokens())
				{
					vTmp.addElement(st.nextElement());
				}
				_observable.replace(ATTR_DESCRIPTION, vTmp);
			}
		}
	};


    /**
     * Implements the IResourceEditorPage interface.
     */
	public void initialize(ResourcePageObservable observable, ResourceEditor parent) {
		_id = _resource.getString(_section, "id");
		_resourceEditor = parent;
		_observable = observable;
		_info = observable.getConsoleInfo();

		// Build and layout components if not already done
		if (_cosName == null) {
			_cosName = new JTextField();
			_cosDescription = new UGTextArea();

			_cosName.addFocusListener(_focusAdaptor);
			_cosDescription.addFocusListener(_focusAdaptor);

	        layoutComponents();
		}

		// Load values from observable
 		_oldCosName = observable.get(ATTR_CN, 0);
		_cosName.setText(_oldCosName);

		_cosDescription.setText("");
		Vector vDesc= observable.get(ATTR_DESCRIPTION);
		Enumeration eDesc= vDesc.elements();
        if (eDesc.hasMoreElements()) {
            _cosDescription.append((String)eDesc.nextElement());
        }
        while(eDesc.hasMoreElements()) {
            _cosDescription.append("\n" + eDesc.nextElement());
        }

    }
    
    
    /**
     * Layout the components
     */
    void layoutComponents() {

		JLabel infoLabel = new JLabel(_resource.getString(_section,"required"));
		JLabel nameLabel = new JLabel(_resource.getString(_section, "name"), SwingConstants.RIGHT);
		infoLabel.setLabelFor(nameLabel);
		nameLabel.setLabelFor(_cosName);
		JLabel descriptionLabel = new JLabel(_resource.getString(_section, "description"), SwingConstants.RIGHT);
		descriptionLabel.setLabelFor(_cosDescription);
		JLabel blankLabel = new JLabel("");	// Prevents components of this panel from centering

        JPanel p = new JPanel(new GridBagLayout());
		int y = 0;
		GridBagUtil.constrain(p, nameLabel,
                              0, y,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                              SuiLookAndFeel.VERT_WINDOW_INSET, SuiLookAndFeel.HORIZ_WINDOW_INSET, 0, 0);
		GridBagUtil.constrain(p, _cosName,
                              1, y++,
                              GridBagConstraints.REMAINDER, 1, 1.0, 0.0,
                              GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                              SuiLookAndFeel.VERT_WINDOW_INSET, SuiLookAndFeel.DIFFERENT_COMPONENT_SPACE,
                              0, SuiLookAndFeel.HORIZ_WINDOW_INSET);

		GridBagUtil.constrain(p, descriptionLabel,
                              0, y,
                              1, 1, 0.0, 0.0,
                              GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                              SuiLookAndFeel.COMPONENT_SPACE, SuiLookAndFeel.HORIZ_WINDOW_INSET, 0, 0);
    	GridBagUtil.constrain(p, _cosDescription,
                              1, y++,
                              GridBagConstraints.REMAINDER, 1, 1.0, 0.0,
                              GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                              SuiLookAndFeel.COMPONENT_SPACE, SuiLookAndFeel.DIFFERENT_COMPONENT_SPACE,
                              0, SuiLookAndFeel.HORIZ_WINDOW_INSET);

		GridBagUtil.constrain(p, infoLabel,
                              1, y++,
                              GridBagConstraints.REMAINDER, 1, 1.0, 1.0,
                              GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                              SuiLookAndFeel.VERT_WINDOW_INSET, SuiLookAndFeel.HORIZ_WINDOW_INSET,
                              0, SuiLookAndFeel.HORIZ_WINDOW_INSET);

		JScrollPane sp = new JScrollPane(p);
        sp.setBorder(null);

		setLayout(new BorderLayout());
        add("Center", sp);

	}

    /**
	 * Implements the Observer interface. Updates the fields when notified.
	 */
	public void update(Observable o, Object arg) {
		if ((o instanceof ResourcePageObservable) == false)
		{
			return;
		}
		ResourcePageObservable observable = (ResourcePageObservable)o;
		if (arg instanceof String)
		{
			String argString = (String) arg;
			if (argString.equalsIgnoreCase(ATTR_CN))
			{
				_cosName.setText(observable.get(ATTR_CN, 0));
			}
			else if (argString.equalsIgnoreCase(ATTR_DESCRIPTION))
			{
				_cosDescription.setText("");
				Vector vDesc= observable.get(ATTR_DESCRIPTION);
				Enumeration eDesc= vDesc.elements();
                if (eDesc.hasMoreElements()) {
                    _cosDescription.append((String)eDesc.nextElement());
                }
				while(eDesc.hasMoreElements()) {
					_cosDescription.append("\n" + eDesc.nextElement());
				}
			}
		}
	}

    /**
     * Implements the IResourceEditorPage interface.
     */
	public String getID() {
		return _id;
	}


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public String getDisplayName() {
        return _id;
    }


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public boolean save(ResourcePageObservable observable) throws Exception {
        if (_cosName.getText().equals(_oldCosName) == false) {
            if (_cosName.getText().trim().length() == 0) {
                observable.delete(ATTR_CN, _oldCosName);
                _oldCosName = "";
            }
            else {
                String newCosName = _cosName.getText().trim();
                observable.replace(ATTR_CN, newCosName);
                _oldCosName = newCosName;
            }
        }

		Vector vTmp=new Vector();
		StringTokenizer st=new StringTokenizer(_cosDescription.getText(),"\n\r");
		while(st.hasMoreTokens())
		{
			vTmp.addElement(st.nextElement());
		}
		if (vTmp.size()==0)
		{
			observable.delete(ATTR_DESCRIPTION);
		}
		else
		{
			observable.replace(ATTR_DESCRIPTION, vTmp);
		}
		return true;
	}


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public boolean isComplete() {
		boolean yes = (_cosName.getText().trim().length() >= 1);
		
		if (!yes) {
			JOptionPane.showMessageDialog(null,
				  _resource.getString(_section,"IncompleteText"),
				  _resource.getString(_section,"IncompleteTitle"),
				  JOptionPane.ERROR_MESSAGE);
			ModalDialogUtil.sleep();
		}
		
		return yes;
	}


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public void help()
	{
		DSUtil.help("configuration-cos-info-help");
	}



	// State variables
    ResourceEditor _resourceEditor;
    ResourcePageObservable _observable;
    ConsoleInfo _info;
    String _id;
	String _oldCosName;

	// Components
	JTextField _cosName;
	JTextArea  _cosDescription;

	// I18N
	static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "cosInfoPage";

	// LDAP schema
    static final String ATTR_CN = "cn";
    static final String ATTR_DESCRIPTION = "description";
}
