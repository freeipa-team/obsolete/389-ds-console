/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.cosedit;

import java.util.*;
import java.awt.*;
import java.awt.event.*; 

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;

import netscape.ldap.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.*;
import com.netscape.management.client.components.*;
import com.netscape.management.nmclf.*;

import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DefaultResourceModel;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.admin.dirserv.propedit.NewAttributeDialog;

/**
 * ResEditorCosTemplate is a plugin for the ResourceEditor.
 * It is used to edit information specific to nscosdefinition entries.
 */
public class ResEditorCosTemplate extends DefaultResEditorPage
                                  implements ActionListener, SuiConstants, Observer
{

	public ResEditorCosTemplate() {
		super();
	}
	
	
    /**
     * Implements the IResourceEditorPage interface.
     */
	public void initialize(ResourcePageObservable observable, ResourceEditor parent) {
		
		_id = _resource.getString(_section, "id");
		_resourceEditor = parent;
		_observable = observable;

		// Build and layout components if not already done
		if (_pointerRadioButton == null) {
			_pointerRadioButton = new JRadioButton(_resource.getString(_section, "pointerRadioButton"));
			_indirectRadioButton = new JRadioButton(_resource.getString(_section, "indirectRadioButton"));
			_classicRadioButton = new JRadioButton(_resource.getString(_section, "classicRadioButton"));
			_pointerRadioButton.addActionListener(this);
			_indirectRadioButton.addActionListener(this);
			_classicRadioButton.addActionListener(this);
			_dnPane = new JPanel();
			_attrPane = new JPanel();
			_dnTextField = new JTextField();			
			_attrTextField = new JTextField();
			_dnBrowseButton = UIFactory.makeJButton(this, _section, "dnBrowseButton", _resource);			
			_attrChangeButton = UIFactory.makeJButton(this, _section, "attrChangeButton", _resource);			
		
			// TO BE REMOVED WHEN FULLY IMPLEMENTED
        	layoutComponents();
		}

		// Load values from observable
		loadFromObservable();
		updateComponentState();
    }
    
    
    /**
     * Layout the components
     */
    void layoutComponents() {
	
		// Enclose all the radio buttons
		JPanel p = new GroupPanel(_resource.getString(_section, "label"), true);
		p.setLayout(new GridLayout(3, 1));
		p.add(_pointerRadioButton);
		p.add(_indirectRadioButton);
		p.add(_classicRadioButton);
		
		ButtonGroup g = new ButtonGroup();
		g.add(_pointerRadioButton);
		g.add(_indirectRadioButton);
		g.add(_classicRadioButton);
		
		// Layout this
		JPanel panel = new JPanel();
    	GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);

        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(VERT_WINDOW_INSET, HORIZ_WINDOW_INSET, 0, HORIZ_WINDOW_INSET);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
        
		panel.add(p);
		gbl.setConstraints(p, gbc);

		panel.add(_dnPane);
		gbc.gridy++;
        gbc.insets = new Insets(SEPARATED_COMPONENT_SPACE, HORIZ_WINDOW_INSET, 0, HORIZ_WINDOW_INSET);
		gbl.setConstraints(_dnPane, gbc);
		
		panel.add(_attrPane);
		gbc.gridy++;
        gbc.insets = new Insets(SEPARATED_COMPONENT_SPACE, HORIZ_WINDOW_INSET, VERT_WINDOW_INSET, HORIZ_WINDOW_INSET);
		gbl.setConstraints(_attrPane, gbc);

		Component glue = Box.createGlue();
		panel.add(glue);
		gbc.weighty = 1;
		gbl.setConstraints(glue, gbc);

		layoutDnPane();
		layoutAttrPane();
		
		JScrollPane scrollPane = new JScrollPane(panel);
		scrollPane.setBorder(null);

		setLayout(new BorderLayout());
		add("Center", scrollPane);
	}
	
	/**
	 * Layout the components of _dnPane
	 */
	void layoutDnPane() {

		String t = _resource.getString(_section, "DN");
		_dnTextField.getAccessibleContext().setAccessibleDescription(t);
		TitledBorder tb = new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED),t);
		tb.setTitleColor(BlankPanel.ORIG_COLOR);
		EmptyBorder eb = new EmptyBorder(COMPONENT_SPACE, SEPARATED_COMPONENT_SPACE, COMPONENT_SPACE, SEPARATED_COMPONENT_SPACE);
		CompoundBorder cb = new CompoundBorder(tb, eb);
        _dnPane.setBorder(cb);

	    GridBagLayout gbl = new GridBagLayout();
        _dnPane.setLayout(gbl);

        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 0;
        gbc.weighty    = 0;
        gbc.fill       = gbc.BOTH;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(0, 0, 0, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;

        gbc.weightx = 1;
		_dnPane.add(_dnTextField);
		gbl.setConstraints(_dnTextField, gbc);

        gbc.gridx++;
        gbc.weightx = 0;
        gbc.insets = new Insets(0, COMPONENT_SPACE, 0, 0);
		_dnPane.add(_dnBrowseButton);
		gbl.setConstraints(_dnBrowseButton, gbc);
	}
	
	/**
	 * Layout the components of _attrPane
	 */
	void layoutAttrPane() {

		String t = _resource.getString(_section, "attrName");
		_attrTextField.getAccessibleContext().setAccessibleDescription(t);
		TitledBorder tb = new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED),t);
		EmptyBorder eb = new EmptyBorder(COMPONENT_SPACE, SEPARATED_COMPONENT_SPACE, COMPONENT_SPACE, SEPARATED_COMPONENT_SPACE);
		CompoundBorder cb = new CompoundBorder(tb, eb);
		tb.setTitleColor(BlankPanel.ORIG_COLOR);
        _attrPane.setBorder(cb);

    	GridBagLayout gbl = new GridBagLayout();
		_attrPane.setLayout(gbl);
		
        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 0;
        gbc.weighty    = 0;
        gbc.fill       = gbc.BOTH;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(0, 0, 0, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
		
        gbc.weightx = 1;
		_attrPane.add(_attrTextField);
		gbl.setConstraints(_attrTextField, gbc);
		
        gbc.gridx++;
        gbc.weightx = 0;
        gbc.insets = new Insets(0, COMPONENT_SPACE, 0, 0);
		_attrPane.add(_attrChangeButton);
		gbl.setConstraints(_attrChangeButton, gbc);

	}

    /**
     * Implements the IResourceEditorPage interface.
     */
	public String getID() {
		return _id;
	}


    /**
	 * Implements the IResourceEditorPage interface.
	 * Here we update the _observable according the input components.
	 */
	public boolean save(ResourcePageObservable observable) throws Exception {
	
		// Get and trim the text from the textfields
		String dn = _dnTextField.getText().trim();
		String attrName = _attrTextField.getText().trim();
		int subType = findSubTypeFromRadioButtons();
		
		// Remove any subtype-specific attributes from the _observable
		_observable.delete("cosTemplateDn");
		_observable.delete("cosIndirectSpecifier");
		_observable.delete("cosSpecifier");
		
		// Add subtype-specific attributes to _observable
		switch(subType) {
			case POINTER_SUBTYPE:
				if (dn.length() >= 1) {
					_observable.add("cosTemplateDn", dn);
				}
				break;

			case INDIRECT_SUBTYPE:
				if (attrName.length() >= 1) {
					_observable.add("cosIndirectSpecifier", attrName);
				}
				break;

			case CLASSIC_SUBTYPE:
				if (dn.length() >= 1) {
					_observable.add("cosTemplateDn", dn);
				}
				if (attrName.length() >= 1) {
					_observable.add("cosSpecifier", attrName);
				}
				break;
				
			default: // Should not happen
				Debug.println("ResEditorCosTemplate.save: invalid subtype");
				break;
		}

		// Update the objectclass of _observable.
		changeObservableFromSubType(subType);
		
		return true;
	}


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public boolean isComplete() {
		return true;
	}


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public String getDisplayName() {
        return _id;
    }


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public void help()
	{
		DSUtil.help("configuration-cos-template-help");
	}


    /**
	 * Implements the ActionListener interface.
	 */
	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();
		
		if (source == _dnBrowseButton) {
			actionBrowseDirectory();
		}
		else if (source == _attrChangeButton) {
			actionChangeAttribute();
		}
		else { // source is _xxxRadioButton
			updateComponentState();
		}
	}
	

	/**
	 * Invoked when the user click on the Browse button
	 */
	public void actionBrowseDirectory() {
		JFrame frame = _resourceEditor.getFrame();		
		LDAPConnection ldc = _resourceEditor.getConsoleInfo().getLDAPConnection();
		DirBrowserDialog dlg = new DirBrowserDialog(frame, ldc);
		dlg.setLocationRelativeTo(_dnBrowseButton);
		dlg.getAccessibleContext().setAccessibleDescription(_resource.getString(_section,
																				"browse-description"));
																				
		dlg.show();
		dlg.dispose();
		if ( ! dlg.isCancel() ) {
			_dnTextField.setText(dlg.getSelectedDN());
		}
	}
	

	/**
	 * Invoked when the user click on the Change button
	 */
	public void actionChangeAttribute() {
		_resourceEditor.setBusyCursor(true);
		
		//
		// Build (if needed) and show the dialog
		//
		if (_attributeDialog == null) {
		
			_attributeDialog = new NewAttributeDialog(_resourceEditor.getFrame(),
			                                       _resourceEditor.getConsoleInfo());
			_attributeDialog.setSingleSelection(true);
			_attributeDialog.setTitle(_resource.getString(_section,
														  "change-attribute-title"));
			_attributeDialog.getAccessibleContext().setAccessibleDescription(_resource.getString(_section,
																								 "change-attribute-description"));
		}
		_attributeDialog.show();
		_attributeDialog.dispose();
		
		//
		// Get the selected attributes and update _attributeVector.
		//
		if ( ! _attributeDialog.isCancel() ) {
			String[] names = _attributeDialog.getSelectedAttributes();
			if ( names != null ) {
				_attrTextField.setText(names[0]);
			}
		}
		
		_resourceEditor.setBusyCursor(false);
	}
	
	
	/**
	 * Set the state of the text fields according 
	 * the state of the radio buttons.
	 */
	void updateComponentState() {

		switch(findSubTypeFromRadioButtons()) {
			case POINTER_SUBTYPE:
				_dnPane.setVisible(true);
				_attrPane.setVisible(false);
				break;

			case INDIRECT_SUBTYPE:
				_dnPane.setVisible(false);
				_attrPane.setVisible(true);
				break;

			case CLASSIC_SUBTYPE:
				_dnPane.setVisible(true);
				_attrPane.setVisible(true);
				break;
				
			default:
				Debug.println(0, "ResEditorCosTemplate.updateComponentState: invalid subtype");
				_dnPane.setVisible(false);
				_attrPane.setVisible(false);
				break;
		}
	}



	/**
	 * Implements Observer.
	 * Only to workaround a console sdk bug.
	 * To be remove when fixed.
	 */
	public void update(Observable observable, Object arg) {
	
	}

	/**
	 * Read _observable and set the UI accordingly.
	 * This method removes all the attributes which are
	 * cos-subtype specific. Those attributes will be restored
	 * by save().
	 */
	void loadFromObservable() {
	
		// Set the radio buttons based on the subtype of the observable
		changeRadioButtonsFromSubType(findSubTypeFromObservable());
		
		// Read cosTemplateDn
		String dn = _observable.get("cosTemplateDn", 0);
		if (dn != null) {
			_dnTextField.setText(dn);
		}
		
		// Read cosSpecifier
		String spec = _observable.get("cosSpecifier", 0);
		if ((spec != null) && (spec.length() >= 1)) {
			_attrTextField.setText(spec);
		}
		
		// Read cosIndirectSpecifier
		String indSpec = _observable.get("cosIndirectSpecifier", 0);
		if ((indSpec != null) && (indSpec.length() >= 1)) {
			_attrTextField.setText(indSpec);
		}
		
	}
	
	/**
	 * Return the subtype of _observable.
	 * This method get the objectclass of the observable and
	 * tries to find a cos subtype.
	 * -1 means no subtype is defined.
	 */
	int findSubTypeFromObservable() {
		int result = -1;
		
		Vector v = _observable.get("objectclass");
		if (v != null) {
			if (DSUtil.indexOfIgnoreCase(v, _subTypeNames[POINTER_SUBTYPE]) != -1) {
				result = POINTER_SUBTYPE;
			}
			else if (DSUtil.indexOfIgnoreCase(v,_subTypeNames[INDIRECT_SUBTYPE]) != -1) {
				result = INDIRECT_SUBTYPE;
			}
			else if (DSUtil.indexOfIgnoreCase(v,_subTypeNames[CLASSIC_SUBTYPE]) != -1) {
				result = CLASSIC_SUBTYPE;
			}
		}
		else {
			Debug.println(0, "ResEditorCosTemplate.findSubTypeFromObservable: no object class");
		}
		
		Debug.println("ResEditorCosTemplate.findSubTypeFromObservable: result = " + result);
		
		return result;
	}


	/**
	 * Return the subtype from the state of the radio buttons.
	 */
	int findSubTypeFromRadioButtons() {
		int result = -1;
		
		if (_pointerRadioButton.isSelected()) {
			result = POINTER_SUBTYPE;
		}
		else if (_indirectRadioButton.isSelected()) {
			result = INDIRECT_SUBTYPE;
		}
		else if (_classicRadioButton.isSelected()) {
			result = CLASSIC_SUBTYPE;
		}
		else {
			Debug.println(0, "ResEditorCosTemplate.findSubTypeFromRadioButtons: ouah grave...");
		}
		
		return result;
	}


	/**
	 * Change the subtype of the observable.
	 * This method removes the existing subtype from
	 * the objectclass of the observable and put
	 * the specified one.
	 */
	void changeObservableFromSubType(int subType) {

		// First remove the existing subtype
		Vector v = _observable.get("objectclass");
		for (int t = POINTER_SUBTYPE; t <= CLASSIC_SUBTYPE; t++) {
			int i = DSUtil.indexOfIgnoreCase(v, _subTypeNames[t]);
			if (i != -1) v.removeElementAt(i);
		}
		
		// And add the new one
		if (subType == -1) {
			subType = POINTER_SUBTYPE;
			Debug.println("ResEditorCosTemplate.changeObservableFromSubType: using default");
		}
		v.addElement(_subTypeNames[subType]);
		_observable.replace("objectclass", v);
	}


	/**
	 * Align the radio buttons on the specified subtype.
	 */
	void changeRadioButtonsFromSubType(int subType) {
	
		switch(subType) {
			case POINTER_SUBTYPE:
				_pointerRadioButton.setSelected(true);
				break;

			case INDIRECT_SUBTYPE:
				_indirectRadioButton.setSelected(true);
				break;

			case CLASSIC_SUBTYPE:
				_classicRadioButton.setSelected(true);
				break;
				
			default: // probably because _observable.isNewUser() == true
				_pointerRadioButton.setSelected(true);
				Debug.println("ResEditorCosTemplate.changeRadioButtonsFromSubtype: using default");
				break;
		}
	}


	/**
	 * To moved in a DSDebug class someday.
	 */
	void DebugDumpObservable(ResourcePageObservable o) {
		Debug.println("===========");
		Debug.println("dn: " + o.getDN());
		Enumeration e = o.getAttributesList();
		while (e.hasMoreElements()) {
			String n = (String)e.nextElement();
			String v = o.getValues(n);
			Debug.println(n + ": " + v);
		}
	}
	
	// State variables
    ResourceEditor _resourceEditor;
	NewAttributeDialog _attributeDialog;
    ResourcePageObservable _observable;
    String _id;

	// Components
	JRadioButton _pointerRadioButton;
	JRadioButton _indirectRadioButton;
	JRadioButton _classicRadioButton;
	JPanel _dnPane;
	JPanel _attrPane;
	JTextField _dnTextField;
	JTextField _attrTextField;
	JButton _dnBrowseButton;
	JButton _attrChangeButton;

	// Schema
	// Warning: constants matches indices in _subTypeNames
	static final int POINTER_SUBTYPE = 0;
	static final int INDIRECT_SUBTYPE = 1;
	static final int CLASSIC_SUBTYPE = 2;
	static final String[] _subTypeNames = {
		"cosPointerDefinition",
		"cosIndirectDefinition",
		"cosClassicDefinition"
	};
	
	// I18N
	static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "cosTemplatePage";
}
