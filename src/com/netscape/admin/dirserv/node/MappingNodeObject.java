/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.util.Enumeration;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;import com.netscape.admin.dirserv.DSResourcePage;
import com.netscape.admin.dirserv.panel.MappingNodePanel;
import com.netscape.admin.dirserv.panel.NewMappingNodePanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import com.netscape.admin.dirserv.panel.MappingUtils;

import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Representation of the Database Root Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version 1.4, 03/15/00
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public 	class MappingNodeObject extends DSResourceObject 
								implements IMenuInfo,
										   ActionListener,
										   TreeExpansionListener{
		
		
	public MappingNodeObject( String sDisplayName,
							  IDSModel model,
							  LDAPEntry entry) {
		this(sDisplayName, _icon, model, entry);
	}
		
		
	public MappingNodeObject( String sDisplayName,
							  RemoteImage icon,
							  IDSModel model,
							  LDAPEntry entry) {

		super( sDisplayName,icon, null, model );
		Debug.println("MappingNodeObject() : entry =" + entry.getDN());
		_entry = entry;
	}

		
	public Component getCustomPanel() {
		if ( _mainPanel == null ) {
			// Need to add a switch between known / unknown plugins
			_mainPanel = new MappingNodePanel( _model, _entry);
		}
		_panel = _mainPanel;
		return _mainPanel;
	}

	/**
   	 *	Called when user wants to execute this object, invoked by a
   	 *  double-click or a menu action.
   	 */
	public boolean run( IPage viewInstance )	{
		
		Debug.println( "MappingNodeObject.run(" +
					   viewInstance.getClass().getName() + ")" );
		reload();
		if (super.getChildCount() != 0) {
			expandPath((ResourcePage)viewInstance);
		}
		refreshTree();
		return true;
	}
		

	public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
		return run( viewInstance );
	}

			/**
			 * Retrieve list of Plugins from the Directory server
			 */
	public void reload() {
		cleanTree();
		//		RemoteImage icon = DSUtil.getPackageImage( _mappingNodeImageName );
		_isLeaf = true;
		try {
			_model.fireChangeFeedbackCursor(null, FeedbackIndicator.FEEDBACK_WAIT );
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			String psRDN = LDAPDN.explodeDN( _entry.getDN(), true)[0];
			String filt = MappingUtils.getParentSearchFilter(psRDN);
			LDAPSearchResults res =
				ldc.search( "cn=mapping tree, cn=config",
							ldc.SCOPE_ONE,
							filt,
							null,
							false );
			Debug.println( " *** " + filt + ": res.getCount()=" + res.getCount());
			while( res.hasMoreElements() ) {
				LDAPEntry bentry = (LDAPEntry)res.nextElement();
				String name = MappingUtils.getMappingNodeName(bentry);
				Debug.println( "*** Mapping node: dn=" + bentry.getDN() + " name=" + name );
				_isLeaf = false ;
				/* Add a node for each Instance */
				MappingNodeObject MapNode = 
					new MappingNodeObject (name,_model, bentry);
				add(MapNode);
			}
		} catch ( LDAPException e ) {
			Debug.println( "MappingNodeObject.reload: " + e );
		} finally {
			_model.fireChangeFeedbackCursor(null, 
											FeedbackIndicator.FEEDBACK_DEFAULT );
		}
		refreshTree();
		_isLoaded = true;
	}
											   

			/**
			 * Called when this object is selected.
			 * Called by: ResourceModel
			 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		super.select( viewInstance );
	}
											   
			/**
			 * see if the backup folders are loaded
			 * @return true, if loaded
			 */
	public boolean isLoaded() {
		return _isLoaded;
	} 
	
			/**
			 * Handle incoming event.
			 *
			 * @param e event
			 */
	public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( REFRESH ) ) {
			reload();
			refreshTree();
		}
	}

		//============  IMENUINFO  ==================
	public String[] getMenuCategoryIDs() {
		return new String[] {
			ResourcePage.MENU_OBJECT,
				ResourcePage.MENU_CONTEXT
				};
	}
											   
	public IMenuItem[] getMenuItems(String category) {
		// return the same thing for both menu categories
		return new IMenuItem[] {
				new MenuItemText( NEWMAPP,
								  _resource.getString("menu", "newmappingnode"),
								  _resource.getString("menu",
													  "newmappingnode-description")),
					new MenuItemText( DELETE,
									  _resource.getString("menu", "delmappingnode"),
									  _resource.getString("menu",
														  "delmappingnode-description"))
		};
	}
											   

	public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
		if( item.getID().equals(NEWMAPP)) {
			create_mapping_node( _entry );
			reload();
		} else if( item.getID().equals(DELETE)) {
			DSResourcePage rp = ( DSResourcePage )viewInstance;
			
			rp.getTree().getSelectionModel().clearSelection();
			delete_mapping_node( _entry );
			TreeNode dtn = getParent();
			if(dtn instanceof MappingNodeObject) { 
				((MappingNodeObject)dtn).reload();
			} else {
				((MappingTreeRootResourceObject)dtn).reload();
			}
		}
	}
	
	public boolean isLeaf() {
		// return _isLeaf;
		return MappingUtils.isLeafMappingNode(_model.getServerInfo().getLDAPConnection(), _entry);
	}

    /**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }
        
    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
		page.expandTreePath(path);
    }
    
    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( equals( o ) ) {
			if ( !_isInitiallyExpanded ) {
				/* Prevent recursion caused by refreshTree() in reload() */
				_isInitiallyExpanded = true;
				Debug.println( "MappingNodeObject.treeExpanded: this" );
				if ( !isLoaded() ) {
					reload();
				}
			}
		}
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {
	}


	private void create_mapping_node( LDAPEntry node) {
		NewMappingNodePanel child = new NewMappingNodePanel(_model, node);
		SimpleDialog dlg = new SimpleDialog( _model.getFrame(),
											 child.getTitle(),
											 SimpleDialog.OK |
											 SimpleDialog.CANCEL |
											 SimpleDialog.HELP,
											 child );
		dlg.setComponent( child );
 		dlg.setOKButtonEnabled( false );
		dlg.setDefaultButton( SimpleDialog.OK );
		dlg.packAndShow();
	}

	private void delete_mapping_node(LDAPEntry node) {
		String dn2delete = node.getDN();
		String rdn2delete = (new DN(dn2delete)).explodeDN(true)[0];
		String[] args = { rdn2delete };
			
		int resDiag = DSUtil.showConfirmationDialog( null,
													 "confirm-del-mapnode",
													 args,
													 SECTION );
		if (resDiag == NO) return;
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
		
		if (!DSUtil.deleteTree ( dn2delete, ldc )) {
			String[] args_m = { dn2delete } ;
			DSUtil.showErrorDialog(_model.getFrame(),
								   "remove",
								   args_m,
								   SECTION);
		}
	}
										   
	private LDAPEntry _entry = null;
	private boolean _isLeaf = false;
	private boolean _isLoaded = false;
    private boolean _isInitiallyExpanded = false;
	private static RemoteImage _icon = DSUtil.getPackageImage( "split-16.gif" );
	private static final String _mappingNodeImageName = "split-16.gif";
	static final String DELETE = "delete_map_node";
	static final String NEWMAPP  = "newmappingnode";
            /*	private static LDAPControl _manageDSAITControl =
			new LDAPControl( LDAPControl.MANAGEDSAIT, true, null );
			*/
	static final String REFRESH = "refresh";
	static final String RESTORE = "restore";
											   //	private MappingNodePanel _mainPanel = null;				   
	private MappingNodePanel _mainPanel = null;
    static final String CONFIG_MAPPING = DSUtil.MAPPING_TREE_BASE_DN  ;
    static final String SECTION = "mappingtree";
    static final int YES = JOptionPane.YES_OPTION;
	static final int NO = JOptionPane.NO_OPTION;
} /* end of class MappingNodeObject */
