/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.awt.Component;
import java.awt.event.*;
import javax.swing.JPanel;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSStatusResourceModel;
import com.netscape.management.client.topology.*;
import javax.swing.Icon;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.util.*;
import netscape.ldap.*;

/**
 * Representation of the Root Node in the Directory Status tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class StatusResourceObject extends DSResourceObject implements ActionListener {
	public StatusResourceObject( IDSModel model ) {
        super( model.getServerInfo().getHost() + ":" +
					model.getServerInfo().getPort(),
				DSUtil.getPackageImage( serverIconName ),
				DSUtil.getPackageImage( serverIconNameL ),
				model );
		setAllowsChildren(true);
		makeTree();
	}

	private void makeTree() {

		/* Performance counters */
		add( new MonitorResourceObject( _model ) );

		/* Logging information */
		add( new LogResourceObject( _model ) );

		/* Replication status */
		add( new ReplicationStatusResourceObject( _model ) );
	}

	public Component getCustomPanel() {
		if ( _panel == null ) {
                        NodeAdapter nodeAdapter = new NodeAdapter(_model.getConsoleInfo());
			_panel = (JPanel)nodeAdapter.getCustomPanel();
		}
		return _panel;
	}

	/**
     *  handle incoming event
     *	 
	 *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSStatusResourceModel.REFRESH  ) ) {			
			reload();
			refreshTree();
			_model.setSelectedNode(this);			
		}
	}

	void reload() {
		cleanTree();
		makeTree();	
	}

	/**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }

	/**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

	private static final String serverIconName = "directory.gif";
	private static final String serverIconNameL = "directm.gif";
}




class NodeAdapter extends AbstractServerObject {
	NodeAdapter(ConsoleInfo info) {
		_icImage = DSUtil.getPackageImage( _imageName );
		try {
		    super.initialize(info);
		} catch (Exception e) {
		    return;
		}
	}
	/**
	 * Implements the AbstractServerObject.run().
	 *
	 */
	public boolean run(IPage viewInstance, IResourceObject selectionList[]) {
	        return false;
	}

	/**
	 * Implements the AbstractServerObject.getServerStatus().
	 */
	public int getServerStatus() {

		return DSUtil.checkServerStatus(_consoleInfo);
	}

	/**
	 * Implements the AbstractServerObject.cloneFrom().
	 *
	 */
	public void cloneFrom(String referenceDN) {
	}

	/**
	 * Overrides the AbstractServerObject.getCustomPanel().
	 * Here we don't want the Open button to be visible.
	 * So here is how we do:
	 *		1) we call the parent implementation
	 *		2) we re-create _nodeDataPanel (hiding the Open button)
	 *      3) we set the corresponding help token
	 * Invoking the parent implementation is important because
	 * it does mandatory operations (like starting the status thread,
	 * setting help) and this operations cannot be replicated here.
	 */
    public Component getCustomPanel() {
	try {
	    Component toBeTrashed = super.getCustomPanel();
	    _nodeDataPanel = 
		new NodeDataPanel(getIcon(),
				  (String)_nodeDataTable.get(_nodeNameKey), 
				  this);
	    _nodeDataPanel.setHelpTopic("slapd", "status-server-general-help");
	    return _nodeDataPanel;
	} catch (Exception e) {
	    return null;
	}
    }

    /**
     * Implements the AbstractServerObject.getIcon().
     *
     */
    public Icon getIcon() {
	return _icImage;
    }	
    
    private RemoteImage _icImage = null;
    private static final String _imageName = "directory.gif";
    static String _nodeNameKey = "serverProductName";
}

