/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.util.Enumeration;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.tree.*;
import javax.swing.event.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.panel.PluginPanel;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSResourceModel;
import netscape.ldap.*;

/**
 * Representation of the Plugin Folder Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	01/14/98
 * @see     com.netscape.management.client.ResourceObject
 */
public class PluginResourceObject extends DSResourceObject
                                  implements IMenuInfo,
	                                         ActionListener,
                                             TreeExpansionListener {
    
    public PluginResourceObject( IDSModel model ) {
        super( _resource.getString("resourcepage","Plugins"),
				DSUtil.getPackageImage( pluginsIconName ),
				DSUtil.getPackageImage( pluginsIconNameL ),
				model );
    }

	/**
	 *	Called when user wants to execute this object, invoked by a
	 *  double-click or a menu action.
	 */
	public boolean run( IPage viewInstance )	{
	    Debug.println( "PluginResourceObject.run(" +
					   viewInstance.getClass().getName() + ")" );
        reload();
        if (super.getChildCount() != 0) {
			expandPath((ResourcePage)viewInstance);
        }
        refreshTree();
		return true;
	}

	public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
		return run( viewInstance );
	}
	
    /**
     * Retrieve list of Plugins from the Directory server
     */
	public void reload() {
	    cleanTree();
		RemoteImage icon = DSUtil.getPackageImage( _pluginImageName );
		_isLeaf = true;
		try {
			_model.fireChangeFeedbackCursor(
				null, FeedbackIndicator.FEEDBACK_WAIT );
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			LDAPSearchResults res =
				ldc.search( "cn=config",
							ldc.SCOPE_SUB,
							"objectclass="+OCLASS,
							null,
							false );
			while( res.hasMoreElements() ) {
				LDAPEntry entry = (LDAPEntry)res.nextElement();
				String name = entry.getDN();
				Debug.println( "Plugin: " + name );
				LDAPAttribute attr = entry.getAttribute( "cn" );
				if ( attr == null )
					attr = entry.getAttribute( ID );
				if ( attr == null )
					attr = entry.getAttribute( PATH );
				if ( attr != null ) {
					_isLeaf = false;
					Enumeration en = attr.getStringValues();
					if ( en.hasMoreElements() ) {
						name = (String)en.nextElement();
					}
				}
				/* Add a node for each plugin */
				PluginObject plugin = 
	                    new PluginObject( name,
										  icon,
										  _model,
										  entry );
				add( plugin );
				/* Plugin enabling requires server restart to take effect */
				DSUtil.addRequiresRestart( entry.getDN(), ENABLED );
	        }
		} catch ( LDAPException e ) {
			Debug.println( "PluginResourceObject.reload: " + e );
		} finally {
			_model.fireChangeFeedbackCursor(
					null, FeedbackIndicator.FEEDBACK_DEFAULT );
		}
		refreshTree();
		_isLoaded = true;
	}

	/**
	 * Called when this object is selected.
     * Called by: ResourceModel
	 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		super.select( viewInstance );
	}

    /**
     * see if the plugins are loaded
     * @return true, if loaded
     */
	public boolean isLoaded() {
	    return _isLoaded;
	}
	
    /**
     * Handle incoming event.
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {
			reload();
			refreshTree();
			_sharedPanel = null;
		}
    }	

	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
		return new IMenuItem[]{			
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description")),
				};
    }

    public void actionMenuSelected(IPage viewInstance, IMenuItem item){
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((DSResourceModel)_model).actionMenuSelected(viewInstance, item);
		}
	}
												 
	
	public boolean isLeaf() {
		return _isLeaf;
	}

    /**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }
        
    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
		page.expandTreePath(path);
    }
    
    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( equals( o ) ) {
			if ( !_isInitiallyExpanded ) {
				/* Prevent recursion caused by refreshTree() in reload() */
				_isInitiallyExpanded = true;
				Debug.println( "PluginResourceObject.treeExpanded: this" );
				if ( !isLoaded() ) {
					reload();
				}
			}
		}
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {
	}

    /*==========================================================
     * variables
     *==========================================================*/
    private boolean _isLoaded = false;
    private boolean _isInitiallyExpanded = false;
	private boolean _isLeaf = false;
	private static final String pluginsIconName = "pluginfolder.gif";
	private static final String pluginsIconNameL = "backupcL.gif";
    private static final String _pluginImageName = "plugin.gif";
	private static final String OCLASS = "nsslapdplugin";
	private static final String PATH = "nsslapd-pluginpath";
	private static final String ID = "nsslapd-pluginid";
	private static final String ENABLED = "nsslapd-pluginenabled";
    

	/* Nested class implements a single plugin resource object */
    class PluginObject extends DSResourceObject
                       implements ChangeListener, ActionListener,IMenuInfo {
    
        public PluginObject( String sDisplayName,
							 RemoteImage icon,
							 IDSModel model,
							 LDAPEntry entry) {
			super( sDisplayName, icon, null, model );
			_entry = entry;
		}

 	    public Component getCustomPanel() {
			if ( _sharedPanel == null ) {
				_sharedPanel = new PluginPanel( _model );
			}
			_panel = _sharedPanel;
			return _sharedPanel;
		}
        
        public void stateChanged(ChangeEvent e) {
            if (e.getSource() instanceof LDAPEntry) {
                _entry = (LDAPEntry) e.getSource();
                String name = getAttrVal(NAME_ATTR);
                Debug.println("PluginObject.stateChanged:" + 
                              " ldap entry update for " + name + " plugin");
                if ((name!=null) && (!name.equals(""))) {
                    setName(name);
                
                }
            }
        }
            
		private String getAttrVal( String attrName ) {
			if ( _entry != null ) {
				LDAPAttribute attr = _entry.getAttribute( attrName );
				if ( attr != null ) {
					Enumeration en = attr.getStringValues();
					if ( (en != null) && en.hasMoreElements() )
						return (String)en.nextElement();
				}
			}
			return "";
		}
		

		/**
		 * Called when this object is selected.
		 * Called by: ResourceModel
		 */
	    public void select(IPage viewInstance) {
			_sharedPanel.editPluginEntry( _entry, this );
		}

		/**
		 *	Called when user wants to execute this object, invoked by a
		 *  double-click or a menu action.
		 */
	    public boolean run( IPage viewInstance ) {
			Debug.println( "PluginObject.run(" +
						   viewInstance.getClass().getName() + ")" );
		        _sharedPanel.advancedCallBack();
			
			return true;
		}

	    /**
		 * Handle incoming event.
		 *
		 * @param e event
		 */
		public void actionPerformed(ActionEvent e) {
			if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {
				reloadEntry();
				/* If the entry corresponding to this plugin does not exist anymore,
				   we update all the plugin tree */
				if (_entry == null) {
					reload();
					refreshTree();
				} else {
					/* We reset the panel with the contents of the server */
					_sharedPanel.refreshFromServer();
				}
			}
		}


	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (this._categoryID == null) {
			this._categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return this._categoryID;
	}
		
		public IMenuItem[] getMenuItems(String category) {
			if (category.equals(ResourcePage.MENU_CONTEXT)) {
				if (this._contextMenuItems == null) {
					this._contextMenuItems = createMenuItems();
				} 
				return this._contextMenuItems;
			} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
				if (this._objectMenuItems == null) {
					this._objectMenuItems = this.createMenuItems();
				}
				return this._objectMenuItems;			
			}
			return null;
		}
								 
		private IMenuItem[] createMenuItems() {
			return new IMenuItem[] {
				new MenuItemText( DSResourceModel.REFRESH,
								  DSUtil._resource.getString("menu", "refresh"),
								  DSUtil._resource.getString("menu",
															 "refresh-description")),
					};
		}
																	
		public void actionMenuSelected(IPage viewInstance, IMenuItem item){
			if (item.getID().equals(DSResourceModel.REFRESH)) {
				((DSResourceModel)_model).actionMenuSelected(viewInstance, item);
			}
		}
																	
        private void reloadEntry() {
			LDAPConnection ldc = this._model.getServerInfo().getLDAPConnection();			
			try {
				_entry = ldc.read(_entry.getDN());
			} catch (LDAPException e) {
				_entry = null;
				Debug.println("PluginResourceObject.PluginObject.reloadEntry(): "+e);
				if (e.getLDAPResultCode() != LDAPException.NO_SUCH_OBJECT) {
					DSUtil.showLDAPErrorDialog(this._model.getFrame(),
											   e,
											   "fetching-directory");
				}
			}
				
		}																	
		protected String[] _categoryID;
		protected IMenuItem[] _contextMenuItems;
		protected IMenuItem[] _objectMenuItems;
															
		private boolean _initialized = false;
        private String _name = null;	
		private LDAPEntry _entry = null;
	}
	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;


	private static final String NAME_ATTR = "cn"; 
    private PluginPanel _sharedPanel = null;
}
