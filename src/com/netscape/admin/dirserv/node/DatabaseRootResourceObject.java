/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.util.Enumeration;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.node.DatabasePluginObject;
import com.netscape.admin.dirserv.node.ChainingPluginObject;
import com.netscape.admin.dirserv.panel.DatabaseRootPanel;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Representation of the Database Root Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class DatabaseRootResourceObject extends DSResourceObject
	                                    implements ActionListener,
                                             TreeExpansionListener,
                                             IMenuInfo {
 
	public DatabaseRootResourceObject( IDSModel model ) {
        super( _resource.getString("resourcepage","Content"),
				DSUtil.getPackageImage( databaseIconName ),
				DSUtil.getPackageImage( databaseIconNameL ),
				model );
	}

	public Component getCustomPanel() {
		if ( _mainPanel == null ) {
			// Need to add a switch between known / unknown plugins
			_mainPanel = new DatabaseRootPanel( _model);
		}
		_panel = _mainPanel;
		return _mainPanel;
	}												 


	/**
	 *	Called when user wants to execute this object, invoked by a
	 *  double-click or a menu action.
	 */
	public boolean run( IPage viewInstance )	{
	    Debug.println( "DatabaseRootResourceObject.run(" +
					   viewInstance.getClass().getName() + ")" );
        reload();
        if (super.getChildCount() != 0) {
			expandPath((ResourcePage)viewInstance);
        }
        refreshTree();
		return true;
	}

	public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
		return run( viewInstance );
	}
	
	
    /**
     * Retrieve list of Plugins from the Directory server
     */
	public void reload() {
	    cleanTree();
		RemoteImage icon = DSUtil.getPackageImage( _dbinstImageName );
		_isLeaf = true;
		try {
			_model.fireChangeFeedbackCursor(
				null, FeedbackIndicator.FEEDBACK_WAIT );
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			LDAPSearchResults res =
				ldc.search( "cn=plugins, cn=config",
							ldc.SCOPE_SUB,
							"nsslapd-plugintype=database",
							null,
							false );
			while( res.hasMoreElements() ) {
				LDAPEntry entry = (LDAPEntry)res.nextElement();
				String name = entry.getDN();
				Debug.println( "******************");
				Debug.println( "Plugin: " + name );
				LDAPAttribute attr = entry.getAttribute( "cn" );
				if ( attr == null )
					attr = entry.getAttribute( ID );
				if ( attr == null )
					attr = entry.getAttribute( BCKD );
				if ( attr != null ) {
					_isLeaf = false;
					Enumeration en = attr.getStringValues();
					if ( en.hasMoreElements() ) {
						name = (String)en.nextElement();
					}
				}

				LDAPAttribute attr_type = entry.getAttribute( ID );
				Debug.println( "attr_type:" + attr_type.toString());
				if( attr_type  !=  null ) {
					StringBuffer bPType = new StringBuffer();
					String PType = null;
					Enumeration en = attr_type.getStringValues();
					if ( en.hasMoreElements() ) {
						bPType.append((String)en.nextElement());
					}
					PType = bPType.toString();
					Debug.println( "Plugin Type:" + PType);
					if(PType.compareTo( "ldbm-backend" ) == 0) {
						/* Add a node for each plugin */
						DatabasePluginObject DBplugin = 
							new DatabasePluginObject(name,icon,_model,entry);
						add( DBplugin );
					} else if(PType.compareTo( "chaining database") == 0) {
						ChainingPluginObject Chaining = new ChainingPluginObject(name,icon,_model,entry);
						add( Chaining );
					}
				
				/* Plugin enabling requires server restart to take effect */
						//				DSUtil.addRequiresRestart( entry.getDN(), ENABLED );
				}
			}
		} catch ( LDAPException e ) {
			Debug.println( "DatabaseRootResourceObject.reload: " + e );
		} finally {
			_model.fireChangeFeedbackCursor(
					null, FeedbackIndicator.FEEDBACK_DEFAULT );
		}
		add( new SchemaResourceObject( _model ) );	   
		refreshTree();
		_isLoaded = true;
	}


	/**
	 * Called when this object is selected.
     * Called by: ResourceModel
	 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		super.select( viewInstance );
	}

    /**
     * see if the backup folders are loaded
     * @return true, if loaded
     */
	public boolean isLoaded() {
	    return _isLoaded;
	}
	
    /**
     * Handle incoming event.
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( REFRESH ) ) {
			reload();
			refreshTree();
		}
    }

	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
		return new IMenuItem[]{			
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description")),
				};
    }

    public void actionMenuSelected(IPage viewInstance, IMenuItem item){
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((DSResourceModel)_model).actionMenuSelected(viewInstance, item);
		}
	}
	
	public boolean isLeaf() {
		return _isLeaf;
	}

    /**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }
        
    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
		page.expandTreePath(path);
    }
    
    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( equals( o ) ) {
			if ( !_isInitiallyExpanded ) {
				/* Prevent recursion caused by refreshTree() in reload() */
				_isInitiallyExpanded = true;
				Debug.println( "PluginResourceObject.treeExpanded: this" );
				if ( !isLoaded() ) {
					reload();
				}
			}
		}
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {
	}


    /*==========================================================
     * variables
     *==========================================================*/
	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;

    private boolean _isLoaded = false;
    private boolean _isInitiallyExpanded = false;
	private boolean _isLeaf = false;
	private static final String _dbinstImageName = "plugin.gif";
	private static final String ID = "nsslapd-pluginid";
	private static final String BCKD = "nsslapd-backend";
	static final String IMPORT = "import";
	static final String EXPORT = "export";
	static final String NEWDB  = "newbackend";
			/*	private static LDAPControl _manageDSAITControl =
			new LDAPControl( LDAPControl.MANAGEDSAIT, true, null );
			*/
	private static String _dbFilter;
	private static final String databaseIconName = "dbcontainer.gif";
	private static final String databaseIconNameL = "dbcontainerL.gif";
	private static final String ENABLED = "nsslapd-pluginenabled";
    static final String REFRESH = "refresh";
	static final String RESTORE = "restore";
	private DatabaseRootPanel _mainPanel = null;
}


