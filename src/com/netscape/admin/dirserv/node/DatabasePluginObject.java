/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.util.Enumeration;
import java.util.Vector;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.panel.LDBMPluginPanel;
import com.netscape.admin.dirserv.panel.LDBMInstancePanel;
import com.netscape.admin.dirserv.panel.DatabaseImportPanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import com.netscape.admin.dirserv.panel.DatabaseExportPanel;
import com.netscape.admin.dirserv.panel.ChainingPluginPanel;
import com.netscape.admin.dirserv.panel.BlankPanel;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Representation of the Database Root Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public 	class DatabasePluginObject extends DSResourceObject
								implements IMenuInfo,
										   ActionListener,
										   TreeExpansionListener{
    
	public DatabasePluginObject( String sDisplayName,
								 RemoteImage icon,
								 IDSModel model,
								 LDAPEntry entry,
								 boolean isStandAloneConf) {
		super( sDisplayName, icon, null, model );
		Debug.println("DatabasePluginObject() : entry =" + entry.getDN());
		_entry = entry;
		/* This bool used to share same resObj between */
		/* merged/non merge version */
		_isStandAloneConf = isStandAloneConf;
		_isLeaf = _isStandAloneConf;
		LDAPAttribute attr = entry.getAttribute( "nsslapd-pluginid" );
		if ( attr == null ) {
			_pluginName = sDisplayName;
		} else {
			Enumeration en = attr.getStringValues();
			if ( en.hasMoreElements() ) {
				_pluginName = (String)en.nextElement();
			} else {
				_pluginName = sDisplayName;
			}
		}

	}
	public DatabasePluginObject( String sDisplayName,
								 RemoteImage icon,
								 IDSModel model,
								 LDAPEntry entry) {
		this( sDisplayName, icon, model, entry, false );
	}
											   

	public Component getCustomPanel() {
		if ( _panel == null ) {
			// Need to add a switch between known / unknown plugins
			if(( _pluginName.compareTo( DSUtil.LDBM_PLUGIN_ID ) == 0 ) ||
			   ( _pluginName.compareTo( DSUtil.LDBM_PLUGIN_NAME ) == 0)) {
				_panel = new LDBMPluginPanel( _model, _entry );
			} else if ( _pluginName.compareTo( DSUtil.CHAINING_PLUGIN_NAME ) == 0 ){
				_panel = new ChainingPluginPanel( _model, _entry );
			} else {
				_panel = new BlankPanel( _model );
			}
		}
		return _panel;
	}

		/**
		 *	Called when user wants to execute this object, invoked by a
		 *  double-click or a menu action.
		 */
	public boolean run( IPage viewInstance )	{
		
		Debug.println( "DatabasePluginObject.run(" +
					   viewInstance.getClass().getName() + ")" );
		reload();
		if (super.getChildCount() != 0) {
			expandPath((ResourcePage)viewInstance);
		}
		refreshTree();
		return true;
	}
											   
	public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
		return run( viewInstance );
	}

	
	public void reload() {
		_model.fireChangeFeedbackCursor(null,
										FeedbackIndicator.FEEDBACK_WAIT );
		cleanTree();
		StringBuffer bPType = new StringBuffer();
		RemoteImage icon = DSUtil.getPackageImage( _dbinstImageName );		
		LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();

		/* get the plugin type */
		LDAPAttribute pattr = _entry.getAttribute( "nsslapd-pluginid" );
		if( pattr != null ) {
			Enumeration en = pattr.getStringValues();
			if ( en.hasMoreElements() ) {
				bPType.append((String)en.nextElement());
			}
		}
		String PType = 	bPType.toString();

		_isLeaf = true;
		if ( ! _isStandAloneConf ){
			try {
				LDAPSearchResults res =
					ldc.search( _entry.getDN(),
								ldc.SCOPE_ONE,
								"objectclass=nsBackendInstance",
								null,
								false );
				
				/* get instance list */
				while( res.hasMoreElements() ) {
					LDAPEntry bentry = (LDAPEntry)res.nextElement();
					String name = bentry.getDN();
					Debug.println( "Instance db: " + name );
					LDAPAttribute attr = bentry.getAttribute( "cn" );
					if ( attr != null ) {
						_isLeaf = false;
						Enumeration en = attr.getStringValues();
						if ( en.hasMoreElements() ) {
							name = (String)en.nextElement();
						}
					}
					/* add appropriate db instance node according plugin type */
					if( PType.compareTo("ldbm-backend") == 0 ) {
						LdbmDatabaseObject DBInst = 
							new LdbmDatabaseObject(name, icon,_model, bentry);
						add(DBInst);
					}
								
				}
			} catch ( LDAPException e ) {
				Debug.println( "DatabasePluginObject.reload: " + e );
			} 
		}
		refreshTree();
		_isLoaded = true;
		_model.fireChangeFeedbackCursor(null,
										FeedbackIndicator.FEEDBACK_DEFAULT );
	}
											   

			/**
			 * Called when this object is selected.
			 * Called by: ResourceModel
			 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		super.select( viewInstance );
	}
											   
			/**
			 * see if the backup folders are loaded
			 * @return true, if loaded
			 */
	public boolean isLoaded() {
		return _isLoaded;
	} 
	
			/**
			 * Handle incoming event.
			 *
			 * @param e event
			 */
	public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( REFRESH ) ) {
			reload();
			refreshTree();
		}
	}

	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
		if ( ! _isStandAloneConf ) {
			// return the same thing for both menu categories
			return new IMenuItem[] {
				new MenuItemText( NEWDB,
								  _resource.getString("menu", "newbackend"),
								  _resource.getString("menu",
													  "newbackend-description")),
			new MenuItemSeparator(),
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description"))
					};
		} else {
			return new IMenuItem[] {			
				new MenuItemText( DSResourceModel.REFRESH,
								  DSUtil._resource.getString("menu", "refresh"),
								  DSUtil._resource.getString("menu",
															 "refresh-description"))
					};
		}
	}
											   
											   
	public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
		if( item.getID().equals(NEWDB)) {
			_model.actionMenuSelected(viewInstance, item);
			_isLeaf = false;
			reload();
		}
	}
	
	public boolean isLeaf() {
		return _isLeaf;
	}

    /**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }
        
    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
		page.expandTreePath(path);
    }
    
    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( equals( o ) ) {
			if ( !_isInitiallyExpanded ) {
				/* Prevent recursion caused by refreshTree() in reload() */
				_isInitiallyExpanded = true;
				Debug.println( "PluginResourceObject.treeExpanded: this" );
				if ( !isLoaded() ) {
					reload();
				}
			}
		}
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {
	}
										   
	private LDAPEntry _entry = null;
	private boolean _isLeaf = false;
	private boolean _isLoaded = false;
    private boolean _isInitiallyExpanded = false;
	private static final String _dbinstImageName = "dbobj.gif";
	static final String IMPORT = "import";
	static final String EXPORT = "export";
	static final String DELETE = "delete_instance";
	static final String NEWDB  = "newbackend";
	static final String INITIALIZE_BACKEND = "initializebackend";
            /*	private static LDAPControl _manageDSAITControl =
			new LDAPControl( LDAPControl.MANAGEDSAIT, true, null );
			*/
	private static String _dbFilter;
	static final String REFRESH = "refresh";
	static final String RESTORE = "restore";

	class LdbmDatabaseObject extends DSResourceObject 
							 implements IMenuInfo {
		public LdbmDatabaseObject( String sDisplayName,
								   RemoteImage icon,
								   IDSModel model,
								   LDAPEntry entry) {
			super( sDisplayName, icon, null, model );
			_bckLoaded = false;
			_entry = entry;
			_backendname  = sDisplayName;
		}

		public Component getCustomPanel() {
			if (_panel == null) {
				_panel = new LDBMInstancePanel( _model, _entry );
			}
			return _panel;
		}

		public void deleteInstance( LDAPEntry dbInst, String backendname ) {

			String[] args = { backendname };			
			int resDiag = DSUtil.showConfirmationDialog( null,
														 "confirm-bck",
														 args,
														 SECTION );
			if (resDiag == NO) return;

			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			String  dnInst = dbInst.getDN();			
			Debug.println("+++++++++++++++ Starting delete mapping tree ++++++++++");
			// Delete mapping tree 
			try {
				LDAPSearchResults res =
					ldc.search( CONFIG_MAPPING,
								ldc.SCOPE_SUB,
								"nsslapd-backend="+ backendname,
								null,
								false );

				while( res.hasMoreElements() ) {
					boolean shouldBeDeleted = false;
					LDAPEntry bentry = (LDAPEntry)res.nextElement();
					String name = bentry.getDN();
					Debug.println( "Instance db: " + name );

					// Check that's the only instance of db in this node
					LDAPAttribute attr_status = bentry.getAttribute( "nsslapd-status" );
					if ( attr_status != null ) {
						Debug.println( "Node type : " + attr_status.getStringValueArray()[0]);
						shouldBeDeleted = attr_status.getStringValueArray()[0].compareToIgnoreCase("backend") == 0;

					} else { // none valid entry remove it !!
						shouldBeDeleted = true;
					}
					LDAPAttribute attr_db = bentry.getAttribute( "nsslapd-backend" );
					
					Debug.println( " attr_db:" + attr_db);
					if((attr_db != null) && ( attr_db.size() == 1 ) && shouldBeDeleted ) {
						deleteTree( name );
					} else {
						LDAPModificationSet mods = new LDAPModificationSet();
						LDAPAttribute backend_instMapping = new LDAPAttribute( "nsslapd-backend",
																			   backendname );
						mods.add( LDAPModification.DELETE, backend_instMapping );
						String[] args_mapping = { backendname, name };
						resDiag = DSUtil.showConfirmationDialog( null,
																 "confirm-mapping",
																 args_mapping,
																 SECTION );
						if (resDiag == YES) {
							try {
								ldc.modify(name,  mods );
							} catch (LDAPException e_map) {
								String[] args_map = { backendname, name,  e_map.toString()} ;
								DSUtil.showErrorDialog(_model.getFrame(),
													   "mod-mapping",
													   args_map,
													   SECTION);

							} // catch
						}
					}
				} // while
			} catch ( LDAPException e_search ) {
				String[] args_map = { backendname, e_search.toString()} ;
				DSUtil.showErrorDialog(_model.getFrame(),
									   "search-mapping",
									   args_map,
									   SECTION);
			}

			if( deleteTree( dnInst )) {
				TreeNode dtn = getParent();
				if(dtn instanceof DatabaseRootResourceObject) { 
					((DatabaseRootResourceObject)dtn).reload();
				} else if(dtn instanceof DatabasePluginObject) {
					((DatabasePluginObject)dtn).reload();
				}
			}
		} // deleteInstance
		

		private boolean deleteTree ( String dnRoot ) {
			return deleteTree ( dnRoot , true);
		}

		private boolean deleteTree ( String dnRoot , boolean first) {

			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			int resDiag;

			//			String msg = "deleting : " + dnRoot;
			Debug.println( "deleting : " + dnRoot );
			String[] args = { dnRoot };

			if ( first ) {
				resDiag = DSUtil.showConfirmationDialog( null,
															 "confirm-inst",
															 dnRoot,
															 SECTION );
			} else {
				resDiag = YES;
			}
			
			if (resDiag == YES) {
				// delete sub entries first
				try { 
					LDAPSearchResults res = 
						ldc.search( dnRoot,
									ldc.SCOPE_ONE,
									"(|(objectclass=*)(objectclass=ldapsubentry))",
									null,
									false );
					
					while( res.hasMoreElements() ) {
						LDAPEntry bentry = (LDAPEntry)res.nextElement();
						String name = bentry.getDN();
						deleteTree( name, false );
					}
				} catch (LDAPException e) {
					String[] args_m = { dnRoot, e.toString()} ;
					DSUtil.showErrorDialog(_model.getFrame(),
										   "search-sub",
										   args_m,
										   SECTION);
					return( false );
				}
				
				// delete the instance
				try {
					ldc.delete ( dnRoot );
					Debug.println( "..........."  + dnRoot + " deleted" );
				} catch (LDAPException e) {
					String[] args_m = { dnRoot, e.toString()} ;
					DSUtil.showErrorDialog(_model.getFrame(),
										   "remove",
										   args_m,
										   SECTION);
					return( false );
				}
			} else {
				return( false );
			}
			return( true );
		} // deleteTree
			
							   


	   /**
	  	 * Called when this object is selected.
	   	 * Called by: ResourceModel
	   	 */
		public void select(IPage viewInstance) {
			// rm : TBD     updateFromEntry( _entry );
			Debug.println( "LdbmDatabaseObject.select(" +
						   viewInstance.getClass().getName() + ")");
			if(_bckLoaded == false) {
				add( new BackupResourceObject( _model ) );
				_bckLoaded = true;
			}
			
		}

                /**
                 *      Called when user wants to execute this object, invoked by a
                 *  double-click or a menu action.
                 */
		public boolean run( IPage viewInstance ) {
			Debug.println( "LdbmDatabaseObject.run(" +
						   viewInstance.getClass().getName() + ")" );
			return true;
		}
								 
        	/**
	 * Implement IMenuInfo Interface
	 */
		public String[] getMenuCategoryIDs(){
			if (this._categoryID == null) {
				this._categoryID = new String[]  {
					ResourcePage.MENU_OBJECT,
						ResourcePage.MENU_CONTEXT
						};
			} 
			return this._categoryID;
		}
		
		public IMenuItem[] getMenuItems(String category) {
			if (category.equals(ResourcePage.MENU_CONTEXT)) {
				if (this._contextMenuItems == null) {
					this._contextMenuItems = createMenuItems();
				} 
				return this._contextMenuItems;
			} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
				if (this._objectMenuItems == null) {
					this._objectMenuItems = this.createMenuItems();
				}
				return this._objectMenuItems;			
			}
			return null;
		}
								 
		private IMenuItem[] createMenuItems() {
			return new IMenuItem[] {				
				new MenuItemText( DELETE,
								  _resource.getString("menu", "delete"),
								  _resource.getString("menu","delete-description"))
					};
		}
								 
								 
		public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
			if( item.getID().equals(EXPORT) ) {
				exportInstance(_backendname);			
			} else if( item.getID().equals(IMPORT) ) {
				_model.actionMenuSelected(viewInstance, item);
			} else if( item.getID().equals(DELETE) ) {
				deleteInstance(_entry, _backendname);
			}	
			else if( item.getID().equals(INITIALIZE_BACKEND) ) {
				initializeBackend(_backendname);
			}	
		}
				

		private void initializeBackend(String backendName) {					 		
			DatabaseImportPanel child = new DatabaseImportPanel( _model, backendName );	
			SimpleDialog dlg = new SimpleDialog( _model.getFrame(),
												 child.getTitle(),
												 SimpleDialog.OK |
												 SimpleDialog.CANCEL |
												 SimpleDialog.HELP,
												 child );
			dlg.setComponent( child );
			dlg.setOKButtonEnabled( false );
			dlg.setDefaultButton( SimpleDialog.OK );
			dlg.packAndShow();	    	 
		}

		private void exportInstance(String backendName) {					 
			DatabaseExportPanel child = new DatabaseExportPanel( _model, backendName );	
			SimpleDialog dlg = new SimpleDialog( _model.getFrame(),
												 child.getTitle(),
												 SimpleDialog.OK |
												 SimpleDialog.CANCEL |
												 SimpleDialog.HELP,
												 child );
			dlg.setComponent( child );
			dlg.setOKButtonEnabled( false );
			dlg.setDefaultButton( SimpleDialog.OK );
			dlg.packAndShow();	        
		}
					 
		protected String[] _categoryID;
		protected IMenuItem[] _contextMenuItems;
		protected IMenuItem[] _objectMenuItems;						 
		private LDAPEntry _entry = null;
		private boolean _bckLoaded = false;
		private String _backendname;		
		protected String _section = "";
        static final String SECTION = "deletedb";
		static final int YES = JOptionPane.YES_OPTION;
		static final int NO = JOptionPane.NO_OPTION;

	} /* end of class LdbmDatabaseObject */

	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;
	private String _pluginName;
	private boolean _isStandAloneConf;
    static final String CONFIG_MAPPING = "cn=mapping tree, cn=config" ;

} /* end of class DatabasePluginObject */
