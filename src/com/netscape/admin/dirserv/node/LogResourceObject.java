/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.awt.Component;
import java.awt.event.*;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.admin.dirserv.DSStatusResourceModel;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import java.awt.event.*;

/**
 * Representation of the Access Log Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class LogResourceObject extends DSResourceObject implements ActionListener {
	public LogResourceObject( IDSModel model ) {
        super( _resource.getString("resourcepage","Logs"),
				DSUtil.getPackageImage( logGroupIconName ),
				DSUtil.getPackageImage( logGroupIconNameL ),
				model );
		makeTree();
	}

	private void makeTree() {
		add( new AccessLogResourceObject( _model ) );
		add( new ErrorLogResourceObject( _model ) );
		add( new AuditLogResourceObject( _model ) );
	}

	/**
     *  handle incoming event
     *	 
	 *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSStatusResourceModel.REFRESH  ) ) {			
			reload();
			refreshTree();
			_model.setSelectedNode(this);
		}
	}

	void reload() {
		cleanTree();
		makeTree();	
	}

	/**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }

	/**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

	private static final String logGroupIconName = "logs.gif";
	private static final String logGroupIconNameL = "logsL.gif";
	static final String logIconName = "logobject.gif";
	static final String logIconNameL = "logobjectL.gif";
}
