/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.awt.Component;
import java.awt.event.*;
import javax.swing.tree.TreeNode;
import netscape.ldap.util.DN;
import netscape.ldap.*;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.admin.dirserv.DSStatusResourceModel;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.panel.AccessLogContentPanel;
import com.netscape.admin.dirserv.panel.ForwardingContainerPanel;
import com.netscape.admin.dirserv.panel.MonitorDatabasePanel;


/**
 * Representation of the Access Log Node in the Directory Configuration tree
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class MonitorDbResourceObject extends DSResourceObject implements ActionListener {
    public MonitorDbResourceObject( IDSModel model,
				    String dn) {
        super( ((new DN( dn )).explodeDN( true ))[1],
	       DSUtil.getPackageImage( monitorIconName ),
	       DSUtil.getPackageImage( monitorIconNameL ),
	       model );
	_dn = dn;
    }

    public Component getCustomPanel() {	
		if ( _panel == null ) {			
			if (reload()) {				
				_panel = 
					new ForwardingContainerPanel( 
												 _model,
												 new MonitorDatabasePanel( _model, 
																		   _dn ),
												 true );
				((ForwardingContainerPanel)_panel).getOKButton().setVisible(false);
				((ForwardingContainerPanel)_panel).getCancelButton().setVisible(false);
			}			
		} 
		return _panel;
    }
	
    /**
     *  handle incoming event
     *	 
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
	if ( e.getActionCommand().equals( DSStatusResourceModel.REFRESH  ) ) {		
	    reload();
	    refreshTree();			
	}
    }
    
    boolean reload() {
	/* We check if this entry exists, if it doesn't we remove this node from the tree */
	LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	if (ldc == null) {
	    return false;
	}
	String[] attrs = {"dn"};
	LDAPEntry entry = null;
	try {
	    entry = ldc.read(_dn, attrs);
	} catch (LDAPException ex) {
	    if (ex.getLDAPResultCode() == LDAPException.NO_SUCH_OBJECT) {
		/* The entry does not exist: we update the parent node */
		TreeNode dtn = getParent();
		if (dtn instanceof MonitorResourceObject) {
		    ((MonitorResourceObject)dtn).reload();
		    ((MonitorResourceObject)dtn).refreshTree();
		    _model.setSelectedNode((MonitorResourceObject)dtn);
		}
	    }
	    return false;
	}
	return true;
    }

    /**
     * Refresh the tree view
     */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }
    
    String _dn;
    private static final String monitorIconName = "monitorobj.gif";
    private static final String monitorIconNameL = "monitorobjL.gif";
}
