/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.awt.Component;
import java.awt.event.*;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.DSStatusResourceModel;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.AccessLogContentPanel;
import com.netscape.admin.dirserv.panel.ForwardingContainerPanel;

/**
 * Representation of the Access Log Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class AccessLogResourceObject extends DSResourceObject implements ActionListener {
	public AccessLogResourceObject( IDSModel model ) {
        super( _resource.getString("resourcepage","AccessLog"),
				DSUtil.getPackageImage( LogResourceObject.logIconName ),
				DSUtil.getPackageImage( LogResourceObject.logIconNameL ),
				model );
	}
	public Component getCustomPanel() {		
		if ( _panel == null ) {
 			_panel = new ForwardingContainerPanel( _model,
 										 new AccessLogContentPanel( _model ),
 										 true );
		}
		return _panel;
	}

	/**
     *  handle incoming event
     *	 
	 *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSStatusResourceModel.REFRESH  ) ) {					
			reload();
			refreshTree();
			_model.setSelectedNode(this);			
		}
	}

	void reload() {
		cleanTree();	
	}

	/**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }

	/**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }
}
