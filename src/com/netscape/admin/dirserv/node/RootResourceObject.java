/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.awt.Component;
import java.awt.event.*;
import javax.swing.tree.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.RootPanel;
import com.netscape.admin.dirserv.panel.replication.ReplicationResourceObject;

/**
 * Representation of the Root Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class RootResourceObject extends DSResourceObject implements ActionListener, IMenuInfo {
    public RootResourceObject( IDSModel model ) {
        super( model.getServerInfo().getHost() + ":" +
	       model.getServerInfo().getPort(),
	       DSUtil.getPackageImage( serverIconName ),
	       DSUtil.getPackageImage( serverIconNameL ),
	       model );
		reload();
		refreshTree();
		_model.setSelectedNode(this);
    }
    
    private void makeTree() {
	
	/* Merge */
	add( new DataRootResourceObject( _model ) );
	
	/* Database */
	//		add( new DatabaseRootResourceObject( _model ) );
	    
	/* Mapping Tree */
	//		add( new MappingTreeRootResourceObject( _model ) );
	    
	//XXX Replication
	_repNode = new ReplicationResourceObject(
			_resource.getString("resourcepage",
					    "ReplicationAgreements"),
			groupIcon, groupIconL, _model);
	add( _repNode );

	/* Schema definition */
	add( new SchemaResourceObject( _model ) );
	    

	/* Logging Information */
	add( new LogConfigResourceObject( _model ) );
	    
	/* Plugins */
	add( new PluginResourceObject( _model ) );
    }

    public Component getCustomPanel() {
	if ( _panel == null )
	    _panel = new RootPanel( _model );
	return _panel;
    }

    public ReplicationResourceObject getReplicationObject() {
	return _repNode;
    }

	/**
     *  handle incoming event
     *	 
	 *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH  ) ) {			
			reload();
			refreshTree();
			_model.setSelectedNode(this);
		}
	}

	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
		return new IMenuItem[]{			
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description")),
				};
	}
	

    public void actionMenuSelected(IPage viewInstance, IMenuItem item){
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((DSResourceModel)_model).actionMenuSelected(viewInstance, item);
		}
	}


    /**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }

	void reload() {
		cleanTree();
		makeTree();
		_isLoaded = true;
	}

   /**
     * see if the node has been loaded
     * @return true, if loaded
     */
	public boolean isLoaded() {
	    return _isLoaded;
	}


    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

	/**
	 * Called when this object is selected.
     * Called by: ResourceModel
	 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		super.select( viewInstance );
	}

	/**
	 *	Called when user wants to execute this object, invoked by a
	 *  double-click or a menu action.
	 */
	public boolean run( IPage viewInstance )	{
	    Debug.println( "RootResourceObject.run(" +
					   viewInstance.getClass().getName() + ")" );
        reload();
        if (super.getChildCount() != 0) {
			expandPath((ResourcePage)viewInstance);
        }
        refreshTree();
		return true;
	}

	public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
		return run( viewInstance );
	}

	/**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
		page.expandTreePath(path);
    }

	/**
     * Override isLeaf so node will show up as expandable
     */
    public boolean isLeaf() {
		return false;
    }
	
	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;

	private boolean _isLoaded = false;    
    private static final String serverIconName = "directory.gif";
    private static final String serverIconNameL = "directm.gif";
    private ReplicationResourceObject _repNode = null;
}
