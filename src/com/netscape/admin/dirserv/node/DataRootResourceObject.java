/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.util.Enumeration;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.node.DatabasePluginObject;
import com.netscape.admin.dirserv.node.ChainingPluginObject;
import com.netscape.admin.dirserv.node.RootResourceObject;
import com.netscape.admin.dirserv.panel.DatabaseRootPanel;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.admin.dirserv.panel.NewSuffixPanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import com.netscape.admin.dirserv.panel.replication.ReplicationResourceObject;

/**
 * Representation of the Database Root Node in the Directory Configuration tree
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class DataRootResourceObject extends DSResourceObject
    implements ActionListener,
    IMenuInfo,
    TreeExpansionListener {
 
    public DataRootResourceObject( IDSModel model ) {
        super( _resource.getString("resourcepage","Data"),
	       DSUtil.getPackageImage( databaseIconName ),
	       DSUtil.getPackageImage( databaseIconNameL ),
	       model );
    }

    public Component getCustomPanel() {
	if ( _mainPanel == null ) {
	    // Need to add a switch between known / unknown plugins
	    _mainPanel = new DatabaseRootPanel( _model);
	}
	_panel = _mainPanel;
	return _mainPanel;
    }												 


    /**
     *	Called when user wants to execute this object, invoked by a
     *  double-click or a menu action.
     */
    public boolean run( IPage viewInstance )	{
	Debug.println( "DataRootResourceObject.run(" +
		       viewInstance.getClass().getName() + ")" );
        reload();
        if (super.getChildCount() != 0) {
	    expandPath((ResourcePage)viewInstance);
        }
        refreshTree();
	return true;
    }

    public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
	return run( viewInstance );
    }
	
	
    /**
     * Retrieve list of Plugins from the Directory server
     */
    public void reload() {
	cleanTree();
	addDBPlugin();
	add( new BackupResourceObject( _model ) );   
	addSuffixes();
	refreshTree();
	_isLoaded = true;
    }

    /**
     * Add databse plugin config : share / default conf
     */

    private void addDBPlugin(){
	RemoteImage icon = DSUtil.getPackageImage( _dbinstImageName );
	try {
	    _model.fireChangeFeedbackCursor(
					    null, FeedbackIndicator.FEEDBACK_WAIT );
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    LDAPSearchResults res =
		ldc.search( "cn=plugins, cn=config",
			    ldc.SCOPE_SUB,
			    "nsslapd-plugintype=database",
			    null,
			    false );
	    while( res.hasMoreElements() ) {
		LDAPEntry entry = (LDAPEntry)res.nextElement();
		String name = entry.getDN();
		Debug.println( "Plugin: " + name );
		LDAPAttribute attr = entry.getAttribute( "cn" );
		if ( attr == null )
		    attr = entry.getAttribute( ID );
		if ( attr == null )
		    attr = entry.getAttribute( BCKD );
		if ( attr != null ) {
		    _isLeaf = false;
		    Enumeration en = attr.getStringValues();
		    if ( en.hasMoreElements() ) {
			name = (String)en.nextElement();
		    }
		}
				
		name = getDBServiceName( name );
				
		LDAPAttribute attr_type = entry.getAttribute( ID );
		Debug.println( "attr_type:" + attr_type.toString());
		if( attr_type  !=  null ) {
		    StringBuffer bPType = new StringBuffer();
		    String PType = null;
		    Enumeration en = attr_type.getStringValues();
		    if ( en.hasMoreElements() ) {
			bPType.append((String)en.nextElement());
		    }
		    PType = bPType.toString();
		    Debug.println( "Plugin Type:" + PType);
		    if(PType.compareTo( "ldbm-backend" ) == 0) {
			/* Add a node for each plugin */
			DatabasePluginObject DBplugin = 
			    new DatabasePluginObject(name,
						     icon,
						     _model,
						     entry,
						     true);
			add( DBplugin );
		    } else if(PType.compareTo( "chaining database") == 0) {
			ChainingPluginObject Chaining = 
			    new ChainingPluginObject(name,
						     icon,
						     _model,
						     entry,
						     true);
			add( Chaining );
		    }
		}
	    }
	} catch ( LDAPException e ) {
	    Debug.println( "DataRootResourceObject.reload: " + e );
	} finally {
	    _model.fireChangeFeedbackCursor(
					    null, FeedbackIndicator.FEEDBACK_DEFAULT );
	}
    }

    /**
     * retrieve Database Service Name from resources
     */
    private String getDBServiceName( String sDBName ) {
	/* change name to be compatible with resources format */
	StringBuffer sbName = new StringBuffer();

	for(int i = 0; i < sDBName.length(); i++) {
	    switch( sDBName.charAt( i )) {
	    case ' ':
		sbName.append( '-' );
		break;
	    default:
		sbName.append( sDBName.charAt( i ));
	    }
	}
	return(  _resource.getString("resourcepage", sbName.toString()));
    }

    private void addSuffixes() {
	try {
	    _model.fireChangeFeedbackCursor(
					    null, FeedbackIndicator.FEEDBACK_WAIT );
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    LDAPSearchResults res =
		ldc.search( "cn=mapping tree, cn=config",
			    ldc.SCOPE_SUB,
			    "(&(objectclass=nsMappingTree)(!(nsslapd-parent-suffix=*)))",
			    null,
			    false );
	    while( res.hasMoreElements() ) {
		LDAPEntry entry = (LDAPEntry)res.nextElement();
		String name = MappingUtils.getMappingNodeName(entry);
		Debug.println( "Mapping node: dn=" + entry.getDN() + " name=" + name );
		_isLeaf = false;
		/* Add a node for each plugin */
		SuffixResourceObject MappNode = 
		    new SuffixResourceObject(name,_model,entry);
		add( MappNode );
	    }
	} catch ( LDAPException e ) {
	    Debug.println( "DataRootResourceObject.reload: " + e );
	} finally {
	    _model.fireChangeFeedbackCursor(
					    null, FeedbackIndicator.FEEDBACK_DEFAULT );
	}
    }

    /**
     * Called when this object is selected.
     * Called by: ResourceModel
     */
    public void select(IPage viewInstance) {
	if ( !isLoaded() )
	    reload();
	super.select( viewInstance );
    }

    /**
     * see if the backup folders are loaded
     * @return true, if loaded
     */
    public boolean isLoaded() {
	return _isLoaded;
    }
	
    /**
     * Handle incoming event.
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
	if ( e.getActionCommand().equals( REFRESH ) ) {
	    reload();
	    refreshTree();
	}
    }

	
    public boolean isLeaf() {
	return _isLeaf;
    }

    /**
     * Refresh the tree view
     */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }

    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
	TreePath path = new TreePath(getPath());
	page.expandTreePath(path);
    }
    
    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

    /**
     * Tree expansion events
     */
    public void treeExpanded( TreeExpansionEvent tee ) {
	IResourceObject o =
	    (IResourceObject)tee.getPath().getLastPathComponent();
	if ( equals( o ) ) {
	    if ( !_isInitiallyExpanded ) {
		/* Prevent recursion caused by refreshTree() in reload() */
		_isInitiallyExpanded = true;
		Debug.println( "PluginResourceObject.treeExpanded: this" );
		if ( !isLoaded() ) {
		    reload();
		}
	    }
	}
    }

    public void treeCollapsed( TreeExpansionEvent tee ) {
    }


    //============  IMENUINFO  ==================
    public String[] getMenuCategoryIDs() {
	return new String[] {
	    ResourcePage.MENU_OBJECT,
	    ResourcePage.MENU_CONTEXT
	};
    }
											   
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
	return new IMenuItem[] {
	    new MenuItemText( NEWMAPP,
			      _resource.getString("menu", "newrootmappingnode"),
			      _resource.getString("menu",
						  "newmappingnode-description")),
			new MenuItemSeparator(),
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description"))
		};
    }
											   

    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((IMenuInfo)_model).actionMenuSelected(viewInstance, item);
		} else if( item.getID().equals(NEWMAPP)) {
			create_mapping_node( null );
			reload();
			refreshReplication();
		}
    }
	

    private void create_mapping_node( LDAPEntry node) {
	NewSuffixPanel child = new NewSuffixPanel(_model, node, true);
	SimpleDialog dlg = new SimpleDialog( _model.getFrame(),
					     child.getTitle(),
					     SimpleDialog.OK |
					     SimpleDialog.CANCEL |
					     SimpleDialog.HELP,
					     child );
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	child.init();
	dlg.setFocusComponent( child.getFocusComponent() );
	dlg.getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString("mappingtree-new-root",
																				   "description"));
	dlg.packAndShow();
    }

    public void refreshReplication(){
	refreshReplication( this );
    }
    private void refreshReplication( TreeNode startingNode ){
	if(startingNode == null) return;

	TreeNode dtn = startingNode.getParent();
	while(dtn != null) {
	    if(dtn instanceof RootResourceObject){
		break;
	    }
	    dtn = dtn.getParent();
	}
	if(dtn != null ){
	    ReplicationResourceObject tmpNode = (ReplicationResourceObject)((RootResourceObject)dtn).getReplicationObject();
	    if(tmpNode != null) {		    
		tmpNode.refreshReplicationNode();
	    }
	} else {
	    Debug.println("DataRootResourceObject.refreshReplicationNode() No RootResourceObject found");
	}
    }

    /*==========================================================
     * variables
     *==========================================================*/

	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;

    private boolean _isLoaded = false;
    private boolean _isInitiallyExpanded = false;
    private boolean _isLeaf = false;
    private static final String _dbinstImageName = "plugin.gif";
    private static final String ID = "nsslapd-pluginid";
    private static final String BCKD = "nsslapd-backend";
    static final String IMPORT = "import";
    static final String EXPORT = "export";
    static final String NEWDB  = "newbackend";
    /*	private static LDAPControl _manageDSAITControl =
	new LDAPControl( LDAPControl.MANAGEDSAIT, true, null );
    */
    static final String NEWMAPP  = "newmappingnode";
    private static String _dbFilter;
    private static final String databaseIconName = "dbcontainer.gif";
    private static final String databaseIconNameL = "dbcontainerL.gif";
    private static final String ENABLED = "nsslapd-pluginenabled";
    static final String REFRESH = "refresh";
    static final String RESTORE = "restore";
    private DatabaseRootPanel _mainPanel = null;
}


