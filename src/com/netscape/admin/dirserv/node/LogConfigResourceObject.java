/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.awt.Component;
import java.awt.event.*;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.LogFileConfigurePanel;

/**
 * Representation of the Access Log Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class LogConfigResourceObject extends DSResourceObject 
                                     implements TreeExpansionListener, ActionListener, IMenuInfo {
	public LogConfigResourceObject( IDSModel model ) {
        super( _resource.getString("resourcepage","Logs"),
				DSUtil.getPackageImage( logGroupIconName ),
				DSUtil.getPackageImage( logGroupIconNameL ),
				model );
        setAllowsChildren(true) ;		
	}


	private void makeTree() {		
		add( new AccessLogConfigResourceObject( _model ) );
		add( new ErrorLogConfigResourceObject( _model ) );
		add( new AuditLogConfigResourceObject( _model ) );
	}

	/**
     *  handle incoming event
     *	 
	 *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {			
			reload();			
			_model.setSelectedNode(this);
		}
	}

	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
		return new IMenuItem[]{			
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description")),
				};
    }

    public void actionMenuSelected(IPage viewInstance, IMenuItem item){
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((DSResourceModel)_model).actionMenuSelected(viewInstance, item);
		}
	}	 

	void reload() {
		_isLoaded = true;
		cleanTree();
		makeTree();
		_model.fireTreeStructureChanged(this);	
	}

   /**
     * see if the node has been loaded
     * @return true, if loaded
     */
	public boolean isLoaded() {
	    return _isLoaded;
	}


    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

	/**
	 * Called when this object is selected.
     * Called by: ResourceModel
	 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		super.select( viewInstance );
	}

	/**
	 *	Called when user wants to execute this object, invoked by a
	 *  double-click or a menu action.
	 */
	public boolean run( IPage viewInstance )	{
	    Debug.println( "LogConfigResourceObject.run(" +
					   viewInstance.getClass().getName() + ")" );
        reload();
        if (super.getChildCount() != 0) {
			expandPath((ResourcePage)viewInstance);
        }        
		return true;
	}

	public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
		return run( viewInstance );
	}

	/**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
		page.expandTreePath(path);
    }

	/**
     * Override isLeaf so node will show up as expandable
     */
    public boolean isLeaf() {
		return false;
    }
	
	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( equals( o ) ) {		
			if ( !isLoaded() ) {
				reload();				
			}
		}
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {
	}	

	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;

	private boolean _isLoaded = false;
	private static final String logGroupIconName = "logs.gif";
	private static final String logGroupIconNameL = "logsL.gif";
	static final String logIconName = "logobject.gif";
	static final String logIconNameL = "logobjectL.gif";
}
