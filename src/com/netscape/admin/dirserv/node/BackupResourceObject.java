/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Icon;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.admin.dirserv.task.ListDB;
import com.netscape.admin.dirserv.task.Restore;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.GalleryPanel;
import com.netscape.admin.dirserv.task.Backup;

/**
 * Representation of the Backup Folder Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	01/14/98
 * @see     com.netscape.management.client.ResourceObject
 */
public class BackupResourceObject extends DSResourceObject
                                  implements IMenuInfo,
                                             TreeExpansionListener,
                                             ActionListener {
    
    public BackupResourceObject( IDSModel model) {
        super( _resource.getString("resourcepage","Backups"),
				DSUtil.getPackageImage( backupsIconName ),
				DSUtil.getPackageImage( backupsIconNameL ),
				model );
    }

	public Component getCustomPanel() {
		if ( _blankPanel == null ) {
			_blankPanel = new GalleryPanel( _model );
		}
		return _blankPanel;
	}

	/**
	 *	Called when user wants to execute this object, invoked by a
	 *  double-click or a menu action.
	 */
	public boolean run( IPage viewInstance )	{
	    Debug.println( "BackupResourceObject.run(" +
					   viewInstance.getClass().getName() + ")" );
        reload();
        if (super.getChildCount() != 0) {
			expandPath((ResourcePage)viewInstance);
        }
        refreshTree();
		return true;
	}

	public boolean run(IPage viewInstance, IResourceObject selectionList[]) {
		return run( viewInstance );
	}
	
    /**
     * Retrieve list of backup folders from the Directory/Admin server
     */
	public void reload() {
		try {
			_model.setWaitCursor( true );
			cleanTree();
			ListDB task = new ListDB( _model.getServerInfo() );
			String[] backups = task.getBackupList();
			if ( backups != null ) {
				Icon backupIcon = getBackupIcon();
				Icon backupIconL = getBackupIconL();
				/* Add a node for each backup folder */
				for (int i = 0; i < backups.length; i++) {
					String name = backups[i];
					int ind = name.lastIndexOf( '/' );
					String shortName;
					if ( ind >= 0 )
						shortName = name.substring( ind+1 );
					else
						shortName = name;
					Debug.println( "Add backup node: " + name );
					BackupFolderResourceObject res = 
	                    new BackupFolderResourceObject( shortName,
														backupIcon,
														backupIconL,
														name,
														_model );
					add( res );
				}
				_isLeaf = (backups.length == 0);
			} else {
				_isLeaf = true;
			}
			refreshTree();
			_isLoaded = true;
		} catch ( Exception e ) {
			Debug.println( "BackupResourceObject.reload: " + e );
		} finally {
			_model.setWaitCursor( false );
		}
	}

	/**
	 * Called when this object is selected.
     * Called by: ResourceModel
	 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		if ( (_blankPanel != null) && (_blankPanel instanceof GalleryPanel) ) {
			((GalleryPanel)_blankPanel).select( this, viewInstance );
		}
	}

    /**
     * Called when this object is unselected.
     * Called by: ResourceModel
     */
    public void unselect(IPage viewInstance) {
		if ( (_blankPanel != null) && (_blankPanel instanceof GalleryPanel) ) {
			((GalleryPanel)_blankPanel).unselect( this, viewInstance );
		}
    }

    /**
     * see if the backup folders are loaded
     * @return true, if loaded
     */
	public boolean isLoaded() {
	    return _isLoaded;
	}
	
 	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
		return new IMenuItem[]
		{
			new MenuItemText(
				BACKUP,
				_resource.getString("menu", "backup"),
				_resource.getString("menu", "backup-description")),
			new MenuItemSeparator(),
			new MenuItemText(
				REFRESH,
				_resource.getString("menu", "refresh"),
				_resource.getString("menu", "refresh-description"))
		};
	}


	public void actionMenuSelected(IPage viewInstance, IMenuItem item)	{
	    Debug.println( "BackupResourceObject.actionMenuSelected(" +
					   viewInstance.getClass().getName() + "," + item + ")" );
		if( item.getID().equals(REFRESH) ) {
		    run(viewInstance);
		} else if( item.getID().equals(BACKUP) ) {
			Backup task = new Backup();
			task.setConsoleInfo( _model.getServerInfo() );
			task.run( viewInstance );
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("refresh")) {
			reload();
			refreshTree();
		}		
	}
	
	public boolean isLeaf() {
		return _isLeaf;
	}

    /**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }
        
    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
		page.expandTreePath(path);
    }
    
    /**
     * Remove all folders from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

    private Icon getBackupIcon() {
		if ( _backupIcon == null ) {
			_backupIcon = DSUtil.getPackageImage( _defaultIconName );
		}
		return _backupIcon;
	}

    private Icon getBackupIconL() {
		if ( _backupIconL == null ) {
			_backupIconL = DSUtil.getPackageImage( _defaultIconNameL );
		}
		return _backupIconL;
	}

	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( equals( o ) ) {
			if ( !_isInitiallyExpanded ) {
				/* Prevent recursion caused by refreshTree() in reload() */
				_isInitiallyExpanded = true;
				Debug.println( "PluginResourceObject.treeExpanded: this" );
				if ( !isLoaded() ) {
					reload();
				}
			}
		}
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {
	}

    /*==========================================================
     * variables
     *==========================================================*/
    private boolean _isLoaded = false;
    private boolean _isInitiallyExpanded = false;
    private Icon _backupIcon = null;
    private Icon _backupIconL = null;
	private boolean _isLeaf = false;
	private static final String BACKUP = "backup";
	private static final String backupsIconName = "backupc.gif";
	private static final String backupsIconNameL = "backupcL.gif";
	private static final String _defaultIconName = "backup.gif";
	private static final String _defaultIconNameL = "backupL.gif";
    
	//get resource bundle
    private static ResourceSet _resource = DSUtil._resource;


	/* Nested class implements a single backup folder resource object */
	class BackupFolderResourceObject extends ResourceObject
                                     implements IMenuInfo {
    
        public BackupFolderResourceObject( String sDisplayName,
										   Icon icon, Icon largeIcon,
										   String name,
										   IDSModel model) {
			super( sDisplayName, icon, largeIcon );
			_name = name;
			_backupIcon = icon;
			_backupIconL = largeIcon;
			_model = model;
		}

 	    public Component getCustomPanel() {
			return _blankPanel;
		}

		/**
		 *	Called when user wants to execute this object, invoked by a
		 *  double-click or a menu action.
		 */
	    public boolean run( IPage viewInstance )	{
			Debug.println( "BackupResourceFolderObject.run(" +
						   viewInstance.getClass().getName() + ")" );
			return true;
		}

		/**
		 * Implement IMenuInfo Interface
		 */
		public String[] getMenuCategoryIDs(){
			if (this._categoryID == null) {
				this._categoryID = new String[]  {
					ResourcePage.MENU_OBJECT,
						ResourcePage.MENU_CONTEXT
						};
			} 
			return this._categoryID;
		}
										 
		public IMenuItem[] getMenuItems(String category) {
			if (category.equals(ResourcePage.MENU_CONTEXT)) {
				if (this._contextMenuItems == null) {
					this._contextMenuItems = createMenuItems();
				} 
				return this._contextMenuItems;
			} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
				if (this._objectMenuItems == null) {
					this._objectMenuItems = this.createMenuItems();
				}
				return this._objectMenuItems;			
			}
			return null;
		}
								 
		private IMenuItem[] createMenuItems() {
			return new IMenuItem[]
			{
				new MenuItemText( RESTORE,
								  _resource.getString("menu", "restore"),
								  _resource.getString("menu",
													  "restore-description") )
			};
		}

	    public void actionMenuSelected(IPage viewInstance, IMenuItem item)	{
			Debug.println( "BackupResourceFolderObject.actionMenuSelected(" +
						   viewInstance.getClass().getName() + "," +
						   item + ")" );
			if (item.getID().equals(RESTORE)) {
				Debug.println( "Restoring " + _name );
				Restore task = new Restore();
				task.setConsoleInfo( _model.getServerInfo() );
				boolean status = task.restore( _model.getSelectedPage(),
											   _name );
			}
		}
		protected String[] _categoryID;
		protected IMenuItem[] _contextMenuItems;
		protected IMenuItem[] _objectMenuItems;	

        private String _name = null;	
        private IDSModel _model = null;
	}

	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;
    static JPanel _blankPanel = null;
	static final String REFRESH = "refresh";
	static final String RESTORE = "restore";
}
