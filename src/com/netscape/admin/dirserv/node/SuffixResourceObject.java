/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.util.Enumeration;
import java.util.Vector;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSResourcePage;
import com.netscape.admin.dirserv.panel.MappingNodePanel;
import com.netscape.admin.dirserv.panel.NewSuffixPanel;
import com.netscape.admin.dirserv.panel.NewLDBMPanel;
import com.netscape.admin.dirserv.panel.NewChainingPanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import com.netscape.admin.dirserv.panel.MappingUtils;
import com.netscape.admin.dirserv.panel.BlankPanel;
import com.netscape.admin.dirserv.panel.confirmDeleteSuffixPanel;
import com.netscape.admin.dirserv.node.LDBMDatabaseObject;
import com.netscape.admin.dirserv.node.ChainingDatabaseObject;
import com.netscape.admin.dirserv.node.RootResourceObject;
import com.netscape.admin.dirserv.panel.replication.ReplicationResourceObject;
import com.netscape.admin.dirserv.GenericProgressDialog;


import netscape.ldap.*;

import netscape.ldap.util.*;

/**
 * Representation of the Database Root Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version 1.4, 03/15/00
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public 	class SuffixResourceObject extends DSResourceObject 
    implements IMenuInfo,
    ActionListener,
    TreeExpansionListener{
    
    
    public SuffixResourceObject( String sDisplayName,
				 IDSModel model,
				 LDAPEntry entry) {
	this(sDisplayName, _icon, model, entry);
    }
    
    
    public SuffixResourceObject( String sDisplayName,
				 RemoteImage icon,
				 IDSModel model,
				 LDAPEntry entry) {

	super( sDisplayName,icon, null, model );
	_entry = entry;
    }

		
    public Component getCustomPanel() {
	if ( _mainPanel == null ) {
	    // Need to add a switch between known / unknown plugins
	    _mainPanel = new MappingNodePanel( _model, _entry);
	}
	_panel = _mainPanel;
	return _mainPanel;
    }

    /**
     *	Called when user wants to execute this object, invoked by a
     *  double-click or a menu action.
     */
    public boolean run( IPage viewInstance )	{
	reload();
	if (super.getChildCount() != 0) {
	    expandPath((ResourcePage)viewInstance);
	}
	refreshTree();
	return true;
    }
    
    
    public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
	return run( viewInstance );
    }

    /**
     * Retrieve list of Plugins from the Directory server
     */
    public void reload() {
	cleanTree();
	//		RemoteImage icon = DSUtil.getPackageImage( _mappingNodeImageName );
	_isLeaf = true;
	addDB();
	addSuffix();
	refreshTree();
	if( _mainPanel != null){
	    _mainPanel.reload(); 
	}
	_isLoaded = true;
    }


    private void addSuffix(){
	String suffix = MappingUtils.getMappingNodeSuffix(_entry);
	String filter = MappingUtils.getParentSearchFilter(suffix);
	try {
	    _model.fireChangeFeedbackCursor(null, FeedbackIndicator.FEEDBACK_WAIT );
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    LDAPSearchResults res =
		ldc.search( "cn=mapping tree, cn=config",
			    ldc.SCOPE_ONE, filter,
			    null,
			    false );
	    while( res.hasMoreElements() ) {
		LDAPEntry bentry = (LDAPEntry)res.nextElement();
		String name = MappingUtils.getMappingNodeName(bentry);
		_isLeaf = false ;
		/* Add a node for each Instance */
		SuffixResourceObject MapNode = 
		    new SuffixResourceObject (name,_model, bentry);
		add(MapNode);
	    }
	} catch ( LDAPException e ) {
	    if( e.getLDAPResultCode() != e.NO_SUCH_OBJECT ) {
		String[] args_m = { suffix , e.toString() };
		DSUtil.showErrorDialog( _model.getFrame(),
					"error-find-subsuffix",
					args_m,
					SECTION);
		Debug.println( "SuffixResourceObject.addSuffix: " + 
			       "suffix: " + suffix +
			       "Error: " + e.toString());
	    }
	} finally {
	    _model.fireChangeFeedbackCursor(null, 
					    FeedbackIndicator.FEEDBACK_DEFAULT );
	}
    }


    private void addDB(){

	StringBuffer bPType = new StringBuffer();;
	RemoteImage icon_ldbm = DSUtil.getPackageImage( _dbinstImageName );
	RemoteImage icon_chaining = DSUtil.getPackageImage( _chinstImageName );

	_model.fireChangeFeedbackCursor(null,
					FeedbackIndicator.FEEDBACK_WAIT );
	LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	String suffix = MappingUtils.getMappingNodeSuffix(_entry);
	String filter = MappingUtils.getSuffixSearchFilter(suffix, "nsslapd-suffix");
	String dbFilter = "(&(objectclass=nsBackendInstance)" + filter + ")";

	try {
	    LDAPSearchResults res =
		ldc.search( DSUtil.PLUGIN_CONFIG_BASE_DN,
			    ldc.SCOPE_SUB,
			    dbFilter,
			    null,
			    false );
				
	    /* get instance list */
	    while( res.hasMoreElements() ){
		_isLeaf = false;
		LDAPEntry bentry = (LDAPEntry)res.nextElement();
		String name = bentry.getDN();
		Debug.println( "Instance db: " + name );
		LDAPAttribute attr = bentry.getAttribute( "cn" );
		if ( attr != null ) {
		    Enumeration en = attr.getStringValues();
		    if ( en.hasMoreElements() ) {
			name = (String)en.nextElement();
		    }
		}
		/* add appropriate db instance node according plugin type */
		switch( DSUtil.DbType( ldc, bentry.getDN())) {
		case DSUtil.LDBM_TYPE:
		    LDBMDatabaseObject DBInst = 
			new LDBMDatabaseObject(name, icon_ldbm,
					       _model, bentry);
		    add(DBInst);
		    break;
		case DSUtil.CHAINING_TYPE:
		    ChainingDatabaseObject CHInst = 
			new ChainingDatabaseObject(name, icon_chaining,
						   _model, bentry);
		    add(CHInst);
		    break;	
		default:
		    Debug.println( "SuffixResourceObject.addDB: unknown db type");
		}
				
	    }
	} catch ( LDAPException e ) {
	    if( e.getLDAPResultCode() != e.NO_SUCH_OBJECT ) {
		String[] args_m = { suffix, e.toString() };
		DSUtil.showErrorDialog( _model.getFrame(),
					"error-find-db",
					args_m,
					SECTION);	
	    }
	} finally {
	    _model.fireChangeFeedbackCursor(null, 
					    FeedbackIndicator.FEEDBACK_DEFAULT );
	}
    }


    /**
     * Called when this object is selected.
     * Called by: ResourceModel
     */
    public void select(IPage viewInstance) {
	if ( !isLoaded() )
	    reload();
	super.select( viewInstance );
    }
											   
    /**
     * see if the backup folders are loaded
     * @return true, if loaded
     */
    public boolean isLoaded() {
	return _isLoaded;
    } 
	
    /**
     * Handle incoming event.
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {
			/* We check if the entry exists...*/
			LDAPEntry entry = null;
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			try {
				entry = ldc.read(_entry.getDN());
				reload();
				refreshTree();
			} catch (LDAPException ex) {
				Debug.println("SuffixResourceObject.actionPerformed() "+ex);
				/* If the entry corresponding to this LDBM database does not exist anymore,
				   we update all the suffix tree */
				if (ex.getLDAPResultCode() == LDAPException.NO_SUCH_OBJECT) {
					TreeNode dtn = getParent();
					if( dtn instanceof SuffixResourceObject) {
						((SuffixResourceObject)dtn).reload();
						((SuffixResourceObject)dtn).refreshTree();
					} else if ( dtn instanceof DataRootResourceObject) {
						((DataRootResourceObject)dtn).reload();
						((DataRootResourceObject)dtn).refreshTree();
					}
					_mainPanel = null;
				}
			}	    
		}    
	}
	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
	return new IMenuItem[] {
	    new MenuItemText( NEWMAPP,
			      _resource.getString("menu", "newmappingnode"),
			      _resource.getString("menu",
						  "newmappingnode-description")),
		new MenuItemText( NEWLDBM,
				  _resource.getString("menu", "newldbm"),
				  _resource.getString("menu",
						      "newldbm-description")),
		new MenuItemText( NEWCHAINING,
				  _resource.getString("menu", "newchaining"),
				  _resource.getString("menu",
						      "newchaining-description")),
		new MenuItemText( DELETE,
				  _resource.getString("menu", "delmappingnode"),
				  _resource.getString("menu",
						      "delmappingnode-description")),
	    new MenuItemSeparator(),
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description"))
			
		};
    }
											   

    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {		
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((IMenuInfo)_model).actionMenuSelected(viewInstance, item);
		} else	if( item.getID().equals(NEWMAPP)) {
	    create_mapping_node( _entry );
	    reload();
	    // refresh agreement list
	    refreshReplication();
	} else if( item.getID().equals( NEWLDBM )){
	    // Need to reload the entry
	    String dn2reread = _entry.getDN();
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try{
		_entry = ldc.read(dn2reread);
	    } catch (LDAPException e){
		String[] args = { dn2reread, e.toString()}; 
		DSUtil.showErrorDialog(_model.getFrame(),
				       "reload-suffix",
				       args,
				       SECTION);
		return;
	    }
	    String[] bckList = DSUtil.getAttrValues(_entry,
						    "nsslapd-backend");
	    if(bckList != null) {
		int resDiag = DSUtil.showConfirmationDialog(_model.getFrame(),
							    "multiple-db",
							    "",
							    SECTION);
		if(resDiag != JOptionPane.OK_OPTION ) {
		    return;
		}
	    }
	    create_ldbm( _entry );
	    reload();		
	    // refresh agreement list
	    refreshReplication();

	} else if( item.getID().equals( NEWCHAINING )){
	    // Need to reload the entry
	    String dn2reread = _entry.getDN();
	    LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	    try{
		_entry = ldc.read(dn2reread);
	    } catch (LDAPException e){
		String[] args = { dn2reread, e.toString()}; 
		DSUtil.showErrorDialog(_model.getFrame(),
				       "reload-suffix",
				       args,
				       SECTION);
		return;
	    }
	     String[] bckList = DSUtil.getAttrValues(_entry,
						    "nsslapd-backend");
	    if(bckList != null) {
		int resDiag = DSUtil.showConfirmationDialog(_model.getFrame(),
							    "multiple-db",
							    "",
							    SECTION);
		if(resDiag != JOptionPane.OK_OPTION ) {
		    return;
		}
	    }
	    create_chaining( _entry );
	    reload();
	} else if( item.getID().equals( DELETE )) {
	    DSResourcePage rp = ( DSResourcePage )viewInstance;

            TreeNode dtn = getParent();
            if( dtn instanceof IResourceObject) {
                _model.setSelectedNode( (IResourceObject)dtn);
            }
            rp.getTree().getSelectionModel().clearSelection();	    
	    if ( delete_suffix( _entry ) > 0) {
                if(dtn instanceof SuffixResourceObject) {
                    ((SuffixResourceObject)dtn).reload();
                } else {
                    ((DataRootResourceObject)dtn).reload();
                }
	    }
	    // refresh agreement list
	    refreshReplication( dtn );
	}
    }
	
    public boolean isLeaf() {
	return _isLeaf;
	//		return (isLeafMappingNode());
    }

    /**
     * Refresh the tree view
     */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }
        
    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
	TreePath path = new TreePath(getPath());
	page.expandTreePath(path);
    }
    
    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

    /**
     * Tree expansion events
     */
    public void treeExpanded( TreeExpansionEvent tee ) {
	IResourceObject o =
	    (IResourceObject)tee.getPath().getLastPathComponent();
	if ( equals( o ) ) {
	    if ( !_isInitiallyExpanded ) {
		/* Prevent recursion caused by refreshTree() in reload() */
		_isInitiallyExpanded = true;
		if ( !isLoaded() ) {
		    reload();
		}
	    }
	}
    }

    public void treeCollapsed( TreeExpansionEvent tee ) {
    }


    private void create_mapping_node( LDAPEntry node) {
	NewSuffixPanel child = new NewSuffixPanel(_model, node);
	SimpleDialog dlg = new SimpleDialog( _model.getFrame(),
					     child.getTitle(),
					     SimpleDialog.OK |
					     SimpleDialog.CANCEL |
					     SimpleDialog.HELP,
					     child );
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	child.init();
	dlg.setFocusComponent( child.getFocusComponent() );
	dlg.getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString("mappingtree-new",
																				   "description"));
	dlg.packAndShow();
    }

    private void create_ldbm( LDAPEntry node) {
	NewLDBMPanel child = new NewLDBMPanel(_model, node);
	SimpleDialog dlg = new SimpleDialog( _model.getFrame(),
					     child.getTitle(),
					     SimpleDialog.OK |
					     SimpleDialog.CANCEL |
					     SimpleDialog.HELP,
					     child );
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	dlg.setDefaultButton( SimpleDialog.OK );
	dlg.getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString("newldbminst",
																				   "description"));
	dlg.packAndShow();
    }

    private void create_chaining( LDAPEntry node) {
	NewChainingPanel child = new NewChainingPanel(_model, node);
	SimpleDialog dlg = new SimpleDialog( _model.getFrame(),
					     child.getTitle(),
					     SimpleDialog.OK |
					     SimpleDialog.CANCEL |
					     SimpleDialog.HELP,
					     child );
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	dlg.setDefaultButton( SimpleDialog.OK );
	dlg.getAccessibleContext().setAccessibleDescription(DSUtil._resource.getString("newchaining",
																				   "description"));
	dlg.packAndShow();
    }

    private int delete_suffix(LDAPEntry node) {
	String suffix = MappingUtils.getMappingNodeSuffix(node);

	confirmDeleteSuffixPanel child =
	    new confirmDeleteSuffixPanel( _model, suffix );
			
	SimpleDialog dlg =
	    new SimpleDialog( _model.getFrame(),
			      child.getTitle(),
			      SimpleDialog.OK |
			      SimpleDialog.CANCEL |
			      SimpleDialog.HELP,
			      child );

	dlg.setComponent( child );
	dlg.setOKButtonEnabled( true );
	dlg.setDefaultButton( SimpleDialog.OK );
	dlg.getAccessibleContext().setAccessibleDescription(_resource.getString("confirmdeletesuffix",
																			"description"));
	dlg.packAndShow();
	
	int resDiag = child.getReturn();
	LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	StringBuffer sbDBLoc = new StringBuffer();	
	switch( resDiag ) {
	case confirmDeleteSuffixPanel.ALL: {
	    del_all_suffix( ldc, suffix, node.getDN(), sbDBLoc);
	    break;
	}
	case confirmDeleteSuffixPanel.ONE:
	    LDAPAttribute dady_attr = node.getAttribute( "nsslapd-parent-suffix");
	    if( dady_attr == null) {
		del_1_suffix( ldc, suffix, node.getDN(), null, true, sbDBLoc);
	    } else {
		Enumeration en = dady_attr.getStringValues();
		if ( en.hasMoreElements() ) {
		    del_1_suffix( ldc, suffix,
				  node.getDN(), (String)en.nextElement(),
				  true, sbDBLoc );
		}
	    }
	    break;
	}
	if( sbDBLoc.toString().length() > 0 ){
	    String[] args_wn = {  sbDBLoc.toString() };
	    DSUtil.showInformationDialog( _model.getFrame(),
					  "del-warning",
					  args_wn,
					  SECTION );
	}
	return resDiag ;	
    }


    private int del_all_suffix( LDAPConnection ldc,
				String dn2delete ,
				String mndn, StringBuffer sbDBLoc) {
	String filter = "(&(objectclass=nsMappingTree)" +
		MappingUtils.getParentSearchFilter(dn2delete) +
		")";
	try {
	    LDAPSearchResults res =
		ldc.search( DSUtil.MAPPING_TREE_BASE_DN,
			    ldc.SCOPE_ONE,
			    filter,
			    null,
			    false );
	    
	    while(( res != null ) && res.hasMoreElements()) {
		Vector v = new Vector();
		LDAPEntry entry = (LDAPEntry)res.nextElement();
		String dn2modify = MappingUtils.getMappingNodeSuffix(entry);
		del_all_suffix( ldc, dn2modify, entry.getDN(), sbDBLoc );
	    }
	} catch( LDAPException e) {
	    if( e.getLDAPResultCode() != e.NO_SUCH_OBJECT ) {
		String[] args_m = { dn2delete, e.toString() };
		DSUtil.showErrorDialog( _model.getFrame(),
					"error-del-find-subsuffix",
					args_m,
					SECTION);	
		return( 0 );
	    }	
	}

	return( del_1_suffix( ldc, dn2delete, mndn, null, false, sbDBLoc));	
	    
    }

    private int del_1_suffix(LDAPConnection ldc,
			     String dn2delete,
			     String mndn,
			     String dndady,
			     boolean changeChildren, StringBuffer sbDBLoc) {
	String[] dlg_arg = { dn2delete };
	GenericProgressDialog dlg = 
	    new GenericProgressDialog(_model.getFrame(), 
				      true, 
				      GenericProgressDialog.NO_BUTTON_OPTION,
				      _resource.getString( SECTION,
							   "del-1-suffix-title",
							   dlg_arg),
				      null,
				      null);
	dlg.addStep(_resource.getString( SECTION,"del-1-check-subsuffix-label"));
	dlg.addStep(_resource.getString( SECTION,"del-1-check-suffix-label"));
	dlg.addStep(_resource.getString( SECTION,"del-1-check-db-label"));
	try { 
	    erase_one_suffix task = 
		new erase_one_suffix( ldc,
				      dn2delete,
				      mndn,
				      dndady,
				      changeChildren,
				      sbDBLoc, dlg );
	    Thread th = new Thread(task);
	    th.start();
	    dlg.packAndShow();	    
	} catch ( Exception e ) {
	    Debug.println("GenericDeleter(): " +
			  e );
	    e.printStackTrace();	
	    return( 0 );
	}
	return( 1 );
    }

    class erase_one_suffix implements  Runnable {

	public erase_one_suffix(LDAPConnection ldc,
				String dn2delete,
				String mndn,
				String dndady,
				boolean changeChildren,
				StringBuffer sbDBLoc, GenericProgressDialog dlg ) {
	    _ldc = ldc;
	    _dn2delete = dn2delete;
	    _mndn = mndn;
	    _dndady = dndady;
	    _changeChildren = changeChildren;
	    _dlg = dlg;
	    _sbDBLoc = sbDBLoc;
	    
	}

	public void run() {
	    boolean  everythingUnderControl = true;
	    if ( _changeChildren ) {
		String filter = "(&(objectclass=nsMappingTree)" +
			MappingUtils.getParentSearchFilter(_dn2delete) +
			")";
		String suffix2delete = _dn2delete;
		try {
		    LDAPSearchResults res =
			_ldc.search( DSUtil.MAPPING_TREE_BASE_DN,
				     _ldc.SCOPE_ONE,
				     filter,
				     null,
				     false );
		
		    while(( res != null ) && res.hasMoreElements()) {
			Vector v = new Vector();
			LDAPEntry entry = (LDAPEntry)res.nextElement();
			String dn2modify = entry.getDN();
			LDAPModificationSet attrs = new LDAPModificationSet();		    
			if( _dndady  == null ) {
			    attrs.add( LDAPModification.DELETE,
				       new LDAPAttribute( "nsslapd-parent-suffix",
							  suffix2delete ));
			} else {
			    suffix2delete = _dndady;
			    attrs.add( LDAPModification.REPLACE,
				       new LDAPAttribute( "nsslapd-parent-suffix",
							  _dndady));
			}
			try {
			    _ldc.modify( dn2modify,
					 attrs ); 
			} catch ( LDAPException e){
			    String[] args_m = { dn2modify,  e.toString() };
			    DSUtil.showErrorDialog(_model.getFrame(),
						   "error-modify-subsuffix",
						   args_m,
						   SECTION);
			    
			    Debug.println("SuffixResourceObject.del_1_suffix(): " +
					  "rdn2 delete or mod = " + suffix2delete );
			    
			    everythingUnderControl = false;
			}
		    }
		} catch(LDAPException e) {
		    if( e.getLDAPResultCode() != e.NO_SUCH_OBJECT ) {
			String[] args_m = { suffix2delete, e.toString() };
			DSUtil.showErrorDialog( _model.getFrame(),
						"error-del-find-subsuffix",
						args_m,
						SECTION);	
			everythingUnderControl = false;
		    }
		}
	    }
	    if( everythingUnderControl ) {
		// check sub suffix 
		this._dlg.stepCompleted(0);
	    } else {
		this._dlg.closeCallBack();
	    }

	    if (!DSUtil.deleteTree ( _mndn, _ldc, false, _dlg )) {
		String[] args_m = { _mndn } ;
		DSUtil.showErrorDialog( _model.getFrame(),
				        "error-remove",
				        args_m,
				        SECTION);
		this._dlg.closeCallBack();
		return;
		
	    } else {	
		// check suffix 
		this._dlg.stepCompleted(1);

		/* now get rid of all its database */
		
		String filter = "(&(objectclass=nsBackendInstance) " +
			MappingUtils.getSuffixSearchFilter(_dn2delete, "nsslapd-suffix") +
			")";
		try {
		    LDAPSearchResults res = 
			_ldc.search( DSUtil.PLUGIN_CONFIG_BASE_DN,
				    _ldc.SCOPE_SUB,
				    filter,
				    null,
				    false );
		    
		    while(( res != null ) && res.hasMoreElements()) {
			Vector v = new Vector();
			LDAPEntry entry = (LDAPEntry)res.nextElement();
			String sLoc = DSUtil.getAttrValue(entry,
							  "nsslapd-directory");
			if(( sLoc != null) && ( sLoc.trim().length() > 0)){
			    _sbDBLoc.append( sLoc );
			    _sbDBLoc.append("\n");
			}
			DSUtil.deleteTree( entry.getDN(), _ldc, false, _dlg );
		    }
		} catch (LDAPException lde) {
		    if( lde.getLDAPResultCode() != lde.NO_SUCH_OBJECT ) {
			String[] args_m = { _dn2delete, lde.toString() };
			DSUtil.showErrorDialog( _model.getFrame(),
						"error-del-find-subsuffix",
						args_m,
						SECTION);	
			everythingUnderControl = false;
		    }		
		}
		if (everythingUnderControl) {
		    this._dlg.stepCompleted(2);
		    try {
			// Pour que l'usager puisse voir que le checkbox a ete coche */
			Thread.sleep(300);
		    } catch (Exception e) {
		    }
		}
		this._dlg.closeCallBack();
	    }
	    return;	    
	}

	LDAPConnection	_ldc;
	String		_dn2delete; // the suffix DN i.e. dc=example,dc=com
	String          _mndn; // the DN of the mapping tree node e.g. cn=dc\3Dexample\2Cdc\3Dcom,cn=mapping tree,cn=config
	String		_dndady; // the parent suffix, if any
	boolean		_changeChildren;
	GenericProgressDialog _dlg;
	StringBuffer _sbDBLoc;
    }
			
    public void refreshReplication(){
	refreshReplication( this );
    }
    private void refreshReplication( TreeNode startingNode ){
	if(startingNode == null) return;

	TreeNode dtn = startingNode.getParent();
	while(dtn != null) {
	    if(dtn instanceof RootResourceObject){
		break;
	    }
	    dtn = dtn.getParent();
	}
	if(dtn != null ){
	    ReplicationResourceObject tmpNode = (ReplicationResourceObject)((RootResourceObject)dtn).getReplicationObject();
	    if(tmpNode != null) {		    
		tmpNode.refreshReplicationNode();
	    }
	} else {
	    Debug.println("SuffixResourceObject.refreshReplicationNode() No RootResourceObject found");
	}
    }
	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;

    private LDAPEntry _entry = null;
    private boolean _isLeaf = false;
    private boolean _isLoaded = false;
    private boolean _isInitiallyExpanded = false;
    private static RemoteImage _icon = DSUtil.getPackageImage( "split-16.gif" );
    private static final String _mappingNodeImageName = "split-16.gif";
    static final String DELETE = "delete_map_node";
    static final String NEWMAPP  = "newmappingnode";
    static final String NEWLDBM  = "newldbm";
    static final String NEWCHAINING = "newchaining";
    /*	private static LDAPControl _manageDSAITControl =
	new LDAPControl( LDAPControl.MANAGEDSAIT, true, null );
    */    
    static final String RESTORE = "restore";
											  
    private MappingNodePanel _mainPanel = null;
    static final String CONFIG_MAPPING = DSUtil.MAPPING_TREE_BASE_DN  ;
    static final String SECTION = "mappingtree";
    static final int YES = JOptionPane.YES_OPTION;
    static final int NO = JOptionPane.NO_OPTION;

    
    private static final String _dbinstImageName = "dbobj.gif";
    private static final String _chinstImageName = "chobj.gif";
} /* end of class SuffixResourceObject */
