/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.awt.Component;
import javax.swing.JPanel;
import com.netscape.management.client.IPage;
import com.netscape.management.client.ResourceObject;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSResourceSelectionListener;

/**
 * Representation of the Log Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class DSResourceObject extends ResourceObject {
	public DSResourceObject( String s,
				 RemoteImage small,
				 RemoteImage large,
				 IDSModel model ) {
	    super( s, small, large );
	    _model = model;
	    if ( groupIcon == null ) {
		groupIcon = DSUtil.getPackageImage( groupIconName );
		groupIconL = DSUtil.getPackageImage( groupIconNameL );
	    }
	}
    /**
     * Default panel is blank, and shared
     */
    public Component getCustomPanel() {
	if ( _blankPanel == null )
	    _blankPanel = new JPanel();
	return _blankPanel;
    }

    /**
     * Called when this object is selected.
     * Called by: ResourceModel
     */
    public void select(IPage viewInstance) {
	Component c = getCustomPanel();
	if ( c instanceof IDSResourceSelectionListener ) {
	    ((IDSResourceSelectionListener)c).select( this, viewInstance );
	}
    }

    static private JPanel _blankPanel = null;
    protected JPanel _panel = null;
    protected IDSModel _model;
    protected static ResourceSet _resource = DSUtil._resource;
    protected static RemoteImage groupIcon = null;
    protected static RemoteImage groupIconL = null;
    private static final String groupIconName = "folder.gif";
    private static final String groupIconNameL = "folderL.gif";
}
