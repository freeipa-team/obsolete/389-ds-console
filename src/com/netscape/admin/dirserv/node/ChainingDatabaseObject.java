/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.util.Enumeration;
import java.util.Vector;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.panel.LDBMPluginPanel;
import com.netscape.admin.dirserv.panel.LDBMInstancePanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import com.netscape.admin.dirserv.panel.DatabaseExportPanel;
import com.netscape.admin.dirserv.panel.ChainingPluginPanel;
import com.netscape.admin.dirserv.panel.BlankPanel;
import com.netscape.admin.dirserv.panel.NewChainingInstancePanel;
import com.netscape.admin.dirserv.panel.ChainingInstancePanel;
import com.netscape.admin.dirserv.panel.MappingUtils;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.admin.dirserv.GenericProgressDialog;


/**
 * Representation of the Database Root Node in the Directory Configuration tree
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public 	class ChainingDatabaseObject extends DSResourceObject
    implements IMenuInfo,
    ActionListener {

    public ChainingDatabaseObject( String sDisplayName,
				   RemoteImage icon,
				   IDSModel model,
				   LDAPEntry entry) {
	super( sDisplayName, icon, null, model );
	_bckLoaded = true;
	_entry = entry;
	_backendname  = sDisplayName;
    }

    public Component getCustomPanel() {
		if (_panel == null) {
			_panel = new ChainingInstancePanel( _model, _entry );
		}
		return _panel;
    }

    public void deleteInstance( LDAPEntry dbInst, String backendname ) {
		
	String[] args = { backendname };			
	int resDiag = DSUtil.showConfirmationDialog( null,
						     "confirm-bck",
						     args,
						     SECTION );
	if (resDiag == NO) return;

	LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
	
	GenericProgressDialog dlg = 
	    new GenericProgressDialog( _model.getFrame(), 
				       true, 
				       GenericProgressDialog.NO_BUTTON_OPTION,
				       _resource.getString( SECTION,
							    "del-ldbm-title",
							    args),
				       null,
				       null);
	dlg.addStep(_resource.getString( SECTION,"del-ldbm-upd-suffix-label"));
	dlg.addStep(_resource.getString( SECTION,"del-ldbm-db-label"));

	try {
	    erase_ldbm task = new erase_ldbm( ldc,
					      dbInst,
					      backendname,
					      dlg);
	    Thread th = new Thread( task );
	    th.start();
	    dlg.packAndShow();
	} catch( Exception e){
	    Debug.println("deleteInstance(): " + e.toString());
	    return;
	}
    } // deleteInstance
											   
    /**
     * Called when this object is selected.
     * Called by: ResourceModel
     */
    public void select(IPage viewInstance) {
	super.select( viewInstance );
    }

    /**
     *      Called when user wants to execute this object, invoked by a
     *  double-click or a menu action.
     */
    public boolean run( IPage viewInstance ) {
	Debug.println( "ChainingDatabaseObject.run(" +
		       viewInstance.getClass().getName() + ")" );
	return true;
    }

	
    /**
     * Handle incoming event.
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {	
		if ( e.getActionCommand().equals( DSResourceModel.REFRESH ) ) {				
			/* We check if the entry exists...*/
			LDAPEntry entry = null;
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			try {
				entry = ldc.read(_entry.getDN());
			} catch (LDAPException ex) {
				Debug.println("ChainingDatabaseObject.actionPerformed() "+ex);
				/* If the entry corresponding to this LDBM database does not exist anymore,
				   we update all the suffix tree */
				if (ex.getLDAPResultCode() == LDAPException.NO_SUCH_OBJECT) {
					TreeNode dtn = getParent();
					if( dtn instanceof IResourceObject) {
						((SuffixResourceObject)dtn).reload();
						((SuffixResourceObject)dtn).refreshTree();
					}
					_panel = null;
				}
			}
		}
    }

											   
	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
	return new IMenuItem[] {				
	    new MenuItemText( DELETE,
			      _resource.getString("menu", "delete"),
			      _resource.getString("menu","delete-description")),
			new MenuItemSeparator(),
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description"))
		};
    }
								 
											   
    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((IMenuInfo)_model).actionMenuSelected(viewInstance, item);
		} else if( item.getID().equals(DELETE) ) {
			deleteInstance(_entry, _backendname);
		}
    }


    class erase_ldbm implements Runnable {

	public erase_ldbm( LDAPConnection ldc,
			   LDAPEntry dbInst,
			   String backendname,
			   GenericProgressDialog dlg){
    
	    _dlg = dlg;
	    _ldc = ldc;
	    _backendname = backendname;
	    _dbInst = dbInst;
	}

	public void run() {
	    boolean  everythingUnderControl = true;
		
	
	    String  dnInst = _dbInst.getDN();			
	    // Delete mapping tree 
	    try {
		LDAPSearchResults res =
		    _ldc.search( MappingUtils.CONFIG_MAPPING,
				 _ldc.SCOPE_SUB,
				 "nsslapd-backend="+ _backendname,
				 null,
				 false );
			
		while( res.hasMoreElements() ) {
		    boolean shouldBeDisable = false;
		    LDAPEntry bentry = (LDAPEntry)res.nextElement();
		    String name = bentry.getDN();
		    Debug.println( "Instance db: " + name );
				
		    // Check that's the only instance of db in this node
		    LDAPAttribute attr_status = bentry.getAttribute( "nsslapd-state" );
		    LDAPAttribute attr_db = bentry.getAttribute( "nsslapd-backend" );
		    // Do we need to disable the mapping tree node ?
		    if ( attr_status != null ) {
			Debug.println( "Node type : " + attr_status.getStringValueArray()[0]);
			shouldBeDisable = 
			    (attr_status.getStringValueArray()[0].compareToIgnoreCase( DISABLE ) != 0) &&
			    ( attr_db.size() == 1 );
			
		    }
		    
		    Debug.println( " attr_db:" + attr_db);
		    if((attr_db != null) && shouldBeDisable ) {
			LDAPModificationSet mods = new LDAPModificationSet();
			Debug.println(" +++++++ must be disable");
			LDAPAttribute Mapping_status = new LDAPAttribute( "nsslapd-state",
									  MappingUtils.DISABLE );
			mods.add( LDAPModification.REPLACE, Mapping_status );
			try {
			    _ldc.modify(name,  mods );
			    Debug.println(" +++++++ OKAAAAYY !");   
			} catch (LDAPException e_map) {
			    String[] args_map = { _backendname, name,  e_map.toString()} ;
			    DSUtil.showErrorDialog(_model.getFrame(),
						   "mod-mapping-status",
						   args_map,
						   SECTION);
			    
			    Debug.println(" +++++++ aie !");
			} // catch
		    }
		    // Now remove db from mapping tree
		    LDAPModificationSet mods = new LDAPModificationSet();
		    LDAPAttribute backend_instMapping = new LDAPAttribute( "nsslapd-backend",
									   _backendname );
		    mods.add( LDAPModification.DELETE, backend_instMapping );
		    String[] args_mapping = { _backendname, name };
		    try {
			_ldc.modify(name,  mods );
		    } catch (LDAPException e_map) {
			String[] args_map = { _backendname, name,  e_map.toString()} ;
			DSUtil.showErrorDialog(_model.getFrame(),
					       "mod-mapping-db",
					       args_map,
					       SECTION);
			
		    } // catch
		} // while
	    } catch ( LDAPException e_search ) {
		String[] args_map = { _backendname, e_search.toString()} ;
		DSUtil.showErrorDialog(_model.getFrame(),
				       "search-mapping",
				       args_map,
				       SECTION);
		_dlg.closeCallBack();
		return;
	    }
	    _dlg.stepCompleted(0);
	    
	    TreeNode dtn = getParent();
	    if( dtn instanceof IResourceObject) {
		_model.setSelectedNode( (IResourceObject)dtn);
	    }
	    if( DSUtil.deleteTree( dnInst, _ldc, false, _dlg )) {
		if(dtn instanceof SuffixResourceObject) { 
		    ((SuffixResourceObject)dtn).reload();
		}
		_dlg.stepCompleted(1);
		try {
		    Thread.sleep(300);
		} catch (Exception e) {
		}
		_dlg.closeCallBack();
	    }	    
	}
	LDAPConnection _ldc;
	LDAPEntry _dbInst;
	String _backendname;
	GenericProgressDialog _dlg;
    }
	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;								 
    private boolean _isLeaf = false;
    private boolean _isLoaded = false;
    private boolean _isInitiallyExpanded = false;
    private LDAPEntry _entry = null;
    private boolean _bckLoaded = false;
    private String _backendname;		
    protected String _section = "";
    static final String SECTION = "deletedb";
    static final int YES = JOptionPane.YES_OPTION;
    static final int NO = JOptionPane.NO_OPTION;
    private static final String _dbinstImageName = "dbobj.gif";
    static final String DELETE = "delete_instance";	
    static final String DISABLE = MappingUtils.DISABLE;
    
} /* end of class ChainingDatabaseObject */
