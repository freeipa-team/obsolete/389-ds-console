/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.util.*;
import java.util.Enumeration;
import java.util.Vector;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.EventListenerList;
import com.netscape.management.client.*;
import com.netscape.management.client.util.Debug;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.MonitorDatabasePanel;
import com.netscape.admin.dirserv.panel.MonitorServerPanel;
import com.netscape.admin.dirserv.panel.ForwardingContainerPanel;


/**
 * Panel for Directory Server resource page
 *
 * @author  rmarco
 * @version %I%, %G%
 * @date	 	7/26/00
 * @see     com.netscape.admin.dirserv
 */
public class MonitorResourceObject extends DSResourceObject 
    implements ActionListener, TreeExpansionListener{

    public MonitorResourceObject( IDSModel model ) {
	super( _resource.getString("resourcepage","PerformanceCounters"),
	       DSUtil.getPackageImage( monitorIconName ),
	       DSUtil.getPackageImage( monitorIconNameL ),
	       model );
    }

    public Component getCustomPanel() {		
	if ( _panel == null ) {
	    _panel =  new ForwardingContainerPanel( _model,
						    new MonitorServerPanel( _model ),
						    true );
		((ForwardingContainerPanel)_panel).getOKButton().setVisible(false);
		((ForwardingContainerPanel)_panel).getCancelButton().setVisible(false);
	}
	return _panel;
    }

	
    public boolean run( IPage viewInstance )	{
	
	Debug.println( "MonitorResourceObject.run(" +
		       viewInstance.getClass().getName() + ")" );
	reload();
	if (super.getChildCount() != 0) {
	    expandPath((ResourcePage)viewInstance);
	}
	refreshTree();
	return true;
    }
    
    public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
	return run( viewInstance );
    }
    
    public void reload() {
	cleanTree();
	_isLeaf = true;
	//	if ( !_monInitialized ) {
	    /* Find out how many backends there are, and what their
	       names are */
	    LDAPConnection ldc =
		_model.getServerInfo().getLDAPConnection();
	    try {
		LDAPEntry entry = ldc.read( "cn=monitor" );
		if ( entry == null ) {
		    Debug.println( "MonitorResourceObject.reload: unable to " +
				   "read cn=monitor" );
		    return;
		}
		_monInitialized = true;
		LDAPAttribute attr = entry.getAttribute( "backendmonitordn" );
		int nDatabases = attr.size();
		Enumeration en = attr.getStringValues();
		Debug.println( "MonitorResourceObject.reload en.hasMoreElements :" +
			       en.hasMoreElements());
		while( en.hasMoreElements() ) {
		    _isLeaf = false;
		    String dn = (String)en.nextElement();
		    DN monitorDN = new DN( dn );
		    if( monitorDN.isDescendantOf( new DN(DSUtil.LDBM_BASE_DN ))){
			MonitorDbResourceObject MonBack =
			    new MonitorDbResourceObject( _model, dn);
			add( MonBack );
		    } else if (monitorDN.isDescendantOf( new DN(DSUtil.CHAINING_CONFIG_BASE_DN ))){
			// for RTM maybe 
		    }
		}
	    } catch ( LDAPException e ) {
		Debug.println( "MonitorResourceObject.reload: " + e );
	    }
	    //	}		
	refreshTree();
	_isLoaded = true;
    }

    public void select(IPage viewInstance) {
	if ( !isLoaded() )
	    reload();	
	super.select( viewInstance );
    }
 
    /**
     * see monitor info are loaded
     * @return true, if loaded
     */
    public boolean isLoaded() {
	return _isLoaded;
    } 

    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( DSStatusResourceModel.REFRESH ) ) {			
			reload();			
		}
    }

    /**
     * Refresh the tree view
     */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }
    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
	TreePath path = new TreePath(getPath());
	page.expandTreePath(path);
    }
    
    /**
     * Tree expansion events
     */
    public void treeExpanded( TreeExpansionEvent tee ) {
	IResourceObject o =
	    (IResourceObject)tee.getPath().getLastPathComponent();
	if ( equals( o ) ) {
	    if ( !_isInitiallyExpanded ) {
		/* Prevent recursion caused by refreshTree() in reload() */
		_isInitiallyExpanded = true;
		Debug.println( "MonitorResourceObject.treeExpanded: this" );
		if ( !isLoaded() ) {
		    reload();
		}
	    }
	}
    }


    public boolean isLeaf() {
	return _isLeaf;
    }


    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

    public void treeCollapsed( TreeExpansionEvent tee ) {
    }

    private boolean _isLeaf = false;
    private boolean _isLoaded = false;
    private boolean _monInitialized = false;

    private static final String monitorIconName = "monitors.gif";
    private static final String monitorIconNameL = "monitorsL.gif";    
    private boolean _isInitiallyExpanded = false;

}
