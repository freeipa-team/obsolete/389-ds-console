/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.awt.Component;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.*;
import com.netscape.admin.dirserv.DSResourceModel;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.SchemaManagementPanel;

/**
 * Representation of the Schema Node in the Directory Configuration tree
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class SchemaResourceObject extends DSResourceObject implements IMenuInfo {
	public SchemaResourceObject( IDSModel model ) {
		super( _resource.getString("resourcepage","Schema"),
			groupIcon, groupIconL, model );
	}
	public Component getCustomPanel() {
		if ( _panel == null )
			_panel = new SchemaManagementPanel( _model );
		return _panel;
	}


	/**
	 * Implement IMenuInfo Interface
	 */
    public String[] getMenuCategoryIDs(){
		if (_categoryID == null) {
			_categoryID = new String[]  {
				ResourcePage.MENU_OBJECT,
					ResourcePage.MENU_CONTEXT
					};
		} 
		return _categoryID;
	}
		
	public IMenuItem[] getMenuItems(String category) {	   
		if (category.equals(ResourcePage.MENU_CONTEXT)) {
			if (_contextMenuItems == null) {
				_contextMenuItems = createMenuItems();
			} 
			return _contextMenuItems;
		} else if (category.equals(ResourcePage.MENU_OBJECT)) { 
			if (_objectMenuItems == null) {
				_objectMenuItems = createMenuItems();
			}
			return _objectMenuItems;			
		}
		return null;
	}

	private IMenuItem[] createMenuItems() {
		return new IMenuItem[] {
			new MenuItemText( DSResourceModel.REFRESH,
							  DSUtil._resource.getString("menu", "refresh"),
							  DSUtil._resource.getString("menu",
														 "refresh-description")),
				};
    }

    public void actionMenuSelected(IPage viewInstance, IMenuItem item){
		if (item.getID().equals(DSResourceModel.REFRESH)) {
			((DSResourceModel)_model).actionMenuSelected(viewInstance, item);
		}
	}
	protected String[] _categoryID;
	protected IMenuItem[] _contextMenuItems;
	protected IMenuItem[] _objectMenuItems;
}
