/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.node;

import java.util.Enumeration;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.tree.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.node.MappingNodeObject;
import com.netscape.admin.dirserv.panel.NewMappingNodePanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import com.netscape.admin.dirserv.panel.MappingUtils;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * Representation of the Mapping Tree Node in the Directory Configuration tree
 *
 * @author  rmarco
 * @version 1.1, 02/18/00
 * @date	05/07/98
 * @see     com.netscape.management.client.ResourceObject
 */

public class MappingTreeRootResourceObject extends DSResourceObject
												implements IMenuInfo,
												ActionListener,
												TreeExpansionListener {
 
	public MappingTreeRootResourceObject( IDSModel model ) {
        super( _resource.getString("resourcepage","Mapping"),
				DSUtil.getPackageImage( mappingIconName ),
				DSUtil.getPackageImage( mappingIconNameL ),
				model );
	}

													//	public Component getCustomPanel() {
													//	}												 


	/**
	 *	Called when user wants to execute this object, invoked by a
	 *  double-click or a menu action.
	 */
	public boolean run( IPage viewInstance )	{
	    Debug.println( "MappingTreeRootResourceObject.run(" +
					   viewInstance.getClass().getName() + ")" );
        reload();
        if (super.getChildCount() != 0) {
			expandPath((ResourcePage)viewInstance);
        }
        refreshTree();
		return true;
	}

	public boolean run( IPage viewInstance, IResourceObject selectionList[] ) {
		return run( viewInstance );
	}
	
	
    /**
     * Retrieve list of Plugins from the Directory server
     */
	public void reload() {
	    cleanTree();
		// RemoteImage icon = DSUtil.getPackageImage( mappingIconName );
		_isLeaf = true;
		try {
			_model.fireChangeFeedbackCursor(
				null, FeedbackIndicator.FEEDBACK_WAIT );
			LDAPConnection ldc = _model.getServerInfo().getLDAPConnection();
			LDAPSearchResults res =
				ldc.search( "cn=mapping tree, cn=config",
							ldc.SCOPE_SUB,
							"(&(objectclass=nsMappingTree)(!(nsslapd-parent-suffix=*)))",
							null,
							false );
			while( res.hasMoreElements() ) {
				LDAPEntry entry = (LDAPEntry)res.nextElement();
				String name = MappingUtils.getMappingNodeName(entry);
				Debug.println( "Mapping node: dn=" + entry.getDN() + " name=" + name );
				_isLeaf = false;
				/* Add a node for each plugin */
				MappingNodeObject MappNode = 
	                    new MappingNodeObject(name,_model,entry);
				add( MappNode );
	        }
		} catch ( LDAPException e ) {
			Debug.println( "MappingTreeRootResourceObject.reload: " + e );
		} finally {
			_model.fireChangeFeedbackCursor(
					null, FeedbackIndicator.FEEDBACK_DEFAULT );
		}
		refreshTree();
		_isLoaded = true;
	}


	/**
	 * Called when this object is selected.
     * Called by: ResourceModel
	 */
	public void select(IPage viewInstance) {
		if ( !isLoaded() )
			reload();
		super.select( viewInstance );
	}

    /**
     * see if the backup folders are loaded
     * @return true, if loaded
     */
	public boolean isLoaded() {
	    return _isLoaded;
	}
	
    /**
     * Handle incoming event.
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
		if ( e.getActionCommand().equals( REFRESH ) ) {
			reload();
			refreshTree();
		}
    }

    //============  IMENUINFO  ==================
	public String[] getMenuCategoryIDs() {
		return new String[] {
			ResourcePage.MENU_OBJECT,
			ResourcePage.MENU_CONTEXT
		};
	}
	
	public IMenuItem[] getMenuItems(String category) {
		// return the same thing for both menu categories
		return new IMenuItem[] {
			new MenuItemText( NEWMAPP,
							  _resource.getString("menu", "newmappingnode"),
							  _resource.getString("menu",
												  "newmappingnode-description"))
				};
	}

	
	public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
		if( item.getID().equals(NEWMAPP) ) {
			NewMappingNodePanel child = new NewMappingNodePanel(_model, null);
			SimpleDialog dlg = new SimpleDialog( _model.getFrame(),
												 child.getTitle(),
												 SimpleDialog.OK |
												 SimpleDialog.CANCEL |
												 SimpleDialog.HELP,
												 child );
			dlg.setComponent( child );
			dlg.setOKButtonEnabled( false );
			dlg.setDefaultButton( SimpleDialog.OK );
			dlg.packAndShow();
			reload();
		}
	}	
	
	public boolean isLeaf() {
		return _isLeaf;
	}

    /**
	 * Refresh the tree view
	 */
    void refreshTree() {
        _model.fireTreeStructureChanged(this);
    }
        
    /**
     * Expand the tree path view
     */
    private void expandPath(ResourcePage page) {
		TreePath path = new TreePath(getPath());
		page.expandTreePath(path);
    }
    
    /**
     * Remove all nodes from the tree model
     */
    private void cleanTree() {
        removeAllChildren();
    }

	/**
	 * Tree expansion events
	 */
	public void treeExpanded( TreeExpansionEvent tee ) {
		IResourceObject o =
			(IResourceObject)tee.getPath().getLastPathComponent();
		if ( equals( o ) ) {
			if ( !_isInitiallyExpanded ) {
				/* Prevent recursion caused by refreshTree() in reload() */
				_isInitiallyExpanded = true;
				Debug.println( "PluginResourceObject.treeExpanded: this" );
				if ( !isLoaded() ) {
					reload();
				}
			}
		}
	}

	public void treeCollapsed( TreeExpansionEvent tee ) {
	}


    /*==========================================================
     * variables
     *==========================================================*/

    private boolean _isLoaded = false;
    private boolean _isInitiallyExpanded = false;
	private boolean _isLeaf = false;
	private static final String ID = "nsslapd-pluginid";
	private static final String BCKD = "nsslapd-backend";
	static final String NEWMAPP  = "mappingtree";
			/*	private static LDAPControl _manageDSAITControl =
			new LDAPControl( LDAPControl.MANAGEDSAIT, true, null );
			*/
	private static String _dbFilter;
	private static final String mappingIconName = "mappnode.gif";
	private static final String mappingIconNameL = "mappnodeL.gif";
	private static final String ENABLED = "nsslapd-pluginenabled";
    static final String REFRESH = "refresh";
	static final String RESTORE = "restore";
}


