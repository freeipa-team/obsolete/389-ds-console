/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.DSResourceEditor;
import com.netscape.management.nmclf.SuiLookAndFeel;
import com.netscape.admin.dirserv.IDSModel;
import com.netscape.admin.dirserv.DSUtil;
import netscape.ldap.*;
import netscape.ldap.util.DN;

/**
 * RoleEditorDialog
 * The dialog which displays the roles associated to an LDAP entry.
 **/

public class RoleEditorDialog extends AbstractDialog
							  implements ActionListener, ListSelectionListener {

	
	/**
	 * Construct an new RoleEditorDialog.
	 * The target LDAP entry is specified by info.getCurrentDN().
	 */
	public RoleEditorDialog(JFrame frame, ConsoleInfo info) {
		super(frame, _resource.getString(_section, "title", info.getCurrentDN() ), true, OK|CANCEL|HELP);
		getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "description"));
						 
		_frame = frame;
		_info = info;
		_managedRoleCache = new RoleCache(true);
		_complexRoleCache = new RoleCache(true);

		_tabPane = new JTabbedPane();
		_managedRoleTab = new RoleEditorTab(true);
		_managedRoleTab.getAddButton().addActionListener(this);
		_managedRoleTab.getAddButton().getAccessibleContext().setAccessibleDescription(_managedRoleTab.getAddButton().getText());
		_managedRoleTab.getRemoveButton().addActionListener(this);
		_managedRoleTab.getRemoveButton().getAccessibleContext().setAccessibleDescription(_managedRoleTab.getRemoveButton().getText());
		_managedRoleTab.getEditButton().addActionListener(this);
		_managedRoleTab.getEditButton().getAccessibleContext().setAccessibleDescription(_managedRoleTab.getEditButton().getText());		
		_managedRoleTab.getTableSelectionModel().addListSelectionListener(this);
		_complexRoleTab = new RoleEditorTab(false);
		_complexRoleTab.getEditButton().addActionListener(this);
		_complexRoleTab.getEditButton().getAccessibleContext().setAccessibleDescription(_complexRoleTab.getEditButton().getText());		
		_complexRoleTab.getTableSelectionModel().addListSelectionListener(this);
		
		loadRoleCaches();
		layoutComponents();
	}


	/**
	 * Assemble and layout the components
	 */
	void layoutComponents() {
		Container contentPane = getContentPane();
		GridBagLayout gbl = new GridBagLayout();
		
		contentPane.setLayout(gbl);

		GridBagConstraints gbc = new GridBagConstraints() ;
		gbc.gridx	  = 0;
		gbc.gridy	  = 0;
		gbc.gridwidth  = 1;
		gbc.gridheight = 1;
		gbc.weightx	= 1;
		gbc.weighty	= 1;
		gbc.anchor	 = gbc.CENTER;
		gbc.fill	   = gbc.BOTH;
		gbc.insets	 = new Insets(0, 0, 0, 0);
		gbc.ipadx	  = 0;
		gbc.ipady	  = 0;
		
		contentPane.add(_tabPane) ;
		gbl.setConstraints(_tabPane, gbc);
		
		_tabPane.addTab(_resource.getString(_section, "tab-managed-roles"), _managedRoleTab);
		_managedRoleTab.getTable().getAccessibleContext().setAccessibleDescription(_resource.getString(_section, 
																									   "tab-managed-roles"));
		_tabPane.addTab(_resource.getString(_section, "tab-complex-roles"), _complexRoleTab);
		_complexRoleTab.getTable().getAccessibleContext().setAccessibleDescription(_resource.getString(_section, 
																									   "tab-complex-roles"));
		_tabPane.setSelectedIndex(0);
		
		pack();
	}



	/**
	 * Implements ActionListener.
	 * Here we process clicks from both _managedRoleTab and _complexRoleTab.
	 */
	public void actionPerformed(ActionEvent e) {
		setBusyCursor(true);
		Object source = e.getSource();
		
		if (source == _managedRoleTab.getEditButton()) {
			int roleIndex = _managedRoleTab.getTableSelectionModel().getMinSelectionIndex();
			actionEdit(_managedRoleCache, roleIndex);
		}
		else if (source == _managedRoleTab.getAddButton()) {
			actionAdd();
		}
		else if (source == _managedRoleTab.getRemoveButton()) {
			actionRemove();
		}
		else if (source == _complexRoleTab.getEditButton()) {
			int roleIndex = _complexRoleTab.getTableSelectionModel().getMinSelectionIndex();
			actionEdit(_complexRoleCache, roleIndex);
		}
		setBusyCursor(false);
	}
	
	
	/**
	 * Process a click on Add.
	 */
	void actionAdd() {
		Vector selectedRoles = null;
		
		//
		// Popup the role picker.
		// The picker is instructed to show only managed roles.
		//
		try {
			String parentDn = RolePickerDialog.makeParentDn(_info.getCurrentDN());
			RolePickerDialog picker;
			picker = new RolePickerDialog(_frame, _info);
			Enumeration exclude = _managedRoleCache.getRoleDNs().elements();
			picker.load(parentDn, "nsmanagedroledefinition", exclude);
			picker.showModal();
			if (!picker.isCancel()) 
				selectedRoles = picker.getSelectedRoles();
		}
		catch(LDAPException e) {
			Debug.println("RoleEditorDialog.actionAdd: reading roles failed with " + e);
			// We popup a dialog to warn the user		   
			DSUtil.showLDAPErrorDialog(_frame, e, "fetching-server-unavailable");
		}
		
		//
		// Adds the selected roles in _managedRoleCache.
		// We must check that a role is not already included.
		//
		if (selectedRoles != null) {
			try {				
				LDAPConnection ldc = _info.getLDAPConnection();
				Enumeration e = selectedRoles.elements();
				while (e.hasMoreElements()) {
					String roleDn = (String)e.nextElement();
					DN roleDN = new DN(roleDn);
					if ( !_managedRoleCache.contains(roleDN) )
						_managedRoleCache.add(roleDn, ldc);
				}				
			}
			catch(LDAPException e) {
				Debug.println("RoleEditorDialog.actionAdd: reading roles failed with " + e);
				// We popup a dialog to warn the user				
				DSUtil.showLDAPErrorDialog(_frame, e,
												 "updating-directory-title");
			}
		}
	}
	
	
	
	/**
	 * Process a click on Remove.
	 */
	void actionRemove() {
		JTable table = _managedRoleTab.getTable();
		int managedRoleCount = table.getModel().getRowCount();
		ListSelectionModel selModel = table.getSelectionModel();
		
		for (int i = 0; i < managedRoleCount; i++) {
			if (selModel.isSelectedIndex(i)) {
				_managedRoleCache.remove(i);
			}
		}
	}
	
	
	
	/**
	 * Process a click on Edit.
	 */
	void actionEdit(RoleCache roleCache, int roleIndex) {
		LDAPConnection ldc = _info.getLDAPConnection();
		String roleDN = roleCache.getRoleDN(roleIndex);
		
		try {
			// Read all the attributes of the roles and the nsroledn attribute
			String[] attrs = {NSROLEDN_ATTR, "*"};
			LDAPEntry entry = DSUtil.readEntry(ldc, roleDN, attrs, null);
		
			// Open the resource editor on the selected entry
			DSResourceEditor ed = new DSResourceEditor(_frame, _info, entry );
			ed.showModal();
			DSUtil.dialogCleanup();
			
			// The user may change the role definition.
			// So we ask the role cache to resync.
			String newDN = ed.getLDAPEntry().getDN();
			if (roleDN.equals(newDN)) { // The role DN didn't change
				roleCache.sync(roleDN, ldc); // Synchronize the description
			}
			else { // The role DN has been changed.
				// We update the cache by removing the old and adding the new.
				roleCache.remove(roleDN);
				roleCache.add(newDN, ldc);
			}
		}
		catch(LDAPException e) {
			// Explain to the user that the role does not exist
			Debug.println("RoleEditorDialog.actionEdit: " + roleDN + " is obsolete");
			JOptionPane.showMessageDialog(this,
					_resource.getString(_section, "dangling-role-message"),
					_resource.getString(_section, "dangling-role-title"), 
					JOptionPane.ERROR_MESSAGE);
		}
			
	}


	/**
	 * Implements ListSelectionListener.
	 * Called when the user clicks in the table.
	 * We update the tooltip text.
	 */
	public void valueChanged(ListSelectionEvent e) {
		ListSelectionModel m = (ListSelectionModel)e.getSource() ;
		
		// Find the table and role cache associated to m
		JTable table ;
		RoleCache roleCache ;
		if (m == _managedRoleTab.getTableSelectionModel()) {
			table = _managedRoleTab.getTable();
			roleCache = _managedRoleCache;
		}
		else {
			table = _complexRoleTab.getTable();
			roleCache = _complexRoleCache;
		}
		
		// Update the tooltip text of the tab
		int min = m.getMinSelectionIndex() ;
		int max = m.getMaxSelectionIndex() ;
		if ((min == -1) || (max - min + 1 > 1) || roleCache.isEmpty()) {
			// The selection is empty or multiple
			table.setToolTipText(null);
		}
		else {
			table.setToolTipText(roleCache.getRoleDN(min));
		}
	}
	
	
	/**
	 * Overrides AbstractDialog.
	 * When OK is clicked, we save the role cache if it is dirty.
	 */
	protected void okInvoked() {
		String entryDn = _info.getCurrentDN();
		if (_managedRoleCache.isRecordingEmpty()) { // Nothing changed
			super.okInvoked();
		}
		else {
			try {				
				_managedRoleCache.saveRecording(entryDn, _info.getLDAPConnection());				
		 		super.okInvoked();
		   }
			catch(LDAPException e) {
				// Explain to the user that something is going wrong.
				Debug.println("RoleEditorDialog.okInvoked: failed to change roles of " 
							   + entryDn);
				// We popup a dialog to warn the user				
				DSUtil.showLDAPErrorDialog(_frame, e,
												 "updating-directory-title");
			}
		}
	}
	
	
	/**
	 * Overrides AbstractDialog.
	 */
	protected void helpInvoked() {
		DSUtil.help("configuration-set-role");
	}
	
	
	/**
	 * Load the role caches.
	 * Here we read the nsrole and nsroledn attributes of the entry and
	 * dispatch the values between _managedRoleCache and _complexRoleCache.
	 */
	void loadRoleCaches() {
		// Debug 
 		String targetDn = _info.getCurrentDN();
		String debugLabel = "RoleEditorDialog.loadRoleCaches(" + targetDn + "): ";
		
		// Load the cache
		LDAPConnection ldc = _info.getLDAPConnection();
		try {
			
			//
			// Read the managed role entries
			//
			LDAPEntry entry = DSUtil.readEntry(ldc, targetDn, ROLE_ATTRS, null);
			LDAPAttribute nsRoleDN = entry.getAttribute(NSROLEDN_ATTR);
			if (nsRoleDN != null) {
				Enumeration e = nsRoleDN.getStringValues();
				if (e != null) {
					int count = 0;
					while (e.hasMoreElements()) {
						String roleDN = (String)e.nextElement();
						_managedRoleCache.add(roleDN, ldc);
						count++;
					}
					if (count == 0) {
						Debug.println(0, debugLabel + "getStringValues(" + NSROLEDN_ATTR + ") returned an empty Enumeration !");
					}
				}
				else {
					Debug.println(0, debugLabel + "getStringValues(" + NSROLEDN_ATTR + ") returned null !");
				}
			}
			else {
				Debug.println(debugLabel + "no " + NSROLEDN_ATTR + " attribute");
			}

			//
			// Read the complex role entries
			//		
			LDAPAttribute nsRole = entry.getAttribute(NSROLE_ATTR);
			if (nsRole != null) {
				Enumeration e = nsRole.getStringValues();
				if (e != null) {
					int count = 0;
					while (e.hasMoreElements()) {
						String roleDN = (String)e.nextElement();
						LDAPEntry roleEntry = DSUtil.readEntry(ldc, roleDN, BASIC_ATTRS, null);
						if (roleEntry == null) {
							Debug.println(0, debugLabel + " readEntry(" + roleDN + ") returned null !");
						}
						// nsrole includes the managed roles
						// We keep only the complex roles.
						else if (isComplexRole(roleEntry)) {
							_complexRoleCache.add(roleEntry);
						}
 						count++;
					}
					if (count == 0) {
						Debug.println(0, debugLabel + "getStringValues(" + NSROLE_ATTR + ") returned an empty Enumeration !");
					}
				}
 				else {
					Debug.println(0, debugLabel + "getStringValues(" + NSROLE_ATTR + ") returned NULL !");
				}
			}
			else {
				Debug.println(debugLabel + "no " + NSROLE_ATTR + " attribute");
			}

		}
		catch(LDAPException e) {
			Debug.println(0, debugLabel + "exception " + e);
			Debug.println(0, debugLabel + "no roles have been loaded");
		}
		
		// Update the table model of the panes
		// If the loading has failed, the table models will be empty.
		_managedRoleTab.getTable().setModel(_managedRoleCache.getTableModel());
		_complexRoleTab.getTable().setModel(_complexRoleCache.getTableModel());

		// Now we record any changes in _managedRoleCache.
		_managedRoleCache.enableRecording(true);		
	}


	/**
	 * Return true if entry is an nscomplexroledefinition instance.
	 */
	static boolean isComplexRole(LDAPEntry entry) {
		boolean yes = false ;
		
		LDAPAttribute objectClass = entry.getAttribute("objectclass");
		if (objectClass == null) {
			Debug.println(0, "RoleEditorDialog.isComplexRole: cannot find objectclass !");
		}
		else {
			Enumeration e = objectClass.getStringValues();
			while (e.hasMoreElements() && !yes) {
				String c = (String)e.nextElement() ;
				yes = c.equalsIgnoreCase("nscomplexroledefinition");
			}
		}
		
		return yes;
	}
	
	
	// Misc
	JFrame _frame;
	ConsoleInfo _info;
	RoleCache _managedRoleCache;
	RoleCache _complexRoleCache;
	
	// Components
	JTabbedPane _tabPane;
	RoleEditorTab _managedRoleTab;
	RoleEditorTab _complexRoleTab;
	
	// I18N
	static private final ResourceSet _resource = DSUtil._resource;
	static private final String _section = "roleEditorDialog";

	// LDAP schema
	private static final String NSROLEDN_ATTR = "nsRoleDN";
	private static final String NSROLE_ATTR = "nsRole";
	// NOTE: I added objectclass in ROLE_ATTRS to workaround the
	// server bug 393670.
	private static final String[] ROLE_ATTRS = { "objectclass", NSROLEDN_ATTR, NSROLE_ATTR };
	private static final String CN_ATTR = "cn";
	private static final String DESCRIPTION_ATTR = "description";
	private static final String OBJECTCLASS_ATTR = "objectclass";
	private static final String[] BASIC_ATTRS = {CN_ATTR, DESCRIPTION_ATTR, OBJECTCLASS_ATTR};
}
