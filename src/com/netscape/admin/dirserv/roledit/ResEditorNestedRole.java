/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.nmclf.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.*;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.UIFactory;

/**
 * ResEditorNestedRole is a plugin for the ResourceEditor.
 * It is used to edit information specific to nsnesteddroledefinition entries.
 */
public class ResEditorNestedRole extends JPanel
                               implements IResourceEditorPage,
                                          Observer,
                                          ActionListener,
                                          ListSelectionListener {
    /**
     * Constructor
     */
    public ResEditorNestedRole() {
        _id = _resource.getString(_section, "id");  
    }


	/**
     * Implements the Observer interface.
     */
	public void update(Observable o, Object arg) {
        if ((o instanceof ResourcePageObservable) == false) {
            return;
        }
        ResourcePageObservable observable = (ResourcePageObservable) o;
        if (arg instanceof String) {
            String argString = (String) arg;
            if (argString.equalsIgnoreCase(NSROLEDN_ATTR)) {
            	loadCache();
            }
        }
    }


    /**
     * Implements the IResourceEditorPage interface.
     * Here we build the UI components, search roles from the directory
     * and initialize the UI components with them.
     */
    public void initialize(ResourcePageObservable observable,
                           ResourceEditor parent) {
        _parent = parent;
        _observable = observable;
        _roleCache = new RoleCache(true);

		// Build and layout the components if not already done
		if (_table == null) {
        	JLabel label = new JLabel(_resource.getString(_section, "label"));
			
        	_table = new SuiTable();
			label.setLabelFor(_table);
        	_table.setModel(_roleCache.getTableModel());
        	_table.setAutoResizeMode(_table.AUTO_RESIZE_ALL_COLUMNS);
        	_table.getSelectionModel().addListSelectionListener(this);
			_table.setPreferredScrollableViewportSize(new Dimension(10, 10));
			JScrollPane scrollPane = SuiTable.createScrollPaneForTable(_table);
        	scrollPane.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));

        	_addButton = UIFactory.makeJButton( this, _section, "addButton", _resource);			
        	_removeButton = UIFactory.makeJButton( this, _section, "removeButton", _resource);

        	JButtonFactory.resizeGroup(_addButton, _removeButton);

        	Box buttonBox = new Box(BoxLayout.X_AXIS);
        	buttonBox.add(Box.createHorizontalGlue());
        	buttonBox.add(_addButton);
        	buttonBox.add(Box.createHorizontalStrut(SuiLookAndFeel.SEPARATED_COMPONENT_SPACE));
        	buttonBox.add(_removeButton);

        	setLayout(new GridBagLayout());
        	GridBagUtil.constrain(this, label,
                            	  0, 0, 1, 1,
                            	  1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
                            	  SuiLookAndFeel.VERT_WINDOW_INSET, SuiLookAndFeel.HORIZ_WINDOW_INSET,
                            	  0, SuiLookAndFeel.HORIZ_WINDOW_INSET);
        	GridBagUtil.constrain(this, scrollPane,
                            	  0, 1, 1, 1,
                            	  1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
                            	  0, SuiLookAndFeel.HORIZ_WINDOW_INSET,
                            	  0, SuiLookAndFeel.HORIZ_WINDOW_INSET);
        	GridBagUtil.constrain(this, buttonBox,
                            	  0, 2, 1, 1,
                            	  0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
                            	  SuiLookAndFeel.SEPARATED_COMPONENT_SPACE, SuiLookAndFeel.COMPONENT_SPACE,
                            	  SuiLookAndFeel.VERT_WINDOW_INSET, SuiLookAndFeel.HORIZ_WINDOW_INSET);
		}
		
		// Load values from the observable
        loadCache();
        valueChanged(null); // To update the state of _removeButton
    }


	/**
     * Load the nested roles in the cache.
     */
    void loadCache() {
    
    	//
        // Empty the cache
        //
        _roleCache.clear();
        _roleCache.enableRecording(false);
        
        //
        // Fill the cache with the roles in the nsroledn attribute of the entry.
        //
    	try {
    	    LDAPConnection ldc = _parent.getConsoleInfo().getLDAPConnection();
            Vector roleDNs = _observable.get(NSROLEDN_ATTR);
            Enumeration e = roleDNs.elements() ;
            while (e.hasMoreElements()) {
        	    String dn = (String)e.nextElement();
                _roleCache.add(dn, ldc);
		    }
        }
        catch(LDAPException e) {
        	Debug.println("ResEditorNestedRole.loadCache: " + e + " while loading the roles");
        }
        _roleCache.enableRecording(true);
    }


    /**
     * Implements the IResourceEditorPage interface.
     */
    public String getID() {
        return _id;
    }


    /**
     * Implements the IResourceEditorPage interface.
     * We have nothing to do here.
     */
    public boolean afterSave(ResourcePageObservable observable) throws Exception {
        return true;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Save the list of nested roles (if modified) in the observable object.
     */
    public boolean save(ResourcePageObservable observable) throws Exception {

        if (!_roleCache.isRecordingEmpty()) {
        	Vector roleDNs = _roleCache.getRoleDNs();
            if (roleDNs == null) {
        		_observable.delete(NSROLEDN_ATTR) ;
            }
            else {
            	_observable.replace(NSROLEDN_ATTR, roleDNs);
            }
            _roleCache.enableRecording(false);
            _roleCache.enableRecording(true); // This clears the change records
        }
        
        return true;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void clear() {
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void reset() {
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void setDefault() {
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Check if some changes have been recorded in the role cache.
     */
    public boolean isModified() {
    	return !_roleCache.isRecordingEmpty();
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Problem here: the 'modified' state is controled only 
     * by the role cache.
     */
    public void setModified(boolean value) {
    	if (value == true) 
        	throw new IllegalArgumentException("Can't set to true");
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public boolean isReadOnly() {
        return _isReadOnly;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void setReadOnly(boolean value) {
        _isReadOnly = value;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void setEnable(boolean value) {
        _isEnabled = value;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * At least one role must be nested.
     */
    public boolean isComplete() {
    	boolean result = true;
        
        if (_roleCache.isEmpty()) {
            JOptionPane.showMessageDialog(null,
                    _resource.getString(_section, "IncompleteText"),
                    _resource.getString(_section, "IncompleteTitle"), 
                    JOptionPane.ERROR_MESSAGE);
            ModalDialogUtil.sleep();
            result = false;
        }

        return result;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public String getDisplayName() {
        return _id;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void help() {
		DSUtil.help("configuration-role-member-nested-help");
    }


    /**
	 * Implements the ActionListener interface.
	 */
    public void actionPerformed(ActionEvent e) 
    {
		_parent.setBusyCursor(true);
    	if (e.getSource() == _addButton) {
        	actionAdd();
        }
        else if (e.getSource() == _removeButton) {
        	actionRemove();
        }
		_parent.setBusyCursor(false);
	}
    
    /**
     * Process a click on Add
     */
    void actionAdd() {		
		ConsoleInfo info = _parent.getConsoleInfo();
    	Vector selectedRoles = null;
    

		// Build the exclude list for the role picker.
		// We exclude :
		//		- the roles which are already nested
		//		- the role being edited (not useful to nest a role inside itself)
		//
		Vector excludeRoles = _roleCache.getRoleDNs();
		String observedRole = _observable.getDN();
		if (observedRole != null) 
			excludeRoles.addElement(observedRole);
			
        //
        // Popup the role picker.
        // The picker is instructed to show all kinds of role.
        //
        try {
        	String baseDn;
        	if (_observable.isNewUser())
            	baseDn = _observable.getCreateBaseDN();
            else
            	baseDn = RolePickerDialog.makeParentDn(_observable.getDN());
    	    RolePickerDialog picker = new RolePickerDialog(_parent.getFrame(), info);
            picker.loadWithNestableRoles(baseDn, excludeRoles.elements());
			picker.allowMultipleSelection(true);
            picker.showModal();
            if (!picker.isCancel())
            	selectedRoles = picker.getSelectedRoles();
        }
        catch(LDAPException e) {
            Debug.println("ResEditorNestedRole.actionAdd: reading roles failed with " + e);
			// We popup a dialog to warn the user
			DSUtil.showLDAPErrorDialog(_parent.getFrame(), e, "fetching-server-unavailable");
        }

        //
        // Adds the roles in _managedRoleCache
        //
        if (selectedRoles != null) {
        	try {
	    	    LDAPConnection ldc = info.getLDAPConnection();
        	    Enumeration e = selectedRoles.elements();
                while (e.hasMoreElements()) {
                	String roleDn = (String)e.nextElement();
					DN roleDN = new DN(roleDn);
                    if ( !_roleCache.contains(roleDN) )
            	    	_roleCache.add(roleDn, ldc);
                }
            }
            catch(LDAPException e) {
                Debug.println("RoleEditorDialog.actionAdd: reading roles failed with " + e);
			    // We popup a dialog to warn the user
				DSUtil.showLDAPErrorDialog(_parent.getFrame(), e,
												 "updating-directory-title");
            }
        }		

    }
    
    /**
     * Process a click on Remove
     */
    void actionRemove() {
        int roleCount = _table.getModel().getRowCount();
        ListSelectionModel selModel = _table.getSelectionModel();
        
        // 600538 Multiple elements from a vector must be deleted starting
        // from the highest index as vector indexes change after each delete
    	for (int i = (roleCount - 1); i >= 0; i--) {
        	if (selModel.isSelectedIndex(i)) {
            	_roleCache.remove(i);
            }
        }
    }
    

    /**
     * Implements ListSelectionListener.
     * Called when the user clicks in the table.
     * We update the tooltip text and the state of _removeButton
     */
    public void valueChanged(ListSelectionEvent e) {
    	// Note: initialize() invokes this method with e == null
            	
    	ListSelectionModel m = _table.getSelectionModel() ;
        int min = m.getMinSelectionIndex() ;
        int max = m.getMaxSelectionIndex() ;
        
        // Update the tooltip text of the tab
        if ((min == -1) || (max - min + 1 > 1) || (_table.getRowCount() <= 0)) {
        	// The selection is empty or multiple or the table is empty
        	_table.setToolTipText(null);
        }
        else {
            _table.setToolTipText(_roleCache.getRoleDN(min));
        }
        
        // _removeButton is disabled if the selection is empty: if we use
		// the tab and the table gets the focus JTable.getSelectedRowCount() is 1
		// (even if the row count is 0 !!)
        _removeButton.setEnabled((_table.getSelectedRowCount() > 0) &&
								 (_table.getRowCount() > 0));
    }
    
    
	// State variables
    ResourceEditor _parent;
    ResourcePageObservable _observable;
    String _id;
	RoleCache _roleCache;
    boolean _isReadOnly = false;
    boolean _isEnabled = true;

	// Components
	SuiTable _table;
    JButton _addButton;
    JButton _removeButton;

	// I18N
	static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "nestedRolePage";

	// LDAP schema
    private static final String NSROLEDN_ATTR = "nsRoleDN";
    private static final String NSROLE_ATTR = "nsRole";
    private static final String[] ROLE_ATTRS = { NSROLEDN_ATTR };
}
