/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.util.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;

import com.netscape.management.nmclf.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.*;

import netscape.ldap.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.UIFactory;


/**
 * ResEditorNestedRole is a plugin for the ResourceEditor.
 * It is used to edit information specific to nsfilteredroledefinition entries.
 */
public class ResEditorFilteredRole extends JPanel
                               implements IResourceEditorPage,
                                          Observer,
                                          ActionListener,
                                          SuiConstants {
    /**
     * Constructor
     */
    public ResEditorFilteredRole() {
        _id = _resource.getString(_section, "id");  
    }


	/**
     * Implements the Observer interface.
     */
	public void update(Observable o,
                       Object arg) {
        if ((o instanceof ResourcePageObservable) == false) {
            return;
        }
        ResourcePageObservable observable = (ResourcePageObservable) o;
        if (arg instanceof String) {
            String argString = (String) arg;
            if (argString.equalsIgnoreCase(NSROLEFILTER_ATTR)) {
                _filterText.setText(observable.get(NSROLEFILTER_ATTR, 0));
            }
        }
    }


    /**
     * Implements the IResourceEditorPage interface.
     * Here we build the UI components and initialize them.
     */
    public void initialize(ResourcePageObservable observable,
                           ResourceEditor parent) {
        _parent = parent;
        _observable = observable;
		
		// Build and layout the components if not already done
		if (_filterText == null) {
			_filterText = new JTextField();
        	_constructButton = UIFactory.makeJButton(this, _section, "constructButton", _resource);			
        	_testButton = UIFactory.makeJButton(this, _section, "testButton", _resource);

			layoutComponents();
		}
		
		// Load values from the observable
        _oldFilterText = _observable.get(NSROLEFILTER_ATTR, 0);
        if(_observable.isNewUser())
            _filterText.setText("(objectclass=*)");
        else
        	_filterText.setText(_oldFilterText);
		
	}

	/**
     * Layout the UI components.
     */
    void layoutComponents() {
    
    	GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        
        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.BOTH;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(VERT_WINDOW_INSET, HORIZ_WINDOW_INSET, 0, HORIZ_WINDOW_INSET);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
        
        JLabel label = new JLabel(_resource.getString(_section, "label"));
		label.setLabelFor(_filterText);
        add(label);
        gbl.setConstraints(label, gbc);

		add(_filterText);
        gbc.gridy++;
        gbc.insets = new Insets(0, HORIZ_WINDOW_INSET, 0, HORIZ_WINDOW_INSET);
        gbl.setConstraints(_filterText, gbc);

        Box box = new Box(BoxLayout.X_AXIS);
        box.add(_testButton);
        box.add(Box.createHorizontalStrut(SEPARATED_COMPONENT_SPACE));
        box.add(_constructButton);
        JButtonFactory.resizeGroup(_testButton, _constructButton);
       
		add(box);
        gbc.gridy++;
        gbc.weightx = 0;
        gbc.weighty = 1;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(VERT_WINDOW_INSET, HORIZ_WINDOW_INSET, VERT_WINDOW_INSET, HORIZ_WINDOW_INSET);
        gbl.setConstraints(box, gbc);
 /*      
		add(_resultPanel);
        gbc.gridy++;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = gbc.BOTH;
        gbc.insets = new Insets(VERT_WINDOW_INSET, HORIZ_WINDOW_INSET, VERT_WINDOW_INSET, HORIZ_WINDOW_INSET);
        gbl.setConstraints(_resultPanel, gbc);
*/    }


    /**
     * Implements the IResourceEditorPage interface.
     */
    public String getID() {
        return _id;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * We have nothing to do here.
     */
    public boolean afterSave(ResourcePageObservable observable) throws Exception {
        return true;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Save the filter (if modified) in the observable object.
     */
    public boolean save(ResourcePageObservable observable) throws Exception {
        // Compare to old value, and if different, save or delete.
        // Trim leading and trailing white space characters.
        if (_filterText.getText().equals(_oldFilterText) == false) {
            if (_filterText.getText().trim().length() == 0) {
                observable.delete(NSROLEFILTER_ATTR, _oldFilterText);
                _oldFilterText = "";
            }
            else {
                String newFilterText = _filterText.getText().trim();
                observable.replace(NSROLEFILTER_ATTR, newFilterText);
                _oldFilterText = newFilterText;
            }
        }

        return true;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void clear() {
    	_filterText.setText(null);
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void reset() {
    	_filterText.setText(null);
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void setDefault() {
    	_filterText.setText("(objectclass=*)");
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public boolean isModified() {
        return _isModified;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void setModified(boolean value) {
        _isModified = value;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public boolean isReadOnly() {
        return _isReadOnly;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void setReadOnly(boolean value) {
        _isReadOnly = value;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void setEnable(boolean value) {
        _isEnabled = value;
    }


    /**
     * Implements the IResourceEditorPage interface.
     * Here we check that _filterText is not empty.
     */
    public boolean isComplete() {
    	boolean result = ( _filterText.getText().trim().length() >= 1 );
        if (!result) {
            JOptionPane.showMessageDialog(null,
                    _resource.getString(_section, "IncompleteText"),
                    _resource.getString(_section, "IncompleteTitle"), 
                    JOptionPane.ERROR_MESSAGE);
            ModalDialogUtil.sleep();
            result = false;
        }

        return result;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public String getDisplayName() {
        return _id;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void help() {
		DSUtil.help("configuration-role-member-filtered-help");
    }


    /**
	 * Implements the ActionListener interface.
	 */
    public void actionPerformed(ActionEvent e) 
    {
		_parent.setBusyCursor(true);
        if (e.getSource() == _testButton) {
        	actionTest();
        }
        else if (e.getSource() == _constructButton) {
        	actionConstruct();
        }
		_parent.setBusyCursor(false);
	}
    
    
    /**
     * Click on the Test button
     */
    void actionTest() {
    	ConsoleInfo info = _parent.getConsoleInfo();
        
        String baseDn;
        if (_observable.isNewUser())
            baseDn = _observable.getCreateBaseDN();
        else
            baseDn = RolePickerDialog.makeParentDn(_observable.getDN());
        String URLstring = "ldap:///" + baseDn + "??sub?" + _filterText.getText().trim();

        try {
            LDAPUrl ldapURL = new LDAPUrl(URLstring);
            LDAPConnection ldc = info.getUserLDAPConnection();
            Debug.println("DynamicQueryDlg:"+
                    ldapURL.getHost() + " "+
                    ldapURL.getPort());
			if (_resultDialog == null) {
				_resultDialog = new SearchResultDialog(_parent.getFrame(), info);
			}
			_resultDialog.invokeDoSearchLater(ldc, ldapURL);
			_resultDialog.show();
        } catch (MalformedURLException malformedError) {
            JFrame frame = UtilConsoleGlobals.getActivatedFrame();
            JOptionPane.showMessageDialog(
                    frame,
                    _resource.getString(_section,
                    "errortext"),
                    _resource.getString(_section,
                    "errortitle"),
                    JOptionPane.ERROR_MESSAGE);
            ModalDialogUtil.sleep();
        }
    }
    

    /**
     * Click on the Construct button
     */
    void actionConstruct() {

		//
        // Clone the console info and change the base dn to
        // the parent of the current entry.
        //
        String baseDn;
        if (_observable.isNewUser())
            baseDn = _observable.getCreateBaseDN();
        else
            baseDn = RolePickerDialog.makeParentDn(_observable.getDN());

    	ConsoleInfo info = (ConsoleInfo)_parent.getConsoleInfo().clone();
        info.setBaseDN(baseDn);		
        info.setUserBaseDN(baseDn);
        //
        // Popup the query builder dialog
        //
        LdapQueryBuilderDialog lqbd = new LdapQueryBuilderDialog(info);
		lqbd.setTitle(_resource.getString(_section, "BuilderDialogTitle"));
		lqbd.getAccessibleContext().setAccessibleDescription(_resource.getString(_section, 
																				 "BuilderDialogTitle-description"));
        lqbd.show();
        
        //
        // Update the filter
        //
        if (lqbd.isCancel() == false) {
            try {
                LDAPUrl ldapURL = new LDAPUrl(lqbd.getQueryString());
                _filterText.setText(ldapURL.getFilter());
            } catch (MalformedURLException malformedError) {
            	Debug.println("ResEditorFilteredRole.actionConstruct: invalid URL !");
            }
        }
    }
	
	
	/**
	 * The dialog to display the test result.
	 * This is an AbstractDialog which embeds a SearchResultPanel
	 * with a Close button.
	 */
	
	class SearchResultDialog extends AbstractModalDialog implements ActionListener {
	
		public SearchResultDialog(Frame f, ConsoleInfo info) {
			super(f, _resource.getString(_section, "SearchResultTitle"), CLOSE);
			_searchResultPanel = new SearchResultPanel(info, this);
			getContentPane().add(_searchResultPanel);
			pack();
		}
		
		/**
		 * Invokes SearchResultPanel.doSearch() through SwingUtilities.invokeLater.
		 * This is a way to delay the execution of the search. If this method
		 * is invoked just before show(), the search will be executed while
		 * the dialog is opened (ie while the show is running).
		 */
		public void invokeDoSearchLater(final LDAPConnection ldc, final LDAPUrl url) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
        			_searchResultPanel.removeAllElements();
					_searchResultPanel.doSearch(ldc, url);
				}
			});
		}
		
		/**
		 * Strangely SearchResultPanel requires an ActionListener.
		 * However it never calls actionPerformed().
		 */
		public void actionPerformed(ActionEvent event) {
			Debug.println("SearchResultDialog.actionPerformed: action event" + event);
		}
		
		protected SearchResultPanel _searchResultPanel;
	}
    

	// State variables
    ResourceEditor _parent;
    ResourcePageObservable _observable;
    String _id;
    boolean _isModified = false;
    boolean _isReadOnly = false;
    boolean _isEnabled = true;
    String _oldFilterText;

	// Components
    JButton _testButton;
    JButton _constructButton;
    JTextField _filterText;
    SearchResultDialog _resultDialog;

	// I18N
	static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "filteredRolePage";

	// LDAP schema
    private static final String NSROLEFILTER_ATTR = "nsRoleFilter";
}
