/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.util.*;
import netscape.ldap.*;
import javax.swing.JLabel;
import javax.swing.table.TableModel;
import javax.swing.table.AbstractTableModel;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.DSUtil;
import netscape.ldap.util.DN;

/**
 * RoleCache is data structure for holding a list of roles and acts:
 *		- a TableModel for a JTable,
 *		- a cache which logs add and remove operations.
 *
 * A RoleCache is filled with roles using add(). Roles can be removed
 * using remove(). These roles can be displayed in a JTable using the 
 * data model returned by getTableModel().
 *
 * A RoleCache can be put in recording mode (using enableRecording):
 * all subsequent adds or removals of roles are logged.
 * save() allows to save into the directory all the logged adds and removals.
 * 
 **/

public class RoleCache {

	/**
     * Construct a cache.
     */
	public RoleCache() {
    	this(false);
	}


	/**
     * Construct a cache.
     */
	public RoleCache(boolean keepDanglingRole) {
    	_keepDanglingRole = true;
	}


	/**
     * Add a role in the cache.
     * The role is identified by its dn.
     * This method uses the LDAPConnection to construct an LDAPEntry
     * and invoke add(LDAPEntry).
     * If the entry does not exist and _keepDanglingRole is false,
     * it is skipped silently. If _keepDanglingRole is true, the role
     * is added to the cache and tagged as a 'dangling role'.
     */
	public void add(String roleDn, LDAPConnection ldc) throws LDAPException {
        try {
			LDAPEntry roleEntry = DSUtil.readEntry(ldc, roleDn, BASIC_ATTRS, null);
            add(roleEntry);
        }
        catch(LDAPException e) {
            if (e.getLDAPResultCode() != e.NO_SUCH_OBJECT) {
                throw e;
            }
            else if (_keepDanglingRole) {
                addDangling(roleDn);
            }
            else {
            	Debug.println("RoleCache.add: skipping " + roleDn +
               	              " because it does not exist");
            }
        }
    }


	/**
     * Add a role in the cache.
     * The role is identified by an LDAPEntry.
     * The LDAPEntry should contain (at least) the following attributes:
     *		- cn
     *		- description
     *		- objectclass
     */
	public void add(LDAPEntry roleEntry) {
    	if (isRoleDefinition(roleEntry)) {
    		String roleDn = roleEntry.getDN();
            LDAPAttribute cnAttr = roleEntry.getAttribute(CN_ATTR);
            LDAPAttribute descAttr = roleEntry.getAttribute(DESCRIPTION_ATTR);
            LDAPAttribute classAttr = roleEntry.getAttribute(OBJECTCLASS_ATTR);

            JLabel cn = new JLabel(makeValueString(cnAttr));
            cn.setIcon(makeIcon(classAttr));
            String desc = makeValueString(descAttr);
            
            _roleDNs.addElement(roleDn);
            _roleNames.addElement(cn);
            _roleDescriptions.addElement(desc);
            sortRoles();
            
            _tableModel.fireTableDataChanged();
            updateRecording(roleDn, true);
       	}
        else {
        	Debug.println("RoleCache.add: skipping " + roleEntry.getDN() + 
                          " because it's not a role");
        }
    }


	/**
     * Sync a role which is already in the cache.
     * The method uses the LDAPConnection to update the description 
     * of the role.
     */
	public void sync(String roleDn, LDAPConnection ldc) throws LDAPException {
    	int i = _roleDNs.indexOf(roleDn);
        if (i == -1) {
        	Debug.println("RoleCache.sync: skipping " + roleDn + 
                          " because it is not in the cache");
        }
        else {
            try {
				LDAPEntry roleEntry = DSUtil.readEntry(ldc, roleDn, BASIC_ATTRS, null);
            	LDAPAttribute descAttr = roleEntry.getAttribute(DESCRIPTION_ATTR);
            	_roleDescriptions.setElementAt(makeValueString(descAttr), i);
                _tableModel.fireTableDataChanged();
            }
            catch(LDAPException e) {
            	Debug.println("RoleCache.add of " + roleDn + ": " + e);
            }
        }
    }


	/**
     * Remove a role from the cache.
     * The role is identified by its dn.
     * Switch the dirty flag on.
     */
    public void remove(String roleDn) {
    	int i = _roleDNs.indexOf(roleDn);
        if (i != -1) {
        	remove(i);
        }
    }
    

	/**
     * Remove a role from the cache.
     * The role is identified by its index in the table model.
     * Switch the dirty flag on.
     */
    public void remove(int roleIndex) {
        updateRecording((String)_roleDNs.elementAt(roleIndex), false);
        
        _roleDNs.removeElementAt(roleIndex);
        _roleNames.removeElementAt(roleIndex);
        _roleDescriptions.removeElementAt(roleIndex);

        _tableModel.fireTableDataChanged();
    }
    


	/**
     * Clear the cache.
     * All the roles are removed.
     * All recording data are cleared.
     * Recording state is unchanged.
     */
    public void clear() {
        clearRecording();
        
        _roleDNs.removeAllElements();
        _roleNames.removeAllElements();
        _roleDescriptions.removeAllElements();

        _tableModel.fireTableDataChanged();
    }
    


	/**
     * Return true if the specified role is in the cache.
     */
    public boolean contains(String roleDn) {
    	return ( _roleDNs.indexOf(roleDn) != -1 );
    }
    
	/**
     * Return true if the specified role DN is in the cache.
     */
    public boolean contains(DN roleDN) {
		boolean contains = false;
		for (Enumeration e = _roleDNs.elements() ; e.hasMoreElements() && !contains;) {
			if (roleDN.equals(new DN(
									 (String)e.nextElement()))) {
				contains = true;
			}
		}
    	return contains;
    }


	/**
     * Switch the recording on or off.
     * When recording is on, the cache log each add or removal.
     * This method clears the log.
     */
    public void enableRecording(boolean on) {
    	_recordingOn = on;
        _addRecords.removeAllElements();
        _removeRecords.removeAllElements();
    }


	/**
     * Save the recorded changes into the directory.
     * The method uses the LDAPConnection to apply the recorded
     * changes to the nsRoleDN attribute of the specified entry.
     */
	public void saveRecording(String entryDn, LDAPConnection ldc) throws LDAPException {

        // Make an LDAPModification for add records
        LDAPModification addMod = null;
        if (_addRecords.size() >= 1) {
            LDAPAttribute addAttr = new LDAPAttribute(NSROLEDN_ATTR);
            Enumeration e = _addRecords.elements();
            while (e.hasMoreElements()) {
        	    addAttr.addValue((String)e.nextElement());
            }
            addMod = new LDAPModification(LDAPModification.ADD, addAttr);
        }
        
        // Make an LDAPModification for remove records
        LDAPModification removeMod = null;
        if (_removeRecords.size() >= 1) {
        	LDAPAttribute removeAttr = new LDAPAttribute(NSROLEDN_ATTR);
            Enumeration e = _removeRecords.elements();
            while (e.hasMoreElements()) {
        	    removeAttr.addValue((String)e.nextElement());
            }
            removeMod = new LDAPModification(LDAPModification.DELETE, removeAttr);
        }

		//
        // Apply the LDAPModifications
        //
        if ((addMod != null) && (removeMod != null)) {
        	LDAPModification[] mods = { addMod, removeMod };
            ldc.modify(entryDn, mods);
        }
        else if (addMod != null) {
        	ldc.modify(entryDn, addMod);
        }
        else if (removeMod != null) {
        	ldc.modify(entryDn, removeMod);
        }
    }
    
    
	/**
     * Return true if no changes are recorded.
     */
    public boolean isRecordingEmpty() {
    	return (_addRecords.size() + _removeRecords.size() == 0);
    }


	/**
     * Get the table model for displaying the roles.
     */
    public TableModel getTableModel() {
        return _tableModel;
    }


	/**
     * Translate a table index into a role DN.
     */
    public String getRoleDN(int roleIndex) {
    	return (String)_roleDNs.elementAt(roleIndex) ;
    }

    
	/**
     * Return the list of roles which are in the cache.
     */
    public Vector getRoleDNs() {
    	return (Vector)_roleDNs.clone();
    }
    

	/**
     * Return true if there is no role in the cache.
     */
    public boolean isEmpty() {
    	return _roleDNs.isEmpty();
    }
    


    /**
     * Check if this entry is a nsroledefinition instance.
     * The entry must contain the objectclass attribute.
     */
    private boolean isRoleDefinition(LDAPEntry entry) {
    	boolean yes = false ;
        
    	LDAPAttribute objectClass = entry.getAttribute("objectclass");
        if (objectClass == null) {
        	Debug.println("RoleCache.isRoleDefinition: cannot find objectclass !");
        }
        else {
        	Enumeration e = objectClass.getStringValues();
            while (e.hasMoreElements() && !yes) {
            	String c = (String)e.nextElement() ;
                yes = c.equalsIgnoreCase("nsroledefinition");
            }
        }
        
        return yes;
    }
    
    
    /**
     * Add a dangling role in the cache.
     * A dangling is assigned a special icon and has no description.
     */
    private void addDangling(String roleDn) {
    
    	String rdn[] = LDAPDN.explodeDN(roleDn, true);
        
        JLabel cn = new JLabel(rdn[0]);
	    String iconName = _resource.getString(_section, "dangling-role-icon");
        cn.setIcon(DSUtil.getPackageImage(iconName));

        _roleDNs.addElement(roleDn);
        _roleNames.addElement(cn);
        _roleDescriptions.addElement("");
        sortRoles();

        _tableModel.fireTableDataChanged();
        updateRecording(roleDn, true);
    }
    
    
    /**
     * Make a string from the first value of an LDAPAttribute
     */
    private String makeValueString(LDAPAttribute attr) {
    	String result = null;
        if (attr == null) {
        	result = "";
        }
        else {
    	    Enumeration e = attr.getStringValues() ;
            if (e == null) {
        	    Debug.println("RoleTableModel.makeStringValue: cannot get values from " + attr);
                result = "";
            }
            else {
        	    result = (String)e.nextElement();
            }
        }
        
        return result;
    }
    
    
    /**
     * Record a change (if recording is on).
     * Here we take care of duplicates.
     */
    private void updateRecording(String dn, boolean add) {
    	if (_recordingOn) {
    	    if (add) { // dn is being added to the cache
        	    int i = _removeRecords.indexOf(dn);
                if (i == -1)
            	    _addRecords.addElement(dn);
                else
            	    _removeRecords.removeElementAt(i);
            }
            else { // dn is being removed from the cache
        	    int i = _addRecords.indexOf(dn);
                if (i == -1)
            	    _removeRecords.addElement(dn);
                else
            	    _addRecords.removeElementAt(i);
            }
        }
    }
    
    
    /**
     * Clear the recording.
     * All the change records are deleted.
     */
    private void clearRecording() {
        _removeRecords.removeAllElements();
        _addRecords.removeAllElements();
    }
    
    
    /**
     * Sort the cache on the name field.
     * Simple bubble sort.
     */
    private void sortRoles() {
    	int roleCount = _roleDNs.size();
    	boolean sorted;
    	do {
        	sorted = true;
            for (int i = 0; i < roleCount-1; i++) {
            	JLabel name1 = (JLabel)_roleNames.elementAt(i);
            	JLabel name2 = (JLabel)_roleNames.elementAt(i+1);
                if (name1.getText().compareTo(name2.getText()) > 0) {
                	sorted = false;
                    swapElements(_roleDNs, i, i+1);
                    swapElements(_roleNames, i, i+1);
                    swapElements(_roleDescriptions, i, i+1);
                }
            }
        }
        while (!sorted);
    }
    
    
	/**
     * Retreive the icon associated to the specified objectclass value.
     */
	RemoteImage makeIcon(LDAPAttribute classAttr) {
		String iconName = null;
    	if (classAttr != null) {
        	Enumeration e = classAttr.getStringValues();
            while (e.hasMoreElements() && (iconName == null)) {
            	String n = ((String)e.nextElement()).toLowerCase();
                iconName = _resource.getString("EntryObject", n+"-icon");
            }
        }
        if (iconName == null) {
        	iconName = _resource.getString("EntryObject","default-icon");
        }
        return DSUtil.getPackageImage(iconName);
    }


    /**
     * Swap two items in a vector
     */
    private static void swapElements(Vector v, int i1, int i2) {
    	Object o1 = v.elementAt(i1);
    	Object o2 = v.elementAt(i2);
        v.setElementAt(o2, i1);
        v.setElementAt(o1, i2);
    }



    // Cache
    Vector _roleDNs = new Vector();
    Vector _roleNames = new Vector();
    Vector _roleDescriptions = new Vector();
    
    // Record vectors
    Vector _addRecords = new Vector();
    Vector _removeRecords = new Vector();
    boolean _recordingOn ;
    
    // Misc
    boolean _keepDanglingRole;
    RoleTableModel _tableModel = new RoleTableModel();
    
    // I18N
	private static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "roleCache";

	// LDAP schema
    private static final String NSROLEDN_ATTR = "nsRoleDN";
    private static final String NSROLE_ATTR = "nsRole";
    private static final String[] ROLE_ATTRS = { NSROLEDN_ATTR, NSROLE_ATTR };
    private static final String CN_ATTR = "cn";
    private static final String DESCRIPTION_ATTR = "description";
    private static final String OBJECTCLASS_ATTR = "objectclass";
    public static final String[] BASIC_ATTRS = {CN_ATTR, DESCRIPTION_ATTR, OBJECTCLASS_ATTR};
    
    

    /**
     * A table model for roles has two columns: the role name and its description.
     */
    class RoleTableModel extends AbstractTableModel {
    
        public String getColumnName(int col) {
        	if (col == 0)
            	return _resource.getString(_section, "header-name");
            else // col == 1
            	return _resource.getString(_section, "header-description");
        }
        
        public int getColumnCount() {
    	    return 2;
        }
        
	    public int getRowCount() {
    	    return _roleDNs.size();
        }

        public Object getValueAt(int row, int col) {
        	if (col == 0)
            	return _roleNames.elementAt(row);
            else // col == 1
            	return _roleDescriptions.elementAt(row);
        }

        // makes the three following methods public
        public void fireTableDataChanged() {
        	super.fireTableDataChanged();
        }
        
        public void fireTableRowsDeleted(int start, int stop) {
        	super.fireTableRowsDeleted(start, stop);
        }
        
        public void fireTableRowsInserted(int start, int stop) {
        	super.fireTableRowsInserted(start, stop);
        }
    }

}
