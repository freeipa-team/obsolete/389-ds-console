/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;


import java.awt.*;
import javax.swing.*;
import com.netscape.management.client.ug.*;
import com.netscape.management.client.util.Help;
import com.netscape.management.nmclf.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.account.*;

/**
 * Plugin page for the ResourceEditor.
 */

public class ResEditorRoleAccountPage extends DefaultResEditorPage 
                                      implements SuiConstants {


	/**
	 * The class loader of the resource editor needs a public constructor.
	 */
	public ResEditorRoleAccountPage() {
	}
	
	/**
     * Implements the IResourceEditorPage interface.
	 */
	public void initialize(ResourcePageObservable o, ResourceEditor parent) {

		// One-time initialization
		if (_activationSubPage == null) {
		
			// Create the sub page
			_activationSubPage = new ResEditorActivationSubPage(_section);

			// Layout everything
 			JPanel p = new JPanel(new GridBagLayout());
			
			GridBagConstraints gbc = new GridBagConstraints();
        	gbc.gridx      = 0;
        	gbc.gridy      = 0;
        	gbc.gridwidth  = 1;
        	gbc.gridheight = 1;
        	gbc.weightx    = 1;
        	gbc.weighty    = 0;
        	gbc.fill       = gbc.BOTH;
        	gbc.anchor     = gbc.NORTHWEST;
        	gbc.insets     = new Insets(COMPONENT_SPACE, COMPONENT_SPACE, 0, COMPONENT_SPACE);
        	gbc.ipadx      = 0;
        	gbc.ipady      = 0;
			
			p.add(_activationSubPage, gbc);
			
			gbc.gridy++;
        	gbc.weighty = 1;
			p.add(Box.createVerticalGlue(), gbc);
  			
 			JScrollPane sp = new JScrollPane(p);
         	sp.setBorder(null);
 
 			setLayout(new BorderLayout());
         	add("Center", sp);
		}
		
		// Every time initialization
		_activationSubPage.initialize(o, parent);
	}
	
	/**
	 * Looks like IResourceEditorPage.initialize() except that
	 * our observable is the activation model.
	 */
	public void initialize(ActivationModel m, ResourceEditor parent) {
		_activationSubPage.initialize(m, parent);
	}
	
	/**
     * Implements the IResourceEditorPage interface.
	 */
	public String getID() {
		return _section;
	}


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public String getDisplayName() {
		return DSUtil._resource.getString(getID(), "displayName");
    }


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public boolean save(ResourcePageObservable o) throws Exception {
		return _activationSubPage.save(o);
	}


    /**
	 * Implements the IResourceEditorPage interface.
	 */
	public boolean afterSave(ResourcePageObservable o) 
	throws Exception {
		return _activationSubPage.afterSave(o);
	}


    /**
	 * Implements the IResourceEditorPage interface. 
	 */
	public void help()
	{
		DSUtil.help("configuration-role-account-help");
	}
	
	protected ResEditorActivationSubPage _activationSubPage;
	private static String _section = "roleAccountPage";
}
