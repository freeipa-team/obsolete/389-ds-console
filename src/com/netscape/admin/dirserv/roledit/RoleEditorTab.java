/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.util.*;
import java.awt.*;
import java.awt.event.*; 

import javax.swing.*;
import javax.swing.event.*;

import netscape.ldap.*;

import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.*;
import com.netscape.management.nmclf.*;

import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.UIFactory;


/**
 * RoleEditorTab is a specialized JPanel for RoleEditorDialog private use.
 *
 * This JPanel contains a JTable and three buttons: Add, Remove and Edit. 
 * Add and Remove are optional.
 * It has no intelligence except tracking the selection in the JTable and
 * enabling Edit and Remove states accordingly.
 */
public class RoleEditorTab extends JPanel 
                           implements SuiConstants, ListSelectionListener
{
    /**
     * Construct a RoleEditorTab with the specified id.
     * The Add/Remove buttons are optional.
	 */
    public RoleEditorTab(boolean withAddRemove) 
    {
        super(true);

		_table = new SuiTable();
		_table.setAutoResizeMode(_table.AUTO_RESIZE_ALL_COLUMNS);
		_table.getSelectionModel().addListSelectionListener(this);
		_table.setPreferredScrollableViewportSize(new Dimension(350, 200));
		if (withAddRemove) {
			_addButton = UIFactory.makeJButton(null, _section, "addButton", _resource);
			_removeButton = UIFactory.makeJButton(null, _section, "removeButton", _resource);
		}
		_editButton = UIFactory.makeJButton(null, _section, "editButton", _resource);

		layoutComponents();
		updateButtonState();
    }
    
    /**
     * Return the Add button or null.
     */
    public JButton getAddButton() {
    	return _addButton;
    }
    
    /**
     * Return the Remove button or null.
     */
    public JButton getRemoveButton() {
    	return _removeButton;
    }
    
    
    /**
     * Return the Edit button (never null)
     */
    public JButton getEditButton() {
    	return _editButton;
    }
    
    /**
     * Return the table
     */
    public JTable getTable() {
    	return _table;
    }
    
    
    /**
     * Return the table selection model
     */
    public ListSelectionModel getTableSelectionModel() {
    	return _table.getSelectionModel();
    }
    
    
    
    /**
     * Layout the components
     */
    void layoutComponents() {

		GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        
        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 0;
        gbc.weighty    = 0;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.fill       = gbc.BOTH;
        gbc.insets     = new Insets(SEPARATED_COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
        
		JScrollPane scrollPane = SuiTable.createScrollPaneForTable(_table);
        scrollPane.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));
		add(scrollPane);
        gbc.gridy++;
        gbc.gridheight = gbc.REMAINDER;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbl.setConstraints(scrollPane, gbc);
        
        if (_addButton != null) { // -> _removeButton != null
            JButtonFactory.resizeGroup(_addButton, _removeButton, _editButton);
            add(_addButton);
            gbc.gridx++;
            gbc.gridheight = 1;
            gbc.weightx = 0;
            gbc.weighty = 0;
            gbc.fill = gbc.NONE;
            gbc.insets = new Insets(SEPARATED_COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE, COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE);
            gbl.setConstraints(_addButton, gbc);

            add(_removeButton);
            gbc.gridy++;
            gbc.weightx = 0;
            gbc.weighty = 0;
            gbc.fill = gbc.NONE;
            gbc.insets = new Insets(0, DIFFERENT_COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE);
            gbl.setConstraints(_removeButton, gbc);

            add(_editButton);
            gbc.gridy++;
            gbc.weightx = 0;
            gbc.weighty = 0;
            gbc.fill = gbc.NONE;
            gbc.insets = new Insets(0, DIFFERENT_COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE);
            gbl.setConstraints(_editButton, gbc);
        }
        else {
            JButtonFactory.resize(_editButton);
            add(_editButton);
            gbc.gridx++;
            gbc.gridheight = 1;
            gbc.weightx = 0;
            gbc.weighty = 0;
            gbc.fill = gbc.NONE;
            gbc.insets = new Insets(SEPARATED_COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE, COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE);
            gbl.setConstraints(_editButton, gbc);
        }
    }
    
    /**
     * Implements ListSelectionListener.
     * Called when the user clicks in the table.
     */
    public void valueChanged(ListSelectionEvent e) {
    	updateButtonState();
    }
    
    /**
     * Set the button state according the selection size.
     * Remove is enabled when a least one item is selected.
     * Edit is enabled when one and only one item is selected.
     */
    public void updateButtonState() {
    	ListSelectionModel m = _table.getSelectionModel() ;
        boolean emptySelection = m.isSelectionEmpty() || (_table.getRowCount() <= 0);
        int selectionSize = m.getMaxSelectionIndex() - m.getMinSelectionIndex() +1;
        
        if (_removeButton != null) _removeButton.setEnabled(!emptySelection);
        _editButton.setEnabled(!emptySelection && ( selectionSize == 1 ) );
    }
    
    
	// Components
    SuiTable _table;
    JButton  _editButton;
    JButton  _addButton;
    JButton  _removeButton;

	// I18N
	static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "roleEditorTab";

}
