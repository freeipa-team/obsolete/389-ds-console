/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.util.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.border.*;

import com.netscape.management.nmclf.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.*;

import netscape.ldap.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSResourceEditorPage;


/**
 * ResEditorRadioPage is a plugin for the ResourceEditor.
 * It is used to switch between a list of IResourceEditorPage.
 */
public class ResEditorRadioPage extends JPanel
                               implements IDSResourceEditorPage,
                                          Observer,
                                          ActionListener,
                                          SuiConstants {
    /**
     * Constructor
     */
    public ResEditorRadioPage() {
    }


    /**
     * Implements the Observer interface.
     * We propage the call to all the subpages which
     * implements Observer.
     */
    public void update(Observable o, Object arg) {
        if (_observable.isNewUser()) { // All the subpages must be notified
    		Enumeration e = _pageInfoVector.elements();
    		while (e.hasMoreElements()) {
        		SubPageInfo info = (SubPageInfo)e.nextElement();
        		if (info.page instanceof Observer) {
            		((Observer)info.page).update(o, arg);
        		}
    		}
		}
		else { // Only the current subpage must be notified
			if (_currentPanel instanceof Observer) {
            	((Observer)_currentPanel).update(o, arg);
			}
		}
    }


    /**
     * Implements the IResourceEditorPage interface.
     * Here we build the UI components and initialize them.
     */
    public void initialize(ResourcePageObservable observable,
                           ResourceEditor parent) {
        _parent = parent;
        _observable = observable;
        _id = "untitled";  // Should be overriden by the subclass
  try {      
        // HACK
        // We don't want the resource editor to observe us
        // because it causes side effects when changing the
        // objectclass attribute.
        _observable.deleteObserver(parent);

		// Build and layout components if not already done
		boolean buildComponent = (_cardLayout == null);
		if (buildComponent) {
        	// Set the layout of this page
        	GridBagLayout gbl = new GridBagLayout();
			_contentPanel = new JPanel(gbl);

        	// Create and layout the panel which enclose the subpages
        	_cardLayout = new CardLayout();
        	_pagePanel = new JPanel(_cardLayout);
	//        _pagePanel.setBorder(new BevelBorder(BevelBorder.RAISED));
        	_pagePanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
        	_contentPanel.add(_pagePanel);
        	GridBagConstraints gbc = new GridBagConstraints() ;
        	gbc.gridx      = 0;
        	gbc.gridy      = 1;
        	gbc.gridwidth  = gbc.REMAINDER;
        	gbc.gridheight = 1;
        	gbc.weightx    = 1;
        	gbc.weighty    = 1;
        	gbc.fill       = gbc.BOTH;
        	gbc.insets     = new Insets(SEPARATED_COMPONENT_SPACE, SEPARATED_COMPONENT_SPACE, SEPARATED_COMPONENT_SPACE, SEPARATED_COMPONENT_SPACE);
        	gbc.ipadx      = 0;
        	gbc.ipady      = 0;
        	gbl.setConstraints(_pagePanel, gbc);
			
			JScrollPane scrollPane = new JScrollPane(_contentPanel);
			scrollPane.setBorder(null);
			
			setLayout(new BorderLayout());
			add("Center", scrollPane);
		}
        

        // The remaining initialization depends on 
        // whether it's a new entry or not
        if (observable.isNewUser()) {
            int subPageCount = _pageInfoVector.size();
            for (int i = 0 ; i < subPageCount ; i++) {
                initializeNewRoleSubPage(i, buildComponent);
            }
        }
        else {
            initializeExistingRoleSubPage(buildComponent);
        }
		}catch(RuntimeException x) {
			x.printStackTrace();
			throw x;
		}
    }
    
    
    
    /**
     * Initialization sequence for a subpage when creating a new entry.
     */
    public void initializeNewRoleSubPage(int index, boolean buildComponent) {
        SubPageInfo info = (SubPageInfo)_pageInfoVector.elementAt(index);
        GridBagLayout gbl = (GridBagLayout)_contentPanel.getLayout();
        
		// Build and layout the components if not already done
		if (buildComponent) {
        	// Create and layout the radio button
        	JRadioButton button = new JRadioButton(info.page.getID());
        	button.setActionCommand(info.page.getID());
        	button.addActionListener(this);
        	_contentPanel.add(button);
        	if (_buttonGroup == null) _buttonGroup = new ButtonGroup();
        	_buttonGroup.add(button);
        	GridBagConstraints gbc = new GridBagConstraints() ;
        	gbc.gridx      = index;
        	gbc.gridy      = 0;
        	gbc.gridwidth  = 1;
        	gbc.gridheight = 1;
        	gbc.weightx    = 1;
        	gbc.weighty    = 0;
        	if (index == 0)
            	gbc.anchor = gbc.WEST;
        	else if (index == (_pageInfoVector.size() - 1)) 
            	gbc.anchor = gbc.EAST;
        	else
            	gbc.anchor = gbc.CENTER;
        	gbc.fill       = gbc.NONE;
        	gbc.insets     = new Insets(SEPARATED_COMPONENT_SPACE, SEPARATED_COMPONENT_SPACE, 0, SEPARATED_COMPONENT_SPACE);
        	gbc.ipadx      = 0;
        	gbc.ipady      = 0;
        	gbl.setConstraints(button, gbc);

        	// Add the subpage to the enclosing panel
        	_pagePanel.add((Component)info.page, info.page.getID());

        	// By default, the first subpage is displayed
        	if (index == 0) {
            	button.setSelected(true);
            	_cardLayout.show(_pagePanel, info.page.getID());
            	_currentPanel = info.page;
            	updateObservableObjectClass(info.objectClass);
        	}
		}
		
		// Load values from observable
        info.page.initialize(_observable, _parent);

    }
    
    
    /**
     * Initialization sequence for an existing role.
     * Here we create components specific to the type of role.
     * We create a label for specifying the type of the role.
     */
    public void initializeExistingRoleSubPage(boolean buildComponent) {
        GridBagLayout gbl = (GridBagLayout)_contentPanel.getLayout();
    
		// Build the components if required
		if (buildComponent) {
		
        	// Create the label to display the role type
        	_typeLabel = new JLabel();
        	_contentPanel.add(_typeLabel);
        	GridBagConstraints gbc = new GridBagConstraints() ;
        	gbc.gridx      = 0;
        	gbc.gridy      = 0;
        	gbc.gridwidth  = 1;
        	gbc.gridheight = 1;
        	gbc.weightx    = 0;
        	gbc.weighty    = 0;
        	gbc.fill       = gbc.NONE;
        	gbc.anchor       = gbc.EAST;
        	gbc.insets     = new Insets(SEPARATED_COMPONENT_SPACE, SEPARATED_COMPONENT_SPACE, 0, SEPARATED_COMPONENT_SPACE);
        	gbc.ipadx      = 0;
        	gbc.ipady      = 0;
        	gbl.setConstraints(_typeLabel, gbc);
		}

        // Search the subpage whose object class match the
        // objectclass attribute in _observable
        Vector objectClass = _observable.get("objectclass");
        SubPageInfo info = null;
        Enumeration e = _pageInfoVector.elements();
        while (e.hasMoreElements() && (info == null)) {
            info = (SubPageInfo)e.nextElement();
            if (!vectorContainsStringArray(objectClass, info.objectClass)) 
				info = null;
        }
        
        // Setup the window on the found subpage
        if (info != null) {
            info.page.initialize(_observable, _parent);
            _typeLabel.setText(info.page.getID());
            _pagePanel.add((Component)info.page, info.page.getID());
            _cardLayout.show(_pagePanel, info.page.getID());
            _currentPanel = info.page;
        }
        else {
            _typeLabel.setText("Unknown Role");
            Debug.println(0, "ResEditorRadioPage.initializeExistingRole:" +
                             _observable.getDN() + " is not a role !");
        }
    }


    /**
     * Implements the IResourceEditorPage interface.
     */
    public String getID() {
        return _id;
    }


    /**
     * Implements the IResourceEditorPage interface.
     * Forward to the current subpage. 
     */
    public boolean afterSave(ResourcePageObservable observable) throws Exception {
        if (_currentPanel != null) 
            return _currentPanel.afterSave(observable);
        else
            return true;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public boolean save(ResourcePageObservable observable) throws Exception {
        if (_currentPanel != null) 
            return _currentPanel.save(observable);
        else
            return true;
    }


    /**
     * Implements the IDSResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public void cancel(ResourcePageObservable observable) throws Exception {
        if ((_currentPanel != null) && (_currentPanel instanceof IDSResourceEditorPage))
            ((IDSResourceEditorPage)_currentPanel).cancel(observable);
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public void clear() {
        if (_currentPanel != null) 
            _currentPanel.clear();
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public void reset() {
        if (_currentPanel != null) 
            _currentPanel.reset();
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public void setDefault() {
        if (_currentPanel != null) 
            _currentPanel.setDefault();
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public boolean isModified() {
        if (_currentPanel != null)
            return _currentPanel.isModified();
        else
            return false;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public void setModified(boolean value) {
        if (_currentPanel != null) 
            _currentPanel.setModified(value);
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public boolean isReadOnly() {
        if (_currentPanel != null)
            return _currentPanel.isReadOnly();
        else
            return true;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public void setReadOnly(boolean value) {
        if (_currentPanel != null) 
            _currentPanel.setReadOnly(value);
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public void setEnable(boolean value) {
        if (_currentPanel != null) 
            _currentPanel.setEnable(value);
    }


    /**
     * Implements the IResourceEditorPage interface.
     * Forward to the current subpage. 
     */
    public boolean isComplete() {
        if (_currentPanel != null)
            return _currentPanel.isComplete();
        else
            return true;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public String getDisplayName() {
        return _id;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Forward to the current subpage. 
     */
    public void help() {
        if (_currentPanel != null) 
            _currentPanel.help();
        else {
            Debug.println(0, "ResEditorRadioPage.help(): no help for unknown role");
        }
    }


    /**
     * Implements the ActionListener interface.
     */
    public void actionPerformed(ActionEvent event) 
    {
        Object cmd = event.getActionCommand();
        
        // Find the subpage which matches the cmd
        Enumeration e = _pageInfoVector.elements();
        SubPageInfo info = null;
        while (e.hasMoreElements() && info == null) {
            info = (SubPageInfo)e.nextElement();
            if (!info.page.getID().equals(cmd)) info = null;
        }
        
        // And display this page
        if (info != null) {
            _cardLayout.show(_pagePanel, info.page.getID());
            _currentPanel = info.page;
            updateObservableObjectClass(info.objectClass);
        }
        else {
            Debug.println(0, "ResEditorRadioPage.actionPerformed(): ignored event " + event);
        }
    }


    /**
     * Update the objectclass attribute in _observable.
     */
    void updateObservableObjectClass(String[] classNameList) {
        Vector objectClass = _observable.get("objectclass");
        if (objectClass == null) {
            Debug.println(0, "ResEditorRadioPage.updateObservableObjectClass: this observable has no object class");
        }
        else {
            // First we remove any value matching an subpage object class.
            Enumeration e = _pageInfoVector.elements();
            while (e.hasMoreElements()) {
                SubPageInfo info = (SubPageInfo)e.nextElement();
                removeStringArrayFromVector(objectClass, info.objectClass);
            }
            // Next we add className
            addStringArrayToVector(objectClass, classNameList);
            _observable.replace("objectclass", objectClass);
        }
    }
    

    //
    // A record for storing subpage related information
    //
    class SubPageInfo {
        public IResourceEditorPage page;
        public String[] objectClass;
        public SubPageInfo(IResourceEditorPage p, String[] c) {
            page = p;
            objectClass = c;
        }
    }
    
    
    // Vector utilities
    
    /**
     * Remove vector elements which match one of the array entry.
     * Equality is tested using String.equalsIgnoreCase().
     */
    static void removeStringArrayFromVector(Vector v, String[] a) {
        for (int i = 0 ; i < a.length ; i++) {
            int pos = DSUtil.indexOfIgnoreCase(v, a[i]);
            if (pos != -1) {
                v.removeElementAt(pos);
            }
        }
    }
    
    /**
     * Add string entries from an array to a vector.
     */
    static void addStringArrayToVector(Vector v, String[] a) {
        for (int i = 0 ; i < a.length ; i++) {
            v.addElement(a[i]);
        }
    }
    

    
    /**
     * Return true if a vector contains an array of strings.
	 * Equality is tested using Object.equalsIgnoreCase().
     */
    static boolean vectorContainsStringArray(Vector v, String[] a) {
		boolean yes = true;
		int i = 0 ;
		while ((i < a.length) && yes) {
			yes = yes && (DSUtil.indexOfIgnoreCase(v, a[i]) != -1);
			i++ ;
		}
		return yes;
    }
    

    
    // State variables
    ResourceEditor _parent;
    ResourcePageObservable _observable;
    Vector _pageInfoVector = new Vector(); // of SubPageInfo
    String _id;

    // Components
    JLabel         _typeLabel;
    ButtonGroup     _buttonGroup;
    JPanel         _pagePanel;
    CardLayout     _cardLayout;
	JPanel         _contentPanel;
    IResourceEditorPage _currentPanel;
}
