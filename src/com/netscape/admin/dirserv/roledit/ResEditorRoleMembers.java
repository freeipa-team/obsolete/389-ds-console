/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.util.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.border.*;

import com.netscape.management.nmclf.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.*;

import netscape.ldap.*;
import com.netscape.admin.dirserv.DSUtil;


/**
 * ResEditorNestedRole is a plugin for the ResourceEditor.
 * It is used to edit information specific to nsfilteredroledefinition entries.
 */
public class ResEditorRoleMembers extends ResEditorRadioPage {


	/**
	 * Constructor.
	 * We populate this radio page with role-specific subpages.
	 */
	public ResEditorRoleMembers() {
		String[] managedClass = { "nssimpleroledefinition", "nsmanagedroledefinition" };
		String[] filteredClass = { "nscomplexroledefinition", "nsfilteredroledefinition" };
		String[] nestedClass = { "nscomplexroledefinition", "nsnestedroledefinition" };
	
		IResourceEditorPage managedPage = new ResEditorManagedRole();
		IResourceEditorPage filteredPage = new ResEditorFilteredRole();
		IResourceEditorPage nestedPage = new ResEditorNestedRole();
		
		_pageInfoVector.addElement(new SubPageInfo(
			managedPage, managedClass));
		_pageInfoVector.addElement(new SubPageInfo(
			filteredPage, filteredClass));
		_pageInfoVector.addElement(new SubPageInfo(
			nestedPage, nestedClass));
	}


    /**
     * We overrides the base class id.
     */
    public void initialize(ResourcePageObservable observable,
                           ResourceEditor parent) {
		super.initialize(observable, parent);
        _id = _resource.getString(_section, "id");  
	}
	
	
	// I18N
	static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "roleMemberPage";

	// LDAP schema
    private static final String NSROLEFILTER_ATTR = "nsRoleFilter";
}
