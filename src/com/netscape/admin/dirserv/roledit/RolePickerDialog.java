/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.ResourceEditor;
import com.netscape.management.client.components.Table;
import com.netscape.management.nmclf.SuiConstants;
import com.netscape.admin.dirserv.*;
import com.netscape.admin.dirserv.panel.MappingUtils;
import netscape.ldap.*;
import netscape.ldap.util.DN;

/**
 * RolePickerDialog is modal dialog which
 * 		- displays the roles in the scope of an LDAP entry,
 *		- allows to select one or more of those roles.
 *
 * A role is 'in the scope' if it is child of the one of the ancestors of
 * the LDAP entry.
 **/

public class RolePickerDialog extends AbstractModalDialog 
                              implements SuiConstants, ListSelectionListener {

	
    /**
     * Construct an new RolePickerDialog.
     * The resulting RolePickerDialog is empty. It must be filled
     * with roles using the load() method.
     */
	public RolePickerDialog(Frame parent, ConsoleInfo info) {
        super(parent, _resource.getString(_section, "title"));
		getAccessibleContext().setAccessibleDescription(_resource.getString(_section, "description"));
                         
        _info = info;
        _roleBag = new RoleCache();
        
		_table = new Table();
        _table.setModel(_roleBag.getTableModel());
        _table.setAutoResizeMode(_table.AUTO_RESIZE_ALL_COLUMNS);
        _table.getSelectionModel().addListSelectionListener(this);
		_table.setPreferredScrollableViewportSize(new Dimension(350, 200));

        layoutComponents();
	}


    /**
     * Load roles in the this RolePickerDialog.
     * This method searches the directory and builds the list of
     * roles which are visible by the targetDn entry.
	 * Roles listed in the exclude enumeration are hidden.
     * Note that targetEntry needs not to exist. However the parent
     * entry must exist.
     * Roles which are not instances of roleClass are ignored.
     * If the RolePickerDialog is visible, the method udpates the
     * display.
     */
    public void load(String targetDn, String roleClass, Enumeration exclude) 
	throws LDAPException {
		// Make a vector with the DNs of the roles to exclude
		Vector excludeDNs = new Vector();
		while (exclude.hasMoreElements()) {
			excludeDNs.addElement(new DN(
										 (String)exclude.nextElement()));
		}

		// Now search the directory and refill _roleBag.
		_roleBag.clear();
		Debug.println("RolePickerDialog.load: searching from " + targetDn);
    	LDAPConnection ldc = _info.getLDAPConnection();
        try {
        	Vector roleEntries = searchRolesVisibleFrom(targetDn, roleClass);
            if (roleEntries != null) {
                Enumeration e = roleEntries.elements();
                while (e.hasMoreElements()) {
					LDAPEntry roleEntry = (LDAPEntry)e.nextElement();
					String roleDn = roleEntry.getDN();
					DN roleDN = new DN(roleDn);
					boolean isToExclude = false;
					for (Enumeration eDN = excludeDNs.elements() ; eDN.hasMoreElements() && !isToExclude;) {
						if (roleDN.equals((DN)eDN.nextElement())) {
							isToExclude = true;
						}
					}
					if (!isToExclude) {
            	    	_roleBag.add(roleEntry);
					}
					else {
						Debug.println("RolePickerDialog.load: excluding " + roleDn);
					}
                }
            }
        }
        catch(LDAPException e) {
			Debug.println( "RoleEditorDialog.load of" + targetDn + ": " + e );    
            throw e;
        }        
    }
    
    
    /**
     * Load roles in the this RolePickerDialog.
     * This method searches the directory and builds the list of
     * roles which can be nested in the role roleDn.
	 * Roles listed in the exclude enumeration are hidden.
     * Note that roleDn needs not to exist. However the parent
     * entry must exist.
     * If the RolePickerDialog is visible, the method udpates the
     * display.
     */
    public void loadWithNestableRoles(String roleDn, Enumeration exclude) 
	throws LDAPException {		
		// Make a vector with the DNs of the entries to exclude
		Vector excludeDNs = new Vector();
		while (exclude.hasMoreElements()) {
			excludeDNs.addElement(new DN(
										 (String)exclude.nextElement()));
		}
		
		// Now search the directory and refill _roleBag.
		_roleBag.clear();
		Debug.println("RolePickerDialog.loadWithNestableRoles: searching for " + roleDn);
    	LDAPConnection ldc = _info.getLDAPConnection();
		String suffix = MappingUtils.getTopSuffixForEntry(ldc, roleDn);
		if (suffix == null) {
			Debug.println(0, "RolePickerDialog.loadWithNestableRoles: cannot determine the suffix of " + roleDn);
		}
		else {
        	try {
        		String filter = "(&(objectclass=nsroledefinition)(objectclass=ldapsubentry))";
        		String[] attrs = RoleCache.BASIC_ATTRS;
				LDAPSearchConstraints cons =
					(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
				cons.setMaxResults( 0 );
        		LDAPSearchResults r = ldc.search(suffix, ldc.SCOPE_SUB, filter, attrs, false, cons);
				while (r.hasMoreElements()) {
            		LDAPEntry entry = r.next();
					String entryDn = entry.getDN();
					DN entryDN = new DN(entryDn);
					boolean isToExclude = false;
					for (Enumeration eDN = excludeDNs.elements() ; eDN.hasMoreElements() && !isToExclude;) {
						if (entryDN.equals((DN)eDN.nextElement())) {
							isToExclude = true;
						}
					}
					if (!isToExclude) {
            	    	_roleBag.add(entry);
					}
					else {
						Debug.println("RolePickerDialog.loadWithNestableRoles: excluding " + entryDn);
					}
        		}				
        	}
        	catch(LDAPException e) {
				Debug.println(0, "RoleEditorDialog.loadWithNestableRoles of" + roleDn + ": " + e );				    	
            	throw e;
        	}
        }
	}


	/**
     * Return a vector of DNs corresponding to the roles
     * selected by the user. Return null if no role have 
     * been selected.
     */
    public Vector getSelectedRoles() {
    	Vector result = null;
    	int roleCount = _table.getModel().getRowCount();
    	ListSelectionModel selModel = _table.getSelectionModel();
        for (int i = 0; i < roleCount; i++) {
        	if (selModel.isSelectedIndex(i)) {
            	if (result == null) result = new Vector();
                result.addElement(_roleBag.getRoleDN(i));
            }
        }
        
    	return result;
    }
    
    
    /**
     * Assemble and layout the components
     */
    void layoutComponents() {
    	Container contentPane = getContentPane();
    	GridBagLayout gbl = new GridBagLayout();
        
        contentPane.setLayout(gbl);

        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 1;
        gbc.anchor     = gbc.CENTER;
        gbc.fill       = gbc.BOTH;
        gbc.insets     = new Insets(0, 0, 0, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
        
        JLabel l = new JLabel(_resource.getString(_section, "label"));
		l.setLabelFor(_table);
        contentPane.add(l) ;
        gbl.setConstraints(l, gbc);

		JScrollPane scrollPane = new JScrollPane(_table);
        scrollPane.setBorder(UIManager.getBorder("Table.scrollPaneBorder"));
		contentPane.add(scrollPane);
        gbc.gridy++;
        gbl.setConstraints(scrollPane, gbc);
      
		pack();
	}


	/**
     * Return the list of role LDAP entries which are visible from the specified dn.
     * This includes:
     *		- all the roles which are children of dn
     *		- all the roles which are visible from the parent of dn
     * Note: this method is recursive.
     */
	Vector searchRolesVisibleFrom(String dn, String roleClass) throws LDAPException {
    	Vector result = null;
        
        //
        // Recurse on the ancestor if dn is not the root
        //
        if (dn != null) {
    		String parentDn = makeParentDn(dn);
        	result = searchRolesVisibleFrom(parentDn, roleClass);
        }
        
        //
        // Add the roles child from dn
        //
    	LDAPConnection ldc = _info.getLDAPConnection();
        String filter = "(&(objectclass=" + roleClass + ")(objectclass=ldapsubentry))";
        String[] attrs = RoleCache.BASIC_ATTRS;
		try {
			LDAPSearchConstraints cons =
					(LDAPSearchConstraints)ldc.getSearchConstraints().clone();
			cons.setMaxResults( 0 );
        	LDAPSearchResults r = ldc.search(dn, ldc.SCOPE_ONE, filter, attrs, false, cons);
			while (r.hasMoreElements()) {
            	LDAPEntry entry = r.next();
            	if (result == null) result = new Vector();
            	result.addElement(entry);
        	}
		}
		catch(LDAPException x) {
			if (x.getLDAPResultCode() != x.NO_SUCH_OBJECT) {
				throw x;
			}
			// NO_SUCH_OBJECT means dn does not exist: in that case,
			// we consider dn is a partial suffix ie a hole in the DIT.
			// We skip silently because there is no attached role definitions.
		}
        
        return result;
    }


    /**
     * Returns the parent dn of a dn.
     */
    static public String makeParentDn(String dn) {
    	String result = null;
    	String[] explosion = LDAPDN.explodeDN(dn, false);
        if ((explosion != null) && (explosion.length >= 2)) {
        	StringBuffer sb = new StringBuffer();
        	for (int i = 1; i < explosion.length; i++) {
            	if (sb.length() >= 1) sb.append(',');
                sb.append(explosion[i]);
            }
            result = sb.toString();
        }
        return result;
    }


    /**
     * Implements ListSelectionListener.
     * Called when the user clicks in the table.
     * We update the tooltip text.
     */
    public void valueChanged(ListSelectionEvent e) {
    	ListSelectionModel m = (ListSelectionModel)e.getSource() ;
                
        // Update the tooltip text of the tab
        int min = m.getMinSelectionIndex() ;
        int max = m.getMaxSelectionIndex() ;
        if ((min == -1) || (max - min + 1 > 1) || (_table.getRowCount() <= 0)) {
        	// The selection is empty or multiple or the table is empty
        	_table.setToolTipText(null);
        }
        else {
            _table.setToolTipText(_roleBag.getRoleDN(min));
        }
    }
    
    
	/**
	 * Overrides AbstractDialog.
	 */
	protected void helpInvoked() {
		DSUtil.help("configuration-choose-role");
	}
	
	/**
	 * Method that sets the dialog in a mode where multiple selections are accepted
	 *
	 * @param allow boolean that indicates if we want to allow multiple selections or not
	 */
	public void allowMultipleSelection(boolean allow) {
		if (allow) {
			_table.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
		} else {
			_table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
		}
	}
	
    // Misc
    IDSModel _model;
    ConsoleInfo _info;
    RoleCache _roleBag;
    
	// Components
    Table _table;

    // I18N
    static private final ResourceSet _resource = DSUtil._resource;
    static private final String _section = "rolePickerDialog";
}
