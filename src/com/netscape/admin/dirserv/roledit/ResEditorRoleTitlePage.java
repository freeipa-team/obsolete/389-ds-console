/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.awt.*;
import java.util.*;
import java.awt.image.*;
import com.netscape.management.nmclf.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.ug.TitlePanel;
import com.netscape.management.client.ug.ResourcePageObservable;
import javax.swing.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.account.*;


/**
 * ResEditorRoleTitlePage is the title panel displayed by the ResourceEditor
 * when editing a role. It is created by ResEditorRoleAccountPage.
 *
 * @see ResEditorRoleAccountPage
 */
public class ResEditorRoleTitlePage extends TitlePanel
{
    /**
	 * Constructor
	 *
	 * @param observable  the observable object
	 */
	public ResEditorRoleTitlePage(ResourcePageObservable o, ActivationModel m) {

		// Observes the activation model
		m.addObserver(this);
		
		// Initializes the components by simulating updates
		_isActivated = true; // By default
		update(o, "cn");
		update(o, "objectclass");
		update(m, null);
	}


    /**
	 * Implements the Observer interface. Updates the information in
	 * this pane when called.
	 *
	 * @param o    the observable object
	 * @param arg  argument
	 */
	public void update(Observable o, Object arg) {

		if ((o instanceof ResourcePageObservable) && (arg != null)) {
			ResourcePageObservable observable = (ResourcePageObservable)o;
			String attrName = arg.toString();
			if (attrName.equals("cn")) {
				setText(observable.get("cn", 0));
			}
			else if (attrName.equals("objectclass")) {
				Vector objectClass = observable.get("objectclass");
				_roleClassName = null;
				if (DSUtil.indexOfIgnoreCase(objectClass, NSMANAGEDROLEDEF_CLASS) != -1)
					_roleClassName = NSMANAGEDROLEDEF_CLASS;
				else if (DSUtil.indexOfIgnoreCase(objectClass, NSFILTEREDROLEDEF_CLASS) != -1)
					_roleClassName = NSFILTEREDROLEDEF_CLASS;
				else if (DSUtil.indexOfIgnoreCase(objectClass, NSNESTEDROLEDEF_CLASS) != -1)
					_roleClassName = NSNESTEDROLEDEF_CLASS;
				
				adjustIcon();
			}  
		}
		else if (o instanceof ActivationModel) {
			_isActivated = ((ActivationModel)o).isActivated();
			adjustIcon();
		}
	}


	/**
	 * The sequence which adjust the icon according
	 * the role class and the activation state.
	 */
	void adjustIcon() {
		String iconName = null;
		RemoteImage iconImage = null;
        if (_roleClassName != null) {
			iconName = DSUtil._resource.getString(_section, _roleClassName + "-icon-24");
        }		
		if (iconName != null) {
        	iconImage = DSUtil.getPackageImage( iconName );
		}
		if (iconImage != null) {
			if ( ! _isActivated )	
				iconImage = DSUtil.inactivatedIcon(iconImage);
			setIcon(iconImage);
		}		
	}


	// State variables aligned on the observable and activation model
	String _roleClassName; // managed, filtered or nested ?
	boolean _isActivated;

	// LDAP schema
	private static final String CN_ATTR = "cn";
	private static final String OBJECTCLASS_ATTR = "objectclass";
	private static final String NSMANAGEDROLEDEF_CLASS = "nsmanagedroledefinition";
	private static final String NSFILTEREDROLEDEF_CLASS = "nsfilteredroledefinition";
	private static final String NSNESTEDROLEDEF_CLASS = "nsnestedroledefinition";
	
	// I18N
	private static final String _section = "EntryObject";
}
