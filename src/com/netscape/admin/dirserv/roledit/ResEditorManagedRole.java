/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.roledit;

import java.net.MalformedURLException;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import com.netscape.management.nmclf.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.*;
import com.netscape.management.client.components.*;
import netscape.ldap.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.IDSResourceEditorPage;
import com.netscape.admin.dirserv.panel.UIFactory;


/**
 * ResEditorManagedRole is a plugin for the ResourceEditor.
 * It is used to edit information specific to nsnesteddroledefinition entries.
 */
public class ResEditorManagedRole extends DefaultResEditorPage
                               implements Observer,
                                          ActionListener,
										  IDSResourceEditorPage,
                                          ListSelectionListener,
                                          AncestorListener,
										  IRPCallBack {
    /**
     * Constructor
     */
    public ResEditorManagedRole() {
        _id = _resource.getString(_section, "id");  
    }


	/**
     * Implements the Observer interface.
     */
	public void update(Observable o, Object arg) {
		// This may be a change to "cn", so let updated _lastRoleName.
		updateLastRoleName();
    }


    /**
     * Implements the IResourceEditorPage interface.
     * Here we build the UI components, search roles from the directory
     * and initialize the UI components with them.
     */
    public void initialize(ResourcePageObservable observable,
                           ResourceEditor parent) {
        _parent = parent;
        _observable = observable;
		_addJournal.removeAllElements();
		_removeJournal.removeAllElements();

		// Build and layout the components if not already done
		if (_searchResultPanel == null) {
        	JLabel label = new JLabel(_resource.getString(_section, "label"));
			_searchResultPanel = new SearchResultPanel(_parent.getConsoleInfo(), this);
			_searchResultPanel.addAncestorListener(this);
			_searchResultPanel.addListSelectionListener(this);
			JTable table = (JTable)DSUtil.searchChildComponent(_searchResultPanel, JTable.class);
			label.setLabelFor(table);
			table.setPreferredScrollableViewportSize(new Dimension(10, 10));
        	_refreshButton = UIFactory.makeJButton(this, _section, "refreshButton", _resource);			
        	_addButton = UIFactory.makeJButton(this, _section, "addButton", _resource);				
        	_removeButton = UIFactory.makeJButton(this, _section, "removeButton", _resource);			

        	JButtonFactory.resizeGroup(_addButton, _removeButton);

        	Box buttonBox = new Box(BoxLayout.X_AXIS);
        	buttonBox.add(_refreshButton);
        	buttonBox.add(Box.createHorizontalGlue());
        	buttonBox.add(_addButton);
        	buttonBox.add(Box.createHorizontalStrut(SuiLookAndFeel.SEPARATED_COMPONENT_SPACE));
        	buttonBox.add(_removeButton);

        	setLayout(new GridBagLayout());
        	GridBagUtil.constrain(this, label,
                            	  0, 0, 1, 1,
                            	  1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
                            	  SuiLookAndFeel.VERT_WINDOW_INSET, SuiLookAndFeel.HORIZ_WINDOW_INSET,
                            	  0, SuiLookAndFeel.HORIZ_WINDOW_INSET);
        	GridBagUtil.constrain(this, _searchResultPanel,
                            	  0, 1, 1, 1,
                            	  1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
                            	  0, SuiLookAndFeel.HORIZ_WINDOW_INSET,
                            	  0, SuiLookAndFeel.HORIZ_WINDOW_INSET);
        	GridBagUtil.constrain(this, buttonBox,
                            	  0, 2, 1, 1,
                            	  0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
                            	  SuiLookAndFeel.SEPARATED_COMPONENT_SPACE, SuiLookAndFeel.HORIZ_WINDOW_INSET,
                            	  SuiLookAndFeel.VERT_WINDOW_INSET, SuiLookAndFeel.HORIZ_WINDOW_INSET);
		}

		// Update _lastRoleName
		updateLastRoleName();
		
		// _searchResultPanel.doSearch() ?
        valueChanged(null); // To update the state of _removeButton
    }


    /**
     * Implements the IResourceEditorPage interface.
     */
    public String getID() {
        return _id;
    }


    /**
     * Implements the IResourceEditorPage interface. 
     * Here we propagate the renaming of the role (if any) to
	 * all the members.
     */
    public boolean save(ResourcePageObservable observable) throws Exception {
		// Note that _lastRoleName != null
		
		String newRoleName = _observable.get("cn", 0);
		// Note that _lastRoleName and newRoleName cannot be null
		// because save() is called only if a name has been given
		// to the role.
		
		// If newRoleName and _lastRoleName differs, this means
		// the user has rename the role. Thus we have to update
		// the nsroledn attribute of each member.
		if (!newRoleName.equals(_lastRoleName)) {
    		ConsoleInfo info = _parent.getConsoleInfo();
			LDAPConnection ldc = info.getUserLDAPConnection();
			
			// Compute last and new role DNs
			String lastRoleDn = makeLastRoleDn();
			String baseDn = RolePickerDialog.makeParentDn(lastRoleDn);
			String newRoleDn = "cn=" + newRoleName + "," + baseDn;
			
			try {
				// Search for entries with nsroledn = lastRoleDn
				String filter = "(nsroledn = " + lastRoleDn + ")";
				LDAPSearchResults sr = ldc.search(baseDn, LDAPv2.SCOPE_SUB, filter, null, true);

				// For each found entry, we replace lastRoleDn by newRoleDn.
				int doneCount = 0;
				int errorCount = 0;
				while (sr.hasMoreElements()) {
					try {
						LDAPEntry entry = sr.next();
						LDAPAttribute remove = new LDAPAttribute("nsroledn", lastRoleDn);
						LDAPAttribute add = new LDAPAttribute("nsroledn", newRoleDn);
						LDAPModificationSet modSet = new LDAPModificationSet();
						modSet.add(LDAPModification.DELETE, remove);
						modSet.add(LDAPModification.ADD, add);
						ldc.modify(entry.getDN(), modSet);
						doneCount++;
					}
					catch(LDAPException x) {
						errorCount++;
						Debug.println(0, "ResEditorManagedRole.save: exception " + x + 
					                	 "while renaming " + newRoleDn);
						if (Debug.getTrace()) {
							x.printStackTrace();
						}
					}
				}
				Debug.println("ResEditorManagedRole.save: updated " + doneCount + " members"); 
			}
			catch(LDAPException x) {
				Debug.println(0, "ResEditorManagedRole.save: exception " + x + 
					             "while searching for members of " + newRoleDn);
				if (Debug.getTrace()) {
					x.printStackTrace();
				}
			}
		}
		
		return true;
	}

    /**
     * Implements the IResourceEditorPage interface. 
     * Here we rollback all the add/remove operations.
     */
    public void cancel(ResourcePageObservable observable) throws Exception {
    	ConsoleInfo info = _parent.getConsoleInfo();
		LDAPConnection ldc = info.getUserLDAPConnection();
	
		// If _lastRoleName == null, no members have been added or removed.
		// So nothing needs to be cancelled.
		if (_lastRoleName != null) {
			String roleDn = makeLastRoleDn();

			// Remove all the new members
			Enumeration e = _addJournal.elements();
			while (e.hasMoreElements()) {
				String entryDn = (String)e.nextElement();
				try {
					Debug.println("ResEditorManagedRole.cancel: cancelling membership of " + entryDn);
					removeManagedRoleFromEntry(ldc, entryDn, roleDn);
				}
				catch(LDAPException x) {
					Debug.println(0, "ResEditorManagedRole.cancel: caught exception " + x);
					if (Debug.getTrace()) {
						x.printStackTrace();
					}
				}
			}

			// Restore all the removed members
			e = _removeJournal.elements();
			while (e.hasMoreElements()) {
				String entryDn = (String)e.nextElement();
				try {
					Debug.println("ResEditorManagedRole.cancel: restoring membership of " + entryDn);
					addManagedRoleToEntry(ldc, entryDn, roleDn);
				}
				catch(LDAPException x) {
					Debug.println(0, "ResEditorManagedRole.cancel: caught exception " + x);
					if (Debug.getTrace()) {
						x.printStackTrace();
					}
				}
			}
		}
    }


    /**
     * Implements the IResourceEditorPage interface. 
     */
    public void help() {
		DSUtil.help("configuration-role-member-managed-help");
    }


    /**
	 * Implements the ActionListener interface.
	 */
    public void actionPerformed(ActionEvent e) 
		{
			_parent.setBusyCursor(true);
		Object source = e.getSource();
		
    	if (source == _addButton) {
        	actionAdd();
        }
        else if (source == _removeButton) {
        	actionRemove();
        }
        else if (source == _refreshButton) {
        	actionRefresh();
        }
		else {
			Debug.println(0, "ResEditorManagedRole.actionPerformed: ignored event" + e);
		}
			_parent.setBusyCursor(false);
	}
    
    /**
     * Process a click on Add
     */
    void actionAdd() {
    	ConsoleInfo info = _parent.getConsoleInfo();
        LDAPConnection ldc = info.getUserLDAPConnection();
	JFrame frame = UtilConsoleGlobals.getActivatedFrame();
		
		// Adding is allowed only if the role has a name.
		if (_lastRoleName == null) {
            JOptionPane.showMessageDialog(
                    frame,
                    _resource.getString(_section, "namedRequired"),
                    _resource.getString(_section, "errortitle"),
                    JOptionPane.ERROR_MESSAGE);
            ModalDialogUtil.sleep();
			return;
		}
		
		// Find the current role and its parent
		String roleDn = makeLastRoleDn();
		String baseDn = RolePickerDialog.makeParentDn(roleDn);

		// Pop up the search user&group dialog
		Debug.println("ResEditorManagedRole.actionAdd: baseDn " + baseDn);
		ConsoleInfo ci = (ConsoleInfo)info.clone();
		ci.setUserBaseDN(baseDn);
		ResourcePickerDlg dlg = new ResourcePickerDlg(
			ci,
			this,
			_parent.getFrame());
        
        _newMembers = null;        
		dlg.show(); // Will update _addedMemberCount
        if (_newMembers != null) {
            addMembers(_newMembers);
            _newMembers = null;
        }
    }
	
	/**
	 * ResourcePickerDlg calls this one with the search result as
	 * arguments.
	 */
	public void getResults(Vector result) {
        _newMembers = result;
    }

	void addMembers (Vector result) {        
		String roleDn = makeLastRoleDn();
    	ConsoleInfo info = _parent.getConsoleInfo();
        LDAPConnection ldc = info.getUserLDAPConnection();
	JFrame frame = UtilConsoleGlobals.getActivatedFrame();
		
		int addedMemberCount = 0;
		Enumeration e = result.elements();
		while (e.hasMoreElements()) {
			LDAPEntry entry = (LDAPEntry)e.nextElement();
			String entryDn = entry.getDN();
			try {
				// Update the nsroledn attribute of entryDn
				if (addManagedRoleToEntry(ldc, entryDn, roleDn)) {
				
					// Update the rollback information
					updateJournalAfterAdd(entryDn);
					addedMemberCount++;
				}
				else { // Inform the user that entryDn is already member
					DSUtil.showInformationDialog(
						frame, "alreadyMember", entryDn,_section);
				}
			}
			catch(LDAPException x) {
				if (Debug.getTrace()) {
					Debug.println(0, "ResEditorManagedRole.actionAdd: exception " + x);
					x.printStackTrace();
				}
				DSUtil.showLDAPErrorDialog(frame, x, "111-title");
			}
		}
		
		// Refresh _searchResultPanel if some members have been added.
		if (addedMemberCount >= 1) {
			actionRefresh();
		}
	}
    
    /**
     * Process a click on Remove
     */
    void actionRemove() {
		JFrame frame = UtilConsoleGlobals.getActivatedFrame();
		Vector v = _searchResultPanel.getSelectedEntries();
		if ((v != null) && (v.size() >= 1)) {
    		ConsoleInfo info = _parent.getConsoleInfo();
            LDAPConnection ldc = info.getUserLDAPConnection();
			
			// Find the current role
        	String roleDn = makeLastRoleDn();
			
			// Update the nsroledn attribute of each selected entry.
			Vector removedVector = new Vector(v.size());
			Enumeration e = v.elements();
			while (e.hasMoreElements()) {
				LDAPEntry entry = (LDAPEntry)e.nextElement();
				String entryDn = entry.getDN();
				try {
					removeManagedRoleFromEntry(ldc, entryDn, roleDn);
					updateJournalAfterRemove(entryDn);
					removedVector.addElement(entry);
				}
				catch(LDAPException x) {
					Debug.println(0, "ResEditorManagedRole.actionRemove: error for " + entryDn);
				}
			}
			
			// Update _searchResultPanel and report error to the user.
			// A error occur if v and removedVector are not of the
			// same size.
			_searchResultPanel.deleteRows(removedVector);
			valueChanged(null); // This update the state of _removeButton
			if (removedVector.size() < v.size()) {
				String errorText = null;
				if (v.size() == 1) {
					errorText = "The operation has failed. Check the ACI of this entry";
				}
				else {
					errorText = "The operation has been failed for the selected entries. Check the ACI of those entries.";
				}
            	JOptionPane.showMessageDialog(
                    	frame,
                    	"Error",
						errorText,
                    	JOptionPane.ERROR_MESSAGE);
            	ModalDialogUtil.sleep();
			}
		}
    }
    
    /**
     * Process a click on Refresh
     */
    void actionRefresh() {
    	JFrame frame = UtilConsoleGlobals.getActivatedFrame();
    	ConsoleInfo info = _parent.getConsoleInfo();
        
		//  is allowed only if the role has a name.
		if (_lastRoleName == null) {
			_searchResultPanel.removeAllElements();
			return;
		}
 
 		String roleDn = makeLastRoleDn();
		String baseDn = RolePickerDialog.makeParentDn(roleDn);
			
        String URLstring = "ldap:///" + baseDn + "??sub?(nsroledn=" + roleDn + ")";
		Debug.println("ResEditorManagedRole.actionRefresh: url = " + URLstring);
        try {
            LDAPUrl ldapURL = new LDAPUrl(URLstring);
            LDAPConnection ldc = info.getUserLDAPConnection();
			_searchResultPanel.removeAllElements();
			_searchResultPanel.doSearch(ldc, ldapURL);
        } catch (MalformedURLException malformedError) {
            JOptionPane.showMessageDialog(
                    frame,
                    _resource.getString(_section,
                    "errortext"),
                    _resource.getString(_section,
                    "errortitle"),
                    JOptionPane.ERROR_MESSAGE);
            ModalDialogUtil.sleep();
        }
    }
    

    /**
     * Implements ListSelectionListener.
     * Called when the user clicks in the table.
     * We update the tooltip text and the state of _removeButton.
	 * 
     */
    public void valueChanged(ListSelectionEvent e) {
		Debug.println(9,"ResEditorManagedRole.valueChanged: e = " + e);
		JTable table = (JTable)DSUtil.searchChildComponent(_searchResultPanel, JTable.class);
		// _removeButton is disabled if the selection is empty: if we use
		// the tab and the table gets the focus JTable.getSelectedRowCount() is 1
		// (even if the row count is 0 !!)
		_removeButton.setEnabled((table.getSelectedRowCount() > 0) &&
								 (table.getRowCount() > 0));
    }
    
    
    /**
     * Implements AncestorListener.
     * When _searchResultPanel is made visible, we
	 * start the search.
     */
    public void ancestorAdded(AncestorEvent e) {
		if (e.getSource() == _searchResultPanel) {
			Runnable r = new Runnable() {
				public void run() {
					actionRefresh();
				}
			};
			SwingUtilities.invokeLater(r);
		}
		else {
			Debug.println(0, "ResEditorManagedRole.componentShown: ignored event " + e);
		}
    }
    
    
    /**
     * Implements ComponentListener.
     * Nothing to do here.
     */
    public void ancestorRemoved(AncestorEvent e) {
    }
    
    
    /**
     * Implements ComponentListener.
     * Nothing to do here.
     */
    public void ancestorMoved(AncestorEvent e) {
    }
    
    
	/**
	 * Update _addJournal and _removeJournal after adding an entry
	 * to the member list.
	 */
	private void updateJournalAfterAdd(String entryDn) {
		int i = _removeJournal.indexOf(entryDn);
		if (i != -1)
			_removeJournal.removeElementAt(i);
		else
			_addJournal.addElement(entryDn);
	}


	/**
	 * Update _addJournal and _removeJournal after removing an entry
	 * from the member list.
	 */
	private void updateJournalAfterRemove(String entryDn) {
		int i = _addJournal.indexOf(entryDn);
		if (i != -1)
			_addJournal.removeElementAt(i);
		else
			_removeJournal.addElement(entryDn);
	}
	
	
	/**
	 * Add a managed role to the nsroledn attribute of the specified entry.
	 * Return false if the role is already part of nsroledn.
	 */
	private boolean addManagedRoleToEntry(LDAPConnection ldc, String entryDn, String roleDn) 
	throws LDAPException {
		boolean result = true;
		
		try {
			LDAPAttribute attr = new LDAPAttribute("nsroledn", roleDn);
			LDAPModification mod = new LDAPModification(LDAPModification.ADD, attr);
			ldc.modify(entryDn, mod);
		}
		catch(LDAPException x) {
			if (x.getLDAPResultCode() == x.ATTRIBUTE_OR_VALUE_EXISTS) 
				result = false;
			else
				throw x;
		}
		
		return result;
	}
	
	
	/**
	 * Remove a managed role from the nsroledn attribute of the specified entry
	 */
	private void removeManagedRoleFromEntry(LDAPConnection ldc, String entryDn, String roleDn) 
	throws LDAPException {
		LDAPAttribute attr = new LDAPAttribute("nsroledn", roleDn);
		LDAPModification mod = new LDAPModification(LDAPModification.DELETE, attr);
		ldc.modify(entryDn, mod);
	}
	
	
	/**
	 * Update _lastRoleName.
	 * In _lastRoleName, we keep a copy of the role name.
	 * If the user renames the role, _lastRoleName will remain unchanged.
	 * Members of the role are searched and created using _lastRoleName.
	 * Thus membership evaluation is not affected by possible renaming.
	 *
	 * Interesting properties of _lastRoleName.
	 * If it is null, this means:
	 *		- we are creating a new role (=> _observable.isNewUser() == true)
	 *		- the user has not given a name to this new role yet
	 *		- there is not add/removal of members to be cancelled
	 * If it is non null and different from "cn", this means:
	 *		- the role has been renamed
	 *		- the renaming must be propagated to all the members when saving.
	 */
	private void updateLastRoleName() {
		if (_lastRoleName == null) {
			_lastRoleName = _observable.get("cn", 0);
			if ((_lastRoleName != null) && (_lastRoleName.length() == 0))
				_lastRoleName = null;
		}
	}
	
	
	/**
	 * Derive the dn from the _lastRoleName.
	 * If _lastRoleName is null, the returned dn is null.
	 */
	private String makeLastRoleDn() {
		String lastRoleDn = null;
		
		if (_lastRoleName != null) {
			String parentDn;
        	if (_observable.isNewUser()) {
				parentDn = _observable.getCreateBaseDN();
			}
        	else {
				parentDn = RolePickerDialog.makeParentDn(_observable.getDN());
			}
			lastRoleDn = "cn=" + _lastRoleName + "," + parentDn;
		}
		
		return lastRoleDn;
	}
	
	
	// State variables
    ResourceEditor _parent;
    ResourcePageObservable _observable;
    String _id;
	String _lastRoleName;
	Vector _addJournal = new Vector();
	Vector _removeJournal = new Vector();
    Vector _newMembers;

	// Components
	SearchResultPanel _searchResultPanel;
    JButton _refreshButton;
    JButton _addButton;
    JButton _removeButton;

	// I18N
	static ResourceSet _resource = DSUtil._resource;
	private static final String _section = "managedRolePage";

	// LDAP schema
    private static final String NSROLEDN_ATTR = "nsRoleDN";
    private static final String NSROLE_ATTR = "nsRole";
    private static final String[] ROLE_ATTRS = { NSROLEDN_ATTR };
}
