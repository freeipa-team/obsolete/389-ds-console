/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;
import netscape.ldap.LDAPAttributeSet;
import netscape.ldap.LDAPModificationSet;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.AbstractDialog;
import com.netscape.admin.dirserv.propedit.DSEntryPanel;

/**
 * DSEntryDialog
 * 
 *
 * @version 1.0
 * @author rweltman
 **/
public class DSEntryDialog  extends AbstractDialog {
    public DSEntryDialog( JFrame parent, DSEntryPanel panel ) {
		super(parent, "", true, OK|CANCEL|HELP);
		setComponent(panel);
//		setSize( 485, 320 );
		_entryPanel = panel;
		pack();
    }

	protected void okInvoked() {
		if (isValidDN()) {
			super.okInvoked();
		}
	}

	/**
	 * handle help event
	 */
	protected void helpInvoked() {
		int options = _entryPanel.getOptions();
		String helpToken;
		if (options == DSEntryPanel.SHOWINGDN_NAMING) {
			helpToken = "property-main-create-help";
		} else {
			helpToken = "property-main-help";
		}
		DSUtil.help( helpToken );
	}

	/**
	 * Get results of the dialog.
	 *
	 * @return Any changes implied for the entry.
	 */
    public LDAPModificationSet getChanges() {
		return (_entryPanel.getChanges());
	}

	/**
	 * Get results of the dialog.
	 *
	 * @return Any attributes defined for the entry.
	 */
	public LDAPAttributeSet getAttributes() {
		return (_entryPanel.getAttributes());
	}

	public boolean isValidDN() {
		return (_entryPanel.isValidDN());
	}

	public String getDN() {
		return (_entryPanel.getDN());
	}

	public String[] getNamingAttributes() {
		return (_entryPanel.getNamingAttributes());
	}

	public void packAndShow() {
		pack();
		/* Using pack the window can be too big (for example for the entry of the schema)*/
		double height = getSize().getHeight();
		double width = getSize().getWidth();

		double parentHeight = getOwner().getSize().getHeight();
		double parentWidth = getOwner().getSize().getWidth();
		
		boolean isTooBig = false;

		if (parentHeight < height) {
			height = parentHeight;
			isTooBig = true;
		}
		
		if (parentWidth < width) {
			width= parentWidth;
			isTooBig = true;
		}
		
		if (isTooBig) {
			setSize((int) width, (int) height);
		}
		
		show();
	}					

	private DSEntryPanel _entryPanel;
}
