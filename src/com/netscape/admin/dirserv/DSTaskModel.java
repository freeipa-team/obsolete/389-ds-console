/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ace.ACIManager;
import com.netscape.management.client.security.CertificateDialog;
import com.netscape.management.client.security.PKCSConfigDialog;
import com.netscape.management.client.security.CertMigrateWizard;
import com.netscape.management.client.preferences.*;
import netscape.ldap.*;


public class DSTaskModel extends TaskModel
	implements IMenuInfo {

    public DSTaskModel( Vector v,
						ConsoleInfo ci, ConsoleInfo si ) {
		_serverInfo = si;
		_info = ci;
		if (v != null) {
			_authListeners = v;
		}
		init();
	}

	public void reset( Vector v,
	                   ConsoleInfo ci, ConsoleInfo si) {
		_serverInfo = si;
		_info = ci;
		if (v != null) {
			_authListeners = v;
		}
		init();
	}


    private void init() {
		TaskObject root = new TaskObject( "root", _info );
		root.setAllowsChildren(true);
		String serverDN = _serverInfo.getCurrentDN();
		if ( serverDN != null ) {
			/* Check if there is a list of tasks */
			String order = findTaskOrder( serverDN );

			/* Accumulate tasks in a hash table */
			Hashtable list = new Hashtable();
			Debug.println( "DSTaskModel.init: Searching for tasks under " +
						   serverDN );
			findTasks( root, serverDN, list );
			/* Need to go up one from the instance entry, to get non-instance-
			   specific task entries. */
			serverDN = "cn=Tasks," +
				new netscape.ldap.util.DN(
					_serverInfo.getCurrentDN() ).getParent().toString();
			Debug.println( "DSTaskModel.init: Searching for tasks under " +
						   serverDN );
			findTasks( root, serverDN, list );
			Debug.println(9, "DSTaskModel.init: Finished searching for tasks");

			/* Now sort them by preferred order, or just list them as found */
			if ( order != null ) {
				StringTokenizer st = new StringTokenizer( order, " " );
				while( st.hasMoreTokens() ) {
					TaskObject task = (TaskObject)list.get( st.nextToken() );
					if ( task != null ) {
						root.add(task);
					}
				}
			} else {
				Enumeration en = list.elements();
				while( en.hasMoreElements() ) {
					root.add( (TaskObject)en.nextElement() );
				}
			}
		} else {
			Debug.println( "DSTaskModel.init: no currentDN" );
		}
		setRoot(root);
	}

    private void findTasks( TaskObject root, String base, Hashtable list ) {
		// connect to the DS and search for task information
		LDAPConnection ldc = _info.getLDAPConnection();
		if ( ldc == null)
			return;
		try {
			String[] attrs = {"nsclassname", "nsexecref"};
			Debug.println(9, "DSTaskModel.findTasks: searching for tasks " +
						  "under DN=" + base + " on directory server " +
						  DSUtil.format(ldc));
			LDAPSearchResults result =
				ldc.search( base, ldc.SCOPE_SUB,
							"(objectclass=nstask)",
							attrs, false );

			while ( result.hasMoreElements() ) {
				String sJavaClassName = null;
				LDAPEntry findEntry = (LDAPEntry)result.nextElement();
				LDAPAttribute anAttr =
					findEntry.getAttribute( attrs[0] );
				if ( anAttr != null )
					sJavaClassName =
						LDAPUtil.flatting( anAttr.getStringValues() );
				Debug.println(9, "DSTaskModel.findTasks: Found task " +
							  findEntry.getDN() +
							  " class name=" + sJavaClassName);
				if ( sJavaClassName != null ) {
					// load the associated task class file
					try {
						Class c =
							ClassLoaderUtil.getClass(_info,
													 sJavaClassName);
						Debug.println(9, "DSTaskModel.findTasks: loaded " +
									  " class =" + c);
						TaskObject task = (TaskObject)c.newInstance();
						Debug.println(9, "DSTaskModel.findTasks: new " +
									  " task =" + task);
						ConsoleInfo taskConsoleInfo =
							(ConsoleInfo)_serverInfo.clone();
						taskConsoleInfo.setCurrentDN(findEntry.getDN());
						/* Add a listener interface for
						   authentication changes */
						taskConsoleInfo.put( DSUtil.AUTH_CHANGE_LISTENERS,
											 _authListeners );
						anAttr = findEntry.getAttribute( attrs[1] );
						if ( anAttr != null ) {
							String s = LDAPUtil.flatting(
								anAttr.getStringValues() );
							taskConsoleInfo.put( "execref", s );
						}
						taskConsoleInfo.put( "dstaskmodel", this );
						taskConsoleInfo.put( GlobalConstants.CONSOLE_INFO, _info );
						task.setConsoleInfo(taskConsoleInfo);
						String[] rdns =
							LDAPDN.explodeDN( findEntry.getDN(), true );
						list.put( rdns[0], task );
						Debug.println(9, "DSTaskModel.findTasks: finish task " +
									   task );
					} catch (Exception e) {
						Debug.println("DSTaskModel.findTasks: could not " +
									  "load class: " + sJavaClassName + ", " +
									  e);
						// This implicitly means that this task should
						// not show up in
						// in the Task list.
					}
				}
			}
		} catch ( LDAPException e ) {
			Debug.println( "DSTaskModel.findTasks: " + e.toString() );
		}
	}

	private String findTaskOrder( String base ) {
		String order = null;
		/* See if there is a personal preference set */
		PreferenceManager pm =
			PreferenceManager.getPreferenceManager(Framework.IDENTIFIER,
												   Framework.VERSION);
		Preferences p = pm.getPreferences(PREFERENCES_TASK_TAB);
		if ( p != null ) {
			order = p.getString( PREFERENCES_TASK_LIST );
			if ( (order != null) && (order.trim().length() > 0) ) {
				return order;
			} else {
				order = null;
			}
		}

		LDAPConnection ldc = _info.getLDAPConnection();
		if ( ldc == null ) {
			return null;
		}
		/* Check if there is a list */
		try {
			String dn = "cn=task summary, cn=Operation, cn=Tasks," + base;
			String[] attrs = {"description"};
			LDAPEntry entry = ldc.read( dn, attrs );
			if ( entry != null ) {
				LDAPAttribute attr = entry.getAttribute( attrs[0] );
				if ( attr != null ) {
					order = (String)attr.getStringValues().nextElement();
				}
			}
		} catch ( LDAPException ex ) {
			Debug.println( "DSTaskModel.findTaskOrder: no list of tasks, " +
						   ex );
		}
		return order;
	}

    /**
      * Called internally when page is selected
      */
	public void pageSelected( IFramework framework, IPage viewInstance ) {
		if (getRefreshUponSelect()) {
			init();
			setRefreshUponSelect(false);
		}
	}

	public void setSelectedPage(IPage viewInstance) {
		if (viewInstance != null)
			_viewInstance = viewInstance;
	}

	public IPage getSelectedPage() {
		return _viewInstance;
	}

    /**
	 * Returns supported menu categories
	 */
    public String[] getMenuCategoryIDs() {
		if (_categoryID == null) {
			_categoryID = new String[]
				{
					Framework.MENU_FILE,
						Framework.MENU_EDIT,
						ResourcePage.MENU_CONTEXT,
						MENU_SECURITY
						};
        }
		return _categoryID;
	}

	/**
	 * add menu items for this page.
	 */
    public IMenuItem[] getMenuItems(String categoryID) {
		if(categoryID.equals(Framework.MENU_FILE)) {
			if (_menuFile == null) {
				_menuFile = new IMenuItem[] {
					new MenuItemCategory( MENU_SECURITY,
										  _resource.getString("menu", "Security")),
						new MenuItemSeparator(),
						new MenuItemText( MENU_OPEN,
										  _resource.getString("menu", "open"),
										  _resource.getString("menu",
															  "open-description")),								
						};				
			}
			return _menuFile;
		} else if( categoryID.equals(MENU_SECURITY) ) {
			if (_menuSecurity == null) {
				_menuSecurity = new IMenuItem[] {
					new MenuItemText(
									 MENU_KEYCERT_MANAGEMENT,
									 _resource.getString( "menu",
														  "SecurityCertManagement"),
									 _resource.getString( "menu",
														  "SecurityCertManagement-description")),
						new MenuItemText(
										 MENU_PKCS11,
										 _resource.getString("menu",
															 "SecurityPKCS11"),
										 _resource.getString("menu",
															 "SecurityPKCS11-description")),
						new MenuItemText(
										 MENU_KEYCERT_IMPORT,
										 _resource.getString( "menu",
															  "SecurityCertImport"),
										 _resource.getString( "menu",
															  "SecurityCertImport-description"))
						};
			}
			return _menuSecurity;
		} else if( categoryID.equals(Framework.MENU_EDIT) ) {
			if (_menuEdit == null) {
				_menuEdit = new IMenuItem[] {
					new MenuItemText( MENU_ACL,
									  _resource.getString("menu", "editacls"),
									  _resource.getString("menu",
														  "editacls-description"),
									  false
									  )
						};
			}
			return _menuEdit;
		}
		else if ( categoryID.equals(ResourcePage.MENU_CONTEXT) ) {
			if (_menuContext == null) {
				_menuContext = new IMenuItem[] {
					new MenuItemText( MENU_ACL,
									  _resource.getString("menu", "editacls"),
									  _resource.getString("menu",
														  "editacls-description")
									  )
						};
			}
			return _menuContext;
		}
		return null;
	}

    public void actionObjectSelected( IPage viewInstance,
									  ITaskObject selection,
									  ITaskObject previousSelection ) {
		super.actionObjectSelected( viewInstance, selection,
									previousSelection);
		setSelectedPage(viewInstance);
		_selection = selection;
		if( selection == null ) {
			fireDisableMenuItem( viewInstance, MENU_ACL );
		} else {
			fireEnableMenuItem( viewInstance, MENU_ACL );
		}
 	}

    /**
	 * Notification that a menu item has been selected.
	 */
    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
		setSelectedPage(viewInstance);
		if( item.getID().equals(MENU_OPEN) ) {
			actionObjectRun(viewInstance, _selection);
		} else if( item.getID().equals(MENU_ACL) ) {
			if ( _selection == null ) {
				return;
			}
			String dn = _selection.getConsoleInfo().getCurrentDN();
			ACIManager acm = new ACIManager(
				getFrame(),
				dn,
				dn);
			acm.show();
		} else if( item.getID().equals(MENU_KEYCERT_MANAGEMENT) ) {
			Framework f = (Framework)viewInstance.getFramework();
			try {
				f.setBusyCursor(true);

				CertificateDialog dlg = new CertificateDialog( 
					getFrame(),
					_info,
					(String)_serverInfo.get("SIE") );
				dlg.showModal();
			}
			finally {
				f.setBusyCursor(false);
			}
		} else if( item.getID().equals(MENU_PKCS11) ) {
			Framework f = (Framework)viewInstance.getFramework();
			try {
				f.setBusyCursor(true);

				PKCSConfigDialog dlg = new PKCSConfigDialog( 
					getFrame(),
					_info,
					(String)_serverInfo.get("SIE") );
				dlg.setVisible(true);
			}
			finally {
				f.setBusyCursor(false);
			}
		} else if( item.getID().equals(MENU_KEYCERT_IMPORT) ) {
			actionMenuImport(viewInstance);
		}
	}


	/**
	 * Import menu action.
	 * Before opening the 'certificate import' dialog, 
	 * we check if SSL is enabled or not. If yes, we
	 * inform the user, it's not possible to import.
	 */
	public void actionMenuImport(IPage viewInstance) {
		DSFramework fmk = (DSFramework)viewInstance.getFramework();
		DSAdmin serverObj = fmk.getServerObject();

		// Check if SSL is enabled
		boolean sslEnabled;
		try {
			sslEnabled = serverObj.getSecurityState() == DSAdmin.SECURITY_ENABLE;
		}
		catch(LDAPException x) {
			Debug.println(0, "DSTaskModel.actionMenuImport: failed to read directory");
			Debug.println(0, "DSTaskModel.actionMenuImport: SSL is considered as off");
			sslEnabled = false;
		}

		// Open the 'import certificate' dialog or warn the user.
		if (sslEnabled) {
			DSUtil.showInformationDialog(getFrame(), "impossible-when-ssl-enabled", "", "dirtask");
		}
		else {
			CertMigrateWizard w = new CertMigrateWizard(
				getFrame(),
				_info,
				(String)_serverInfo.get("SIE") );
			w.setVisible(true);
			serverObj.fireDSAdminEvent(DSAdminEvent.IMPORT_CERTIFICATE);
		}
	}


    /**
      * Notification that the framework window is closing.
      * Called by ResourcePage
      */
	public void actionViewClosing(IPage viewInstance)
		        throws CloseVetoException {
		/* No unsaved changes in this model */
	}

    public void setWaitCursor( boolean on ) {
//		Debug.println( "DSTaskModel.setWaitCursor: " + on +
//					   ", _viewInstance = " + _viewInstance );
		fireChangeFeedbackCursor(
			null,
			on ? FeedbackIndicator.FEEDBACK_WAIT :
		         FeedbackIndicator.FEEDBACK_DEFAULT );

		if (on) {
			fireChangeStatusItemState( getSelectedPage(),
									   ResourcePage.STATUS_PROGRESS,
									   StatusItemProgress.STATE_BUSY );
		}
		else {
			fireChangeStatusItemState( getSelectedPage(),
									   ResourcePage.STATUS_PROGRESS,
									   new Integer(0) );
        }
	}

	public void setRefreshUponSelect(boolean val) {
		_refreshUponSelect = val;
	}

	public boolean getRefreshUponSelect() {
		return _refreshUponSelect;
	}
	
	public void setFrame(JFrame f) {
		_frame = f;
	}

	public JFrame getFrame() {
		if (_frame == null) {
			Debug.println(0, "DSTaskModel.getFrame: returning null frame !");
		}
		return _frame;	
	}
	
	// This flag will be true if the view for this model needs
	// to be refreshed the next time it's displayed e.g. because
	// the authentication changed and the new user may not have
	// permission to see the same view the old user saw
	private boolean _refreshUponSelect = false;
	static public String MENU_OPEN = "OPEN";
	static public String MENU_ACL = "ACL";
	static public String MENU_SECURITY = "SECURITY";
	static public String MENU_KEYCERT_MANAGEMENT = "CERT_MANAGEMENT";
	static public String MENU_PKCS11 = "PKCS_MANAGEMENT";
	static public String MENU_KEYCERT_IMPORT = "CERT_IMPORT";
    private static final String PREFERENCES_TASK_TAB = "TaskTab";
    private static final String PREFERENCES_TASK_LIST = "TaskList";
	static ResourceSet _resource = DSUtil._resource;
    protected ITaskObject _selection;
	private IPage _viewInstance = null;
	private ConsoleInfo _info;
	private ConsoleInfo _serverInfo;
	private Vector _authListeners = null;
	private JFrame _frame;
	private String[] _categoryID = null;
	private IMenuItem[] _menuFile = null;
	private IMenuItem[] _menuContext = null;
	private IMenuItem[] _menuSecurity = null;
	private IMenuItem[] _menuEdit = null;	
}
