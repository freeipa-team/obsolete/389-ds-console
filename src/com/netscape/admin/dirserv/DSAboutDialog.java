/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.StringTokenizer;
import javax.swing.JFrame;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.RemoteImage;
/**
 * Display this dialog as the About dialog in the Directory console.
 *
 * DSAboutDialog
 * 
 *
 * @version 1.0
 * @author rweltman
 **/

public class DSAboutDialog extends com.netscape.management.client.AboutDialog{
	/**
	 * constructor
	 *
	 * @param parentFrame parent frame
	 * @param resourceSet resourceSet which contains the about strings
	 */
    public DSAboutDialog( JFrame parent, ResourceSet resourceSet ) {
	  super( parent, resourceSet.getString(_section, "dialogTitle" ) );
	  
	  
	  setProduct ( new RemoteImage(resourceSet.getString(_section, "productLogo")), 
				   resourceSet.getString(_section, "productCopyright"),
				   resourceSet.getString(_section, "productLicense") );

	  StringTokenizer st = 
		new StringTokenizer(resourceSet.getString( _section,"vendorsList"), 
							",", 
							false); 
	  
	  while (st.hasMoreTokens()) { 
		String token = st.nextToken(); 
		RemoteImage logo = null; 
		try { 
             logo = new RemoteImage(resourceSet.getString(_section,
														  "vendor-"+token+"-logo")); 
		} catch (Exception e) {} 
         addVendor(logo, resourceSet.getString(_section,
											   "vendor-"+token+"-license")); 
     } 
	}
	private static final String _section = "aboutDialog";
}
