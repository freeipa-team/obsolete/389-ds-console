/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.*;
import java.io.UnsupportedEncodingException;
import javax.swing.*;
import javax.swing.event.*;
import netscape.ldap.*;
import netscape.ldap.controls.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.task.CreateVLVIndex;

class VListEntryModel extends AbstractListModel {
	VListEntryModel( LDAPConnection ldc, String base,
					 int scope, String filter,
					 IDSModel model,
					 String selectedPartitionView) {
		_ldc = (LDAPConnection)ldc.clone();
		_base = base;
		_scope = scope;
		_filter = filter;
		_model = model;
		_host = ldc.getHost();
		_port = ldc.getPort();
		_authDN = ldc.getAuthenticationDN();
		_authPassword = ldc.getAuthenticationPassword();
		_selectedPartitionView = selectedPartitionView;
	}

	VListEntryModel( LDAPConnection ldc, String base,
					 int scope, String filter,
					 IDSModel model ) {
		this(ldc, base, scope, filter, model, DSContentModel.ALL);
	}

	/**
	 * Called by JList to get virtual list size
	 * @return Virtual list size
	 */
    public int getSize() {
		Debug.println(9, "VListEntryModel.getSize()");
		if ( !_initialized ) {
			_initialized = true;
			_pageControls = new LDAPControl[2];
			// Paged results also require a sort control.  We take them from CreateVLVIndex (where we create VLV Indexes)
			LDAPSortKey[] keys = new LDAPSortKey[CreateVLVIndex.DEFAULT_ATTRIBUTES.length];
			for (int i=0; i<keys.length; i++) {
				keys[i] = new LDAPSortKey(CreateVLVIndex.DEFAULT_ATTRIBUTES[i]);
			}
			_pageControls[0] = new LDAPSortControl( keys, true );
			// Do an initial search to get the virtual list size
			// Keep one page before and one page after the start
			_beforeCount = _pageSize;
			_afterCount = _pageSize;
			// Create the initial paged results control
			LDAPVirtualListControl cont =
				new LDAPVirtualListControl( "A",
											_beforeCount,
											_afterCount );
			_pageControls[1] = cont;
			_vlc = (LDAPVirtualListControl)_pageControls[1];
			getPage( 0 );
		}
		Debug.println(9, "VListEntryModel.getSize()=" + _size);
		return _size;
	}

	// Called by JList to fetch data to paint a single list item
    public Object getElementAt(int index) {
		Debug.println(9, "VListEntryModel.getElementAt: need entry " +
					  index );
		if ( _mismatch ) {
			Debug.println(9, "VListEntryModel.getElementAt: mismatch");
			IDSEntryObject idseo = DSEntryObject.getBogusEntryObject();
			_entries.addElement(idseo);
			return idseo;
		}
		Debug.println(9, "VListEntryModel.getElementAt: _top=" + _top +
			" _entries.size()=" + _entries.size());
		if ( (index < _top) || (index >= _top + _entries.size()) ) {
			Debug.println(9, "VListEntryModel.getElementAt: fetching a " +
						  "page starting at " + index );
			if ( !getPage( index ) ) {
				Debug.println(9, "VListEntryModel.getElementAt: " +
							  "first getPage(" + index + ") is null");
				return DSEntryObject.getBogusEntryObject();
			}
			/* DS 4.0 may return entries that do not include the requested
			   one! If so, reduce the preceding offset. */
			int offset = index - _top;
			if ( offset >= _entries.size() ) {
				_beforeCount = _entries.size() / 3;
				_afterCount = _pageSize * 2 - _beforeCount;
				if ( !getPage( index ) ) {
					Debug.println(9, "VListEntryModel.getElementAt: " +
								  "second getPage(" + index + ") is null");
					return DSEntryObject.getBogusEntryObject();
				}
			}
		}
		int offset = index - _top;
		if ( (offset < 0) || (offset >= _entries.size()) ) {
			Debug.println(9, "VListEntryModel.getElementAt: " +
						  "bogus offset=" + offset);
			IDSEntryObject idseo = DSEntryObject.getBogusEntryObject();
			_entries.addElement(idseo);
			return idseo;
		}
		else {
			Debug.println(9, "VListEntryModel.getElementAt: " +
						  "success for element=" + offset);
			IDSEntryObject idseo =
				(IDSEntryObject)_entries.elementAt( offset );
			Debug.println(9, "VListEntryModel.getElementAt: " +
						  "object=" + idseo);
			return idseo;
//			return (IDSEntryObject)_entries.elementAt( offset );
		}
	}

	/**
	 * Force recalculation of list size
	 */
	public void recalculate() {
		Debug.println(9, "VListEntryModel.recalculate: BEGIN");
		_initialized = false;
		getSize();
	}
	
	private IDSEntryObject getEntryObject( LDAPEntry entry ) {
		Debug.println(8, "VListEntryModel.getEntryObject: entry=" + entry);
		DSEntryObject dseo = new DSEntryObject( _model, entry,
												getReferralsEnabled());
		return dseo;
	}

	private String getNameForEntry( LDAPEntry entry ) {
		LDAPAttribute attr = entry.getAttribute( "cn" );
		if ( attr != null ) {
			Enumeration en = attr.getStringValues();
			if( en.hasMoreElements() ) {
				return (String)en.nextElement();
			}
		}
		String[] rdns = LDAPDN.explodeDN( entry.getDN(), true );
		return rdns[0];
	}

	private LDAPVirtualListResponse getEntries() {
		Debug.println(9, "VListEntryModel.getEntries: begin" );
		LDAPVirtualListResponse lVLresponse = null;
		if ( _ldc == null ) {
			return lVLresponse;
		}
		// Specify necessary controls for vlv
		LDAPSearchConstraints cons  =
				(LDAPSearchConstraints)_ldc.getSearchConstraints().clone();
		cons.setMaxResults( 0 );

		LDAPControl[] controls = _pageControls;

		if (_selectedPartitionView.equals(DSContentModel.ALL)) {
			if ( !getReferralsEnabled() ) {
				controls = new LDAPControl[_pageControls.length + 1];
				for( int i = 0; i < _pageControls.length; i++ ) {
					controls[i] = _pageControls[i];
				}
				controls[_pageControls.length] = _manageDSAITControl;
			} else {
				Debug.println(9, "VListEntryModel.getEntries: referrals " +
							  "are enabled");
			}
		} else {
			if ( !getReferralsEnabled() ) {
				try {
					byte[] vals = _selectedPartitionView.getBytes( "UTF8" );
					LDAPControl searchControl = new LDAPControl(DSEntryObject.SEARCH_OID, true, vals);					
					controls = new LDAPControl[_pageControls.length + 2];
					for( int i = 0; i < _pageControls.length; i++ ) {
						controls[i] = _pageControls[i];
					}
					controls[_pageControls.length-1] = _manageDSAITControl;
					controls[_pageControls.length] = searchControl;
				} catch ( UnsupportedEncodingException e ) {
					Debug.println( "VListEntryModel.getEntries(): Error: UTF8 not supported" );
					controls = new LDAPControl[_pageControls.length + 1];
					for( int i = 0; i < _pageControls.length; i++ ) {
						controls[i] = _pageControls[i];
					}
					controls[_pageControls.length] = _manageDSAITControl;
				}
			} else {
				try {
					byte[] vals = _selectedPartitionView.getBytes( "UTF8" );
					LDAPControl searchControl = new LDAPControl(DSEntryObject.SEARCH_OID, true, vals);					
					controls = new LDAPControl[_pageControls.length + 1];
					for( int i = 0; i < _pageControls.length; i++ ) {
						controls[i] = _pageControls[i];
					}					
					controls[_pageControls.length] = searchControl;
				} catch ( UnsupportedEncodingException e ) {
					Debug.println( "VListEntryModel.getEntries(): Error: UTF8 not supported" );					
				}
			}
		}
		
		cons.setServerControls( controls );

		// Empty the buffer
		_entries.removeAllElements();
		// Do a search
		try {
			Debug.println(8, "VListEntryModel.getEntries: searching <" +
						  _base + ">, " + _filter );
			LDAPSearchResults result =
				_ldc.search( _base,
								 _scope,
								 _filter,
								 DSEntryObject.MINIMAL_ATTRS,
								 false,
								 cons );
			int numReadEntries = 0;
			while ( result.hasMoreElements() ) {
				try {
					Debug.println(8, "VListEntryModel.getEntries: " +
						"getting next entry from result" );
					LDAPEntry entry = result.next();
					Debug.println(8, "VListEntryModel.getEntries: adding <" +
								   entry.getDN() + ">" );
					_entries.addElement( getEntryObject( entry ) );
					numReadEntries++;
				} catch ( LDAPReferralException ref ) {
					/* This clause only executed if automatic referral
					   following is off (which it isn't) */
					LDAPUrl refUrls[] = ref.getURLs();
					for( int i = 0; i < refUrls.length; i++ ) {
						Debug.println( "VListEntryModel.getEntries: " +
									   "referral " + refUrls[i].getUrl() );
					}
				}
			}
			int numRequestedEntries = _afterCount + _beforeCount;
			// Check if we have a control returned
			LDAPControl[] c = _ldc.getResponseControls();
			if (c != null) {
				lVLresponse = LDAPVirtualListResponse.parseResponse( c );
				if (lVLresponse != null) {
					Debug.println(8, "VListEntryModel.getEntries: result <" +
								  result.hasMoreElements() +
								  "> total content count=" +
								  lVLresponse.getContentCount());
					if (lVLresponse.getContentCount() < numRequestedEntries) {
						numRequestedEntries = lVLresponse.getContentCount();
					}
				} else {
					Debug.println(8,
								  "VListEntryModel.getEntries: VLVResponse is null");
				}
			} else {
				Debug.println(8,
							  "VListEntryModel.getEntries: null response controls");
				// this will typically happen when searching the front end entries
				// e.g. cn=config.  VLV searches are not supported on those entries
				// just assume the number of entries read is the number we
				// requested
				numRequestedEntries = numReadEntries;
			}
			Debug.println(9, "VListEntryModel.getEntries: requested " +
						  numRequestedEntries + " entries, read " +
						  numReadEntries);
			for (int ii = numReadEntries; ii < numRequestedEntries; ++ii) {
				_entries.addElement(DSEntryObject.getBogusEntryObject());
			}
		} catch ( LDAPException e ) {
			Debug.println( "VListEntryModel.getEntries: " + e +
						   ", searching" );
			if ( _pageControls == null ) {
				return null;
			}
		}
		Debug.println(8, "VListEntryModel.getEntries: Returning " +
					   _entries.size() + " entries for " + _base );
		return lVLresponse;
	}

	// Get a page starting at first (although we may also fetch
	// some preceding entries)
	boolean getPage( int first ) {
		// Get a full buffer, if possible
		int offset = first - _beforeCount;
		if ( offset < 0 ) {
			first -= offset;
		}
		_vlc.setRange( first, _beforeCount, _afterCount );
		Debug.println(9, "VListEntryModel.getPage: Setting requested " +
					  "range to " +
					  first + ", -" + _beforeCount + ", +" +
					  _afterCount );
		return getPage();
	}
	
	// Fetch a buffer
	boolean getPage() {
		// Get the actual entries
		LDAPVirtualListResponse nextCont = getEntries();

		// Check if we have a control returned
		if ( nextCont == null ) {
			_size = _entries.size();
			_vlc.setListSize(_size);
			_selectedIndex = 0;
		} else {
			_selectedIndex = nextCont.getFirstPosition() - 1;
			_top = Math.max( 0, _selectedIndex - _beforeCount );
			Debug.println(9, "VListEntryModel.getPage: _selectedIndex=" +
						  _selectedIndex + " _beforeCount=" + _beforeCount +
						  " _entries.size()=" + _entries.size());
			// Now we know the total size of the virtual list box
			_size = nextCont.getContentCount();
			if ( _size < _entries.size() ) {
				Debug.println( "VListEntryModel.getPage: Reported list " +
							   "size = " + _size );
				_size = _entries.size();
			} else if ( _size > _entries.size() ) {
				// in some cases, e.g. bad referrals, there can be more
				// elements in the vlv control than could actually be
				// retrieved from the directory.  We fill in the list
				// with bogus entries
				Debug.println( "VListEntryModel.getPage: mismatch " +
							   (_size - _entries.size()) + " entries");
			}
			_vlc.setListSize( _size );
			Debug.println(8, "VListEntryModel.getPage: Virtual window: " +
						   _top + ".." + (_top+_entries.size()-1) +
						   " of " + _size );
			_mismatch = ( (_size > 0) && (_entries.size() == 0) );
		}
		return true;
	}

	// Called by application to find out the virtual selected index
    public int getSelectedIndex() {
		return _selectedIndex;
	}

	// Called by application to find out the top of the buffer
    public int getFirstIndex() {
		return _top;
	}

	/** 
	  * Called by DSEntryList to get the entries in memory
	  *
	  * @returns the vector with the entries in memory
	  */
	public Vector getStoredEntries() {
		return _entries;
	}

	public void setDebug( boolean debug ) {
		_debug = debug;
	}

	public void setPageSize( int size ) {
		_pageSize = size;
	}

    public void reset() {
		Debug.println(9, "VListEntryModel.reset: BEGIN");
		if (_entries != null && _entries.size() > 0) {
			Debug.println(9, "VListEntryModel.reset: old first entry=" +
						  _entries.elementAt(0));
		}
		_entries.removeAllElements();
		_initialized = false;
	}

	/**
	 * Set a parameter for future searches, which determines if the
	 * ManagedSAIT control is sent with each search. If referrals are
	 * disabled, the control is sent and you will receive the referring
	 * entry back.
	 *
	 * @param on true (the default) if referrals are to be followed
	 */
    public void setReferralsEnabled( boolean on ) {
		Debug.println(8, "VListEntryModel.setReferralsEnabled: " + on);
		_followReferrals = on;
	}

	/**
	 * Get the parameter which determines if the
	 * ManagedSAIT control is sent with each search.
	 *
	 * @returns true if referrals are to be followed
	 */
    public boolean getReferralsEnabled() {
		return _followReferrals;
	}

	Vector _entries = new Vector();
	private String _selectedPartitionView = DSContentModel.ALL;
    protected boolean _initialized = false;
    private int _top = 0;
    protected int _beforeCount;
	protected int _afterCount;
	private int _pageSize = 50;
	private int _selectedIndex = 0;
	protected LDAPControl[] _pageControls = null;
	protected LDAPVirtualListControl _vlc = null;
	private static LDAPControl _manageDSAITControl =
			new LDAPControl( LDAPControl.MANAGEDSAIT, true, null );
	private boolean _followReferrals = true;
	private boolean _mismatch = false;
	protected int _size = -1;
	private String _base;
	private int _scope;
	private String _filter;
	private String _host;
	private int _port;
	private String _authDN;
	private String _authPassword;
	protected boolean _debug = false;
	private LDAPConnection _ldc;
	private IDSModel _model = null;

	static final private boolean _verbose =
	             (System.getProperty("verbose") != null);
}

