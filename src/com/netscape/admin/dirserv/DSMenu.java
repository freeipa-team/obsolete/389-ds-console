/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

/**
 * DSMenu
 * Utility methods to populate a menu from a properties file.
 *
 * @version 1.0
 * @author rweltman
 **/
package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.client.util.*;

public class DSMenu {
    public DSMenu() {
    }

	public static void addMenuItem( String section, String name,
									JComponent menu, ActionListener parent,
									ResourceSet resource) {
		addMenuItem(section, name, menu, parent, resource, true);
	}

    public static void addMenuItem( String section, String name,
									JComponent menu, ActionListener parent,
									ResourceSet resource, boolean setMnemonic ) {
		String menuName = "menuItem" + name;
		int nItems = Integer.parseInt(resource.getString(section,
														 "N"+menuName));
		for (int i=0; i<nItems; i++) {
			String item = resource.getString(section, menuName+i);
			if ( item.equals("SEPARATOR") ) {
				if ( menu instanceof JMenu )
					((JMenu)menu).addSeparator();
				else
					((JPopupMenu)menu).addSeparator();
			} else {
				String command = resource.getString( section,
													 menuName+"Command"+i );
				String shortcut = resource.getString( section,
													  menuName+"Key"+i );
				String category = resource.getString( section,
													  menuName+"Category"+i );
				int key = 0;
				if ( (shortcut != null) && (shortcut.length() > 0) ) {
					shortcut = shortcut.toUpperCase();
					key = shortcut.charAt( 0 );
				}
				Debug.println(9, "DSMenu.addMenuItem: item " + i +
							  " command=" + command + " shortcut=" +
							  shortcut + " category=" + category + " key=" +
							  key);
				if ( (category != null) && (category.equals("POPUP")) ) {
					JMenu popup = new JMenu( item );
					if ( menu instanceof JMenu ) {
						((JMenu)menu).add( popup ); 
					} else {
						((JPopupMenu)menu).add( popup );
					}
					String token = resource.getString( section,
													  menuName+"Token"+i );
					addMenuItem( section, name+token, popup, parent,
								 resource, setMnemonic );
				} else {
					JMenuItem menuItem = createMenuItem( menu, item,
														 parent, command,
														 key, category, setMnemonic );
				}
			}
		}
	}

	public static JMenuItem createMenuItem( JComponent menu, String s,
											ActionListener listener,
											String command,
											int key, String category ) {
		return createMenuItem(menu, s, listener, command, key, category, true);
	}

	public static JMenuItem createMenuItem( JComponent menu, String s,
											ActionListener listener,
											String command,
											int key, String category, boolean setMnemonic ) {
		if ( s == null )
			Debug.println( "Missing menu label" );
		if ( command == null )
			Debug.println( "Missing menu command for " + s );
		JMenuItem menuItem = null;
		if ( menu instanceof DSRadioMenu ) {
			((DSRadioMenu)menu).add( s, category ); 
			menuItem =
				(JMenuItem)((DSRadioMenu)menu).getMenuComponent(
					((DSRadioMenu)menu).getMenuComponentCount()-1 );
		}
		else if ( menu instanceof JMenu ) {
			((JMenu)menu).add( s ); 
			menuItem =
				(JMenuItem)((JMenu)menu).getMenuComponent(
					((JMenu)menu).getMenuComponentCount()-1 );
		}
		else {
			menuItem = ((JPopupMenu)menu).add( new JMenuItem( s ) );
		}
		if ( menuItem == null ) {
			Debug.println( "Failed to create menu item for " + s );
		} else {
			if ( key != 0 ) {
				menuItem.setAccelerator(
					KeyStroke.getKeyStroke(key, Event.CTRL_MASK));

				if (setMnemonic) {
					menuItem.setMnemonic( key );
				}

				if (listener instanceof JComponent) {
					JComponent comp = (JComponent)listener;
					comp.registerKeyboardAction(listener, command,
						KeyStroke.getKeyStroke(key, Event.CTRL_MASK),
						comp.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
					Debug.println(9, "DSMenu.addMenuItem: added key listener" +
								  "=" + listener + " for key=" + key +
								  ":" +
								  KeyStroke.getKeyStroke(key, Event.CTRL_MASK).
								  toString() + " command=" + command);
				}
			}
			menuItem.setBackground(Color.lightGray);
			menuItem.addActionListener(listener);
			menuItem.setActionCommand(command);
		}
		return menuItem;
	}

    public static void main( String[] args ) {
    }
}
