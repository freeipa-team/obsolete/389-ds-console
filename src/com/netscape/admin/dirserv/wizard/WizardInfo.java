/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.wizard;

import java.util.Hashtable;

/**
 * Wizard Data Container
 */
public class WizardInfo {

    public WizardInfo() {
        _content = new Hashtable();
    }
    
    public void addEntry(String name, Object entry) {
        _content.put(USER_EXTENSION+name, entry);    
    }
    
    public Object getEntry(String name) {
        return _content.get(USER_EXTENSION+name);
    }
    
    private static final String USER_EXTENSION = "user-";
    
    protected Hashtable _content;
}



