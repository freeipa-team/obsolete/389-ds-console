/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.wizard;


import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.client.util.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.UIFactory;


/**
 * WizardWidget provides the most fundamental functionalities
 * of an wizard widget.
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/02/97
 * @see     com.netscape.admin.dirserv.wizard
 */
public class WizardWidget extends JDialog implements ActionListener 
{

    /*==========================================================
     * constructors
     *==========================================================*/
     
    /**
     * Construct Wizard with specified title and parent frame.
     * @param parent parent frame
     * @param title string to be displayed on the dialog box title bar
     * @param size specify wizard size
     */
    public WizardWidget(JFrame parent, String title, Dimension size) {
        this(parent,title);
        _size = size;
        setSize(size.width, size.height);
    }
    
    /**
     * Construct Wizard with specified title and parent frame.
     * @param parent parent frame
     * @title string to be displayed on the dialog box title bar
     */
    public WizardWidget(JFrame parent, String title) {       
        super(parent, title, true);
        _title = title;
        _size = DEFAULT_SIZE;
        _parent = parent;
        getContentPane().setLayout(new BorderLayout());
        setSize(_size.width, _size.height);
        getRootPane().setDoubleBuffered(true);
//      setBackground(SystemColor.control);
        setLocationRelativeTo(parent);
        _info = new WizardInfo();

        _nextLabel = _resource.getString("wizard", "nextButton");
        _doneLabel = _resource.getString("wizard", "doneButton");

        //create display panel
        _display = new JPanel();
        _display.setLayout(new BorderLayout());
        _display.setBorder(BorderFactory.createEtchedBorder());
        getContentPane().add("Center",_display);

        _bHelp = new JButton();
        _bHelp.setText(_resource.getString("wizard", "helpButton"));
        _bHelp.addActionListener(this);

        _bBack = new JButton();
        _bBack.setText(_resource.getString("wizard", "backButton"));
        _bBack.addActionListener(this);
        _bBack.setEnabled(false);

        
        _bNext_Done = new JButton();
        _bNext_Done.setText(_nextLabel);
        _bNext_Done.addActionListener(this);
//      _bNext_Done.setBackground(SystemColor.control);
        
        _bCancel = new JButton();
        _bCancel.setText(_resource.getString("wizard", "cancelButton"));
        _bCancel.addActionListener(this);
//      _bCancel.setBackground(SystemColor.control);

		JButton[] buttons = { _bBack, _bNext_Done, _bCancel, _bHelp };
        JPanel buttonPanel = UIFactory.makeJButtonPanel( buttons, true );

        //create button panel
        JPanel south = new JPanel();
        south.setLayout(new BorderLayout());
        south.add("North", Box.createVerticalStrut(STRUT_SIZE));
        south.add("Center",buttonPanel);
        south.add("South", Box.createVerticalStrut(STRUT_SIZE));
        south.add("East", Box.createHorizontalStrut(STRUT_SIZE));
        getContentPane().add("South",south);
        getContentPane().add("East", Box.createHorizontalStrut(STRUT_SIZE));
        getContentPane().add("West", Box.createHorizontalStrut(STRUT_SIZE));
        getContentPane().add("North", Box.createVerticalStrut(STRUT_SIZE));
    }

	/*==========================================================
	 * public methods
     *==========================================================*/
     
    /**
     * get parent frame
     * @return parent frame
     */
    public JFrame getFrame() {
        return _parent;
    }

    /**
     * Add a IWizardPanel into wizard.  Note the sequence you add
     * will the be the sequence it will appear.
     * @param page IWizardPanel to be displayed
     */
    public void addPage(JPanel page) {
        if (_current == null) {
            _current = page;
            _display.add("Center",page);
            initializeWizardPanel();			
        } else {
            _next.insertElementAt(page, 0);			
        }		
    }

    /**
     * Returns wizard data container
     */
    protected WizardInfo getWizardInfo() {
        return _info;
    }
    
    /**
     * set wizard data container
     */
    protected void setWizardInfo(WizardInfo info) {
        _info = info;
    }
    
    /**
     * Initialize currently displayed panel
     * Implemetation is delegated to initialize() method
     * of IWizardPanel. 
     * If error occurred, wizard will be terminated.
     */
    protected boolean initializeWizardPanel() {

        if (_current instanceof IWizardPanel) {
            boolean status = ((IWizardPanel)_current).initializePanel(_info);
			setTitle( ((IWizardPanel)_current).getTitle() );	
			return status;
        }
        return true;
    }

    /**
     * Verify if a page is complete. It means all the
     * require fields are fill out. It delegates implementation
     * details to validate() method of the IWizardPanel obejct.
     * If failed, error dialog is displayed but not terminated.
     */
    boolean validateWizardPanel() {
        boolean complete = true;

        if (_current instanceof IWizardPanel) {
            if (!( (IWizardPanel)_current ).validatePanel()) {
				String msg = ((IWizardPanel)_current).getErrorMessage();
				DSUtil.showErrorDialog( _parent, "error", msg,
										"wizard" );
                Debug.println(((IWizardPanel)_current).getErrorMessage());
                complete = false;
            }
        }

        return complete;
    }

    /**
     * Some panel may require post-processing before moving to next stage.
     * Ususally the last IWizardPanel use this method to perform
     * save/update operation on the server via cgi/rmi/ldap.
     * If error occurred, wizard will be terminated.
     */
    boolean concludeWizardPanel() {
        boolean complete = true;
        if(_current instanceof IWizardPanel) {
            if (!((IWizardPanel)_current).concludePanel(_info)) {
                complete = false;
            }
        }
        return complete;
    }

    /**
     * Retrieve the update information from the
     * IWizardPanel into WizardInfo.
     */
    void updateWizardInfo() {
        if(_current instanceof IWizardPanel) {
            ((IWizardPanel)_current).getUpdateInfo(_info);
        }
    }
    
    protected void callHelp() {
        Debug.println("Overwrite this method");    
    }

    /**
     * Action Performed when button pressed. ActionListener implementation.
     * @param event
     */
    public void actionPerformed(ActionEvent e) {

        //DONE or NEXT Pressed
        if (e.getSource().equals(_bNext_Done)) {

            if (!validateWizardPanel()) {
                return;
            }

            if (concludeWizardPanel())  {

                if (_next.empty()) {
                    this.dispose();
					DSUtil.dialogCleanup();
                    return;
                } else {
                    updateWizardInfo();
                    _prev.push(_current);
                    _display.remove(_current);
					setCurrentPanel((JPanel)(_next.pop()));
                    while (!initializeWizardPanel()) {
                        //move to next
                        if (_next.empty()) {
                            this.dispose();
							DSUtil.dialogCleanup();
                            return;
                        }
                        _prev.push(_current);
						setCurrentPanel((JPanel)(_next.pop()));
                    }
					_display.add("Center",_current);
                    _display.invalidate();
                    _display.validate();
                    _display.repaint(1);
                    getRootPane().paintImmediately(
						getRootPane().getVisibleRect() );
                }

            } else {
                return;
            }
        }

        //Cancel Pressed
        if (e.getSource().equals(_bCancel)) {
            //prompt for confirm
			String msg = _resource.getString("wizard","confirmMessage");
			int option = DSUtil.showConfirmationDialog(
				                             this,
											 "question",
											 msg,
											 "wizard");
            if (option == JOptionPane.YES_OPTION) {
                this.dispose();
				DSUtil.dialogCleanup();
				_isCancelled = true;
			}
        }

        //Back Pressed
        if (e.getSource().equals(_bBack)) {
            //move back to previous page
            if (!(_prev.empty())) {
                Debug.println("Back Called");
                _next.push(_current);
                _display.remove(_current);
				setCurrentPanel((JPanel)(_prev.pop()));
                while (!initializeWizardPanel()) {
                    //move to prev
                    if (_prev.empty()) {
                        return;
                    }
                    _next.push(_current);
                    setCurrentPanel((JPanel)(_prev.pop()));
                }
                _display.add("Center",_current);
                _display.invalidate();
                _display.validate();
                _display.repaint(1);
				getRootPane().paintImmediately(
					getRootPane().getVisibleRect() );
            }
        }
        
        //Help Pressed
        if (e.getSource().equals(_bHelp)) {
            callHelp();    
        }
     		
        changeButton();
    }

	/**
	 * Set the currently active panel
	 *
	 * @param current Current panel
	 */
	protected void setCurrentPanel( JPanel panel ) {
		Debug.println( "WizardWidget.setCurrentPanel: " + panel );
		_current = panel;
	}

    /**
     * Button enable/disable and label changes
     */
    private void changeButton() {
        
        if (_prev.size()==0) {
            _bBack.setEnabled(false);
            _bBack.repaint();
        } else {
            _bBack.setEnabled(true);
            _bBack.repaint();
        }

        if (_next.size()==0) {
            _bNext_Done.setText(_doneLabel);
        } else {
            _bNext_Done.setText(_nextLabel);
        }
        _bNext_Done.repaint();
    }


	protected JButton getbNext_Done() {
		return _bNext_Done;
	}

	public boolean isCancelled() {
		return _isCancelled;
	}
	
    //variables
    static final Dimension DEFAULT_SIZE = new Dimension(460,500);
    static final int STRUT_SIZE = UIFactory.getDifferentSpace();
    
	protected boolean _isCancelled = false;

    //private variables
    private JButton _bNext_Done, _bCancel, _bBack, _bHelp;
    private Stack _prev = new Stack();
    private Stack _next = new Stack();
    protected JPanel _current = null;
    private JPanel _display;
    private String _doneLabel, _nextLabel;
    private WizardInfo _info;
    private JFrame  _parent;
    private String _title;
    private Dimension _size;
    
	//get resource bundle
    private static ResourceSet _resource =
	    new ResourceSet("com.netscape.admin.dirserv.wizard.wizard");
}


