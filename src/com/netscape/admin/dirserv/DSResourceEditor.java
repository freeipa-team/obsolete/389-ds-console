/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.Vector;
import java.util.Observable;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import netscape.ldap.LDAPEntry;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.ug.*;
import com.netscape.admin.dirserv.account.*;
import com.netscape.admin.dirserv.roledit.ResEditorRoleAccountPage;
import com.netscape.admin.dirserv.account.ResEditorUserTitlePage;
import com.netscape.admin.dirserv.roledit.ResEditorRoleTitlePage;
import com.netscape.admin.dirserv.cosedit.ResEditorCosTitlePage;


/**
 * DSResourceEditor extends the base ResourceEditor in three ways:
 *
 *	- when the user clicks on the Cancel button: DSResourceEditor
 *    catches this click and notifies the pages which implements
 *    IDSResourceEditorPage (by calling the cancel() method).
 *
 *	- it overrides the default title panel and title string
 *    according the object class of the observable.
 *
 *	- it creates and maintains an activation model: this model
 *    is an extra observable shared between the title panel
 *    and the account page.
 */
public class DSResourceEditor extends ResourceEditor {

	/**
	 * Base constructor + a call to afterSetupPlugin()
	 */
    public DSResourceEditor(JFrame parent, ConsoleInfo info,
                          Vector objectClassList, String sCreatedLocDN) {
        super(parent, info, objectClassList, sCreatedLocDN);
		initActivationModel();
		afterSetupPlugin();
	}
	
	/**
	 * Base constructor + a call to afterSetupPlugin()
	 */
    public DSResourceEditor(JFrame parent, ConsoleInfo info,
                          LDAPEntry entry) {
        super(parent, info, entry);
		initActivationModel();
		afterSetupPlugin();
	}
	
    /**
     * Overrides actionPerformed() to catch the click on Cancel.
	 * This method finds the IDSResourceEditorPage instances and
	 * invokes their cancel() method.
     *
     * @param e  the action event
     */
    public void actionPerformed(ActionEvent e) {

		// Invoke the cancel() method of each IDSResourceEditorPage
        if (e.getActionCommand().equals("Cancel")) {
			int pageCount = getPageCount();
			ResourcePageObservable o = getLDAPObservable();
            for (int i = 1; i <= pageCount; i++) {
				Object page = getPage(i);
				if (page instanceof IDSResourceEditorPage) {
					try {
						((IDSResourceEditorPage)page).cancel(o);
					}
					catch(Exception x) {
						Debug.println(0, "DSResourceEditor.actionPerformed: caught exception " + e);
						if (Debug.getTrace()) {
							x.printStackTrace();
						}
					}
				}
			}
		}
		
		// Now call the normal processing
		super.actionPerformed(e);
	}


    /**
     * Implements the Observer interface
     */
    public void update(Observable o, Object arg) {
		super.update(o, arg);
		
        if (o instanceof ResourcePageObservable) {
            String sAttrName = (String) arg;
            if ((sAttrName != null) &&
                (sAttrName.toLowerCase().equals("objectclass"))) {
				afterSetupPlugin();
            }
        }
    }
    
	/**
	 * Creates the activation model.
	 */

	void initActivationModel() {
		ResourcePageObservable observable = getLDAPObservable();
		Vector objectClass = observable.get("objectclass");
		
		// Only two kinds of entries need an activation model:
		// user and role.
		if ((DSUtil.indexOfIgnoreCase(objectClass, "person") != -1)) {
			_activationModel = new UserActivationModel();
		}
		else if (DSUtil.indexOfIgnoreCase(objectClass, "nsroledefinition") != -1) {
			_activationModel = new RoleActivationModel();
		}
		if (_activationModel != null)
			_activationModel.initialize(observable,this);
	}
	
	
	/**
	 * This method encapsulates all the logic which must
	 * be executed after ResourceEditor.setupPlugin().
	 *
	 * It contains the hard-coded logic which:
	 *    - assign a title and a title panel according the
	 *	    object class of the observable
	 *	  - link the title panel and the account pages 
	 *      using the activation model.
	 */
	 
	void afterSetupPlugin() {
		ResourcePageObservable observable = getLDAPObservable();
		Vector objectClass = observable.get("objectclass");
		JPanel titlePanel = null;
		String titleNew = null;
		
		// Select a title string and a title panel
		// according the object class of the observable.
		if ((DSUtil.indexOfIgnoreCase(objectClass, "person") != -1)) {
			_activationModel.deleteObservers();
			ResEditorUserAccountPage p = findUserAccountPage();
			p.initialize(_activationModel, this);
			titlePanel = new ResEditorUserTitlePage(observable, _activationModel);
			titleNew = DSUtil._resource.getString( "UserGroup", "person-title" );
		}
		else if (DSUtil.indexOfIgnoreCase(objectClass, "groupofuniquenames") != -1) {
			// The default title panel is ok
			titleNew = DSUtil._resource.getString( "UserGroup", "group-title" );
		}
		else if (DSUtil.indexOfIgnoreCase(objectClass, "organizationalunit") != -1) {
			// The default title panel is ok
			titleNew = DSUtil._resource.getString( "UserGroup", "organization-title" );
		}
		else if (DSUtil.indexOfIgnoreCase(objectClass, "nsroledefinition") != -1) {
			_activationModel.deleteObservers();
			ResEditorRoleAccountPage p = findRoleAccountPage();
			p.initialize(_activationModel, this);
			titlePanel = new ResEditorRoleTitlePage(observable, _activationModel);
			titleNew = DSUtil._resource.getString( "roleInfoPage", "title" );
		}
		else if (DSUtil.indexOfIgnoreCase(objectClass, "cossuperdefinition") != -1) {
			titlePanel = new ResEditorCosTitlePage(observable); 
			titleNew = DSUtil._resource.getString( "cosInfoPage", "title" );
		}
		
		
		// Apply the title string and panel		
		if (observable.isNewUser() && (titleNew != null)) 
			setTitle(titleNew);		
		if (titlePanel != null)
			setTitlePanel(titlePanel);

		Debug.println("DSResourceEditor.afterSetupPlugin: title updated");			
	}

	/**
	 * Search the pages for a ResEditorUserAccountPage instance.
	 * If no instance is found, then we add one automatically.
	 * This is a temporary workaround: at the moment, we
	 * don't have any other solution to add a page to an
	 * nsAdminResourceEditorExtension owned by the admin server.
	 */
	ResEditorUserAccountPage findUserAccountPage() {
		ResEditorUserAccountPage result = null;
		int pageCount = getPageCount();
		int i = 1;
		while ((i <= pageCount) && (result == null)) {
			if (getPage(i) instanceof ResEditorUserAccountPage) {
				result = (ResEditorUserAccountPage)getPage(i);
			}
			i++;
		}
		if (result == null) {
			if (_userAccountPage == null) {
				_userAccountPage = new ResEditorUserAccountPage();
			}
			result = _userAccountPage;
			if (addPage(result) ) {
				Debug.println("DSResourceEditor.findUserAccountPage: added user account page !");
			}
			else {
				Debug.println(0, "DSResourceEditor.findUserAccountPage: addPage returned false !");
			}
		}
		
		return result;
	}
	
	
	/**
	 * Search the pages for a findRoleAccountPage instance.
	 * Return null if not found.
	 */
	ResEditorRoleAccountPage findRoleAccountPage() {
		ResEditorRoleAccountPage result = null;
		int pageCount = getPageCount();
		int i = 1;
		while ((i <= pageCount) && (result == null)) {
			if (getPage(i) instanceof ResEditorRoleAccountPage) {
				result = (ResEditorRoleAccountPage)getPage(i);
			}
			i++;
		}
		if (result == null) {
			Debug.println(0, "DSResourceEditor.findRoleAccountPage: no page found !");
		}
		
		return result;
	}

	// Activation model
	// The activation model can be seen as an extra
	// ResourcePageObservable shared between the title panel
	// and the Account page when this page is present.
	ActivationModel _activationModel;
	
	// ResourceEditor caches its ResourceEditorPage
	// in _oldClassHashtable. We do the same here for
	// the user account page (which is added on the fly).
	ResEditorUserAccountPage _userAccountPage;
	
};
