/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv;

import java.util.Hashtable;
import javax.swing.*;
import java.awt.Event;
import java.awt.event.*;
import com.netscape.management.client.*;

/**
  * This page displays a tree view on the left hand side and a detail 
  * panel on the right.  The data for the tree comes from IResourceModel.
  * The detail panel is a Component returned by IResourceModel::getCustomPanel().
  *
  * @version b2 11/17/97
  * @author ahakim@netscape.com
  * @author terencek@netscape.com
  * @see IPage
  * @see IResourceModel
  * @see IResourceModelListener
  * @see IFrameworkListener
  * @see TreeSelectionListener
  */
public class DSResourcePage extends ResourcePage
                            implements Cloneable,
	                                   FocusListener {
    
    /**
     * Return ResourcePage using the data model specified.
     */
	public DSResourcePage(IResourceModel resourceModel) {
		super(resourceModel);
		getTree().addFocusListener( this );
		createShortCutKeys();
	}
	
    /**
      * Called internally when page is selected
      */
	public void pageSelected(IFramework framework) {
		super.pageSelected( framework );
		((IDSModel)getModel()).pageSelected( framework, this );
	}

	/**
	 * Need to overwrite this one to clone
	 * DSResourcePage instead of ResourcePage
	 * @return copy of resource page
	 */
    public Object clone() {
		ResourcePage rp = new DSResourcePage(getModel());
		rp.setCellRenderer( _treeRenderer );
		rp.setPageTitle(getPageTitle());
		return rp;
	}
	
	/**
     * Retrieve JTree Object
     * @return JTree object in the resource page
     */
    public JTree getTree() {
        return (JTree)_tree;
    }

	public void focusGained( FocusEvent e ) {
		((IDSModel)getModel()).focusGained( e );
	}

	public void focusLost( FocusEvent e ) {
	}

	protected  void addMenuItem(java.lang.String categoryID, IMenuItem menuItem) {		
		super.addMenuItem(categoryID, menuItem);
		addShortCut(menuItem);		
	}


	protected void addMenuItems(IMenuInfo menuInfo,
            ActionListener menuActionListener) {	
		super.addMenuItems(menuInfo, menuActionListener);
		addShortCut(menuInfo);				
	}

	public void addMenuItems(ResourceModelEvent e) {		
		super.addMenuItems(e);
		IMenuInfo menuInfo = e.getMenuInfo();
		addShortCut(menuInfo);			
	}

	protected void addShortCut(IMenuItem menuItem) {
		if (menuItem instanceof MenuItemText) {
			MenuItemText menuItemText = ((MenuItemText) menuItem);
			String id = menuItemText.getID();			
			if ((menuItemText.getAccelerator() == null) &&
				(id != null)) {
				KeyStroke stroke = (KeyStroke)_shortCutTable.get(id);
				if (stroke != null) {
					menuItemText.setAccelerator(stroke);
					
					MenuActionListener listener = new MenuActionListener(this, menuItemText);
					
					getTree().registerKeyboardAction(listener,												 
													 menuItemText.getID(),												 												 
													 menuItemText.getAccelerator(),
													 JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
				}
				menuItemText.setActionCommand(menuItemText.getID());
			}
		}
	}

	protected void addShortCut(IMenuInfo menuInfo) {
		String[] ids = menuInfo.getMenuCategoryIDs();
		if (ids != null) {
			for (int i=0; i<ids.length; i++) {
				IMenuItem[] items = menuInfo.getMenuItems(ids[i]);
				if (items != null) {
					for (int j=0; j < items.length; j++) {
						addShortCut(items[j]);
					}
				}
			}
		}
	}

	protected void createShortCutKeys() {
		_shortCutTable = new Hashtable();
		_shortCutTable.put(DSResourceModel.REFRESH, KeyStroke.getKeyStroke(KeyEvent.VK_R, Event.CTRL_MASK));		
	}

	protected Hashtable _shortCutTable;
}


class  MenuActionListener implements ActionListener {
	public MenuActionListener(ResourcePage page, MenuItemText menuItem) {
		_menuItem = menuItem;
		_page = page;
	}
	
	public void actionPerformed(ActionEvent e) {
		/* We send the action to the selected node if it is a IMenuInfo, if not we send it to the model */
		IResourceObject[] selection = _page.getSelection();
		if (selection != null) {
			if (selection[0] instanceof IMenuInfo) {
				((IMenuInfo)selection[0]).actionMenuSelected(_page, _menuItem);
				return;
			}
		}
		((IDSModel)_page.getModel()).actionMenuSelected(_page, _menuItem);						   
	}
	
	MenuItemText _menuItem;
	ResourcePage _page;
}
