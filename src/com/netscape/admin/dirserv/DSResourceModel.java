/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.Component;
import java.awt.event.*;
import java.util.Enumeration;
import java.util.Vector;
import java.util.Hashtable;
import com.netscape.management.client.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.admin.dirserv.node.*;
import com.netscape.admin.dirserv.panel.BlankPanel;
import com.netscape.admin.dirserv.panel.LDAPAddPanel;
import com.netscape.admin.dirserv.panel.DatabaseImportPanel;
import com.netscape.admin.dirserv.panel.DatabaseExportPanel;
import com.netscape.admin.dirserv.panel.ConfirmationPreferencesPanel;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import netscape.ldap.*;

/**
 *	Netscape Directory Server 4.0 resource model.
 *
 * @author  rweltman
 * @version %I%, %G%
 */

public class DSResourceModel extends DSBaseModel
    implements IDSContentListener {

    /**
     * Constructor - create all panels.
     *
     * @param info	Global console connection information
     * @param serverInfo Server instance connection information
     */
    public DSResourceModel( ConsoleInfo info,
			    ConsoleInfo serverInfo ) {
	super( info, serverInfo );

	initialize();

	ResourceObject root = new RootResourceObject( this );

	setRoot( root );
	setSelectedNode( root );
    }

    /**
     * Any creation-time initialization.
     */
    private void initialize() {
	getServerInfo().put( "dsresmodel", this );
    }

    /**
     * Returns supported menu categories
     */
    public String[] getMenuCategoryIDs() {
		if (_categoryID == null) {
			_categoryID = new String[] {
				Framework.MENU_FILE,
					Framework.MENU_EDIT,
					Framework.MENU_VIEW
					};
		}
		return _categoryID; 
	}

    /**
     * add menu items for this page.
     */
    public IMenuItem[] getMenuItems(String category) {
	if(category.equals(Framework.MENU_FILE)) {
		if (_menuFile == null) {
			_menuFile = new IMenuItem[] {
				new MenuItemText( AUTHENTICATE,
								  _resource.getString("menu", "authenticate"),
								  _resource.getString("menu",
													  "authenticate-description")),				
					new MenuItemSeparator(),
					new MenuItemText( USE_LDIF_FILE,
									  _resource.getString("menu", "useldiffile"),
									  _resource.getString("menu",
														  "useldiffile-description")),					
					 new MenuItemText( IMPORT,
					 _resource.getString("menu", "import"),
					 _resource.getString("menu",
					 "import-description")),					 
					new MenuItemText( EXPORT,
									  _resource.getString("menu", "export"),
									  _resource.getString("menu",
														  "export-description")),
					new MenuItemSeparator()
					};
		}
		return _menuFile;
	} else if(category.equals(ResourcePage.MENU_CONTEXT)) {
		if (_menuContext == null) {
			/* Resource tree nodes don't currently have entries in the
			   Directory, so they don't have access control */
			_menuContext = new IMenuItem[] {
				new MenuItemText( ACL,
								  _resource.getString("menu", "editacls"),
								  _resource.getString("menu",
													  "editacls-description")),
					};
		}
		return _menuContext;
	}  else if(category.equals(Framework.MENU_EDIT)) {
		if (_menuEdit == null) {
			_menuEdit = new IMenuItem[] {
				/*
				  new MenuItemText( MOVE,
				  _resource.getString("menu", "EditMove"),
				  _resource.getString("menu",
				  "EditMove-description")),
				  new MenuItemText( COPY,
				  _resource.getString("menu", "EditCopy"),
				  _resource.getString("menu",
				  "EditCopy-description")),
				  new MenuItemText( PASTE,
				  _resource.getString("menu", "EditPaste"),
				  _resource.getString("menu",
				  "EditPaste-description")),
				  new MenuItemText( DELETE,
				  _resource.getString("menu", "EditDelete"),
				  _resource.getString("menu",
				  "EditDelete-description"))
				  */
				new MenuItemText( CONFIRMATION,
								  _resource.getString("menu", "confirmation"),
								  _resource.getString("menu",
													  "confirmation-description"))
					};
		}
		return _menuEdit;
	} else if(category.equals(Framework.MENU_VIEW)) {
		if (_menuView == null) {
			_menuView = new IMenuItem[] {
				new MenuItemSeparator(),
					new MenuItemText( REFRESH_ALL,
									  _resource.getString("menu", "refresh-all"),
									  _resource.getString("menu",
														  "refresh-all-description"))
					};
		}
		return _menuView;
	}
	return null;
    }

    public void refreshView() {
		refreshObject();
    }

    /**
     * Notification that a menu item has been selected.
     */
    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
	super.actionMenuSelected(viewInstance, item);
	setWaitCursor(true);
	if (item.getID().equals(OPEN)) {
	    actionObjectRun(viewInstance, _selection);

	} else if(item.getID().equals(AUTHENTICATE)) {
	    boolean result = getNewAuthentication();

	} else if(item.getID().equals(ACL)) {
	    /* ??? There should be a DN for each entry */
	    /* This will not get called, because we no longer register
	       a menu item for it. But if we sometime in the future
	       created a directory entry for each one, we could reeenable
	       it. */
	    /*
	      String dn = getConsoleInfo().getCurrentDN();
	      getConsoleInfo().setAclDN( dn );
	      getConsoleInfo().setUserGroupDN("");
	      DSACLEditor ed = new DSACLEditor(getConsoleInfo());
	      ed.show();
	    */
	} else if(item.getID().equals(USE_LDIF_FILE)) {
	    useLDIFFile();	
	}   else if(item.getID().equals(IMPORT)) {
	    databaseImport( );	
	}else if(item.getID().equals(EXPORT)) {
	    databaseExport();
	} else if(item.getID().equals(REFRESH)) {
	    refreshObject();
	} else if(item.getID().equals(REFRESH_ALL)) {
		setSelectedNode((IResourceObject)getRoot());		
	    refreshObject();
	} else if(item.getID().equals(CONFIRMATION)) {
	    confirmationPreferences();
	} 
	setWaitCursor(false);
    }

    public void actionObjectSelected( IPage viewInstance,
				      IResourceObject[] selection,
				      IResourceObject[] previousSelection) {
	super.actionObjectSelected(viewInstance, selection, previousSelection);
	_selection = selection;
	if ( _selection == null )
	    _selection = new IResourceObject[0];
	Vector selected = new Vector();
	Vector toNotify = new Vector();
	/* Signal all selected objects, keep track of which ones */
	for( int i = 0; i < _selection.length; i++ ) {
	    IResourceObject sel = _selection[i];
	    Component c = sel.getCustomPanel(); 
	    if ( (c != null) && _selectionListeners.contains( c ) ) {
		toNotify.addElement( sel );
	    }
	    selected.addElement( c );
	}

	/* All other listeners must be unselected */
	if ( previousSelection != null ) {
	    for( int i = 0; i < previousSelection.length; i++ ) {
		IResourceObject sel = previousSelection[i];
		Component c = sel.getCustomPanel();
		if ( (c != null) &&
		     _selectionListeners.contains( c ) &&
		     !selected.contains( c ) ) {
		    try {
			IDSResourceSelectionListener l = 
			    (IDSResourceSelectionListener)c;
			l.unselect( sel, viewInstance );
		    } catch ( Exception e ) {
			Debug.println(
				      "DSResourceModel.actionObjectSelected: " +
				      "c = " + c + ", " + e.toString() );
		    }
		}
	    }
    	}

	for( int i = 0; i < toNotify.size(); i++ ) {
	    IResourceObject sel =
		(IResourceObject)toNotify.elementAt( i );
	    Component c = sel.getCustomPanel(); 
	    IDSResourceSelectionListener l =
		(IDSResourceSelectionListener)c;
	    l.select( sel, viewInstance );
	}
    }

    /**
     * Notification that the framework window is closing.
     * Called by ResourcePage
     */
    public void actionViewClosing(IPage viewInstance)
	throws CloseVetoException {
	/* Check if any clients have unsaved changes */
	Enumeration e = _changeClients.elements();
	while( e.hasMoreElements() ) {
	    if ( ((IChangeClient)e.nextElement()).isDirty() ) {
				/* Yes, notify the framework */
		throw new CloseVetoException();
	    }
	}
    }



    private void useLDIFFile( ) {		
	LDAPAddPanel child = new LDAPAddPanel( this );
	SimpleDialog dlg = new SimpleDialog( getFrame(),
					     child.getTitle(),
					     SimpleDialog.OK |
					     SimpleDialog.CANCEL |
					     SimpleDialog.HELP,
					     child );
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	dlg.setDefaultButton( SimpleDialog.OK );
	dlg.packAndShow();
    }

    private void databaseImport( ) {		
	DatabaseImportPanel child = new DatabaseImportPanel( this );	
	SimpleDialog dlg = new SimpleDialog( getFrame(),
					     child.getTitle(),
					     SimpleDialog.OK |
					     SimpleDialog.CANCEL |
					     SimpleDialog.HELP,
					     child );
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	dlg.setDefaultButton( SimpleDialog.OK );
	dlg.packAndShow();	    
    }
	       

    private void databaseExport() {
	/* Do it as a dialog */
        SubtreeSelectionDialog dialog =
	    new SubtreeSelectionDialog( getFrame(),
					getServerInfo(), true, false );
		
	DatabaseExportPanel child =
	    new DatabaseExportPanel( this, dialog, true);
	SimpleDialog dlg = new SimpleDialog( getFrame(),
					     child.getTitle(),
					     SimpleDialog.OK |
					     SimpleDialog.CANCEL |
					     SimpleDialog.HELP,
					     child );
	dlg.setComponent( child );
	dlg.setOKButtonEnabled( false );
	dlg.setDefaultButton( SimpleDialog.OK );

	dlg.packAndShow();
    }

    private void confirmationPreferences() {
	ConfirmationPreferencesPanel child =
	    new ConfirmationPreferencesPanel( new DefaultResourceModel() );
	SimpleDialog dlg = new SimpleDialog( getFrame(),
					     child.getTitle(),
					     SimpleDialog.OK |
					     SimpleDialog.CANCEL |
					     SimpleDialog.HELP,
					     child );
	dlg.setComponent( child );
	dlg.setDefaultButton( SimpleDialog.OK );
	dlg.getAccessibleContext().setAccessibleDescription(_resource.getString("confirmation",
																			"description"));
	dlg.packAndShow();
    }

    private void refreshObject() {
	if ( (_selection != null) && (_selection.length > 0) ) {
	    IResourceObject sel = _selection[0];
		if (sel.equals(getRoot())) {
			setSchema( null ); /* Refresh schema if we are refreshing the root */
		}
	    Component panel = sel.getCustomPanel();
	    if ( sel instanceof ActionListener )
		((ActionListener)sel).actionPerformed(
						      new ActionEvent( this,
								       ActionEvent.ACTION_PERFORMED,
								       REFRESH ) );
	    if ( (panel != null) && (panel instanceof ActionListener) )
		((ActionListener)panel).actionPerformed(
							new ActionEvent( this,
									 ActionEvent.ACTION_PERFORMED,
									 REFRESH ) );	
	}
    }

    /**
     * Notifies that directory contents have changed
     */
    public void contentChanged() {
	Enumeration e = _contentListeners.elements();
	while( e.hasMoreElements() ) {
	    ((IDSContentListener)e.nextElement()).contentChanged();
	}
    }

    public void addContentListener( IDSContentListener listener ) {
	if ( !_contentListeners.contains( listener ) ) {
	    _contentListeners.addElement( listener );
	}
    }

    public void removeContentListener( IDSContentListener listener ) {
	_contentListeners.removeElement( listener );
    }
	
    private String[] _categoryID = null;
	private IMenuItem[] _menuFile = null;
	private IMenuItem[] _menuContext = null;
	private IMenuItem[] _menuView = null;
	private IMenuItem[] _menuEdit = null;
    private Vector _contentListeners = new Vector();
    static final String USE_LDIF_FILE = "useldiffile";							 
    static final String IMPORT = "import";
    static final String EXPORT = "export";
    static public final String REFRESH = "refresh";
	static public final String REFRESH_ALL = "refresh-all";
    static final String CONFIRMATION = "confirmation";
}
