/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.Hashtable;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import netscape.ldap.LDAPAttribute;

/**
 * AttributeAlias translates LDAP attribute names to friendly names
 *
 * @author  rweltman
 * @version %I%, %G%
 */
public class AttributeAlias {
    /**
	 * Look up friendly name for LDAP attribute.
	 *
	 * @param attrName The LDAP attribute name
	 * @return Friendly name, if any
	 */
    public static String getAlias( String attrName ) {
		if ( _aliases == null ) {
			_aliases = new ResourceSet( ALIAS_FILE );
		}
		/* For most attributes, there is a direct lookup */
		String label = _aliases.getString("attribute", attrName );
		if ( (label == null) || (label.length() < 1) ) {
			/* Check if this is a subtyped attribute. Show any
			   subtypes within parentheses. ??? Will this work
			   in all character sets? */
			String name = LDAPAttribute.getBaseName( attrName );
			String s = _aliases.getString("attribute", name );
			if ( s != null ) {
				label = s;
				String[] types = LDAPAttribute.getSubtypes(attrName);
				if ( types != null ) {
					label += " (";
					for( int j = 0; j < types.length; j++ ) {
						String t = types[j];
						String alias;
						if ( (t.length() >= 5) &&
							 t.substring(0,5).equals( "lang-") ) {
							String lang = t.substring(5,t.length());
							alias = _langResource.getString(
								"userPage", lang );
							if ( alias == null )
								alias = lang;
						} else {
							alias = _langResource.getString(
								"userPage", t );
						}
						if ( alias == null )
							alias = t;
						label += alias;
						if ( j < (types.length-1) )
							label += ',';
					}
					label += ")";
				} /* If there are subtypes */
			}
			if ( (label == null) || (label.length() < 1) ) {
				label = attrName;
			}
		}
		if ( _verbose ) {
			Debug.println( "AttributeAlias.getAlias: using <" +
						   label + "> for " + attrName );
		}
		return label;
	}

    /**
	 * Look up friendly names for LDAP attributes. Both arrays are
	 * sorted by friendly name.
	 *
	 * @param names The LDAP attribute names
	 * @param labels Equally sized array to receive sorted friendly names
	 */
    public static void getAliases( String[] names, String[] labels ) {
		Hashtable h = new Hashtable();
		for( int i = 0; i < names.length; i++ ) {
			labels[i] = getAlias( names[i] );
			h.put( labels[i], names[i] );
		}
		/* Sort the attributes alphabetically by friendly name */
		DSUtil.bubbleSort( labels );
		for( int i = 0; i < names.length; i++ ) {
			names[i] = (String)h.get( labels[i] );
		}
		h.clear();
		h = null;
	}

	static final private boolean _verbose = false;
    private static ResourceSet _aliases = null; // Attribute and objectclasses
	private static final String ALIAS_FILE =
	                                  "com.netscape.admin.dirserv.alias";
	static ResourceSet _langResource = DSUtil._langResource;
}
