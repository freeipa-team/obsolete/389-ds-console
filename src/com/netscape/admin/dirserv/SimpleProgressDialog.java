/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.panel.UIFactory;

/**
 * SimpleProgressDialog
 * Simple cancellable progress dialog - it does not actually have progress indicator
 * but it has a Cancel button to interrupt a process. It is inteneded to be used with
 * blocking events that can be interrupted like ORC. To use this class, you need to
 * create a companion class that implements StopRunnable interface. The run function of
 * the class is called to start the task and stop is called to interrup the task. 
 * For convinience, SimpleProgressTask class is defined as the base class for tasks.
 *
 * @version 1.0
 * @author olga
 **/

 public class SimpleProgressDialog extends JDialog
                                        implements ActionListener {
	public SimpleProgressDialog(JFrame parent, String title) {
		this(parent, null, title, null);
	}

	public SimpleProgressDialog(JFrame parent, Stoppable task, String title, String msg) {
		super(parent, true);
		setTitle(title);

		setContentPane(new JPanel());
		getContentPane().setLayout(new GridBagLayout());
//		getContentPane().setBackground(SystemColor.control);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridheight = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = gbc.REMAINDER;
		gbc.insets = new Insets(0,PAD,3,8);
		gbc.anchor     = gbc.CENTER;
		gbc.weightx = 1;
		gbc.fill       = gbc.HORIZONTAL;

		JLabel labelMsg = new JLabel(msg);
		labelMsg.setHorizontalAlignment( SwingConstants.CENTER );
		labelMsg.setOpaque( true );
		getContentPane().add(labelMsg, gbc);
		gbc.gridy++;
        
		JButton _bCancel = UIFactory.makeJButton(this, "general", "Cancel");
		_bCancel.setActionCommand(CANCEL);
		_bCancel.setOpaque(true);
		gbc.weightx = 0;
		gbc.fill       = gbc.NONE;
		gbc.insets = new Insets(PAD,PAD,3,8);
		getContentPane().add(_bCancel, gbc);
		setSize(WIDTH, HEIGHT);

		/* Cancel if the window is closed */
		addWindowListener(new winAdapter());

		m_task = task;
		_isCancelled = false;
	}

	/* start the process, if any an show the dialog */
	public void setVisible(boolean visible) {
		if (visible && m_task != null)
			new Thread(m_task).start();

                try{
	            super.setVisible (visible);
                } catch (Exception e){
                  /* java throws an exception if dialog is disposed before
                     it is properly displayed. But, in that case, we are
                     done, so can just ignore                              */
                } 
	}

	/**
	 *	Handle incoming event from button.
	 *
	 * @param e event
	 */
	public void actionPerformed(ActionEvent e) {
		Debug.println( "SimpleProgressDialog.actionPerformed " + e);
		if ( e.getActionCommand().equals(CANCEL) || e.getActionCommand().equals("close")) {
			if (e.getActionCommand().equals(CANCEL)) {
				_isCancelled = true;
			}
			if (m_task != null)
				m_task.stop ();
		}
	}

	/* dismisses the dialog and does all necessary cleanup     */
	/* called internally and by the task class when it is done */
	public void stop () {
		dispose();
		DSUtil.dialogCleanup();
    }

	/* this is necessary to cancel the task if dialog is closed */
	class winAdapter extends WindowAdapter {
	    public void windowClosing(WindowEvent e) {
			if (m_task != null)
				m_task.stop ();
		}
    }

	 public void packAndShow() {
		 super.pack();
		 setVisible(true);
	 }

	 public boolean isCancelled() {
		 return _isCancelled;
	 }

	private Stoppable m_task = null;
	 private boolean _isCancelled = false;
	private static final int PAD = 10;
	private static final int WIDTH = 600;
	private static final int HEIGHT = 150;
	private static final String CANCEL = "cancel";  
}
