/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.Vector;
import java.util.Enumeration;
import java.net.*;
import javax.swing.*;
import javax.swing.plaf.FileChooserUI;
import javax.swing.plaf.ComponentUI;
import javax.swing.filechooser.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.*;

/**
 * DSFileDialog
 * 
 *
 * @version 1.0
 * @author rweltman
 **/
public class DSFileDialog {
    public DSFileDialog() {
    }

    protected static String getFilePath( String path, boolean save, int mode,
										 FileFilter[] filters,
										 Component c ) {
		/* Use JFileChooser, not AWT FileDialog */

		// With JDK1.4 on Unix the first instance of JFileChooser
		// has an incorrect layout for some of the buttons. Discard
		// the first instance.
		if (_discardFirst) {
			_discardFirst = false;
			Object o = new JFileChooser();
		}

		JFileChooser fileChooser = new JFileChooser( path );
		fileChooser.setFileSelectionMode( mode );		
		if ( (filters != null) && (filters.length > 0) ) {
			for( int i = 0; i < filters.length; i++ ) {
				fileChooser.addChoosableFileFilter( filters[i] );
			}
		}
		JFrame frame = UtilConsoleGlobals.getActivatedFrame();
		if (c == null) {
			c = frame;
		}
		int result = save ? fileChooser.showSaveDialog( c ) :			
			fileChooser.showOpenDialog( c );
		String name = null;
		if ( result == fileChooser.APPROVE_OPTION ) {
			File curFile = fileChooser.getSelectedFile();
			if ( curFile != null ) {
				name = curFile.getPath();// + curFile.getName();
			}
		}
		return name;
	}

    protected static String getFilePath( String path, boolean save, int mode,
										 FileFilter[] filters ) {
		return getFilePath( path, save, mode, filters, null );
	}

    public static String getFileName(String path, boolean save,
									 Component c ) {
		return getFilePath( path, save, JFileChooser.FILES_ONLY, null, c );
	}
       
    public static String getFileName(String path, boolean save ) {
		return getFileName( path, save, null );
	}

    public static String getFileName(String path, boolean save,
									 String[] extensions,
									 String[] descriptions,
									 Component c) {
		FileFilter[] filters = null;
		if ( (extensions != null) && (descriptions != null) &&
			 (extensions.length == descriptions.length) ) {
			filters = new FileFilter[extensions.length];
			for( int i = 0; i < extensions.length; i++ ) {
				filters[i] = new ExtensionFileFilter( extensions[i], descriptions[i] );
			}
		}
		String returnFileName =  getFilePath( path, save, JFileChooser.FILES_ONLY, filters, c );
		if (returnFileName != null) {
			File file = new File(returnFileName);
			String parent = file.getParent();
			if (parent != null) {
				_path = parent + java.io.File.separator;
				Debug.println("DSFileDialog.getFilePath new path "+_path);
			}
		}
		return returnFileName;		
	}


    public static String getFileName(String path, boolean save,
									 String[] extensions,
									 String[] descriptions ) {
		return getFileName( path, save, extensions, descriptions,
							null );
	}

    public static String getDirectoryName(String path, boolean save,
										  Component c) {
		return getFilePath( path, save, JFileChooser.DIRECTORIES_ONLY, null,
							c );
	}
       
    public static String getDirectoryName(String path, boolean save ) {
		return getDirectoryName( path, save, null );
	}
       
    public static String getFileName(String path, Component c) {
		return getFileName( path, true, c );
    }
    
    public static String getFileName(String path) {
		return getFileName( path, null );
	}

	public static String getFileName( boolean save,
									 String[] extensions,
									 String[] descriptions,
									 Component c,
									 String defaultPath ) {		
		return getFileName(save, extensions, descriptions, c, "*", defaultPath);
	}

	public static String getFileName( boolean save,
									  String[] extensions,
									  String[] descriptions,
									  Component c ,
									  String extension,
									  String defaultPath) {
		if (_path == null) {
			if (defaultPath != null) {
				File defaultPathFile = new File(defaultPath);
				if (defaultPathFile.exists()) {
					_path = defaultPath;
				} else {
					_path = ".." + java.io.File.separator;
				}
			} else {
				_path = ".." + java.io.File.separator;
			}
		}
		String path = _path +extension;
		String returnFileName = getFileName(path, save, extensions, descriptions, c);
		if (returnFileName != null) {
			File file = new File(returnFileName);
			String parent = file.getParent();
			if (parent != null) {
				_path = parent+java.io.File.separator;
				Debug.println("DSFileDialog.getFilePath new path "+_path);
			}
		}
		return returnFileName;
	}

	public static String getPath() {
		return _path;
	}

    public static void main( String[] args ) {
        if (args.length < 1) {
            System.out.println("Please input filename");
            System.exit(1);
        }
        String s = getFileName(args[0], true);
		System.out.println( "Selected file: " + s );
		System.exit( 0 );
    }
	
	private static String _path;
	private static boolean _discardFirst = true;
}

class ExtensionFileFilter extends FileFilter {
	ExtensionFileFilter( String extension, String description ) {
		_extension = extension;
		_description = description;
	}
    public boolean accept( File f ) {
		if ( f == null ) {
			return false;
		}
		/* Allow navigating through directories */
		if ( f.isDirectory() ) {
			return true;
		}

		/* Case-insensitive on Windows; how about Mac? Does "." indicate Mac? */
		/* Note: this applies only for files on the local file system */
		if ( File.separator.equals("\\") || File.separator.equals(".") ) {
			return f.getName().toLowerCase().endsWith( "." + _extension.toLowerCase() );
		} else {
			return f.getName().endsWith( "." + _extension );
		}
	}

    /**
     * The description of this filter. For example: "JPG and GIF Images"
     * @see FileView#getName
     */
    public String getDescription() {
		return _description;
	}
	private String _extension;
	private String _description;
}

	
