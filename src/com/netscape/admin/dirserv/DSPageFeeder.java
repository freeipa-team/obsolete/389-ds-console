/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.Cursor;
import java.util.Vector;
import javax.swing.JFrame;
import com.netscape.management.client.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.UtilConsoleGlobals;

/**
 *	Netscape Directory Sever 4.0 page generator.
 *
 * @author  rweltman
 * @version 1.3, 03/21/00
 * @date	 	11/1/97
 */
public class DSPageFeeder extends FrameworkInitializer
	implements IAuthenticationChangeListener {
	/**
	 *	Constructor.
	 *
	 * @param l An object to be notified of authentication changes.
	 * @param info	Global console connection information
	 * @param serverInfo Server instance connection information
	 */
    public DSPageFeeder( IAuthenticationChangeListener l,
						 ConsoleInfo info,
						 ConsoleInfo serverInfo ) {
		_info = info;
		_serverInfo = serverInfo;
		
		_authChangeListeners.addElement(this);
		_authChangeListeners.addElement(l);

		// create the model
		DSResourceModel resourceModel = new DSResourceModel( _info,
															 _serverInfo );
		_authChangeListeners.addElement(resourceModel);
		_resourcePage = new DSResourcePage(resourceModel);
		_resourcePage.setPageTitle(DSUtil._resource.getString("resourcepage",
															  "title"));

//		DSContentModel contentModel = new DSContentModel(_info, _serverInfo);
//		_authChangeListeners.addElement(contentModel);
//		_contentPage = new DSResourcePage(contentModel);
//		_contentPage.setPageTitle(DSUtil._resource.getString("contentpage",
//															 "title"));
//		resourceModel.addContentListener( contentModel );

		_newContentPage = new DSContentPage(resourceModel);
		_authChangeListeners.addElement(_newContentPage);
		
		IDSModel statusModel = new DSStatusResourceModel( _info,
														  _serverInfo );
		_authChangeListeners.addElement(statusModel);
		_statusPage = new DSResourcePage(statusModel);
		_statusPage.setPageTitle(DSUtil._resource.getString("statuspage",
															"title"));

		setFrameTitle(DSUtil._skinResource.getString("dsAdmin","title"));
		String iconImage = DSUtil.isNT(info) ? _smallIconImage : _bigIconImage;
		setMinimizedImage( DSUtil.getPackageImage( iconImage ).getImage() );
		setBannerImage( DSUtil.getPackageImage( _bannerImage ).getImage() );
		setBannerText("");
		
		DSTaskModel taskModel = new DSTaskModel(_authChangeListeners,
												info, serverInfo);
		_taskPage = new TaskPage(taskModel);

		resourceModel.setAuthenticationChangeListener(_authChangeListeners);
//		contentModel.setAuthenticationChangeListener(_authChangeListeners);
		statusModel.setAuthenticationChangeListener(_authChangeListeners);
		_newContentPage.setAuthenticationChangeListener(_authChangeListeners);
		
		addPage(_taskPage);
		addPage(_resourcePage);
//		addPage(_contentPage);
		addPage(_newContentPage);
		addPage(_statusPage);
	}

	/**
	 * Set the frame associated to this feeder.
	 * In fact, we simply propagate the frame to the
	 * the associated resource models.
	 */
	public void setFrame(JFrame frame) {
	
		DSTaskModel taskModel = (DSTaskModel)_taskPage.getModel();
		DSResourceModel resourceModel = (DSResourceModel)_resourcePage.getModel();
//		DSContentModel contentModel = (DSContentModel)_contentPage.getModel();
		DSStatusResourceModel statusModel = (DSStatusResourceModel)_statusPage.getModel();
		
		taskModel.setFrame(frame);
		resourceModel.setFrame(frame);
//		contentModel.setFrame(frame);
		statusModel.setFrame(frame);
	}
	
	
	/**
	 * Override this to postpone creation of the about dialog.
	 *
	 */
	public void aboutInvoked(JFrame parent) {
	  if (aboutDialog == null ) {
		UtilConsoleGlobals.getActivatedFrame().
			setCursor(new Cursor(Cursor.WAIT_CURSOR));
		/* Set up About dialog. */
		aboutDialog = new DSAboutDialog(parent , DSUtil._skinResource );
		UtilConsoleGlobals.
			getActivatedFrame().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	  }
	  aboutDialog.show();
	}

	/**
	 * Called when authentication changes.
	 *
	 * @param oldAuth Previous authentication DN.
	 * @param newAuth New authentication DN.
	 */
	public void authenticationChanged(String oldAuth,
									  String newAuth,
									  String oldPassword,
									  String newPassword) {
		Debug.println("DSPageFeeder.authenticationChanged(): new bind DN = " +
					  newAuth + " old bind DN = " + oldAuth);
		DSTaskModel taskModel = (DSTaskModel)_taskPage.getModel();
		taskModel.reset(_authChangeListeners, _info, _serverInfo);
		_taskPage.setTaskModel(_taskPage.getModel());
	}

	private ConsoleInfo _info;				// global information
	private ConsoleInfo _serverInfo;		// instance information

	private TaskPage _taskPage;					// task page
	private ResourcePage	 _resourcePage;		// resource page
//	private ResourcePage	 _contentPage;		// content page
	private DSContentPage	 _newContentPage;	// new content page
	private ResourcePage	 _statusPage;		// status page
	private static final String _smallIconImage = "directory.gif";
	private static final String _bigIconImage = "directory.gif";
	private static final String _bannerImage = "dsbanner.gif";
	private Vector _authChangeListeners = new Vector();
}
