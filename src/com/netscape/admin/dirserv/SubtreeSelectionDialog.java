/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.EmptyBorder;
import com.netscape.admin.dirserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;

/**
 * Subtree DN selection Panel
 *
 * This class implements the minimum to take the DSContentModel
 * in as TreeModel and use its functionalities.
 *
 * @author  jpanchen
 * @version %I%, %G%
 * @date	 	12/13/97
 */
public class SubtreeSelectionDialog extends AbstractDialog
    implements ISubtreeSelectionDialog,
               ActionListener,
               TreeSelectionListener,
               MouseListener {

    /*==========================================================
     * constructors
     *==========================================================*/
    /**
     * Construct the subtree selection dialog.
     * @param parent A parent frame.
     * @param serverInfo Connection parameters to Directory.
     * @param rootOK true if "root" is a valid selection.
     * @param multipleOK true if multiple selection is allowed.
     */
	public SubtreeSelectionDialog( JFrame parent, ConsoleInfo serverInfo,
	                               boolean rootOK, boolean multipleOK ) {
		super(parent, true);
        _rootOK = rootOK;
        _multipleOK = multipleOK;
		String label = serverInfo.getHost()+":"+serverInfo.getPort();
		setTitle(_resource.getString("browser","title")
		        +" ["+label+"]");
		getAccessibleContext().setAccessibleDescription(_resource.getString("browser",
																			"description"));
		
		setSize(350,300);
		setLocationRelativeTo(parent);
		getContentPane().setLayout(new BorderLayout());
		_treeRenderer = new ResourceCellRenderer();
		DSEntryObject root = new DSEntryObject( null, "", label, false );
		_resourceModel = new DSContentModel( null, serverInfo, root);
		root.setModel(_resourceModel);

		getContentPane().add("Center",createTreePanel(_resourceModel));

        //button panel
		_bOK = UIFactory.makeJButton(this, _section, "OK");
        if ( !_rootOK )
		    _bOK.setEnabled( false );
		_bCancel = UIFactory.makeJButton(this, _section, "Cancel");
		_bHelp = UIFactory.makeJButton(this, _section, "Help");

		JButton[] buttons = { _bOK, _bCancel, _bHelp };
		JPanel buttonPanel = UIFactory.makeJButtonPanel( buttons, true );
		JPanel p = new JPanel();
		p.setLayout( new BorderLayout() );
		p.add( "Center", buttonPanel );
		p.add( "North",
			   Box.createVerticalStrut(UIFactory.getDifferentSpace()) );
		getContentPane().add("South", p);
	}

	/*==========================================================
	 * public methods
     *==========================================================*/

	/**
     * Returns array of selected IResourceObjects.
     */
	public IResourceObject[] getSelection() {
		IResourceObject[] selection = null;
		TreePath path[] = _tree.getSelectionPaths();
		if((path != null) && (path.length > 0))
		{
			selection = new IResourceObject[path.length];
			for(int index = 0; index < path.length; index++)
			{
				selection[index] =
					(IResourceObject)path[index].getLastPathComponent();
			}
		}
		return selection;
	}

	/**
     * Returns array of previously selected IResourceObjects.
     */
	public IResourceObject[] getPreviousSelection() {
		return _previousSelection;
	}

	//==== TREESELECTIONLISTENER ======
	public void valueChanged(TreeSelectionEvent ev) {
		IResourceObject[] selection = getSelection();
		_resourceModel.actionObjectSelected(
			_resourceModel.getSelectedPage(), selection,
			getPreviousSelection());
		_previousSelection = selection;
		checkSelection();
	}

    private void checkSelection() {
        if ( _bOK != null ) {
    		int nSel = _tree.getSelectionCount();
	    	boolean ok = (nSel > 0);
	    	if ( !_rootOK )
	    	    ok &= (_tree.getMinSelectionRow() > 0);
	    	if ( !_multipleOK )
	    	    ok &= (nSel < 2);
	    	_bOK.setEnabled( ok );
	    }
	}

	//==== MOUSELISTENER ========
	public void mouseClicked(MouseEvent e) {
		IResourceObject[] selection = getSelection();
		if(selection != null)
		{
			if(e.getClickCount() == 2)  // double click
			{
				if(selection.length == 1) // single selection
				{
					if(!_resourceModel.isLeaf(selection[0]))
						return;
				}

				_resourceModel.actionObjectRun(
					_resourceModel.getSelectedPage(),
					selection );
			}
		}
	}

	public void mousePressed(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}

    public void mouseReleased(MouseEvent e) {}

	//
	public void runComplete(IResourceObject ranObject) {}

    //==== ACTIONLISTENER =======
    public void actionPerformed(ActionEvent e) {
        //Cancel Pressed
        try {
            if (e.getSource().equals(_bCancel) || e.getActionCommand().equals("close")) {
				_isOk = false;
				setVisible(false);
				dispose();    
				DSUtil.dialogCleanup();
            }
            
            if (e.getSource().equals(_bHelp)) {
				DSUtil.help( _helpToken );
            }
            
            //Ok Pressed
            if ( e.getSource().equals(_bOK) ) {
                _dnString =
					((DSEntryObject)_tree.getLastSelectedPathComponent()).
					                getDN();
                _isOk = true;
                setVisible(false);
                dispose();
				DSUtil.dialogCleanup();
            }
        } catch (NullPointerException ex) {
            //temporary attemp to fix the JFC event null pointer error
            //which cause the dialog to malfunction.
            Debug.println("SubtreeSelectionDialog: Caught: "+ex.toString());
        }
    }
    
    /**
     * @return true if successful
     */
    public boolean isOk() {
        return _isOk;
    }
    
    /**
     * @return dn string if successful
     */
    public String getDN() {
        return _dnString;
    }

    /*==========================================================
	 * private methods
	 *==========================================================*/

	/**
	 * Creates JPanel that contains the scrollable tree view
	 * @param resourceModel tree model used
	 * @return tree panel
	 */
	private JComponent createTreePanel(IResourceModel resourceModel) {
		_tree = new JTree(resourceModel);
		_tree.setCellRenderer(_treeRenderer);
		_tree.addTreeSelectionListener(this);
		_tree.addMouseListener(this);
		int space = UIFactory.getBorderInsets().left;
		_tree.setBorder(new EmptyBorder(space,space,space,space));
        _tree.setSelectionRow(0);

		JScrollPane jscrollpane = new JScrollPane(
		                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
		                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jscrollpane.setBorder( UITools.createLoweredBorder() );
		jscrollpane.getViewport().add(_tree);

		return jscrollpane;
	}

				   
	public void packAndShow() {
		pack();
		super.show();
	}


    /*==========================================================
     * variables
     *==========================================================*/
    private JTree _tree = null;
    private DSContentModel _resourceModel;
    private ResourceCellRenderer _treeRenderer;
    private IResourceObject[] _previousSelection = null;
    private JButton _bOK, _bCancel, _bHelp;
    private boolean _isOk = false;
    private String _dnString;
    private boolean _rootOK;
    private boolean _multipleOK;

	private static final String _helpToken = "subtree-selection-dbox-help";

	//get resource bundle
	private static final String _section = "general";
    private static ResourceSet _resource = DSUtil._resource;
}
