#
# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# END COPYRIGHT BLOCK
#
David Gang had a vision
Andy Spillane made it real
Will Morris got the train moving
Frank Keeney made smooth the path
Neel Phadnis kept the train on its track
Rob Weltman SOAPed the rails
Mark Smith cleaned the tracks
Rich Megginson declared "Out of chaos will come order."
David McNeely painted a picture
Mike Kazmierczak put it into words
Don Young crossed the T's and dotted the I's
Jay Carson brought the bugs home
Ken Yu made sure they were safe
Nathan Kinder sustained it all
Anthony Foxworth sourced the source
Ulf Weltman spun the bits
Kim Dang spun them some more
Danny Chan led the hunt
Orla Hegarty exposed the cracks
To Ngan exposed some more
Chandra Kannan lit the bugs
Lakshmi Gopal added more light
Kelly Lucas made sure they were dead
Andy Hakim delivered the administration
Miodrag Kekic consoled the console
Alan Morgenegg helped the help
Supriya Shetty made all things clear
Maggie Lee lended a hand
Pete Rowley indexed what was not there
E.Y. Tsai sped things along
Kaining Gu made our message clear
Noriko Hosoi kept the data safe
Bindu Kaw Verified the Signs
Darshan Jani fed the penguins
David Boreham increased their appetite
Ashy Koshy Screened the Names
Prasanta Behera led the search for identity
Surendra Rajam made his presence known
Anish Nair did too
Ignatia Suwarna added more SOAP
Atul Bhandari led the delegates
Hin Man flattened the hierarchy
Rob Powers improved our image
Jason Nassi managed the support
Krishna Kuchibhotla supported the customers
Tammy Kim deployed the bits
