/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.*;
import com.netscape.management.client.IPage;
import com.netscape.management.client.IResourceObject;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.UITools;
import netscape.ldap.*;
import netscape.ldap.util.DN;
import com.netscape.admin.dirserv.propedit.*;
import com.netscape.admin.dirserv.attredit.*;
import com.netscape.admin.dirserv.account.AccountInactivation;

/**
  * Creates a customizer panel for a DS entry. It displays the
  * list of children of the node.
  *
  * @author  rweltman
  * @version %I%, %G%
  * @date	 11/3/97
  */
public class DSEntryList extends JPanel
                         implements IDSResourceSelectionModel,
	                                IDSResourceSelectionListener,
	                                ListSelectionListener,
	                                ListDataListener,
	                                ActionListener,
	                                FocusListener,
	                                MouseListener,
                                    MouseMotionListener {


	/**
	 * Constructs a list information panel with directory entries.
	 * This constructor is for use with a DS supporting Virtual List
	 * View. A VListEntryModel is created to supply entries as needed.
	 *
	 * @param ldc An open LDAP connection
	 * @param base The search base.
	 * @param scope The search scope - ONE or SUB
	 * @param filter The search filter - usually "objectclass=*"
	 * @param model Content model
	 */
	public DSEntryList( LDAPConnection ldc,
						String base,
						int scope,
						String filter,
						DSBaseModel model,
						IDSEntryObject parent ) {
		_model = model;
		_parent = parent;
		_host = ldc.getHost();
		_port = ldc.getPort();
		_authDN = ldc.getAuthenticationDN();
		_authPassword = ldc.getAuthenticationPassword();

		_hasVlist = true;
		_dataModel =
			new VListEntryModel( ldc, base, scope, filter, model, parent.getSelectedPartitionView() );
		_dataModel.setDebug( Debug.getTrace() );
		if (parent != null) {
			Debug.println(9, "DSEntryList.DSEntryList: referrals=" +
						  parent.getReferralsEnabled());
			_dataModel.setReferralsEnabled(parent.getReferralsEnabled());
		}

		// create the actual list structure
		_entryList = new JList( _dataModel );
		init();
	}

	/**
	 * Constructs a list information panel with directory entries.
	 * This constructor is for a brute force model - all entries
	 * are in the Vector.
	 *
	 * @param entries Vector av DSEntryObjects
	 * @param model Content model
	 */
	public DSEntryList( Vector entries,
						DSBaseModel model,
						IDSEntryObject parent ) {
		_entries = entries;
		_model = model;
		_parent = parent;
		_hasVlist = false;

		// create the actual list structure
		_entryList = new JList( entries );
		init();
	}

	private void init() {
		setLayout(new BorderLayout());
		setBackground( backgroundColor );
		setBorder( UITools.createLoweredBorder() );

		IconLabelCellRenderer renderer = new IconLabelCellRenderer();
		_entryList.setCellRenderer( renderer );
		/* Prevent the list from cycling through all values to calculate
		   size */
		_entryList.setPrototypeCellValue("1234567890 1234567890");

		/* Do not repaint while scrolling */
		_entryList.setValueIsAdjusting( false );
		KeyListener listener = new ListKeyListener();
		_entryList.addKeyListener( listener );
		addKeyListener( listener );

		/* Listen for various events on the list */
		_entryList.addMouseListener( this );
		_entryList.addMouseMotionListener( this );

		_entryList.registerKeyboardAction(
			                    new ListActionListener( this, _model.OPEN ),
								KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0),
								_entryList.WHEN_FOCUSED );
		_entryList.registerKeyboardAction(
			                    new ListActionListener( this, _model.ACL ),
								KeyStroke.getKeyStroke(KeyEvent.VK_A,0),
								_entryList.WHEN_FOCUSED );

		_entryList.registerKeyboardAction(
			new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
					String text = _entryList.getToolTipText();
					if ( text != null ) {
						StringSelection ss = new StringSelection( text );
						getToolkit().getSystemClipboard().setContents( ss,
																	   ss );
					}
				}
		    },
			KeyStroke.getKeyStroke(KeyEvent.VK_C,
								   Event.SHIFT_MASK|Event.CTRL_MASK),
			_entryList.WHEN_FOCUSED );
		_entryList.addListSelectionListener( this );
		_entryList.getSelectionModel().setSelectionMode( 2 );

		/* Scroll if necessary */
		_scrollPane = new SuiScrollPane( _entryList );
		_scrollPane.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );
		_scrollPane.addKeyListener( listener );
		_scrollPane.getVerticalScrollBar().addKeyListener( listener );
		int incr = ROW_HEIGHT;
		if (_entryList.getCellBounds(0, 0) != null) {
			incr = _entryList.getCellBounds(0, 0).height;
		}
		_scrollPane.getVerticalScrollBar().setUnitIncrement(incr);
		add(_scrollPane, BorderLayout.CENTER);

		/* Context menu */
		_contextMenu = new JPopupMenu();
		_entryList.add( _contextMenu );
		DSMenu.addMenuItem( _section, "Edit", _contextMenu, this,
							_resource, false );
		Component[] components = _contextMenu.getComponents();
		if (components != null) {
			for (int i=0; i < components.length; i++) {
				if (components[i] instanceof JMenuItem) {
					String actionCommand = ((JMenuItem)components[i]).getActionCommand();
					if (actionCommand.equals("paste")) {
						_pasteMenuItem = (JMenuItem)components[i];
					} else if (actionCommand.equals(DSContentModel.ACTIVATE)) {
						_activateMenuItem = (JMenuItem)components[i];
					} else if (actionCommand.equals(DSContentModel.INACTIVATE)) {
						_inactivateMenuItem = (JMenuItem)components[i];
					}
				}
			}
		}
		_entryList.addFocusListener( this );
		repaint();
		validate();
	}

	public void focusGained( FocusEvent e ) {
		repaint();
	}

	public void focusLost( FocusEvent e ) {
		repaint();
	}

	class ListKeyListener implements KeyListener {
		public void keyTyped(KeyEvent e) {
		}
		public void keyPressed(KeyEvent e) {
			if ( !_pressed & (e.getKeyCode() == _key) ) {
				Debug.println( "Key pressed" );
				_pressed = true;
				_scrollPane.setUpdateWhileAdjusting( _pressed );
			}
		}
		public void keyReleased(KeyEvent e) {
			if ( e.getKeyCode() == _key ) {
				Debug.println( "Key released" );
				_pressed = false;
				_scrollPane.setUpdateWhileAdjusting( _pressed );
			}
		}
		private final int _key = KeyEvent.VK_SHIFT;
		private boolean _pressed = false;
	}

	/* Handle keyboard events */
    class ListActionListener implements ActionListener {
		ListActionListener( ActionListener parent, String cmd ) {
			_parent = parent;
			_cmd = cmd;
		}
		/* Just dispatch our stored command to the parent */
        public void actionPerformed(ActionEvent e) {
 			Debug.println( "DSEntryListActionListener.actionPerformed <"  +
 						   e +"> <" + e.getSource() + "> <" +
 				           e.getActionCommand() + ">" );
			_parent.actionPerformed( new ActionEvent( e.getSource(),
													  e.getID(),
													  _cmd,
													  e.getModifiers() ) );
		}
		private String _cmd;
		private ActionListener _parent;
	}

	private IDSEntryObject getEntryObject( int index ) {
		IDSEntryObject deo;
		if ( _hasVlist ) {
			deo =
				(IDSEntryObject)_entryList.getModel().getElementAt( index );
		} else {
			deo = (IDSEntryObject)_entries.elementAt( index );
		}
		if ( deo != null ) {
			deo.setParentNode( _parent );
		}
		return deo;
	}


   /**
	 * Called by DSContentModel to get the object with the specified dn.  This method only looks in the entries that are 
	 * in memory as it is used for Refresh issues by DSContentModel.
	 *
	 * @returns the IDSEntryObject corresponding to the dn provided. null if not found.
	 */
	public IDSEntryObject getEntryObject(String dn) {		
		DN entryDN = new DN(dn);
		if (!_hasVlist) {
			if (_entries != null) {
				int size = _entries.size();
				for (int i=0; i< size; i++) {
					Object o = _entries.elementAt(i);
					if (o instanceof IDSEntryObject) {						
						DN currentDN = new DN(((IDSEntryObject)o).getDN());
						if (currentDN.equals(entryDN)) {
							return (IDSEntryObject)o;
						}
					}
				}
			}
		} else {
			if (_dataModel != null) {
				Vector entries = _dataModel.getStoredEntries();
				if (entries != null) {
					int size = entries.size();
					for (int i=0; i< size; i++) {
						Object o = entries.elementAt(i);
						if (o instanceof IDSEntryObject) {
							DN currentDN = new DN(((IDSEntryObject)o).getDN());
							if (currentDN.equals(entryDN)) {
								return (IDSEntryObject)o;
							}
						}
					}
				}
			}
		}
		return null;
	}

    /**
	 * Returns the parent DN of the objects of the panel
	 * @returns a DN containing the dn of the parent. null if not found.
	 */
	public DN getParentDN() {
		if (!_hasVlist) {			
			if (_entries != null) {
				for (int i=0; i< _entries.size(); i++) {
					Object o = _entries.elementAt(i);
					if (o != null) {						
						if (o instanceof IDSEntryObject) {  		   						
							DN entryDN = new DN(((IDSEntryObject)o).getDN());
							if (entryDN != null) {
								return entryDN.getParent();
							}
						}
					}
				}
			}
		} else {
			if (_dataModel != null) {
				Vector entries = _dataModel.getStoredEntries();				
				if (entries != null) {
					for (int i=0; i< entries.size(); i++) {
						Object o = entries.elementAt(i);
						if (o != null) {							
							if (o instanceof IDSEntryObject) {							   						
								DN entryDN = new DN(((IDSEntryObject)o).getDN());
								if (entryDN != null) {
									return entryDN.getParent();
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	void recalculate() {
		if ( _hasVlist ) {
			Debug.println(9, "DSEntryList.recalculate");
			_dataModel.recalculate();
		} else {
			Debug.println(9, "DSEntryList.recalculate: _entries=" +
						  _entries.size());
			_entryList.setListData( _entries );
		}
	}
	/**
	 * Process action events, mostly keyboard ones.
	 *
	 * @param e The event.
	 */
    public void actionPerformed(ActionEvent e) {
		Debug.println( "DSEntryList.actionPerformed <"  + e +"> <" +
			e.getSource() + ">" + " _model=" + _model);
		IResourceObject[] selection = getCurrentSelection();
		if ( selection.length > 0 ) {
			_model.actionSelected( e.getActionCommand(), selection,
								   _model.getSelectedPage() );
		}
	}

	/**
	 * Return list of currently selected objects
	 */
    private IResourceObject[] getCurrentSelection() {
		int[] indices = _entryList.getSelectedIndices();
		if ( indices.length > 0 ) {
			IResourceObject[] selection =
				new IResourceObject[indices.length];
			for( int i = 0; i < indices.length; i++ ) {
				selection[i] = getEntryObject( indices[i] );
			}
			return selection;
		}
		return new IResourceObject[0];
	}

    /**
	 * Process mouse events, in order to pop up context menu
	 */
    void singleClicked( MouseEvent e ) {
		Debug.println( "DSEntryList.singleClicked " + e );
		int index = _entryList.locationToIndex(e.getPoint());
		if ( index >= 0 ) {
			int mods = e.getModifiers();
			if ( (mods & e.BUTTON3_MASK) != 0 ) {
				popup( e );
			}
		}
	}

    void doubleClicked( MouseEvent e ) {
		int index = _entryList.locationToIndex(e.getPoint());
        //Debug.println("DSEntryList.doubleClicked on Item " + index);
		if ( index >= 0 ) {
			IDSEntryObject deo = getEntryObject( index );
			if ( deo != null ) {
				deo.editProperties();
			}
		}
	}

    private void popup( MouseEvent e ) {
		Debug.println( "DSEntryList.popup " + e );
		if ( _contextMenu != null ) {
			/* Update the Paste Menu Item */
			if ((_model instanceof DSContentModel) && (_pasteMenuItem != null)) {
				if (((DSContentModel)_model).isClipboardEmpty()) {
					_pasteMenuItem.setEnabled(false);
				} else {
					_pasteMenuItem.setEnabled(true);
				}
			}
			/* Update the Activate and Inactivate Menu Item */
			int[] indices = _entryList.getSelectedIndices();
			if ( indices.length > 0 ) {
				boolean oneLocked = false;
				boolean oneNotLocked = false;
				boolean isLocked = false;
				for (int i=0; i< indices.length; i++) {
					int index = indices[i];
					if ( index >= 0 ) {
						IDSEntryObject deo = getEntryObject( index );
						if ( deo != null ) {
							LDAPEntry entry = deo.getEntry();
							if (entry != null) {
								/* Check if the entry is activated or inactivated */							
								AccountInactivation account = new AccountInactivation(entry);
								try {
									isLocked = account.isLocked(_model.getServerInfo().getLDAPConnection());
									oneLocked = oneLocked || isLocked;
									oneNotLocked = oneNotLocked || !isLocked;
								} catch (LDAPException ex) {
									Debug.println("DSEntryList.popup() "+ex);
								}
							}
						}
					}
				}
				if ( oneLocked ) {
					if (_activateMenuItem != null) {
						_activateMenuItem.setEnabled(true);
					}
				} else {
					if (_activateMenuItem != null) {
						_activateMenuItem.setEnabled(false);
					}
				}
				
				if (oneNotLocked) {			   
					if (_inactivateMenuItem != null) {
						_inactivateMenuItem.setEnabled(true);
					}
				} else {
					if (_inactivateMenuItem != null) {
						_inactivateMenuItem.setEnabled(false);
					}
				}
			}			
			_contextMenu.show( _entryList, e.getX(), e.getY() );
			e.consume();
		}
	}

	public void processMouseEvent(MouseEvent e)	{
		super.processMouseEvent( e );
	}

	void keyEvent( ListSelectionEvent e ) {
		int i = e.getFirstIndex();
		Debug.println( "DSEntryList keyEvent " + i );
	}

	/**
	 * Process mouse clicks, in order to distinguish between single-clicks
	 * and double-clicks; dispatch accordingly.
	 */
    public void mouseClicked(MouseEvent e) {
		/* Don't make a left-click plus a right-click be a double */
		int mods = e.getModifiers();
		if ( (e.getClickCount() == 2) && (mods & e.BUTTON3_MASK) == 0 ) {
			doubleClicked( e );
		} else {
			singleClicked( e );
		}
	}

    public void mousePressed(MouseEvent e) {
	}

    public void mouseReleased(MouseEvent e) {
	}

    public void mouseEntered(MouseEvent e) {
	}

    public void mouseExited(MouseEvent e) {
//		_entryList.setToolTipText( null );
	}

    public void mouseDragged(MouseEvent e) {
//		Debug.println( "DSEntryList.mouseDragged " + e );
	}

    public void mouseMoved(MouseEvent e) {

		Point pt = e.getPoint();
		int i = _entryList.locationToIndex( pt );
		String tip = null;
		if ( i >= 0 ) {
			IDSEntryObject deo = getEntryObject( i );
			if ( deo != null ) {
				tip = deo.getDN();
			}
		}
		_entryList.setToolTipText( tip );
	}

    void setData( Vector v ) {
		_entries = v;
		/* Force recalculation */
		recalculate();
	}

    void addElement( Object o ) {
		if ( _entries != null )
			_entries.addElement( o );
		/* Force recalculation */
		recalculate();
	}

    public void removeElement( Object o ) {
		if ( _entries != null )
			_entries.removeElement( o );
		/* Force recalculation */
		recalculate();
	}

	void removeAllElements() {
		if ( _entries != null )
			_entries.removeAllElements();
		if ( _dataModel != null )
			_dataModel.reset();
	}

	/** 
	 * Called whenever the value of the selection changes. Just delegate
	 * to the model.
	 * @param e the event that characterizes the change.
	 */
    public void valueChanged(ListSelectionEvent e) {
//		Debug.println( "DSEntryList.valueChanged " + e );
		IResourceObject[] oldSel = _previousSelection;
		IResourceObject[] selection = getCurrentSelection();
		_previousSelection = selection;
		_model.setSelected( _model.getSelectedPage(),
							selection, oldSel );
	}

    /**
	 * Sent after the indices in the index0,index1 interval have been
	 * inserted in the data model. The new interval includes both
     * index0 and index1. 
	 */
    public void intervalAdded(ListDataEvent e) {
		Debug.println( "DSEntryList.intervalChanged " + e );
	}

    /**
	 * Sent after the indices in the index0,index1 interval have been
	 * removed from the data model. The interval includes both
     * index0 and index1. 
	 */
    public void intervalRemoved(ListDataEvent e) {
		Debug.println( "DSEntryList.intervalRemoved " + e );
	}

    /**
	 * Sent when the contents of the list has changed in a way that's too
	 * complex to characterize with the previous methods.
     * Index0 and index1 bracket the change. 
	 */
    public void contentsChanged(ListDataEvent e) {
		Debug.println( "DSEntryList.contentsChanged " + e );
	}

    /**
      * Adds a listener that is interested in receiving selection events.
	  * Called by panels
      */
	public void addIDSResourceSelectionListener(
	                                    IDSResourceSelectionListener l) {
		_selectionListeners.addElement(l);
	}

    /**
      * Removes previously added IDSResourceSelectionListener.
	  * Called by panels
      */
	public void removeIDSResourceSelectionListener(
	                                    IDSResourceSelectionListener l) {
		_selectionListeners.removeElement(l);
    }	                                        

	/**
     *   Returns list of listeners for this model.
	 */
	public Enumeration getSelectionListeners() {
		return _selectionListeners.elements();
	}

    /**
      * Called when the object is selected.
      */
    public void select(IResourceObject parent, IPage viewInstance) {
        Debug.println("DSEntryList.select() called.");
    }

    /**
      * Called when the object is unselected.
      */
    public void unselect(IResourceObject parent, IPage viewInstance) {
        Debug.println("DSEntryList.unselect() called.");
    }


	private String _host;
	private int _port;
	private String _authDN;
	private String _authPassword;
	private JList 	_entryList;		// entry table control
	private Vector  _entries = null;
	private VListEntryModel _dataModel = null;	// data model for the entries
    private DSBaseModel _model = null;
	private JPopupMenu _contextMenu;
	private JMenuItem _pasteMenuItem;
	private JMenuItem _activateMenuItem;
	private JMenuItem _inactivateMenuItem;
	private Vector _selectionListeners = new Vector();
    private IDSEntryObject _parent = null;
	private boolean _hasVlist = true;
	private IResourceObject[] _previousSelection = new IResourceObject[0];
	private SuiScrollPane _scrollPane = null;
	
	static ResourceSet _resource = DSUtil._resource;
	static final String _section = "entryList";

// Display an icon and a string for each object in the list.
   
class IconLabelCellRenderer
    extends com.netscape.management.nmclf.SuiListCellRenderer {

    IconLabelCellRenderer() {
        super();
    }

    // This is the only method defined by ListCellRenderer.  We just
    // reconfigure the Jlabel each time we're called.
    public Component getListCellRendererComponent(
        JList list,              
        Object value,            // value to display
        int index,               // cell index
        boolean isSelected,      // is the cell selected
        boolean cellHasFocus)    // the list and the cell have the focus
    {
        String text = null;
        Icon icon = null;

        if ( value == null ) {
            text = "null";
        } else if ( value instanceof IDSEntryObject ) {
            IDSEntryObject deo = (IDSEntryObject)value;
            text = deo.getDisplayName();
            icon = deo.getIcon();
        } else {
            text = value.toString();
        }

        super.getListCellRendererComponent(list, text, index,
                                           isSelected,
                                           cellHasFocus);

        // 603083: Starting with JDK 1.3, the icon must be set after
        // the call to super.getListCellRendererComponent(...)
        if (icon != null) {
            setIcon(icon);
        }

        return this;
    }
}
	static private Color backgroundColor =
			UIManager.getColor("List.background");
	static private final int ROW_HEIGHT = 10;
}
