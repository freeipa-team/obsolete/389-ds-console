/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv;

import java.util.*;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.browser.NumSubordinateHacker;

public class DatabaseConfig {
	Vector _rootSuffixes;
	NumSubordinateHacker _numSubordinateHacker;

	LDAPSearchConstraints _searchConstraints;

	public static final int LDBM_DATABASES = 0;
	public static final int CHAINING_DATABASES = 1;
	public static final int ALL_DATABASES = 2;

    private static final String CHAINING_CONFIG_BASEDN = "cn=chaining database, cn=plugins, cn=config" ;
    private	static final String LDBM_CONFIG_BASEDN = "cn=ldbm database, cn=plugins, cn=config" ;

	public DatabaseConfig() {
		// NUMSUBORDINATE HACK
		// DatabseConfig is the owner of the hacker.
		// It will keep it up to date.
		_numSubordinateHacker = new NumSubordinateHacker();
	}

	public synchronized void reload(LDAPConnection ldc) throws LDAPException {
		_rootSuffixes = new Vector();

		/* Get the LDBM databases */
		String filter = "objectclass=nsBackendInstance";
		String baseDN = LDBM_CONFIG_BASEDN;
		String[] attrs = {"cn", "nsslapd-suffix"};

		_searchConstraints = (LDAPSearchConstraints)ldc.getSearchConstraints().clone();
		_searchConstraints.setServerControls(DSContentPage._manageDSAITControl);
		LDAPSearchResults res = ldc.search( baseDN,
											ldc.SCOPE_SUB,
											filter,
											attrs,
											false,
											_searchConstraints);
		while( res.hasMoreElements() ) {
			LDAPEntry entry = (LDAPEntry)res.nextElement();
			String databaseName = DSUtil.getAttrValue(entry, "cn");
			Database database = new Database();
			database.setName(databaseName);
			database.setIsLocal(true);		   
			
			String suffixName = DSUtil.getAttrValue(entry, "nsslapd-suffix");
			Suffix suffix = getSuffix(suffixName, _rootSuffixes);
			if ( suffix == null) {
				suffix = new Suffix();
				suffix.setName(suffixName);								
				suffix.setHasRootEntry(hasRootEntry(ldc, suffix));
				insertSuffix(suffix);				
			}
			database.setSuffix(suffix);
			suffix.addDatabase(database);
		}

		/* Get the Chaining databases */		
		baseDN = CHAINING_CONFIG_BASEDN;		
		res = ldc.search( baseDN,
						  ldc.SCOPE_SUB,
						  filter,
						  attrs,
						  false,
						  _searchConstraints);
		while( res.hasMoreElements() ) {
			LDAPEntry entry = (LDAPEntry)res.nextElement();
			String databaseName = DSUtil.getAttrValue(entry, "cn");
			Database database = new Database();
			database.setName(databaseName);
			database.setIsLocal(false);		   
			
			String suffixName = DSUtil.getAttrValue(entry, "nsslapd-suffix");
			Suffix suffix = getSuffix(suffixName, _rootSuffixes);
			if ( suffix == null) {
				suffix = new Suffix();
				suffix.setName(suffixName);								
				suffix.setHasRootEntry(hasRootEntry(ldc, suffix));
				insertSuffix(suffix);
			}
			database.setSuffix(suffix);
			suffix.addDatabase(database);
		}
		// NUMSUBORDINATE HACK
		// Let's keep the hacker up to date
		_numSubordinateHacker.update(
			getSuffixesWithEntryList(),
			getRootSuffixesWithEntry(),
			ldc.getHost(),
			ldc.getPort()
		);
	}

	// NUMSUBORDINATE HACK
	// Make the hacker public so that controllers can
	// use it.
	public NumSubordinateHacker getNumSubordinateHacker() {
		return _numSubordinateHacker;
	}
	
	
	public Vector getRootSuffixesWithEntry() {
		Vector v = new Vector();
		Enumeration e = _rootSuffixes.elements();
		while (e.hasMoreElements()) {
			Suffix currentSuffix = (Suffix)e.nextElement();
			if (currentSuffix.hasRootEntry()) {
				v.addElement(currentSuffix.getName());
			}
		}
		return v;
	}

	public Vector getRootSuffixesWithoutEntry() {
		Vector v = new Vector();
		Enumeration e = _rootSuffixes.elements();
		while (e.hasMoreElements()) {
			Suffix currentSuffix = (Suffix)e.nextElement();
			if (!currentSuffix.hasRootEntry()) {
				v.addElement(currentSuffix.getName());
			}
		}
		return v;
	}

	public Vector getSuffixesWithEntryList() {
		Vector v = new Vector();
		getSuffixesWithEntryList(_rootSuffixes, v);
		return v;
	}

	public Vector getSuffixesWithoutEntryList() {
		Vector v = new Vector();
		getSuffixesWithoutEntryList(_rootSuffixes, v);
		return v;
	}

	public Vector getDatabaseList(int type) {
		Vector v = new Vector();
		getDatabaseList(_rootSuffixes, v, type);
		return v;
	}

	public String getRootSuffixForEntry(String dn) {
		DN entryDN = new DN(dn);
		String suffix = null;
		boolean done = false;
		Enumeration e = _rootSuffixes.elements();
		while (e.hasMoreElements() && 
			   !done) {
			Suffix currentSuffix = (Suffix)e.nextElement();
			DN currentDN = new DN(currentSuffix.getName());			
			if (entryDN.equals(currentDN) ||
				entryDN.isDescendantOf(currentDN)) {
				done = true;
				suffix = currentSuffix.getName();
			}
		}
		return suffix;
	}

	public String getSuffixForDatabase(String databaseName) {
		String suffixName = null;
		Database database = getDatabase(databaseName, _rootSuffixes);
		if (database != null) {
			Suffix suffix = database.getSuffix();
			if (suffix != null) {
				suffixName = suffix.getName();
			}
		}
		return suffixName;
	}	

	/**
	  * Returns the suffix to which this entry belongs.
	  * If it is a suffix itself, it returns its parent. 
	  * If it is a root suffix, it returns "".
	  * If nothing is found, returns null.
	  */
	public String getParentSuffix(String dn) {
		String parentSuffix = null;
	    Suffix s = getParentSuffix(dn, _rootSuffixes);
		if (s != null) {
			parentSuffix = s.getName();
		}
		return parentSuffix;
	}

	/* Sets hasRootEntry boolean of the suffix */
	public void setHasRootEntry(String suffixDn, boolean hasRootEntry) {
		Suffix suffix = getSuffix(suffixDn, _rootSuffixes);
		if ( suffix != null) {										
			suffix.setHasRootEntry(hasRootEntry);
		} else {
			throw new IllegalArgumentException("Suffix "+suffixDn+" does not exist");
		}
	}

	private Suffix getParentSuffix(String dn, Vector startPoint) {
		Suffix parentSuffix = null;
		boolean done = false;
		DN entryDN = new DN(dn);
		for (int i=0; (i < startPoint.size()) && !done; i++) {
			Suffix s = (Suffix)startPoint.elementAt(i);
			String sName = s.getName();
			DN currentDN = new DN(sName);
			if (entryDN.equals(currentDN) || currentDN.isDescendantOf(entryDN)) {
				parentSuffix = s.getParentSuffix();
				if (parentSuffix == null) {
					/* Is a root suffix, 'create' the root entry suffix */
					parentSuffix =  new Suffix();
					parentSuffix.setName("");								
					parentSuffix.setHasRootEntry(true);
				}
				done = true;
			} else if (entryDN.isDescendantOf(currentDN)) {
				parentSuffix = getParentSuffix(dn, s.getChildSuffixes());				
				done = true;
			}
		}
		return parentSuffix;
	}

	private boolean hasRootEntry(LDAPConnection ldc, Suffix suffix) throws LDAPException {
		boolean hasRootEntry = false;
		String[] attrs = {"dn"};
		try {
			if (ldc.read(suffix.getName(), attrs, _searchConstraints) != null) {
				hasRootEntry = true;
			}
		} catch (LDAPException lde) {
			if (lde instanceof LDAPReferralException) {
				/* The suffix is a referral (we get this even with the _managedDSAIT control...
				 assume the root entry exists in the remote server */
				hasRootEntry = true;
			} else if (lde.getLDAPResultCode() != LDAPException.NO_SUCH_OBJECT) {
				/* If something else went wrong: return true and expect the BrowserController to 
				   show an error icon */
				hasRootEntry = true;
				Debug.println("DatabaseConfig.hasRootEntry: "+lde);
			}
		}
		return hasRootEntry;
	}

	private void insertSuffix(Suffix suffix) {		
		Suffix parentSuffix = getParentSuffix(suffix, _rootSuffixes);		
		Vector childSuffixes = new Vector();
		if (parentSuffix == null) {			
			_rootSuffixes.addElement(suffix);
						
			childSuffixes = getChildSuffixes(suffix, _rootSuffixes);			
		} else {			
			suffix.setParentSuffix(parentSuffix);
			parentSuffix.addChildSuffix(suffix);

			childSuffixes = getChildSuffixes(suffix, parentSuffix.getChildSuffixes());
		}
		if (childSuffixes != null) {
			for (int i=0; i<childSuffixes.size(); i++) {				
				Suffix childSuffix = (Suffix)childSuffixes.elementAt(i);				
				suffix.addChildSuffix(childSuffix);
				
				/* Set correctly the parent of the child suffix (and remove it
				   from its previous parent suffix) */
				if (childSuffix.getParentSuffix() != null) {
					childSuffix.getParentSuffix().removeChildSuffix(childSuffix);
				} else {
					/* If the parent is null, it may had been considered as a root suffix */
					_rootSuffixes.removeElement(childSuffix);
				}
				childSuffix.setParentSuffix(suffix);
			}
		}
	}

	private Vector getChildSuffixes(Suffix suffix, Vector startPoint) {
		Vector childSuffixes = new Vector();
		DN suffixDN = new DN(suffix.getName());
		for (int i=0; i < startPoint.size(); i++) {
			Suffix s = (Suffix)startPoint.elementAt(i);
			String sName = s.getName();
			DN currentDN = new DN(sName);
			if (currentDN.isDescendantOf(suffixDN)) {
				childSuffixes.addElement(s);
			}
		}
		return childSuffixes;
	}

	private Suffix getParentSuffix(Suffix suffix, Vector startPoint) {
		Suffix parentSuffix = null;
		boolean done = false;
		DN suffixDN = new DN(suffix.getName());
		for (int i=0; (i < startPoint.size()) && !done; i++) {
			Suffix s = (Suffix)startPoint.elementAt(i);
			String sName = s.getName();
			DN currentDN = new DN(sName);
			if (currentDN.isDescendantOf(suffixDN)) {
				/* there's no parent suffix in this start point! */
				done = true;
			} else if (suffixDN.isDescendantOf(currentDN)) {
				Suffix deeperSuffix = getParentSuffix(suffix, s.getChildSuffixes());
				if ( deeperSuffix == null) {
					/* The case where we can't go deeper: the parent suffix is the current suffix */
					parentSuffix = s;
				} else {
					parentSuffix = deeperSuffix;
				}
				done = true;
			}
		}
		return parentSuffix;
	}

	private Suffix getSuffix(String suffixName, Vector startPoint) {
		Suffix suffix = null;
		boolean done = false;
		DN suffixDN = new DN(suffixName);
		for (int i=0; (i < startPoint.size()) && !done; i++) {
			Suffix s = (Suffix)startPoint.elementAt(i);
			String sName = s.getName();
			DN currentDN = new DN(sName);
			if (suffixDN.equals(currentDN)) {
				suffix = s;
			} else if (currentDN.isDescendantOf(suffixDN)) {
				/* there's no suffix in this start point! */
				done = true;
			} else if (suffixDN.isDescendantOf(currentDN)) {
				suffix = getSuffix(suffixName, s.getChildSuffixes());				
				done = true;
			}
		}
		return suffix;
	}

	private void getSuffixesWithEntryList(Vector startPoint, Vector v) {
		Enumeration e = startPoint.elements();
		while (e.hasMoreElements()) {
			Suffix currentSuffix = (Suffix)e.nextElement();
			if (currentSuffix.hasRootEntry()) {
				v.addElement(currentSuffix.getName());
			}
			if (currentSuffix.getChildSuffixes() != null) {
				getSuffixesWithEntryList(currentSuffix.getChildSuffixes(), v);
			}
		}		
	}

	private void getSuffixesWithoutEntryList(Vector startPoint, Vector v) {
		Enumeration e = startPoint.elements();
		while (e.hasMoreElements()) {
			Suffix currentSuffix = (Suffix)e.nextElement();
			if (!currentSuffix.hasRootEntry()) {
				v.addElement(currentSuffix.getName());
			}			
			if (currentSuffix.getChildSuffixes() != null) {
				getSuffixesWithoutEntryList(currentSuffix.getChildSuffixes(), v);
			}
		}		
	}   	

	private void getDatabaseList(Vector startPoint, Vector v, int type) {
		Enumeration e = startPoint.elements();
		while (e.hasMoreElements()) {
			Suffix currentSuffix = (Suffix)e.nextElement();
			Vector databases = currentSuffix.getDatabases();
			if (databases != null) {
				Enumeration eDatabases = databases.elements();
				while (eDatabases.hasMoreElements()) {
					Database currentDatabase = (Database)eDatabases.nextElement();
					if ((type == ALL_DATABASES) ||
						((type == LDBM_DATABASES) && currentDatabase.isLocal()) ||
						((type == CHAINING_DATABASES) && !currentDatabase.isLocal())) {
						v.addElement(currentDatabase.getName());
					}
				}
			}
			if (currentSuffix.getChildSuffixes() != null) {
				getDatabaseList(currentSuffix.getChildSuffixes(), v, type);
			}
		}
	}	

	private Database getDatabase(String databaseName, Vector startPoint) {
		Database database = null;
		Enumeration e = startPoint.elements();
		while (e.hasMoreElements() &&
			   (database == null)) {
			Suffix currentSuffix = (Suffix)e.nextElement();
			Vector databases = currentSuffix.getDatabases();
			if (databases != null) {
				Enumeration eDatabases = databases.elements();
				while (eDatabases.hasMoreElements() &&
					   (database == null)) {
					Database currentDatabase = (Database)eDatabases.nextElement();
					if (currentDatabase.getName().equals(databaseName)) { // TO MODIFY ? -> ignore case ??
						database = currentDatabase;
					}
				}
			}
			if (database == null) {
				if (currentSuffix.getChildSuffixes() != null) {
					database = getDatabase(databaseName, currentSuffix.getChildSuffixes());
				}
			}
		}		
		return database;
	}
}

class Suffix {
	String _suffixName;
	Vector _databases = new Vector();
	Vector _childSuffixes = new Vector();
	Suffix _parentSuffix = null;
	boolean _hasRootEntry;
	
	public void setName(String suffixName) {
		_suffixName = suffixName;
	}
	
	public void setParentSuffix(Suffix parentSuffix) {
		_parentSuffix = parentSuffix;
	}

	public Suffix getParentSuffix() {
		return _parentSuffix;
	}
	
	public Vector getChildSuffixes() {
		return _childSuffixes;
	}

	public void addChildSuffix(Object suffix) {
		_childSuffixes.addElement(suffix);
	}

	public void removeChildSuffix(Object suffix) {
		_childSuffixes.removeElement(suffix);
	}

	public String getName() {
		return _suffixName;
	}
	
	public void addDatabase(Object database) {		
		_databases.addElement(database);
	}

	public boolean hasRootEntry() {
		return _hasRootEntry;
	}

	public void setHasRootEntry(boolean hasRootEntry) {
		_hasRootEntry = hasRootEntry;
	}

	public Vector getDatabases() {
		return _databases;
	}
}

class Database {
	boolean _isLocal;
	String _databaseName;
	Suffix _suffix;

	public void setName(String databaseName) {
		_databaseName = databaseName;
	}

	public String getName() {
		return _databaseName;
	}

	public void setSuffix(Suffix suffix) {
		_suffix = suffix;
	}

	public Suffix getSuffix() {
		return _suffix;
	}

	public void setIsLocal(boolean isLocal) {
		_isLocal = isLocal;
	}

	public boolean isLocal() {
		return _isLocal;
	}
}
