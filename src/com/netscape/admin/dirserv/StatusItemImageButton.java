/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

//package com.netscape.management.client;
package com.netscape.admin.dirserv;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import com.netscape.management.client.IStatusItem;

/**
  * A status item that displays a button with two image states.
  *
  * @see IStatusItem
  */
public class StatusItemImageButton extends TwoStateButton
                                   implements IStatusItem {
	protected String _id = null;

    /**
      * Creates status component with specified state.
      */
	public StatusItemImageButton(String id, Icon upImage, Icon downImage ) {
		super( upImage, downImage );
		setID( id );
		setState( upImage );
	}

	/**
      * Returns the associated view Component.
      */
	public Component getComponent() {
		return this;
	}

    /**
      * Returns unique, language independant ID.
      */
	public String getID() {
		return _id;
	}

    /**
      * Sets ID
      */
	public void setID(String id) {
		_id = id;
	}

    /**
      * Sets state.
      */
	public void setState(Object state) {
		setIcon((Icon)state);
	}

    /**
      * Returns current state.
      */
	public Object getState() {
		return getIcon();
	}
}
