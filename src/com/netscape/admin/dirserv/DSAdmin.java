/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.*;
import java.net.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import com.netscape.management.client.*;
import com.netscape.management.client.topology.AbstractServerObject;
import com.netscape.management.client.topology.NodeData;
import com.netscape.management.client.topology.IRemovableServerObject;
import com.netscape.management.client.console.Console;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.console.VersionInfo;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.LDAPUtil;
import com.netscape.management.client.util.UtilConsoleGlobals;
import com.netscape.management.client.preferences.Preferences;
import com.netscape.management.client.preferences.PreferenceManager;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.admin.dirserv.panel.BlankPanel;
import com.netscape.admin.dirserv.task.CGITask;
import com.netscape.admin.dirserv.task.Remove;
import com.netscape.admin.dirserv.task.Start;
import com.netscape.admin.dirserv.task.Stop;

/**
 * Netscape directory server 4.0 configuration entry point. The
 * directory server needs to contain the name of this class in order
 * for the topology view to load this class.
 *
 * @author	rweltman
 * @version %I%, %G%
 * @date		10/30/97
 */

public class DSAdmin extends AbstractServerObject
					 implements IAuthenticationChangeListener,
								IMenuInfo,
								IRemovableServerObject {
	/**
	 *	Default constructor.
	 */
	public DSAdmin() {
		Debug.println(9, "DSAdmin.DSAdmin: constructor");
	}


	/**
	 * Return the securityState property.
	 * If securityState is SECURITY_UNKNOWN, then
	 * try to read nsslapd-security from the server.
	 */
	public int getSecurityState () throws LDAPException {
		if (_securityState == SECURITY_UNKNOWN) {
			// Let's try to read nsslapd-security from the server
			LDAPConnection ldc = getServerInfo().getLDAPConnection();
			String[] configAttrs = { "nsslapd-security" };
			LDAPEntry configEntry = ldc.read("cn=config", configAttrs);
			String v = DSUtil.getAttrValue(configEntry, configAttrs[0]);
			if (v.equalsIgnoreCase("on"))
				_securityState = SECURITY_ENABLE;
			else
				_securityState = SECURITY_DISABLE;
		}
		return _securityState;
	}
	
	/**
	 * Set the securityState property.
	 */
	public void setSecurityState(int state) {
		_securityState = state;
	}
	
	
	/**
	 * Return the instance name of this server object.
	 */
	public String getInstanceName() {
		return DSUtil.getInstanceName(_serverInfo);
	}
	
	
	/**
	 * Add a DSAdminEvent listener to this server object.
	 */
	public void addDSAdminEventListener(IDSAdminEventListener l) {
		_dsAdminEventListeners.addElement(l);
	}
	
	
	/**
	 * Remove a DSAdminEvent listener from this server object.
	 */
	public void removeDSAdminEventListener(IDSAdminEventListener l) {
		_dsAdminEventListeners.removeElement(l);
	}
	
	/**
	 * Fire a DSAdminEvent to all the listeners.
	 */
	public void fireDSAdminEvent(int id) {
		DSAdminEvent evt = new DSAdminEvent(this, id);
		Enumeration e = _dsAdminEventListeners.elements();
		while (e.hasMoreElements()) {
			IDSAdminEventListener l = (IDSAdminEventListener)e.nextElement();
			l.processDSAdminEvent(evt);
		}
	}
	
	/**
	 *	Initialize the page with global information.
	 *
	 * @param info	global information.
	 */
	public void initialize(ConsoleInfo info) {
		Debug.println(5, "DSAdmin.initialize(): _removed=" + _removed +
					  " info=" + info + " _info=" + _info);

		if (!checkVersion(info)) {
			return;
		}
		
		_info = info;
		_info.setLDAPConnection(
			(LDAPConnection)info.getLDAPConnection().clone());
		DSUtil.setConfigConnection(_info.getLDAPConnection());
		_icImage = DSUtil.getPackageImage( _imageName );
		_removed = false; // reset if initialized
		resource = DSUtil._resource;
		getInfo( info.getCurrentDN());
		super.initialize( info );
		Debug.println(9, "DSAdmin.initialize(): _nodeDataTable=" + _nodeDataTable);
		Debug.println(9, "DSAdmin.initialize(): serverProductName=" +
					  _nodeDataTable.get("serverProductName"));
		Debug.println(9, "DSAdmin.initialize(): cn=" +
					  _nodeDataTable.get("cn"));
		Debug.println(9, "DSAdmin.initialize(): name=" + getName());
	}

    protected Vector initializeNodeDataVector(String dataKeys[]) {
		Vector v = super.initializeNodeDataVector(dataKeys);
		if ( _serverInfo != null ) {
			int port = _serverInfo.getPort();
			if ( port > 0 ) {
				String label = resource.getString("dsAdmin", "nsServerPort");
				v.addElement(new NodeData("nsServerPort",
										  label,
										  Integer.toString(port)));
			}
		}
		return v;
	}

	/**
	 * Extract a single string
	 *
	 * @param entry A Directory entry
	 * @param name Name of attribute to fetch
	 * @return A concatenated string
	 */
	static public String getAttrVal( LDAPEntry entry, String name ) {
		LDAPAttribute findAttr =
			entry.getAttribute( name, LDAPUtil.getLDAPAttributeLocale() );
		if ( findAttr != null ) {
//			Debug.println(9,  "Attribute " + name + " = " + findAttr );
			return LDAPUtil.flatting(findAttr);
		}
		Debug.println( "Attribute " + name + " not found in " +
			entry.getDN() );
		return null;
	}

	/**
	 *	get the attribute information to display in the information panel
	 *
	 * @param sDN	DN for the entry.
	 */
	private void getInfo(String sDN) {
		String host = null;
		int port = 0;
		int securePort = 0;
		String baseDN = null;
		String	sServerID = null;			  // Instance of DS server
		try {
			LDAPConnection ldc = _info.getLDAPConnection();
			if ( ldc == null ) {
				Debug.println( "DSAdmin.getInfo: No LDAP connection " +
							   "in _info" );
				return;
			}
			Debug.println(9,  "DSAdmin.getInfo: Fetching " + sDN + " from " +
						   _info.getHost() + ":" + _info.getPort() );
			LDAPEntry entry = ldc.read( sDN );

			host = getAttrVal( entry, "serverHostName" );
			String sPort = getAttrVal(entry, "nsServerPort");
			baseDN = getAttrVal( entry, "nsBaseDN" );
			_bindDN = getAttrVal( entry, "nsBindDN" );
			sServerID = getAttrVal( entry, "nsServerID" );

			if ( (host == null) || (sPort == null) ||
				 (baseDN == null) || (sServerID == null) ) {
				return;
			}
			try {
				port = Integer.parseInt( sPort );
			} catch (Exception e) {
				Debug.println( "DSAdmin.getInfo: Invalid nsServerPort - " +
							   sPort );
				return;
			}
			
			/* Check if we should use SSL */
			String sSSLPort = getAttrVal( entry, "nsServerSecurity" );
			if ( (sSSLPort != null) && (sSSLPort.equalsIgnoreCase("on")) ) {
				sSSLPort = getAttrVal( entry, "nsSecureServerPort" );
				try {
					securePort = Integer.parseInt( sSSLPort );
					Debug.println( "DSAdmin.getInfo: using secure port " +
								   securePort );
				} catch (Exception e) {
					Debug.println( "DSAdmin.getInfo: Invalid " +
								   "nsSecureServerPort - " +
								   sSSLPort );
					securePort = 0;
				}
			}

			/* get the config admin user DN */
			String filter = "(uid=*)";
			String[] attrs = { "uid" };
			String[] rdns = LDAPDN.explodeDN(sDN, false);
			String base = null;
			if (rdns != null) {
				base = rdns[rdns.length-1]; // something like o=NetscapeRoot
			} else {
				base = LDAPUtil.getConfigurationRoot();
			}
			LDAPSearchResults search_results =
				ldc.search(base, ldc.SCOPE_SUB, filter, attrs, false);
			if ((search_results != null) && search_results.hasMoreElements() &&
				((entry = (LDAPEntry)search_results.nextElement()) != null)) {
				_configAdminDN = entry.getDN();
				Debug.println(9, "DSAdmin.getInfo: configAdminDN=" + _configAdminDN);
			}
		} catch( Exception e) {
			Debug.println( "DSAdmin.getInfo: Fetching " + sDN + " from " +
						   _info.getHost() + ":" + _info.getPort() +
						   ", " + e );
		}
		/* Need own info object for this server */
		_serverInfo = (ConsoleInfo)_info.clone();
		_serverInfo.setHost( host );
		_serverInfo.setBaseDN( baseDN );
//		_serverInfo.setAuthenticationDN( bindDN );
		if ( !host.equalsIgnoreCase( _info.getHost() ) ) {
			/* Check if this is the same host without the domain part */
			try {
				String canon1 = DSUtil.canonicalHost( host );
				String canon2 = DSUtil.canonicalHost( _info.getHost() );
				if ( (canon1 != null) && (canon2 != null) &&
					 (canon1.equalsIgnoreCase( canon2 )) ) {
					host = canon1;
					_info.setHost( host );
					Debug.println(9,  "DSAdmin.getInfo: setting console " +
								   "host to " + host );
				}
			} catch ( Exception e ) {
			}
		}
		/* If this server instance is the topology instance, clone the
		   connection */
		boolean canClone = host.equalsIgnoreCase(_info.getHost());
		if ( securePort == 0 )
			canClone = canClone && (port == _info.getPort());
		else
			canClone = canClone && (securePort == _info.getPort());
		if ( canClone ) {
			Debug.println( "DSAdmin.getInfo: cloning console connection" );
			_serverInfo.setLDAPConnection(
				(LDAPConnection)_info.getLDAPConnection().clone() );
		} else {
			/* Not the same instance as the topology */
			if ( securePort == 0 ) {
				/* Non-secure connection */
				_serverInfo.setPort( port );
				_serverInfo.setLDAPConnection(
					DSUtil.makeLDAPConnection( false ) );
				Debug.println("DSAdmin.getInfo: created an LDAP connection to " +
						_serverInfo.getHost() + ":" + _serverInfo.getPort());
			} else {
				/* Secure connection */
				_serverInfo.setPort( securePort );
				_serverInfo.setLDAPConnection(
					DSUtil.makeLDAPConnection( true ) );
				Debug.println("DSAdmin.getInfo: created an LDAPS connection to " +
					_serverInfo.getHost() + ":" + _serverInfo.getPort());
			}
		}

		/* If the auth DN for this server is the same as for the console,
		   try assuming the same password; otherwise require new one */
/*
		if ( !bindDN.equalsIgnoreCase( _info.getAuthenticationDN() ) ) {
			_serverInfo.setAuthenticationPassword( "" );
		}
*/    
		Debug.println( "DSAdmin.getInfo: setting current DN to " +
					   _info.getCurrentDN() );

		/* Set up authentication for CGIs */
		_serverInfo.put( GlobalConstants.TASKS_AUTH_DN,
						 _info.getAuthenticationDN() );
		_serverInfo.put( GlobalConstants.TASKS_AUTH_PWD,
						 _info.getAuthenticationPassword() );
		
		/* Save the instance name */
		_serverInfo.put( "ServerInstance", sServerID );

		/* For the key and cert wizard */
		_serverInfo.put( "SIE", sServerID );

		Debug.println(9,  "DSAdmin.getInfo: OS = " + _info.getAdminOS() );

/*
		Debug.println( "Console info:" );
		DSUtil.dumpConsoleInfo( _info );
		Debug.println( "Server info:" );
		DSUtil.dumpConsoleInfo( _serverInfo );
*/
		Debug.println(9,  "DSAdmin.getInfo: admin URL = " +
					   _serverInfo.getAdminURL() );
		UtilConsoleGlobals.setAdminURL( _serverInfo.getAdminURL() );
		Debug.println(9,  "DSAdmin.getInfo initialized for " +
					   _info.getCurrentDN() );
		setShowServerStatus( true );
		_initialized = true;
	}

	/**
	 * Return connection info for a server instance.
	 * @return Connection info for a server instance.
	 */
	public ConsoleInfo getServerInfo() {
		return _serverInfo;
	}

	/**
	 * Returns the global console info.
	 *
	 * @return Global console info reference.
	 **/
	public ConsoleInfo getConsoleInfo() {
		return _info;
	}

	/**
	 * This function is called when the directory server is deselected
	 * on the topology view.
	 */
	public void unselect(IPage viewInstance) {
//		Debug.println(9,  "DSAdmin unselect" );
		super.unselect(viewInstance);
		fireRemoveMenuItems( viewInstance, this );
	}

	/**
	 * This function is called when the directory server is selected
	 * on the topology view.
	 */
	public void select(IPage viewInstance) {
		Debug.println("DSAdmin.select(): _removed=" + _removed +
					  " viewInstance=" + viewInstance + " _info=" + _info);
		if (_removed)
			return;

		super.select(viewInstance); // sets _viewInstance used
									// by getViewInstance()
		Debug.println(9,  "DSAdmin.select(): viewInstance =" +
					   getViewInstance() );
		fireAddMenuItems( viewInstance, this );
		if ( getServerStatus() == STATUS_STARTED ) {
			fireDisableMenuItem( viewInstance, START );
		} else {
			fireDisableMenuItem( viewInstance, STOP );
		}
	}

						 
    /**
      * Returns supported menu categories
      */
	public String[] getMenuCategoryIDs() {
		return new String[] 
		{
			ResourcePage.MENU_CONTEXT,
			ResourcePage.MENU_OBJECT
		};
	}

	/**
	 * Add menu items for this page.
	 *
	 * @param category Which menu
	 */
	public IMenuItem[] getMenuItems(String category) {
		/* Same for both CONTEXT and OBJECT menus */
		return new IMenuItem[] {
			new MenuItemText( START,
							  resource.getString("menu", "start"),
							  resource.getString("menu",
												  "start-description")),
			new MenuItemText( STOP,
							  resource.getString("menu", "stop"),
							  resource.getString("menu",
												  "stop-description")),
			new MenuItemSeparator() };
	}

    /**
      * Notification that a menu item has been selected.
      */
	public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
		if (item.getID().equals(START)) {
			ConsoleInfo info = (ConsoleInfo)getServerInfo();
			/* Fire off the Start task */
			Start task = new Start();
			task.setConsoleInfo( info );
			boolean status = task.run( viewInstance );
			if( status ) {
				try {
					LDAPConnection ldc = info.getLDAPConnection();
					ldc.connect( 3, info.getHost(), info.getPort(),
								 info.getAuthenticationDN(),
								 info.getAuthenticationPassword() );
					DSUtil.setDefaultReferralCredentials( ldc );
				} catch ( LDAPException ex ) {
				}
				fireEnableMenuItem( viewInstance, STOP );
				fireDisableMenuItem( viewInstance, START );
			}
		} else if(item.getID().equals(STOP)) {
			Stop task = new Stop();
			ConsoleInfo info = (ConsoleInfo)getServerInfo().clone();
			task.setConsoleInfo( info );
			boolean status = task.run( viewInstance );
			if ( status ) {
				fireEnableMenuItem( viewInstance, START );
				fireDisableMenuItem( viewInstance, STOP );
			}
		} 
	}

	/**
	 * Called when authentication changes.
	 *
	 * @param oldAuth Previous authentication DN.
	 * @param newAuth New authentication DN.
	 */
	public void authenticationChanged( String oldAuth,
									   String newAuth,
									   String oldPassword,
									   String newPassword ) {
		Debug.println("DSAdmin.authenticationChanged(): new bind DN = " +
					  newAuth + " old bind DN = " + oldAuth);
		_serverInfo.setAuthenticationDN( newAuth );
		_serverInfo.setAuthenticationPassword( newPassword );
		updateTitle();
		LDAPConnection ldc = _serverInfo.getLDAPConnection();
		if (!newAuth.equalsIgnoreCase(ldc.getAuthenticationDN()) ||
			!newPassword.equals(ldc.getAuthenticationPassword())) {
			try {
				Debug.println(5, "DSAdmin.authenticationChanged(): newAuth " +
							  newAuth + " and/or password are not the same " +
							  " as " + ldc.getAuthenticationDN());
				if (ldc.isConnected()) {
					Debug.println(5, "DSAdmin.authenticationChanged(): ldc " +
								  "is already connected, reauthenticating");
					ldc.authenticate(3, newAuth, newPassword);
				} else {
					Debug.println(5, "DSAdmin.authenticationChanged(): ldc " +
								  "is not connected, connecting");
					DSUtil.reconnect(ldc);
				}
			} catch (LDAPException lde) {
				Debug.println("DSAdmin.authenticationChanged(): error: could " +
					"not connect ldc " + DSUtil.format(ldc) + ":" + lde);
			}
		} 		
	}

	/**
	 * Set the title bar from the server info.
	 *
	 **/
	public void updateTitle () {
		if ( _authid != null ) {
			String text = "";
			if ( _authButton.isSelected() ) {
				text = resource.getString(
					"statusbar", "auth", _serverInfo.getAuthenticationDN());
				_authButton.setToolTipText( null );
			} else {
				_authButton.setToolTipText(
					_serverInfo.getAuthenticationDN() );
			}
			if ( _animate ) {
				_animText = DSUtil.getTTString() + "....";
// 				_animText = DSUtil.getTTString() + "...." +
// 					_animText;
// 				_animText =
// 					_animText.substring( 0,Math.min(100,_animText.length()) );
				text = _animText;
			}
			_authid.setState( text );
			if ( _framework != null ) {
				_framework.changeStatusItemState(_authid);
			}
		}
		if ( (_framework != null) && !_animate ) {
			String id = (String)_serverInfo.get( "ServerInstance" );
			int i = id.indexOf( '-' );
			if ( (i > 0) && (i < (id.length()-1)) )
				id = id.substring( i + 1 );
			_framework.setTitle( _serverInfo.getHost() + " - " +
								 DSUtil._skinResource.getString("dialog", "configtitle") +
								 " - " + id );
		}
	}

    private boolean startServer( IPage viewInstance, String host, int port, boolean prompt ) {
		JFrame frame = viewInstance.getFramework().getJFrame();
		LDAPConnection ldc = getServerInfo().getLDAPConnection();
		Debug.println("DSAdmin.startServer(): begin");
		synchronized (ldc) {
			/* Make sure the connection is in the disconnected state */
			try {
				ldc.disconnect();
			} catch ( Exception ex ) {
			}
			int response = JOptionPane.YES_OPTION;
			if ( prompt ) {
				String[] args = { host, Integer.toString( port ) };
				response = DSUtil.showConfirmationDialog(
					frame,
					"start-server",
					args,
					"general" );
			}
			if ( response == JOptionPane.YES_OPTION ) {
				/* Fire off the Start task */
				Start task = new Start();
				ConsoleInfo info = (ConsoleInfo)_serverInfo.clone();
				info.setHost( host );
				info.setPort( port );
				info.setLDAPConnection( ldc );
				task.setConsoleInfo( info );
				boolean status = task.run( viewInstance );
				if( status ) {
					try {
						ldc.connect( host, port );
						Debug.println("DSAdmin.startServer(): end true");
						return true;
					} catch ( LDAPException ex ) {
					}
				}
			}
		}
		Debug.println("DSAdmin.startServer(): end false");
		return false;
	}

	class AuthPanel extends JPanel implements IStatusItem {
		AuthPanel() {
			super();
			setLayout( new GridBagLayout() );
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets( 0, 0, 0, 0 );
			gbc.gridwidth = gbc.RELATIVE;
			add( _authButton, gbc );
			gbc.gridwidth = gbc.REMAINDER;
			gbc.insets.left = 6;
			add( _authid, gbc );
		}
		public String getID() { return "AUTHPANEL"; }
		public Component getComponent() { return this; }
		public void setState( Object state ) {
			if ( state instanceof Boolean ) {
				_authButton.setSelected( ((Boolean)state).booleanValue() );
			}
		}
		public Object getState() {
			return new Boolean( _authButton.isSelected() );
		}
		public boolean isOpaque() { return false; }
	}

	/**
	 * This function is called when the server is double clicked on
	 * the topology view.
	 */
	public boolean run(IPage viewInstance) {
		JFrame frame = (viewInstance != null) ?
			viewInstance.getFramework().getJFrame() :
			UtilConsoleGlobals.getActivatedFrame();
		
		if (_removed) // possible race condition here?
			return false;

		if ( !_initialized )
			return false;

		if (!checkVersion(_info)) {
			return false;
		}

		Debug.println("DSAdmin.run: begin for server " + _serverInfo);
		String host = _serverInfo.getHost();
		int port = _serverInfo.getPort();

		/* First make sure we're connected */
		LDAPConnection ldc = _serverInfo.getLDAPConnection();
		if ( ldc == null ) {
			System.err.println( "DSAdmin.run: null LDAPConnection" );
			return false;
		}

		boolean serverDown = false;
		synchronized (ldc) {
			if ( !ldc.isConnected() ) {
				try {
					ldc.connect( host, port );
				} catch ( LDAPException e ) {
					Debug.println("DSAdmin.run: connecting to " + host + ":" +
								  port + ", " + e);
					serverDown = ( (e.getLDAPResultCode() == e.UNAVAILABLE) ||
								   (e.getLDAPResultCode() == e.SERVER_DOWN) );
				}
			}
			/* Don't bother with the DHCP case; it's not supported by
			   Kingpin anyway */
			//if ( !ldc.isConnected() ) {
			if ( false ) {
				String shortHost = host;
				try {
					/* Try scrapping the domain part of the host name, in case
					   the host is on DHCP */
					int dot = host.indexOf( '.' );
					if ( dot > 0 ) {
						shortHost = host.substring( 0, dot );
						Debug.println("DSAdmin.run: Trying " + shortHost );
						ldc.connect( shortHost, port );
						host = shortHost;
					}
				} catch ( LDAPException e ) {
					Debug.println("DSAdmin.run: connecting to " + shortHost +
								  ":" + port + ", " + e);
					serverDown = ( (e.getLDAPResultCode() == e.UNAVAILABLE) ||
							   (e.getLDAPResultCode() == e.SERVER_DOWN) );
				}
			}
		}
		/* If the server appears to be down, ask if we should try
		   to start it */
		if ( serverDown && !startServer( viewInstance, host, port, true ) ) {
			return false;
		}

		/*
		  Need to try to authenticate to save some trouble.  We run into problems
		  if you log into the console as some user, then try to bring up the
		  directory console for a directory server that is not the config directory
		  server.  We try to use the same bind credentials, but the bind DN we're
		  using to talk to the config directory may not exist on the target
		  directory server, or may have different credentials.  So, in order to
		  avoid a lot of "No Such Object" error messages and other related error
		  messages, we offer the user a chance to reauthenticate here.  If the
		  user Cancels, the user will need to reauthenticate using the directory
		  console
		*/
		try {
			String authDN = ldc.getAuthenticationDN();
			String authPassword = ldc.getAuthenticationPassword();
			if (authDN == null) {
				authDN = _info.getLDAPConnection().getAuthenticationDN();
			}
			if (authPassword == null) {
				authPassword =
					_info.getLDAPConnection().getAuthenticationPassword();
			}
			ldc.authenticate(3, authDN, authPassword);
		} catch (LDAPException authEx) {
			// the authentication failed using the default credentials, which are
			// usually the same as the credentials used to log into the console
			// if the credentials are the config admin, default to the directory
			// manager, otherwise use the config admin DN
			String newAuthDN = _configAdminDN;
			if ((newAuthDN == null) ||
				DSUtil.equalDNs(newAuthDN, ldc.getAuthenticationDN())) {
				newAuthDN = _bindDN;
			}
			boolean status =
				DSUtil.reauthenticate(ldc,
									  frame,
									  null /* Vector listeners */,
                                      newAuthDN, null);
			if (!status) { // user cancelled reauthentication
				Debug.println("DSAdmin.run: user cancelled reauthentication " +
							  "to server " + DSUtil.format(ldc) + " after " +
							  "trying to use auth DN " + newAuthDN);
				return false;
			}
			_serverInfo.setAuthenticationDN(ldc.getAuthenticationDN());
			_serverInfo.setAuthenticationPassword(ldc.getAuthenticationPassword());
		}

		/* Initialize DSUtil */
		/* ??? This is probably wrong; shouldn't be sharing these
		   things statically across server instances */
		DSUtil.initialize( ldc, _serverInfo.getAuthenticationDN() );

		// create new frame
		_serverInfo.frame = null;
		_pagefeeder = new DSPageFeeder( this, _info, _serverInfo );
		_framework = new DSFramework( this, _pagefeeder );
		_framework.setHelpDirectory( DSUtil.HELP_DIRECTORY );
		_framework.getAccessibleContext().setAccessibleDescription(DSUtil._skinResource.getString("dsAdmin",
                                                                                              "framework-description"));
		_pagefeeder.setFrame( _framework );
		_pagefeeder.addPreferencesTab(new DSPreferencesTab());

		_authid = new StatusItemText( "authid", "" );
		/* Try to figure out what color to make status text, based on
		   Kingpin preferences. This is awful, but Kingpin doesn't
		   expose a straight-forward way to figure out what color to use.
		   Kingpin does white if the banner image is displayed, otherwise
		   black.
		*/
		PreferenceManager preferenceManager =
			PreferenceManager.getPreferenceManager(
				Console.IDENTIFIER, Console.VERSION );
		Preferences p =
			preferenceManager.getPreferences( Framework.PREFERENCES_GENERAL );
		boolean banner =
			p.getBoolean( Framework.PREFERENCE_SHOW_BANNER, true );
		_authid.setForeground( banner ? Color.white : Color.black );
		_authid.setFont( UIManager.getFont("Status.font"));
		_authButton =
			new StatusItemImageButton( "AUTH_BUTTON-"+toString(),
									   DSUtil.getPackageImage(AUTHUP),
									   DSUtil.getPackageImage(AUTHDOWN) );
		_authButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ( !_animate ) {
					updateTitle();
				}
			}
		});
		_authButton.addMouseListener( new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				int mods = e.getModifiers();
				int check = e.SHIFT_MASK | e.CTRL_MASK;
				boolean animate = ( (mods & check) != 0 );
				setAnimation( animate );
			}
		});
		final boolean kingpinNeedsPanel = true;
		if ( kingpinNeedsPanel ) {
			_framework.addStatusItem( new AuthPanel(), IStatusItem.LEFTFIRST );
		} else {
			_framework.addStatusItem( _authButton, IStatusItem.LEFTFIRST );
			_framework.addStatusItem( new StatusItemSpacer("space"),
									  IStatusItem.LEFT );
			_framework.addStatusItem( _authid, IStatusItem.LEFT );
		}

		updateTitle();

		return true; // TODO: check for multiple selection,
	}

	public boolean run(IPage viewInstance, IResourceObject selectionList[]) {
		return run( viewInstance );
	}

	private void setAnimation( boolean animate ) {
		if ( animate ) {
			startUpdater();
		} else {
			stopUpdater();
		}
	}


    private void startUpdater() {
		_animate = true;
		if ( _updater == null ) {
			_updater = new Thread() {
				public void run() {
					while( _animate ) {
						updateTitle();
						try {
							sleep( 2000 );
						} catch ( InterruptedException ex ) {
						}
					}
					_updater = null;
				}
			};
			_updater.start();
		}
	}

    private void stopUpdater() {
		_animate = false;
	}

	/**
	 *	perform the specified action. The command string is specified either
	 *	from the content menu or the menu bar.
	 *
	 * @param command	Command String
	 */
	public void performAction(String command) {
		// Act on any messages to this object
		if (command.equalsIgnoreCase("acleditor")) {
			// start ACL Editor
			ConsoleInfo info = _info;
			info.setAclDN(_info.getCurrentDN());
			info.setUserGroupDN("");

			DSACLEditor ed = new DSACLEditor(info);
			ed.show();
		}
	}

	/**
	 *	Return the server icon.
	 *
	 * @return The Directory Server icon.
	 */
	public Icon getIcon() {
		return _icImage;
	}

	/**
	 *	Return the current status of the server (running or not).
	 *
	 * @return The Directory Server status.
	 */
	public int getServerStatus() {
		if (_removed)
			return STATUS_STOPPED;


		int status = DSUtil.checkServerStatus(_serverInfo);
		boolean state = (status == STATUS_STARTED);

		if (getViewInstance() != null) {
			fireDisableMenuItem( getViewInstance(), state ? START : STOP );
			fireEnableMenuItem( getViewInstance(), state ? STOP : START );
		}
		Debug.println(7, "DSAdmin.getServerStatus(): end state = " + state);
		return status;
	}

	/**
	 * Get the schema of the Directory instance
	 *
	 * @return A reference to a schema object.
	 */
	public LDAPSchema getSchema() {
		return DSUtil.getSchema( _serverInfo );
	}

	/**
	 * Sets a reference to the schema of the Directory instance.
	 *
	 * @param schema A reference to a schema object.
	 */
	synchronized public void setSchema( LDAPSchema schema ) {
		DSUtil.setSchema( _serverInfo, schema );
	}

	/**
	 * Debugging utility method: dump the contents of the global console info.
	 */
	public void dumpConsoleInfo() {
		DSUtil.dumpConsoleInfo( _info );
	}

	/**
	 * Debugging utility method: dump the contents of the global server info.
	 */
	public void dumpServerInfo() {
		DSUtil.dumpConsoleInfo( _serverInfo );
	}
	
	
	public boolean isCloningEnabled() {
		return false;
	}

	/**
	 * The concrete class implementing this method will clone its
	 * configuration from the reference server. This supports using the
	 * GET method for cloning the server.
	 *
	 * @param  sourceDN - DN of server to clone from.
	 */
	public void cloneFrom(String sourceDN) {
		JFrame frame = UtilConsoleGlobals.getActivatedFrame();
		boolean status = CloneServer.cloneServer(_info.getLDAPConnection(),
												 sourceDN,
												 _info.getCurrentDN(),
												 frame);
	}

	/**
	 * Implements the IRemovableServerObject interface.
	 *
	 * @return	true if the server was successfully removed, false otherwise
	 */
	public boolean removeServer() {
		JFrame frame = UtilConsoleGlobals.getActivatedFrame();
		Debug.println("DSAdmin.removeServer: Removing server:");
		_removed = true; // so we don't try to get server status
		dumpConsoleInfo();

		boolean status = false;

		// execute the CGI to stop and remove the server instance
		CGITask task = new Remove();
		Debug.println(9, "DSAdmin.removeServer: remove task=" + task);		
		ConsoleInfo taskConsoleInfo = (ConsoleInfo)_serverInfo.clone();
				task.setConsoleInfo(taskConsoleInfo);
		status = task.run (null );

		String instance = (String)_serverInfo.get("ServerInstance");
		if (instance == null)
			instance = "";
		String[] args = {instance, ""};

		// if the CGI succeeded, remove the topology information.
		// If it didn't, return.
		// This API should be provided by the Admin team.
		// remove the SIE		
		if (!status) {
			_removed = false;
			Debug.println( "DSAdmin:removeServer <" + instance + ">");
			args[1] = resource.getString("dsAdmin", "remove-server-cgi-failed");
			DSUtil.showErrorDialog(frame, "removeinstance",
								   args);
			return false;
		}

		LDAPConnection ldc = _info.getLDAPConnection(); 
		String[] attrs = { "*", "numsubordinates" };
		String sieDN = _info.getCurrentDN();
		Debug.println ("DSAdmin:removeServer:sieDN:" + sieDN);
		LDAPEntry sieEntry = null;
		try {
			sieEntry = ldc.read( sieDN, attrs );
		} catch ( LDAPException ex ) {
			Debug.println("DSAdmin:removeServer <" + sieDN + "> " + ex);
			// Ignore the no such object error; sieEntry is already removed
			if ( ex.getLDAPResultCode() == ex.NO_SUCH_OBJECT ) {
				status = true;
			} else {
				args[1] = ex.toString();
				DSUtil.showErrorDialog(frame, "removeinstance", args);
				_removed = false; // problem reading sieEntry
				return false;
			}
		} catch ( Exception ex ) {
			Debug.println("DSAdmin:removeServer <" + sieDN + "> " + ex);
			args[1] = ex.toString();
			DSUtil.showErrorDialog(frame, "removeinstance", args);
			_removed = false; // problem reading sieEntry
			return false;
		}
		if ( sieEntry != null )	{
			try {
				status = delete_sieTree(sieEntry );
			} catch (Exception ex ) {
				Debug.println( "DSAdmin:removeServer:Unable to delete the " +
							   "tree");
				args[1] = ex.toString();
				DSUtil.showErrorDialog(frame, "removesie",
									   args);
				_removed = false; // remove failed
				return false;
			}
			// Now we need to remove the reference of this server
			status = remove_serverInstance(sieDN);
		}	
		if ( status == false ) {
			args[1] = "";
			DSUtil.showErrorDialog(frame, "removesie",
								   args);
		} else {
			DSUtil.showInformationDialog(frame, "121",
										 (String)null) ;
		}

		if (!status)
			_removed = false;

		return status;
	}
	
	private boolean delete_sieTree (LDAPEntry entry ) 
			throws LDAPException {

		LDAPConnection ldc = _info.getLDAPConnection();
		boolean ret = false;
		
		String dn = entry.getDN();
		if ( DSContentModel.entryHasChildren( entry ) ) {
			LDAPSearchResults search_results = null;
			String[] attrs = { "numsubordinates" };
			search_results = ldc.search( dn,
					LDAPConnection.SCOPE_ONE,
						"(objectClass=*)", attrs, false );

			while ( search_results.hasMoreElements() ) {
				/* Get the next child */
				LDAPEntry child_entry =
						(LDAPEntry)search_results.nextElement();
				ret = delete_sieTree( child_entry );

			}
		}
		ldc.delete(dn);
		return true;
	}
	private boolean remove_serverInstance (String sieDN )  {

		LDAPSearchResults search_results = null;
		String baseDN =(String)	 _info.get("BaseDN");
		LDAPConnection ldc = _info.getLDAPConnection();
		String[] attrs = { "*", "uniquemember" };
		String filter = "(&(objectclass=groupOfUniquenames)(uniquemember=" +
						sieDN+"))";
	
		try {
			search_results = ldc.search( baseDN, ldc.SCOPE_SUB,
						filter, attrs, false);
		} catch (LDAPException e) {
			Debug.println( "Failed to search - " + e.toString() );
			return false;
		}
		LDAPEntry entry = null;
		while ( search_results.hasMoreElements() ) {
			// need to remove the reference to the sieDN from
			// this entry.
			
			entry = (LDAPEntry)search_results.nextElement();
			String eDN = (String) entry.getDN();
			// Now we need to modify the entry to delete the
			// reference to the serevr.
			remove_instanceFromEntry(ldc, eDN, sieDN);
		}
		return true;
	}
	private boolean remove_instanceFromEntry ( LDAPConnection ldc,
						String eDN, String sieDN )	{

		LDAPModificationSet mods = new LDAPModificationSet();
		LDAPAttribute attUmember = new LDAPAttribute("uniquemember", sieDN);
		Debug.println(9, "DSAdmin:remove_instanceFromEntry: Modifying entry:" +
					   eDN);
		mods.add( LDAPModification.DELETE, attUmember );	
		try {
			ldc.modify(eDN, mods );
		} catch ( LDAPException e ) {
			Debug.println ( "Modifying " + eDN + ", " + e);
			return false;
		}
		return true;
	}

	/*
	 * Convenience method to get an LDAP connection to a server whose
	 * information is stored as an entry in another (kingpin) server.
	 */
	static public LDAPConnection getLDAPConnection(LDAPConnection ldc,
												   String dn,
												   String hostNameAttr,
												   String portAttr,
												   Hashtable otherAttrs)
		                          throws LDAPException, Exception {
		LDAPEntry entry = null;

		// construct a list of attributes to get the values of
		String[] attrlist = new String[otherAttrs.size() + 4];
		int index = 0;
		attrlist[index++] = hostNameAttr;
		attrlist[index++] = portAttr;
		attrlist[index++] = "nsServerSecurity";
		attrlist[index++] = "nsSecureServerPort";
		for (Enumeration e = otherAttrs.keys(); e.hasMoreElements();) {
			attrlist[index++] = (String)e.nextElement();
		}

		// read the attributes from the entry
		entry = ldc.read(dn, attrlist);

		// fill in the user's hashtable
		otherAttrs.put(hostNameAttr, getAttrVal(entry, hostNameAttr));
		otherAttrs.put(portAttr, getAttrVal(entry, portAttr));
		for (Enumeration e = otherAttrs.keys(); e.hasMoreElements();) {
			String key = (String)e.nextElement();
			otherAttrs.put(key, getAttrVal(entry, key));
		}

		// get the host and port to connect to
		String host = getAttrVal(entry, hostNameAttr);
		int port = -1;
		int secureport = -1;
		String serverSecurity = getAttrVal(entry, "nsServerSecurity");
		if (serverSecurity != null &&
			serverSecurity.equalsIgnoreCase("on")) {
			// Check if there is a secure port
			try {
				secureport = Integer.parseInt(getAttrVal(entry,
											  "nsSecureServerPort"));
			} catch (Exception e) {
				secureport = -1;
			}
		}

		if (secureport < 0) {
			// No secure port, get the regular one
			try {
				port = Integer.parseInt(getAttrVal(entry, portAttr));
			} catch (Exception e) {
				port = -1;
			}
		} else {
			port = secureport;
		}

		// create a new connection object
		LDAPConnection newone = DSUtil.makeLDAPConnection(secureport >= 0);
		
		// attempt to connect with the current authentication
		try {
			newone.connect(host, port, ldc.getAuthenticationDN(),
						   ldc.getAuthenticationPassword());
		} catch (Exception e) {
			Debug.println("DSAdmin.getLDAPConnection(): could not " +
						  "connect to " +
						  host + ":" + port + " as dn " +
						  ldc.getAuthenticationDN() + ": will attempt " +
						  "anonymous bind");
			Debug.println(9, "DSAdmin.getLDAPConnection(): exception " + e);
			// couldn't connect, so rebind as anonymous
			newone.connect(host, port, "", "");
		}

		// NOTE that we may have thrown an exception before we get here...
		return newone;
	}

	/**
	 * Note: it would be better if this method were declared in the superclass,
	 * but having it here is better than nothing, since I need access to it
	 * for getServerStatus() . . .
	 */
	private IPage getViewInstance() {
		return _viewInstance;
	}

	private boolean isLocalDirectoryManager() {
		// read the nsslapd-rootdn attribute from cn=config
		// if this fails, we are probably not bound as the root dn
		Debug.println("DSAdmin.isLocalDirectoryManager: begin");
		boolean status = false;
		try {
			LDAPConnection ldc = _serverInfo.getLDAPConnection();
			Debug.println(9, "DSAdmin.isLocalDirectoryManager: ldc=" +
						  DSUtil.format(ldc));
			String[] attrs = { "nsslapd-rootdn" };
			LDAPEntry lde = ldc.read("cn=config", attrs);
			String val = getAttrVal(lde, "nsslapd-rootdn");
			if (val == null || val.length() == 0) {
				throw new LDAPException("No value for rootdn attribute",
										LDAPException.NO_SUCH_OBJECT);
			}
			status = DSUtil.equalDNs(ldc.getAuthenticationDN(), val);
		} catch (Exception e) {
			Debug.println("DSAdmin.isLocalDirectoryManager(): could not read " +
						  "cn=config from " +
						  DSUtil.format(_serverInfo.getLDAPConnection()) +
						  " bound as " + _serverInfo.getAuthenticationDN() +
						  ":" + e);
		}

		Debug.println("DSAdmin.isLocalDirectoryManager: end status = " + status);
		return status;
	}

	private boolean checkVersion(ConsoleInfo info) {
		String version = null;
		JFrame frame = UtilConsoleGlobals.getActivatedFrame();
		version = VersionInfo.getMajorVersionNumber();

		try {
			Float f = new Float(version);
			// Check if the console meets the minimum version requirement
			if ((f != null) && (f.floatValue() >= MINIMUM_VERSION)) {
				return true;
			} else {
				DSUtil.showErrorDialog(frame, "neednewconsole", version);
			}
		} catch (Exception e) { // could not parse version as float
		}

		return false;
	}

	private ConsoleInfo _info;				// global information
	private ConsoleInfo _serverInfo;		// server-specific information
	private DSFramework	_framework = null;	// parent frame
	private DSPageFeeder	_pagefeeder;	// what generates tab views
	private LDAPSchema _schema;				// Schema of DS server
	private int _securityState = SECURITY_UNKNOWN; // Whether SSL is enable or not

	private StatusItemText _authid = null;
	private StatusItemImageButton _authButton = null;
	private RemoteImage _icImage = null;
	private boolean _initialized = false;
	private boolean _animate = false;
	private String _animText = "";
	protected Thread _updater = null;
	protected Vector _dsAdminEventListeners = new Vector();
	
	// Values for the securityState property
	static public int SECURITY_UNKNOWN = 0;
	static public int SECURITY_DISABLE = 1;
	static public int SECURITY_ENABLE  = 2;

	// Minimum required console version
	private static final float MINIMUM_VERSION = 1.1f;

	// Icon for the console
	private static final String _imageName = "directory.gif";
	private static final String AUTH_ICON = "face.gif";
	private static final String AUTHUP = "authup.gif";
	private static final String AUTHDOWN = "authdown.gif";
	private static final String START = "start";
	private static final String STOP = "stop";
	public static ResourceSet resource = null; // Global properties
	private static final String INSTANCE_DN = "cn=config";
	private boolean _removed = false; // set to true if this node
								// is being deleted
	private String _bindDN = "cn=Directory Manager"; // fallback
	private String _configAdminDN = null; // the DN of the Config DS admin


	// When the class is loaded, we set the change indicator
	// in BlankPanel according the preferences.
	static {
		PreferenceManager pm = PreferenceManager.getPreferenceManager(Framework.IDENTIFIER, Framework.VERSION);
		Preferences prefs = pm.getPreferences(DSPreferencesTab.PREFERENCES_GROUP);
		int changeIndicator = prefs.getInt(DSPreferencesTab.PREFERENCES_CHANGE_INDICATOR, 
		                                   BlankPanel.CHANGE_INDICATOR_COLOR);
		BlankPanel.setChangeIndicator(changeIndicator);
	}
}
