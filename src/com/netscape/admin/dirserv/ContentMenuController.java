/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.Event;
import java.awt.Component;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Enumeration;
import javax.swing.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.UITools;
import com.netscape.admin.dirserv.account.AccountInactivation;
import com.netscape.admin.dirserv.task.CreateVLVIndex;
import com.netscape.admin.dirserv.browser.BrowserController;
//FIXME: remove the PWPPanel import if I move the PWP logic to another class
import com.netscape.admin.dirserv.panel.PasswordPolicyPanel;

public class ContentMenuController {
	public ContentMenuController(IContentPageInfo contentPage,
								 JPopupMenu contextMenu,		
								 IFramework framework,
								 DatabaseConfig databaseConfig,							 
								 ActionListener listener) {
		_contentPage = contentPage;
		_contextMenu = contextMenu;		
		_framework = framework;
		_shortcutRegisterers = new Vector(2);
		_databaseConfig = databaseConfig;
		_listener = listener;		

		createShortCutKeys();
	}

	public void addShortCutRegisterer(JComponent registerer) {
		_shortcutRegisterers.addElement(registerer);
	}

	/* Need to be called when we have added or removed a root entry.
	   A call to the reload() method of the DatabaseConfig passed to this object needs to be done 
	   before calling recreateNewRootEntryMenus */
	public void recreateNewRootEntryMenus() {
		unpopulateMenuCategory(CONTEXTNEWROOTENTRY);
		unpopulateMenuCategory(OBJECTNEWROOTENTRY);

		_objectNewRootEntryMenuItems = null;		
		_contextNewRootEntryMenuItems = null;

		populateMenuCategory(CONTEXTNEWROOTENTRY);
		populateMenuCategory(OBJECTNEWROOTENTRY);
		updateMenuState();
	}

	public void recreateDynamicMenus() {
		unpopulateMenuCategory(CONTEXTNEWROOTENTRY);
		unpopulateMenuCategory(OBJECTNEWROOTENTRY);
		unpopulateMenuCategory(PARTITIONVIEW);

		_objectNewRootEntryMenuItems = null;		
		_contextNewRootEntryMenuItems = null;
		_partitionViewMenuItems = null;

		populateMenuCategory(CONTEXTNEWROOTENTRY);
		populateMenuCategory(OBJECTNEWROOTENTRY);
		populateMenuCategory(PARTITIONVIEW);
		updateMenuState();
	}

	/**
	  * Enables/disables the menu items, according to which entry is selected.
	  * It assumes that an object is selected in the tree.
	  */
	public void updateMenuState() {
		boolean isRootSelected = _contentPage.isRootSelected();
		boolean isSelectedNodeRemote = _contentPage.isSelectedNodeRemote();
		boolean isSuffixSelected = _contentPage.isSelectedNodeSuffix();
		boolean isClipboardEmpty = _contentPage.isClipboardEmpty();
		boolean isDnInClipboardEmpty = _contentPage.isDnInClipboardEmpty(); // used by move
		Integer selectionActivationState = new Integer(AccountInactivation.CANNOT_BE_ACTIVATED_INACTIVATED);
		Integer selectionVlvState = new Integer(CreateVLVIndex.CAN_NOT_HAVE_INDEX);
		Integer selectionPWPState = new Integer(PasswordPolicyPanel.NO_PWP);

		if (!isSelectedNodeRemote) {
			selectionPWPState = _contentPage.getSelectionPWPState();
			selectionActivationState = _contentPage.getSelectionActivationState();
			selectionVlvState = _contentPage.getSelectionVlvState();
		}		

		updateNewMenuItems(isRootSelected);
		updateAciMenuItems(isRootSelected);
		updateRoleMenuItems(isRootSelected);
		updateReferralMenuItems(isRootSelected);
		updateMoveCopyPasteMenuItems(isRootSelected,
									isSuffixSelected,
									isClipboardEmpty,
									isDnInClipboardEmpty);
		updateSearchMenuItems(isRootSelected);
		updateActivateInactivateMenuItems(selectionActivationState, isSelectedNodeRemote);
		updateVLVMenuItems(selectionVlvState, isSelectedNodeRemote);
		updatePWPMenuItems(selectionPWPState, isSelectedNodeRemote, isRootSelected, isSuffixSelected);
				
		setEnabledMenuItem(_contextMenuItems, OPEN, true);		
		setEnabledMenuItem(_objectMenuItems, OPEN, true);

		setEnabledMenuItem(_contextMenuItems, ADVANCED_OPEN, true);		
		setEnabledMenuItem(_objectMenuItems, ADVANCED_OPEN, true);

		updateFollowReferralsMenuItem();
        
		/* The refresh in context menu should be always activated */
		setEnabledMenuItem(_contextMenuItems, REFRESHNODE, true);
        
	}

	public void populateMenuItems() {
		String[] menuCategoryIDs = getMenuCategoryIDs();
		for (int i=0; i < menuCategoryIDs.length; i++) {
			populateMenuCategory(menuCategoryIDs[i]);
		}
	}

	public void unpopulateMenuItems() {
		String[] menuCategoryIDs = getMenuCategoryIDs();
		for (int i=menuCategoryIDs.length-1; i >= 0; i--) {
			unpopulateMenuCategory(menuCategoryIDs[i]);
		}
	}

	public void disableMenus() {				
		setEnabledMenuItem(_contextMenuItems, false);
		setEnabledMenuItem(_objectMenuItems, false);
		setEnabledMenuItem(_editMenuItems, false);				
	}

	private void createFileMenuItems() {
		_fileMenuItems = new IMenuItem[] {
			new MenuItemText( AUTHENTICATE,
							  _resource.getString("menu", "authenticate"),
							  _resource.getString("menu",
												  "authenticate-description"),
							  _listener),
				new MenuItemSeparator(),
				};		
		addShortCuts(_fileMenuItems);
		setActionCommand(_fileMenuItems);
	}

	private void createContextMenuItems() {
		_contextMenuItems = new IMenuItem[] {			
			new MenuItemText( OPEN,
							  _resource.getString("menu", "properties"),
							  _resource.getString("menu",
												  "properties-description"),
							  _listener),
				new MenuItemText( ADVANCED_OPEN,
							  _resource.getString("menu", "advanced-properties"),
							  _resource.getString("menu",
												  "advanced-properties-description"),
							  _listener),
				new MenuItemText( SEARCH_UG,
								  _resource.getString("menu", "EditFindUG"),
								  _resource.getString("menu",
													  "EditFindUG-description"),
								  _listener),
				new MenuItemSeparator(),										
				new MenuItemCategory( CONTEXTNEW,
									  _resource.getString("menu",
														  "EditNew")),		
				new MenuItemCategory( CONTEXTNEWROOTENTRY,
									  _resource.getString("menu",
														  "EditNewRootEntry")),
				
				new MenuItemSeparator(),
				new MenuItemText( ACL,
								  _resource.getString("menu", "editacls"),
								  _resource.getString("menu",
													  "editacls-description"),
								  _listener),
				new MenuItemText( ROLES,
								  _resource.getString("menu", "editroles"),
								  _resource.getString("menu",
													  "editroles-description"),
								  _listener),
			    new MenuItemText( SET_REFERRALS,
								  _resource.getString("menu", "editreferrals"),
								  _resource.getString("menu",
													  "editreferrals-description"),
								  _listener),
				new MenuItemSeparator(),
				new MenuItemText( CREATE_VLV_INDEX,
								  _resource.getString("menu", "createIndex"),
								  _resource.getString("menu",
													  "createIndex-description"),
								  _listener),
				new MenuItemText( DELETE_VLV_INDEX,
								  _resource.getString("menu", "deleteIndex"),
								  _resource.getString("menu",
													  "deleteIndex-description"),
								  _listener),
				new MenuItemSeparator(),

				new MenuItemCategory( CONTEXTSETPWP,
									  _resource.getString("menu",
														  "setPWP")),		

				new MenuItemSeparator(),
				new MenuItemText( ACTIVATE,
								  _resource.getString("menu", "activate"),
								  _resource.getString("menu", "activate-description"),
								  _listener),
				new MenuItemText( INACTIVATE,
								  _resource.getString("menu", "inactivate"),
								  _resource.getString("menu", "inactivate-description"),
								  _listener),
				new MenuItemSeparator(),
				new MenuItemText( MOVE,
								  _resource.getString("menu", "EditMove"),
								  _resource.getString("menu",
													  "EditMove"),
								  _listener),
				new MenuItemText( COPY,
								  _resource.getString("menu", "EditCopy"),
								  _resource.getString("menu",
													  "EditCopy-description"),
								  _listener),
				new MenuItemText( PASTE,
								  _resource.getString("menu", "EditPaste"),
								  _resource.getString("menu",
													  "EditPaste-description"),
								  _listener),
				new MenuItemText( MOVEPASTE,
								  _resource.getString("menu", "EditMovePaste"),
								  _resource.getString("menu",
													  "EditMovePaste-description"),
								  _listener),
				new MenuItemText( DELETE,
								  _resource.getString("menu", "EditDelete"),
								  _resource.getString("menu",
													  "EditDelete-description"),
								  _listener),
					new MenuItemSeparator(),
				new MenuItemText( REFRESHNODE,
								  _resource.getString("menu", "refresh"),
									  _resource.getString("menu",
														  "refresh-description"),
								  _listener),
				};
		addShortCuts(_contextMenuItems);
		setActionCommand(_contextMenuItems);
	}

	private void createContextPWPMenuItems() {
		_contextPWPMenuItems =  new IMenuItem[] {
			
			new MenuItemText( SET_PWP_USER,
							  _resource.getString("menu", "setPWP-user"),
							  _resource.getString("menu",
												  "setPWP-user-description"),
							  _listener),
				
				new MenuItemText( SET_PWP_SUBTREE, 
							  _resource.getString("menu", "setPWP-subtree"),
							  _resource.getString("menu",
												  "setPWP-subtree-description"),
							  _listener) };
		addShortCuts(_contextPWPMenuItems);
		setActionCommand(_contextPWPMenuItems);
	}
	
	private void createContextNewMenuItems() {
		_contextNewMenuItems =  new IMenuItem[] {
			
			new MenuItemText( NEW_USER,
							  _resource.getString("menu", "EditNewUser"),
							  _resource.getString("menu",
												  "EditNewUser-description"),
								  _listener),
				
				new MenuItemText( NEW_GROUP, 
								  _resource.getString("menu", "EditNewGroup"),
								  _resource.getString("menu",
													  "EditNewGroup-description"),
								  _listener),
				new MenuItemText( NEW_ORGANIZATIONALUNIT, 
								  _resource.getString("menu", "EditNewOu"),
								  _resource.getString("menu",
													  "EditNewOu-description"),
								  _listener),
				new MenuItemSeparator(),
				new MenuItemText( NEW_ROLE, 
								  _resource.getString("menu", "EditNewRole"),
								  _resource.getString("menu",
													  "EditNewRole-description"),
								  _listener),
				new MenuItemText( NEW_COS, 
								  _resource.getString("menu", "EditNewCos"),
								  _resource.getString("menu",
													  "EditNewCos-description"),
								  _listener),
				new MenuItemSeparator(),
				new MenuItemText( NEW_OBJECT,
								  _resource.getString("menu", "EditNewObject"),
								  _resource.getString("menu",
													  "EditNewObject-description"),
								  _listener),
				};		
		addShortCuts(_contextNewMenuItems);
		setActionCommand(_contextNewMenuItems);
	}

	private void createObjectPWPMenuItems() {
		_objectPWPMenuItems = new IMenuItem[] {

				new MenuItemText( SET_PWP_USER,
					_resource.getString("menu", "setPWP-user"),
					_resource.getString("menu", "setPWP-user-description"),
					_listener),

				new MenuItemText( SET_PWP_SUBTREE,
					_resource.getString("menu", "setPWP-subtree"),
					_resource.getString("menu", "setPWP-subtree-description"),
					_listener) };

		addShortCuts(_objectPWPMenuItems);
		setActionCommand(_objectPWPMenuItems);
	}

	private void createObjectNewMenuItems() {
		_objectNewMenuItems =  new IMenuItem[] {
			
				new MenuItemText( NEW_USER,
								  _resource.getString("menu", "EditNewUser"),
								  _resource.getString("menu",
											  "EditNewUser-description"),
								  _listener),

				new MenuItemText( NEW_GROUP, 
								  _resource.getString("menu", "EditNewGroup"),
								  _resource.getString("menu",
											  "EditNewGroup-description"),
								  _listener),
				new MenuItemText( NEW_ORGANIZATIONALUNIT, 
								  _resource.getString("menu", "EditNewOu"),
								  _resource.getString("menu",
											  "EditNewOu-description"),
								  _listener),
				new MenuItemSeparator(),
				new MenuItemText( NEW_ROLE, 
								  _resource.getString("menu", "EditNewRole"),
								  _resource.getString("menu",
											  "EditNewRole-description"),
								  _listener),
				new MenuItemText( NEW_COS, 
								  _resource.getString("menu", "EditNewCos"),
								  _resource.getString("menu",
											  "EditNewCos-description"),
								  _listener),
				new MenuItemSeparator(),
				new MenuItemText( NEW_OBJECT,
								  _resource.getString("menu", "EditNewObject"),
								  _resource.getString("menu",
											  "EditNewObject-description"),
								  _listener),
					};
		addShortCuts(_objectNewMenuItems);
		setActionCommand(_objectNewMenuItems);
	}

	private void createObjectMenuItems() {
		_objectMenuItems = new IMenuItem[] {				
			new MenuItemText( OPEN,
							  _resource.getString("menu", "properties"),
							  _resource.getString("menu",
												  "properties-description"),
							  _listener),
				new MenuItemText( ADVANCED_OPEN,
							  _resource.getString("menu", "advanced-properties"),
							  _resource.getString("menu",
												  "advanced-properties-description"),
							  _listener),
				new MenuItemText( SEARCH_UG,
								  _resource.getString("menu", "EditFindUG"),
								  _resource.getString("menu",
													  "EditFindUG-description"),
								  _listener),
				new MenuItemSeparator(),
				new MenuItemCategory( OBJECTNEW,
									  _resource.getString("menu",
														  "EditNew")),
				new MenuItemCategory( OBJECTNEWROOTENTRY,
									  _resource.getString("menu",
														  "EditNewRootEntry")),					
				new MenuItemSeparator(),
				new MenuItemText( ACL,
								  _resource.getString("menu", "editacls"),
								  _resource.getString("menu",
													  "editacls-description"),
								  _listener),
				new MenuItemText( ROLES,
								  _resource.getString("menu", "editroles"),
								  _resource.getString("menu",
													  "editroles-description"),
								  _listener),
				new MenuItemText( SET_REFERRALS,
								  _resource.getString("menu", "editreferrals"),
								  _resource.getString("menu",
													  "editreferrals-description"),
								  _listener),
				new MenuItemSeparator(),
				new MenuItemText( CREATE_VLV_INDEX,
								  _resource.getString("menu", "createIndex"),
								  _resource.getString("menu",
													  "createIndex-description"),
								  _listener),
				new MenuItemText( DELETE_VLV_INDEX,
								  _resource.getString("menu", "deleteIndex"),
								  _resource.getString("menu",
													  "deleteIndex-description"),
								  _listener),
				new MenuItemSeparator(),

				new MenuItemCategory( OBJECTSETPWP,
									  _resource.getString("menu",
														  "setPWP")),
				new MenuItemSeparator(),
				new MenuItemText( ACTIVATE,
								  _resource.getString("menu", "activate"),
								  _resource.getString("menu", "activate-description"),
								  _listener),
				new MenuItemText( INACTIVATE,
								  _resource.getString("menu", "inactivate"),
								  _resource.getString("menu", "inactivate-description"),
								  _listener),
				};
		addShortCuts(_objectMenuItems);
		setActionCommand(_objectMenuItems);
	}

	private void createEditMenuItems() {
		_editMenuItems = new IMenuItem[] {
			new MenuItemSeparator(),
				new MenuItemText( MOVE,
								  _resource.getString("menu", "EditMove"),
								  _resource.getString("menu",
													  "EditMove-description"),
								  _listener),
				new MenuItemText( COPY,
								  _resource.getString("menu", "EditCopy"),
								  _resource.getString("menu",
													  "EditCopy-description"),
								  _listener),
				new MenuItemText( PASTE,
								  _resource.getString("menu", "EditPaste"),
								  _resource.getString("menu",
													  "EditPaste-description"),
								  _listener),
				new MenuItemText( 
								  _resource.getString("menu", "EditMovePaste"),
								  _resource.getString("menu",
													  "EditMovePaste-description"),
								  _listener),
				new MenuItemText( DELETE,
								  _resource.getString("menu", "EditDelete"),
								  _resource.getString("menu",
													  "EditDelete-description"),
								  _listener),
			new MenuItemSeparator(),
				new MenuItemText( COPYDN,
								  _resource.getString("menu", "EditCopyDN"),
								  _resource.getString("menu",
													  "EditCopyDN-description"),
								  _listener),
				new MenuItemText( COPYLDAPURL,
								  _resource.getString("menu", "EditCopyLDAPURL"),
								  _resource.getString("menu",
													  "EditCopyLDAPURL-description"),
								  _listener),
				};
		addShortCuts(_editMenuItems);
		setActionCommand(_editMenuItems);
	}

	private void createNewRootEntryMenuItems() {		
		Debug.println("ContentMenuController.createNewRootEntryMenuItems()");
		
		Vector suffixWithNoEntryList = _databaseConfig.getRootSuffixesWithoutEntry();
		Debug.println("ContentMenuController.createNewRootEntryMenuItems() the list is "+suffixWithNoEntryList);
		if (suffixWithNoEntryList != null) {
			int size = suffixWithNoEntryList.size();
			
			_objectNewRootEntryMenuItems = new IMenuItem[size];
			_contextNewRootEntryMenuItems = new IMenuItem[size];
			
			for (int i=0; i< _objectNewRootEntryMenuItems.length ; i++) {
				String suffix = (String)(suffixWithNoEntryList.elementAt(i));
				_objectNewRootEntryMenuItems[i] = (IMenuItem) new MenuItemText((String)(suffix),
																			   (String)(suffix),
																			   _resource.getString("menu",
																								   "EditNewRoot-description",suffix),
																			   _listener);			
				_contextNewRootEntryMenuItems[i] = (IMenuItem) new MenuItemText((String)(suffix),
																				(String)(suffix),
																				_resource.getString("menu",
																									"EditNewRoot-description",suffix),
																				_listener);			
			}
			addShortCuts(_contextNewRootEntryMenuItems);
			setActionCommand(_contextNewRootEntryMenuItems);
			addShortCuts(_objectNewRootEntryMenuItems);
			setActionCommand(_objectNewRootEntryMenuItems);
		}
	}

	private void createLayoutMenuItems() {
		_layoutMenuItems = new IMenuItem[] {	
			new MenuItemRadioButton( NODE_LEAF_LAYOUT,
									 _resource.getString("menu", "nodeleaflayout"),
									 _resource.getString("menu",
														 "nodeleafview-description"),
									 _contentPage.getPanelLayout().equals(NODE_LEAF_LAYOUT),
									 _listener),
				new MenuItemRadioButton( ONLY_TREE_LAYOUT,
										 _resource.getString("menu", "onlytreelayout"),
										 _resource.getString("menu",
															 "onlytreeview-description"),
										 _contentPage.getPanelLayout().equals(ONLY_TREE_LAYOUT),
										 _listener),				
				new MenuItemRadioButton( ATTRIBUTE_LAYOUT,
										 _resource.getString("menu", "attributelayout"),
										 _resource.getString("menu",
															 "attributeview-description"),
										 _contentPage.getPanelLayout().equals(ATTRIBUTE_LAYOUT),
										 _listener)
				};
		addShortCuts(_layoutMenuItems);
		setActionCommand(_layoutMenuItems);

		ButtonGroup group = new ButtonGroup();
		for (int i=0; i<_layoutMenuItems.length; i++) {
			if (_layoutMenuItems[i] instanceof MenuItemRadioButton) {
				group.add((MenuItemRadioButton)_layoutMenuItems[i]);
			}
		}
	}

	private void createDisplayMenuItems() {
		_displayMenuItems = new IMenuItem[] {
			new MenuItemCheckBox( DISPLAY_ACI_COUNT,
								  _resource.getString("menu", "acicount"),
								  _resource.getString("menu",
														 "acicount-description"),
								  _listener,
								  (_contentPage.getDisplay() & BrowserController.DISPLAY_ACI_COUNT) != 0),								  
				new MenuItemCheckBox( DISPLAY_ROLE_COUNT,
									  _resource.getString("menu", "rolecount"),
									  _resource.getString("menu",
														  "rolecount-description"),
									  _listener,
									  (_contentPage.getDisplay() & BrowserController.DISPLAY_ROLE_COUNT) != 0),				
				new MenuItemCheckBox( DISPLAY_ACCOUNT_INACTIVATION,
									  _resource.getString("menu", "viewaccountinactivation"),
									  _resource.getString("menu",
														  "viewaccountinactivation-description"),									  
									  _listener,
									  (_contentPage.getDisplay() & BrowserController.DISPLAY_ACTIVATION_STATE) != 0)									  
				};
		addShortCuts(_displayMenuItems);
		setActionCommand(_displayMenuItems);		
	}

	private void createPartitionViewMenuItems() {		
		Debug.println("ContentMenuController.createPartitionViewMenuItems()");
		Vector ldbmDatabaseList = _databaseConfig.getDatabaseList(DatabaseConfig.LDBM_DATABASES);
		
		IMenuItem[] items = null;
		if ((ldbmDatabaseList != null) && (ldbmDatabaseList.size() > 0)) {
			items = new IMenuItem[ldbmDatabaseList.size() + 2];
		} else {
			items = new IMenuItem[1];
		}
		boolean state = true;						
		
		ButtonGroup group = new ButtonGroup();
				
		String backend = _resource.getString("menu", "PartitionView-all");
		state = _contentPage.getSelectedPartitionView().equals(VIEW_ALL_PARTITIONS);
		items[0] = new MenuItemRadioButton(VIEW_ALL_PARTITIONS,
										   backend,
										   _resource.getString("menu",
															   "PartitionView-all-description",backend),
										   state,
										   _listener);
		group.add((MenuItemRadioButton)items[0]);
		if (items.length > 1) {
			items[1] = new MenuItemSeparator();
		}
		for (int i=2; i< items.length ; i++) {
			backend = (String)ldbmDatabaseList.elementAt(i-2);
			state = _contentPage.getSelectedPartitionView().equals(backend);
			items[i] = new MenuItemRadioButton(backend,
											   backend,
											   _resource.getString("menu",
																   "PartitionView-description",backend),
											   state,
											   _listener);
			group.add((MenuItemRadioButton)items[i]);
		}			
		_partitionViewMenuItems =  (IMenuItem[])items;	
		addShortCuts(_partitionViewMenuItems);
		setActionCommand(_partitionViewMenuItems);
	}

	private void createViewMenuItems() {		
		IMenuItem[] viewMenuItems = new IMenuItem[] {			
			new MenuItemSeparator(),
			new MenuItemCheckBox( FOLLOW_REFERRALS, 
								  _resource.getString( "menu", "referrals" ), 
								  _resource.getString("menu", "referrals-description"),											   
								  _listener,
								  _contentPage.getFollowReferrals()),
				new MenuItemCheckBox( SORT, 
									  _resource.getString( "menu", "sort" ), 
									  _resource.getString("menu", "sort-description"),											   
									  _listener,
									  _contentPage.isSorted()),
				new MenuItemSeparator(),
				new MenuItemCategory( DISPLAY,
									  _resource.getString("menu", "display")),				
				new MenuItemCategory(LAYOUT,
									 _resource.getString( "menu", "layout" )),				
				new MenuItemCategory( PARTITIONVIEW,
									  _resource.getString("menu",
														  "PartitionView") ),
				new MenuItemSeparator(),
				new MenuItemText( REFRESHTREE,
								  _resource.getString("menu", "refresh-all"),
								  _resource.getString("menu",
													  "refresh-description"),
								  _listener),				
				};
		_viewMenuItems = viewMenuItems;
		addShortCuts(_viewMenuItems);
		setActionCommand(_viewMenuItems);
	}	

	protected void addShortCuts(IMenuItem[] menuItems) {
		for (int i=0; i<menuItems.length; i++) {
			if (menuItems[i] instanceof MenuItemText) {
				MenuItemText menuItemText = (MenuItemText)menuItems[i];
				String id = menuItemText.getID();
				if (id != null) {
					KeyStroke stroke = (KeyStroke)_shortCutTable.get(id);
					if (stroke != null) {
						menuItemText.setAccelerator(stroke);
						Enumeration e = _shortcutRegisterers.elements();
						while (e.hasMoreElements()) {
							JComponent register = (JComponent)e.nextElement();
							
							register.registerKeyboardAction(_listener,
															menuItemText.getID(),
															menuItemText.getAccelerator(),
															JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
						}
					}
				}
			}
		}
	}

	protected void setActionCommand(IMenuItem[] items) {
		for (int i=0; i<items.length; i++) {
			if (items[i] instanceof MenuItemText) {
				((MenuItemText)items[i]).setActionCommand(items[i].getID());
			} else if (items[i] instanceof MenuItemCheckBox) {
				((MenuItemCheckBox)items[i]).setActionCommand(items[i].getID());
			}
		}
	}

	private void populateMenuCategory(String categoryID) {
		IMenuItem[] menuItems = getMenuItems(categoryID);
		if (menuItems != null) {
			if (categoryID.equals(MENU_CONTEXT)) {
				Debug.println("ContentMenuController.populateMenuCategory() adding "+categoryID+ " to context menu"); 
				for (int i=0; i<menuItems.length; i++) {
					_contextMenu.add(menuItems[i].getComponent());
				}
			} else {
				JMenu menu = getMenu(_contextMenu,
									 categoryID);
				if (menu != null) {
					Debug.println("ContentMenuController.populateMenuCategory() adding "+categoryID+ " to some menu");
					for (int i=0; i<menuItems.length; i++) {
						menu.add(menuItems[i].getComponent());
					}
				} else if (_framework != null) {
					Debug.println("ContentMenuController.populateMenuCategory() adding "+categoryID+ " to Framework");
					for (int i=0; i<menuItems.length; i++) {
						_framework.addMenuItem(categoryID,
											   menuItems[i]);
					}
				}
			}		
		} else {
			Debug.println("ContentMenuController.populateMenuCategory() "+categoryID+" has no menu items");
		}
	}

	private void unpopulateMenuCategory(String categoryID) {
		IMenuItem[] menuItems = getMenuItems(categoryID);
		if (menuItems != null) {
			if (categoryID.equals(MENU_CONTEXT)) {
				Debug.println("ContentMenuController.unpopulateMenuCategory() removing "+categoryID+" from context");
				for (int i=0; i<menuItems.length; i++) {
					_contextMenu.remove(menuItems[i].getComponent());
				}
			} else {
				JMenu menu = getMenu(_contextMenu,
								 categoryID);
				if (menu != null) {
					Debug.println("ContentMenuController.unpopulateMenuCategory() removing "+categoryID+" from menu");
					for (int i=0; i<menuItems.length; i++) {
						removeMenuItem(menu, menuItems[i]);
					}
				} else if (_framework != null) {
					Debug.println("ContentMenuController.unpopulateMenuCategory() removing "+categoryID+" from framework");
					for (int i=0; i<menuItems.length; i++) {
						_framework.removeMenuItem(menuItems[i]);	
					}			
				}
			}
		} else {
			Debug.println("ContentMenuController.unpopulateMenuCategory() "+categoryID+" has no menu items");
		}
	}

	/**
     * Returns a JFC menu item for a specified menu category.
     */
    private JMenu getMenu(JPopupMenu popupMenu, String categoryID) {
        JMenu result = null;
        for (int index = 0; index < popupMenu.getComponentCount();
                index++) {
            Component menu = popupMenu.getComponentAtIndex(index);
            if ((menu instanceof IMenuItemCategory) && (categoryID.equals(
                    ((IMenuItemCategory) menu).getID()))) {
                result = (JMenu)((IMenuItemCategory) menu).getComponent();
                break;
            } else if (menu instanceof JMenu) {
                JMenu newResult =
					getMenu((JMenu) menu, categoryID);
                if (newResult != null) {
                    result = newResult;
                    break;
                }
            }
        }
        return result;
    }


    /**
     * Returns JFC menu item for a specified menu category.
     * See IMenuCategory for predefined menu categories.
     */
    private JMenu getMenu(JMenu parent, String categoryID) {
        JMenu result = null;
        for (int index = 0; index < parent.getMenuComponentCount();
                index++) {
            Component c = parent.getMenuComponent(index);
            if ((c instanceof IMenuItemCategory) &&
                    (categoryID.equals(((IMenuItemCategory) c).getID()))) {
                result = (JMenu)((IMenuItemCategory) c).getComponent();
                break;
            } else if (c instanceof JMenu) {
                JMenu newResult = getMenu((JMenu) c, categoryID);
                if (newResult != null) {
                    result = newResult;
                    break;
                }
            }
        }
        return result;
    }

    /**
       * Returns a JFC menu item for a specified menu category.
       */
    private JMenu getMenu(JMenuBar menuBar, String categoryID) {
        JMenu result = null;
        for (int index = 0; index < menuBar.getMenuCount(); index++) {
            JMenu menu = menuBar.getMenu(index);
            if ((menu instanceof IMenuItemCategory) && (categoryID.equals(
                    ((IMenuItemCategory) menu).getID()))) {
                result = (JMenu)((IMenuItemCategory) menu).getComponent();
                break;
            } else if (menu instanceof JMenu) {
                JMenu newResult = getMenu(menu, categoryID);
                if (newResult != null) {
                    result = newResult;
                    break;
                }
            }
        }
        return result;
    }

	private boolean removeMenuItem(JMenu parent, IMenuItem item) {
        boolean result = false;
        for (int index = 0; index < parent.getMenuComponentCount();
                index++) {
            Component c = parent.getMenuComponent(index);
            if ((c instanceof IMenuItem) && (c == item)) {
                parent.remove(index);
                result = true;
                break;
            } else if (c instanceof JMenu) {
                if (removeMenuItem((JMenu) c, item) == true) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

	/**
	 * Returns supported menu categories. 
	 * NOTE: is necessary that the order and the hierarchy are respected: 
	 * if a category is superior to another category, it has to go first.
	 * For example Framework.MENU_TOP is before Framework.MENU_FILE because is
	 * its superior.  Idem for MENU_CONTEXT and CONTEXTNEW.
	 */
	private String[] getMenuCategoryIDs() {
		if (_categoryID == null) {
			_categoryID = new String[] {
				Framework.MENU_TOP,
				    Framework.MENU_FILE,
					Framework.MENU_EDIT,
					Framework.MENU_VIEW,	
					MENU_CONTEXT,
					MENU_OBJECT,				
					CONTEXTNEW,
					CONTEXTNEWROOTENTRY,
					CONTEXTSETPWP,
					OBJECTNEW,
					OBJECTNEWROOTENTRY,
					OBJECTSETPWP,
					DISPLAY,
					LAYOUT,
					PARTITIONVIEW
					};
		}
		return _categoryID;
	}

	/**
	 * add menu items for this page.
	 */
	private IMenuItem[] getMenuItems(String category) {	
		if(category.equals(Framework.MENU_TOP)) {
			return _objectTopMenuItems;
		} else if(category.equals(Framework.MENU_FILE)) {
			if (_fileMenuItems == null) {
				createFileMenuItems();
			}
			return _fileMenuItems;
		} else if ( category.equals(MENU_CONTEXT) ) {	
			if (_contextMenuItems == null) {
				createContextMenuItems();
			}
			return 	_contextMenuItems;			
		} else if( category.equals(CONTEXTNEW)) { 			
			if (_contextNewMenuItems == null) {
				createContextNewMenuItems();
			}
			return _contextNewMenuItems;
		} else if( category.equals(CONTEXTSETPWP)) { 			
			if (_contextPWPMenuItems == null) {
				createContextPWPMenuItems();
			}
			return _contextPWPMenuItems;
		} else if ( category.equals(OBJECTNEW)) {
			if (_objectNewMenuItems == null) {
				createObjectNewMenuItems();
			}
			return _objectNewMenuItems;
		} else if ( category.equals(OBJECTSETPWP)) {
			if (_objectPWPMenuItems == null) {
				createObjectPWPMenuItems();
			}
			return _objectPWPMenuItems;
		} else if (category.equals(CONTEXTNEWROOTENTRY)) {
			if (_contextNewRootEntryMenuItems == null) {
				createNewRootEntryMenuItems();
			}				
			return _contextNewRootEntryMenuItems;
		} else if (category.equals(OBJECTNEWROOTENTRY)) {
			if (_objectNewRootEntryMenuItems == null) {
				createNewRootEntryMenuItems();
			}
			return _objectNewRootEntryMenuItems;
			
		} else if( category.equals(Framework.MENU_EDIT) ) {
			if (_editMenuItems == null) {
				createEditMenuItems();
			}
			return _editMenuItems;
		} else if(category.equals(Framework.MENU_VIEW)) {
			if (_viewMenuItems == null) {
				createViewMenuItems();
			}			
			return _viewMenuItems;
		} else if(category.equals(MENU_OBJECT)) {			
			if (_objectMenuItems == null) {
				createObjectMenuItems();
			}
			return _objectMenuItems;  		
		} else if(category.equals(LAYOUT)) {
			if (_layoutMenuItems == null) {
				createLayoutMenuItems();
			}
			return _layoutMenuItems;
		} else if(category.equals(DISPLAY)) {
			if (_displayMenuItems == null) {
				createDisplayMenuItems();
			}
			return _displayMenuItems;		
		} else if(category.equals(PARTITIONVIEW)) {
		   if (_partitionViewMenuItems == null) {
			   createPartitionViewMenuItems();
		   }
		   return _partitionViewMenuItems;
		}
		return null;
	}


	private void setEnabledMenuItem(IMenuItem[] menu, String ID, boolean state) {
		boolean done = false;
		for (int i=0; (i<menu.length) && !done; i++ ) {			
			if (menu[i].getID().equals(ID)) {
				done = true;
				if (menu[i] instanceof JMenuItem) {					
					((JMenuItem)menu[i]).setEnabled(state);
				}
			}
		}
	}

	private void setEnabledMenuItem(IMenuItem[] menu, boolean state) {		
		for (int i=0; i<menu.length; i++ ) {			
			if (menu[i] instanceof JMenuItem) {					
				((JMenuItem)menu[i]).setEnabled(state);				
			}
		}
	}
	
							  
	private void setCheckedMenuItem(IMenuItem[] menu, String ID, boolean state) {
		boolean done = false;
		for (int i=0; (i<menu.length) && !done; i++ ) {
			Debug.println("ContentMenuController.setCheckedMenuItem() ID " + menu[i].getID());
			if (menu[i].getID().equals(ID)) {
				done = true;
				if (menu[i] instanceof MenuItemCheckBox) {
					((MenuItemCheckBox)menu[i]).setChecked(state);
				}
			}
		}
	}


	private void updateSearchMenuItems(boolean isRootSelected) {		
		setEnabledMenuItem(_objectMenuItems, SEARCH_UG, !isRootSelected);
		setEnabledMenuItem(_contextMenuItems, SEARCH_UG, !isRootSelected);		
	}
		
	private void updateAciMenuItems(boolean isRootSelected) {			
		setEnabledMenuItem(_objectMenuItems, ACL, !isRootSelected);
		setEnabledMenuItem(_contextMenuItems, ACL, !isRootSelected);		
	}

	private void updateRoleMenuItems(boolean isRootSelected) {		
		setEnabledMenuItem(_objectMenuItems, ROLES, !isRootSelected);
		setEnabledMenuItem(_contextMenuItems, ROLES, !isRootSelected);		
	}

	private void updateReferralMenuItems(boolean isRootSelected) {
		setEnabledMenuItem(_objectMenuItems, SET_REFERRALS, !isRootSelected);
		setEnabledMenuItem(_contextMenuItems, SET_REFERRALS, !isRootSelected);
	}

	private void updateMoveCopyPasteMenuItems(boolean isRootSelected, 
                                              boolean isSuffixSelected,
											  boolean isClipboardEmpty,
											  boolean isDnInClipboardEmpty) {
		setEnabledMenuItem(_editMenuItems, COPY, (!isRootSelected && !isSuffixSelected));
		setEnabledMenuItem(_contextMenuItems, COPY, (!isRootSelected && !isSuffixSelected));
		
		setEnabledMenuItem(_editMenuItems, MOVE, (!isRootSelected && !isSuffixSelected));
		setEnabledMenuItem(_contextMenuItems, MOVE, (!isRootSelected && !isSuffixSelected));
		
		
		setEnabledMenuItem(_editMenuItems, DELETE, (!isRootSelected && !isSuffixSelected));
		setEnabledMenuItem(_contextMenuItems, DELETE, (!isRootSelected && !isSuffixSelected));
		
		setEnabledMenuItem(_editMenuItems, COPYDN, !isRootSelected);
		setEnabledMenuItem(_editMenuItems, COPYLDAPURL, !isRootSelected);

		setEnabledMenuItem(_editMenuItems, PASTE, !isRootSelected && !isClipboardEmpty);
		setEnabledMenuItem(_contextMenuItems, PASTE, !isRootSelected && !isClipboardEmpty);

		setEnabledMenuItem(_editMenuItems, MOVEPASTE, !isRootSelected && !isDnInClipboardEmpty);
		setEnabledMenuItem(_contextMenuItems, MOVEPASTE, !isRootSelected && !isDnInClipboardEmpty);
	}

	private void updatePWPMenuItems(Integer selectionPWPState, 
			boolean isSelectedNodeRemote, boolean isRootSelected, boolean isSuffixSelected) {
		if (isSelectedNodeRemote || isRootSelected) {
			setEnabledMenuItem(_objectMenuItems, OBJECTSETPWP, false);
			setEnabledMenuItem(_contextMenuItems, CONTEXTSETPWP, false);
		} else {
			setEnabledMenuItem(_objectMenuItems, OBJECTSETPWP, true);
			setEnabledMenuItem(_contextMenuItems, CONTEXTSETPWP, true);
		}
		if (selectionPWPState.intValue() == PasswordPolicyPanel.NO_PWP) {
			setEnabledMenuItem(_objectPWPMenuItems, SET_PWP_USER, false);
			setEnabledMenuItem(_contextPWPMenuItems, SET_PWP_USER, false);
			setEnabledMenuItem(_objectPWPMenuItems, SET_PWP_SUBTREE, false);
			setEnabledMenuItem(_contextPWPMenuItems, SET_PWP_SUBTREE, false);
		} else {
			if (isSuffixSelected) {
				setEnabledMenuItem(_objectPWPMenuItems, SET_PWP_USER, false);
				setEnabledMenuItem(_contextPWPMenuItems, SET_PWP_USER, false);
			} else {
				setEnabledMenuItem(_objectPWPMenuItems, SET_PWP_USER, true);
				setEnabledMenuItem(_contextPWPMenuItems, SET_PWP_USER, true);
			}
			setEnabledMenuItem(_objectPWPMenuItems, SET_PWP_SUBTREE, true);
			setEnabledMenuItem(_contextPWPMenuItems, SET_PWP_SUBTREE, true);
		}
	}

	private void updateNewMenuItems(boolean isRootSelected) {
		boolean newRootsToAdd = false;
		if (_contextNewRootEntryMenuItems != null) {
			newRootsToAdd = _contextNewRootEntryMenuItems.length > 0;			
		}
		setEnabledMenuItem(_objectMenuItems, OBJECTNEWROOTENTRY, isRootSelected && newRootsToAdd);
		setEnabledMenuItem(_contextMenuItems, CONTEXTNEWROOTENTRY, isRootSelected && newRootsToAdd);		
		
		setEnabledMenuItem(_objectMenuItems, OBJECTNEW, !isRootSelected);
		setEnabledMenuItem(_contextMenuItems, CONTEXTNEW, !isRootSelected);		
	}

	private void updateVLVMenuItems(Integer selectionVlvState,
									boolean isSelectedNodeRemote) {		
		if (isSelectedNodeRemote) {
			/* If is remote we enable the creation of index (when the user clicks on
			 it should be informed that the operation can not be done in a remote node) */
			setEnabledMenuItem(_objectMenuItems, CREATE_VLV_INDEX, true);
			setEnabledMenuItem(_contextMenuItems, CREATE_VLV_INDEX, true);
			setEnabledMenuItem(_objectMenuItems, DELETE_VLV_INDEX, false);
			setEnabledMenuItem(_contextMenuItems, DELETE_VLV_INDEX, false);
		} else if (selectionVlvState.intValue() == CreateVLVIndex.NO_INDEX) {
			setEnabledMenuItem(_objectMenuItems, CREATE_VLV_INDEX, true);
			setEnabledMenuItem(_contextMenuItems, CREATE_VLV_INDEX, true);
			setEnabledMenuItem(_objectMenuItems, DELETE_VLV_INDEX, false);
			setEnabledMenuItem(_contextMenuItems, DELETE_VLV_INDEX, false);
		} else if (selectionVlvState.intValue() == CreateVLVIndex.HAS_INDEX) {
			setEnabledMenuItem(_objectMenuItems, CREATE_VLV_INDEX, false);
			setEnabledMenuItem(_contextMenuItems, CREATE_VLV_INDEX, false);
			setEnabledMenuItem(_objectMenuItems, DELETE_VLV_INDEX, true);
			setEnabledMenuItem(_contextMenuItems, DELETE_VLV_INDEX, true);
		} else {
			setEnabledMenuItem(_objectMenuItems, CREATE_VLV_INDEX, false);
			setEnabledMenuItem(_contextMenuItems, CREATE_VLV_INDEX, false);
			setEnabledMenuItem(_objectMenuItems, DELETE_VLV_INDEX, false);
			setEnabledMenuItem(_contextMenuItems, DELETE_VLV_INDEX, false);
		}		
	}

	private void updatePWPMenuItems(Integer selectionPWPState,
									boolean isSelectedNodeRemote) {
		if (selectionPWPState.intValue() == PasswordPolicyPanel.NO_PWP) {
			setEnabledMenuItem(_objectMenuItems, SET_PWP_USER, false);
			setEnabledMenuItem(_contextMenuItems, SET_PWP_USER, false);
			setEnabledMenuItem(_objectMenuItems, SET_PWP_SUBTREE, false);
			setEnabledMenuItem(_contextMenuItems, SET_PWP_SUBTREE, false);
		} else {
			setEnabledMenuItem(_objectMenuItems, SET_PWP_USER, true);
			setEnabledMenuItem(_contextMenuItems, SET_PWP_USER, true);
			setEnabledMenuItem(_objectMenuItems, SET_PWP_SUBTREE, true);
			setEnabledMenuItem(_contextMenuItems, SET_PWP_SUBTREE, true);
		}
	}


    private void updateActivateInactivateMenuItems(Integer selectionActivationState,
												   boolean isSelectedNodeRemote) {		
		if (isSelectedNodeRemote) {		
			/* If is remote we enable the inactivation (when the user clicks on
			 it should be informed that the operation can not be done in a remote node) */
			setEnabledMenuItem(_objectMenuItems, ACTIVATE, false);
			setEnabledMenuItem(_contextMenuItems, ACTIVATE, false);
			setEnabledMenuItem(_objectMenuItems, INACTIVATE, true);
			setEnabledMenuItem(_contextMenuItems, INACTIVATE, true);
		} else if (selectionActivationState.intValue() == AccountInactivation.CANNOT_BE_ACTIVATED_INACTIVATED) {
			setEnabledMenuItem(_objectMenuItems, ACTIVATE, false);
			setEnabledMenuItem(_contextMenuItems, ACTIVATE, false);
			setEnabledMenuItem(_objectMenuItems, INACTIVATE, false);
			setEnabledMenuItem(_contextMenuItems, INACTIVATE, false);
		} else if (selectionActivationState.intValue() == AccountInactivation.CAN_BE_INACTIVATED) {
			setEnabledMenuItem(_objectMenuItems, ACTIVATE, false);
			setEnabledMenuItem(_contextMenuItems, ACTIVATE, false);
			setEnabledMenuItem(_objectMenuItems, INACTIVATE, true);
			setEnabledMenuItem(_contextMenuItems, INACTIVATE, true);
		} else {
			setEnabledMenuItem(_objectMenuItems, ACTIVATE, true);
			setEnabledMenuItem(_contextMenuItems, ACTIVATE, true);
			setEnabledMenuItem(_objectMenuItems, INACTIVATE, false);
			setEnabledMenuItem(_contextMenuItems, INACTIVATE, false);
		}
	}
	
	private void createShortCutKeys() {
		_shortCutTable = new Hashtable();
		_shortCutTable.put(OPEN, KeyStroke.getKeyStroke(KeyEvent.VK_P, Event.CTRL_MASK));
		_shortCutTable.put(ACL, KeyStroke.getKeyStroke(KeyEvent.VK_L, Event.CTRL_MASK));
		//_shortCutTable.put(REFRESHTREE, KeyStroke.getKeyStroke(KeyEvent.VK_R, Event.CTRL_MASK));
        _shortCutTable.put(REFRESHNODE, KeyStroke.getKeyStroke(KeyEvent.VK_R, Event.CTRL_MASK));
		_shortCutTable.put(COPY, KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK));
		_shortCutTable.put(MOVE, KeyStroke.getKeyStroke(KeyEvent.VK_M, Event.CTRL_MASK));
		_shortCutTable.put(PASTE, KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK));
		_shortCutTable.put(MOVEPASTE, KeyStroke.getKeyStroke(KeyEvent.VK_W, Event.CTRL_MASK));
		_shortCutTable.put(DELETE, KeyStroke.getKeyStroke(KeyEvent.VK_D, Event.CTRL_MASK));
	}

	public void updateFollowReferralsMenuItem() {
		if (_contentPage.getSelectedPartitionView().equals(VIEW_ALL_PARTITIONS)) {
			setEnabledMenuItem(_viewMenuItems, FOLLOW_REFERRALS, true);
		} else {
			setEnabledMenuItem(_viewMenuItems, FOLLOW_REFERRALS, false);
			if (_contentPage.getFollowReferrals()) {
				setCheckedMenuItem(_viewMenuItems, FOLLOW_REFERRALS, false);
			}
		}
	}

	JPopupMenu _contextMenu;
	IContentPageInfo _contentPage;
	DatabaseConfig _databaseConfig;	
	ActionListener _listener;
	IFramework _framework;
	Vector _shortcutRegisterers;
	Hashtable _shortCutTable;

	private String[] _categoryID = null;
	IMenuItem[] _viewMenuItems;
	IMenuItem[] _displayMenuItems;
	IMenuItem[] _partitionViewMenuItems;
	IMenuItem[] _objectNewRootEntryMenuItems;
	IMenuItem[] _contextNewRootEntryMenuItems;
	IMenuItem[] _objectTopMenuItems	= new IMenuItem[] {
		new MenuItemCategory(MENU_OBJECT,
							 Framework.i18n("menu", "Object"))
	};
	IMenuItem[] _fileMenuItems;
	IMenuItem[] _contextMenuItems;
	IMenuItem[] _contextNewMenuItems;
	IMenuItem[] _objectNewMenuItems;
	IMenuItem[] _contextPWPMenuItems;
	IMenuItem[] _objectPWPMenuItems;
	IMenuItem[] _editMenuItems;
	IMenuItem[] _objectMenuItems;
	IMenuItem[] _layoutMenuItems;

	static ResourceSet _resource = DSUtil._resource;

	static final String MENU_CONTEXT = "CONTEXT";
	static final String MENU_OBJECT = "OBJECT";		
	static final String OPEN = "open";
	static final String ADVANCED_OPEN = "advanced open";
	static final String ACL = "acl";
	static final String ROLES = "roles";
	static final String SET_REFERRALS = "referral";
	static final String AUTHENTICATE = "authenticate";
	static final String MOVE = "move";
	static final String COPY = "copy";
	static final String PASTE = "paste";
	static final String MOVEPASTE = "movepaste";
	static final String UNDO = "undo";
	static final String DELETE = "delete";
	static final String NEW_USER = "newuser";
	static final String NEW_GROUP = "newgroup";
	static final String NEW_ORGANIZATIONALUNIT = "newou";
	static final String NEW_ROLE = "newrole";
	static final String NEW_COS = "newcos";
	static final String NEW_OBJECT = "newobject";
	static final String SEARCH_UG = "search";	
	static final String CREATE_VLV_INDEX = "createVLVIndex";
	static final String DELETE_VLV_INDEX = "deleteVLVIndex";
	static final String OBJECTSETPWP = "object_setPWP";
	static final String CONTEXTSETPWP = "context_setPWP";
	static final String SET_PWP_USER = "setPWP-user";
	static final String SET_PWP_SUBTREE = "setPWP-subtree";
	static final String CONTEXTNEW = "CONTEXTNEW";
	static final String CONTEXTNEWROOTENTRY = "CONTEXTNEWROOTENTRY";
	static final String OBJECTNEW = "OBJECTNEW";
	static final String OBJECTNEWROOTENTRY = "OBJECTNEWROOTENTRY";
	static final String REFRESHTREE = "REFRESHTREE";
	static final String REFRESHNODE = "REFRESHNODE";
	static final String FOLLOW_REFERRALS = "FOLLOW_REFERRALS";
	static final String COPYDN = "copydn";
	static final String COPYLDAPURL = "copyldapurl";
	static final String PARTITIONVIEW = "PARTITIONVIEW";
    static final String SORT = "SORT";
    static final String DISPLAY = "DISPLAY";
    static final String DISPLAY_ACI_COUNT = "DISPLAY_ACI_COUNT";
    static final String DISPLAY_ROLE_COUNT = "DISPLAY_ROLE_COUNT";
	static final String DISPLAY_ACCOUNT_INACTIVATION = "DISPLAY_ACCOUNT_INACTIVATION";
	static final String LAYOUT = "LAYOUT";	
	static final String ACTIVATE = "activate";
	static final String INACTIVATE = "inactivate";

	static final String VIEW_ALL_PARTITIONS = "ALL_PARTITIONS";

	final static String ONLY_TREE_LAYOUT = "ONLY_TREE_LAYOUT";
	final static String NODE_LEAF_LAYOUT = "NODE_LEAF_LAYOUT";
	final static String ATTRIBUTE_LAYOUT = "ATTRIBUTE_LAYOUT";
}

class MenuItemRadioButton extends JRadioButtonMenuItem implements IMenuItem {
	String _id = null;
	String _description = null;
	
	public MenuItemRadioButton(String label, String description, boolean state, ActionListener listener) {
		this("<noID>", label, description, state, listener);
	}
	
	public MenuItemRadioButton(String id, String label,
							   String description, boolean checked, ActionListener listener) {
		setID(id);
		setText(UITools.getDisplayLabel(label));
		setMnemonic(UITools.getMnemonic(label));
		setDescription(description);
		setChecked(checked);
		setActionCommand(id);
		addActionListener(listener);
	}
	
	public void setText(String label) {
		super.setText(UITools.getDisplayLabel(label));
	}
	
	public Component getComponent() {
		return this;
	}
	
	public String getID() {
		return _id;
	}
	
	public void setID(String id) {
		_id = id;
	}
	
	public String getDescription() {
		return _description;
	}
	
	public void setDescription(String description) {
		_description = description;
	}
	
	public boolean isChecked() {
		return isSelected();
	}
	
	public void setChecked(boolean checked) {
		super.setSelected(checked);
	}
}

