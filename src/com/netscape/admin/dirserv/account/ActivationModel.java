/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.account;

import java.util.Vector;
import java.util.Observable;
import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.ug.ResourceEditor;
import com.netscape.management.client.ug.ResourcePageObservable;


/**
 * An ActivationModel holds the activation state of the an
 * associated ResourcePageObservable ie:
 *		- the observable is activated or inactivated
 *		- the observable activation state can be edited or not
 *		- the (inactivated) observable can be reactivated
 * It also holds a modification flag raised by setActivated() 
 * and reset by save().
 *
 * An ActivationModel is queried by title panels and resource 
 * editor pages to display the activation state of an entry.
 * It isolates the other classes of the way the activation state
 * is computed.
 */
public abstract class ActivationModel extends Observable {

	/**
	 * Initialize this activation model from a observable.
	 * This method scans the observable and initializes the
	 * state of this activation model.
	 * It must be implemented by the derived classes depending
	 * on how the activation is computed and represented.
	 */
	public abstract void initialize(
	                     ResourcePageObservable o,
						 ResourceEditor ed);
	
	
	/**
	 * Return true if editing the activation state is permitted.
	 * If false, setActivated() should not be invoked on this
	 * activation model.
	 */
	public boolean isEditingEnable()
	{
		return _editingEnable;
	}
	
	
	/**
	 * Return true if the associated observable is activated.
	 */
	public boolean isActivated()
	{
		return _activated;
	}
	
	
	/**
	 * Return true if the associated observable can be activated.
	 */
	public boolean isActivationPermitted()
	{
		return _activationPermitted;
	}


	/**
	 * Activate/inactivate the associated observable.
	 * This method notifies all the observers and mark this
	 * activation model has modified.
	 */
	public void setActivated(boolean active)
	{
		if (active != _activated) {
			_activated = active;
			_modified = true;
			setChanged();
			notifyObservers();
		}
	}
	
	/**
	 * Return true if this activation model has been modified.
	 */
	public boolean isModified()
	{
		return _modified;
	}


	/**
	 * Return the roles which inactivate the entry.
	 * Returned value is null or a vector of dn string.
	 */
	public Vector getLockingRoles()
	{
		return null;
	}
	
	/**
	 * Align the observable state on this activation model.
	 * Mark this activation model as un-modified.
	 */
	public boolean save(ResourcePageObservable o)
	{
		Debug.println("ActivationModel.save: nothing done because not overriden");
		_modified = false;
		return true;
	}


	/**
	 * Call after the observable has been save in the directory.
	 * Here takes place all the additional savings needed to 
	 * commit the activation state changes.
	 */
	public boolean afterSave(ResourcePageObservable o, ResourceEditor ed) 
	throws LDAPException
	{
		Debug.println("ActivationModel.afterSave: nothing done because not overriden");
		return true;
	}
	

	protected boolean _editingEnable = false;
	protected boolean _activated = true;
	protected boolean _activationPermitted = true;
	protected boolean _modified = false;
};
