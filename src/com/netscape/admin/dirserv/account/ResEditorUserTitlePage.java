/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.account;

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import netscape.ldap.*;
import com.netscape.management.nmclf.*;
import com.netscape.management.client.ug.TitlePanel;
import com.netscape.management.client.ug.ResourcePageObservable;
import com.netscape.management.client.ug.PickerEditorResourceSet;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.*;
import com.netscape.management.nmclf.SuiTitle;
import com.netscape.admin.dirserv.DSUtil;


/**
 * RoleTitlePanel is the title panel displayed by the ResourceEditor
 * when editing a user (more precisely a person). It is created 
 * by ResEditorUserAccountPage.
 *
 * @see ResEditorUserAccountPage
 */
public class ResEditorUserTitlePage extends JPanel implements Observer
{

    /**
	 * Constructor
	 *
	 * @param observable  the observable object
	 */
	public ResEditorUserTitlePage(ResourcePageObservable o, ActivationModel m) {

		// Observes the activation model
		m.addObserver(this);
		
		// Build components
		_name = new SuiTitle("");
		_imageLabel = new JLabel();
		_department = new JLabel();
		_faxPhone = new JLabel();
		_wPhone = new JLabel();
		layoutComponents();
		
		// Initialize components by simulating updates
		_isActivated = true;
		update(o, "cn");
		update(o, "telephonenumber");
		update(o, "facsimiletelephonenumber");
		update(o, "ou");
		update(o, "photo");
		update(m, null);
	}
	
     /**
	 * Implements the Observer interface. Updates the information in
	 * this pane when called.
	 *
	 * @param o    the observable object
	 * @param arg  argument
	 */
	public void update(Observable o, Object arg) {
	
		if (o instanceof ResourcePageObservable) {
			ResourcePageObservable observable = (ResourcePageObservable)o;
			if (arg.equals("cn"))
				_name.setText(observable.get("cn", 0));
			else if (arg.equals("telephonenumber"))
				_wPhone.setText(observable.get("telephonenumber", 0));
			else if (arg.equals("facsimiletelephonenumber"))
				_faxPhone.setText(observable.get("facsimiletelephonenumber", 0));
			else if (arg.equals("ou"))
				_department.setText(observable.get("ou", 0));
			else if (arg.equals("photo") || arg.equals("jpegphoto")) {
        		byte photoBytes[] = observable.getBytes("photo");
        		if (photoBytes == null) {
            		photoBytes = observable.getBytes("jpegphoto");
        		}
        		if (photoBytes != null) {
            		_photo = new ImageIcon(photoBytes);
            		Image img = _photo.getImage();
            		if (img != null) {
                		if (img.getHeight(null) > 30) {
                    		img = img.getScaledInstance(-1, 30, Image.SCALE_FAST);
                    		_photo = new ImageIcon(img);
                		}
            		}

        		} else {
            		_photo = new RemoteImage("com/netscape/management/nmclf/icons/user24.gif");
        		}
				adjustIcon();
			}
				
		}
		else if (o instanceof ActivationModel) {
			_isActivated = ((ActivationModel)o).isActivated();
			adjustIcon();
		}
	}



	/**
	 * The sequence which adjust the icon according
	 * the photo and the activation state.
	 */
	void adjustIcon() {
		ImageIcon iconImage = _photo;
		if ((iconImage != null) && ! _isActivated )	{
			iconImage = DSUtil.inactivatedIcon(iconImage);
		}
		_imageLabel.setIcon(iconImage);
	}


	/**
	 * Layout the components
	 */
	void layoutComponents() {
        PickerEditorResourceSet resource = new PickerEditorResourceSet();

	    GridBagLayout layout = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        setLayout(layout);
		
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = c.REMAINDER;
        c.insets = new Insets(SuiLookAndFeel.COMPONENT_SPACE,
                SuiLookAndFeel.COMPONENT_SPACE,
                SuiLookAndFeel.COMPONENT_SPACE,
                SuiLookAndFeel.COMPONENT_SPACE);
        c.anchor = c.CENTER;
		add(_imageLabel, c);
		
		c.gridx++;
		c.gridheight = 1;
		c.weightx = 1;
        c.insets = new Insets(0, SuiLookAndFeel.COMPONENT_SPACE, 0,
                SuiLookAndFeel.COMPONENT_SPACE);
        c.anchor = c.WEST;
        add(_name, c);
		
		c.gridy++;
		c.gridheight = c.REMAINDER;
		add(_department, c);
		
		JLabel l = new JLabel(resource.getString("userTitlePage", "phoneWork"));
		c.gridx++;
		c.gridheight = 1;
		c.weightx = 0;
		c.anchor = c.EAST;
		c.insets = new Insets(0,0,0,0);
		add(l, c);
		
		l = new JLabel(resource.getString("userTitlePage", "phoneFax"));
		c.gridy++;
		add(l, c);
		
		c.gridx++;
		c.gridy--;
		c.anchor = c.WEST;
		add(_wPhone, c);
		
		c.gridy++;
		add(_faxPhone, c);
	}
	

	// State aligned on the observable
	boolean _isActivated;
	
	// Components
	JLabel _name;
	JLabel _imageLabel;
	JLabel _department;
	JLabel _faxPhone;
	JLabel _wPhone;
	ImageIcon _photo;
	
}
