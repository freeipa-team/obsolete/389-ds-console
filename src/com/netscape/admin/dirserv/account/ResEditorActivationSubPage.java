/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.account;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import netscape.ldap.LDAPConnection;
import com.netscape.management.nmclf.*;
import com.netscape.management.client.ug.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.UIFactory;

/**
 * Subpage embedded in ResEditorUserAccountPage and ResEditorRoleAccountPage.
 *
 * This subpage contains the components to activate/inactivate a user/role.
 * At instantation time, the caller specifies a resource section which 
 * contains the user/role specific labels (_extraSection).
 * The 'activation state' is maintained in the ActivationModel.
 * The ActivationModel is also observed by the title panel to keep the icon
 * up to date.
 */

public class ResEditorActivationSubPage extends JPanel
                                        implements SuiConstants, ActionListener {

	
	public ResEditorActivationSubPage(String extraSection) {
		_extraSection = extraSection;
	}

	
	/**
	 * @see IResourceEditorPage#initialize
	 */			
	public void initialize(
				ResourcePageObservable observable, 
				ResourceEditor ed)
	{
		// Keep a ref on the editor for afterSave()
		_editor = ed;
		
		// Create and layout the components if needed
		if (_activateButton == null) {

			// Create components
			_activateButton = new JButton("");
			_activateButton.addActionListener(this);
			_activateStatusTextField = new JLabel("", SwingConstants.LEFT);
			_activateDescTextField = new JLabel("");
			
			// Layout
			setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
        	gbc.gridx      = 0;
        	gbc.gridy      = 0;
        	gbc.gridwidth  = gbc.REMAINDER;
        	gbc.gridheight = 1;
        	gbc.weightx    = 1;
        	gbc.weighty    = 0;
        	gbc.fill       = gbc.NONE;
        	gbc.anchor     = gbc.NORTHWEST;
        	gbc.insets     = new Insets(COMPONENT_SPACE, COMPONENT_SPACE, 0, COMPONENT_SPACE);
        	gbc.ipadx      = 0;
        	gbc.ipady      = 0;
			
			add(_activateStatusTextField, gbc);
			
			gbc.gridy++;
			add(_activateDescTextField, gbc);
			
			gbc.gridx++;
			gbc.gridy++;
       		gbc.weightx = 0;
        	gbc.anchor = gbc.NORTHEAST;
			add(_activateButton, gbc);
			
			gbc.gridy++;
       		gbc.gridx = 0;
			add(Box.createVerticalStrut(0), gbc);
		}
		
	}

	
	/**
	 * Looks like IResourceEditorPage.initialize() except that
	 * our observable is the activation model.
	 */			
	public void initialize(
				ActivationModel actModel, 
				ResourceEditor ed)
	{
		_actModel = actModel;
		
		// Set labels based on the observable state.
		adjustComponentsFromActivationModel();
	}


	/**
	 * @see IResourceEditorPage#save
	 */			
	public boolean save(ResourcePageObservable observable)
	throws Exception {
		return _actModel.save(observable);
	}


	/**
	 * @see IResourceEditorPage#afterSave
	 */			
	public boolean afterSave(ResourcePageObservable observable)
	throws Exception {
		return _actModel.afterSave(observable, _editor);
	}


	/**
	 * Implements ActionListener.
	 * Here we process clicks on _activateButton.
	 */
	public void actionPerformed(ActionEvent e) {
		// By construction, e.getSource() == _activateButton
		
		// Update the activation state
		if (_actModel.isActivated()) {
			_actModel.setActivated(false);
		}
		else if (_actModel.isActivationPermitted())
			_actModel.setActivated(true);
		else
			actionShowInactivatedRoles();
		
		// Adjust the components from the activation state
		adjustComponentsFromActivationModel();
	}


	/**
	 * Sequence which creates and popup the dialog displaying
	 * the roles which inactivates the observable.
	 */
	void actionShowInactivatedRoles() {
		JFrame frame = _editor.getFrame();
		LDAPConnection ldc = _editor.getConsoleInfo().getLDAPConnection();
		
		InactivatingRoleDialog dlg = new InactivatingRoleDialog(frame);
		dlg.setRoleList(_actModel.getLockingRoles(), ldc);
		dlg.show();
		dlg.dispose();
	}
	
	
	/**
	 * Set the components according the activation state.
	 * This methods send an event to update the title page.
	 */
	void adjustComponentsFromActivationModel() {
	
		//
		// First, update the components
		//
		String buttonText, statusText, descText;
		if (_actModel.isActivated()) {
			statusText = "activeStatusText";
			descText   = "inactivationPermittedDescText";
			buttonText = "inactivationPermittedButtonText";
		}
		else if (_actModel.isActivationPermitted()) {
			statusText = "inactiveStatusText";
			descText   = "activationPermittedDescText";
			buttonText = "activationPermittedButtonText";
		}
		else {
			statusText = "inactiveStatusText";
			descText   = "activationNotPermittedDescText";
			buttonText = "activationNotPermittedButtonText";
		}
			
		_activateStatusTextField.setText(DSUtil._resource.getString(_extraSection, statusText));		
		_activateButton.setText(DSUtil._resource.getString(_section, buttonText));	
		UIFactory.setMnemonic(_section, buttonText, _activateButton, DSUtil._resource);
	}

	// State
	ActivationModel _actModel;
	ResourceEditor _editor;

	// Input/output components
	protected JLabel _activateStatusTextField;
	protected JLabel _activateDescTextField;
	protected JButton _activateButton;
	
	// I18N
	protected static String _section = "activationSubPage";
	protected String _extraSection;
}
