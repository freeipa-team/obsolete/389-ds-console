/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.account;

import java.util.Vector;
import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.ug.ResourceEditor;
import com.netscape.management.client.ug.ResourcePageObservable;
import com.netscape.admin.dirserv.DSUtil;


/**
 * This activation model computes the activation state
 * using the AccountInactivation class.
 */
public class BuiltinActivationModel extends ActivationModel {


	/**
	 * The builtin activation mechanism is based on role and cos.
	 */
	public void initialize(
	            ResourcePageObservable o,
                ResourceEditor ed)
	{
		LDAPConnection ldc = ed.getConsoleInfo().getLDAPConnection();
		
		_activator = new AccountInactivation(o);
		if (o.isNewUser()) {
			_lockingRoles = null;
			_activated = true;
			_editingEnable = true;
			_activationPermitted = true;
		}
		else {
			try {
				int state = _activator.getState(ldc);
				_lockingRoles = _activator.getLockingRoles(ldc);

				if (state == _activator.ACTIVATED) {
					_activated = true;
					_editingEnable = true;
					_activationPermitted = true;
				}
				else {
					_activated = false;
					_editingEnable = (state == _activator.INACTIVATED);
					_activationPermitted = (_lockingRoles == null) || (_lockingRoles.size() == 0);
				}
			}
			catch(LDAPException x) {
				Debug.println(0, "BuiltinActivationModel.initialize: " + x);
				if (Debug.getTrace()) {
					x.printStackTrace();
				}
				_activated = false;
				_editingEnable = false;
				_activationPermitted = false;
			}
		}
	}

	/**
	 * Align the observable state on this activation model.
	 */
	public boolean save(ResourcePageObservable o)
	{
		return true;
	}

	/**
	 * Return the locking roles
	 */
	public Vector getLockingRoles()
	{
		return _lockingRoles;
	}

	AccountInactivation _activator;
	Vector _lockingRoles;
}
