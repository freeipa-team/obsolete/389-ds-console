/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.account;

import java.util.Vector;
import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.ug.ResourceEditor;
import com.netscape.management.client.ug.ResourcePageObservable;
import com.netscape.admin.dirserv.DSUtil;


/**
 * The specialized version of BuiltinActivationModel for
 * user entries.
 */
public class UserActivationModel extends BuiltinActivationModel {


	/**
	 * Save the activation/inactivation state for a user
	 */
	public boolean afterSave(ResourcePageObservable o, ResourceEditor ed) 
	throws LDAPException
	{
		if (_modified) {
			LDAPConnection ldc = ed.getConsoleInfo().getLDAPConnection();
			try {
				if (! _activator.isLockingInfrastructureCreated(ldc)) {
					Debug.println(0, "UserActivationModel.afterSave: creating the locking infrastructure");
					_activator.createLockingInfrastructure(ldc);
				}
				if (_activated)
					_activator.modifyEntryToActivateEntry(ldc);
				else
					_activator.modifyEntryToInactivateEntry(ldc);
				Debug.println("UserActivationModel.afterSave: committed activation = " + _activated);
			}
			catch(LDAPException x) {
				Debug.println(0, "UserActivationModel.afterSave: " + x);
				if (Debug.getTrace())
					x.printStackTrace();
				DSUtil.showLDAPErrorDialog(ed.getFrame(), x, "updating-directory-title");
			}
			_modified = false;
		}
		else {
			Debug.println("UserActivationModel.afterSave: no change, nothing to do");
		}
		return true;
	}

}
