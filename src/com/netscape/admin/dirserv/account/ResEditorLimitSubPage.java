/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.account;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import com.netscape.management.client.ug.*;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.GroupPanel;


/**
 * Subpage for the ResourceEditor.
 */

public class ResEditorLimitSubPage extends GroupPanel implements Observer {

	public ResEditorLimitSubPage() {
		super(DSUtil._resource.getString(_section, "groupTitle"), true);
	}

	/**
	 * Same semantic as IResourceEditorPage.initialize()
	 */
	public void initialize(
				ResourcePageObservable o, 
				ResourceEditor ed)
	{
		// Create and Layout the components if needed
		if (_lookThroughLimitTextField == null) {
		
			_lookThroughLimitTextField = new JTextField();
			_sizeLimitTextField = new JTextField();
			_timeLimitTextField = new JTextField();
			_idleTimeoutTextField = new JTextField();

			// Layout
        	layoutComponents();
		
			// Align the components on the observable
			// Note: this should normally be done outside of the if
			// statement. However as ResEditorAccountPage is added on
			// the fly, it is initialized multiple time. Doing
			// this here, avoid to loose the user value when saving.
			// ResourceEditor behaviour is a bit hard to predict...
			_lookThroughLimitTextField.setText(o.get("nslookthroughlimit", 0));
			_sizeLimitTextField.setText(o.get("nssizelimit", 0));
			_timeLimitTextField.setText(o.get("nstimelimit", 0));
			_idleTimeoutTextField.setText(o.get("nsidletimeout", 0));
		}
	}


    /**
     * Layout the components
     */
    void layoutComponents() {
    	GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);

        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 0;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.CENTER;
        gbc.insets     = new Insets(0, 0, COMPONENT_SPACE, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
        
		JLabel l = new JLabel(DSUtil._resource.getString(_section, "lookThroughLimit"), SwingConstants.RIGHT);
		l.setLabelFor(_lookThroughLimitTextField);
		add(l, gbc);

		l = new JLabel(DSUtil._resource.getString(_section, "sizeLimit"), SwingConstants.RIGHT);
		l.setLabelFor(_sizeLimitTextField);
		gbc.gridy++;
		add(l, gbc);
		
		gbc.gridx++;
		gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.insets = new Insets(0, COMPONENT_SPACE, COMPONENT_SPACE, 0);
		add(_lookThroughLimitTextField, gbc);
		
		gbc.gridy++;
		add(_sizeLimitTextField, gbc);


		gbc.gridx++;
		gbc.gridy = 0;
        gbc.weightx = 0;
		add(new JLabel(DSUtil._resource.getString(_section, "entryUnit")), gbc);
		
		gbc.gridy++;
		add(new JLabel(DSUtil._resource.getString(_section, "entryUnit")), gbc);
		
		
		gbc.gridx++;
		gbc.gridy = 0;
        gbc.insets = new Insets(0, 3 * COMPONENT_SPACE, COMPONENT_SPACE, 0);
		l = new JLabel(DSUtil._resource.getString(_section, "timeLimit"), SwingConstants.RIGHT);
		l.setLabelFor(_timeLimitTextField);
		add(l, gbc);

		l = new JLabel(DSUtil._resource.getString(_section, "idleTimeout"), SwingConstants.RIGHT);
		l.setLabelFor(_idleTimeoutTextField);
		gbc.gridy++;
		add(l, gbc);

		gbc.gridx++;
		gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.insets = new Insets(0, COMPONENT_SPACE, COMPONENT_SPACE, 0);
		add(_timeLimitTextField, gbc);
		
		gbc.gridy++;
		add(_idleTimeoutTextField, gbc);

		gbc.gridx++;
		gbc.gridy = 0;
        gbc.weightx = 0;
		add(new JLabel(DSUtil._resource.getString(_section, "timeUnit")), gbc);
		
		gbc.gridy++;
		add(new JLabel(DSUtil._resource.getString(_section, "timeUnit")), gbc);
    }


	/**
	 * @see IResourceEditorPage#save
	 */
	public boolean isComplete() {
		JTextField textField = null;
		String text;
		int value;
		try {
			// Check _lookThroughLimitTextField
			textField = _lookThroughLimitTextField;
			text = textField.getText().trim();
			if (text.length() >= 1)
				value = Integer.parseInt(text);
			
			// Check _sizeLimitTextField
			textField = _sizeLimitTextField;
			text = textField.getText().trim();
			if (text.length() >= 1)
				value = Integer.parseInt(text);
			
			// Check _timeLimitTextField
			textField = _timeLimitTextField;
			text = textField.getText().trim();
			if (text.length() >= 1)
				value = Integer.parseInt(text);
			
			// Check _idleTimeoutTextField
			textField = _idleTimeoutTextField;
			text = textField.getText().trim();
			if (text.length() >= 1)
				value = Integer.parseInt(text);
			
			// Everything ok
			textField = null;
		}
		catch(NumberFormatException x) {
			textField.requestFocus();
			textField.selectAll();
		}
		
		return (textField == null);
	}
	
	/**
	 * @see IResourceEditorPage#save
	 */
	public boolean save(ResourcePageObservable o)
	throws Exception {
		String text;
		
		// Save _lookThroughLimitTextField
		text = _lookThroughLimitTextField.getText().trim();
		if (text.length() >= 1)
			o.replace("nslookthroughlimit", text);
		else
			o.delete("nslookthroughlimit");

		// Save _sizeLimitTextField
		text = _sizeLimitTextField.getText().trim();
		if (text.length() >= 1)
			o.replace("nssizelimit", text);
		else
			o.delete("nssizelimit");

		// Save _timeLimitTextField
		text = _timeLimitTextField.getText().trim();
		if (text.length() >= 1)
			o.replace("nstimelimit", text);
		else
			o.delete("nstimelimit");

		// Save _idleTimeoutTextField
		text = _idleTimeoutTextField.getText().trim();
		if (text.length() >= 1)
			o.replace("nsidletimeout", text);
		else
			o.delete("nsidletimeout");

		return true;
	}
	
	/**
	 * @see IResourceEditorPage#afterSave
	 */			
	public boolean afterSave(ResourcePageObservable o)
	throws Exception {
		return true;
	}


    /**
	 * Implements the Observer interface. Updates the fields when notified.
	 */
	public void update(Observable o, Object arg) {
		if (o instanceof ResourcePageObservable) {
			ResourcePageObservable rpo = (ResourcePageObservable)o;
			String argString = (String) arg;
			if (argString.equalsIgnoreCase("nslookthroughlimit"))
				_lookThroughLimitTextField.setText(rpo.get("nslookthroughlimit", 0));
			else if (argString.equalsIgnoreCase("nssizelimit"))
				_sizeLimitTextField.setText(rpo.get("nssizelimit", 0));
			else if (argString.equalsIgnoreCase("nstimelimit"))
				_timeLimitTextField.setText(rpo.get("nstimelimit", 0));
			else if (argString.equalsIgnoreCase("nsidletimeout"))
				_idleTimeoutTextField.setText(rpo.get("nsidletimeout", 0));
		}
	}


	// Components
	JTextField _lookThroughLimitTextField;
	JTextField _sizeLimitTextField;
	JTextField _timeLimitTextField;
	JTextField _idleTimeoutTextField;
	
	// I18N
	private static String _section = "limitSubPage";
}

