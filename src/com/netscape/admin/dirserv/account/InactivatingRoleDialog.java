/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
package com.netscape.admin.dirserv.account;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.*;
import com.netscape.management.client.ug.ResourceEditor;
import com.netscape.management.client.components.Table;
import com.netscape.management.nmclf.*;
import com.netscape.admin.dirserv.*;
import netscape.ldap.*;

/**
 * InactivatingRoleDialog is modal dialog which
 * 		- displays the roles in the scope of an LDAP entry,
 *		- allows to select one or more of those roles.
 *
 * A role is 'in the scope' if it is child of the one of the ancestors of
 * the LDAP entry.
 **/

public class InactivatingRoleDialog extends AbstractModalDialog 
implements SuiConstants, ListSelectionListener {

    /**
     * Construct an new InactivatingRoleDialog.
     * The resulting InactivatingRoleDialog is empty. It must be filled
     * with roles using the load() method.
     */
	public InactivatingRoleDialog(Frame parent) {
        super(parent, _resource.getString(_section, "title"), CLOSE);

		_tableModel = new RoleTableModel();
		_cnTable = new Hashtable();
		_descriptionTable = new Hashtable();
		
		_table = new Table(_tableModel);
        _table.setAutoResizeMode(_table.AUTO_RESIZE_DATA);
        _table.getSelectionModel().addListSelectionListener(this);
		_table.setPreferredScrollableViewportSize(new Dimension(350, 200));
		
		_dnLabel = new JLabel(_resource.getString(_section, "dn-prefix"));

        layoutComponents();
	}


    /**
	 * Set the list of role dn to be displayed.
	 */
	 public void setRoleList(Vector roleList, LDAPConnection ldc) {
	 
	 	_roleList = roleList;
		_ldc = ldc;
		
		// Rebuilt _cnTable and _descriptionTable
		_cnTable.clear();
		_descriptionTable.clear();
		Enumeration e = _roleList.elements();
		while (e.hasMoreElements()) {
			String dn = (String)e.nextElement();
			loadRole(dn);
		}
	 }
    
    
	/**
	 * Extends the base show() with the asynchronous loading.
	 */
/*	public void show() {
	
	 	// Before showing the dialog, we start the thread which
	 	// read cn and description of each role.
		Loader loader = new Loader();
		loader.start();
		
		// Now show the dialog
		super.show();
		
		// We tell the loader to giveup in case it is still running
		loader.abandon();
	}
*/	
	
    /**
     * Assemble and layout the components
     */
    void layoutComponents() {
    	Container contentPane = getContentPane();
    	GridBagLayout gbl = new GridBagLayout();
        
        contentPane.setLayout(gbl);

        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 1;
        gbc.anchor     = gbc.CENTER;
        gbc.fill       = gbc.BOTH;
        gbc.insets     = new Insets(0, 0, 0, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
        
        JLabel l = new JLabel(_resource.getString(_section, "label"));
        gbc.weighty = 0;
        contentPane.add(l, gbc) ;

		JScrollPane scrollPane = new JScrollPane(_table);
        gbc.gridy++;
        gbc.weighty = 1;
		contentPane.add(scrollPane, gbc);
		
		gbc.gridy++;
        gbc.weighty = 0;
		contentPane.add(_dnLabel, gbc);
      
		pack();
	}


    /**
     * Implements ListSelectionListener.
     * Called when the user clicks in the table.
     * We update _dnLabel with the dn of the selected role.
     */
    public void valueChanged(ListSelectionEvent e) {
    	ListSelectionModel m = (ListSelectionModel)e.getSource() ;
                
        // Update the tooltip text of the tab
		String dn;
        int min = m.getMinSelectionIndex() ;
        int max = m.getMaxSelectionIndex() ;
        if ((min == -1) || (max - min + 1 > 1)) {
        	// The selection is empty or multiple
			dn = "";
        }
        else {
			dn = (String)_roleList.elementAt(min);
        }
		
		_dnLabel.setText(_resource.getString(_section, "dn-prefix") + dn);
    }        
	
	
	/**
	 * Read the directory for the cn and description of
	 * the specified role and update _cnTable and _descriptionTable.
	 */
	void loadRole(String roleDn) {
	
		// Read the directory
		String cn = "", desc = "";
		try {
			LDAPAttribute attr;
			LDAPEntry roleEntry = DSUtil.readEntry(_ldc, roleDn, BASIC_ATTRS, null);

			// cn
			attr = roleEntry.getAttribute(CN_ATTR);
			cn = (attr == null) ? "" : attr.getStringValueArray()[0];

			// description
			attr = roleEntry.getAttribute(DESCRIPTION_ATTR);
			desc = (attr == null) ? "" : attr.getStringValueArray()[0];
		}
		catch(LDAPException x) {
			Debug.println("InactivatingRoleDialog.loadRole: failed to read " + roleDn);
			cn = roleDn;
			desc = "";
		}
		
		// Populate _cnTable and _descriptionTable.
		_cnTable.put(roleDn, cn);
		_descriptionTable.put(roleDn, desc);
	}
	
	

	
	// State variables
	Vector _roleList;
	RoleTableModel _tableModel;
	LDAPConnection _ldc;
	Hashtable _cnTable;
	Hashtable _descriptionTable;
	
	// Components
    Table _table;
	JLabel _dnLabel;

    // I18N
    static private final ResourceSet _resource = DSUtil._resource;
    static private final String _section = "inactivatingRoleDialog";
	
	// LDAP schema
    private static final String CN_ATTR = "cn";
    private static final String DESCRIPTION_ATTR = "description";
    private static final String OBJECTCLASS_ATTR = "objectclass";
    private static final String[] BASIC_ATTRS = {CN_ATTR, DESCRIPTION_ATTR, OBJECTCLASS_ATTR};
	
	// This value is used to clear _dnLabel but keeping
	// a good geometry
	private static final String NO_DN = "";
	
	
	/**
     * A table model for roles has two columns: the role name and its description.
	 * Data are taken from the outer class members _cnTable and _descriptionTable.
	 * 
     */
    class RoleTableModel extends AbstractTableModel {
    
        public String getColumnName(int col) {
        	if (col == 0)
            	return _resource.getString(_section, "header-name");
            else // col == 1
            	return _resource.getString(_section, "header-description");
        }
        
        public int getColumnCount() {
    	    return 2;
        }
        
	    public int getRowCount() {
			return (_roleList == null) ? 0 :  _roleList.size();
        }

        public Object getValueAt(int row, int col) {
			String dn = (String)_roleList.elementAt(row);
			Object result = null;
        	if (col == 0)
				result = _cnTable.get(dn);
            else // col == 1
				result = _descriptionTable.get(dn);
			if (result == null) result = "...";
			return result;
        }

        // makes the three following methods public
        public void fireTableDataChanged() {
        	super.fireTableDataChanged();
        }
        
        public void fireTableRowsDeleted(int start, int stop) {
        	super.fireTableRowsDeleted(start, stop);
        }
        
        public void fireTableRowsInserted(int start, int stop) {
        	super.fireTableRowsInserted(start, stop);
        }
    }
}
