/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.attredit;

import java.util.Vector;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.border.*;
import com.netscape.admin.dirserv.panel.UIFactory;
import com.netscape.management.client.util.Debug;
import java.awt.*;
import com.netscape.admin.dirserv.propedit.DSEntryPanel;
import javax.swing.text.BadLocationException;
import com.netscape.admin.dirserv.AttributeAlias;
/**
 * StringAttributeEditor<BR>
 * Attribute editor for string-valued attributes. A single label may
 * be visible, and have 0 or more text fields (one for each value). If
 * the editor is single-valued, at most one text field is allowed.
 *
 * @version 1.0
 * @author rweltman
 **/
public class StringAttributeEditor extends AttributeEditor
                                   implements DocumentListener {
    /**
     * Construct an empty one.
     *
     **/
    public StringAttributeEditor() {
		super();
    }

    /**
     * Gets a default (blank) value.
     *
	 * @return A single value.
     */
    public Object getDefaultValue() {
		return new String( "" );
	}

    protected JComponent fetchField( Object value ) {
		JTextField field;
		if ( _blankField != null ) {
			field = (JTextField)_blankField;
			field.setText( (String)value );
			_blankField = null;
		} else {
			field = makeTextField( (String)value );
		}
		field.setOpaque( true );
		field.setBorder( _textBorder );
		field.setEnabled( true );
		field.addFocusListener( this );
		field.addMouseListener( this );
		if ( _textFont != null )
			field.setFont( _textFont );
		field.setBackground( _textBackground );
		field.setForeground( _textColor );
		return field;
	}

    protected JComponent makeField() {
		return makeTextField( "" );
	}

    protected JTextField makeTextField( String value ) {
		return UIFactory.makeJTextField( this, value );
	}

    /**
     * Gets all values.
     *
	 * @return A vector of all values.
     **/
    public Vector getValues() {
		_values = new Vector();
		for( int i = 0; i < _fields.size(); i++ )
			_values.addElement(
				((JTextField)_fields.elementAt(i)).getText() );
		return super.getValues();
	}
		
    /**
     * Gets a value.
     *
     * @param index Index of the value to be retrieved.
	 * @return A single value.
     **/
    public Object getValue( int index ) {
		getValues();
		return super.getValue( index );
	}

    /**
	 * Get the last component that can have focus, for chaining.
	 *
	 * @return The last component internally that can have focus.
	 */
    public JComponent getLastFocusableComponent() {
		int i = getValueCount();
		if ( i > 1 )
			return (JComponent)_fields.elementAt( i - 1 );
		return this;
	}

    public boolean isBinary() {
		return false;
	}

    public void changedUpdate(DocumentEvent e) {
		Debug.println("StringAttributeEditor.changedUpdate()");
        setDirty();

		/* We see if we have to notify the parent that the dn changed*/
		Container parent = getEditorParent();
		try {	
			String[] namingAttributes = ((DSEntryPanel)parent).getNamingAttributes();
			boolean isNamingAttribute = false;
			String name = getName();
			if (namingAttributes != null) {
				for (int i=0; i < namingAttributes.length; i++) {
					if (name.equals(getName())) {
						isNamingAttribute = true;
						break;
					}
				}
			}
			if (isNamingAttribute) {				
				((DSEntryPanel)parent).updateNamingValue(name);
			}
		} catch (ClassCastException classException) {
			Debug.println("StringAttributeEditor.changedUpdate(). Exception"+ classException);
		} 	
    }

    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }
}
