/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.attredit;

import java.awt.Rectangle;
import java.util.Hashtable;
import java.util.Enumeration;
import com.netscape.management.client.util.Debug;
import netscape.ldap.LDAPAttributeSchema;

/**
  * A factory for producing an attribute editor for a particular attribute
  *
  * @author  rweltman
  * @version 1.1, 02/18/00
  * @date	 5/24/98
  */
public class AttributeEditorFactory {

	/**
	 * Set up the built-in associations
	 */
	static private boolean init() {
		_nameTable = new Hashtable();
		_typeTable = new Hashtable();
		_editorTable = new Hashtable();
		_allTable = new Hashtable();

		/* Set up the built-in preferences */
		addName( "objectclass",
				 _package + ".ObjectClassAttributeEditor" );
		addName( "jpegphoto", 
				 _package + ".ImageAttributeEditor" );
		addName( "userpassword",
				 _package + ".PasswordAttributeEditor" );
		addName( "usercertificate", 
				 _package + ".BinaryAttributeEditor" );

		addSyntax( LDAPAttributeSchema.integer,
				 _package + ".StringAttributeEditor" );
		addSyntax( LDAPAttributeSchema.cis,
				 _package + ".StringAttributeEditor" );
		addSyntax( LDAPAttributeSchema.ces,
				 _package + ".StringAttributeEditor" );
		addSyntax( LDAPAttributeSchema.dn,
				 _package + ".StringAttributeEditor" );
		addSyntax( LDAPAttributeSchema.telephone,
				 _package + ".StringAttributeEditor" );
		addSyntax( LDAPAttributeSchema.binary,
				 _package + ".BinaryAttributeEditor" );
		addSyntax( LDAPAttributeSchema.telephone,
				 _package + ".StringAttributeEditor" );
		/* For the unknown syntaxes we use the string attribute editor (the default one) */
		addSyntax( LDAPAttributeSchema.unknown,
				 _package + ".StringAttributeEditor" );

		setDefaultEditor( _defaultEditorName );
		return true;
	}

	/**
	 * Register a class to invoke for editing a particular attribute.
	 * Any previous registration for this name is overwritten.
	 *
	 * @param name Name of attribute
	 * @param className Name of class to invoke
	 */
    static public void addName( String name, String className ) {
		_nameTable.put( name, className );
		_allTable.put( className, name.trim().toLowerCase() );
	}

	/**
	 * Register a class to invoke for editing a particular attribute syntax
	 * Any previous registration for this syntax is overwritten.
	 *
	 * @param syntax Syntax, as defined in LDAPAttributeSchema
	 * @param className Name of class to invoke
	 */
    static public void addSyntax( int syntax, String className ) {
		_typeTable.put( new Integer(syntax), className );
		_allTable.put( className, new Integer(syntax) );
	}

	/**
	 * Register a class which will be asked to register any editors
	 * it contains.
	 *
	 * @param className Name of class to invoke
	 */
    static public void addClass( String className ) {
		/* Instantiate the class to let it register its editors */
		Class editor;
		try {
			editor = Class.forName( className );
			IAttributeEditor ed = (IAttributeEditor)editor.newInstance();
			ed.registerEditors();
		} catch ( Exception e ) {
			Debug.println( "AttributeEditorFactory.addClass: no " +
						   "class for " + className + ", " + e );
		}
	}

	static private void removeClassFromTable( String className, Hashtable h ) {
		Enumeration en = h.keys();
		while( en.hasMoreElements() ) {
			String key = (String)en.nextElement();
			Class c = (Class)h.get( key );
			if ( c.getName().equals( className ) ) {
				h.remove( key );
			}
		}
	}

	/**
	 * Unregister a class which was previously registered as an editor
	 *
	 * @param className Name of class to unregister
	 */
    static public void removeClass( String className ) {
		/* Remove it from the editor table */
		_editorTable.remove( className );
		/* Remove it from the name and syntax tables */
		removeClassFromTable( className, _nameTable );
		removeClassFromTable( className, _typeTable );
	}

	/**
	 * Register a class to be used as the default editor, if there is no
	 * editor registered which matches a particular attribute.
	 *
	 * @param className Name of class to invoke
	 */
    static public void setDefaultEditor( String className ) {
		/* Try to obtain a class reference */
		try {
			_defaultEditor = Class.forName( className );
		} catch ( ClassNotFoundException e ) {
			Debug.println( "AttributeEditorFactory.setDefaultEditor: no " +
								   "class for " + className );
		}
	}

	/**
	 * Get the class to be used as the default editor, if there is no
	 * editor registered which matches a particular attribute.
	 *
	 * @return Name of the class, or null if none
	 */
    static public String getDefaultEditor() {
		if ( _defaultEditor != null ) {
			return _defaultEditor.getClass().getName();
		} else {
			return null;
		}
	}

	/**
	 * Get a class reference for an editor class name
	 *
	 * @param className Name of the class
	 *
	 * @return Editor class, or null if none can be instantiated
	 */
    static public Class getClassForEditor( String className ) {
		if ( _editorTable == null ) {
			_editorTable = new Hashtable();
		}

		Class editor = null;
		/* Try to obtain a class reference */
		try {
			editor = Class.forName( className );
			/* Save the class reference for the future */
			_editorTable.put( className, editor );
		} catch ( ClassNotFoundException e ) {
			Debug.println(
				"AttributeEditorFactory.getClassForEditor: no " +
				"class for " + className );
		}
		return editor;
	}

	/**
	 * Get the class to be used as the editor for a particular attribute.
	 *
	 * @param name Name of the attribute
	 *
	 * @return Editor class for the attribute, or null if none
	 */
    static public Class getEditorForAttribute( String name ) {
		if ( _nameTable == null ) {
			return null;
		}
		Class editor = null;
		name = name.trim().toLowerCase();
		/* See if there is a registered class name */
		String editorName = (String)_nameTable.get( name );
		if ( editorName != null ) {
			/* Try to obtain a class reference */
			editor = getClassForEditor( editorName );
		}
		return editor;
	}

	/**
	 * Get the class to be used as the editor for a particular syntax.
	 *
	 * @param syntax Identifier of the syntax
	 *
	 * @return Editor class for the syntax, or null if none
	 */
    static public Class getEditorForSyntax( int syntax ) {
		if ( _typeTable == null ) {
			return null;
		}
		Class editor = null;
		String editorName =
			(String)_typeTable.get( new Integer( syntax ) );
		if ( editorName != null ) {
			/* Try to obtain a class reference */
			editor = getClassForEditor( editorName );
		}
		return editor;
	}

	/**
	 * Get all known editor classes
	 *
	 * @return Enumeration of all known attribute editor classes
	 */
    static public Enumeration getAttributeEditors() {
		if ( _allTable == null ) {
			return null;
		}
		return _allTable.keys();
	}

	/**
	 * Report if this is a known editor
	 *
	 * @param name Name of a class
	 * @return true if the name corresponds to a known Editor class
	 */
    static public boolean isEditor( String name ) {
		if ( _allTable == null ) {
			Debug.println( "AttributeEditorFactory.isEditor: null _allTable" );
			return false;
		}
		return (_allTable.get(name) != null);
	}

	/**
	 * Instantiate an editor appropriate for this attribute type
	 *
	 * @param name Name of the attribute
	 * @param labelText Text for label.
	 * @param rect Bounding rectangle for label.
     * @param singleValued <code>true</code> if at most one value is allowed.
	 * @param syntax Syntax, as defined in LDAPAttributeSchema
	 *
	 * @return An instance of an approprite editor, or null
	 */
    static public IAttributeEditor makeEditor( String name,
											   String labelText,
											   Rectangle rect,
											   boolean singleValued,
											   int syntax ) {
		Class editor = getEditorForAttribute( name );
		if ( editor == null ) {
			editor = getEditorForSyntax( syntax );
		}
		if ( editor == null ) {
			editor = _defaultEditor;
		}
		if ( editor != null ) {
			try {
				IAttributeEditor ed = (IAttributeEditor)editor.newInstance();
				ed.init( name, labelText, rect, singleValued );
				return ed;
			} catch ( Exception e ) {
				Debug.println( "AttributeEditorFactory.makeEditor: " +
							   "cannot instantiate " + editor.getName() );
			}
		}

		return null;
	}

	static private Hashtable _nameTable = null;
	static private Hashtable _typeTable = null;
	static private Hashtable _editorTable = null;
	static private Hashtable _allTable = null;
	static private final String _package =
	                               "com.netscape.admin.dirserv.attredit";
	static private final String _defaultEditorName =
	                               _package + ".StringAttributeEditor";
	static private Class _defaultEditor = null;
	static private final boolean _initialized = init();
}
