/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.attredit;

import java.awt.*;
import java.util.Vector;
import netscape.ldap.LDAPSchema;

/**
 * IAttributeEditor<BR>
 * Interface for a pluggable editor for a single attribute of a
 * Directory Server.<BR><BR>
 * The most common attribute editor is StringAttributeEditor.
 *
 * @version 1.0
 * @author rweltman
 **/
public interface IAttributeEditor {

    public int getValueCount();

    public boolean isReadOnly();

    public boolean isDirty();

	public void setDirty( boolean state );

	public void setDirty();

    public boolean isBinary();

    public boolean isSingleValued();

    /**
     * Sets the color of the label.
     *
     * @param color Color of label.
     **/
    public void setLabelColor( Color color );

    /**
     * Sets the font of the label.
     *
     * @param font Font of label.
     **/
    public void setLabelFont( Font font );

    /**
     * Sets the text of the label.
     *
     * @param text Text of label.
     **/
    public void setLabelText( String text );

    public String getLabelText();

    /**
     * Sets the name of the associated attribute.
     *
     * @param name Name of attribute.
     **/
    public void setName( String name );

    public String getName();

    /**
     * Adds a default value for this attribute
     **/
    public void addValue();

    /**
     * Adds a value for this attribute
     *
     * @param o A new value to be added.
     **/
    public void addValue( Object o );

    /**
     * Gets all values.
     *
	 * @return A vector of all values.
     */
    public Vector getValues();
		
    /**
     * Gets a value.
     *
     * @param index Index of the value to be retrieved.
	 * @return A single value.
     */
    public Object getValue( int index );

    /**
     * Gets a default (blank) value.
     *
	 * @return A single value.
     */
    public Object getDefaultValue();

    /**
     * Deletes a value.
     *
     * @param index Index of the value to be deleted.
     */
    public void deleteValue( int index );

    /**
     * Deletes a value.
     *
     * @param object The object to be deleted.
     **/
    public void deleteValue( Object object );

    /**
     * Delete all values.
     *
     **/
    public void deleteValues();

    /**
     * Replaces a value.
     *
     * @param index Index of the value to be replaced.
     **/
    public void replaceValue( Object o, int index );

	/**
	 * Set the set of permitted values for this editor
	 *
	 * @param allowed Vector of permitted values
	 **/
	public void setAllowedValues( Vector allowed );

	/**
	 * Set the owner component, which may receive events from this object
	 *
	 * @param parent The owner component
	 */
	public void setParent( Container parent );

	/**
	 * Get the user input field with the editor which currently has focus
	 */
    public Component getFocusField();

	/**
	 * Reports if this editor can be deleted
	 *
	 * @return true if this editor can be deleted
	 */
	public boolean isDeletable();

	/**
	 * Instantiate all UI
	 */
	public void init( String name, String label, Rectangle labelBounds,
					  boolean singleValued );

	/**
	 * Called when it is time to register all editors with the factory
	 */
	public void registerEditors();

	/**
	 * May be called to associate a schema with this editor
	 *
	 * @param schema Directory schema
	 */
	public void setSchema( LDAPSchema schema );

	public void setSelected( boolean state );

	public boolean isSelected();

	public int getFieldIndex(Object object);
}
