/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

/**
 * This is a dialog which requests the user to enter a file name.
 * It composed of a multiline label which explains the request,
 * a text field to enter the file name and a Browse button.
 * After creation, the multiline label is empty and should be
 * initialized by the caller.
 */

package com.netscape.admin.dirserv.attredit;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*; 
import javax.swing.event.*; 
import javax.swing.text.Document; 
import com.netscape.management.client.components.GenericDialog;
import com.netscape.management.client.util.MultilineLabel;
import com.netscape.admin.dirserv.DSFileDialog;
import com.netscape.admin.dirserv.DSUtil;


class RequestFileDialog extends GenericDialog
                        implements ActionListener, DocumentListener {

	/**
	 * Initialize the dialog. The request message is empty.
	 */
	public RequestFileDialog(JFrame f, String requestText) {
		super(f, DSUtil._resource.getString("attrEdit", "replace-label"), 
		      OK|CANCEL, HORIZONTAL);
		
		_requestTextLabel = new MultilineLabel(requestText);
		_requestTextLabel.setLineWrap(false);
//		DSUtil.adjustTextArea(_requestTextLabel);
		_fileNameTextField = new JTextField();
		_fileNameTextField.getDocument().addDocumentListener(this);
		_browseButton = new JButton(DSUtil._resource.getString("import", "browse-file-label"));
		_browseButton.addActionListener(this);
		
		layoutComponents();
		pack();
		adjustOKButtonState();
	}
	
	
	/**
	 * Get the file name entered by the user.
	 * This value is valid only if isCancel() return false.
	 */
	public String getFileName() {
		return _fileNameTextField.getText().trim();
	}
	


	/**
	 * Called with the Browse button is clicked.
	 */
	public void actionPerformed(ActionEvent e) {
		String fileName = DSFileDialog.getFileName(false, null, null, null, "");
		if (fileName != null) {
			_fileNameTextField.setText(fileName);
		}
	}
	
	
	/**
	 * Called when the text field is changed by the user.
	 */
	public void changedUpdate(DocumentEvent e) {
		adjustOKButtonState();
	}
	
	public void insertUpdate(DocumentEvent e) {
		adjustOKButtonState();
	}
	
	public void removeUpdate(DocumentEvent e) {
		adjustOKButtonState();
	}
	
	public void adjustOKButtonState() {
		setOKButtonEnabled(_fileNameTextField.getText().trim().length() >= 1);
	}
	
	
	/**
	 * The ugly code sequence.
	 */
	void layoutComponents() {
		Container contentPanel = getContentPane();
		contentPanel.setLayout(new GridBagLayout());
		
        GridBagConstraints gbc = new GridBagConstraints() ;
        gbc.gridx      = 0;
        gbc.gridy      = 0;
        gbc.gridwidth  = 1;
        gbc.gridheight = 1;
        gbc.weightx    = 1;
        gbc.weighty    = 0;
        gbc.fill       = gbc.HORIZONTAL;
        gbc.anchor     = gbc.NORTHWEST;
        gbc.insets     = new Insets(0, 0, 0, 0);
        gbc.ipadx      = 0;
        gbc.ipady      = 0;
        
		contentPanel.add(_requestTextLabel, gbc);

		gbc.gridy++;
        gbc.insets     = new Insets(COMPONENT_SPACE, 0, 0, 0);
		contentPanel.add(_fileNameTextField, gbc);

		gbc.gridx++;
		gbc.weightx = 0;
        gbc.insets     = new Insets(COMPONENT_SPACE, COMPONENT_SPACE, 0, 0);
		contentPanel.add(_browseButton, gbc);
	}
	
	
	/**
	 * Components
	 */
	protected MultilineLabel _requestTextLabel;
	protected JTextField _fileNameTextField;
	protected JButton _browseButton;
};
