/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.attredit;

import java.awt.Container;
import java.awt.Frame;
import java.awt.Rectangle;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import javax.swing.JComponent;
import javax.swing.JTextField;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.admin.dirserv.DefaultResourceModel;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.DSSchemaHelper;
import com.netscape.admin.dirserv.panel.SimpleDialog;
import netscape.ldap.LDAPSchema;
import netscape.ldap.LDAPObjectClassSchema;

/**
 * ObjectClassAttributeEditor<BR>
 * Attribute editor for the ObjectClass. It differs from StringAttributeEditor
 * in that it MUST have the value "top", at least, and that the attribute
 * can't be deleted.
 *
 * @version 1.0
 * @author rweltman
 **/
public class ObjectClassAttributeEditor extends StringAttributeEditor {

    /**
     * Construct an empty one.
     *
     **/
    public ObjectClassAttributeEditor() {
		super();
    }

	/**
	 * Disable "top", since it is illegal to delete it or change it
	 */
    protected JComponent fetchField( Object value ) {
		JTextField field = (JTextField)super.fetchField( value );
//		field.setEnabled( false );
		field.setEditable( false );
		return field;
	}

	/**
	 * Add a default value to this attribute.
	 * Since this is an object class, there are a finite number of
	 * allowed values, so we put up a dialog to allow selecting one.
	 */
	public void addValue() {
		if ( (_allowed == null) || (_allowed.size() < 1) ) {
			addValue( getDefaultValue() );
			return;
		}
		/* Filter out values already present */
		Vector allowed = new Vector();
		Enumeration e = _allowed.elements();
		Vector v = getValues();
		while( e.hasMoreElements() ) {
			Object o = e.nextElement();					
			if ( v.indexOf( o ) < 0 ) {
				/* Check that the objectclass is not a forbidden objectclass */
				boolean isForbidden = false;
				for (int i=0; i<DSSchemaHelper.FORBIDDEN_OBJECTCLASSES.length; i++) {
					if (((String)o).equalsIgnoreCase(DSSchemaHelper.FORBIDDEN_OBJECTCLASSES[i])) {
						isForbidden = true;
						break;
					}
				}
				if (!isForbidden) {
					allowed.addElement( o );
				}
			}
		}
		NewObjectClassPanel child =
			new NewObjectClassPanel( new DefaultResourceModel(),
									 allowed );
		SimpleDialog dlg = new SimpleDialog(getOwnerFrame(), child);
		dlg.getAccessibleContext().setAccessibleDescription(_resource.getString(_section,
																				"dialog-description"));
		dlg.packAndShow();		
		Object[] values = child.getSelectedItems();
		if (values !=null) {
			for (int i=0; i<values.length; i++) {
				String name = (String)values[i];				
				/* Need to make sure all superiors are included */
				if (_schema != null) {
					String value;
					Vector parents;
					parents = DSSchemaHelper.getObjectClassVector(name, _schema);
					for (int j=0; j<parents.size(); j++) {
						value = (String)(parents.elementAt(j));
						if (!(value.equalsIgnoreCase("top")) && (value.length() > 0) ) {
							boolean newValue = true;
							for (int idx=0; idx < _values.size(); idx++) {
								String oc = (String) _values.elementAt(idx);
								// ObjectClass is case-insensitive
								if (oc.equalsIgnoreCase( value )) {
									newValue = false;
									break;
								}
							}
							if (newValue) {
								addValue( value );
							}
						}
					}			
				} else {
					if (!(name.equalsIgnoreCase("top")) && (name.length() > 0) ) {
						boolean newValue = true;
						for (int idx=0; idx < _values.size(); idx++) {
							String oc = (String) _values.elementAt(idx);
							// ObjectClass is case-insensitive
							if (oc.equalsIgnoreCase( name )) {
								newValue = false;
								break;
							}
						}
						if (newValue) {
							addValue( name );
						}
					}
				}						
			}
		}
	}

    /**
     * Delete a value. But do not allow deleting "top"!
     *
     * @param index Index of the value to be deleted.
     **/
	public void deleteValue( int index ) {
		String name = (String)getValue( index );
		if ( !( name.equalsIgnoreCase( "top" ) ) ) {
			/* Can't delete a value if it is a superior to some other
			   value: in the algorithm we assume that all the chain of inheritance
			   of the classes is included.  For example, if we have 'organizationalperson'
			   we have 'person' and 'top' too */
			if ( _schema != null ) {
				Enumeration en = getValues().elements();
				while( en.hasMoreElements() ) {
					String val = (String)en.nextElement();
					LDAPObjectClassSchema oschema =
						_schema.getObjectClass( val );
					if ( oschema != null ) {
						String[] superiors = DSSchemaHelper.getSuperiors(oschema);
						if (superiors != null) {
							for (int i=0 ; i< superiors.length ; i++) {							
								String superior = superiors[i];
								if ( superior != null ) {
									if ( superior.equalsIgnoreCase( name ) ) {
										String[] args = { name, val };
										DSUtil.showErrorDialog( _parent,
																"descendant-exists",
																args,
																_section,
																_resource );
										return;
									}								
								}
							}
						}
					}
				}
			}
			super.deleteValue( index );
		}
	}

	/**
	 * Set the set of permitted values for this editor.
	 *
	 * @param allowed Vector of permitted values
	 **/
	public void setAllowedValues( Vector allowed ) {
		_allowed = allowed;
	}

	/**
	 * This attribute may not be deleted.
	 */
    public boolean isDeletable() {
		return false;
	}

    /**
     * Find the hosting Frame
     */
    private Frame getOwnerFrame() {
		for (Container p = getParent(); p != null; p = p.getParent()) {
			if (p instanceof Frame) {
				return (Frame)p;
			}
		}
		return null;
    }

	private Vector _allowed = null;
    private final static String _section = "objectclass";
	private static ResourceSet _resource =
	      new ResourceSet("com.netscape.admin.dirserv.propedit.propedit");
}
