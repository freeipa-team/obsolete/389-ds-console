/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.attredit;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.io.FileInputStream;
import javax.swing.*;

import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.admin.dirserv.DSUtil;
import com.netscape.admin.dirserv.panel.UIFactory;

/**
 * BinaryAttributeEditor<BR>
 * Attribute editor for binary-valued attributes. A single label may
 * be visible, and have 0 or more buttons (one for each value). If
 * the editor is single-valued, at most one button is allowed.
 *
 * @version 1.0
 * @author rweltman
 **/
public class BinaryAttributeEditor extends AttributeEditor
                                   implements ActionListener {

    /**
     * Construct an empty one.
     *
     **/
    public BinaryAttributeEditor() {
		super();
    }

    /**
     * Gets a default (blank) value.
     *
	 * @return A single value.
     */
    public Object getDefaultValue() {
		return new byte[0];
	}

    protected JComponent fetchField( Object value ) {
		JButton field;
		int index = 0;
		if ( _blankField != null ) {
			field = (JButton)_blankField;
			_blankField = null;
		} else {
			field = (JButton)makeField();
			index = getValueCount() - 1;
		}
		field.setOpaque( true );
		field.setBorder( _textBorder );
		field.setEnabled( true );
		field.addFocusListener( this );
		field.addMouseListener( this );
		Dimension dim = new Dimension(18, 22);
		field.setMinimumSize( dim );
		field.setPreferredSize( dim );
		if ( _textFont != null )
			field.setFont( _textFont );
		/* The action command is the index of this value */
		field.setActionCommand( Integer.toString( index) );
		return field;
	}

    /**
	 * Add a value to this attribute
	 *
	 * @param value The new value
	 */
    public void addValue( Object value ) {
//		Debug.println( "BinaryAttributeEditor.addValue" );
		super.addValue( value );
		int i = getValueCount();
		if ( i > 0 ) {
			update( i-1 );
		}
	}

   /**
	* Update the button at a particular index
	*
	* @param index Which button to update.
	*/
    protected void update( int index ) {
//		Debug.println( "BinaryAttributeEditor.update: " + index );
		JButton field = (JButton)_fields.elementAt( index );
		field.setText( _buttonText );
	}

    protected JComponent makeField() {
//		Debug.println( "BinaryAttributeEditor.makeField" );
		JButton button = UIFactory.makeJButton( this, "" );
		return button;
	}


    /**
	 * Get the last component that can have focus, for chaining.
	 *
	 * @return The last component internally that can have focus.
	 */
    public JComponent getLastFocusableComponent() {
		int i = getValueCount();
		if ( i > 1 )
			return (JComponent)_fields.elementAt( i - 1 );
		return this;
	}

    /**
     *  Handle incoming event from button
     *
     * @param e event
     */
    public void actionPerformed(ActionEvent e) {
//		Debug.println( "BinaryAttributeEditor.actionPerformed: " +
//					   e.getSource() +
//					   ", command = " + e.getActionCommand() );
        if ( e.getSource() instanceof JButton ) {
			RequestFileDialog dlg = makeRequestFileDialog(null);
			dlg.show();
			dlg.dispose();
			if (!dlg.isCancel()) {
				String fileName = dlg.getFileName();
				if ( fileName != null ) {
					try {
						FileInputStream f = new FileInputStream( fileName );
						int nBytes = f.available();
						byte[] buffer = new byte[nBytes];
						f.read( buffer );
						int index = Integer.parseInt( e.getActionCommand() );
						replaceValue( buffer, index );
						setDirty();
						update( index );
					} catch ( Exception ex ) {
						DSUtil.showUnknownErrorDialog( null,
													   ex,
													   "111-title" );
					}
				}
			}
        }
    }
	
	/**
	 * Make the RequestFileDialog instance.
	 * This method is overriden by subclasses to tailor the
	 * aspect of the RequestFileDialog.
	 */
	protected RequestFileDialog makeRequestFileDialog(JFrame f) {
		String requestText = _resource.getString( _section, "binary-request-label" );
		return new RequestFileDialog(null, requestText);
	}

	static private ResourceSet	_resource = DSUtil._resource;
	private static final String _section = "attrEdit";
	private static String _buttonText =
	    _resource.getString( _section, "replace-label" );
}
