/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.attredit;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.Border;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.panel.UIFactory;
import netscape.ldap.LDAPSchema;

/**
 * AttributeEditor<BR>
 * Base class for pluggable attribute editors for a Directory Server
 * entry.<BR><BR>
 * The most common attribute editor is StringAttributeEditor.
 *
 * @version 1.0
 * @author rweltman
 **/
public class AttributeEditor extends JPanel
							 implements IAttributeEditor,
										MouseListener,
										FocusListener {

	/**
	 * Construct an editor with no initial values.
	 *
	 **/
    public AttributeEditor() {
		addMouseListener( this );
		addFocusListener( this );
	}

    /**
     * Sets the bounding rectangle of the label.
     *
     * @param rect Initial bounding rectangle of label.
     **/
    public void setLabelRect( Rectangle rect ) {
		createLabel();
		_labelBounds = rect;
		_label.setBounds( _labelBounds );
		Dimension d = new Dimension( rect.width, rect.height );
		_label.setPreferredSize( d );
		_label.setMinimumSize( d );
	}

	/**
	 * Set the owner component, which may receive events from this object
	 *
	 * @param parent The owner component
	 */
    public void setParent( Container parent ) {
		_parent = parent;
		if ( _parent instanceof MouseListener ) {
			addMouseListener( (MouseListener)_parent );
		}
	}

    protected void createLabel( String text) {
		if ( _label == null ) {
			_label = UIFactory.makeJLabel( text );
			if ( _labelFont != null )
				_label.setFont( _labelFont );
//			_label.setForeground( _labelColor );
			_label.setHorizontalAlignment( SwingConstants.RIGHT );
		} else {
			_label.setText( text );
		}
	}

    protected void createLabel() {
		String text = (getLabelText() != null) ? getLabelText() : "";
		createLabel( text );
	}

	/**
	 * Initialize an editor with no initial values.
	 *
	 **/
    public void init() {
		setBorder( BorderFactory.createLoweredBevelBorder() );
		setLayout( new GridBagLayout() );
		/* Remember the default border */
		JComponent field = makeField();
		_textBorder = field.getBorder();		

		addComponent( getLabel(), 0, 0,
					  1, 1,
					  GridBagConstraints.VERTICAL,
					  GridBagConstraints.NORTH,
					  0.0, 1.0);
		_list = new JPanel();
		_list.setLayout( new ListLayout() );
		addComponent( _list, 1, 0,
					  GridBagConstraints.REMAINDER, 1,
					  GridBagConstraints.HORIZONTAL,
					  GridBagConstraints.WEST,
					  1.0, 0.0);
	}											

	/**
	 * Initialize an editor with no initial values.
	 *
	 * @param name Attribute name.
	 * @param label Text for label.
	 * @param labelBounds Bounding rectangle for label.
	 * @param singleValued <code>true</code> if at most one value is allowed.
	 **/
	public void init( String name, String label, Rectangle labelBounds,
					  boolean singleValued ) {
		setName( name );
		_singleValued = singleValued;
		setLabelText( label );
		setLabelRect( labelBounds );
		setLabelToolTip( name );
		init();
	}

	/**
	 * Identifies whether or not this component can receive the focus.
	 * A disabled button, for example, would return false. Overridden
	 * from JComponent to always return true.
	 *
	 * @return true if this component can receive the focus
	 */
	public boolean isFocusTraversable() {
		/* Tab through values, if any; otherwise allow focus on this
		   component */
		return ( _fields.size() < 1 );
	}

	public int getValueCount() {
		return _values.size();
	}

	public boolean isReadOnly() {
		return _readOnly;
	}

	public boolean isDirty() {
		return _dirty;
	}

	public void setDirty( boolean state ) {
		_dirty = state;
	}

	public void setDirty() {
		setDirty( true );
	}

	public boolean isBinary() {
		return true;
	}

	public boolean isSingleValued() {
		return _singleValued;
	}

	/**
	 * Sets the color of the label.
	 *
	 * @param color Color of label.
	 **/
	public void setLabelColor( Color color ) {
		_labelColor = color;
	}

	/**
	 * Sets the font of the label.
	 *
	 * @param font Font of label.
	 **/
	public void setLabelFont( Font font ) {
		_labelFont = font;
	}

	/**
	 * Sets the text of the label.
	 *
	 * @param text Text of label.
	 **/
	public void setLabelText( String text ) {
		_labelText = text;
		createLabel( text );
	}

	/**
	 * Sets the bounding box of the label.
	 *
	 * @param bounds Bounding box of label.
	 **/
	public void setLabelBounds( Rectangle bounds ) {
		_labelBounds = bounds;
	}

	/**
	 * Sets a tool tip for the label.
	 *
	 * @param tooltip the toolTip for the label.
	 **/
	public void setLabelToolTip( String toolTip ) {
		_label.setToolTipText(toolTip);
	}										


	/**
	 * Add a value to this attribute
	 *
	 * @param value The new value
	 */
	public void addValue( Object value ) {
//		Debug.println( "AttributeEditor.addValue: " +
//					   getName() + " <" + value + ">" );
		JComponent field;
		_values.addElement( value );
		field = fetchField( value );
		if (_label != null) {
			_label.setLabelFor(field);
		}
		/* Keep track of the UI component for this value */
		addField( field );
		_list.add( field );
		field.requestFocus();

		Dimension d = new Dimension( _labelBounds.width, _labelBounds.height );
		field.setPreferredSize( d );
		field.setMinimumSize( d );
		
		invalidate();
		validate();
		setDirty();
	}

	/**
	 * Add a default value to this attribute
	 */
	public void addValue() {
		addValue( getDefaultValue() );
	}

	/**
	 * Gets all values.
	 *
	 * @return A vector of all values.
	 */
	public Vector getValues() {
		return _values;
	}
		
	/**
	 * Gets a value.
	 *
	 * @param index Index of the value to be retrieved.
	 * @return A single value.
	 */
    public Object getValue( int index ) {
		if (_values.size() > index) {
			return _values.elementAt( index );
		} 
		return null;
	}											
	/**
	 * Gets a default (blank) value.
	 *
	 * @return A single value.
	 */
	public Object getDefaultValue() {
		return null;
	}

	/**
	 * Delete a value.
	 *
	 * @param index Index of the value to be deleted.
	 **/
	public void deleteValue( int index ) {
		Debug.println( "AttributeEditor.deleteValue: index " + index );
		Vector old = getValues();
		Vector v = new Vector();
		for( int i = 0; i < old.size(); i++ ) {
			if ( i != index ) {
				v.addElement( old.elementAt( i ) );
//				Debug.println( "AttributeEditor.deleteValue: " +
//							   "saving " + old.elementAt(i) + " at " + i );
			}
		}
		int count = v.size();
		deleteValues();
		init();

		for( int i = 0; i < count; i++ ) {
			try {
				addValue( v.elementAt( i ) );
			} catch ( Exception e ) {
			}
		}
		if ( count > 0 ) {
			((Component)_fields.elementAt( 0 )).requestFocus();
		} else {
			requestFocus();
		}
		setDirty();
	}

	/**
	 * Deletes a value.
	 *
	 * @param object The object to be deleted.
	 **/
	public void deleteValue( Object object ) {
		Debug.println( "AttributeEditor.deleteValue: " +
					   object.getClass().getName() );
		if ( object == null ) {
			return;
		}
		int index = _fields.indexOf( object );
		if ( index >= 0 ) {
			deleteValue( index );
		} else {
			Debug.println( "No recollection of " + object +
								", <" + ((JTextField)object).getText() +
								">" );
		}
	}

	/**
	 * Delete all values.
	 *
	 **/
	public void deleteValues() {
		/* Remove the UI components from the panel */
		removeAll();
		/* Remove them from the vector */
		_fields.removeAllElements();
		/* Delete the values themselves */
		_values.removeAllElements();
		setDirty();
	}

	/**
	 * Replaces a value.
	 *
	 * @param index Index of the value to be replaced.
	 **/
    public void replaceValue( Object o, int index ) {
		_values.setElementAt( o, index );
	}

	/**
	 * Set the set of permitted values for this editor. Does nothing
	 * for the general case.
	 *
	 * @param allowed Vector of permitted values
	 **/
	public void setAllowedValues( Vector allowed ) {
	}

	public int getFieldIndex(Object object) {
		int index = _fields.indexOf( object );
		return index;
	}


	/**
	 * May be called to associate a schema with this editor
	 *
	 * @param schema Directory schema
	 */
	public void setSchema( LDAPSchema schema ) {
		_schema = schema;
	}

	/**
	 * Adds a field for this attribute
	 *
	 * @param field A new field to be added.
	 **/
	public void addField( Component field ) {
		_fields.addElement( field );
		_focusField = field;
	}

	public String getLabelText() {
		return _labelText;
	}

	public void setName( String name ) {
		_name = name;
	}

	public String getName() {
		return _name;
	}

	protected JLabel getLabel() {
		return _label;
	}

	public void setContextMenu( JPopupMenu menu ) {
//		_contextMenu = menu;
//		_label.add( menu );
	}

    /**
     * Find the hosting Window
     */
    protected Window getOwnerWindow() {
		for (Container p = getParent(); p != null; p = p.getParent()) {
			if (p instanceof Window) {
				return (Window)p;
			}
		}
		return null;
    }

	private boolean isFocusInHierarchy() {
		/* Return true if we or one of our children has focus */
		Window w = getOwnerWindow();
		Component comp = w.getFocusOwner();
		return ( equals( comp ) || isAncestorOf( comp ) );
	}

	public void setSelected( boolean state ) {
//		Debug.println( "AttributeEditor.setSelected: " + this + ", " +
//					   state );
		_isSelected = state;
		if ( _isSelected ) {
			/* If we or one of our children does not have focus, get
			   focus */
			if ( !isFocusInHierarchy() ) {
				requestFocus();
			}
		}
		repaint();
	}

	public boolean isSelected() {
		return _isSelected;
	}

	public void focusGained(FocusEvent e) {
		Component field = e.getComponent();
		Debug.println( "AttributeEditor.focusGained: focus on " +
					   field );
// 		if ( field instanceof JTextField ) {
// 			Debug.println( "AttributeEditor.focusGained: text = " +
// 						   ((JTextField)field).getText() );
// 		}
		/* Kingpin may be using different class loaders for the base
		   AttributeEditor and for dynamically loaded ones, so you can't
		   do instanceof any more */
		if ( AttributeEditorFactory.isEditor(field.getClass().getName()) ) {
			/* Selected the editor - set focus to first value field */
 			if ( _fields.size() > 0 ) {
 				Debug.println( "AttributeEditor.focusGained: requesting " +
 							   "focus to " + _fields.elementAt(0) );
 				((Component)_fields.elementAt( 0 )).requestFocus();
 			}
		} else if ( !(field instanceof JLabel) ) {
			_focusField = field;
			Debug.println( "AttributeEditor.focusGained: new focus is on " +
					   _focusField );
		}
		/* Pass the event on to our container */
		Container p = getParent();
		while ( (p != null) && !(p instanceof FocusListener) ) {
			p = p.getParent();
		}
		if ( p != null ) {
			Debug.println( "AttributeEditor.focusGained: dispatching to " +
						   p );
			((FocusListener)p).focusGained( e );
		}
	}

	public void focusLost(FocusEvent e) {
		if ( !e.isTemporary() ) {
//			Debug.println( "AttributeEditor.focusLost: " + this );
			repaint();
		}
	}

	public Component getFocusField() {		
		return _focusField;
	}

	public boolean isDeletable() {
		return true;
	}

	protected JComponent fetchField( Object value ) {
		return null;
	}

	protected JComponent makeField() {
		return null;
	}

	/** Overriden from JPanel.
	 *	The color used is otherwise the one returned by getBackground().
	 */
	public void paintComponent(Graphics g) {
		if ( !_isSelected ) {
			setBorder( BorderFactory.createLoweredBevelBorder() );
			super.paintComponent( g );
		} else {
			setBorder( BorderFactory.createRaisedBevelBorder() );
			super.paintComponent( g );
//			paintFocused( g );
		}
	}

/* If we want to show the selected attribute editor with a different color
	private void paintFocused(Graphics g) {
		if( isOpaque() ) {
			Rectangle r = g.getClipBounds();
			if(r == null) {
				r = getBounds();
				r.x = r.y = 0;
			}
			g.setColor( Color.green );
			g.fillRect(r.x,r.y,r.width,r.height);
		}
	}
*/

	/**
	 * Get the last component that can have focus, for chaining.
	 *
	 * @return The last component internally that can have focus.
	 */
	public JComponent getLastFocusableComponent() {
		return this;
	}

	public void mouseClicked(MouseEvent e) {
// 		Debug.println( "AttributeEditor.mouseClicked " + e + 
// 					   ", parent = " + getParent() );
//		if (e.getClickCount() == 2) {
//		} else {
//		}
		/* Pass the event on to our container */
		Container p = getParent();
		while ( (p != null) && !(p instanceof MouseListener) ) {
			p = p.getParent();
		}
		if ( p != null ) {
			p.dispatchEvent( e );
		}
	}

	public void mousePressed(MouseEvent e) {
// 		Debug.println( "AttributeEditor.mousePressed: on " +
// 			e.getComponent() );
		/* Pass the event on to our container */
		Container p = getParent();
		while ( (p != null) && !(p instanceof MouseListener) ) {
			p = p.getParent();
		}
		if ( p != null ) {
			((MouseListener)p).mousePressed( e );
		}
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	/**
	 * Called when it is time to register all editors with the factory
	 * Default implementation does nothing.
	 */
	public void registerEditors() {
	}

	/**
	 * Adds grid bag components.
	 */
	void addComponent( JComponent comp,
					   int gridx, int gridy,
					   int gridw, int gridh,
					   int fill, int anchor,
					   double weightx, double weighty,
					   int position ) {
		if ( _c == null ) {
			setLayout( new GridBagLayout() );
			_c = new GridBagConstraints();
			_c.insets = new Insets(1,6,1,6);
		}
		_c.gridx = gridx;
		_c.gridy = gridy;
		_c.gridwidth = gridw;
		_c.gridheight = gridh;
		_c.fill = fill;
		_c.anchor = anchor;
		_c.weightx = weightx;
		_c.weighty = weighty;
		if ( position == -1 ) {
			add( comp, _c );
		} else {
			add( comp, _c, position );
		}
	}

	void addComponent( JComponent comp,
					   int gridx, int gridy,
					   int gridw, int gridh,
					   int fill, int anchor,
					   double weightx, double weighty ) {
		addComponent( comp, gridx, gridy, gridw, gridh, fill, anchor,
					  weightx, weighty, -1 );
	}

	class ListLayout implements LayoutManager2 {
		ListLayout() {}
		public void layoutContainer( Container parent ) {
			int y = 0;
			for ( int i = 0 ; i < parent.getComponentCount(); i++ ) {
				Component thisComp = parent.getComponent( i );
				if ( !thisComp.isVisible() ) continue;

				Dimension d = thisComp.getPreferredSize();
				thisComp.setBounds( 0, y, parent.getBounds().width, d.height );
				y += d.height;
			}
		}

		public Dimension getDimensions( Container parent ) {
			int x = 0;
			int y = 0;
			for ( int i = 0 ; i < parent.getComponentCount(); i++ ) {
				Component thisComp = parent.getComponent( i );
				if ( !thisComp.isVisible() ) continue;

				Dimension d = thisComp.getPreferredSize();
				y += d.height;
				x = Math.max( x, d.width );
			}
			return new Dimension( x, y );
		}

		public Dimension preferredLayoutSize( Container parent ) {
			return getDimensions( parent );
		}

		public Dimension minimumLayoutSize( Container parent ) {
			return getDimensions( parent );
		}

		public Dimension maximumLayoutSize( Container parent ) {
			return getDimensions( parent );
		}


		public void addLayoutComponent( String constraint, Component comp ) {}
		public void addLayoutComponent( Component comp, Object constraint ) {}
		public void removeLayoutComponent( Component comp ) {}
		public void invalidateLayout( Container parent ) {}
		public float getLayoutAlignmentX( Container parent ) { return 0.5f; }
		public float getLayoutAlignmentY( Container parent ) { return 0.5f; }
	}

	protected Container getEditorParent() {
		return _parent;
	}

    protected Container _parent = null;
	private JPanel _list;
	protected Vector _values = new Vector();
	protected LDAPSchema _schema = null;
	private Rectangle _labelBounds = null;
	private JLabel _label;
	private String _labelText = null;
	private String _name = null;
	private Color  _labelColor = UIManager.getColor("text");
	private Font   _labelFont = null;
	private boolean _readOnly = false;
	private boolean _singleValued = false;
    private boolean _initialized = false;
	private boolean _dirty = false;
	protected JPopupMenu _contextMenu = null;
	protected Vector _fields = new Vector();
	private boolean _isSelected = false;
	protected Component _focusField = null;
	private GridBagConstraints _c = null;
	protected JComponent _blankField = null; // Placeholder for no values
	protected Border _textBorder = null;
//	protected Color _textBackground = SystemColor.window;
	protected Color _textBackground = Color.white;
	protected Color _textColor = UIManager.getColor("textText");
	protected Font _textFont = null;
	private Rectangle _defaultBounds = null;
}
