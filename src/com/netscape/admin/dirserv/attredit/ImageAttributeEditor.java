/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.attredit;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import com.netscape.management.client.util.Debug;
import com.netscape.admin.dirserv.DSUtil;

/**
 * BinaryAttributeEditor<BR>
 * Attribute editor for binary-valued attributes. A single label may
 * be visible, and have 0 or more buttons (one for each value). If
 * the editor is single-valued, at most one button is allowed.
 *
 * @version 1.0
 * @author rweltman
 **/
public class ImageAttributeEditor extends BinaryAttributeEditor {

    /**
     * Construct an empty one.
     *
     **/
    public ImageAttributeEditor() {
		super();

		/* Make sure all images get a chance to update when we're resized */
		addComponentListener( new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				int count = getValueCount();
				for( int i = 0; i < count; i++ )
					update( i );
			}
		} );
    }
	
	/**
	 * Overrides the parent method.
	 * We create a RequestFileDialog with a label
	 * specific to this editor.
	 */
	protected RequestFileDialog makeRequestFileDialog(JFrame f) {
		String requestText = DSUtil._resource.getString( "attrEdit", "image-request-label" );
		return new RequestFileDialog(null, requestText);
	}
	

   /**
	* Update the button at a particular index
	*
	* @param index Which button to update.
	*/
    protected void update( int index ) {
		try {
			JButton field = (JButton)_fields.elementAt( index );
			byte[] value = null;
			value = (byte[])getValue( index );
			if ( (value == null) || (value.length == 0) ) {
				/* If there is no image yet, just allow the default label
				   and no icon */
				super.update( index );
				return;
			} else {
				/* We have a value, so nix the text label */
				field.setText( null );
			}
			Image rawImage = null;
			try {
				rawImage = Toolkit.getDefaultToolkit().createImage(
					                                 value );
			} catch ( Exception e ) {
				Debug.println( e.toString() );
			}
			if ( rawImage == null ) {
				Debug.println( "ImageAttributeEditor.update: " +
									"failed to load image" );
				field.setText( "failed" );
			} else {
				Dimension size = field.getSize();
				size.height =  Math.max(labelHeight, size.height);
				Dimension originalSize = getSize();
				int fieldWidth = size.width;
				int fieldHeight = size.height;
				int originalHeight = fieldHeight;
				Image buffer;
				/* Make the scaled image no wider than the button, and then
				   adjust the height to keep the proportions */
				ImageIcon icon = new ImageIcon( rawImage );
				int h = icon.getIconHeight();
				int w = icon.getIconWidth();
				Debug.println( "ImageAttributeEditor.update: " +
							   "loaded image " + w +
							   "x" + h + " pixels" );
				size.width = Math.min( size.width, w );
				fieldHeight = size.height = size.width * h / w;
				/* Scale the image? */
				if ( w <= size.width ) {
					buffer = rawImage;
				} else {
					/* If there is image data, scale it */
					buffer = createImage( size.width, size.height );
					Graphics bufferGraphics = buffer.getGraphics();
					bufferGraphics.drawImage( rawImage, 0, 0,
											  size.width, size.height,
											  0, 0, w, h, null );
				}

				field.setIcon( new ImageIcon( buffer ) );
//				size.width = fieldWidth;
				size.width += 6;
				size.height += 6;
				field.setMinimumSize( size );
				field.setPreferredSize( size );
				Debug.println( "ImageAttributeEditor.update: " +
							   "setting minimum field size = " + size );
				if ( fieldHeight > originalHeight ) {
					originalSize.height += (fieldHeight + 6 - originalHeight);
					Debug.println( "ImageAttributeEditor.update: " +
								   "setting minimum size = " + originalSize );
					setMinimumSize( originalSize );
					setPreferredSize( originalSize );
					forceLayout();
				} else {
					invalidate();
					validate();
				}
			}
			field.repaint();
		} catch ( Exception e ) {
			Debug.println( "ImageAttributeEditor.update: " + e );
		}
	}

    private void forceLayout() {
		Component comp = this;
		for (Container p = comp.getParent(); p != null;
			 p = p.getParent()) {
			if ( (p instanceof Frame) || (p instanceof Dialog) ) {
				p.invalidate();
				p.validate();
				break;
			}
		}
	}
	private static final int labelHeight = 23;
}
