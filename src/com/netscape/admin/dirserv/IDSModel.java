/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.event.FocusEvent;
import java.util.Vector;
import javax.swing.JFrame;
import com.netscape.management.client.*;
import com.netscape.management.client.console.ConsoleInfo;
import netscape.ldap.LDAPSchema;

/**
 *	Netscape Directory Server 4.0 base resource model interface.
 *
 * @author  rweltman
 * @version %I%, %G%
 * @date	 	11/1/97
 */

public interface IDSModel extends IResourceModel {

    /**
     * Return the global console information reference.
     *
     * @return The global console information reference.
     **/
    public ConsoleInfo getConsoleInfo();

    /**
     * Return the Directory-specific information reference.
     *
     * @return The Directory-specific information reference.
     **/
    public ConsoleInfo getServerInfo();
    
    /**
	 * Get new authentication credentials from a dialog, reauthenticate
	 * the connection in _serverInfo.
	 *
	 * @return <CODE>true</CODE> if the connection was reauthenticated.
	 */
    public boolean getNewAuthentication();

    /**
	 * Get new authentication credentials from a dialog, reauthenticate
	 * the connection in _serverInfo.
	 *
	 * @param notifyListeners If true, the authentication change listeners
	 *                        will be updated immediately.  If false, the
	 *                        caller will have to call the
	 *                        notifyAuthChangeListeners() method of this
	 *                        class.  This is useful if the reauthentication
	 *                        happens during some extended operation, such as
	 *                        subtree deletion, and it is not desirable to
	 *                        notify the listeners until the operation is
	 *                        completed.
	 *
	 * @return <CODE>true</CODE> if the connection was reauthenticated.
	 */
    public boolean getNewAuthentication(boolean notifyListeners);
	
	/**
	 * If getNewAuthentication was called with the notifyListeners set to false,
	 * this method must be called.
	 */
	public void notifyAuthChangeListeners();

	public JFrame getFrame();
	
	/**
	 * Get the schema of the Directory instance
	 *
	 * @return A reference to a schema object.
	 */
	public LDAPSchema getSchema();

	/**
	 * Sets a reference to the schema of the Directory instance
	 *
	 * @param schema A reference to a schema object.
	 */
	public void setSchema( LDAPSchema schema );

	/**	
	 * Add an object to be notified on authentication changes.
	 *
	 * @param l Object to be notified.
	 */
	public void addAuthenticationChangeListener(
		                        IAuthenticationChangeListener l );

	/**	
	 * Set the list of objects to be notified on authentication changes.
	 *
	 * @param v vector of objects to be notified.
	 */
	public void setAuthenticationChangeListener(Vector v);

    public IPage getSelectedPage();


    /**
     * Informs the tree that a particular resource object's structure
	 * has changed
     * and its view needs to be updated.
     * Called by serverloc\StatusThread.java
     * @see EventListenerList
     **/
     public void fireTreeStructureChanged(ResourceObject node);

     /**
      *  sends IResourceModelListener.changeFeedbackCursor()
      * notifications to all listeners
	  */
    public void fireChangeFeedbackCursor(IPage viewInstance,
										   int feedbackIndicatorType);

    public void initialize( Object root );

	public void removeElement( IDSEntryObject deo );

     /**
	  * sends IResourceModelListener.addStatusItem() notifications to
	  * all listeners
	  */
    public void fireAddStatusItem(IPage viewInstance, IStatusItem item);

	/**
	 *  sends IResourceModelListener.removeStatusItem() notifications
	 * to all listeners
	 **/
    public void fireRemoveStatusItem(IPage viewInstance, IStatusItem item);

	/**
	 *  sends IResourceModelListener.changeStatusItemState()
	 * notifications to all listeners
	 **/
     public void fireChangeStatusItemState(IPage viewInstance,
										   String itemID,
										   Object state);

	/**
	 * Called when a menu item is selected
	 */
    public void actionMenuSelected(IPage viewInstance, IMenuItem item);

	/**
	 * Called when an object of the Model receives focus.
	 * This is implemented in the Directory console, because Kingpin
	 * doesn't support it yet.
	 */
	public void focusGained( FocusEvent e );

	/**
	 * Used by a client other than the framework to set the currently
	 * selected objects.
	 * This is implemented in the Directory console, because Kingpin
	 * doesn't support it yet.
	 */
	public void setSelected( IPage viewInstance,
							 IResourceObject[] selection,
							 IResourceObject[] previousSelection);

    /**
      * Adds a listener that is interested in receiving selection events.
	  * Called by panels
      */
	public void addIDSResourceSelectionListener(
	                                    IDSResourceSelectionListener l);

    /**
      * Removes previously added IDSResourceSelectionListener.
	  * Called by panels
      */
	public void removeIDSResourceSelectionListener(
	                                    IDSResourceSelectionListener l);

    /**
      * Adds a client who can report unsaved changes.
	  * Called by panels
      */
	public void addChangeClient( IChangeClient client );

    /**
      * Removes previously added Change client.
	  * Called by panels
      */
	public void removeChangeClient( IChangeClient client );

	public void pageSelected( IFramework framework, IPage viewInstance );

	/**
	 * Notifies that the root DN changed
	 */
	public void rootDNChanged( String rootDN );

    public void setSelectedNode( IResourceObject deo );

    public void setWaitCursor( boolean on );

	/**
	 * Notifies that directory contents have changed
	 */
	public abstract void contentChanged();

	/**
	 * Tells this model that it needs to refresh its view
	 */
	public void refreshView();
}

