/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.cmdln;

/**
 *	Netscape Admin Server 4.0 task for stopping the directory server.
 *
 * @author  RJP
 * @version %I%, %G%
 * @date	 	1/15/98
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.CGITask
 */
public class DSStop extends DSCommandLine {
	public DSStop() {
	}

	public static void main( String[] args ) {
		try {
			/* Make a new commandline class */
			DSStop task = new DSStop();

			/* extract parameters from the arguments list */
			if (task.extractParameters("", args) == null) {
				task.doUsage("DSStop");
				System.exit( 1 );
			}
			
			/* Run DSCommandLine run method */
			if ( !task.run("Stop") ) {
				task.printError();
				System.exit( -1 );
			}
		} catch ( Exception e ) {
			System.err.println( e.toString() );
			System.exit( -1 );
		}		
		System.exit( 0 );
	}
}
