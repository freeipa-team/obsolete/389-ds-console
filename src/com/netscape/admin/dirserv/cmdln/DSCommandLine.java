/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.cmdln;

import javax.swing.UIManager;
import javax.swing.JFrame;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.IPage;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.comm.CommRecord;
import netscape.ldap.util.GetOpt;
import com.netscape.admin.dirserv.task.CGITask;
import com.netscape.admin.dirserv.DSUtil;

/**
 *	Netscape Admin Server 4.0 class for dealing with Directory Server
        commandine programs.
 *
 * @author  RJP
 * @version %I%, %G%
 * @date	 	1/15/98
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.CGITask
 */
public class DSCommandLine extends CGITask {
	/**
	 *	Constructor for the commandline utility
	 */
	public DSCommandLine() {
		super();

		/* Set look and feel */

		try { 
			UIManager.setLookAndFeel( "com.netscape.management.nmclf.SuiLookAndFeel" ); 
		} catch (Exception e) { 
			System.err.println("Cannot load nmc look and feel."); 
			System.exit(-1);
		} 

	}

	public boolean run (String operation) {
		/*_sCmd = "bin/shutdown?admin"*/
		_sCmd = "Tasks/Operation/" + operation;

		_consoleInfo = new ConsoleInfo();
		_consoleInfo.setAdminURL( "http://" + m_host + ":" + m_port + "/" );
		if (operation.equals("Start?InstanceName=slapd-" + m_instance)) {
		  _consoleInfo.put( "ServerInstance", "slapd" ) ;
		} else {
		  _consoleInfo.put( "ServerInstance", "slapd-" + m_instance ) ;
		}
		setAsync( true );
		return(super.run( (IPage)null ));

	}


	protected void showDialog( JFrame frame, String msg, String item,
				   boolean error  ) {
		System.err.println( msg );
	}

	/**
	 *	pass the username to the admin server
	 */
	public String username(Object authObject, CommRecord cr) {
		return( m_username );
	}
	
	/**
	 *	pass the user password to the admin server
	 */
	public String password(Object authObject, CommRecord cr) {
		return( m_password );
	}

	/**
	 * This function is to extract specified parameters from the
	 * arguments list. Returns true if the user entered everything
	 * correctly, false otherwise
	 * @param args list of args
	 */
	protected static GetOpt extractParameters(String privateOpts, String args[]) { 
		
		GetOpt options = new GetOpt("Hh:p:u:w:i:v" + privateOpts, args);
		
		/* -H Help */
		if (options.hasOption('H')) {
			return (null);
		} /* Help */
  
		if (options.hasOption('v')) {
            _fTrace = true;
			Debug.setTrace( true );
		}

		/* -h host */
		if (options.hasOption('h'))
			m_host = options.getOptionParam('h');
		
		/* -p port */
		if (options.hasOption('p')) { /* if the option is -p */
			try { 
				m_port = Integer.parseInt(options.getOptionParam('p'));
			} catch (NumberFormatException e) { 
				System.err.println("Invalid Port");
				System.exit(-1);
			}
		} /* if the option is -p */

		/* -u username */
		if (options.hasOption('u'))
			m_username = options.getOptionParam('u');

		/* -w password */
		if (options.hasOption('w'))
			m_password = options.getOptionParam('w');

		/* -i instance */
		if (options.hasOption('i'))
			m_instance = options.getOptionParam('i');
		

		if (m_host == null || m_port == 0 ||
		    m_username == null || m_password == null ||
		    m_instance == null) {
		  return (null);
		}
		
		return (options);

	}
	
	/**
	 * Prints usage.
	 */
	protected static void doUsage(String appname) {
		String msg = _resource.getString( "cmdln",
										  "usage-msg",
										  appname );
		System.out.println( msg );
	}

	/**
	 * Print error message, using key from derived class
	 */
	protected void printError( String arg ) {
		if ( _errorCode != 0 ) {
			String msg = _resource.getString( "dirtask",
											  "error-" + _errorCode + "-msg",
											  arg );
			System.err.println( msg );
		}
	}

	protected void printError() {
		printError( null );
	}

	/**
	 * Internal variables
	 */
	private static String m_host     = null;
	private static int    m_port     = 0;
	private static String m_username = null;
	private static String m_password = null;
	protected static String m_instance = null;
	protected static boolean _fTrace = false;
	protected static ResourceSet _resource = DSUtil._resource;
}
