/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.cmdln;

import netscape.ldap.util.GetOpt;

/**
 *	Netscape Admin Server 4.0 commandline utility for restoring 
 *      the directory server's database.
 *
 * @author  RJP
 * @version %I%, %G%
 * @date	 	1/20/98
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.CGITask
 */
public class DSRestore extends DSCommandLine {
	public DSRestore() {
	}

	public static void main( String[] args ) {
		try {
			/* Make a new commandline class */
			DSRestore task = new DSRestore();
			
			/* extract parameters from the arguments list */
			if (!extractParameters(args)) {
				doUsage();
				System.exit( 1 );
			}
			
			/* Run DSCommandLine run method */
			if ( !task.run("Restore?filename=" + m_bakdir) ) {
				task.printError( m_bakdir );
				System.exit( -1 );
			}
		} catch ( Exception e ) {
			System.err.println( e.toString() );
			System.exit( -1 );
		}		
		System.exit( 0 );
	}
	
	
	/**
	 * This function is to extract specified parameters from the
	 * arguments list.
	 * @param args list of args
	 */
	protected static boolean extractParameters(String args[]) { 
		
		String privateOpts = "d:";
		
		GetOpt options = DSCommandLine.extractParameters( privateOpts, args );
		
		if ( options == null) {
			return (false);
		}
		
		/* -d backup directory */
		if (options.hasOption('d')) {
		  m_bakdir = options.getOptionParam('d');
		  return(true);
		}
		
		return(false);

	}

	/**
	 * Prints usage.
	 */
	private static void doUsage() {
		DSCommandLine.doUsage("DSRestore");
		String msg = _resource.getString( "dsrestore", "usage-msg" );
		System.out.println( msg );
	}

	/**
	 * Internal variables
	 */
	private static String m_bakdir = null;

}




