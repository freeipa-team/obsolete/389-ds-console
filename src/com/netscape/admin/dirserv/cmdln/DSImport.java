/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv.cmdln;

import netscape.ldap.util.GetOpt;

/**
 *	Netscape Admin Server 4.0 commandline utility for importing
 *      the directory server's database.
 *
 * @author  RJP
 * @version %I%, %G%
 * @date	 	3/11/98
 * @see     com.netscape.admin.dirserv.DSAdmin
 * @see     com.netscape.admin.dirserv.CGITask
 */
public class DSImport extends DSCommandLine {
	public DSImport() {
	}

	public static void main( String[] args ) {
		try {
			/* Make a new commandline class */
			DSImport task = new DSImport();
			
			/* extract parameters from the arguments list */
			if (!extractParameters(args)) {
				doUsage();
				System.exit( 1 );
			}
			
			/* Run DSCommandLine run method */
			if( !task.run("Import?filename=" + m_ldif) ) {
				task.printError( m_ldif );
				System.exit( -1 );
			}
		} catch ( Exception e ) {
			System.err.println( e.toString() );
			System.exit( -1 );
		}		
		System.exit( 0 );
	}
	
	
	/**
	 * This function is to extract specified parameters from the
	 * arguments list.
	 * @param args list of args
	 */
	protected static boolean extractParameters(String args[]) { 
		
		String privateOpts = "f:";
		
		GetOpt options = DSCommandLine.extractParameters( privateOpts, args );
		
		if ( options == null) {
			return (false);
		}
		
		/* -f ldif file */
		if (options.hasOption('f')) {
		  m_ldif = options.getOptionParam('f');
		  return(true);
		}
		
		return(false);

	}

	/**
	 * Prints usage.
	 */
	private static void doUsage() {
		DSCommandLine.doUsage("DSImport");
		String msg = _resource.getString( "dsimport", "usage-msg" );
		System.out.println( msg );
	}

	/**
	 * Internal variables
	 */
	private static String m_ldif = null;

}




