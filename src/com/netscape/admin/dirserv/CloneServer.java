/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.util.*;
import java.net.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.client.util.ResourceSet;
import com.netscape.management.client.util.Debug;
import com.netscape.management.client.util.RemoteImage;
import com.netscape.management.client.util.LDAPUtil;
import com.netscape.management.client.util.UtilConsoleGlobals;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.admin.dirserv.task.CGITask;
import com.netscape.admin.dirserv.task.Remove;
import com.netscape.admin.dirserv.task.Start;

/**
 * Handles cloning of directory servers over LDAP.  There will be a source
 * LDAP server and a destination LDAP server.  There will also be a
 * connection to the Configuration server because we will need to read
 * some entries from that server.
 *
 * @author	richm
 * @version %I%, %G%
 * @date		07/10/98
 */

public class CloneServer {

	/**
	 * The configDS is the connection to the Configuration directory where
	 * the topology and SIE information is stored.  The sourceDN is the SIE
	 * of the source DS, and the destDN is the SIE of the destination DS.
	 */
	public static boolean cloneServer(LDAPConnection configDS, String sourceDN,
									  String destDN, JFrame frame) {
		Debug.println("CloneServer.cloneServer: begin: configDS=" +
					  DSUtil.format(configDS) + " sourceDN=" + sourceDN +
					  " destDN=" + destDN);
		// sourceValues will contain the attribute/value pairs from the DN
		Hashtable sourceValues = new Hashtable();
		LDAPConnection source = getConnection(configDS, sourceDN, sourceValues,
											  frame);
		if (source == null)
			return false;

		// destValues will contain the attribute/value pairs from the DN
		Hashtable destValues = new Hashtable();
		LDAPConnection dest = getConnection(configDS, destDN, destValues,
											frame);
		if (dest == null) {
			try {
				source.disconnect();
			} catch (Exception e) {
			}
			return false;
		}

		// now we have ldap connections to the source and destination for the
		// clone; we want to read the configuration entries in the source and
		// write the values to the corresponding entries on the destination
		DN[] entryList = getEntriesToClone(source, frame);
		if (entryList == null) {
			DSUtil.showErrorDialog(frame, "clonesourceempty", "cn=config");
			try {
				source.disconnect();
			} catch (Exception e) {
			}
			try {
				dest.disconnect();
			} catch (Exception e) {
			}
			return false;
		}

		for (int ii = 0; ii < entryList.length; ++ii) {
			Debug.println("entryList[" + ii + "]=" + entryList[ii]);
		}

		String oldDir = replace(sourceValues.get("serverroot").toString() +
								"/" +
								sourceValues.get("nsserverid").toString(),
								"\\", "/");

		String newDir = replace(destValues.get("serverroot").toString() +
								"/" +
								destValues.get("nsserverid").toString(),
								"\\", "/");

		// some attributes can only be cloned if the servers are not
		// running on the same host, so we need to determine if the
		// source and destination hosts are the same
		String sourceHost = (String)sourceValues.get("serverhostname");
		try {
			sourceHost = DSUtil.canonicalHost(sourceHost);
		} catch (UnknownHostException e) {
		}
		if (sourceHost == null || sourceHost.equals(""))
			sourceHost = (String)sourceValues.get("serverhostname");

		String destHost = (String)destValues.get("serverhostname");
		try {
			destHost = DSUtil.canonicalHost(destHost);
		} catch (UnknownHostException e) {
		}
		if (destHost == null || destHost.equals(""))
			destHost = (String)destValues.get("serverhostname");

		boolean hostsAreSame = sourceHost.equalsIgnoreCase(destHost);

		// for each entry, read it from the source, delete those attributes
		// we don't want to copy, and write the entry to the destination
		String err = null; // which error dialog
		String[] args = null; // arguments for dialog
		boolean cloneWasSuccessful = true;
		for (int index = 0;
			 cloneWasSuccessful && index < entryList.length; ++index) {
			LDAPEntry entry = null;
			Debug.println("CloneServer.cloneServer: reading entry DN=" +
						  entryList[index].toString());
			boolean done = false;
			while (!done) {
				try {
					entry = source.read(entryList[index].toString());
					// if the entry is null or contains no elements, this
					// usually means that we didn't have permission to read
					// that particular entry, so we need to reauthenticate
					// as a more priviledged user
					if (entry == null) {
						throw new LDAPException("no elements",
							LDAPException.INSUFFICIENT_ACCESS_RIGHTS);
					}
					done = true;
				} catch (LDAPException lde) {
					done = reauthenticate(source, lde,
										  entryList[index].toString(),
										  frame, "clonesourceread");
					if (done) // unhandled error or cancel
						cloneWasSuccessful = false;
				}
			}

			if (entry != null) {
				Debug.println("CloneServer.cloneServer: modifying DN=" +
							  entry.getDN());
				LDAPModificationSet ldapmodset = getMods(entry, dest, frame,
														 hostsAreSame, oldDir,
														 newDir);

				done = false;
				if (ldapmodset == null || ldapmodset.size() == 0) {
					done = true; // nothing to be done
				} else {
					Debug.println("CloneServer.cloneServer: DN=" +
								  entry.getDN() + " num attrs to modify=" +
								  ldapmodset.size());
				}
				while (!done) {
					try {
						dest.modify(entryList[index].toString(), ldapmodset);
						done = true;
					} catch (LDAPException lde) {
						done = reauthenticate(dest, lde,
											  entryList[index].toString(),
											  frame, "clonedestwrite");
						if (done) // unhandled error or cancel
							cloneWasSuccessful = false;
					}
				}
			} else { // entry is null
				Debug.println("DSAdmin.cloneFrom(): could not read entry " +
							  "from source " + DSUtil.format(source));
				err = "clonesourceempty";
				args = new String[1];
				args[0] = entryList[index].toString();
			}

			if (err != null) {
				DSUtil.showErrorDialog(frame, err, args);
				cloneWasSuccessful = false;
			}
			err = null; // reset for next entry
			args = null; // reset for next entry
		}

		if (cloneWasSuccessful)
			cloneWasSuccessful = cloneSchema(source, dest, frame);

		try {
			source.disconnect();
		} catch (Exception e) {
		}
		try {
			dest.disconnect();
		} catch (Exception e) {
		}

		if (cloneWasSuccessful) {
			DSUtil.showInformationDialog(frame, "clonesuccess",
										 (String)null);
		} else {
			DSUtil.showErrorDialog(frame, "clonefailed", (String)null);
		}

		return cloneWasSuccessful;
	}

	private static LDAPConnection getConnection(LDAPConnection configDS,
												String sieDN, Hashtable values,
												JFrame frame) {
		values.put("nsbinddn", "");
		values.put("cn", "");
		values.put("serverroot", "");
		values.put("nsserverid", "");

		LDAPConnection ldc = null;
		try {
			ldc = DSAdmin.getLDAPConnection(configDS, sieDN,
											"serverhostname",
											"nsserverport",
											values);
		} catch (LDAPException lde) {
			String host = (String)values.get("serverhostname");
			String port = (String)values.get("nsserverport");
			Debug.println("CloneServer.cloneServer(): could not read DN = " +
						  sieDN + " or connect to " + host + ":" + port +
						  " LDAP exception = " + lde);
		} catch (Exception other) {
			String host = (String)values.get("serverhostname");
			String port = (String)values.get("nsserverport");
			Debug.println("CloneServer.cloneServer(): could not read DN = " +
						  sieDN + " or connect to " + host + ":" + port +
						  " exception = " + other);
		}

		if (ldc == null || !ldc.isConnected()) {
			String name = (String)values.get("cn");
			String host = (String)values.get("serverhostname");
			String port = (String)values.get("nsserverport");
			DSUtil.showErrorDialog(frame, "120", name + " " +
								   host + ":" + port);
			return null;
		}

		return ldc;
	}

	/*
	 * The entries to clone from the source can be found by enumerating the
	 * nsslapd-privatenamespaces attributes in the entry cn=config and by
	 * getting these entries and their corresponding ldbm-config children.
	 * Also, by looking at cn=features, cn=config and its children.
	 */
	static private DN[] getEntriesToClone(LDAPConnection ldc, JFrame frame) {
		Vector entryList = null;
		String configDN = "cn=config";
		// list of DNs to ignore
		String[] ignoreList = { "cn=monitor", "cn=schema", "cn=encryption",
								"cn=features" };
		String feConfigAttr = "nsslapd-privatenamespaces";
		LDAPEntry configEntry = null;
		String[] attrs = { feConfigAttr };
		boolean done = false;

		while (!done) {
			try {
				configEntry = ldc.read(configDN, attrs);
				// if the entry is null or contains no elements, this
				// usually means that we didn't have permission to read
				// that particular entry, so we need to reauthenticate
				// as a more priviledged user
				if (configEntry == null) {
					throw new LDAPException("no elements",
								LDAPException.INSUFFICIENT_ACCESS_RIGHTS);
				}
				done = true;
			} catch (LDAPException lde) {
				done = reauthenticate(ldc, lde, configDN, frame,
									  "clonesourcread");
			}
		}

		if (configEntry == null)
			return null;

		entryList = new Vector();
		for (int ii = 0; ii < attrs.length; ++ii) {
			Enumeration en = configEntry.getAttribute(attrs[ii]).
				getStringValues();
			while (en.hasMoreElements()) {
				String dn = (String)en.nextElement();

				if (dn.equals(""))
					continue; // ignore root DSE

				for (int jj = 0; jj < ignoreList.length; ++jj) {
					if (dn.indexOf(ignoreList[jj]) != -1) {
						dn = null;
						break;
					}
				}

				if (dn == null)
					continue;

				// for the config entries, we need to do a subtree
				// search to get all of their entries as well
				LDAPSearchResults result = null;
				String filter = "(objectclass=*)";
					
				try {
					result = ldc.search(dn, LDAPConnection.SCOPE_SUB,
										filter, null, false);
				} catch (LDAPException e) {
					Debug.println("error CloneServer.getEntriesToClone: LDAP" +
								  " search failed: " + filter);
					Debug.println("error CloneServer.getEntriesToClone: " +
								  "could not find any servers under " +
								  dn + " error: " + e);
				}

				while (result != null && result.hasMoreElements()) {
					LDAPEntry lde = (LDAPEntry)result.nextElement();
					for (int jj = 0; jj < ignoreList.length; ++jj) {
						if (lde.getDN().indexOf(ignoreList[jj]) != -1) {
							lde = null;
							break;
						}
					}
					if (lde != null)
						entryList.addElement(new DN(lde.getDN()));
				}
			}
		}

		DN[] retval = null;
		if (entryList != null && entryList.size() > 0) {
			retval = new DN[entryList.size()];
			entryList.copyInto(retval);
		}

		return retval;
	}

	static private boolean reauthenticate(
		LDAPConnection ldc, LDAPException lde, String dn, JFrame frame,
		String err
	) {
		boolean done = false;
		Debug.println("CloneServer.reauthenticate(): LDAP error code=" +
					  lde.getLDAPResultCode() + " error=" +
					  lde);
		Debug.println("CloneServer.reauthenticate(): could not read DN = " +
					  dn + " from source ldap = " +
					  DSUtil.format(ldc));
		// if the read failed due to permissions, notify the user
		// via a dialog and ask the user to authenticate using a
		// different user id/password.	If the user selects OK,
		// popup the password dialog asking for another user id and
		// password.  If the user hits OK, update the userid and
		// password in the _ldc object and retry the modify.  Keep
		// looping until: 1. The modify succeeds 2. The user hits
		// Cancel in either the confirm dialog or the password
		// dialog 3. we get some other type of LDAP error
		if (lde.getLDAPResultCode() ==
			LDAPException.INSUFFICIENT_ACCESS_RIGHTS) {
			// display a message
			DSUtil.showPermissionDialog(frame, ldc);
			// hack; for some reason, reauthenticate does not
			// rebind to the server; so, I disconnect in order to
			// force a re-connection
			try {
				ldc.disconnect();
			} catch (Exception e) {
			}
			done = !DSUtil.reauthenticate(ldc, frame, null,
										  ldc.getAuthenticationDN(),
										  null);
		} else {
			Debug.println("CloneServer.reauthenticate(): could not " +
						  "handle " +
						  "the exception, entry will not be " +
						  "read from source " + DSUtil.format(ldc));
			if (err != null) {
				String[] args = { dn, lde.toString() };
				DSUtil.showErrorDialog(frame, err, args);
			}
				
			done = true; // could not read entry ...
		}

		return done;
	}

	static private boolean cloneSchema(LDAPConnection source,
									   LDAPConnection dest,
									   JFrame frame) {
		String schemaDN = "cn=schema";

		LDAPEntry sourceSchema = null;
		try {
			sourceSchema = source.read(schemaDN);
		} catch (LDAPException e) {
			sourceSchema = null;
			String[] args = { schemaDN, e.toString() };
			DSUtil.showErrorDialog(frame, "clonesourceread", args);
		}

		if (sourceSchema == null)
			return false;

		Debug.println("CloneServer.cloneSchema(): read source schema");
		LDAPEntry destSchema = null;
		try {
			destSchema = dest.read(schemaDN);
		} catch (LDAPException e) {
			destSchema = null;
			String[] args = { schemaDN, e.toString() };
			DSUtil.showErrorDialog(frame, "clonedestwrite", args);
		}

		if (destSchema == null)
			return false;

		Debug.println("CloneServer.cloneSchema(): read dest schema");
		// read the dest schema values into a hashtable
		Hashtable dhash = new Hashtable();
		LDAPAttributeSet las = destSchema.getAttributeSet();
		for (Enumeration attrs = las.getAttributes();
			 attrs.hasMoreElements();) {
			LDAPAttribute la = (LDAPAttribute)attrs.nextElement();
			for (Enumeration vals = la.getStringValues();
				 vals.hasMoreElements();) {
				String val = (String)vals.nextElement();
				if (val == null)
					continue;
				dhash.put(val, la.getName());
			}
		}

		Debug.println("CloneServer.cloneSchema(): dest schema size=" +
					  dhash.size());
		// loop through the source looking for values which are not
		// present in the dest
		Vector ocList = null; // for objectclass mods
		LDAPModificationSet lms = null; // for attribute mods
		int op = LDAPModification.ADD;
		las = sourceSchema.getAttributeSet();
		for (Enumeration attrs = las.getAttributes();
			 attrs.hasMoreElements();) {
			LDAPAttribute la = (LDAPAttribute)attrs.nextElement();
			LDAPAttribute newla = null;
			for (Enumeration vals = la.getStringValues();
				 vals.hasMoreElements();) {
				String val = (String)vals.nextElement();
				if (val == null)
					continue;
				if (!dhash.containsKey(val)) {
					/*
					  Objectclasses should be added via the
					  LDAPSchemaElement.add interface rather than by
					  the modify() interface
					*/
					if (isObjectClass(la)) {
						if (ocList == null)
							ocList = new Vector();
						ocList.addElement(new LDAPObjectClassSchema(val));
					} else {
						if (newla == null)
							newla = new LDAPAttribute(la.getName());
						newla.addValue(val);
					}
					Debug.println(9, "CloneServer.cloneSchema(): found " +
								  "attribute value not present on dest=" +
								  newla);
				}
			}
			// now newla contains only those values not present on the dest
			if (newla != null) {
				if (lms == null)
					lms = new LDAPModificationSet();
				lms.add(op, newla);
			}
		}

		if ((lms == null || lms.size() == 0) &&
			((ocList == null) || (ocList.size() == 0))) {
			Debug.println("CloneServer.cloneSchema(): source and dest are" +
						  " identical, nothing to modify");
			return true;
		}

		Debug.println("CloneServer.cloneSchema(): modifying " +
					  lms.size() + " schema elements");
		try {
			dest.modify(schemaDN, lms);
			for (int ii = 0; ocList != null && ii < ocList.size(); ++ii) {
				LDAPObjectClassSchema locs =
					(LDAPObjectClassSchema)ocList.elementAt(ii);
				locs.add(dest);
			}
		} catch (LDAPException e) {
			destSchema = null;
			String[] args = { schemaDN, e.toString() };
			DSUtil.showErrorDialog(frame, "clonedestwrite", args);
			return false;
		}

		Debug.println("CloneServer.cloneSchema(): success");
		return true;
	}

	static private LDAPAttribute convertValue(LDAPAttribute la, int factor) {
		Vector newVals = new Vector();
		for (Enumeration e = la.getStringValues(); e.hasMoreElements();) {
			String sval = (String)e.nextElement();
			int ival = -1;
			try {
				Debug.println("Converting value=" + sval + " using" +
					" factor=" + factor);
				ival = Integer.parseInt(sval);
				if (factor != 0) {
					ival /= factor;
					sval = Integer.toString(ival);
					Debug.println("Converting new value=" + sval);
				}
			} catch (Exception ex) {
			} finally {
				newVals.addElement(sval);
			}
		}

		String[] nv = new String[newVals.size()];
		newVals.copyInto(nv);
		return new LDAPAttribute(la.getName(), nv);
	}

	static private String replace(String src, String old, String news) {
		int index = src.indexOf(old);
		if (index == -1)
			return src;

		StringBuffer sb = new StringBuffer();
		int start = 0;
		while (index != -1) {
			// copy the src into the buffer
			sb.append(src.substring(start, index));
			// copy the new string instead of the old one
			sb.append(news);
			// start the next copy after this one
			start = index + old.length();
			// find the next occurance of the old string
			index = src.indexOf(old, start);
		}
		// copy remainder of string
		if (start < src.length())
			sb.append(src.substring(start));

		return sb.toString();
	}

	static private LDAPAttribute replaceValue(LDAPAttribute la, String oldStr,
											  String newStr) {
		Vector newVals = new Vector();
		for (Enumeration e = la.getStringValues(); e.hasMoreElements();) {
			String sval = (String)e.nextElement();
			newVals.addElement(replace(sval, oldStr, newStr));
		}

		String[] nv = new String[newVals.size()];
		newVals.copyInto(nv);
		return new LDAPAttribute(la.getName(), nv);
	}

	static private LDAPModificationSet getMods(LDAPEntry sourceEntry,
											   LDAPConnection dest,
											   JFrame frame,
											   boolean hostsAreSame,
											   String oldDir, String newDir) {
		
		LDAPEntry destEntry = null;
		boolean done = false;
		Debug.println("CloneServer.getMods(): getting attributes to modify" +
					  " for DN=" + sourceEntry.getDN());
		boolean noEntry = false;
		while (!done) {
			try {
				destEntry = dest.read(sourceEntry.getDN());
				// if the entry is null or contains no elements, this
				// usually means that we didn't have permission to read
				// that particular entry, so we need to reauthenticate
				// as a more privileged user
				if (destEntry == null) {
					throw new LDAPException("no elements",
								LDAPException.INSUFFICIENT_ACCESS_RIGHTS);
				}
				done = true;
			} catch (LDAPException lde) {
				if (lde.getLDAPResultCode() == LDAPException.NO_SUCH_OBJECT) {
					noEntry = true;
					done = true;
					// its ok if the dest entry does not exist because we will
					// create it
				} else {
					done = reauthenticate(dest, lde,
										  sourceEntry.getDN(),
										  frame, "clonedestread");
				}
			}
		}

		if (destEntry == null && !noEntry)
			return null;

		Debug.println("CloneServer.getMods(): got dest entry DN=" +
					  sourceEntry.getDN());
		LDAPAttributeSet ldas = sourceEntry.getAttributeSet();

		LDAPModificationSet ldapmodset = new LDAPModificationSet();
		// loop through sourceEntry attributes; if the attribute exists on the
		// source entry but not on the dest entry, the op is add otherwise
		// it's replace
		for (Enumeration e = ldas.getAttributes();
			 e.hasMoreElements();) {
			LDAPAttribute lda = (LDAPAttribute)e.nextElement();
			Enumeration e2 = lda.getStringValues();
			// Make sure the attribute has a value.  Only attributes
			// which have a value will be ADDed or REPLACEd.  Attributes
			// which do not have a value will be handled by the delete
			// loop below
			if (!hasAValue(lda))
				continue;

			int op = LDAPModification.REPLACE;
			if ((destEntry == null) ||
				!hasAValue(destEntry.getAttribute(lda.getName())))
				op = LDAPModification.ADD;
			else if (noCopyAttrs.containsKey(lda.getName()))
				continue; // we cannot replace noCopyAttrs, but we
						  // can add them

			// see if we need to do some conversion of the value before
			// we write it to the dest
			if (convertAttrs.containsKey(lda.getName())) {
				Debug.println("converting attribute " +
							  lda.getName());
				Integer factor =
					(Integer)convertAttrs.get(lda.getName());
				lda = convertValue(lda, factor.intValue());
			} else if (replacePathAttrs.containsKey(lda.getName())) {
				lda = replaceValue(lda, oldDir, newDir);
			} else if (hostsAreSame &&
					   differentHostsAttrs.containsKey(lda.getName())) {
				continue; // do not add this attribute to the
				// list to copy
			}

			ldapmodset.add(op, lda);
		}

		// loop through destEntry attributes; if the attribute exists on
		// the dest entry but not the source, or if the attribute value
		// is a special delete or remove value, it should be deleted from
		// the dest
		if (destEntry != null) {
			ldas = destEntry.getAttributeSet();
			for (Enumeration e = ldas.getAttributes();
				 e.hasMoreElements();) {
				LDAPAttribute lda = (LDAPAttribute)e.nextElement();
				if (!hasAValue(lda))
					continue; // attribute does not exist on dest

				if (hasAValue(sourceEntry.getAttribute(lda.getName())))
					continue; // attribute exists on source

				int op = LDAPModification.DELETE;
				// check for attributes which are not removed the conventional
				// way . . .
				if (specialRemoveAttrs.containsKey(lda.getName())) {
					lda.addValue(specialRemoveAttrs.get(lda.getName()).toString());
					op = LDAPModification.REPLACE;
				}

				ldapmodset.add(op, lda);
			}
		}

		return ldapmodset;
	}

	/**
	 * An attribute has a value if its non null, if its size != 0, if
	 * it has a list of values, and if the values do not contain the
	 * special null or delete value
	 */
	static private boolean hasAValue(LDAPAttribute lda) {
		if (lda == null) {
			Debug.println("CloneServer.hasAValue(): attr is null");
			return false;
		}

		if (lda.size() == 0) {
			Debug.println("CloneServer.hasAValue(): attr=" + lda.getName() +
						  " is empty");
			return false;
		}

		Enumeration e = lda.getStringValues();
		if ((e == null) || !e.hasMoreElements()) {
			Debug.println("CloneServer.hasAValue(): attr=" + lda.getName() +
						  " has no values");
			return false;
		}

		boolean hasAVal = false;
		if (specialDeleteAttrs.containsKey(lda.getName())) {
			Debug.println("checking for deletion of attr=" + lda.getName() +
						  " value=" + lda.toString());
			for (; !hasAVal && e.hasMoreElements(); ) {
				String val = (String)e.nextElement();
				String delval =
					(String)specialDeleteAttrs.get(lda.getName());
				hasAVal = (val != null) && !val.equals(delval);
			}
		}
		else {
			hasAVal = true;
		}

		if (!hasAVal) {
			Debug.println("CloneServer.hasAValue(): attr=" + lda.getName() +
						  " has all null values");
		} else {
			Debug.println(9, "CloneServer.hasAValue(): attr=" + lda.getName() +
						  " has value=" + lda.toString());
		}
		return hasAVal;
	}

	/**
	 * Returns true if the attribute is the raw objectclass representation
	 * from the attribute value in the schema entry
	 */
	static private boolean isObjectClass(LDAPAttribute la) {
		return la.getName().equalsIgnoreCase("objectclasses");
	}

	/**
	 * noCopyAttrs is a look up table of attributes which should not be
	 * copied from the source to the dest.  Most of these are pretty
	 * obvious.
	 */
	static private Hashtable noCopyAttrs = new Hashtable();
	static {
		noCopyAttrs.put("cn", "");
		noCopyAttrs.put("objectclass", "");
		noCopyAttrs.put("oid", "");
		noCopyAttrs.put("nsslapd-config", "");
		noCopyAttrs.put("nsslapd-betype", "");
		noCopyAttrs.put("nsslapd-privatenamespaces", "");
		noCopyAttrs.put("nsslapd-rootdn", "");
		noCopyAttrs.put("nsslapd-rootpw", "");
		noCopyAttrs.put("nsslapd-instancedir", "");
		noCopyAttrs.put("nsslapd-useroc", "");
		noCopyAttrs.put("nsslapd-userat", "");
		noCopyAttrs.put("nsslapd-localhost", "");
		noCopyAttrs.put("nsslapd-security", "");
		noCopyAttrs.put("nsslapd-plugin", "");
		noCopyAttrs.put("nsslapd-include", "");
		noCopyAttrs.put("nsslapd-backendconfig", "");
		noCopyAttrs.put("nsslapd-accesslog", "");
		noCopyAttrs.put("nsslapd-accesslog-list", "");
		noCopyAttrs.put("nsslapd-errorlog", "");
		noCopyAttrs.put("nsslapd-errorlog-list", "");
		noCopyAttrs.put("nsslapd-auditlog", "");
		noCopyAttrs.put("nsslapd-auditlog-list", "");
		noCopyAttrs.put("nsslapd-requiresrestart", "");
		noCopyAttrs.put("nsslapd-database", "");
		noCopyAttrs.put("nsslapd-directory", "");
		noCopyAttrs.put("nsslapd-idlistscanlimit", ""); // why?
		noCopyAttrs.put("nsslapd-parentcheck", ""); // why?
		noCopyAttrs.put("nsslapd-mode", ""); // why?
		noCopyAttrs.put("nsslapd-groupevalnestlevel", ""); // why?
		// only the nsslapd-pluginenabled attribute should be copied
		noCopyAttrs.put("nsslapd-pluginpath", "");
		noCopyAttrs.put("nsslapd-plugininitfunc", "");
		noCopyAttrs.put("nsslapd-plugintype", "");
		noCopyAttrs.put("nsslapd-pluginid", "");
		noCopyAttrs.put("nsslapd-pluginversion", "");
		noCopyAttrs.put("nsslapd-pluginvendor", "");
		noCopyAttrs.put("nsslapd-plugindescription", "");
		noCopyAttrs.put("nsslapd-backend", "");
		noCopyAttrs.put("nsslapd-referralmode", "");
	}

	/**
	 * This is a table of attributes which have a special value
	 * which indicates that the destination should be deleted
	 * usually, if the source attribute is empty or has a null value,
	 * it means the destination attribute should be deleted.  There
	 * are also special attributes which can have a special value
	 * which means the destination should be deleted
	 * the key is the attribute name and the value is the special
	 * value which means deletion
	 */
	static private Hashtable specialDeleteAttrs = new Hashtable();
	static {
		specialDeleteAttrs.put("nsslapd-changelogmaxentries", "0");
		specialDeleteAttrs.put("nsslapd-changelogmaxage", "0");
		specialDeleteAttrs.put("nsslapd-accesslog-logmaxdiskspace", "0");
		specialDeleteAttrs.put("nsslapd-errorlog-logmaxdiskspace", "0");
		specialDeleteAttrs.put("nsslapd-auditlog-logmaxdiskspace", "0");
		specialDeleteAttrs.put("nsslapd-accesslog-maxlogsize", "0");
		specialDeleteAttrs.put("nsslapd-errorlog-maxlogsize", "0");
		specialDeleteAttrs.put("nsslapd-auditlog-maxlogsize", "0");
	}

	/**
	 * this is a table of attributes whose values must be converted
	 * by dividing by an integral before writing to the destination
	 */
	static private Hashtable convertAttrs = new Hashtable();
	static {
//		convertAttrs.put("passwordmaxage", new Integer(86400));
//		convertAttrs.put("passwordwarning", new Integer(86400));
	}

	/**
	 * this is a table of attributes whose values must undergo
	 * a string replacement of the path
	 */
	static private Hashtable replacePathAttrs = new Hashtable();
	static {
		replacePathAttrs.put("nsslapd-changelogdir", "");
	}

	/**
	 * this is a table of attributes that we only want to clone
	 * if the source and the destination are on different hosts
	 */
	static private Hashtable differentHostsAttrs = new Hashtable();
	static {
		differentHostsAttrs.put("nsslapd-port", "");
		differentHostsAttrs.put("nsslapd-secureport", "");
		differentHostsAttrs.put("nsslapd-ntsynch-port", "");
	}

	/**
	 * this is a table of attributes which have a special remove
	 * value e.g. we don't do an LDAP REMOVE, we just set the
	 * value of the attribute to this special value
	 */
	static private Hashtable specialRemoveAttrs = new Hashtable();
	static {
		specialRemoveAttrs.put("nsslapd-referral", "remove");
	}
}

