/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.io.*;
import java.net.*;
import java.util.*;
import netscape.ldap.util.MimeBase64Encoder;
import netscape.ldap.util.ByteBuf;
import com.netscape.management.client.util.*;
import netscape.ldap.*;
import netscape.ldap.util.*;
import com.netscape.admin.dirserv.panel.MappingUtils;

/**
 * DSExportImport
 * Imports and exports LDIF files.
 *
 * @version 1.0
 * @author rweltman
 **/
public class DSExportImport implements Runnable {

	/**
	 * Construct object with a reference to an open LDAP connection.
	 *
	 * @param ldc An open LDAP connection.
	 */
    public DSExportImport( LDAPConnection ldc ) {
		_ldc = ldc;
    }

	/**
	 * Construct object with a reference to an open LDAP connection,
	 * for export.
	 *
	 * @param ldc An open LDAP connection.
	 * @param listener A callback object for progress.
	 */
    public DSExportImport( LDAPConnection ldc,
						   String path, String base,
						   boolean includeOperational,
						   String dataVersion) {
		this( ldc );
		this.path = path;
		this.base = base;
		this.includeOperational = includeOperational;
        this.insertValue = dataVersion;
		_export = true;
	}

    public DSExportImport( LDAPConnection ldc,
						   String path, String base,
						   boolean includeOperational ) {
		this( ldc, path, base, includeOperational, null );
	}

	/**
	 * Construct object with a reference to an open LDAP connection,
	 * for importing an LDIF file.
	 *
	 * @param ldc An open LDAP connection.
	 * @param listener A callback object for progress.
	 * @param path Full path to input file.
	 * @param addOnly If true, fail if there are any modifies, deletes,
	 * or modRDNs in the LDIF file.
	 * @param continuous If true, continue to process remaining entries
	 * if one fails; consider an ADD on an existing entry to mean
	 * modify that entry.
	 * @param rejectsFile If non-null and set to a full file path,
	 * any rejected entries from the LDIF file will be reported here.
	 */
    public DSExportImport(  LDAPConnection ldc,
							String path, boolean addOnly,
							boolean continuous,
							String rejectsFile ) {
		this( ldc );
		this.path = path;
		this.addOnly = addOnly;
		this.continuous = continuous;	
		this.rejectsFile = rejectsFile;
		_export = false;
	}

    public void addEntryChangeListener( IEntryChangeListener listener ) {
		_listener = listener;
	}

    /**
     * Export the contents of a Directory to a file.
     *
     * @param path Full path to output file.
     * @param base Subtree to export. If null, the entire database will
     * be exported (all naming contexts from the root).
     * @param includeOperational If true, also output the operational
     * attributes (besides the regular attributes).
     * @param dataVersion if null, the copiedFrom attribute does not get
	 * copied. Otherwise, the copiedFrom attribute is added to the entry
	 * which has the same DN as the given base.
     */
    public int exportLDIF( String path, String base,
                           boolean includeOperational, String dataVersion)
                           throws IOException, LDAPException {
        insertValue = dataVersion;
        return exportLDIF(path, base, includeOperational);
    }

	/**
	 * Export the contents of a Directory to a file.
	 *
	 * @param path Full path to output file.
	 * @param base Subtree to export. If null, the entire database will
	 * be exported (all naming contexts from the root).
	 * @param includeOperational If true, also output the operational
	 * attributes (besides the regular attributes).
	 */
    public int exportLDIF( String path, String base,
						   boolean includeOperational )
		                   throws IOException, LDAPException {
		FileOutputStream out = new FileOutputStream( path );
		PrintWriter pw = new PrintWriter( out );
//		tmpPw = new PrintWriter( new FileOutputStream( path + ".tmp" ) );
        String[] attrs;
		if ( includeOperational ) {
			String[] operationalAttributes = null;
			LDAPSchema schema = null;
			try {
				/* Get the schema from the Directory */		
				schema = new LDAPSchema();
				schema.fetchSchema( _ldc );
			} catch ( LDAPException e ) {		
				schema = null;
			}
			if (schema != null) {
				operationalAttributes = DSSchemaHelper.getEditableOperationalAttributes(schema);
			}

			if (operationalAttributes != null) {
				attrs = new String[operationalAttributes.length + 1];
				for (int i=0; i < operationalAttributes.length; i++) {
					attrs[i] = operationalAttributes[i];
				}
				attrs[operationalAttributes.length] = "*";
			} else {
				attrs = new String[1];
				attrs[0] = "*";
			}
		} else {
			attrs = BASE_ATTRS;
		}
		int result = STATUS_OK;

		/* If exporting all naming contexts */
		if ( base == null ) {
			/* Get the root DSE */
			LDAPEntry entry = _ldc.read( "" );

			/* Get all the naming contexts */
			LDAPAttribute attr = entry.getAttribute( "namingContexts" );
			/* Export each naming context */
			Enumeration e = attr.getStringValues();
			while ( e.hasMoreElements() ) {
				base = (String)e.nextElement();
				/* Ignore the bogus cn=schema */
				if ( base.equalsIgnoreCase( "cn=schema" ) )
					continue;
				/* Not all naming contexts may have corresponding entries */
				try {
					Debug.println("DSExportImport.exporLDIF() searching under "+base);
					result = searchTree( pw, base, _ldc.SCOPE_SUB, attrs );
					if ( result != STATUS_OK ) {
						break;
					}
				} catch ( LDAPException ex ) {
					if ( ex.getLDAPResultCode() != ex.NO_SUCH_OBJECT )
						throw( ex );
					Debug.println( "DSExportImport.exportLDIF: " +
								   "no entries under <" + base + ">" );
				}
			}
		} else {
			Debug.println("DSExportImport.exporLDIF() searching under "+base);
			result = searchTree( pw, base, _ldc.SCOPE_SUB, attrs );
		}
		pw.flush();
		pw.close();
		_status = ( (result == STATUS_CANCEL) ||
					(result == STATUS_OK) );
		if ( result != STATUS_OK ) {
			/* Delete the file created, if any */
			try {
				File f = new File( path );
				f.delete();
				Debug.println( "DSExportImport.exportLDIF: deleted " +
									"<" + path + ">" );
			} catch ( Exception e ) {
				Debug.println( "DSExportImport.exportLDIF: " + e +
									" <" + path + ">" );
			}
		}
		return result;
    }
    
    private boolean doExport() throws IOException, LDAPException {
		_error = exportLDIF( path, base, includeOperational, insertValue );
		_finished = true;
		return ( (_error == STATUS_OK) || (_error == STATUS_CANCEL) );
	}


    public void run() {		
		_status = false;
		/* This is done in order the progress dialog to have time to display */
		try {
			Thread.sleep(100);
		} catch (Exception ex) {
		}
		try {
			if ( _export ) {
				_status = doExport();
			} else {
				_status = doImport();
			}
		} catch ( IOException e ) {
			_exception = e;
		} catch ( LDAPException e ) {
			_exception = e;
		}
		if ( _listener != null ) {
			Debug.println("DSExportImport.run(): at the end");
			_listener.entryChanged( null );
		}
		_finished = true;
	}

   public boolean getStatus() {
	   return _status;
   }

   public int getError() {
	   return _error;
   }

   public Exception getException() {
	   return _exception;
   }

   public boolean isFinished() {
	   return _finished;
   }

    private void breakString( PrintWriter pw, String value ) {
		int leftToGo = value.length();
		int written = 0;
		int maxChars = MAX_LINE;
		/* Limit to 150 characters per line */
		while( leftToGo > 0 ) {
			int toWrite = Math.min( maxChars, leftToGo );
			String s = value.substring( written, written+toWrite );
			if ( written != 0 )
				pw.print( " " + s );
			else {
				pw.print( s );
				maxChars -= 2;
			}
			written += toWrite; 
			leftToGo -= toWrite;
			/* Don't use pw.println, because it outputs an extra CR
			   in Win32 */
			pw.print( '\n' );
		}
		if ( tmpPw != null ) {
			tmpPw.print( value + '\n' );
			tmpPw.flush();
		}
	}

	/**
	 * Export a subtree of a Directory to a stream.
	 *
	 * @param pw Output stream.
	 * @param base Subtree to export.
	 * @param scope One of the standard LDAP search scopes.
	 * @param includeOperational If true, also output the operational
	 * attributes (besides the regular attributes).
	 */
    private int searchTree( PrintWriter pw, String base, int scope,
							String[] attrs )
		throws LDAPException, IOException {

		_chainingSuffix = getChainingSuffixForEntry(base);						

		MimeBase64Encoder encoder = new MimeBase64Encoder();
		LDAPSearchConstraints cons = _ldc.getSearchConstraints();
		cons.setMaxResults( 0 );
        LDAPSearchResults res =
            _ldc.search( base, scope, "(|(objectclass=*)(objectclass=ldapsubentry))", attrs, false, cons );
		int count = 0;
		/* Loop on results until finished */
		while ( res.hasMoreElements() ) {

			/* Next directory entry */
			LDAPEntry findEntry;
			try {
				findEntry = res.next();
			} catch ( LDAPException ex ) {
				_ldc.abandon( res );		
				throw ex;
			}	
			String dn = "";
			if ( findEntry.getDN() != null )
				dn = findEntry.getDN();
			breakString( pw, "dn: " + dn );
			if ( _listener != null ) {
				if ( !_listener.entryChanged( dn ) ) {
					_ldc.abandon( res );
					_entries += count;
					/* In this context, CANCEL is considered success */					
					return STATUS_CANCEL;
				}
			}

            if ((insertValue != null) && (dn.equalsIgnoreCase(base)))
                breakString(pw, "copiedFrom: "+insertValue);

			/* Get the attributes of the entry */
			LDAPAttributeSet findAttrs = findEntry.getAttributeSet();
			Enumeration enumAttrs = findAttrs.getAttributes();
			/* Loop on attributes */
			while ( enumAttrs.hasMoreElements() ) {
				LDAPAttribute anAttr =
					(LDAPAttribute)enumAttrs.nextElement();
				String attrName = anAttr.getName();
				/* Loop on values for this attribute */
				Enumeration enumVals;
				enumVals = anAttr.getByteValues();
// 				if ( !enumVals.hasMoreElements() ) {
// 					pw.print( attrName + ": \n" );
// 				}
				while ( enumVals.hasMoreElements() ) {
					byte[] b = (byte[])enumVals.nextElement();
					if ( (b == null) || (b.length < 1) ) {
						pw.print( attrName + ": \n" );
						continue;
					}
					boolean printable = LDIF.isPrintable( b );
					String s = null;
					if ( printable ) {
						try {
							s = new String( b, "UTF8" );
						} catch ( Exception e ) {
							printable = false;
						}
					}
					if ( printable ) {
						breakString( pw, attrName + ": " + s );
					} else {
						ByteBuf inBuf = new ByteBuf( b, 0, b.length );
						ByteBuf encodedBuf = new ByteBuf();
						/* Translate to base 64 */
						encoder.translate( inBuf, encodedBuf );
						int nBytes = encodedBuf.length();
						if ( nBytes > 0 ) {
							byte[] encoded = new byte[nBytes];
							encodedBuf.getBytes( 0, nBytes, encoded, 0 );
							s = new String( encoded, 0, nBytes );
							breakString( pw, attrName + ":: " + s );
						} else {
							pw.print( attrName + ": \n" );
						}
					}
				}
			}
	        pw.print( '\n' );
			count++;
		}
		pw.flush();
		_entries += count;
		Debug.println( "Wrote " + count + " entries for " + base );
		return STATUS_OK;
	}


    private boolean doImport() throws IOException, LDAPException {
		_error = importLDIF( path, addOnly, continuous,
							 rejectsFile );
		return ( (_error == STATUS_OK) || (_error == STATUS_CANCEL) );
	}

    /**
	 * Import an LDIF file to a Directory.
	 *
	 * @param path Full path to input file.
	 * @param addOnly If true, fail if there are any modifies, deletes,
	 * or modRDNs in the LDIF file.
	 * @param continuous If true, continue to process remaining entries
	 * if one fails; consider an ADD on an existing entry to mean
	 * modify that entry.
	 * @param rejectsFile If non-null and set to a full file path,
	 * any rejected entries from the LDIF file will be reported here.
	 */
    public int importLDIF( String path, boolean addOnly,
						   boolean continuous,
						   String rejectsFile )
		throws IOException, LDAPException {

		int result = STATUS_OK;		
		/* Open LDIF reader stream for LDIF file */
		LDIF ldif = new LDIF( path );

		/* Possible output stream for errors */
		PrintWriter reject = null;

		/* Loop on records in the LDIF file */
		LDIFRecord rec = null;
		boolean done = false;

		/* Initialize the variable used in the flux control algorithm for the on-line/bulk import*/
		_lastBusyTime = System.currentTimeMillis();

		_numEntry = 0;
		while( !done ) {
			try {
				_numEntry++;
				rec = ldif.nextRecord();
			} catch ( Exception e ) {
				/* AN ERROR OCCURRED READING LDIF FILE... */
				Debug.println( "DSExportImport.importLDIF getting next record: "+e);
				_rejects++; // This entry has been rejected...
				String s = "";
				if ( rec != null ) {
					if (!_lastDN.equals(rec.getDN())) {
						s = rec.getDN();
						_lastDN = s;
					}
				}
				/* Create error message */
				String msg;
                /* see if there was an invalid char in the input e.g. a non-utf8 8 bit char */
				if (e instanceof java.io.CharConversionException) {
					String[] args = {String.valueOf(_numEntry), 
									 e.toString()};					
					msg = DSUtil._resource.getString("import", "malformedinputerror-label", args);
					result = MALFORMED_EXPRESSION_ERROR;
				} else {					
					String[] args = {String.valueOf(_numEntry), 
									 e.getMessage()};
					msg = DSUtil._resource.getString("import", "ioerror-label", args);	
					result = STATUS_ERROR;
				}

				/* Notify to listener the error */
				if ( _listener != null ) {									
					if ( !_listener.entryChanged( s, msg ) ) {						
						Debug.println("DSExportImport.importLDIF() ERROR entryChanged for entry "+s);
						result = STATUS_ERROR;
						done = true;						
					}
				}			   
								
				/* Write to rejects file */
				if ( rejectsFile != null ) {
					try {
						if ( reject == null ) {
							reject = new PrintWriter(
													 new FileOutputStream( rejectsFile ) );
						}
					} catch ( Exception exc ) {
						Debug.println("DSExportImport.importLDIF creating PrintWriter"+ exc.toString() );					
						result = STATUS_UNWRITABLE;
						done = true;						
					}
					if ( reject != null ) {						
						reject.println(msg);						
						done = true;						
					}					
				}					
			}

			/* If the record is null it means:
			   a.) We reached the end of the file
			   or
			   b.) An error occurred while reading the file and there's not much more we can do...
			   */
			if ( rec == null ) {
				Debug.println( "DSExportImport.importLDIF: the LDIF record number "+_numEntry +" is null");
				if ( _listener != null ) {					
					_listener.entryChanged( null );
				}				
				done = true;			   
			}
			
			if (!done) {
				_lastDN = rec.getDN();	
				LDIFContent content = rec.getContent();
				LDAPModification mods[] = null;
				LDAPAttribute addAttrs[] = null;
				boolean doDelete = false;				
				boolean skip = false; // boolean that indicates that something went wrong with this record
				String error = "";		

				/* What type of record is this? */
				if ( content instanceof LDIFModifyContent ) {
					if (addOnly) {
						String[] args = {_lastDN};
						error = DSUtil._resource.getString("import", "onlyadderror-label", args);
						skip = true;
					} else {
						mods = ((LDIFModifyContent)content).getModifications();
					}			    				
				} else if ( content instanceof LDIFAddContent ) {
					addAttrs = ((LDIFAddContent)content).getAttributes();
				} else if ( content instanceof LDIFAttributeContent ) {
					/* No change type; treat it as an ADD */
					addAttrs =
						((LDIFAttributeContent)content).getAttributes();
				} else if ( content instanceof LDIFDeleteContent ) {
					if (addOnly) {
						String[] args = {_lastDN};
						error = DSUtil._resource.getString("import", "onlyadderror-label", args);
						skip = true;
					} else {
						doDelete = true;
					}				
				} else {
					String[] args = {_lastDN};
					error = DSUtil._resource.getString("import", "unknowrecordtype-label", args);
					skip = true;
				}
				
				if (!skip) {
					/* Prepare for doing the import: create the entry to add, or the modification set to 
					 modify */					
					LDAPModificationSet modSet = null;
					LDAPEntry newEntry = null;

					if ( mods == null ) {
						if ( addAttrs != null ) {
							LDAPAttributeSet set = new LDAPAttributeSet(addAttrs);
							newEntry = new LDAPEntry( _lastDN, set );
						}
					} else {					
						modSet = new LDAPModificationSet();
						for( int m = 0; m < mods.length; m++ ) {
							modSet.add( mods[m].getOp(),
										mods[m].getAttribute() );
						}						
					} 
				
					try {	
						while (importOneEntry(_lastDN, modSet, newEntry, mods, doDelete)) {
							Debug.println("DSExportImport.importLDIF(): going to sleep because "+_lastDN+" is busy...");
							/* Sleep.  Note: this is the same algorithm and the same values used when we execute
							   a replica init in the server */
							long timeNow = System.currentTimeMillis();						
							if ((timeNow - _lastBusyTime) < 
								(_sleepOnBusyTime + SLEEP_ON_BUSY_WINDOW)) {
								_sleepOnBusyTime +=5000;
							}
							else {
								_sleepOnBusyTime = 5000;
							}
							_lastBusyTime = timeNow;
							try {
								Thread.sleep(_sleepOnBusyTime);
							} catch(Exception ex) {
							}
							if ( _listener != null ) {
								if ( !_listener.entryChanged( _lastDN ) ) {
									Debug.println("DSExportImport.importLDIF() ERROR entryChanged for entry "+_lastDN+" STATUS CANCEL");
									result = STATUS_CANCEL;
								}
							}
						}
						if ( _listener != null ) {
							if ( !_listener.entryChanged( _lastDN ) ) {
								Debug.println("DSExportImport.importLDIF() ERROR entryChanged for entry "+_lastDN+" STATUS CANCEL");
								result = STATUS_CANCEL;
							}
						}
					} catch (LDAPException e) {		
						/* Error happened doing the import... */
						Debug.println("DSExportImport.importLDIF() (2) "+e);					
						String ldapError =  e.errorCodeToString();
						String ldapMessage = e.getLDAPErrorMessage();
						if ((ldapMessage != null) &&
							(ldapMessage.length() > 0)) {
							ldapError = ldapError + ". "+ldapMessage;
						}
						if ( mods != null ) {
							String[] args = {_lastDN,
											 ldapError,
											 mods.toString()};
							error = DSUtil._resource.getString("import", "error-modify-label", args);							
						} else if ( newEntry != null ) {
							String[] args = {_lastDN,
											 ldapError,
											 newEntry.toString()};
							error = DSUtil._resource.getString("import", "error-add-label", args);
						} else if ( doDelete ) {
							String[] args = {_lastDN,											 
											 ldapError};
							error = DSUtil._resource.getString("import", "error-delete-label", args);
						}
						//					Debug.println( dn + ": " + s );						
						skip = true;					
					}
				}			

				if ( skip ) {
					_rejects++;
				} else {
					_entries++;
				}
		
				if (skip) {
					/* Something wrong happened to the entry, just send the error to the listener */
					if ( _listener != null ) {
						if ( !_listener.entryChanged( _lastDN, error) ) {						
							result = STATUS_CANCEL;
						}
					}
					
					/* Write the error in the rejects file */
					if (rejectsFile != null) {
						try {
							if ( reject == null ) {
								reject = new PrintWriter(
														 new FileOutputStream( rejectsFile ) );
							}
						} catch ( Exception e ) {
							Debug.println( e.toString() );
							result = STATUS_UNWRITABLE;							
						}
						if ( reject != null ) {
							reject.println( error );							
						}
					}
				}
			
				if ((result == STATUS_CANCEL) ||
					(result == STATUS_UNWRITABLE) ||
					(!continuous && skip)) {
					done = true;
				}
			}
		}
		
		if ( reject != null ) {
			reject.flush();
		}
		
		return result;
	}

	/**
	 * This method adds/deletes/modifies the entry.
	 * @returns true if we had an BUSY exception (to control the flow in the bulk import)
	 * @throws Exception if other exception occurs
	 */
	private boolean importOneEntry( String dn,
									LDAPModificationSet modSet,
									LDAPEntry newEntry,
									LDAPModification[] mods,
									boolean doDelete) throws LDAPException {
		boolean isBusy = false;		
		try {
			if ( mods != null ) {					
				_ldc.modify( dn, modSet );
			} else if ( newEntry != null ) {					
				_ldc.add( newEntry );
			} else if ( doDelete ) {					
				_ldc.delete( dn );
			}			
		} catch (LDAPException e) {				
			/* We check if the server is busy: used to control the flow in the case of the online
			   import */
			Debug.println("DSExportImport.importOneEntry dn "+dn+" Exception "+e);
			if (e.getLDAPResultCode() == LDAPException.BUSY) {
				isBusy = true;
			} else {
				throw e;
			}
		}
		return isBusy;
	}


	/**
	 * Report the number of rejected entries (on import with the continuous
	 * mode set)
	 */
    public int getRejectCount() {
		return _rejects;
	}

	/**
	 * Report the number of entries exported or imported
	 */
    public int getEntryCount() {
		return _entries;
	}

	/**
	  * Returns the number of the entry that is actually used in the import
	  */
	public int getCurrentEntry() {
		return _numEntry;
	}

	/**
	 * Report the dn of the last entry treated for import
	 */

	public String getLastDN() {
		return _lastDN;
	}

	/* Returns the suffix of the chaining to which the entry with 'dn' belongs,
	   if it belongs to no chaining, it returns null */
	private String getChainingSuffixForEntry(String dn) {	
		String[] orderedAllSuffixes = MappingUtils.getOrderedSuffixList(_ldc, MappingUtils.ALL);
		String[] chainingSuffixes = MappingUtils.getOrderedSuffixList(_ldc, MappingUtils.CHAINING);
		String[] auxChaining = new String[1];
		for (int i=0; i<chainingSuffixes.length; i++) {		
			auxChaining[0] = chainingSuffixes[i];
			if (MappingUtils.isEntryInSuffixes(dn, auxChaining, orderedAllSuffixes)) {
				return chainingSuffixes[i];
			}
		}		
		return null;
	}

	public String getChainingName() {
		if (_chainingSuffix == null) {
			return null;
		}		
		return  MappingUtils.getBackendForSuffix(_ldc, _chainingSuffix);
	}

	public String getChainingSuffix() {
		if (_chainingSuffix == null) {
			return null;
		}
		return _chainingSuffix;
	}


    private LDAPConnection _ldc;
	private PrintWriter tmpPw = null;
	private IEntryChangeListener _listener = null;
    private String path;
	private String base;
	private boolean includeOperational;
    private boolean addOnly;
    private boolean continuous;
    private String rejectsFile;
	private String _lastDN;
	private boolean _status = false;
	private int _error = STATUS_OK;
	private boolean _finished = false;
	private Exception _exception = null;
    private boolean _export = true;
	private int _rejects = 0;
	private int _entries = 0;
	private int _numEntry = 0;
	public static final int STATUS_OK = 0;
	public static final int STATUS_CANCEL = 1;
	public static final int STATUS_ERROR = 2;
	public static final int STATUS_UNWRITABLE = 3;
	public static final int LDIF_SYNTAX_ERROR = 4;
	public static final int MALFORMED_EXPRESSION_ERROR = 5;
	/* Export seems to only export the modifier attributes for the
	   root DSE */
	private static final String[] ROOT_ATTRS = { "objectclass",
												"passwordchange",
												"passwordchecksyntax",
												"passwordminlength",
												"passwordexp",
												"passwordmaxage",
												"passwordwarning",
												"passwordkeephistory",
												"passwordinhistory",
												"passwordlockout",
												"passwordmaxfailure",
												"passwordunlock",
												"passwordlockoutduration",
												"passwordresetduration",
												"modifytimestamp",
												 "modifiersname" };
	
	private static final String[] BASE_ATTRS = { "*" };
	private static final int MAX_LINE = 300;
    private String insertValue = null;
	private long _sleepOnBusyTime = 0;
	private long _lastBusyTime = 0;
	private final long SLEEP_ON_BUSY_WINDOW = 10000;  /* 10 seconds */
	private String _chainingSuffix;
}
