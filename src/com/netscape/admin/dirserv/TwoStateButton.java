/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

package com.netscape.admin.dirserv;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.*;

/**
 * Toggle button which has a depressed state icon and an up state icon
 */
public class TwoStateButton extends JToggleButton {

	/**
	 * @param upIcon Image to use when not selected
	 * @param downIcon Image to use when selected
	 */
	public TwoStateButton( Icon upIcon, Icon downIcon ) {
		super(upIcon);
		_upIcon = upIcon;
		_downIcon = downIcon;
		Dimension dim = new Dimension( upIcon.getIconWidth(),
									   upIcon.getIconHeight() );
		setSize( dim );
		setMinimumSize( dim );
		setMaximumSize( dim );
		setPreferredSize( dim );
		setBorder( new EmptyBorder(0,0,0,0) );
		addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setIcon( isSelected() ? _downIcon : _upIcon );
			}
		}
		);
	}

	private Icon _downIcon;
	private Icon _upIcon;

	public static void main( String[] args ) {
		JDialog dlg = new JDialog();
		JPanel panel = new JPanel();
		dlg.setContentPane(panel);
		final TwoStateButton button = new TwoStateButton(
			new ImageIcon(UP_IMAGE),
			new ImageIcon(DOWN_IMAGE) );
		panel.add( button );
 		button.addActionListener( new ActionListener() {
 			public void actionPerformed(ActionEvent e) {
 				System.out.println( "Button pressed is " +
 									button.isSelected() );
 			}
 		}
 		);
		dlg.setLocation(new Point(300,320));
		dlg.setSize( 100, 100 );
		dlg.show();
	}
	private static final String UP_IMAGE = "images/authup.gif";
	private static final String DOWN_IMAGE = "images/authdown.gif";
}
